-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 10.0.1.232    Database: ibms_uat
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ibms_room_open_hours`
--

DROP TABLE IF EXISTS `ibms_room_open_hours`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ibms_room_open_hours` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `created_by` varchar(36) DEFAULT NULL COMMENT '创建人',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(36) DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int(11) DEFAULT NULL COMMENT '版本号',
  `from_hour` varchar(20) NOT NULL COMMENT '从时间',
  `to_hour` varchar(20) NOT NULL COMMENT '到时间',
  `room_id` varchar(36) NOT NULL COMMENT 'FK',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FKjr1d1tw18i27c399vbe08l2pn` (`room_id`) USING BTREE,
  CONSTRAINT `FKjr1d1tw18i27c399vbe08l2pn` FOREIGN KEY (`room_id`) REFERENCES `ibms_room` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='会议室开放时间';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ibms_room_open_hours`
--

LOCK TABLES `ibms_room_open_hours` WRITE;
/*!40000 ALTER TABLE `ibms_room_open_hours` DISABLE KEYS */;
INSERT INTO `ibms_room_open_hours` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `from_hour`, `to_hour`, `room_id`) VALUES ('0f02cce2-063f-43bd-85f0-b6e7454c98ca',NULL,'2020-12-30 20:23:18',0,NULL,'2020-12-30 20:23:18',0,'08:00','22:00','ed2dc26f-cd02-4142-86e1-eee8e0753d63');
INSERT INTO `ibms_room_open_hours` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `from_hour`, `to_hour`, `room_id`) VALUES ('5bc21d58-2e9a-4f69-ad70-2ea7d7663980',NULL,'2020-12-31 09:47:19',1,NULL,'2020-12-31 09:47:19',1,'08:00','22:00','6d9faaf8-9215-4402-aaaf-b2282a63bb87');
INSERT INTO `ibms_room_open_hours` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `from_hour`, `to_hour`, `room_id`) VALUES ('7780cf49-b158-4cc2-97c2-e719012be5b7',NULL,'2020-12-31 08:10:20',0,NULL,'2020-12-31 08:10:20',0,'08:00','22:00','4af6f5da-98f0-4fcc-9120-2b7f12b74426');
INSERT INTO `ibms_room_open_hours` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `from_hour`, `to_hour`, `room_id`) VALUES ('cba17b1a-9e96-478b-85f7-0acb198c85da',NULL,'2020-12-30 20:24:26',0,NULL,'2020-12-30 20:24:26',0,'08:00','22:00','e8e1e58b-6db8-4ca4-8133-7146d70a2177');
INSERT INTO `ibms_room_open_hours` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `from_hour`, `to_hour`, `room_id`) VALUES ('deab29d0-dbf4-45b1-84fe-1c96efd282e4',NULL,'2020-12-30 20:23:57',0,NULL,'2020-12-30 20:23:57',0,'08:00','22:00','11de355e-dd8e-4b3a-b6c7-483874d2cac2');
/*!40000 ALTER TABLE `ibms_room_open_hours` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-21 19:00:39
