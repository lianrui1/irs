package cn.com.cowain.ibms.entity.distinguish;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/6 15:35
 */
@Data
@Entity
@Table(name = "ibms_time_plan")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_time_plan", comment = "时间计划表")
@Where(clause = "is_del=0")
public class TimePlan extends BaseEntity {

    @Column(name = "name", nullable = false, columnDefinition = "varchar(8)  COMMENT '计划名称'")
    private String name;
}
