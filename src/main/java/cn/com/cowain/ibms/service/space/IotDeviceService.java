package cn.com.cowain.ibms.service.space;

import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.enumeration.iot.AccessControlSpotPlace;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.feign.bean.iotc.resp.DeviceResp;
import cn.com.cowain.ibms.mq.bean.DeviceStatusChangeMsgPushMessage;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.device.DeviceControlLogReq;
import cn.com.cowain.ibms.rest.req.iot.HIKFaceMachinePageReq;
import cn.com.cowain.ibms.rest.req.space.DevicePageReq;
import cn.com.cowain.ibms.rest.req.space.IotDeviceCreateReq;
import cn.com.cowain.ibms.rest.req.space.IotDeviceSortReq;
import cn.com.cowain.ibms.rest.resp.StaffResp;
import cn.com.cowain.ibms.rest.resp.device.DeviceControlLogResp;
import cn.com.cowain.ibms.rest.resp.iot.HIKAccessRulePageResp;
import cn.com.cowain.ibms.rest.resp.iot.IotDeviceNetworkChangeResp;
import cn.com.cowain.ibms.rest.resp.iot.IotDeviceResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;

import java.util.List;
import java.util.Optional;

/**
 * Description
 *
 * @author yuchunlei
 * @version 1.0.0
 * @createTime 2020年12月22日 09:49:00
 */
public interface IotDeviceService {

    /**
     * 判断设备是否是旷视鸿图设备
     *
     * @param device 设备
     * @return true ： 是鸿图设备， false ： 不是鸿图设备
     * @author Yang.Lee
     * @date 2022/2/14 10:23
     **/
    static boolean isKSHTDevice(IotDevice device) {

        // 判断依据： 设备类型为旷视设备，并且sn不以M开头。
        // 因为鸿图设备无SN，因为唐其亮将uuid赋给SN字段，而九霄设备有SN，且以M开头

        return DeviceType.FACE_RECOGNITION.equals(device.getDeviceType())
                && Optional.ofNullable(device.getSn()).isPresent()
                && !device.getSn().startsWith("M");
    }

    /**
     * 添加设备
     *
     * @param dto
     * @return
     */
    ServiceResult create(IotDeviceCreateReq dto);

    /**
     * 分页查询设备列表
     *
     * @param devicePageReq
     * @return
     */
    PageBean<IotDeviceResp> search(DevicePageReq devicePageReq);

    /**
     * 设备详情
     *
     * @param id
     * @param token
     * @return
     */
    IotDeviceResp getIotDeviceInfo(String id, String token);

    /**
     * 删除设备
     *
     * @param id
     * @return
     */
    ServiceResult deleteById(String id);

    /**
     * 编辑iot设备
     *
     * @param id
     * @param dto
     * @return
     */
    ServiceResult update(String id, IotDeviceCreateReq dto);

    /**
     * 对设备数据进行排序
     *
     * @param sort
     * @return
     */
    ServiceResult sort(IotDeviceSortReq sort);

    /**
     * 获取设备网络变化分页数据
     *
     * @param deviceId     设备ID
     * @param deviceStatus 设备状态
     * @param page         页码
     * @param size         页长
     * @return 分页数据
     * @author Yang.Lee
     * @date 2021/5/7 10:40
     **/
    PageBean<IotDeviceNetworkChangeResp> getDeviceNetworkChangedPage(String deviceId, String deviceStatus, int page, int size);

    /**
     * 控制设备
     *
     * @param deviceId 设备
     * @param cmd      控制指令
     * @return 控制结果
     * @author Yang.Lee
     * @date 2021/6/16 15:41
     **/
    ServiceResult control(String deviceId, String cmd);

    /**
     * 打开空间下的所有设备
     *
     * @param space 空间
     * @return 控制结果
     * @author Yang.Lee
     * @date 2021/6/16 15:43
     **/
    ServiceResult openSpaceDevices(Space space);

    /**
     * 关闭空间下所有设备
     *
     * @param space space 空间
     * @return 控制结果
     * @author Yang.Lee
     * @date 2022/1/19 15:45
     **/
    ServiceResult closeSpaceDevices(Space space);

    /**
     * 获取海康面板机分页数据
     *
     * @param req 请求参数
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/7/20 15:19
     **/
    PageBean<HIKAccessRulePageResp> getHIKFaceMachinePage(HIKFaceMachinePageReq req);

    /**
     * 从iotc获取设备列表数据
     *
     * @return 设备列表
     * @author Yang.Lee
     * @date 2021/10/27 16:12
     **/
    List<DeviceResp> getDeviceListFromIOTC();

    /**
     * 保存设备数据（从IOTC同步的数据）
     *
     * @param deviceList 设备数据
     * @return 保存结果
     * @author Yang.Lee
     * @date 2021/10/27 16:45
     **/
    ServiceResult saveDeviceList(List<DeviceResp> deviceList);

    /**
     * 楼栋总览
     *
     * @return
     */
    ServiceResult fullView();

    /**
     * 楼栋视图
     *
     * @param projectId
     * @param spaceId
     * @return
     */
    ServiceResult buildingView(String projectId, String spaceId);

    /**
     * @param spaceId
     * @return
     * @description 楼层视图
     * @author tql
     * @date 21-11-15
     */
    ServiceResult floorView(String spaceId);

    /**
     * 查询可以控制设备的员工列表
     *
     * @param deviceId 设备ID
     * @param keyword  查询关键字
     * @return 员工列表
     * @author Yang.Lee
     * @date 2021/11/15 22:07
     **/
    List<StaffResp> getDeviceStaff(String deviceId, String keyword);

    /**
     * 移除用户的设备控制权限
     *
     * @param deviceId 设备id
     * @param userHrId 用户工号
     * @return 操作结果
     * @author Yang.Lee
     * @date 2021/11/22 13:19
     **/
    ServiceResult removeDeviceControlAccess(String deviceId, String userHrId);

    // 获取所有面板机和门磁设备
    PageBean<HIKAccessRulePageResp> getFaceMachinePage(HIKFaceMachinePageReq req);

    /**
     * 移除设备的所有控制权限人员
     *
     * @param deviceId 设备ID
     * @return 操作结果
     * @author Yang.Lee
     * @date 2021/11/26 10:46
     **/
    ServiceResult removeDeviceAllControlUsers(String deviceId);

    /**
     * 消费iotc 修改设备mq 同步ibms
     *
     * @author: yanzy
     * @data: 2022/9/8 13:09:34
     */
    ServiceResult iotcToIbmsUpdDevice(DeviceResp deviceResp);

    /**
     * @param spaceId 空间id
     * @return
     * @description 根据空间id获取空间下所有子集
     * @author tql
     * @date 21-12-9
     */
    List<Space> getChildSpacesBySpaceId(String spaceId);

    // 发送设备状态变化推送消息
    void deviceStatusChangeMsgPush(DeviceStatusChangeMsgPushMessage parseObject);

    /**
     * 根据设备号删除设备
     *
     * @param deviceSn
     * @return
     */
    ServiceResult deleteBySn(String deviceSn);

    /**
     * 梯控点绑定设备场景-获取面板机列表
     *
     * @param req
     * @return
     */
    PageBean<HIKAccessRulePageResp> faceDevice(HIKFaceMachinePageReq req);


    /**
     * @param place 根据地区获取旷视鸿图报餐设备id
     * @return
     * @description
     * @author tql
     * @date 22-5-19
     */
    List<String> getKsHtPlaceDevice(AccessControlSpotPlace place);

    // 设备控制日志列表(分页)
    PageBean<DeviceControlLogResp> getDeviceControlLogPage(DeviceControlLogReq req);

    /**
     * 获取设备详情，优先从缓存中查询
     * @param id
     * @return
     */
    IotDevice getIotDeviceFirstFromRedis(String id);
}
