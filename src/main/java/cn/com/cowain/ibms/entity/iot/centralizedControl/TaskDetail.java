package cn.com.cowain.ibms.entity.iot.centralizedControl;


import lombok.Data;

@Data
public class TaskDetail {

    // 设备ID
    private String target;

    // 状态
    private String status;

    // 时间戳
    private Long t;

    // 任务执行数量
    private TaskProgress task_progress;
}
