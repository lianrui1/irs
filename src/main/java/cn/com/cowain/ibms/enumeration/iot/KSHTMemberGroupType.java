package cn.com.cowain.ibms.enumeration.iot;

/**
 * 旷视鸿图员工组类型
 *
 * @author Yang.Lee
 * @date 2022/2/14 10:48
 */
public enum KSHTMemberGroupType {

    STAFF("员工组", 1),

    VISITOR("访客组", 2),

    MEAL_STAFF("员工报餐组", 1),

    MEAL_VISITOR("访客报餐组", 2),

    OTHER("其他", -1);

    private String name;


    private int code;

    KSHTMemberGroupType(String name, int code) {
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return this.name;
    }

    public int getCode() {
        return this.code;
    }
}
