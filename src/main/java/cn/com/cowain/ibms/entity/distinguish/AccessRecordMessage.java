package cn.com.cowain.ibms.entity.distinguish;

import lombok.Data;

import java.io.Serializable;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/22 11:12
 */
@Data
public class AccessRecordMessage implements Serializable {
    /**
     * 从时间
     */
    private String passTime;

    /**
     * 抓怕照片
     */
    private String snapshotUrl;

    /**
     * 底库照片
     */
    private String personImageUrl;

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 设备地址
     */
    private String deviceLocation;

    /**
     * 通行人员工号
     */
    private String personCode;

    /**
     * 通行人员姓名
     */
    private String personName;

    /**
     * 员工类型
     **/
    private int recognitionType;
    /**
     * 体温
     */
    private String temperature;
    /**
     * 通行状态
     **/
    private int passType;
    /**
     * 通行状态
     **/
    private String snCode;
}
