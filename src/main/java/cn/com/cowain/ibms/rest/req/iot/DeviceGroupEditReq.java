package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

/**
 * @author Yang.Lee
 * @date 2021/10/21 10:08
 */
@Data
@ApiModel("设备群组编辑请求")
public class DeviceGroupEditReq {

    @ApiModelProperty(value = "父群组ID,如无父节点，则为null", position = 1)
    private String parentId;

    @NotEmpty
    @Length(max = 15)
    @ApiModelProperty(value = "群组名称", required = true, position = 2)
    private String name;

    @NotEmpty
    @Length(max = 100)
    @ApiModelProperty(value = "群组描述", required = true, position = 3)
    private String description;
}
