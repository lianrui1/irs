package cn.com.cowain.ibms.entity.office;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Yang.Lee
 * @date 2021/12/7 13:32
 */
@Data
@Entity
@Table(name = "ibms_intelligent_office_user_guide")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "ibms_intelligent_office_user_guide", comment = "智能办公室用户引导记录表")
public class IntelligentOfficeUserGuide extends BaseEntity {

    @Column(name = "user_hr_id", nullable = false, columnDefinition = "varchar(20) COMMENT '用户工号'")
    private String userHrId;
}
