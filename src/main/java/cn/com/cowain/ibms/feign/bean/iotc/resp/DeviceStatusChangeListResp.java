package cn.com.cowain.ibms.feign.bean.iotc.resp;

import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.utils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author Yang.Lee
 * @date 2021/10/26 15:47
 */
@Data
public class DeviceStatusChangeListResp {

    private Device device;

    private DeviceStatus deviceStatus;

    @JsonFormat(pattern = DateUtils.PATTERN_DATETIME, shape = JsonFormat.Shape.STRING)
    private LocalDateTime dataTime;

    @Data
    public static class Device {
        private String id;

        private String chName;

        private String enName;

        private String brand;

        private String channelId;

        private String status;

        private int page;

        private int size;
    }
}
