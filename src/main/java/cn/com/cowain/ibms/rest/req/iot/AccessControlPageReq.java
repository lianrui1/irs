package cn.com.cowain.ibms.rest.req.iot;

import cn.com.cowain.ibms.enumeration.iot.AccessControlSpotType;
import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/18 18:17
 */
@Data
@ApiModel("门禁列表请求对象")
public class AccessControlPageReq extends PageReq {

    @ApiModelProperty(value = "门禁类型")
    private AccessControlSpotType type;

    @ApiModelProperty(value = "门禁点名称")
    private String name;
}
