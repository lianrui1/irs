package cn.com.cowain.ibms.service.bean.aliyun.sms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 访客邀请短信邀请参数
 *
 * @author Yang.Lee
 * @date 2021/6/30 10:53
 */
@Slf4j
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GuestInvitationReq {

    /**
     * 邀请人姓名
     **/
    private String name;
    /**
     * 邀请人电话
     **/
    private String tel;

    /**
     * 邀请参数
     **/
    private String vid;
}
