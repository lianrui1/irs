package cn.com.cowain.ibms.enumeration;

/**
 * 预定明细状态
 * some Ibms Config
 *
 * @author Yuxin Zhang
 * @version 1.0
 * @since 2020-08-08
 */

public enum ReservationRecordItemStatus {

    /**
     * 未确认
     */
    UNDETERMINED,
    /**
     * 已确认
     */
    CONFIRM,
    /**
     * 已取消
     */
    CANCEL,
    /**
     * 已完成, 预留
     */
    FINISH

}
