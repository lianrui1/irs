package cn.com.cowain.ibms.config;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/4 15:41
 */
@SpringBootTest
public class RestTemplateConfigTest {

    @Autowired
    private RestTemplate restTemplate;

    @Test
    public void test0() {
        Assertions.assertNotNull(restTemplate);
    }

}
