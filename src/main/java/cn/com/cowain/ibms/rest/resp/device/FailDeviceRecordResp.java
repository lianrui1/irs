package cn.com.cowain.ibms.rest.resp.device;


import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("执行失败设备列表响应对象")
public class FailDeviceRecordResp {

    @ApiModelProperty(value = "设备ID", required = true)
    private String id;

    @ApiModelProperty(value = "设备name", required = true)
    private String deviceName;

    @ApiModelProperty(value = "设备华为云ID", required = true)
    private String hwDeviceId;

    @ApiModelProperty(value = "设备类型", required = true)
    private DeviceType deviceType;

    @ApiModelProperty(value = "设备类型名称", required = true)
    private String deviceTypeName;

    @ApiModelProperty(value = "设备状态", required = true)
    private DeviceStatus status;

    @ApiModelProperty(value = "设备状态名称")
    private String statusDesc;

    @ApiModelProperty(value = "设备位置", required = true)
    private String address;

    @ApiModelProperty(value = "ip", required = true)
    private String ip;

    @ApiModelProperty(value = "是否开启")
    private Boolean isOn;
}
