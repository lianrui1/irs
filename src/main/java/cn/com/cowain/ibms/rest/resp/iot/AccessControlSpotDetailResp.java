package cn.com.cowain.ibms.rest.resp.iot;

import cn.com.cowain.ibms.entity.iot.AccessControlSpot;
import cn.com.cowain.ibms.enumeration.iot.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Optional;

/**
 * @author Yang.Lee
 * @date 2021/11/7 15:41
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("门禁点详情响应")
public class AccessControlSpotDetailResp {

    @ApiModelProperty(value = "门禁点ID", required = true, position = 1)
    private String id;

    @ApiModelProperty(value = "门禁点名称", required = true, position = 2)
    private String name;

    @ApiModelProperty(value = "门禁点类型", required = true, position = 3)
    private AccessControlSpotType type;

    @ApiModelProperty(value = "门禁点类型名称", required = true, position = 4)
    private String typeName;

    @ApiModelProperty(value = "门禁点地址", required = true, position = 5)
    private String address;

    @ApiModelProperty(value = "设备详情", position = 6)
    private IotDeviceResp device;

    @ApiModelProperty(value = "门禁点等级", required = true, position = 7)
    private AccessControlSpotLevel level;

    @ApiModelProperty(value = "门禁点等级名称", required = true, position = 8)
    private String levelName;

    @ApiModelProperty(value = "门禁点所属空间ID", required = true, position = 9)
    private String spaceId;

    @ApiModelProperty(value = "进出类型", required = true, position = 10)
    private AccessControlInOut inOut;

    @ApiModelProperty(value = "门禁点位置", required = true, position = 11)
    private AccessControlSpotPlace accessControlSpotPlace;

    @ApiModelProperty(value = "权限类型", required = true, position = 11)
    private AuthType authType;

    /**
     * 对象转换
     *
     * @param source 待转对象
     * @return 转换后对象
     * @author Yang.Lee
     * @date 2021/11/23 17:01
     **/
    public static AccessControlSpotDetailResp convert(AccessControlSpot source) {

        return AccessControlSpotDetailResp.builder()
                .id(source.getId())
                .name(source.getName())
                .address(Optional.ofNullable(source.getAddress()).orElse(""))
                .type(source.getAccessControlSpotType())
                .typeName(Optional.ofNullable(source.getAccessControlSpotType()).map(AccessControlSpotType::getName).orElse(""))
                .level(source.getAccessControlSpotLevel())
                .levelName(source.getAccessControlSpotLevel().getName())
                .device(IotDeviceResp.convert(source.getDevice()))
                .spaceId(source.getSpace().getId())
                .inOut(source.getInOut())
                .accessControlSpotPlace(source.getAccessControlSpotPlace())
                .authType(source.getAuthType())
                .build();
    }
}
