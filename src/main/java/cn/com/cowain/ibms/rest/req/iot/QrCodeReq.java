package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 二维码数据请求对象
 * @author Yang.Lee
 * @date 2020/12/3 9:30
 */
@Data
@ApiModel("二维码模块")
public class QrCodeReq {

    @NotNull
    @ApiModelProperty(value = "门牌ID", required = true)
    private String doorPlateId;

    @NotNull
    @ApiModelProperty(value = "二维码中的uuid", required = true, position = 1)
    private String uuid;

    @ApiModelProperty(value = "员工工号",  position = 3)
    private String empNo;

    @ApiModelProperty(value = "访客手机号码",  position = 4)
    private String guestMobile;

    @ApiModelProperty(value = "门磁编码",  position = 5)
    private String dm;
}
