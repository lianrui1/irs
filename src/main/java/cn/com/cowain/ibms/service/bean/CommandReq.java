package cn.com.cowain.ibms.service.bean;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/6/27 10:21
 */
@Data
public class CommandReq {

    /**
     * 场景模式
     */
    private String mode;


    /**
    * 设备ID
    */
private String device_id;
    /**
     * 设备的服务ID
     */
    private String service_id;
    /**
     * 设备命令名称
     */
    private String command_name;
    /**
     * 设备执行的命令
     */
    private String paras;
}

