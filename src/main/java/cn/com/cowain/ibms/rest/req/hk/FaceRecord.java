package cn.com.cowain.ibms.rest.req.hk;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class FaceRecord {

    @JsonProperty(value="start_date")
    @ApiModelProperty(value = "开始时间", required = true)
    private String startDate;

    @JsonProperty(value="end_date")
    @ApiModelProperty(value = "结束时间", required = true)
    private String endDate;

    @ApiModelProperty(value = "manual：手动 或 automatic：自动", required = true)
    private String type;
}
