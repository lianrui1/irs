package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.IntelligentCircuitBreaker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

/**
 * @author wei.cheng
 * @date 2022/03/29 18:43
 */
public interface IntelligentCircuitBreakerDao extends JpaRepository<IntelligentCircuitBreaker, String>, JpaSpecificationExecutor<IntelligentCircuitBreaker> {
    /**
     * 查询deviceId = {@code deviceId}的IntelligentCircuitBreaker
     *
     * @param deviceId
     * @return
     */
    Optional<IntelligentCircuitBreaker> findByDeviceId(String deviceId);
}
