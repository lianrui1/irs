package cn.com.cowain.ibms.service.device;

import cn.com.cowain.ibms.entity.iot.HwProductProperties;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.rest.req.iot.centralized.CommandTemplate;
import cn.com.cowain.ibms.rest.req.iot.centralized.PropertiesTemplate;
import cn.com.cowain.ibms.rest.resp.device.TbDataResp;
import cn.com.cowain.ibms.rest.resp.distinguish.SwitchResp;
import cn.com.cowain.ibms.rest.resp.iot.CircuitBreakerResp;
import cn.com.cowain.ibms.service.bean.DoorMagneticHWParasReq;
import cn.com.cowain.ibms.service.bean.DoorMagneticHWReq;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.bean.hwCloud.DeviceDetail;
import cn.com.cowain.sfp.comm.JsonResult;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface HwProductsService {

    // 获取断路器用电数据
    CircuitBreakerResp getIntelligentCircuitBreakerData(IotDevice device, String startTime, String endTime);

    // 从华为云获取产品列表
    void findHwProducts();

    // 从华为云获取设备列表
    void findHwDevices();

    // 根据产品ID获取产品属性
    DoorMagneticHWReq findProduct(String hwProductId);

    // 下发设备控制命令
    ServiceResult command(DoorMagneticHWReq hwReq, String hrId, String name);

    // 根据天数获取遥测数据
    List<TbDataResp> findData(String id, int days);

    // 获取空调设备属性
    ServiceResult findAirProperties(String id, String serviceId);

    // 根据华为云设备id获取TB设备id
    String findTbId(String id);

    // 查询最近上传的数据
    List<TbDataResp> findNowData(String tbId);

    // 控制四路开关
    ServiceResult updateSwitch(DoorMagneticHWReq hwReq, String hrId, String name);

    JsonResult<HwProductProperties> findProductPropertiesByHwProductId(String hwProductId, DeviceType deviceType);

    JsonResult<CommandTemplate> packageCommandTemplate(String hwProductId, DeviceType deviceType);

    JsonResult<PropertiesTemplate> packagePropertiesTemplate(String hwProductId, DeviceType deviceType, String action);

    // 获取设备属性
    SwitchResp findProperties(String id, String serviceId);

    @Transactional
    ServiceResult findByDeviceAndServiceId(IotDevice device, String serviceId);

    // 存储电路器用电量
    void saveCircuitBreaker(IotDevice device, SwitchResp data);

    // 查询设备属性2
    SwitchResp findPropertiesTwo(String deviceId, String service_id);

    // 根据毫米波传感器控制设备
    void sensorBodySensing(List<IotDevice> deviceList);

    // 会议室通过设备属性控制设备 只有会议参会人可控制
    ServiceResult properties(String id, String hrId, DoorMagneticHWParasReq paras);

    // 会议室通过命令控制设备 只有会议参会人可控制
    ServiceResult commandRecord(String id, String hrId, DoorMagneticHWParasReq paras);

    // 设置工作模式
    ServiceResult workingMode(String id);

    // 记录设备操作日志
    void saveDeviceControlLog(DoorMagneticHWReq hwReq, String hrId, String name);

    /**
     * 获取设备详情
     *
     * @param hwDeviceId 华为云设备ID
     * @return 设备详情,获取不到设备或获取失败时，返回Optional.empty();
     * @author Yang.Lee
     * @date 2022/7/13 16:48
     **/
    Optional<DeviceDetail> getDeviceDetail(String hwDeviceId);

    // 获取设备所属产品的服务ID和命令名称
    ServiceResult findProductProperties(IotDevice device);
}
