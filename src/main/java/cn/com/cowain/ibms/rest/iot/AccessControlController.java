package cn.com.cowain.ibms.rest.iot;

import cn.com.cowain.ibms.annotation.Idempotent;
import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.PageReq;
import cn.com.cowain.ibms.rest.req.iot.*;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.iot.*;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.iot.AccessControlSpotService;
import cn.com.cowain.ibms.service.iot.AccessControlTimePlanService;
import cn.com.cowain.ibms.utils.IConst;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.OutputStream;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/18 15:07
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/accessControl")
@Api(tags = IConst.MODULE_ACCESS_CONTROL_SPOT)
public class AccessControlController {

    @Resource
    private AccessControlSpotService accessControlSpotService;
    @Resource
    private AccessControlTimePlanService accessControlTimePlanService;

    @GetMapping("/page/spot/record")
    @ApiOperation(value = "门禁点人员通行记录列表", tags = {IConst.MODULE_ACCESS_CONTROL_SPOT, IConst.V20})
    public ResponseEntity<JsonResult<PageBean<SpotRecordPageResp>>> spotRecordPage(SpotRecordPageReq req) {

        return ResponseEntity.ok(JsonResult.ok("OK", accessControlSpotService.spotRecordPage(req)));
    }

    @GetMapping("/download/spot/record")
    @ApiOperation(value = "门禁点人员通行记录导出", tags = {IConst.MODULE_ACCESS_CONTROL_SPOT, IConst.V20})
    public void spotRecordDownload(HttpServletResponse response, SpotRecordPageReq req) {

        // 获取门禁点通行记录
        PageBean<SpotRecordPageResp> pageBean = accessControlSpotService.spotRecordPage(req);

        byte[] bytes = accessControlSpotService.spotRecordDownload(pageBean);

        response.addHeader("content-type", "application/octet-stream;charset=UTF-8");
        response.addHeader("Content-Disposition", "attachment;filename=accessControlSpotRecordRecord.xlsx");
        response.addHeader("Content-Length", String.valueOf(bytes.length));
        OutputStream stream = null;
        try {
            stream = response.getOutputStream();
            stream.write(bytes);
            stream.flush();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }


    @GetMapping("/page/user")
    @ApiOperation(value = "通行权限人员列表", tags = {IConst.MODULE_ACCESS_CONTROL_SPOT, IConst.V20})
    public ResponseEntity<JsonResult<PageBean<AccessControlPageResp>>> userPage(AccessControlUserPageReq req) {

        PageBean<AccessControlPageResp> pageBean = accessControlSpotService.userPage(req);
        return ResponseEntity.ok(JsonResult.ok("OK", pageBean));
    }

    @GetMapping("/page/user/spot")
    @ApiOperation(value = "人员门禁点权限列表", tags = {IConst.MODULE_ACCESS_CONTROL_SPOT, IConst.V20})
    public ResponseEntity<JsonResult<PageBean<AccessControlSpotPageResp>>> userSpotPage(@ApiParam(name = "empNo", value = "人员工号", required = true) String empNo, PageReq req) {

        return ResponseEntity.ok(JsonResult.ok("OK", accessControlSpotService.userSpotPage(empNo, req)));
    }

    @GetMapping("/page/spot/user")
    @ApiOperation(value = "门禁点人员列表", tags = {IConst.MODULE_ACCESS_CONTROL_SPOT, IConst.V20})
    public ResponseEntity<JsonResult<PageBean<AccessControlPageResp>>> spotUserPage(SpotUserPageReq req) {

        return ResponseEntity.ok(JsonResult.ok("OK", accessControlSpotService.spotUserPage(req)));
    }

    @Idempotent(value = "/accessControl/post", expireTime = 2L)
    @PostMapping
    @ApiOperation(value = "批量分配", tags = {IConst.MODULE_ACCESS_CONTROL_SPOT, IConst.V20})
    public ResponseEntity<JsonResult<IdResp>> createRules(@RequestBody @Validated AccessControlRulesCreateReq req, BindingResult bindingResult) {

        // 根据部门ID获取人员
        if (null != req.getDepartmentIds() && !req.getDepartmentIds().isEmpty()) {
            ServiceResult userResult = accessControlSpotService.getUserByDept(req.getDepartmentIds(), req.getUsers());
            if (!userResult.isSuccess()) {
                return ResponseEntity.ok(JsonResult.error(String.valueOf(userResult.getObject()), null, null));
            }
        }

        ServiceResult result = accessControlSpotService.createRules(req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @Idempotent(value = "/accessControl/post/hk", expireTime = 2L)
    @PostMapping("/hk")
    @ApiOperation(value = "hk门禁点权限添加", tags = {IConst.MODULE_ACCESS_CONTROL_SPOT, IConst.V20})
    public ResponseEntity<JsonResult<IdResp>> createHK(@RequestBody @Validated AccessControlHkCreateReq req, BindingResult bindingResult) {

        // 根据部门ID获取人员
        if (!req.getDepartmentIds().isEmpty()) {
            ServiceResult userResult = accessControlSpotService.getUserByDept(req.getDepartmentIds(), req.getUsers());
            if (!userResult.isSuccess()) {
                return ResponseEntity.ok(JsonResult.error(String.valueOf(userResult.getObject()), null, null));
            }
        }

        // 向HK设备添加人员
        ServiceResult result = accessControlSpotService.createHK(req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @Idempotent(value = "/accessControl/post/ks", expireTime = 2L)
    @PostMapping("/ks")
    @ApiOperation(value = "ks门禁点权限添加", tags = {IConst.MODULE_ACCESS_CONTROL_SPOT, IConst.V20})
    public ResponseEntity<JsonResult<IdResp>> createKS(@RequestBody @Validated AccessControlKsCreateReq req, BindingResult bindingResult) {

        ServiceResult result = accessControlSpotService.createKS(req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @PutMapping
    @ApiOperation(value = "修改人员通行权限", tags = {IConst.MODULE_ACCESS_CONTROL_SPOT, IConst.V20})
    public ResponseEntity<JsonResult<String>> updateUserTime(@RequestBody @Validated AccessControlUpdateTimeReq req, BindingResult bindingResult) {

        ServiceResult result = accessControlSpotService.updateUserTime(req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除人员门禁点通行权限", tags = {IConst.MODULE_ACCESS_CONTROL_SPOT, IConst.V20})
    public ResponseEntity<JsonResult<String>> delete(@PathVariable @ApiParam(name = "id", value = "员工门禁点ID") String id, @ApiParam(name = "empNo", value = "员工工号") String empNo) {

        ServiceResult result = accessControlSpotService.deleteUserTime(id, empNo);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @DeleteMapping("/failUser/{id}")
    @ApiOperation(value = "删除异常人员门禁点通行权限", tags = {IConst.MODULE_ACCESS_CONTROL_SPOT, IConst.V29})
    public ResponseEntity<JsonResult<String>> failUserDelete(@PathVariable @ApiParam(name = "id", value = "员工门禁点ID") String id, @ApiParam(name = "empNo", value = "员工工号") String empNo) {

        ServiceResult result = accessControlSpotService.deleteFailUserTime(id, empNo);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @PostMapping("/failUser/add")
    @ApiOperation(value = "失败人员同步", tags = {IConst.MODULE_ACCESS_CONTROL_SPOT, IConst.V29})
    public ResponseEntity<JsonResult<String>> failUserAdd(@RequestBody @Validated AccessControlFailUserReq req, BindingResult bindingResult) {

        ServiceResult result = accessControlSpotService.failUserAdd(req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "绑定设备", tags = {IConst.ACCESS_CONTROL, IConst.V29})
    public ResponseEntity<JsonResult<String>> updateDevice(@PathVariable @ApiParam(name = "id", value = "门禁点ID") String id, @ApiParam(name = "deviceId", value = "设备ID", required = true) String deviceId) {

        ServiceResult result = accessControlSpotService.updateDevice(id, deviceId);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @Idempotent(value = "/accessControl/delete/device", expireTime = 2L)
    @DeleteMapping("/delete/device/{id}")
    @ApiOperation(value = "解绑设备", tags = {IConst.ACCESS_CONTROL, IConst.V29})
    public ResponseEntity<JsonResult<String>> deleteDevice(@PathVariable @ApiParam(name = "id", value = "门禁点ID") String id, @ApiParam(name = "coerce", value = "是否强制删除 1是") String coerce) {

        ServiceResult result = accessControlSpotService.deleteDevice(id, coerce);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, result.getErrorCode()));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @GetMapping("/fail")
    @ApiOperation(value = "查询指定门禁同步人员列表", tags = {IConst.ACCESS_CONTROL, IConst.V29})
    public ResponseEntity<JsonResult<AccessControlUserFailResp>> synchronization(AccessControlUserPageaReq req) {

        return ResponseEntity.ok(JsonResult.ok("OK", accessControlSpotService.synchronization(req)));
    }

    @GetMapping("/progress")
    @ApiOperation(value = "查询指定门禁同步人员进度", tags = {IConst.ACCESS_CONTROL, IConst.V29})
    public ResponseEntity<JsonResult<String>> progress(@ApiParam(name = "taskId", value = "任务ID") String taskId) {

        ServiceResult result = accessControlSpotService.progress(taskId);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, result.getErrorCode()));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }


    @PostMapping("/bindTimePlan")
    @ApiOperation(value = "用户门禁点权限绑定时间计划", tags = {IConst.ACCESS_CONTROL, IConst.V34})
    public ResponseEntity<JsonResult<IdResp>> bindTimePlan(@RequestBody @Validated AccessControlBindTimePlanReq req, BindingResult bindingResult) {
        return accessControlTimePlanService.bindTimePlan(req);
    }

    @DeleteMapping("/deleteTimePlan")
    @ApiOperation(value = "删除用户门禁点权限绑定的时间计划", tags = {IConst.ACCESS_CONTROL, IConst.V34})
    public ResponseEntity<JsonResult<IdResp>> deleteTimePlan(@RequestParam @ApiParam(name =  "spotId", value = "门禁点ID") String spotId,
                                                             @RequestParam @ApiParam(name = "userHrId", value = "工号") String userHrId) {
        return accessControlTimePlanService.deleteTimePlan(spotId, userHrId);
    }
}
