package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.DoorPlate;
import cn.com.cowain.ibms.entity.iot.DoorPlateConn;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DoorPlateConnDao extends JpaRepository<DoorPlateConn, String> {
    // 根据门牌获取最近通讯时间
    Page<DoorPlateConn> findByDoorPlate(DoorPlate doorPlate, Pageable pageableDoor);
}
