package cn.com.cowain.ibms.entity.distinguish;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.enumeration.Week;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalTime;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/6 15:40
 */
@Data
@Entity
@Table(name = "ibms_access_time")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_access_time", comment = "通行时间表")
@Where(clause = "is_del=0")
public class AccessTime extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "time_plan_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 时间计划ID'")
    private TimePlan planId;

    /**
     * 从时间
     */
    @Column(name = "t_from", nullable = false, columnDefinition = "time COMMENT '从时间'")
    private LocalTime from;

    /**
     * 到时间
     */
    @Column(name = "t_to", nullable = false, columnDefinition = "time COMMENT '到时间'")
    private LocalTime to;

    /**
     * 星期
     */
    @Column(name = "week", nullable = false, columnDefinition = "varchar(36) COMMENT '星期'")
    private Week days;
}
