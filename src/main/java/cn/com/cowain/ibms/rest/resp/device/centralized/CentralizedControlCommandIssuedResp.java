package cn.com.cowain.ibms.rest.resp.device.centralized;

import cn.com.cowain.ibms.enumeration.centralized.TraceStatus;
import cn.com.cowain.ibms.rest.resp.device.FailDeviceRecordResp;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
public class CentralizedControlCommandIssuedResp {
    @ApiModelProperty(value = "事务ID（唯一标识）", required = true)
    @NotBlank
    private String traceId;

    @ApiModelProperty(value = "集控码", required = true)
    @NotBlank
    private String centralizedCode;

    @ApiModelProperty(value = "剩余数量：关闭时倒序，开启时正序", required = true)
    @NotBlank
    private Integer surplusCount;

    @ApiModelProperty(value = "控制设备总数", required = true)
    @NotBlank
    private Integer total;

    @ApiModelProperty(value = "最新执行的设备名称", required = true)
    @NotBlank
    private String deviceName;

    @ApiModelProperty(value = "任务状态", required = true)
    @NotEmpty
    private TraceStatus traceStatus;

    @ApiModelProperty(value = "指令状态 on off", required = true)
    private String controlStatus;

    @ApiModelProperty(value = "管理员工号")
    private String adminHrId = "";

    @ApiModelProperty(value = "管理员姓名")
    private String adminName = "";

    @ApiModelProperty(value = "失败设备列表", required = true)
    private List<FailDeviceRecordResp> list;
}
