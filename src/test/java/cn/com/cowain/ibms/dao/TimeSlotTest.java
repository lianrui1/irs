package cn.com.cowain.ibms.dao;

import cn.com.cowain.ibms.entity.TimeSlot;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@SpringBootTest
public class TimeSlotTest {


    @Test
    public void test33() {
        Integer seq = 0;
        String TimeFrom = "08:00";
        String TimeTO = "10:30";
        String TimeFromWithout = TimeFrom.replace(":", "");
        String TimeTOWithout = TimeTO.replace(":", "");
        Integer TimeFromNum = Integer.valueOf(TimeFromWithout);
        Integer TimeTONum = Integer.valueOf(TimeTOWithout);
        Integer TimeDifference = TimeTONum - TimeFromNum;
        if (TimeDifference >= 30) {

            String[] TimeFromSplit = TimeFrom.split(":");
            Integer TimeFromHigh = Integer.valueOf(TimeFromSplit[0]);
            Integer TimeFromLow = Integer.valueOf(TimeFromSplit[1]);
            String[] TimeTOSplit = TimeTO.split(":");
            Integer TimeTOHigh = Integer.valueOf(TimeTOSplit[0]);
            Integer TimeTOLow = Integer.valueOf(TimeTOSplit[1]);
            Integer LowNum = 0;
            if ((TimeTOLow - TimeFromLow) > 0) {
                LowNum = 1;
            } else if ((TimeTOLow - TimeFromLow) < 0) {
                LowNum = -1;
            }
            Integer Number = 2 * (TimeTOHigh - TimeFromHigh) + LowNum;
            List<TimeSlot> list = new ArrayList<>();
            String StringTimeFromHigh;
            String StringTimeFromLow;
            for (Integer i = 0; i < Number; i++) {
                TimeSlot timeSlot = new TimeSlot();
                StringTimeFromHigh = String.valueOf(TimeFromHigh);
                for (Integer NumberLength = StringTimeFromHigh.length(); NumberLength < 2; NumberLength++) {
                    StringTimeFromHigh = "0" + StringTimeFromHigh;
                }
                StringTimeFromLow = String.valueOf(TimeFromLow);
                for (Integer NumberLength = StringTimeFromLow.length(); NumberLength < 2; NumberLength++) {
                    StringTimeFromLow = "0" + StringTimeFromLow;
                }
                timeSlot.setFrom(StringTimeFromHigh + ":" + StringTimeFromLow);
                TimeFromLow = TimeFromLow + 30;
                if (TimeFromLow == 60) {
                    TimeFromLow = 0;
                    TimeFromHigh = TimeFromHigh + 1;
                }

                StringTimeFromHigh = String.valueOf(TimeFromHigh);
                for (Integer NumberLength = StringTimeFromHigh.length(); NumberLength < 2; NumberLength++) {
                    StringTimeFromHigh = "0" + StringTimeFromHigh;
                }
                StringTimeFromLow = String.valueOf(TimeFromLow);
                for (Integer NumberLength = StringTimeFromLow.length(); NumberLength < 2; NumberLength++) {
                    StringTimeFromLow = "0" + StringTimeFromLow;
                }
                timeSlot.setTo(StringTimeFromHigh + ":" + StringTimeFromLow);
                timeSlot.setSeq(seq);
                seq++;
                list.add(timeSlot);
            }
            System.out.println(String.valueOf(list));
        }
    }

}
