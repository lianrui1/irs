package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.DeviceAccessRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author tql
 * @Description 设备通行记录表 DAO
 * @Date 21-12-3 下午1:33
 * @Version 1.0
 */
@Repository
public interface DeviceAccessRecordDao extends JpaRepository<DeviceAccessRecord, String>, JpaSpecificationExecutor<DeviceAccessRecord> {
    /**
     * 查询iotDeviceId = {@code iotDeviceId}的DeviceAccessRecord列表
     *
     * @param iotDeviceId
     * @return
     */
    List<DeviceAccessRecord> findAllByIotDeviceId(String iotDeviceId);
}
