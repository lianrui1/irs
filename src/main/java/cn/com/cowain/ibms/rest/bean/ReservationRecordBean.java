package cn.com.cowain.ibms.rest.bean;

import cn.com.cowain.ibms.enumeration.ReservationRecordStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 分页组件 类
 *
 * @author Zhang YuXin
 * @since 2020-08-05
 */
@Data
@ApiModel("获取会议列表")
public class ReservationRecordBean implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 会议主题
     */
    @ApiModelProperty(value = "会议主题", required = true, position = 0)
    private String topic;

    /**
     * 开始时间
     */
    @ApiModelProperty(value = "开始时间", required = true, position = 1)
    private LocalDateTime timeFrom;

    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间", required = true, position = 2)
    private LocalDateTime timeTo;

    /**
     * 会议室名称
     */
    @ApiModelProperty(value = "会议室名称", required = true, position = 3)
    private String roomName;

    /**
     * 会议室ID
     */
    @JsonIgnore
    private String roomId;

    /**
     * 楼栋
     */
    @ApiModelProperty(value = "楼栋", required = true, position = 4)
    private String roomBuilding;

    /**
     * 楼层
     */
    @ApiModelProperty(value = "楼层", required = true, position = 5)
    private String floor;

    /**
     * 发起人
     */
    @ApiModelProperty(value = "发起人id", required = true, position = 6)
    private String initiator;


    /**
     * 发起人
     */
    @ApiModelProperty(value = "发起人姓名", required = true, position = 7)
    private String initiatorName;

    /**
     * 会议室状态
     */
    @ApiModelProperty(value = "会议室状态 DEFAULT:未确认 PENDING:进行中 FINISH:已结束  CANCEL:已取消", required = true, position = 8)
    private ReservationRecordStatus reservationRecordStatus;

    /**
     * 是否本人发起 0否 1是
     */
    @ApiModelProperty(value = "是否本人发起 0否 1是", required = true, position = 9)
    private Integer isOwn = 0;

    /**
     * 会议预约id
     */
    @ApiModelProperty(value = "会议预约id", required = true, position = 10)
    private String id;

    /**
     * 特殊要求
     */
    @ApiModelProperty(value = "特殊要求", position = 11)
    private String service;

    /**
     * 会议备注
     */
    @ApiModelProperty(value = "会议备注", required = true, position = 12)
    private String remark;
    /**
     * 会议室状态
     */
    @ApiModelProperty(value = "人员接受会议状态", required = true, position = 13)
    private String userStatus;

    /**
     * 创建人工号
     **/
    @ApiModelProperty(value = "创建人工号", required = true, position = 14)
    private String initiatorEmpNo;

    /**
     * 位置
     */
    @ApiModelProperty(value = "空间地址", required = true, position = 15)
    private String location;
}
