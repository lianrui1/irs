package cn.com.cowain.ibms.service.weather.impl;

import cn.com.cowain.ibms.rest.resp.weather.BaseWeatherResp;
import cn.com.cowain.ibms.service.amap.AMapService;
import cn.com.cowain.ibms.service.bean.amap.enumeration.CoordinateSystem;
import cn.com.cowain.ibms.service.bean.amap.geo.ReGeoCode;
import cn.com.cowain.ibms.service.bean.amap.poi.LonLat;
import cn.com.cowain.ibms.service.bean.amap.weather.ForceCastsTempResult;
import cn.com.cowain.ibms.service.bean.amap.weather.ForcecastsResult;
import cn.com.cowain.ibms.service.bean.amap.weather.Lives;
import cn.com.cowain.ibms.service.weather.WeatherService;
import cn.com.cowain.ibms.utils.RedisUtil;
import cn.com.cowain.ibms.utils.amap.WeatherTools;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/4/19 14:30
 */
@Slf4j
@Service
public class AMapWeatherServiceImpl implements WeatherService {

    @Resource
    private AMapService aMapService;

    @Resource
    private RedisUtil redisUtil;

    private static final String LIVEREDISKEY= "liveWeather";
    private static final String FORECASTREDISKEY= "forecastWeather";
    /**
     * 获取实时天气情况。天气数据缓存1H。
     *
     * @param cityCode 城市编码
     * @return 天气情况
     * @author Yang.Lee
     * @date 2021/4/19 14:29
     **/
    @Override
    public BaseWeatherResp getWeatherByCityCode(String cityCode) {

        // 查询redis中是否已存在天气信息
        String redisKey = redisUtil.createRealKey(LIVEREDISKEY + cityCode);
        // redis中已存在则返回redis中数据
       if (redisUtil.hasKey(redisKey)) {

            return JSON.parseObject(String.valueOf(redisUtil.get(redisKey)), BaseWeatherResp.class);
        }

        // 向高德API发送请求
        Lives lives = aMapService.getLiveWeather(cityCode);
        BaseWeatherResp resp = convert(lives);

        resp.setWeatherPicture(WeatherTools.getWeatherPicture(resp.getWeather()));

        // 天气数据缓存1小时
        redisUtil.set(redisKey, resp, 60 * 60L);
        return resp;
    }

    /**
     * 获取实时天气情况。天气数据缓存1H。
     *
     * @param cityCode 城市编码
     * @return 天气情况
     * @author Yang.Lee
     * @date 2021/4/19 14:29
     **/
    @Override
    public ForceCastsTempResult getForecastsWeatherByCityCode(String cityCode) {

        // 查询redis中是否已存在天气信息
        String redisKey = redisUtil.createRealKey(FORECASTREDISKEY + cityCode);
        // redis中已存在则返回redis中数据
        if (redisUtil.hasKey(redisKey)) {
            return JSON.parseObject(String.valueOf(redisUtil.get(redisKey)), ForceCastsTempResult.class);
        }

        // 向高德API发送请求
        ForceCastsTempResult tempResult = aMapService.getWeatherForecasts(cityCode);
        // 设置天气对应的图片
        this.putWeatherPicture(tempResult);
        // 天气数据缓存1小时
        redisUtil.set(redisKey, tempResult, 60 * 60L);
        return tempResult;
    }
    private void putWeatherPicture(ForceCastsTempResult resp){
        if(resp!=null&&!CollectionUtils.isEmpty(resp.getCasts())){
                for(ForcecastsResult temp:resp.getCasts()){
                    temp.setWeatherPicture(WeatherTools.getWeatherPicture(temp.getDayweather()));
                }
        }
    }
    /**
     * 根据经纬度获取天气状况
     *
     * @param lon 精度，gps坐标
     * @param lat 维度，gps坐标
     * @return 天气状况
     * @author Yang.Lee
     * @date 2021/4/21 13:19
     **/
    @Override
    public BaseWeatherResp getWeatherByLonLat(double lon, double lat) {

        log.debug("获取天气信息，坐标 lon {}, lat {}", lon, lat);
        String lon5Str = String.valueOf((int)(lon * 10000));
        String lat5Str = String.valueOf((int)(lat * 10000));
        log.debug("获取天气信息，转换后坐标 lon {}, lat {}", lon5Str, lat5Str);
        String redisKey = redisUtil.createRealKey("weather-"+lon5Str + "," + lat5Str);

        if(redisUtil.hasKey(redisKey)){
            log.debug("缓存中有天气信息，直接从缓存中获取");
            return JSON.parseObject(String.valueOf(redisUtil.get(redisKey)), BaseWeatherResp.class);
        }

        LonLat gpsLonLat = new LonLat(lon, lat);
        // 将gps经纬度转成高德坐标
        List<LonLat> aMapLonLatList =  aMapService.coordinateConvert(CoordinateSystem.GPS, gpsLonLat);
        LonLat aMapLonLat = aMapLonLatList.get(0);
        // 解析经纬度获取城市编码
        List<ReGeoCode> reGeoCodeList = aMapService.reGeo(null, aMapLonLat);
        ReGeoCode reGeoCode = reGeoCodeList.get(0);
         // 城市编码
        String cityCode = reGeoCode.getAddressComponent().getAdCode();
        // 根据城市编码获取天气信息

        BaseWeatherResp resp = getWeatherByCityCode(cityCode);

        redisUtil.set(redisKey, resp, RedisUtil.HALF_AN_HOUR_SECOND * 2);

        return resp;
    }
    /**
     * 根据经纬度获取天气预报
     *
     * @param lon 精度，gps坐标
     * @param lat 维度，gps坐标
     * @return 天气状况
     * @author Yang.Lee
     * @date 2021/4/21 13:19
     **/
    @Override
    public ForceCastsTempResult getForecastsWeatherByLonLat(double lon, double lat) {

        String lon5Str = String.valueOf((int)(lon * 10000));
        String lat5Str = String.valueOf((int)(lat * 10000));

        String redisKey = redisUtil.createRealKey("weatherForecast-"+lon5Str + "," + lat5Str);

        if(redisUtil.hasKey(redisKey)){

            return JSON.parseObject(String.valueOf(redisUtil.get(redisKey)), ForceCastsTempResult.class);
        }

        LonLat gpsLonLat = new LonLat(lon, lat);
        // 将gps经纬度转成高德坐标
        List<LonLat> aMapLonLatList =  aMapService.coordinateConvert(CoordinateSystem.GPS, gpsLonLat);
        LonLat aMapLonLat = aMapLonLatList.get(0);
        // 解析经纬度获取城市编码
        List<ReGeoCode> reGeoCodeList = aMapService.reGeo(null, aMapLonLat);
        ReGeoCode reGeoCode = reGeoCodeList.get(0);
        // 城市编码
        String cityCode = reGeoCode.getAddressComponent().getAdCode();
        // 根据城市编码获取天气信息

        ForceCastsTempResult resp = getForecastsWeatherByCityCode(cityCode);
        redisUtil.set(redisKey, resp, RedisUtil.HALF_AN_HOUR_SECOND * 2);
        return resp;
    }

    /**
     * 对象转换
     *
     * @param lives 待转对象
     * @return 转换结果
     * @author Yang.Lee
     * @date 2021/4/19 16:46
     **/
    private BaseWeatherResp convert(Lives lives) {

        BaseWeatherResp result = new BaseWeatherResp();
        if (lives == null) {
            return result;
        }

        result.setWeather(lives.getWeather());
        result.setCity(lives.getCity());
        result.setDataTime(lives.getReportTime());
        result.setTemperature(lives.getTemperature());
        result.setWindDirection(lives.getWindDirection());
        // todo 天气图标转换

        return result;
    }
}
