package cn.com.cowain.ibms.rest.req.meeting;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2021/6/25 9:21
 */
@Data
@ApiModel("短信邀请请求对象")
public class MobileInvitationReq {

    @ApiModelProperty(value = "受邀者姓名", required = true, example = "张三", position = 1)
    private String name;

    @ApiModelProperty(value = "受邀者手机", required = true, example = "13809809090", position = 2)
    private String cellPhoneNumber;
}
