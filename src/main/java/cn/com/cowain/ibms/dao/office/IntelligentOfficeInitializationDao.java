package cn.com.cowain.ibms.dao.office;

import cn.com.cowain.ibms.bo.SimpleIntelligentOfficeInitialization;
import cn.com.cowain.ibms.entity.office.IntelligentOfficeInitialization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Space对象数据持久层
 *
 * @author Yang.Lee
 * @date 2020/12/9 18:19
 */
@Repository
public interface IntelligentOfficeInitializationDao extends JpaRepository<IntelligentOfficeInitialization, String>, JpaSpecificationExecutor<IntelligentOfficeInitialization> {
    List<IntelligentOfficeInitialization> findByUserHrId(String userHrId);

    Optional<IntelligentOfficeInitialization> findByUserHrIdAndIntelligentOfficeId(String userHrId, String intelligentOfficeId);

    // 获取办公室下初始化记录
    List<IntelligentOfficeInitialization> findByIntelligentOfficeId(String id);



    /**
     * 简化查询
     *
     * @param userHrId
     * @param intelligentOfficeId
     * @return
     */
    @Query(
            value = "select new cn.com.cowain.ibms.bo.SimpleIntelligentOfficeInitialization(i.intelligentOffice.id, i.userHrId, i.userName, i.alisa) from IntelligentOfficeInitialization i where i.userHrId = ?1 and i.intelligentOffice.id = ?2 and i.isDelete = 0"
    )
    SimpleIntelligentOfficeInitialization findSimpleIntelligentOfficeInitializationByUserHrIdAndIntelligentOfficeId(String userHrId, String intelligentOfficeId);
}

