package cn.com.cowain.ibms.bo;

import cn.com.cowain.ibms.utils.DateUtils;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author wei.cheng
 * @date 2022/03/23 15:17
 */
@Slf4j
@Data
@Builder
public class AuthTime {
    /**
     * 识别成功的时间
     */
    private LocalDateTime startTime;

    /**
     * 识别成功的时间
     */
    private LocalDateTime endTime;

    public static List<AuthTime> redisValueConvert(Object value) {
        if (Objects.isNull(value)) {
            return new ArrayList<>();
        }
        String valueString = value.toString();
        if (StringUtils.isEmpty(valueString)) {
            return new ArrayList<>();
        }
        List<AuthTime> result = new ArrayList<>();
        String[] contentList = valueString.split(",");
        for (String contents : contentList) {
            String[] content = contents.split("/");
            if (content.length != 2) {
                log.warn("invalid value in redis of authTime, value:{}", contents);
            } else {
                try {
                    result.add(AuthTime.builder()
                            .startTime(DateUtils.parseLocalDateTime(content[0], DateUtils.PATTERN_DATETIME))
                            .endTime(DateUtils.parseLocalDateTime(content[1], DateUtils.PATTERN_DATETIME))
                            .build());
                } catch (Exception e) {
                    log.warn("invalid value in redis of authTime, value:{}", contents);
                }
            }
        }
        return result;
    }

    public static String toRedisValue(List<AuthTime> authTimeList, LocalDateTime minEndTime) {
        if (CollectionUtils.isEmpty(authTimeList)) {
            return null;
        }
        authTimeList.removeIf(a -> a.getEndTime().isBefore(minEndTime));
        if (CollectionUtils.isEmpty(authTimeList)) {
            return null;
        }
        authTimeList.sort((a1, a2) -> ObjectUtils.compare(a1.getStartTime(), a2.getStartTime()));
        List<String> contentList = authTimeList.stream().map(a -> DateUtils.formatLocalDateTime(a.getStartTime(), DateUtils.PATTERN_DATETIME) + "/" +
                DateUtils.formatLocalDateTime(a.getEndTime(), DateUtils.PATTERN_DATETIME)).collect(Collectors.toList());
        contentList = contentList.stream().distinct().collect(Collectors.toList());
        String value = StringUtils.join(contentList, ",");
        log.info("authTime to redisValue is {}", value);
        return value;
    }
}
