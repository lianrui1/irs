package cn.com.cowain.ibms.rest.req.space;

import cn.com.cowain.ibms.entity.iot.Properties;
import cn.com.cowain.ibms.enumeration.iot.SpaceType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 空间树请求对象类
 * @author Yang.Lee
 * @date 2020/12/17 11:05
 */
@Data
@ApiModel("空间树信息")
public class SpaceTreeReq implements Serializable {


    @ApiModelProperty(value = "主键ID")
    private String id;

    @Length(max = 20)
    @NotNull
//    @Pattern(regexp = "^[a-z0-9A-Z\u4e00-\u9fa5]{1,20}$", message = "空间名称校验失败，只允许输入中文英文数字，且长度不超过20")
    @ApiModelProperty(value = "空间名称", required = true,  position = 1)
    private String name;

    @NotNull
    @ApiModelProperty(value = "所属项目ID", required = true, position = 3)
    private String projectId;

    @ApiModelProperty(value = "父节点ID", required = true, position = 4)
    private String parentId;

    @Length(max=30)
    @ApiModelProperty(value = "备注", position = 5)
    private String remark;

    @NotNull
    @ApiModelProperty(value = "空间类型 DEFAULT(默认)|ROOM(房间)|BUILDING(建筑)|GROUND(场地)|FLOOR(楼层)|BUILDINGS(楼宇)|GREEN_SPACE(绿化)|OTHERS(其他)", required = true)
    private SpaceType spaceType;

    @ApiModelProperty(value = "空间属性", position = 7)
    private Properties properties;

    @ApiModelProperty(value = "管理人员工号")
    private List<String> empNos;

    @Length(max = 200)
    @ApiModelProperty(value = "简介")
    private String introduction;

    @ApiModelProperty(value = "图片")
    private String img;

    @ApiModelProperty(value = "空间楼层")
    private String floor;
}
