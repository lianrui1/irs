package cn.com.cowain.ibms.service.impl;

import cn.com.cowain.ibms.dao.ReservationRecordDao;
import cn.com.cowain.ibms.dao.ReservationRecordItemDao;
import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.entity.ReservationRecordItem;
import cn.com.cowain.ibms.enumeration.ReservationRecordStatus;
import cn.com.cowain.ibms.service.MeetingPollService;
import cn.com.cowain.ibms.service.WeChatService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Set;

/**
 * Description
 *
 * @author 张宇鑫
 * @version 1.0
 * @since 2020 8/14/20
 */
@Service
public class MeetingPollServiceImpl implements MeetingPollService {


    private ReservationRecordDao reservationRecordDao;
    private ReservationRecordItemDao reservationRecordItemDao;
    private WeChatService weChatService;

    public MeetingPollServiceImpl(ReservationRecordDao reservationRecordDao,
                                  ReservationRecordItemDao reservationRecordItemDao,
                                  WeChatService weChatService) {
        this.reservationRecordDao = reservationRecordDao;
        this.reservationRecordItemDao = reservationRecordItemDao;
        this.weChatService = weChatService;
    }

    /**
     * @param timeMinute 获取时间配置
     * @return Set<ReservationRecord>
     * @author 张宇鑫
     */
    @Override
    public Set<ReservationRecord> getUpcomingMeeting(Integer timeMinute) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime timeStart = LocalDateTime.now().plusMinutes(timeMinute);
        String s = timeStart.toString();
        String substring = s.substring(0, 10);
        String substrings = s.substring(11, 19);
        String ss = substring +" "+substrings;
        LocalDateTime ldt = LocalDateTime.parse(ss,df);
        ReservationRecordStatus aDefault = ReservationRecordStatus.DEFAULT;
        Set<ReservationRecord> reservationRecords = reservationRecordDao.findByFromAndStatus(ldt,aDefault);
        reservationRecords.forEach(reservationRecord -> {
            Set<ReservationRecordItem> reservationRecordItemSet = reservationRecordItemDao.findByReservationRecordId(reservationRecord.getId());
            String topic = reservationRecord.getTopic();
            reservationRecordItemSet.forEach(reservationRecordItem ->
                    weChatService.requirementsComingReminder(reservationRecordItem, timeMinute,topic)
            );
        });
        return reservationRecords;
    }

    @Override
    public Set<ReservationRecord> getUpcomingMeetingEnd(Integer timeMinute) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime timeStart = LocalDateTime.now().plusMinutes(timeMinute);
        String s = timeStart.toString();
        String substring = s.substring(0, 10);
        String substrings = s.substring(11, 19);
        String ss = substring +" "+substrings;
        LocalDateTime ldt = LocalDateTime.parse(ss,df);
        ReservationRecordStatus aDefault = ReservationRecordStatus.DEFAULT;
        Set<ReservationRecord> reservationRecords = reservationRecordDao.findByToAndStatus(ldt,aDefault);
        reservationRecords.forEach(reservationRecord -> {
            Set<ReservationRecordItem> reservationRecordItemSet = reservationRecordItemDao.findByReservationRecordId(reservationRecord.getId());
            String topic = reservationRecord.getTopic();
            reservationRecordItemSet.forEach(reservationRecordItem ->
                    weChatService.requirementsComingReminderEnd(reservationRecordItem, timeMinute,topic)
            );
        });
        return reservationRecords;
    }

}
