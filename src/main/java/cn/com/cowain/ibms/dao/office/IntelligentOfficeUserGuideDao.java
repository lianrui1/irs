package cn.com.cowain.ibms.dao.office;

import cn.com.cowain.ibms.entity.office.IntelligentOfficeUserGuide;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Space对象数据持久层
 *
 * @author Yang.Lee
 * @date 2020/12/9 18:19
 */
@Repository
public interface IntelligentOfficeUserGuideDao extends JpaRepository<IntelligentOfficeUserGuide, String>, JpaSpecificationExecutor<IntelligentOfficeUserGuide> {

    /**
     * 根据用户工号查找是否执行过引导操作
     *
     * @param userHrId 用户工号
     * @return 数据对象
     * @author Yang.Lee
     * @date 2021/12/21 13:31
     **/
    Optional<IntelligentOfficeUserGuide> findByUserHrId(String userHrId);
}

