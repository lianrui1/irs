package cn.com.cowain.ibms.dao.iotc;

import cn.com.cowain.ibms.entity.iotc.DeviceApplicationRelation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/10/20 16:18
 */
public interface DeviceApplicationRelationDao extends JpaRepository<DeviceApplicationRelation, String>, JpaSpecificationExecutor<DeviceApplicationRelation> {

    /**
     * 根据应用查询关系列表
     *
     * @param id 应用ID
     * @return 结果
     * @author Yang.Lee
     * @date 2021/10/20 16:20
     **/
    List<DeviceApplicationRelation> findByApplicationId(String id);

    /**
     * 根据设备查询关系列表
     *
     * @param id 设备ID
     * @return 结果
     * @author Yang.Lee
     * @date 2021/10/20 16:20
     **/
    List<DeviceApplicationRelation> findByDeviceId(String id);
}
