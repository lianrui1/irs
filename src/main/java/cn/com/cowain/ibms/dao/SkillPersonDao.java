package cn.com.cowain.ibms.dao;

import cn.com.cowain.ibms.entity.SkillPerson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface SkillPersonDao extends JpaRepository<SkillPerson, String>, JpaSpecificationExecutor<SkillPerson> {
}
