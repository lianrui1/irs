package cn.com.cowain.ibms.service.iot;

import cn.com.cowain.ibms.entity.iot.DeviceAccessRecord;
import cn.com.cowain.ibms.service.bean.ServiceResult;

/**
 * 门禁点通行记录Service
 *
 * @author Yang.Lee
 * @date 2021/12/8 11:22
 */
public interface AccessControlSpotAccessRecordService {

    /**
     * 创建门禁点通行记录
     *
     * @param deviceAccessRecord 设备通行记录
     * @return 创建结果
     * @author Yang.Lee
     * @date 2021/12/8 11:24
     **/
    ServiceResult create(DeviceAccessRecord deviceAccessRecord);
}
