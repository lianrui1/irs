package cn.com.cowain.ibms.enumeration.meeting;

/**
 * 访客邀请状态
 * @author Yang.Lee
 * @date 2021/6/24 18:48
 */
public enum GuestInvitationStatus {

    /**
     * 待反馈
     **/
    DEFAULT,

    /**
     * 已接受
     **/
    ACCEPTED,

    /**
     * 已拒绝
     **/
    REJECTED,

    /**
     * 已取消
     **/
    CANCEL,

    /**
     * 已删除
     **/
    DELETE
}
