package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 权限表，用于支持ibms通行2.0支持兼容南通工厂权限申请表
 *
 * @author wei.cheng
 * @date 2022/06/29 18:12
 */
@Data
@Entity
@Table(name = "iot_auth")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "iot_auth", comment = "权限表")
@Where(clause = "is_del=0")
public class Auth extends BaseEntity {

    @Column(name = "name", columnDefinition = "varchar(36) COMMENT '权限名'")
    private String name;
    /**
     * 关联门禁点ID列表(逗号分割)
     */
    @Column(name = "spot_ids", columnDefinition = "text COMMENT '关联门禁点ID列表'")
    private String spotIds;
}
