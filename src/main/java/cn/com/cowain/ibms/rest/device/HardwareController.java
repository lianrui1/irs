package cn.com.cowain.ibms.rest.device;

import cn.com.cowain.ibms.annotation.Idempotent;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.IdAndSeqBean;
import cn.com.cowain.ibms.rest.req.Sorts;
import cn.com.cowain.ibms.rest.req.device.HardwarePageReq;
import cn.com.cowain.ibms.rest.req.device.HardwareReq;
import cn.com.cowain.ibms.rest.resp.device.HardwareDetailResp;
import cn.com.cowain.ibms.rest.resp.device.HardwarePageResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.device.HardwareService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.ExportExcel;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * 硬件设备Controller
 *
 * @author Yang.Lee
 * @date 2021/3/17 15:48
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/hardware")
@Api(tags = IConst.HARDWARE)
public class HardwareController {

    @Autowired
    private HardwareService hardwareService;

    /**
     * 新建硬件
     *
     * @param req           请求参数
     * @param bindingResult 参数校验对象
     * @return 请求结果
     * @author Yang.Lee
     * @date 2021/3/17 16:15
     **/
    @Idempotent(API_BASE + "/hardware/post")
    @PostMapping
    @ApiOperation(value = "创建新硬件信息", tags = IConst.HARDWARE)
    public ResponseEntity<JsonResult<IdAndSeqBean>> create(@RequestBody @Validated @ApiParam("硬件信息") HardwareReq req, BindingResult bindingResult) {

        log.debug(req.toString());
        ServiceResult result = hardwareService.save(req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }


    /**
     * 查询硬件清单列表（分页）
     *
     * @param req 请求参数
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/3/17 18:16
     **/
    @GetMapping("/page")
    @ApiOperation(value = "查询硬件列表（分页）", tags = IConst.HARDWARE)
    public ResponseEntity<JsonResult<PageBean<HardwarePageResp>>> getPage(HardwarePageReq req) {

        log.debug(req.toString());
        // 默认根据创建时间倒叙查询数据
        req.addOrderDesc(Sorts.CREATED_TIME);

        PageBean<HardwarePageResp> pageBean = hardwareService.getPage(req);

        return ResponseEntity.ok(JsonResult.ok("ok" ,pageBean));
    }

    /**
     * 更新硬件信息
     *
     * @param id            硬件ID
     * @param req           请求参数
     * @param bindingResult 参数校验对象
     * @return 更新结果
     * @author Yang.Lee
     * @date 2021/3/17 18:18
     **/
    @PutMapping("/{id}")
    @ApiOperation(value = "更新硬件信息", tags = IConst.HARDWARE)
    public ResponseEntity<JsonResult<String>> update(@PathVariable @ApiParam("硬件ID") String id, @RequestBody @Validated @ApiParam("硬件信息") HardwareReq req, BindingResult bindingResult) {

        log.debug(req.toString());

        ServiceResult result = hardwareService.update(id ,req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.okNoData("ok"));
    }

    /**
     * 删除硬件
     *
     * @param id 硬件ID
     * @return 删除结果
     * @author Yang.Lee
     * @date 2021/3/17 18:20
     **/
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除硬件信息", tags = IConst.HARDWARE)
    public ResponseEntity<JsonResult<String>> delete(@PathVariable @ApiParam("硬件ID") String id) {
        log.info("删除硬件: " + id);

        ServiceResult result = hardwareService.delete(id);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()) ,null ,ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.okNoData("ok"));
    }

    /**
     * 查询硬件详情
     *
     * @param id 硬件ID
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/3/17 18:21
     **/
    @GetMapping("/detail/{id}")
    @ApiOperation(value = "查询硬件详情", tags = IConst.HARDWARE)
    public ResponseEntity<JsonResult<HardwareDetailResp>> getDetail(@PathVariable String id) {
        log.debug("查询硬件信息: " + id);

        ServiceResult result = hardwareService.get(id);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()) ,null , ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.ok("ok" ,(HardwareDetailResp)result.getObject()));
    }

    /**
     * 请求下载excel文件
     *
     * @author Yang.Lee
     * @date 2021/3/17 18:24
     **/
    @GetMapping("/download/excel")
    @ApiOperation(value = "下载excel文件", tags = IConst.HARDWARE)
    public void downloadExcel(HardwarePageReq req) {

        // 默认根据创建时间倒叙查询数据

        req.addOrderDesc(Sorts.CREATED_TIME);
        PageBean<HardwarePageResp> pageBean = hardwareService.getPage(req);

        log.info("导出参数： " + pageBean.getList());
        String[] fileds = {"硬件SN","设备名称CH","设备名称EN","所属产品","设备类型","品牌","型号","创建时间"};
        String[] attr = {"hardwareSN","nameZH","nameEN","productName","deviceType","brand","model","createTime"};
        ExportExcel.setFilesAndAttributes(fileds,attr);
        ExportExcel.writeExcel("deviceList", pageBean.getList());
    }
}
