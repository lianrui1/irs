package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.AccessControlSpot;
import cn.com.cowain.ibms.entity.iot.AccessControlSpotUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface AccessControlSpotUserDao extends JpaRepository<AccessControlSpotUser, String>, JpaSpecificationExecutor<AccessControlSpotUser> {


    // 查询人员拥有门禁数
    int countByUserHrId(String empNo);

    Page<AccessControlSpotUser> findByUserHrIdContainsOrUserNameContains(String keyword, String keyword1, Pageable pageable);

    // 获取人员门禁点分页
    Page<AccessControlSpotUser> findByUserHrId(String empNo, Pageable pageable);

    // 根据面板机和工号获取通行人员信息
    List<AccessControlSpotUser> findByAccessControlSpotIdAndUserHrId(String id, String empNo);

    // 判断用户是否存在门禁点的权限
    Boolean existsByAccessControlSpotIdAndUserHrId(String spotId, String userHrId);

    // 根据门禁点ID获取门禁点人员
    List<AccessControlSpotUser> findByAccessControlSpotId(String id);

    /**
     * 根据设备id和用户工号查询门禁点权限
     *
     * @param accessControlSpot 门禁点ID
     * @param userHrId 用户工号
     * @return 门禁点权限
     * @author lianrui
     * @date 2022/09/02 10:02
     **/
    List<AccessControlSpotUser> findAccessControlSpotUserByUserHrIdAndAccessControlSpot(String userHrId, AccessControlSpot accessControlSpot);

    /**
     * 查询用户是否有权限
     *
     * @param userHrId  用户工号
     * @param startTime (YYYY-MM-dd HH:mm:ss) 权限开始时间
     * @param endTime   (YYYY-MM-dd HH:mm:ss) 权限结束时间
     * @param spotId    门禁点ID
     * @return 查询结果, 若数据存在则返回1
     * @author Yang.Lee
     * @date 2022/2/7 17:21
     **/
    @Query(value = "select 1 from iot_access_control_spot_user where user_hr_id = ?1 and access_start_time = ?2 and access_end_time = ?3 and access_control_spot_id = ?4 and is_del = 0 limit 1", nativeQuery = true)
    Integer checkSpotUser(String userHrId, String startTime, String endTime, String spotId);

    /**
     * 查询用户所有权限
     *
     * @param userId 用户ID
     * @return 权限列表
     * @author Yang.Lee
     * @date 2022/2/9 9:43
     **/
    List<AccessControlSpotUser> findByUserHrId(String userId);

    /**
     * 根据用户工号和门禁点集合查询数据
     *
     * @param userId           用户ID
     * @param spotIdCollection 门禁点ID集合
     * @return 查询结果
     * @author Yang.Lee
     * @date 2022/3/1 15:39
     **/
    List<AccessControlSpotUser> findByUserHrIdAndAccessControlSpotIdIn(String userId, Collection<String> spotIdCollection);

    List<AccessControlSpotUser> findByAccessControlSpotIdAndUserHrIdIn(String id, List<String> hrIds);
}
