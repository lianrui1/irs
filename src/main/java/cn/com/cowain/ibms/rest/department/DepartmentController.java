package cn.com.cowain.ibms.rest.department;

import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.ibms.rest.resp.department.DepartmentStaffPageResp;
import cn.com.cowain.ibms.rest.resp.department.DepartmentStaffPageRootResp;
import cn.com.cowain.ibms.rest.resp.department.DepartmentTreeResp;
import cn.com.cowain.ibms.service.department.DepartmentService;
import cn.com.cowain.ibms.utils.IConst;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * 组织部门Controller
 *
 * @author Yang.Lee
 * @date 2021/2/5 11:20
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/department")
@Api(tags = IConst.DEPARTMENT)
public class DepartmentController {

    @Resource
    private DepartmentService departmentService;

    /**
     * 查询所有组织部门树
     *
     * @return 组织部门树
     */
    @GetMapping("/tree")
    @ApiOperation(value = "查询部门树", tags = IConst.DEPARTMENT)
    public ResponseEntity<JsonResult<DepartmentTreeResp>> departmentTree() {

        DepartmentTreeResp departmentTreeResp = departmentService.getDepartmentTree();
        return ResponseEntity.ok(JsonResult.ok("ok", departmentTreeResp));
    }


    /**
     * 获取部门下所有员工数据
     *
     * @param id 部门ID
     * @return 该部门下员工列表
     */
    @GetMapping("/{id}/staff")
    @ApiOperation(value = "查询部门下的所有员工数据", tags = IConst.DEPARTMENT)
    public ResponseEntity<JsonResult<DepartmentStaffPageResp>> getStaffByDepartmentId(
            @PathVariable @ApiParam(value = "部门编号", required = true) long id) {

        DepartmentStaffPageResp departmentStaffPageResp = departmentService.getDepartmentStaffPage(id);
        return ResponseEntity.ok(JsonResult.ok("ok", departmentStaffPageResp));
    }

    /**
     * 模糊查询用户
     *
     * @param name 姓名关键字
     * @return 员工列表
     */
    @GetMapping("/users")
    @ApiOperation(value = "模糊查询用户", tags = {IConst.DEPARTMENT, IConst.V26})
    public ResponseEntity<JsonResult<DepartmentStaffPageRootResp>> searchStaff(@ApiParam(value = "姓名模糊查询关键字", required = true) @RequestParam String name) {

        DepartmentStaffPageRootResp departmentStaffPageRootResp = departmentService.searchStaff(name);
        return ResponseEntity.ok(JsonResult.ok("ok", departmentStaffPageRootResp));
    }
}
