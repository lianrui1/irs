package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author wei.cheng
 * @date 2022/04/14 10:13
 */
@Data
@ApiModel("日期是否存在警戒抓拍记录查询参数")
public class AlertCaptureorGetExistOfDayQueryReq {

    @ApiModelProperty(value = "开始日期", required = true, example = "2022-02-01")
    @NotBlank
    private String startDate;

    @ApiModelProperty(value = "结束日期", required = true, example = "20220-03-28")
    @NotBlank
    private String endDate;
}
