package cn.com.cowain.ibms.service;


import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.feign.bean.pass_v2.PassV2UserResp;
import cn.com.cowain.ibms.rest.resp.distinguish.StaffPageResp;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Description
 *
 * @author 张宇鑫
 * @version 1.0
 * @date 2020/8/11 12:41
 */

public interface SysUserService {

    /**
     * 判断工号是否是访客（劳务）
     *
     * @param hrId 工号
     * @return true: 是访客（劳务）；false:不是
     * @author Yang.Lee
     * @date 2022/3/8 17:03
     **/
    static boolean isVisitor(String hrId) {

        return hrId.startsWith("VN") || hrId.startsWith("TN") || hrId.startsWith("UN");
    }

//    /**
//     * 保存用户信息(根据 stId)
//     * 该接口在对接UC后已弃用
//     *
//     * @param stId
//     * @return
//     * @author 张宇鑫
//     */
//    SysUser trySaveIfNeed(String stId);

//    /**
//     * 将用户信息 存入 redis mysql
//     *
//     * @param sysUser
//     * @return
//     * @author 张宇鑫
//     */
//    void tryToCreateRedis(SysUser sysUser);

//    /**
//     * 根据sysId 获取用户信息。
//     * 对接UC后该接口已启用，使用getUserByEmpNo(String empNo)方法代替
//     *
//     * @param sysId
//     * @return
//     * @author 张宇鑫
//     */
//    SysUser getSysUserInfo(String sysId);

//    /**
//     * 根据userId 获取用户信息
//     *
//     * @param userId
//     * @return 用户信息
//     * @author cem
//     */
//    SysUser trySaveIfNeedWithUserId(String userId);

    /**
     * 通过工号查询用户信息，并存入数据库中
     *
     * @param empNo: 员工工号
     * @return 员工信息
     * @author Yang.Lee
     * @date 2021/3/3 10:52
     **/
    SysUser trySaveIfNeed(String empNo);

    /**
     * 根据员工编号获取员工信息
     *
     * @param empNo: 员工编号
     * @return 员工数据
     * @author Yang.Lee
     * @date 2021/3/3 11:20
     **/
    SysUser getUserByEmpNo(String empNo);

    /**
     * 根据token获取用户信息
     *
     * @param token: 登录返回的token令牌
     * @return 员工信息
     * @author Yang.Lee
     * @date 2021/3/3 11:19
     **/
    SysUser getUserByToken(String token);

    /**
     * 获取所有用户信息（包含所有员工及访客）
     *
     * @return 员工列表
     * @author Yang.Lee
     * @date 2021/12/14 10:57
     **/
    List<StaffPageResp> syncStaffList();

    /**
     * 判断是否是劳务
     *
     * @param hrId
     * @author wei.cheng
     * @date 2022/3/25 4:42 下午
     **/
    Boolean isLabour(String hrId);

    /**
     * 获取所有访客信息
     *
     * @author wei.cheng
     * @date 2022/4/2 12:44 下午
     **/
    List<StaffPageResp> syncGuestList();

    /**
     * 根据Uid查询用户信息
     *
     * @param uid   ucId
     * @return 用户数据，查询不到时返回Optional.empty()
     * @author Yang.Lee
     * @date 2022/7/14 14:58
     **/
    Optional<PassV2UserResp> getUserByUid(String uid);

    /**
     * 批量获取员工名称
     *
     * @param empNos
     * @return
     */
    Map<String,String> batchGetNameByEmpNos(List<String> empNos);
}
