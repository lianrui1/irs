package cn.com.cowain.ibms.service.bean.amap;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2021/4/19 15:20
 */
@Data
public class AMapResp {

    private String status;

    private String info;

    @JSONField(name = "infocode")
    private String infoCode;

    /**
     * 判断响应是否成功
     * @return true: 成功; fase失败
     * @author Yang.Lee
     * @date 2021/4/19 15:44
     **/
    public boolean success(){

        return "1".equals(status);
    }

    /**
     * 获取响应信息
     * @return 响应信息
     * @author Yang.Lee
     * @date 2021/4/19 15:46
     **/
    public String getInfoMessage(){

        return "{\"status\" : \""+this.status+"\", \"info\" : \""+this.info+"\", \"infoCode\" : \""+this.infoCode+"\"}";
    }

}
