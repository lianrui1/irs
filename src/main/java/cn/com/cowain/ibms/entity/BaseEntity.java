package cn.com.cowain.ibms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Base Entity 声明各实体类的公共属性
 *
 * @author 胡荆陵
 * @since 2020-7-30
 */
@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class BaseEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 主键长度
     */
    private static final int ID_LENGTH = 36;

    /**
     * 主键ID
     */
    @Id
    @Column(name = "id", length = ID_LENGTH, nullable = false, columnDefinition = "varchar(36) COMMENT '主键'")
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     * 创建时间
     */
    @CreatedDate
    @Column(name = "created_time", updatable = false, columnDefinition = "datetime COMMENT '创建时间'")
    private LocalDateTime createdTime;

    /**
     * 创建人
     */
    @Column(name = "created_by", updatable = false, columnDefinition = "varchar(36) COMMENT '创建人'")
    private String createdBy;

    @LastModifiedDate
    @Column(name = "updated_time", columnDefinition = "datetime COMMENT '更新时间'")
    private LocalDateTime updatedTime;

    @Column(name = "updated_by", columnDefinition = "varchar(36) COMMENT '更新人'")
    private String updatedBy;

    /**
     * 版本号
     */
    @Version
    @JsonIgnore
    @Column(name = "version", columnDefinition = "integer  COMMENT '版本号'")
    private int version;

    /**
     * 是否已删除 0:否 1:是
     */
    @TableLogic
    @Column(name = "is_del", nullable = false, columnDefinition = "tinyint(1)  COMMENT '是否已删除,0:否;1:是'")
    @TableField("is_del")
    @JsonIgnore
    private Integer isDelete = 0;

}
