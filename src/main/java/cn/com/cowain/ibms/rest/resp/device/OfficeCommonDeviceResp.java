package cn.com.cowain.ibms.rest.resp.device;

import cn.com.cowain.ibms.rest.req.office.DeviceNameEditReq;
import cn.com.cowain.ibms.rest.resp.office.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author feng
 * @title: OfficeCommonDeviceResp
 * @projectName ibms
 * @Date 2021/12/21 17:39
 */
@Data
public class OfficeCommonDeviceResp {

    /**
     *  通用状态
     */
    @ApiModelProperty(value = "id", required = true)
    private String id;
    @ApiModelProperty(value = "华为设备id", required = true)
    private  String hwDeviceId;
    @ApiModelProperty(value = "华为设备状态", required = true)
    private String hwStatus;
    @ApiModelProperty(value = "设备类型", required = true)
    private String deviceType;
    /**窗帘*/
    @ApiModelProperty(value = "窗帘", required = false)
    private String action;
    /**空调*/
    @ApiModelProperty(value = "设置空调的打开与关闭", required = false)
    private String setPower;
    @ApiModelProperty(value = "设置温度", required = false)
    private String temperature;
    @ApiModelProperty(value = "风向", required = false)
    private String windDirection;
    @ApiModelProperty(value = "设置风速 小 中 大 自动", required = false)
    private String setFanAuto;
    @ApiModelProperty(value = "风速", required = false)
    private String  windSpeed;
    @ApiModelProperty(value = "设置制冷 制热 除湿 送风 自动 模式", required = false)
    private String mode;
    @ApiModelProperty(value = "空调名称", required = false)
    private String deviceName;
    @ApiModelProperty(value = " 设置扫风模式 ，无扫风，上下扫风，左右扫风，上下左右扫风", required = false)
    private String setSwing;


    /**多路开关*/
    @ApiModelProperty(value = "on,off", required = false)
    private String light1;
    @ApiModelProperty(value = "on,off", required = false)
    private String light2;
    @ApiModelProperty(value = "on,off", required = false)
    private String light3;
    @ApiModelProperty(value = "on,off", required = false)
    private String light4;
    @ApiModelProperty(value = "电量", required = false)
    private Double electricity;
    @JsonProperty("switch")
    @ApiModelProperty(value = "电量", required = false)
    private String isSwitch;
    @ApiModelProperty(value = "湿度", required = false)
    private String humidity;
    @ApiModelProperty(value = "光照，单位流明", required = false)
    private String luminance;
    @ApiModelProperty(value = "噪音", required = false)
    private String noise;
    @ApiModelProperty(value = "pm10", required = false)
    private String pm10;
    @ApiModelProperty(value = "pm2.5", required = false)
    private String pm2_5;
    private String read = "read";

    @ApiModelProperty(value = "设备附加名称", required = true)
    private DeviceNameEditReq.AdditionalName additionalName;

    // 人体传感器
    @ApiModelProperty(value = "人存在 true,否则false", required = false)
    private String isPeopleThere;

    private String value;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime ts;
    public OfficeCommonDeviceResp(AirConditionerStatusDetailResp air,String deviceType){
        this.deviceType=deviceType;
          this.setPower=air.getSetPower();
        this.temperature=air.getTemperature()+"";
        this.windDirection=air.getWindDirection();
        this.setFanAuto=air.getSetFanAuto();
        this.windSpeed=air.getWindSpeed();
        this.mode=air.getMode();
        this.deviceName=air.getDeviceName();
        this.setSwing=air.getSetSwing();
        this.id=air.getId();
        this.hwDeviceId=air.getHwDeviceId();
        this.hwStatus=air.getHwStatus();

    }
    public OfficeCommonDeviceResp(TbDataDetailResp tb,String deviceType){
        this.deviceType=deviceType;
        this.luminance=tb.getLuminance();
        // 温度
        this.temperature=tb.getTemperature();
        // 湿度
        this.humidity=tb.getHumidity();
        // 噪音
        this.noise=tb.getNoise();
        // 空气质量10
        this.pm10=tb.getPm10();
        // 空气质量pm2_5
        this.pm2_5=tb.getPm2_5();
        // 人体传感器
        this.isPeopleThere=tb.getIsPeopleThere();
        this.id=tb.getId();
        this.hwDeviceId=tb.getHwDeviceId();
        this.hwStatus=tb.getHwStatus();
        this.deviceName=tb.getDeviceName();
    }
    public OfficeCommonDeviceResp(CurtainDetailResp curtain,String deviceType){
        this.deviceType=deviceType;
        this.id=curtain.getId();
        this.hwDeviceId=curtain.getHwDeviceId();
        this.hwStatus=curtain.getHwStatus();
        this.action=curtain.getAction();
        this.deviceName=curtain.getDeviceName();
    }
    public OfficeCommonDeviceResp(SwitchDetailResp switchDetailResp,String deviceType){
        this.deviceType=deviceType;
        this.light1=switchDetailResp.getLight1();
        this.light2=switchDetailResp.getLight2();
        this. light3=switchDetailResp.getLight3();
        this. light4=switchDetailResp.getLight4();
        this.electricity=switchDetailResp.getElectricity();
        this.isSwitch=switchDetailResp.getIsSwitch();
        this.humidity=switchDetailResp.getHumidity();
        this.luminance=switchDetailResp.getLuminance();
        this.noise=switchDetailResp.getNoise();
        this.pm10=switchDetailResp.getPm10();
        this.pm2_5=switchDetailResp.getPm2_5();
        // this. temperature;
        this.read =switchDetailResp.getRead();
        this.id=switchDetailResp.getId();
        this.hwDeviceId=switchDetailResp.getHwDeviceId();
        this.hwStatus=switchDetailResp.getHwStatus();
        this.deviceName=switchDetailResp.getDeviceName();
        this.additionalName = switchDetailResp.getAdditionalName();
    }

    public OfficeCommonDeviceResp(DoorlResp doorlResp,String deviceType){
        this.deviceType=deviceType;
        this.id=doorlResp.getId();
        this.hwDeviceId=doorlResp.getHwDeviceId();
        this.hwStatus=doorlResp.getHwStatus();
        this.deviceName=doorlResp.getDeviceName();
    }

}
