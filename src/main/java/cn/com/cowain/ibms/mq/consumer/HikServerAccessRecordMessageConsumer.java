package cn.com.cowain.ibms.mq.consumer;

import cn.com.cowain.ibms.dao.ability.OpenAbilityAccessControlSpotDao;
import cn.com.cowain.ibms.dao.iot.AccessControlSpotAccessRecordDao;
import cn.com.cowain.ibms.dao.iot.AccessControlSpotDao;
import cn.com.cowain.ibms.dao.iot.DeviceAccessRecordDao;
import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.ability.OpenAbility;
import cn.com.cowain.ibms.entity.ability.OpenAbilityAccessControlSpot;
import cn.com.cowain.ibms.entity.iot.AccessControlSpot;
import cn.com.cowain.ibms.entity.iot.AccessControlSpotAccessRecord;
import cn.com.cowain.ibms.entity.iot.DeviceAccessRecord;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.enumeration.ability.Ability;
import cn.com.cowain.ibms.enumeration.iot.DeviceAccessRecordType;
import cn.com.cowain.ibms.enumeration.iot.PersonType;
import cn.com.cowain.ibms.feign.bean.oa.guest.VisitorPassReturn;
import cn.com.cowain.ibms.feign.oa.OAApi;
import cn.com.cowain.ibms.mq.Topic;
import cn.com.cowain.ibms.mq.bean.AccessRecordMessage;
import cn.com.cowain.ibms.mq.producer.AccessRecordProducer;
import cn.com.cowain.ibms.rest.resp.distinguish.VisitorAccessRecordResp;
import cn.com.cowain.ibms.rest.resp.distinguish.WorkTimeCheckResp;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.utils.DateUtils;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @Author tql
 * @Description 海康服务通行记录消费者
 * @Date 21-12-8 下午2:16
 * @Version 1.0
 */
@Slf4j
@RocketMQMessageListener(
        topic = "${rocketmq.env}-" + Topic.HIK_SERVER_ACCESS_RECORD,
        consumerGroup = "device-${rocketmq.group}-" + Topic.HIK_SERVER_ACCESS_RECORD,
        selectorExpression = Topic.SEND_MSG
)
public class HikServerAccessRecordMessageConsumer implements RocketMQListener<String> {

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserServiceUC;

    @Resource
    private DeviceDao deviceDao;

    @Resource
    private AccessControlSpotDao accessControlSpotDao;

    @Resource
    private DeviceAccessRecordDao deviceAccessRecordDao;

    @Resource
    private AccessControlSpotAccessRecordDao accessControlSpotAccessRecordDao;

    @Resource
    private AccessRecordProducer accessRecordProducer;

    @Resource
    private OAApi oAapi;

    @Resource
    private OpenAbilityAccessControlSpotDao openAbilityAccessControlSpotDao;

    @Override
    public void onMessage(String s) {
        try {
            log.info("消费通行记录消息:{}", s);
            AccessRecordMessage accessRecordMessage = JSON.parseObject(s, AccessRecordMessage.class);
            // resourceIndexCode不等于空
            if (!"".equals(accessRecordMessage.getResourceIndexCode())) {
                Optional<IotDevice> bySn = deviceDao.findBySn(accessRecordMessage.getResourceIndexCode());
                if (bySn.isPresent()) {
                    IotDevice iotDevice = bySn.get();
                    // 门禁点通行
                    DeviceAccessRecord deviceAccessRecord = new DeviceAccessRecord();
                    AccessControlSpotAccessRecord accessRecord = new AccessControlSpotAccessRecord();
                    // 工号
                    deviceAccessRecord.setEmpNo(StringUtils.isBlank(accessRecordMessage.getJobNo()) ? "" : accessRecordMessage.getJobNo());
                    // 人名
                    deviceAccessRecord.setNameZh(StringUtils.isBlank(accessRecordMessage.getName()) ? "陌生人" : accessRecordMessage.getName());

                    if (SysUserService.isVisitor(deviceAccessRecord.getEmpNo())) {
                        // 访客
                        deviceAccessRecord.setStaffType(PersonType.VISITOR);
                        deviceAccessRecord.setDepartment("--");
                        // 访客工号设置
                        deviceAccessRecord.setEmpNo(Optional.ofNullable(accessRecordMessage.getJobNo()).orElse("--"));
                    } else {
                        // 人员类型 1 工号开头不是VN 2  工号开头为VN
                        // 设置部门/工号
                        // 根据工号获取人员信息
                        // uc人员数据
                        if (StringUtils.isBlank(accessRecordMessage.getJobNo())) {
                            deviceAccessRecord.setDepartment("无部门");
                            //工号为空 陌生人
                            deviceAccessRecord.setStaffType(PersonType.UNKNOWN);

                        } else  {
                            SysUser userByEmpNo = sysUserServiceUC.getUserByEmpNo(accessRecordMessage.getJobNo());
                            // 部门设置(UC数据异常处理)
                            deviceAccessRecord.setDepartment(Optional.ofNullable(userByEmpNo).map(SysUser::getFullDepartmentName).orElse("无部门"));
                            //员工
                            deviceAccessRecord.setStaffType(PersonType.EMPLOYEE);
                        }
                    }

                    deviceAccessRecord.setAccessStatus(StringUtils.isBlank(accessRecordMessage.getAccessStatus()) ? "通行" : accessRecordMessage.getAccessStatus());
                    // 通行图片
                    deviceAccessRecord.setCapturePicture(accessRecordMessage.getCapturePicture());
                    // 底库图片
                    deviceAccessRecord.setDefaultPicture(accessRecordMessage.getDefaultPicture());
                    // 通行时间
                    deviceAccessRecord.setDataTime(accessRecordMessage.getHappentime());
                    // 设备id
                    deviceAccessRecord.setIotDevice(iotDevice);
                    // 设备来源
                    deviceAccessRecord.setRecordType(DeviceAccessRecordType.HIK);
                    // 设备通行记录
                    deviceAccessRecordDao.save(deviceAccessRecord);

                    WorkTimeCheckResp workTimeCheckResp = WorkTimeCheckResp.convert(deviceAccessRecord);

                    //访客resp
                    VisitorAccessRecordResp visitorAccessRecordResp = VisitorAccessRecordResp.convert(deviceAccessRecord);

                    Set<Ability> abilitySet = new HashSet<>();
                    // 获取门禁点
                    Optional<AccessControlSpot> byDeviceId = accessControlSpotDao.findByDeviceId(iotDevice.getId());
                    AccessControlSpot accessControlSpot = null;
                    if (byDeviceId.isPresent()) {

                        accessControlSpot = byDeviceId.get();
                        workTimeCheckResp.setCcDeviceCode(accessControlSpot.getId());

                        //访客
                        visitorAccessRecordResp.setCcDeviceCode(accessControlSpot.getId());

                        accessRecord.setAccessControlSpot(accessControlSpot);
                        accessRecord.setDeviceAccessRecord(deviceAccessRecord);
                        // 门禁点记录
                        accessControlSpotAccessRecordDao.save(accessRecord);

                        if (accessControlSpot.getInOut() != null) {
                            switch (byDeviceId.get().getInOut()) {
                                case IN:
                                    //考勤打卡类型 1代表出 2代表入
                                    workTimeCheckResp.setType(2);
                                    //访客进出类型 1代表入 2代表出
                                    visitorAccessRecordResp.setType(1);
                                    break;
                                case OUT:
                                    workTimeCheckResp.setType(1);
                                    visitorAccessRecordResp.setType(2);
                                    break;
                                default:
                            }
                        }

                        List<OpenAbilityAccessControlSpot> abilitySpotList = openAbilityAccessControlSpotDao.findBySpotId(accessControlSpot.getId());

                        // 查询设备所属能力
                        abilitySet = abilitySpotList.stream()
                                .map(OpenAbilityAccessControlSpot::getOpenAbility)
                                .map(OpenAbility::getAbility)
                                .collect(Collectors.toSet());

                    }

                    // 过滤工号不对的数据
                    if (StringUtils.isNotBlank(deviceAccessRecord.getEmpNo()) && !"无组织".equals(deviceAccessRecord.getEmpNo())) {
                        // 将数据推送到考勤mq
                        try {
                            accessRecordProducer.push(workTimeCheckResp);
                        } catch (Exception e) {
                            log.error("考勤数据推送失败");
                            log.error(e.getMessage(), e);
                        }

                        try {
                            if (abilitySet.contains(Ability.MEAL)) {
                                String result = oAapi.mealReturn(accessRecordMessage.getJobNo(),
                                        DateUtils.formatLocalDateTime(
                                                accessRecordMessage.getHappentime(), DateUtils.PATTERN_DATETIME)
                                );

                                log.info("就餐回调EHR结果：{}", result);
                            }
                        } catch (Exception e) {
                            log.error("报餐回调数据下发失败");
                            log.error(e.getMessage(), e);
                        }


                        try {
                            //过滤未通行数据
                            if (abilitySet.contains(Ability.VISITOR) && "通行".equals(deviceAccessRecord.getAccessStatus())) {

                                if (accessControlSpot == null) {
                                    log.error("未找到访客门禁点信息不能下发访客回调到ehr. {}", s);
                                } else {

                                    VisitorPassReturn passReturn = VisitorPassReturn.builder()
                                            .company(accessControlSpot.getAccessControlSpotPlace() != null ? accessControlSpot.getAccessControlSpotPlace().geyEhrId() : 0)
                                            .hcCardNo(accessRecordMessage.getJobNo())
                                            .state(visitorAccessRecordResp.getType())
                                            .result(1)
                                            .build();
                                    log.debug("发送访客通行接口参数 {}", JSON.toJSONString(passReturn));
                                    String result = oAapi.visitorUserGate(passReturn);
                                    log.debug("访客通行接口回调结果 {}", result);
                                }
                            }
                        } catch (Exception e) {
                            log.error("访客回调数据下发失败");
                            log.error(e.getMessage(), e);
                        }
                    }

                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

    }
}
