package cn.com.cowain.ibms.entity.meeting;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * 用户自定义用户组
 *
 * @author Yang.Lee
 * @date 2021/6/8 17:57
 */
@Data
@Entity
@Table(name = "ibms_custom_user_group")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "ibms_custom_user_group", comment = "用户自定义用户组")
public class CustomUserGroup extends BaseEntity {

    @Column(name = "owner_emp_no", nullable = false, columnDefinition = "varchar(20) COMMENT '创建人工号'")
    private String ownerEmpNo;

    @Column(name = "name", nullable = false, columnDefinition = "varchar(10) COMMENT '用户组名称'")
    private String name;
}
