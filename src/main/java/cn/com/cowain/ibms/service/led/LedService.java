package cn.com.cowain.ibms.service.led;

import cn.com.cowain.ibms.rest.req.iot.AttendanceInOutResp;
import cn.com.cowain.ibms.rest.req.iot.AttendanceStatisticsResp;

/**
 * @author Yang.Lee
 * @date 2022/5/4 10:32
 */
public interface LedService {


    /**
     * 获取考勤数据统计
     *
     * @param projectId 项目ID，用于筛选
     * @return 考勤数据统计
     * @author Yang.Lee
     * @date 2022/5/4 10:33
     **/
    AttendanceStatisticsResp getAttendanceStatistic(String projectId);


    /**
     * 查询考勤设备进出的数据（最近12小时数据）
     *
     * @param projectId 项目ID
     * @return 数据对象
     * @author Yang.Lee
     * @date 2022/5/4 14:30
     **/
    AttendanceInOutResp getAttendanceInOut(String projectId);
}
