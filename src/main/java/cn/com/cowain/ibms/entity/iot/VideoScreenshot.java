package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author Yang.Lee
 * @date 2021/11/18 13:51
 */
@Data
@Entity
@Table(name = "iot_video_screenshot")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_video_screenshot", comment = "视频监控点截图表")
public class VideoScreenshot extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "video_monitor_spot_id", nullable = false, columnDefinition = "varchar(36) COMMENT '视频点ID FK'")
    private VideoMonitorSpot videoMonitorSpot;

    @Column(name = "pic", columnDefinition = "varchar(500) COMMENT '图片地址'")
    private String pic;

}
