package cn.com.cowain.ibms.enumeration.iot;

/**
 * 智慧办公室状态
 *
 * @author Yang.Lee
 * @date 2021/11/8 14:48
 */
public enum IntelligentOfficeStatus {

    ENABLE("启用"),

    DISABLE("禁用");

    IntelligentOfficeStatus (String name){
        this.name = name;
    }
    String name;

    public String getName(){
        return this.name;
    }
}
