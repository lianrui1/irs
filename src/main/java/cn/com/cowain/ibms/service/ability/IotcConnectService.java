package cn.com.cowain.ibms.service.ability;

import cn.com.cowain.ibms.rest.req.iot.PermisssionReq;
import cn.com.cowain.ibms.rest.req.iot.UpdatePermisssionReq;
import cn.com.cowain.ibms.service.bean.ServiceResult;

/**
 * @author feng
 * @title: IotcConnectService
 * @projectName ibms
 * @Date 2022/1/12 9:49
 */
public interface IotcConnectService {


    /**
     *  @title feign请求iotc新增
     *  @Description 描述
     *  @author jf.sui
     *  @Date  2022/1/12 16:43
     */
    ServiceResult bindingDevice(PermisssionReq paramReq, String channelType, String chainSite);
    /**
     *  @title feign请求iotc 删除
     *  @Description 描述
     *  @author jf.sui
     *  @Date  2022/1/12 16:43
     */
    String deleteBinding(UpdatePermisssionReq updateReq, String channelType, String chainSite);
}
