package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Data
@Entity
@Table(name = "hw_product_properties")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "hw_product_properties", comment = "iot华为云产品属性")
public class HwProductProperties extends BaseEntity {

    @Column(name = "hw_product_id", nullable = false, columnDefinition = "varchar(50) COMMENT '华为云产品ID'")
    private String hwProductId;

    @Column(name = "service_id", columnDefinition = "varchar(50) COMMENT '华为云产品设备的服务ID。注：产品内不允许重复'")
    private String serviceId;

    @Column(name = "command_name", columnDefinition = "varchar(50) COMMENT '华为云产品设备命令名称。注：设备服务内不允许重复。'")
    private String commandName;

    @Enumerated(EnumType.STRING)
    @Column(name = "device_type", columnDefinition = "varchar(50) COMMENT '设备类型'")
    private DeviceType deviceType;
}
