package cn.com.cowain.ibms.component;

import lombok.Data;

@Data
public class DeviceDataInfo {
    private String hwDeviceId;
    private String serviceId;
}
