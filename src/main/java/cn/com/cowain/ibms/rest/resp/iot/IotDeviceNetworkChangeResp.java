package cn.com.cowain.ibms.rest.resp.iot;

import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 网络变化
 * @author Yang.Lee
 * @date 2021/4/29 9:03
 */
@Data
@Builder
@ApiModel("设备网络变化")
public class IotDeviceNetworkChangeResp {

    @ApiModelProperty(value = "设备id", required = true, position = 1)
    private String deviceId;

    @ApiModelProperty(value = "设备名称", required = true, position = 2)
    private String deviceName;

    @ApiModelProperty(value = "设备SN", required = true, position = 3)
    private String sn;

    @ApiModelProperty(value = "网络状态", required = true, position = 4)
    private DeviceStatus deviceStatus;

    @ApiModelProperty(value = "网络状态名称", required = true, position = 5)
    private String deviceStatusName;

    @ApiModelProperty(value = "数据时间", required = true, position = 6)
    private LocalDateTime dataTime;
}
