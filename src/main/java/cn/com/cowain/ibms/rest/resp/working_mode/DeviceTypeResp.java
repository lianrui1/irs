package cn.com.cowain.ibms.rest.resp.working_mode;

import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author feng
 * @title: DeviceTypeResp
 * @projectName ibms
 * @Date 2021/12/31 15:57
 */
@Data
@ApiModel("类型返回")
public class DeviceTypeResp {
    String typeName;
    String typeCode;
    public DeviceTypeResp(DeviceType type){
        typeName=type.getName();
        typeCode=type.name();
    }
}
