package cn.com.cowain.ibms.rest.advice;

import cn.com.cowain.ibms.exceptions.*;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.ibms.utils.ErrConst;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.NoSuchElementException;

import static org.springframework.http.HttpStatus.*;

/**
 * 业务 异常处理
 *
 * @author Hu Jingling
 */
@Slf4j
@RestControllerAdvice
public class DataExceptionAdvice {

    /**
     * NoSuchElementException
     * <p>
     * see:
     * https://stackoverflow.com/questions/53543627/what-is-the-correct-http-status-code-for-search-results-not-found
     *
     * @param e 异常参数
     * @return json结果
     */
    @ExceptionHandler(NoSuchElementException.class)
    @ResponseStatus(NOT_FOUND)
    public JsonResult<String> noSuchElement(NoSuchElementException e) {
        log.error("NoSuchElementException Found:" + e);
        return JsonResult.error("记录不存在!需要对业务逻辑进行封装,以及异常捕获.", e.getMessage(), ErrConst.E11);
    }

    /**
     * BizLogicNotMatchException
     *
     * @param e
     * @return
     */
    @ExceptionHandler(BizLogicNotMatchException.class)
    @ResponseStatus(OK)
    public JsonResult<String> bizLogicNotMatch(BizLogicNotMatchException e) {
        log.error("BizLogicNotMatchException Found:" + e);
        String msg = "业务逻辑异常";
        if (StringUtils.isNotBlank(e.getMessage())) {
            msg = e.getMessage();
        }
        return JsonResult.error(msg, null, "0");
    }

    /**
     * DataNotFoundException
     *
     * @param e
     * @return
     */
    @ExceptionHandler(DataNotFoundException.class)
    @ResponseStatus(OK)
    public JsonResult<String> bizLogicNotMatch(DataNotFoundException e) {
        log.error("DataNotFoundException Found:" + e);
        String msg = "[数据未找到异常]";
        if (StringUtils.isNotBlank(e.getMessage())) {
            msg = msg + e.getMessage();
        }
        return JsonResult.error(msg, null, "0");
    }

    /**
     * RemoteServiceUnavailableException
     *
     * @param e
     * @return
     */
    @ExceptionHandler(RemoteServiceUnavailableException.class)
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    public JsonResult<String> remoteServiceUnavailable(RemoteServiceUnavailableException e) {
        log.error("RemoteServiceUnavailableException Found:" + e);
        return JsonResult.error("远程服务调用异常", e.getMessage(), ErrConst.E13);
    }

    /**
     * NullCheckException
     *
     * @param e
     * @return
     */
    @ExceptionHandler(NullCheckException.class)
    @ResponseStatus(OK)
    public JsonResult<String> nullCheck(NullCheckException e) {
        log.error("NullCheckException Found:" + e);
        return JsonResult.error("获取数据为空异常", e.getMessage(), ErrConst.E04);
    }

    /**
     * 幂等异常
     *
     * @param e
     * @return
     */
    @ExceptionHandler(IdempotentException.class)
    @ResponseStatus(OK)
    public JsonResult<String> idempotentError(IdempotentException e) {
        log.error("idempotent Error :" + e);
        return JsonResult.error("数据处理中，请稍候", e.getMessage(), ErrConst.E05);
    }
    /**
     * 执行异常
     *
     * @param e
     * @return
     */
    @ExceptionHandler(CommandExecuteException.class)
    @ResponseStatus(OK)
    public JsonResult<String> commandError(CommandExecuteException e) {
        log.error("commandError Error :" + e);
        return JsonResult.error("命令下发失败", e.getMessage(), ErrConst.E25);
    }
    /**
     * 并发异常
     *
     * @param e
     * @return
     */
    @ExceptionHandler(ConcurrentException.class)
    @ResponseStatus(OK)
    public JsonResult<String> concurrentError(ConcurrentException e) {
        log.error("concurrentError Error :" + e);
        return JsonResult.error("设备正在被控制，请稍候...", e.getMessage(), ErrConst.E05);
    }

    /**
     * 不支持的操作
     *
     * @param e
     * @return
     */
    @ExceptionHandler(UnsupportedOperationException.class)
    @ResponseStatus(OK)
    public JsonResult<String> unsupportedOperation(UnsupportedOperationException e) {
        return JsonResult.error("当前操作目前不支持调用", null, ErrConst.E01);
    }

}
