package cn.com.cowain.ibms.job;

import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.meeting.IntelligentMeetingAppService;
import cn.com.cowain.ibms.websocket.bean.MessageResult;
import cn.com.cowain.ibms.websocket.bean.resp.MeetingAppResp;
import cn.com.cowain.ibms.websocket.enumeration.MessageType;
import cn.com.cowain.ibms.websocket.enumeration.WSActionType;
import cn.com.cowain.ibms.websocket.server.WsServer;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author Yang.Lee
 * @date 2022/1/6 18:44
 */
@Slf4j
@Component
public class MeetingAppJob {

    @Resource
    private IntelligentMeetingAppService intelligentMeetingAppService;

    @Scheduled(cron = "5/59 * * * * ?")
    public void run(){

        WsServer.webSocketSet.forEach(obj -> {

            if(!WSActionType.MEETING.equals(obj.getType())){
                return ;
            }

            String id = obj.getId();

            ServiceResult result = intelligentMeetingAppService.getMeetingAppDetail(id);
            if (!result.isSuccess()) {
                obj.sendMessage(JSON.toJSONString(MessageResult.error((String) result.getObject(), null, MessageType.MEETING_DETAIL)), obj.getSession());
                obj.close(obj.getSessionId());

            } else {

                obj.sendMessage(JSON.toJSONString(MessageResult.ok((MeetingAppResp) result.getObject(), MessageType.MEETING_DETAIL)), obj.getSession());
            }
        });
    }
}
