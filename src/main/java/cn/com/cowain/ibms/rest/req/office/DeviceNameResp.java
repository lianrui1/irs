package cn.com.cowain.ibms.rest.req.office;

import cn.com.cowain.ibms.entity.office.IntelligentOfficeDevice;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author Yang.Lee
 * @date 2022/2/10 13:15
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DeviceNameResp {

    @ApiModelProperty(value = "办公室ID", required = true)
    private String officeId;

    @ApiModelProperty(value = "设备ID")
    private String deviceId;

    @ApiModelProperty(value = "设备名称")
    private String deviceName;

    @ApiModelProperty(value = "设备类型")
    private DeviceType deviceType;

    @ApiModelProperty(value = "附加名称", required = true)
    private DeviceNameEditReq.AdditionalName additionalName;


    public static DeviceNameResp convert(IntelligentOfficeDevice officeDevice) {

        return DeviceNameResp.builder()
                .deviceId(officeDevice.getDevice().getId())
                .deviceName(StringUtils.isEmpty(officeDevice.getDeviceAlisa())
                        ? officeDevice.getDevice().getDeviceName() :
                        officeDevice.getDeviceAlisa())
                .deviceType(officeDevice.getDevice().getDeviceType())
                .additionalName(StringUtils.isEmpty(officeDevice.getDeviceAlisaProperties())
                        ? DeviceNameEditReq.AdditionalName.empty() :
                        JSON.parseObject(officeDevice.getDeviceAlisaProperties(), DeviceNameEditReq.AdditionalName.class))
                .build();
    }
}
