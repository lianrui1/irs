package cn.com.cowain.ibms.rest.resp.ota;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author Yang.Lee
 * @date 2021/4/14 18:20
 */
@Data
@ApiModel("app升级任务分页数据")
public class UpdateTaskPageResp {

    @ApiModelProperty(value = "升级任务名称", required = true, example = "task name", position = 1)
    private String taskName;

    @ApiModelProperty(value = "应用安装包url", required = true, example = "url", position = 2)
    private String appPackageUrl;

    @ApiModelProperty(value = "应用安装包", required = true, example = "88.8MB", position = 3)
    private String appPackageSize;

    @ApiModelProperty(value = "升级应用名称", required = true, example = "智能语音助手", position = 4)
    private String applicationName;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "升级时间", required = true, example = "3000-12-31 09:30", position = 5)
    private LocalDateTime upgradeTime;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "更新内容", example = "3000-12-31 09:30", position = 6)
    private LocalDateTime createTime;

}
