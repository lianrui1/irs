package cn.com.cowain.ibms.rest.req.meeting;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author wei.cheng
 * @date 2022/03/02 10:28
 */
@Data
@ApiModel("会议室文档")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MeetingFileReq {

    @ApiModelProperty(value = "文件URL", required = true, position = 2, example = "http://tests.cowain.cn/group1/M00/00/0C/rBIQA2IUxcSACgUHAAIqBPKjJ5A851.pdf")
    @NotBlank
    private String url;

    @ApiModelProperty(value = "源文件文件名", required = true, position = 3, example = "测试文件.pdf")
    @NotBlank
    private String originalFileName;

    @ApiModelProperty(value = "文件大小，单位为B", required = true, position = 4)
    @NotNull
    @Min(value = 0)
    private Long size;
}
