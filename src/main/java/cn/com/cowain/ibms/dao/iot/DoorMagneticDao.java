package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.DoorMagnetic;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * 门磁数据持久层
 * @author Yang.Lee
 * @date 2020/12/11 9:24
 */
@Repository
public interface DoorMagneticDao extends JpaRepository<DoorMagnetic, String>, JpaSpecificationExecutor<DoorMagnetic> {

    /**
     * 根据门磁编号查询门磁信息
     * @param number 门磁编号
     * @return
     */
    Optional<DoorMagnetic> findByNumber(String number);

    /**
     * 根据设备id查询门磁信息
     * @return
     */
    Optional<DoorMagnetic> findByDeviceId(String id);

    /**
     * 根据设备查询
     * @author: yanzy
     * @data: 2022/9/6 18:09:23
     */
    Optional<DoorMagnetic> findByDevice(IotDevice iotDevice);

}
