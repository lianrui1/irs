package cn.com.cowain.ibms.mq.bean;

import lombok.Data;

/**
 * @author wei.cheng
 * @date 2022/04/07 18:42
 */
@Data
public class AlertCaptureorMessage {
    /**
     * 报警事件的uuid，当事件已存在忽略或者覆盖，请勿重复插入
     */
    private String alertRecordUuid;
    /**
     * 报警设备ID
     */
    private String deviceId;
    /**
     * 设备名
     */
    private String deviceName;
    /**
     * 设备uuid
     */
    private String deviceUuid;
    /**
     * 报警时间
     */
    private String alertTime;
    /**
     * 报警类型，`1`人员越界，`2`人员入侵，`3`车辆越界，`4`车辆禁停，`5`人员越界-翻墙检测，`6`人员徘徊，`7`机动车离开，`8`非机动车离开，`10`人员值岗/离岗-离岗，`11`人员值岗/离岗-超员，`12`人员值岗/离岗-少员，`13`人员奔跑，`14`摔倒检测，`15`人员扭打，`16`抽烟检测，`17`看手机，`18`接打电话，`19`人员聚众，`20`防碰撞预警，`21`车辆逆行，`22`睡岗检测，`31`机动车越界，`32`非机动车越界，`41`机动车禁停，`42`非机动车禁停
     */
    private Integer alarmType;
    /**
     * 报警子类型，可选值：`1`单绊线检测，`2`进入区域，`3`离开区域，`4`进出区域，`5`区域停留，`6`翻墙检测
     */
    private Integer alarmSubType;
    /**
     * 区域ID
     */
    private Integer zoneId;
    /**
     * 区域uuid
     */
    private String zoneUuid;
    /**
     * 区域名
     */
    private String zoneName;
    /**
     * 布点ID，由于旷世鸿图不返回布点ID，并且也没有相关查询API，该字段暂一直为空
     */
    private String channelId;
    /**
     * 布点名称，由于旷世鸿图不返回布点ID，并且也没有相关查询API，该字段暂一直为空
     */
    private String channelName;
    /**
     * 抓拍图
     */
    private String captureImg;
    /**
     * 底库图
     */
    private String groupImg;
    /**
     * 全景图
     */
    private String fullImg;
    /**
     * 目标类型，可选值：`1`人员
     */
    private Integer targetType;
    /**
     * 识别出的人员工号，如果人员未识别出，该字段为空，即为陌生人；targetType=`1`人员时存在
     */
    private String jobNo;
    /**
     * 识别出的人员名称，如果人员未识别出，该字段为空，即为陌生人；targetType=`1`人员时存在
     */
    private String name;
    /**
     * 识别出来的人员在鸿图平台的身份类型，`1`员工，`访客`，`3`黑名单，`4`陌生人；targetType=`1`人员时存在
     */
    private Integer groupType;
    /**
     * 鸿图回调所有数据，json格式
     */
    private String data;
}
