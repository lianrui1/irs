package cn.com.cowain.ibms.enumeration.centralized;

public enum TraceStatus {
    FINISH("完成"),

    PROCESSING("执行中");

    private String name;

    TraceStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
