package cn.com.cowain.ibms.service.wps.impl;

import cn.com.cowain.ibms.dao.meetingfile.MeetingFileDao;
import cn.com.cowain.ibms.dao.wps.WpsNotificationDao;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.meeting.MeetingFile;
import cn.com.cowain.ibms.entity.wps.WpsNotification;
import cn.com.cowain.ibms.enumeration.wps.WpsScene;
import cn.com.cowain.ibms.rest.req.wps.WpsOnnotifyReq;
import cn.com.cowain.ibms.rest.resp.wps.FileInfoResp;
import cn.com.cowain.ibms.rest.resp.wps.UserAclResp;
import cn.com.cowain.ibms.rest.resp.wps.UserResp;
import cn.com.cowain.ibms.rest.resp.wps.WpsGetFileInfoResp;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.wps.WpsCallbackService;
import cn.com.cowain.sfp.comm.utils.JwtUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.ZoneOffset;
import java.util.Objects;
import java.util.Optional;

/**
 * @author wei.cheng
 * @date 2022/02/28 09:50
 */
@Slf4j
@Service
public class WpsCallbackServiceImpl implements WpsCallbackService {
    @Autowired
    private MeetingFileDao meetingFileDao;
    @Autowired
    private WpsNotificationDao wpsNotificationDao;
    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserService;
    @Value("${wps.app-id}")
    private String appId;
    @Value("${fastdfs.fileUrl}")
    private String fastDfsHttpUrl;

    @Override
    public ServiceResult getFileInfo(String appId, String fileId, String token, String scene) {
        log.info("start to get file info by wps, appId:{}, fileId:{}, token:{}, scene:{}", appId, fileId, token, scene);
        // 校验token并且获取用户的hrId
        String userHrId = JwtUtil.getHrId(token);
        // 校验appId是否有效
        if (!StringUtils.equals(this.appId, appId)) {
            return ServiceResult.error("appId is invalid, appId: " + appId);
        }
        WpsGetFileInfoResp resp = new WpsGetFileInfoResp();
        // 校验scene是否有效
        if (WpsScene.MEETING_FILE.name().equals(scene)) {
            Optional<MeetingFile> meetingFileOptional = meetingFileDao.findById(fileId);
            if (meetingFileOptional.isEmpty()) {
                return ServiceResult.error("file is not found by fileId: " + fileId);
            } else {
                MeetingFile meetingFile = meetingFileOptional.get();
                FileInfoResp fileInfoResp = convertToFileInfoResp(meetingFile);
                resp.setFile(fileInfoResp);
                SysUser sysUser = sysUserService.getUserByEmpNo(userHrId);
                UserResp userResp = convertToUserResp(sysUser);
                resp.setUser(userResp);
            }
        } else {
            return ServiceResult.error("scene is invalid, scene: " + scene);
        }
        return ServiceResult.ok(resp);
    }

    /**
     * sysUser转换为UserResp对象
     *
     * @param sysUser
     * @return
     */
    private UserResp convertToUserResp(SysUser sysUser) {
        UserResp userResp = new UserResp();
        userResp.setId(sysUser.getEmpNo());
        userResp.setName(sysUser.getNameZh());
        userResp.setPermission("read");
        userResp.setAvatarUrl(sysUser.getHeadImgUrl());
        return userResp;
    }

    /**
     * meetingFile 转换为FileInfoResp对象
     *
     * @param meetingFile
     * @return
     */
    private FileInfoResp convertToFileInfoResp(MeetingFile meetingFile) {
        FileInfoResp fileInfoResp = new FileInfoResp();
        fileInfoResp.setId(meetingFile.getId());
        fileInfoResp.setName(meetingFile.getFileName() + "." + meetingFile.getFileExt());
        fileInfoResp.setVersion(1L);
        fileInfoResp.setSize(meetingFile.getSize());
        fileInfoResp.setCreator(meetingFile.getCreatedBy());
        fileInfoResp.setCreateTime(Objects.nonNull(meetingFile.getCreatedTime()) ?
                meetingFile.getUpdatedTime().toInstant(ZoneOffset.ofHours(8)).getEpochSecond() : null);
        fileInfoResp.setModifier(meetingFile.getUpdatedBy());
        fileInfoResp.setModifyTime(Objects.nonNull(meetingFile.getUpdatedTime()) ?
                meetingFile.getUpdatedTime().toInstant(ZoneOffset.ofHours(8)).getEpochSecond() : null);
        fileInfoResp.setDownloadUrl(fastDfsHttpUrl + meetingFile.getFilePath());
        fileInfoResp.setPreviewPages(null);
        fileInfoResp.setUserAcl(null);
        fileInfoResp.setWatermark(null);
        fileInfoResp.setUserAcl(buildDefaultUserAclResp());
        return fileInfoResp;
    }

    private UserAclResp buildDefaultUserAclResp() {
        UserAclResp userAclResp = new UserAclResp();
        userAclResp.setRename(0);
        userAclResp.setHistory(0);
        userAclResp.setCopy(0);
        userAclResp.setExport(0);
        userAclResp.setPrint(0);
        return userAclResp;
    }

    @Override
    public ServiceResult onNotify(String appId, String fileId, String token, String scene, WpsOnnotifyReq req) {
        log.info("start to onNotify by wps, appId:{}, fileId:{}, token:{}, scene:{}, req:{}", appId, fileId, token, scene, JSON.toJSONString(req));
        String userHrId = JwtUtil.getHrId(token);
        if (!StringUtils.equals(this.appId, appId)) {
            return ServiceResult.error("appId is invalid, appId: " + appId);
        }
        if (WpsScene.MEETING_FILE.name().equals(scene)) {
            Optional<MeetingFile> meetingFileOptional = meetingFileDao.findById(fileId);
            if (meetingFileOptional.isEmpty()) {
                return ServiceResult.error("file is not found by fileId: " + fileId);
            } else {
                wpsNotificationDao.save(buildWpsNotification(fileId, scene, req, userHrId));
            }
        } else {
            return ServiceResult.error("scene is invalid, scene: " + scene);
        }
        return ServiceResult.ok("通知成功");
    }

    /**
     * 构建wpsNotification对象
     *
     * @param fileId
     * @param scene
     * @param req
     * @param userHrId
     * @return
     */
    private WpsNotification buildWpsNotification(String fileId, String scene, WpsOnnotifyReq req, String userHrId) {
        WpsNotification wpsNotification = new WpsNotification();
        wpsNotification.setOperatorHrId(userHrId);
        wpsNotification.setFileId(fileId);
        wpsNotification.setScene(WpsScene.resolves(scene));
        wpsNotification.setCmd(req.getCmd());
        wpsNotification.setBody(JSON.toJSONString(req.getBody()));
        return wpsNotification;
    }
}
