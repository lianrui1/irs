package cn.com.cowain.ibms.rest.resp.iot;

import cn.com.cowain.ibms.enumeration.iot.ApproverType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/3/8 13:15
 */
@Data
@ApiModel("门禁点分配权限人接收对象")
public class AccessControlSpotApproverPageReq {

    @NotEmpty
    @ApiModelProperty(value = "人员工号", required = true)
    private String empNo;

    @NotEmpty
    @ApiModelProperty(value = "分配人员类型", required = true)
    private List<ApproverType> approveType;

    @NotNull
    @ApiModelProperty(value = "分配门禁点ID", required = true)
    private List<String> accessControlSpotIds;
}
