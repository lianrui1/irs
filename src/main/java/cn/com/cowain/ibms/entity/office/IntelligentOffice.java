package cn.com.cowain.ibms.entity.office;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.enumeration.iot.IntelligentOfficeStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 智能办公室
 *
 * @author Yang.Lee
 * @date 2021/11/8 14:45
 */
@Data
@Entity
@Table(name = "ibms_intelligent_office",indexes = {@Index(columnList = "space_id")})
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_intelligent_office", comment = "智能办公室表")
@Where(clause = "is_del=0")
public class IntelligentOffice extends BaseEntity {

    @Column(name = "name", nullable = false, columnDefinition = "varchar(10)  COMMENT '办公室名称'")
    private String name;

    @ManyToOne
    @JoinColumn(name = "space_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'iot_space FK'")
    private Space space;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", columnDefinition = "varchar(10) COMMENT '办公室状态'")
    private IntelligentOfficeStatus status;
}
