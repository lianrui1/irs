package cn.com.cowain.ibms.websocket.bean.xunfei.rtasr;

import lombok.Data;

import java.util.List;

/**
 * 实时转写结果对象
 * @author Yang.Lee
 * @date 2021/2/3 17:38
 */
@Data
public class RTResp {

    /**
     * 解析结果ws结合
     */
    private List<WSResp> ws;
}
