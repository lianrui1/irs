package cn.com.cowain.ibms.rest.resp.device;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

/**
 * @Author tql
 * @Description 楼宇总览 出参类
 * @Date 21-11-4 下午3:13
 * @Version 1.0
 */
@Data
@Builder
@ApiModel("空间列表")
public class SpaceListViewResp {

    /**
     * 空间名
     */
    @ApiModelProperty(value = "空间Id", example = "12asdasd", position = 1)
    private String id;

    /**
     * 空间名
     */
    @ApiModelProperty(value = "空间名", example = "总经理办公室", position = 1)
    private String name;

    /**
     * 空间位置
     */
    @ApiModelProperty(value = "空间位置", example = "1", position = 2)
    private String position;

    /**
     * 描述
     */
    @ApiModelProperty(value = "描述", example = "1", position = 3)
    private String desc;

    /**
     * 预定日期
     */
    @JsonFormat(pattern = "MM-dd")
    @ApiModelProperty(name = "日期", example = "12-31", position = 4)
    private LocalDate date;

    /**
     * 空间设备列表
     */
    @ApiModelProperty(value = "空间设备列表", example = "1", position = 5)
    private List<SpaceDeviceListViewResp> spaceDeviceListViewRespList;


}
