package cn.com.cowain.ibms.dao;

import cn.com.cowain.ibms.entity.Room;
import cn.com.cowain.ibms.rest.bean.PageBean;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/3 19:37
 */
@SpringBootTest
public class RoomDaoTest3 {

    @Autowired
    private RoomDao roomDao;

    @Test
    public void test0() {
        Assertions.assertNotNull(roomDao);
    }

    @Test
    public void test2() {
        Sort sort = Sort.by(Sort.Direction.ASC, "seq");
        Pageable pageable = PageRequest.of(2, 2, sort);
        Page<Room> roomPage = roomDao.findByNameContains("会议室", pageable);
        PageBean pageBean = new PageBean();

        pageBean.setPage(pageable.getPageNumber());  //当前页码
        pageBean.setSize(pageable.getPageSize());  //每页记录数
        pageBean.setTotalElements(roomPage.getTotalElements());  //总记录数
        pageBean.setTotalPages(roomPage.getTotalPages());  //总页数
        pageBean.setList(roomPage.getContent());
        System.out.println(pageBean);
    }

    @Test
    public void test33() {
        Integer maxSeq = roomDao.findMaxSeq();
        System.out.println(maxSeq);
    }

}