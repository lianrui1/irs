package cn.com.cowain.ibms;

import cn.com.cowain.ibms.dao.iot.SpaceDao;
import cn.com.cowain.ibms.dao.meal.KshtTimePlanDao;
import cn.com.cowain.ibms.dao.office.DeviceModeDefaultStatusDao;
import cn.com.cowain.ibms.dao.office.DeviceWorkingDefaultModeDao;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.meal.KsHtTimePlan;
import cn.com.cowain.ibms.entity.office.DeviceModeDefaultStatus;
import cn.com.cowain.ibms.entity.office.DeviceWorkingDefaultMode;
import cn.com.cowain.ibms.enumeration.iot.AccessControlSpotPlace;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.mq.bean.OpenAbilityMessage;
import cn.com.cowain.ibms.rest.req.working_mode.DeviceModeStatusSaveReq;
import cn.com.cowain.ibms.service.ability.AbilityService;
import cn.com.cowain.ibms.service.bean.DoorMagneticHWParasReq;
import cn.com.cowain.ibms.service.bean.DoorMagneticHWReq;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.device.HwProductsService;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.usercenter.api.service.UserCenterStaffApi;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Yang.Lee
 * @date 2021/1/30 10:30
 */
@Slf4j
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UCTest {

    @Autowired
    private SpaceDao spaceDao;

    @Resource
    private UserCenterStaffApi userCenterStaffApi;

    @Resource
    private DeviceWorkingDefaultModeDao defaultModeDao;

    @Resource
    private DeviceModeDefaultStatusDao defaultStatusDao;


    @Resource
    private KshtTimePlanDao kshtTimePlanDao;

    @Resource
    private AbilityService abilityService;

    @Autowired
    private HwProductsService productsService;

    @Test
    public void test6(){
        DoorMagneticHWParasReq paras = new DoorMagneticHWParasReq();
        paras.setAction("close");
            // 获取设备所属产品的服务ID和命令名称
            DoorMagneticHWReq hwReq = productsService.findProduct("62ca3acd2c34d634690e6403");
            String hrId = "";
            String name = "";
            // 下发设备控制命令
            hwReq.setDeviceId("");
            hwReq.setDevice_id("62ca3acd2c34d634690e6403_duanzong_curtain012");
            hwReq.setParas(paras);
            hwReq.setIsOne("0");
            ServiceResult result = productsService.command(hwReq, hrId, name);
    }

    @Test
    public void test5(){
        OpenAbilityMessage abilityMessage = JSON.parseObject("{\"ability\":\"VISITOR\",\"over\":1,\"type\":\"DELETE\",\"userList\":[{\"hrId\":\"TN015079\",\"leaveType\":1,\"name\":\"测试仪魏沃\",\"phone\":\"17758076963\",\"place\":\"KS\"}]}", OpenAbilityMessage.class);
        abilityService.process(abilityMessage);
    }

    @Test
    public void test() {
        JsonResult result = userCenterStaffApi.list();
        System.out.println(result.toString());
    }

    @Test
    public void test4() {
        JsonResult result = userCenterStaffApi.passportList(1L);
        System.out.println(result.toString());
    }


    @Test
    public void test2() {
        JsonResult result = userCenterStaffApi.passportList("李");
        System.out.println(result.toString());
    }

    @Test
    public void test3() {
        JsonResult result = userCenterStaffApi.passport("CWA1715");
        System.out.println(result.toString());
    }

    @Test
    public void ceshi() {
        System.out.println("获取的子集数据");
        List<Space> spaces = spaceDao.findAll();
        List<String> childMenu = new ArrayList<String>();
        treeMenuList(spaces, "8b6a847d-dbd7-42bc-ab48-153a874313cd", childMenu);
        log.info("获取的子集数据:{}", childMenu.toString());
    }

    /**
     * 获取某个父节点下面的所有子节点
     *
     * @param menuList
     * @param pid
     * @return
     */
    public List<String> treeMenuList(List<Space> menuList, String pid, List<String> childMenu) {
        for (Space mu : menuList) {
            // 遍历出父id等于参数的id，add进子节点集合
            if (mu.getParent() != null) {
                if (pid.equals(mu.getParent().getId())) {
                    // 递归遍历下一级
                    childMenu.add(mu.getId());
                    treeMenuList(menuList, mu.getId(), childMenu);
                }
            }
        }
        return childMenu;
    }


    @Test
    public void test1(){

        // 保存默认模式
        List<DeviceWorkingDefaultMode> addModeList = new ArrayList<>();
        DeviceWorkingDefaultMode toWork = new DeviceWorkingDefaultMode();
        toWork.setImg("icon-scene-meeting");
        toWork.setName("上班");
        toWork.setPurpose("只需点击上班,办公室相关智能设备即可按照模式进行工作");
        addModeList.add(toWork);
        DeviceWorkingDefaultMode offWork = new DeviceWorkingDefaultMode();
        offWork.setImg("icon-scene-offwork");
        offWork.setName("下班");
        offWork.setPurpose("自动关闭设备,切断电源,节能环境、轻松下班");
        addModeList.add(offWork);
        DeviceWorkingDefaultMode rest = new DeviceWorkingDefaultMode();
        rest.setImg("icon-scene-rest");
        rest.setName("休息");
        rest.setPurpose("启动后会自动调节灯光亮度等操作,提供放松的午休环境");
        addModeList.add(rest);
        // 保存
        defaultModeDao.saveAll(addModeList);

        // 设备属性
        // 窗帘
        DeviceModeStatusSaveReq curtainsOpen = new DeviceModeStatusSaveReq();
        curtainsOpen.setAction("open");
        DeviceModeStatusSaveReq curtainsClose = new DeviceModeStatusSaveReq();
        curtainsClose.setAction("close");
        // 灯
        DeviceModeStatusSaveReq lightOn = new DeviceModeStatusSaveReq();
        lightOn.setLight1("on");
        lightOn.setLight2("on");
        lightOn.setLight3("on");
        lightOn.setLight4("on");
        DeviceModeStatusSaveReq lightOff = new DeviceModeStatusSaveReq();
        lightOff.setLight1("off");
        lightOff.setLight2("off");
        lightOff.setLight3("off");
        lightOff.setLight4("off");
        // 空调
        DeviceModeStatusSaveReq airConditionerOn = new DeviceModeStatusSaveReq();
        airConditionerOn.setSetPower("on");
        airConditionerOn.setSetTemp(27);
        airConditionerOn.setSetMode("heat");
        airConditionerOn.setSetFanSpeed("auto");
        airConditionerOn.setSetSwing("noSwing");
        DeviceModeStatusSaveReq airConditionerOff = new DeviceModeStatusSaveReq();
        airConditionerOff.setSetPower("off");
        airConditionerOff.setSetTemp(27);
        airConditionerOff.setSetMode("heat");
        airConditionerOff.setSetFanSpeed("auto");
        airConditionerOff.setSetSwing("noSwing");

        // 保存模式下设备状态
        List<DeviceModeDefaultStatus> addStatusList = new ArrayList<>();
        // 上班
        DeviceModeDefaultStatus toWorkStatus1 = new DeviceModeDefaultStatus();
        toWorkStatus1.setDeviceType(DeviceType.CURTAINS);
        toWorkStatus1.setMode(toWork);
        toWorkStatus1.setProperties(JSON.toJSONString(curtainsOpen));
        addStatusList.add(toWorkStatus1);

        DeviceModeDefaultStatus toWorkStatus2 = new DeviceModeDefaultStatus();
        toWorkStatus2.setDeviceType(DeviceType.FOUR_WAY_SWITCH);
        toWorkStatus2.setMode(toWork);
        toWorkStatus2.setProperties(JSON.toJSONString(lightOn));
        addStatusList.add(toWorkStatus2);

        DeviceModeDefaultStatus toWorkStatus3 = new DeviceModeDefaultStatus();
        toWorkStatus3.setDeviceType(DeviceType.AIR_CONDITIONER);
        toWorkStatus3.setMode(toWork);
        toWorkStatus3.setProperties(JSON.toJSONString(airConditionerOn));
        addStatusList.add(toWorkStatus3);

        // 下班
        DeviceModeDefaultStatus offWorkStatus1 = new DeviceModeDefaultStatus();
        offWorkStatus1.setDeviceType(DeviceType.CURTAINS);
        offWorkStatus1.setMode(offWork);
        offWorkStatus1.setProperties(JSON.toJSONString(curtainsClose));
        addStatusList.add(offWorkStatus1);

        DeviceModeDefaultStatus offWorkStatus2 = new DeviceModeDefaultStatus();
        offWorkStatus2.setDeviceType(DeviceType.FOUR_WAY_SWITCH);
        offWorkStatus2.setMode(offWork);
        offWorkStatus2.setProperties(JSON.toJSONString(lightOff));
        addStatusList.add(offWorkStatus2);

        DeviceModeDefaultStatus offWorkStatus3 = new DeviceModeDefaultStatus();
        offWorkStatus3.setDeviceType(DeviceType.AIR_CONDITIONER);
        offWorkStatus3.setMode(offWork);
        offWorkStatus3.setProperties(JSON.toJSONString(airConditionerOff));
        addStatusList.add(offWorkStatus3);

        // 休息
        DeviceModeDefaultStatus restStatus1 = new DeviceModeDefaultStatus();
        restStatus1.setDeviceType(DeviceType.CURTAINS);
        restStatus1.setMode(rest);
        restStatus1.setProperties(JSON.toJSONString(curtainsClose));
        addStatusList.add(restStatus1);

        DeviceModeDefaultStatus restStatus2 = new DeviceModeDefaultStatus();
        restStatus2.setDeviceType(DeviceType.FOUR_WAY_SWITCH);
        restStatus2.setMode(rest);
        restStatus2.setProperties(JSON.toJSONString(lightOff));
        addStatusList.add(restStatus2);

        DeviceModeDefaultStatus restStatus3 = new DeviceModeDefaultStatus();
        restStatus3.setDeviceType(DeviceType.AIR_CONDITIONER);
        restStatus3.setMode(rest);
        restStatus3.setProperties(JSON.toJSONString(airConditionerOn));
        addStatusList.add(restStatus3);

        defaultStatusDao.saveAll(addStatusList);
    }


    @Test
    void getSysUserInfo() {
        Optional<KsHtTimePlan> timePlane = kshtTimePlanDao.findByStartTimeAndEndTimeAndPlace("10:00","11:00", AccessControlSpotPlace.KS);

    }

}
