package cn.com.cowain.ibms.service.meeting.impl;

import cn.com.cowain.ibms.dao.meeting.ScheduleDao;
import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.entity.meeting.Schedule;
import cn.com.cowain.ibms.feign.task_center.ScheduleApi;
import cn.com.cowain.ibms.service.bean.ServiceMessage;
import cn.com.cowain.ibms.service.bean.schedule.req.DataSyncReq;
import cn.com.cowain.ibms.service.meeting.ScheduleService;
import cn.com.cowain.ibms.utils.DateUtils;
import cn.com.cowain.sfp.comm.JsonResult;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Optional;

/**
 * 日程Service服务实现类
 *
 * @author Yang.Lee
 * @date 2021/6/21 13:21
 */
@Slf4j
@Service
public class ScheduleServiceImpl implements ScheduleService {

    @Resource
    private ScheduleApi scheduleApi;

    @Resource
    private ScheduleDao scheduleDao;

    /**
     * 创建日程
     *
     * @param reservationRecord 会议信息
     * @param empNo             日程所属员工
     * @author Yang.Lee
     * @date 2021/6/21 19:46
     **/
    @Async
    @Override
    @Transactional
    public void create(ReservationRecord reservationRecord, String empNo) {

        Optional<Schedule> scheduleOptional = scheduleDao.findByEmpNoAndReservationRecordId(empNo, reservationRecord.getId());

        if (scheduleOptional.isPresent()) {
            log.error("已存在日程数据, id: {}, empNo: {}", reservationRecord.getId(), empNo);
            return ;
        }

        // 创建日程对象
        Schedule schedule = new Schedule();
        schedule.setReservationRecord(reservationRecord);
        schedule.setEmpNo(empNo);

        scheduleDao.save(schedule);

        log.debug("创建 schedule完成, {}", JSON.toJSONString(schedule));


        String startTime = DateUtils.formatLocalDateTime(reservationRecord.getFrom(), DateUtils.PATTERN_DATETIME);
        DataSyncReq req = DataSyncReq.builder()
                .busiId(schedule.getId())
                .from("ibms")
                .busiEventDesc(reservationRecord.getRoom().getName() + " 会议室")
                .busiEventTitle(StringUtils.isEmpty(reservationRecord.getTopic()) ? "会议" : reservationRecord.getTopic())
                .busiEventType("1000")
                .startTimeStr(startTime)
                .endTimeStr(DateUtils.formatLocalDateTime(reservationRecord.getTo(), DateUtils.PATTERN_DATETIME))
                .targetJobNo(empNo)
                .build();

        log.debug("请求创建日程，参数：{}", JSON.toJSONString(req));

        JsonResult<String> jsonResult = scheduleApi.create(req);

        if (jsonResult.getStatus() == 0) {
            log.error("向日程中心创建会议数据错误，请求参数：{}， 返回结果：{}", JSON.toJSONString(req), jsonResult);
            return ;
        }

        schedule.setTaskCenterId(jsonResult.getData());
        scheduleDao.save(schedule);

        log.debug("同步日程数据成功，用户{}, 会议{}", empNo, reservationRecord.getId());
    }

    /**
     * 取消日程
     *
     * @param reservationRecord 会议信息
     * @param empNo             日程所属员工
     * @author Yang.Lee
     * @date 2021/6/21 20:50
     **/
    @Async
    @Override
    @Transactional
    public void cancel(ReservationRecord reservationRecord, String empNo) {

        Optional<Schedule> scheduleOptional = scheduleDao.findByEmpNoAndReservationRecordId(empNo, reservationRecord.getId());
        if(scheduleOptional.isEmpty()){
            log.error(ServiceMessage.NOT_FOUND_MEETING_SCHEDULE + " " + empNo + " " + reservationRecord.getId());
            return ;
        }

        Schedule schedule = scheduleOptional.get();
        DataSyncReq req =  DataSyncReq.builder().id(scheduleOptional.get().getTaskCenterId()).build();

        log.debug("请求取消日程，参数：{}", JSON.toJSONString(req));

        JsonResult<String> jsonResult = scheduleApi.delete(req);

        if (jsonResult.getStatus() == 0) {
            log.error("取消日程数据错误，请求参数：{}， 返回结果：{}", JSON.toJSONString(req), jsonResult);
            return ;
        }
        log.debug("取消日程数据成功，用户{}, 会议{}", empNo, reservationRecord.getId());

        // 取消后删除该记录
        schedule.setIsDelete(1);
        scheduleDao.save(schedule);
    }

    /**
     * 修改日程
     *
     * @param reservationRecord 会议信息
     * @param empNo             日程所属员工
     * @author Yang.Lee
     * @date 2021/6/21 21:38
     **/
    @Async
    @Override
    @Transactional
    public void update(ReservationRecord reservationRecord, String empNo) {

        Optional<Schedule> scheduleOptional = scheduleDao.findByEmpNoAndReservationRecordId(empNo, reservationRecord.getId());
        if(scheduleOptional.isEmpty()){

            log.error(ServiceMessage.NOT_FOUND_MEETING_SCHEDULE);
            return;
        }

        Schedule schedule = scheduleOptional.get();
        String startTime = DateUtils.formatLocalDateTime(reservationRecord.getFrom(), DateUtils.PATTERN_DATETIME);

        DataSyncReq req = DataSyncReq.builder()
                .id(schedule.getTaskCenterId())
                .busiId(schedule.getId())
                .from("ibms")
                .busiEventDesc(reservationRecord.getRoom().getName() + " 会议室")
                .busiEventTitle(StringUtils.isEmpty(reservationRecord.getTopic()) ? "会议" : reservationRecord.getTopic())
                .busiEventType("1000")
                .startTimeStr(startTime)
                .endTimeStr(DateUtils.formatLocalDateTime(reservationRecord.getTo(), DateUtils.PATTERN_DATETIME))
                .targetJobNo(empNo)
                .build();

        log.debug("请求更新日程，参数：{}", JSON.toJSONString(req));

        JsonResult<String> jsonResult = scheduleApi.update(req);

        if (jsonResult.getStatus() == 0) {
            log.error("编辑日程数据错误，请求参数：{}， 返回结果：{}", JSON.toJSONString(req), jsonResult);
            return ;
        }
        log.debug("编辑日程数据成功，用户{}, 会议{}", empNo, reservationRecord.getId());
    }
}
