package cn.com.cowain.ibms.dao.device;

import cn.com.cowain.ibms.entity.iot.centralizedControl.CentralizedControlAdmin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface CentralizedControlAdminDao extends JpaRepository<CentralizedControlAdmin, String>, JpaSpecificationExecutor<CentralizedControlAdmin> {
}
