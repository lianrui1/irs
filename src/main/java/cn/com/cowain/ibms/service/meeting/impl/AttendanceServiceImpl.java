package cn.com.cowain.ibms.service.meeting.impl;

import cn.com.cowain.ibms.dao.meeting.AttendanceDao;
import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.entity.meeting.Attendance;
import cn.com.cowain.ibms.enumeration.meeting.AttendanceStatus;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.meeting.AttendancePageReq;
import cn.com.cowain.ibms.rest.resp.meeting.AttendancePageResp;
import cn.com.cowain.ibms.service.meeting.AttendanceService;
import cn.com.cowain.ibms.service.meeting.IntelligentMeetingAppService;
import cn.com.cowain.ibms.service.space.IotDeviceService;
import cn.com.cowain.ibms.utils.DateUtils;
import cn.com.cowain.ibms.utils.enums.DateUnit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nullable;
import javax.annotation.Resource;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Yang.Lee
 * @date 2021/6/15 10:48
 */
@Slf4j
@Service
public class AttendanceServiceImpl implements AttendanceService {

    @Resource
    private AttendanceDao attendanceDao;

    @Resource
    private IotDeviceService iotDeviceService;

    @Resource
    private IntelligentMeetingAppService intelligentMeetingAppService;

    /**
     * 会议签到(异步执行)
     *
     * @param reservationRecord 会议ID
     * @param empNo             用户工号
     * @author Yang.Lee
     * @date 2021/6/15 10:24
     **/
    @Async
    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void signIn(@Nullable ReservationRecord reservationRecord, String empNo) {

        if (reservationRecord == null) {
            log.error("会议签到，会议记录为null");
            return;
        }
        long tp = DateUtils.betweenTime(LocalDateTime.now(), reservationRecord.getFrom(), DateUnit.MINUTE);
        // 会议开始前5分钟才可以签到
        if (tp >= 5) {
            log.debug("会议开始前5分钟扫码，才可以签到");
            return;
        }


        // 已经签到的话就不需要再次签到
        List<Attendance> attendanceList = attendanceDao.findByReservationRecordId(reservationRecord.getId());

        if (attendanceList.isEmpty()) {
            // 无签到记录，第一个签到的人开启房间设备
            iotDeviceService.openSpaceDevices(reservationRecord.getRoom().getSpace());

            Attendance attendance = new Attendance();
            attendance.setReservationRecord(reservationRecord);
            attendance.setUserEmpNo(empNo);
            attendanceDao.save(attendance);

            // 推送消息
            intelligentMeetingAppService.pushMeetingRoomDetail(reservationRecord.getRoom().getId());
        } else {
            //  已有签到记录，判断当前用户是否需要签到
            Optional<Attendance> attendanceOptional = attendanceDao.findByUserEmpNoAndReservationRecordId(empNo, reservationRecord.getId());
            if (attendanceOptional.isEmpty()) {
                // 如果用户未签到则签到
                Attendance attendance = new Attendance();
                attendance.setReservationRecord(reservationRecord);
                attendance.setUserEmpNo(empNo);
                attendanceDao.save(attendance);
                // 推送消息
                intelligentMeetingAppService.pushMeetingRoomDetail(reservationRecord.getRoom().getId());
            }
        }
    }

    /**
     * 获取分页数据
     *
     * @param req 查询参数
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/7/16 16:11
     **/
    @Override
    @Transactional(readOnly = true)
    public PageBean<AttendancePageResp> getPage(AttendancePageReq req) {

        Pageable pageable = PageRequest.of(req.getPage(), req.getSize(), Sort.by(req.getOrders()));
        Page<Map<String, Object>> page = attendanceDao.selectAttendancePage(req.getProjectId(),
                req.getRoomId(),
                req.getSignState(),
                req.getKeyWord(),
                pageable);

        PageBean result = new PageBean<>();
        result.setPage(req.getPage());
        result.setSize(req.getSize());
        result.setTotalPages(page.getTotalPages());
        result.setTotalElements(page.getTotalElements());
        result.setList(page.getContent()
                .stream()
                .map(obj -> {
                    AttendanceStatus signStatus;
                    LocalDateTime signDatetime = null;
                    if (null == obj.get("signDatetime")) {
                        signStatus = AttendanceStatus.NOT_SIGN;
                    } else {
                        signStatus = AttendanceStatus.SIGNED;
                        signDatetime = ((Timestamp) obj.get("signDatetime")).toLocalDateTime();
                    }
                    return AttendancePageResp.builder()
                            .name((String) obj.get("name"))
                            .empNo((String) obj.get("empNo"))
                            .department((String) obj.get("full_department_name"))
                            .roomName((String) obj.get("roomName"))
                            .projectName((String) obj.get("projectName"))
                            .spaceName((String) obj.get("spaceName"))
                            .meetingDate(((Date) obj.get("meetingDate")).toLocalDate())
                            .startTime(((Timestamp) obj.get("startTime")).toLocalDateTime().toLocalTime())
                            .endTime(((Timestamp) obj.get("endTime")).toLocalDateTime().toLocalTime())
                            .topic((String) obj.get("topic"))
                            .signDatetime(signDatetime)
                            .signState(signStatus)
                            .signStateName(signStatus.getName())
                            .build();
                })
                .collect(Collectors.toList()));

        return result;
    }
}
