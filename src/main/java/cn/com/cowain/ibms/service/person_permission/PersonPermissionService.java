package cn.com.cowain.ibms.service.person_permission;

import cn.com.cowain.ibms.rest.req.person_permission.DepartmentPersonPermissionCreateReq;
import cn.com.cowain.ibms.rest.req.person_permission.PersonPermissionCreateReq;
import cn.com.cowain.ibms.service.bean.ServiceResult;

/**
 * Description
 *
 * @author yuchunlei
 * @version 1.0.0
 * @createTime 2020年12月21日 08:55:00
 */
public interface PersonPermissionService {

    /**
     * 新增
     *
     * @param personPermissionCreateReq
     * @return
     */
    ServiceResult save(PersonPermissionCreateReq personPermissionCreateReq);

    /**
     * @param personPermissionCreateReq 部门入参
     * @return
     * @description 部门人员新增
     * @author tql
     * @date 21-11-19
     */
    ServiceResult departmentSave(DepartmentPersonPermissionCreateReq personPermissionCreateReq);


    /**
     * 删除
     *
     * @param empNo
     * @param spaceId
     * @return
     */
    ServiceResult deleteByEmpNoAndSpaceId(String empNo, String spaceId);


    /**
     * 删除
     *
     * @param id 空间id
     * @return
     */
    ServiceResult spaceDetail(String id);

}
