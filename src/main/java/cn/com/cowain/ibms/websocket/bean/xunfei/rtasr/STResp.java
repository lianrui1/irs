package cn.com.cowain.ibms.websocket.bean.xunfei.rtasr;

import lombok.Data;

import java.util.List;

/**
 * 讯飞实时转写返回数据
 * @author Yang.Lee
 * @date 2021/2/3 17:35
 */
@Data
public class STResp {
    /**
     * 句子在整段语音中的开始时间，单位毫秒(ms),中间结果的bg为准确值
     */
    private int bg;
    /**
     * 句子在整段语音中的结束时间，单位毫秒(ms), 中间结果的ed为0
     */
    private int ed;
    /**
     * 实时转写结果
     */
    private List<RTResp> rt;
    /**
     * 结果类型标识, 0-最终结果；1-中间结果
     */
    private String type;
}