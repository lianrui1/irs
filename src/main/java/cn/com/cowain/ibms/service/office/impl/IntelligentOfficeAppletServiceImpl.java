package cn.com.cowain.ibms.service.office.impl;

import cn.com.cowain.ibms.dao.device.DeviceModeStatusDao;
import cn.com.cowain.ibms.dao.device.DeviceWorkingModeDao;
import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.dao.office.*;
import cn.com.cowain.ibms.dao.space.SpaceAdminDao;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.office.*;
import cn.com.cowain.ibms.entity.space.SpaceAdmin;
import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.enumeration.iot.UsableStatus;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.PageReq;
import cn.com.cowain.ibms.rest.req.office.CreatDefaultWorkingModeToMyWorkingModeReq;
import cn.com.cowain.ibms.rest.req.office.DeviceNameEditReq;
import cn.com.cowain.ibms.rest.req.working_mode.DeviceModeStatusSaveReq;
import cn.com.cowain.ibms.rest.req.working_mode.ManagementSaveReq;
import cn.com.cowain.ibms.rest.req.working_mode.WorkingModePageReq;
import cn.com.cowain.ibms.rest.req.working_mode.WorkingModeSaveReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.working_mode.*;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.DoorMagneticHWParasReq;
import cn.com.cowain.ibms.service.bean.DoorMagneticHWReq;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.device.HwProductsService;
import cn.com.cowain.ibms.service.office.IntelligentOfficeAppletService;
import cn.com.cowain.ibms.service.office.IntelligentOfficeDeviceService;
import cn.com.cowain.ibms.service.workingmode.WorkingModeService;
import cn.com.cowain.ibms.utils.TokenUtils;
import cn.com.cowain.sfp.comm.utils.JwtUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/12/7 16:43
 */
@Slf4j
@Service
public class IntelligentOfficeAppletServiceImpl implements IntelligentOfficeAppletService {

    @Autowired
    private DeviceWorkingModeDao workingModeDao;

    @Autowired
    private SpaceAdminDao spaceAdminDao;

    @Autowired
    private DeviceModeStatusDao modeStatusDao;

    @Autowired
    private DeviceDao deviceDao;

    @Autowired
    private IntelligentOfficeDao officeDao;

    @Autowired
    private IntelligentOfficeUserDao officeUserDao;

    @Resource
    private DeviceWorkingDefaultModeDao defaultModeDao;

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserServiceUC;

    @Autowired
    private WorkingModeService workingModeService;

    @Autowired
    private HwProductsService productsService;

    @Autowired
    private IntelligentOfficeDeviceService intelligentOfficeDeviceService;

    @Autowired
    private IntelligentOfficeDeviceDao officeDeviceDao;

    @Autowired
    private DeviceModeDefaultStatusDao deviceModeDefaultStatusDao;

    @Autowired
    private IntelligentOfficeDeviceDao intelligentOfficeDeviceDao;

    @Override
    @Transactional
    public ServiceResult save(WorkingModeSaveReq req) {

        // 从token中解析员工工号
        String hrId = JwtUtil.getHrId(TokenUtils.getToken());

        if(StringUtils.isEmpty(req.getName()) || req.getName().length() > 20){
            return ServiceResult.error("场景名称不符合规范");
        }
        // 根据会议室ID获取会议室信息
        Optional<IntelligentOffice> officeOp = officeDao.findById(req.getOfficeId());
        if(officeOp.isEmpty()){
            return ServiceResult.error("会议室ID有误,不存在");
        }
        IntelligentOffice intelligentOffice = officeOp.get();

        // 判断创建人是否是该空间管理员
        Optional<SpaceAdmin> spaceAdminOp = spaceAdminDao.findBySpaceIdAndAdminEmpNo(req.getSpaceId(), hrId);
        if(spaceAdminOp.isEmpty()){
            return ServiceResult.error("添加失败,无权限");
        }

        // 判断该空间下创建人该模式是否已经存在
        Optional<DeviceWorkingMode> workingModeOp = workingModeDao.findBySpaceIdAndNameAndUserHrId(req.getSpaceId(), req.getName(), hrId);
        if(workingModeOp.isPresent()){
            return ServiceResult.error("此空间下该模式已存在,不可重复创建");
        }

        // 工作模式
        DeviceWorkingMode workingMode = new DeviceWorkingMode();
        workingMode.setIsDefault(1);
        workingMode.setName(req.getName());
        workingMode.setImg(req.getImgUrl());
        workingMode.setSpace(intelligentOffice.getSpace());
        workingMode.setIntelligentOffice(intelligentOffice);
        workingMode.setUsableStatus(req.getUsableStatus());
        workingMode.setPurpose(req.getPurpose());
        workingMode.setUserHrId(hrId);
        // 保存
        workingModeDao.save(workingMode);

        // 保存模式下对应设备
        for(DeviceModeStatusSaveReq modeStatusSaveReq : req.getSaveReqs()){

            Optional<IotDevice> deviceOp = deviceDao.findByHwDeviceId(modeStatusSaveReq.getDeviceId());
            if(deviceOp.isEmpty()){
                // 业务处理失败，手动回滚事务,返回异常
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error("设备不存在，绑定失败");
            }

            // 设备状态
            IotDevice device = deviceOp.get();
            DeviceModeStatus modeStatus = new DeviceModeStatus();
            modeStatus.setProperties(JSON.toJSONString(modeStatusSaveReq));
            modeStatus.setDevice(device);
            modeStatus.setMode(workingMode);
            modeStatusDao.save(modeStatus);
        }

        log.info("场景新增成功: " + workingMode.getName());

        // 返回主键ID
        IdResp idResp = new IdResp();
        idResp.setId(workingMode.getId());
        return ServiceResult.ok(idResp);
    }

    @Override
    public PageBean<WorkingModePageResp> getPage(WorkingModePageReq req) {

        // 获取创建人工号
        // 从token中解析员工工号
        String hrId = JwtUtil.getHrId(TokenUtils.getToken());
        Optional<IntelligentOfficeUser> officeUserOp = officeUserDao.findByIntelligentOfficeIdAndUserHrId(req.getOfficeId(), hrId);
        if(officeUserOp.isPresent()){
            hrId = officeUserOp.get().getInvitedUserHrId();
        }

        // 工作模式多条件排序
        PageBean<WorkingModePageResp> pageBean = new PageBean<>();
        Sort sort;
        if(0 == req.getSeq()){
            sort = Sort.by(Sort.Direction.DESC, "controlTime");
        }else{
            List<Sort.Order> orders= new ArrayList<>();
            orders.add( new Sort.Order(Sort.Direction.DESC, "usableStatus"));
            orders.add( new Sort.Order(Sort.Direction.DESC, "createdTime"));
            sort = Sort.by(orders);
        }

        Pageable pageable = PageRequest.of(req.getPage(), req.getSize(), sort);

        // 获取智能办公室下工作场景
        String finalHrId = hrId;
        Page<DeviceWorkingMode> workingModePage = workingModeDao.findAll((root, criteriaQuery, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();

            // 智能办公室ID
            list.add(criteriaBuilder.equal(root.get("intelligentOffice").get("id"), req.getOfficeId()));

            // 创建人工号
            list.add(criteriaBuilder.equal(root.get("userHrId"), finalHrId));

            // 是否启用
            if(null != req.getStatus()){
                list.add(criteriaBuilder.equal(root.get("usableStatus"), req.getStatus()));
            }

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, pageable);

        List<WorkingModePageResp> list = new ArrayList<>();
        workingModePage.getContent().forEach(it ->{

            WorkingModePageResp resp = new WorkingModePageResp();

            resp.setId(it.getId());
            resp.setName(it.getName());
            resp.setImgUrl(it.getImg());
            resp.setStatus(it.getUsableStatus());
            resp.setPurpose(it.getPurpose());
            resp.setIsDefault(it.getIsDefault());
            list.add(resp);
        });

        pageBean.setList(list);
        pageBean.setPage(req.getPage());
        pageBean.setSize(req.getSize());
        pageBean.setTotalPages(workingModePage.getTotalPages());
        pageBean.setTotalElements(workingModePage.getTotalElements());
        return pageBean;
    }

    @Override
    public ServiceResult detail(String id) {

        Optional<DeviceWorkingMode> workingModeOp = workingModeDao.findById(id);
        if(workingModeOp.isEmpty()){
            return ServiceResult.error("id有误");
        }
        DeviceWorkingMode workingMode = workingModeOp.get();

        // 获取模式下设备状态
        List<DeviceModeStatusDetailResp> detailResps = new ArrayList<>();
        modeStatusDao.findByModeId(id).forEach(it ->{
            IotDevice device = it.getDevice();
            DeviceModeStatusDetailResp resp = new DeviceModeStatusDetailResp();
            if (StringUtils.isNotBlank(it.getProperties())) {
                DeviceModeStatusDetailResp properties = JSON.parseObject(it.getProperties(), DeviceModeStatusDetailResp.class);
                BeanUtils.copyProperties(properties, resp);
            }
            resp.setId(device.getId());
            resp.setDeviceName(device.getDeviceName());
            resp.setHwDeviceId(device.getHwDeviceId());
            resp.setDeviceType(device.getDeviceType());
            resp.setDeviceTypeName(device.getDeviceType().getName());
            //设置用户对设备的别名
            Optional<IntelligentOfficeDevice> intelligentOfficeDeviceOptional=intelligentOfficeDeviceDao.findByIntelligentOfficeIdAndDeviceIdAndUserHrId(
                    workingMode.getIntelligentOffice().getId(),device.getId(), workingMode.getUserHrId());
            if(intelligentOfficeDeviceOptional.isPresent()){
                IntelligentOfficeDevice officeDevice=intelligentOfficeDeviceOptional.get();
                if(StringUtils.isNotEmpty(officeDevice.getDeviceAlisa())){
                    resp.setDeviceName(officeDevice.getDeviceAlisa());
                }
                resp.setAdditionalName(StringUtils.isEmpty(officeDevice.getDeviceAlisaProperties()) ? DeviceNameEditReq.AdditionalName.empty() :
                        JSON.parseObject(officeDevice.getDeviceAlisaProperties(), DeviceNameEditReq.AdditionalName.class));
            }
            detailResps.add(resp);
        });

        WorkingModeDetailResp resp = new WorkingModeDetailResp();
        resp.setId(workingMode.getId());
        resp.setName(workingMode.getName());
        resp.setPurpose(workingMode.getPurpose());
        resp.setIsDefault(workingMode.getIsDefault());
        resp.setStatus(workingMode.getUsableStatus());
        resp.setImgUrl(workingMode.getImg());
        resp.setDetailResps(detailResps);

        return ServiceResult.ok(resp);
    }

    @Override
    public ServiceResult delete(String id) {

        Optional<DeviceWorkingMode> workingModeOp = workingModeDao.findById(id);
        if(workingModeOp.isEmpty()){
            return ServiceResult.error("id有误");
        }

        // 从token中解析员工工号
        String hrId = JwtUtil.getHrId(TokenUtils.getToken());
        Optional<DeviceWorkingMode> op = workingModeDao.findByIdAndUserHrId(id, hrId);
        if(op.isEmpty()){
            return ServiceResult.error("无权限");
        }
        DeviceWorkingMode deviceWorkingMode = op.get();

        // 删除模式下设备状态
        modeStatusDao.findByModeId(id).forEach(it ->{
            it.setIsDelete(1);
            modeStatusDao.save(it);
        });
        deviceWorkingMode.setIsDelete(1);
        workingModeDao.save(deviceWorkingMode);

        return ServiceResult.ok();
    }

    @Override
    public ServiceResult updateStatus(String id, UsableStatus status) {

        // 从token中解析员工工号
        String hrId = JwtUtil.getHrId(TokenUtils.getToken());

        Optional<DeviceWorkingMode> workingModeOp = workingModeDao.findByIdAndUserHrId(id, hrId);
        if(workingModeOp.isEmpty()){
            return ServiceResult.error("无权修改此模式");
        }
        DeviceWorkingMode deviceWorkingMode = workingModeOp.get();
        deviceWorkingMode.setUsableStatus(status);
        // 保存
        workingModeDao.save(deviceWorkingMode);

        return ServiceResult.ok();
    }

    @Override
    public ServiceResult update(String id, WorkingModeSaveReq req) {

        // 从token中解析员工工号
        String hrId = JwtUtil.getHrId(TokenUtils.getToken());
        if(StringUtils.isEmpty(req.getName()) || req.getName().length() > 20){
            return ServiceResult.error("场景名称不符合规范");
        }
        Optional<DeviceWorkingMode> workingModeOp = workingModeDao.findByIdAndUserHrId(id, hrId);
        if(workingModeOp.isEmpty()){
            return ServiceResult.error("无权修改此模式");
        }
        DeviceWorkingMode deviceWorkingMode = workingModeOp.get();

        // 判断名称是否使用
        Optional<DeviceWorkingMode> modeOp = workingModeDao.findBySpaceIdAndNameAndUserHrId(deviceWorkingMode.getIntelligentOffice().getSpace().getId(), req.getName(), hrId);
        if(modeOp.isPresent() && !modeOp.get().getId().equals(deviceWorkingMode.getId())){
            return ServiceResult.error("修改失败,该模式已存在");
        }

        deviceWorkingMode.setImg(req.getImgUrl());
        deviceWorkingMode.setName(req.getName());
        deviceWorkingMode.setPurpose(req.getPurpose());
        deviceWorkingMode.setUsableStatus(req.getUsableStatus());

        // 更新模式下设备
        List<DeviceModeStatus> list = new ArrayList<>();
        modeStatusDao.findByModeId(id).forEach(it ->{
            it.setIsDelete(1);
            list.add(it);
        });

        // 保存模式下对应设备
        for(DeviceModeStatusSaveReq modeStatusSaveReq : req.getSaveReqs()){

            Optional<IotDevice> deviceOp = deviceDao.findByHwDeviceId(modeStatusSaveReq.getDeviceId());
            if(deviceOp.isEmpty()){
                // 业务处理失败，手动回滚事务,返回异常
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error("设备不存在，绑定失败");
            }

            // 设备状态
            IotDevice device = deviceOp.get();
            DeviceModeStatus modeStatus = new DeviceModeStatus();
            modeStatus.setProperties(JSON.toJSONString(modeStatusSaveReq));
            modeStatus.setDevice(device);
            modeStatus.setMode(deviceWorkingMode);
            list.add(modeStatus);
        }
        modeStatusDao.saveAll(list);

        workingModeDao.save(deviceWorkingMode);

        return ServiceResult.ok();
    }

    @Override
    public PageBean<DefaultWorkingModePageResp> getDefaultWorkingModePage(String officeId, PageReq req) {

        // 从token中解析员工工号
        String hrId = JwtUtil.getHrId(TokenUtils.getToken());

        PageBean<DefaultWorkingModePageResp> pageBean = new PageBean<>();
        Pageable pageable = PageRequest.of(req.getPage(), req.getSize(), Sort.by(Sort.Direction.DESC, "createdTime"));

        Page<DeviceWorkingDefaultMode> all = defaultModeDao.findAll(pageable);

        List<DefaultWorkingModePageResp> list = new ArrayList<>();
        all.getContent().forEach(it ->{
            DefaultWorkingModePageResp resp = new DefaultWorkingModePageResp();
            resp.setId(it.getId());
            resp.setImgUrl(it.getImg());
            resp.setName(it.getName());
            resp.setPurpose(it.getPurpose());
            // 判断推荐场景是否被添加
            Optional<DeviceWorkingMode> workingModeOp = workingModeDao.findByIntelligentOfficeIdAndUserHrIdAndDefaultModeId(officeId, hrId, it.getId());
            if(workingModeOp.isEmpty()){
                resp.setAdd(false);
            }else {
                resp.setAdd(true);
            }
            list.add(resp);
        });

        pageBean.setList(list);
        pageBean.setPage(req.getPage());
        pageBean.setSize(req.getSize());
        pageBean.setTotalElements(all.getTotalElements());
        pageBean.setTotalPages(all.getTotalPages());
        return pageBean;
    }

    @Override
    @Transactional
    public ServiceResult managementCreate(ManagementSaveReq req) {

        Optional<IntelligentOffice> officeOp = officeDao.findById(req.getOfficeId());
        if(officeOp.isEmpty()){
            return ServiceResult.error("id有误，办公室不存在");
        }
        IntelligentOffice office = officeOp.get();

        String token = TokenUtils.getToken();
        // 添加人员空间内设备控制权限
        ServiceResult result = workingModeService.managementCreate(req.getSpaceId(), req.getEmpNos(), token, "1");
        if (!result.isSuccess()) {
            // 业务处理失败，手动回滚事务,返回异常
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return result;
        }
        List<String> spaceIds = new ArrayList<>();
        spaceIds.add(office.getSpace().getId());
        intelligentOfficeDeviceService.getListBySpace(spaceIds, office.getSpace());

        // 保存办公室用户权限
        for(String empNo : req.getEmpNos()){

            Optional<SpaceAdmin> spaceAdminOp = spaceAdminDao.findBySpaceIdAndAdminEmpNo(req.getSpaceId(), empNo);
            if(spaceAdminOp.isPresent()){
                // 业务处理失败，手动回滚事务,返回异常
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error("该成员已拥有权限，无需要重复添加:" + empNo);
            }

            /*if(officeUserOp.isPresent()){
                // 业务处理失败，手动回滚事务,返回异常
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error("该成员已拥有权限，无需要重复添加:" + empNo);
            }*/

            try {
                // 保存人员权限
                SysUser userByEmpNo = sysUserServiceUC.getUserByEmpNo(empNo);
                spaceIds.forEach(spaceId ->{
                    Optional<IntelligentOffice> officeOptional = officeDao.findBySpaceId(spaceId);
                    if(officeOptional.isPresent() && officeUserDao.findByIntelligentOfficeIdAndUserHrId(officeOptional.get().getId(), empNo).isEmpty()){
                            IntelligentOfficeUser officeUser = new IntelligentOfficeUser();
                            officeUser.setIntelligentOffice(officeOptional.get());
                            officeUser.setInvitedUserHrId(JwtUtil.getHrId(token));
                            officeUser.setUserHrId(userByEmpNo.getEmpNo());
                            officeUser.setUserName(userByEmpNo.getNameZh());
                            officeUserDao.save(officeUser);
                    }
                });
            }catch (Exception e){
                log.error(e.getMessage());
                // 业务处理失败，手动回滚事务,返回异常
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error("添加失败,人员工号 " + empNo);
            }

        }

        return ServiceResult.ok();
    }

    @Override
    public PageBean<ManagementPageResp> managementPage(String id, int page, int size) {

        PageBean<ManagementPageResp> pageBean = new PageBean<>();
        List<ManagementPageResp> list = new ArrayList<>();

        Optional<IntelligentOffice> officeOp = officeDao.findById(id);
        if(officeOp.isEmpty()){
            pageBean.setList(list);
            return pageBean;
        }

        // 获取空间管理员信息
        spaceAdminDao.findBySpaceId(officeOp.get().getSpace().getId()).forEach(admin ->{
            ManagementPageResp resp = new ManagementPageResp();
            resp.setEmpNo(admin.getAdminEmpNo());
            resp.setName(admin.getAdminName());
            resp.setIsAdmin(1);
            try {
                // 保存人员权限
                SysUser sysUser = sysUserServiceUC.getUserByEmpNo(admin.getAdminEmpNo());
                resp.setImg(sysUser.getHeadImgUrl());
                resp.setDept(sysUser.getFullDepartmentName());
            }catch (Exception e){
                log.error(e.getMessage());
                log.error("获取人员信息失败,人员工号 " + admin.getAdminEmpNo());
            }
            list.add(resp);
        });

        // 获取办公室权限人员信息
        officeUserDao.findByIntelligentOfficeIdOrderByCreatedTimeDesc(id).forEach( officeUser ->{
            ManagementPageResp resp = new ManagementPageResp();
            resp.setEmpNo(officeUser.getUserHrId());
            resp.setName(officeUser.getUserName());
            resp.setIsAdmin(0);
            try {
                // 保存人员权限
                SysUser sysUser = sysUserServiceUC.getUserByEmpNo(officeUser.getUserHrId());
                resp.setImg(sysUser.getHeadImgUrl());
                resp.setDept(sysUser.getFullDepartmentName());
            }catch (Exception e){
                log.error(e.getMessage());
                log.error("获取人员信息失败,人员工号 " + officeUser.getUserHrId());
            }
            list.add(resp);
        });
        return PageBean.createPageBeanByAllData(list, page, size);
    }

    @Override
    @Transactional
    public ServiceResult managementDelete(String id, List<String> empNos) {

        if(empNos.isEmpty()){
            return ServiceResult.error("请选中人员");
        }

        // 获取工号
        String hrId = JwtUtil.getHrId(TokenUtils.getToken());
        Optional<IntelligentOffice> officeOp = officeDao.findById(id);
        if(officeOp.isEmpty()){
            return ServiceResult.error("id有误，办公室不存在");
        }

        Optional<SpaceAdmin> spaceAdminOp = spaceAdminDao.findBySpaceIdAndAdminEmpNo(officeOp.get().getSpace().getId(), hrId);
        if(spaceAdminOp.isEmpty()){
            return ServiceResult.error("无权限");
        }

        for(String empNo : empNos){
            Optional<IntelligentOfficeUser> officeUserOp = officeUserDao.findByIntelligentOfficeIdAndUserHrId(id, empNo);
            if(officeUserOp.isPresent()){

                // 删除人员设备权限
                ServiceResult result = workingModeService.managementDelete(officeOp.get().getSpace().getId(), empNos, TokenUtils.getToken());
                if(!result.isSuccess()){
                    // 业务处理失败，手动回滚事务,返回异常
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return result;
                }

                IntelligentOfficeUser officeUser = officeUserOp.get();
                officeUser.setIsDelete(1);
                officeUserDao.save(officeUser);
            }else {
                // 业务处理失败，手动回滚事务,返回异常
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error("删除失败,人员不存在");
            }
        }

        return ServiceResult.ok();
    }

    @Override
    @Transactional(isolation=Isolation.SERIALIZABLE)
    public ServiceResult controlWorkingMode(String id) {

        Optional<DeviceWorkingMode> workingModeOp = workingModeDao.findById(id);
        if(workingModeOp.isEmpty()){
            return ServiceResult.error("控制失败,模式不存在");
        }
        DeviceWorkingMode workingMode = workingModeOp.get();

        // 判断权限
        String hrId = JwtUtil.getHrId(TokenUtils.getToken());
        String name = "";
        try {
            // 保存人员权限
            name = sysUserServiceUC.getUserByEmpNo(hrId).getNameZh();
        }catch (Exception e){
            log.error(e.getMessage());
            log.error("获取人员信息失败");
        }
        Optional<SpaceAdmin> spaceAdminOp = spaceAdminDao.findBySpaceIdAndAdminEmpNo(workingMode.getIntelligentOffice().getSpace().getId(), hrId);
        Optional<IntelligentOfficeUser> officeUserOp = officeUserDao.findByIntelligentOfficeIdAndUserHrId(workingMode.getIntelligentOffice().getId(), hrId);
        if(spaceAdminOp.isEmpty() && officeUserOp.isEmpty()){
            return ServiceResult.error("无权限");
        }
        if(officeUserOp.isPresent() && !officeUserOp.get().getInvitedUserHrId().equals(workingMode.getUserHrId())){
            return ServiceResult.error("无权限");
        }

        // 控制时间
        workingMode.setControlTime(LocalDateTime.now());
        workingModeDao.save(workingMode);

        // 循环控制设备
        StringBuilder errorMsg = new StringBuilder();
        List<DeviceModeStatus> list = modeStatusDao.findByModeId(id);
        for(DeviceModeStatus it : list){
            IotDevice device = it.getDevice();

            if(!DeviceStatus.ONLINE.equals(device.getHwStatus())){
                errorMsg.append(it.getDevice().getHwDeviceName()).append(",");
                continue;
            }

            // 获取设备所属产品的服务ID和命令名称
            ServiceResult serviceResult = productsService.findProductProperties(device);
            if(!serviceResult.isSuccess()){
                errorMsg.append(it.getDevice().getHwDeviceName()).append(",");
                continue;
            }
            DoorMagneticHWReq hwReq = (DoorMagneticHWReq) serviceResult.getObject();
            hwReq.setMode(workingMode.getName());

            DoorMagneticHWParasReq paras = JSON.parseObject(it.getProperties(), DoorMagneticHWParasReq.class);
            String hwDeviceId = device.getHwDeviceId();
            // 下发控制设备
            hwReq.setDeviceId(device.getId());
            hwReq.setDevice_id(hwDeviceId);
            hwReq.setParas(paras);

            // 空调
            if(DeviceType.AIR_CONDITIONER.equals(device.getDeviceType())){
                ServiceResult result = productsService.command(hwReq, hrId, name);
                if (!result.isSuccess()) {
                    errorMsg.append(it.getDevice().getHwDeviceName()).append(",");
                    continue;
                }
            }

            // 窗帘
            if(DeviceType.CURTAINS.equals(device.getDeviceType())){
                ServiceResult result = productsService.command(hwReq, hrId, name);
                if (!result.isSuccess()) {
                    errorMsg.append(it.getDevice().getHwDeviceName()).append(",");
                    continue;
                }
            }

            // 门磁
            if(DeviceType.DOOR_MAGNETIC.equals(device.getDeviceType())){
                ServiceResult result = productsService.command(hwReq, hrId, name);
                if (!result.isSuccess()) {
                    errorMsg.append(it.getDevice().getHwDeviceName()).append(",");
                    continue;
                }
            }

            // 四路开关
            if(DeviceType.FOUR_WAY_SWITCH.equals(device.getDeviceType())){
                ServiceResult result = productsService.updateSwitch(hwReq, hrId, name);
                if (!result.isSuccess()) {
                    errorMsg.append(it.getDevice().getHwDeviceName()).append(",");
                }
            }

        }

        if(StringUtils.isNotBlank(errorMsg.toString())){
            return ServiceResult.error("控制失败设备" + errorMsg.substring(0, errorMsg.length() - 1));
        }

        return ServiceResult.ok();
    }

    @Override
    public ServiceResult creatDefaultWorkingModeToMyWorkingMode(CreatDefaultWorkingModeToMyWorkingModeReq req) {

        // 获取工号
        String hrId = JwtUtil.getHrId(TokenUtils.getToken());

        Optional<IntelligentOffice> officeOp = officeDao.findById(req.getOfficeId());
        if(officeOp.isEmpty()){
            return ServiceResult.error("添加失败,办公室不存在");
        }
        IntelligentOffice office = officeOp.get();

        Optional<SpaceAdmin> spaceAdminOp = spaceAdminDao.findBySpaceIdAndAdminEmpNo(office.getSpace().getId(), hrId);
        if(spaceAdminOp.isEmpty()){
            return ServiceResult.error("无权限");
        }

        Optional<DeviceWorkingDefaultMode> defaultModeOp = defaultModeDao.findById(req.getDefaultWorkingModeId());
        if(defaultModeOp.isEmpty()){
            return ServiceResult.error("添加失败默认模式不存在");
        }
        DeviceWorkingDefaultMode defaultMode = defaultModeOp.get();

        Optional<DeviceWorkingMode> deviceWorkingModeOp = workingModeDao.findByIntelligentOfficeIdAndUserHrIdAndDefaultModeId(office.getId(), hrId, defaultMode.getId());
        if(deviceWorkingModeOp.isPresent()){
            return ServiceResult.error("该模式已被添加");
        }

        Optional<DeviceWorkingMode> deviceWorkingModeOp1 = workingModeDao.findBySpaceIdAndNameAndUserHrId(office.getSpace().getId(), defaultMode.getName(), hrId);
        if(deviceWorkingModeOp1.isPresent()){
            return ServiceResult.error("添加失败,名称已被使用");
        }

        // 获取用户办公室下可用设备
        List<IntelligentOfficeDevice> officeDeviceList = officeDeviceDao.findByIntelligentOfficeIdAndUserHrIdAndUsableStatus(office.getId(), hrId, UsableStatus.ENABLE);
        // 获取默认模式下设备状态
        List<DeviceModeDefaultStatus> deviceModeDefaultStatusList = deviceModeDefaultStatusDao.findByModeId(defaultMode.getId());
        // 保存工作模式
        DeviceWorkingMode workingMode = new DeviceWorkingMode();
        workingMode.setName(defaultMode.getName());
        workingMode.setImg(defaultMode.getImg());
        workingMode.setPurpose(defaultMode.getPurpose());
        workingMode.setSpace(office.getSpace());
        workingMode.setIntelligentOffice(office);
        workingMode.setUsableStatus(UsableStatus.ENABLE);
        workingMode.setUserHrId(hrId);
        workingMode.setDefaultMode(defaultMode);
        workingModeDao.save(workingMode);

        // 保存模式下对应设备
        deviceModeDefaultStatusList.forEach(defaultStatus -> officeDeviceList.forEach(officeDevice ->{
            if(defaultStatus.getDeviceType().equals(officeDevice.getDevice().getDeviceType())){
                DeviceModeStatus deviceModeStatus = new DeviceModeStatus();
                deviceModeStatus.setProperties(defaultStatus.getProperties());
                deviceModeStatus.setDevice(officeDevice.getDevice());
                deviceModeStatus.setMode(workingMode);
                modeStatusDao.save(deviceModeStatus);
            }
        }));
        return ServiceResult.ok();
    }
}
