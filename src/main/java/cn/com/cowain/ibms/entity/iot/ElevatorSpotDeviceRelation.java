package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/3/31 14:25
 */
@Data
@Entity
@Table(name = "iot_elevator_spot_device_relation")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_elevator_spot_device_relation", comment = "梯控点设备关联表")
public class ElevatorSpotDeviceRelation extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "elevator_spot_id", nullable =  false, columnDefinition = "varchar(36) COMMENT '梯控点Id'")
    private ElevatorSpot elevatorSpot;

    @ManyToOne
    @JoinColumn(name = "device_id", nullable =  false, columnDefinition = "varchar(36) COMMENT '设备Id'")
    private IotDevice device;
}
