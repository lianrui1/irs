package cn.com.cowain.ibms.service.qrcode;

import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.enumeration.iot.DoorPlateStatus;
import cn.com.cowain.ibms.rest.req.iot.QrCodeReq;
import cn.com.cowain.ibms.service.bean.ServiceResult;

/**
 * 二维码逻辑信息操作类
 *
 * @author Yang.Lee
 * @date 2020/12/2 13:50
 */
public interface QrCodeService {

    /**
     * 创建普通二维码
     *
     * @param content
     * @return
     */
    ServiceResult create(final String content);

    /**
     * 扫码开门
     *
     * @param qrCodeReq
     * @return
     * @author Yang.Lee
     */
    ServiceResult scan(QrCodeReq qrCodeReq);

    /**
     * 判断扫码人是否是会议参与人
     *
     * @param recordId 会议id
     * @param stId     员工id
     * @return
     * @author Yang.Lee
     */
    boolean isStaffJoin(String recordId, String stId);

    /**
     * 扫描静态二维码开门
     *
     * @param doorMagnetismNo 门磁编号
     * @param empNo           员工工号
     * @return 开门结果
     * @author Yang.Lee
     * @date 2021/3/22 15:24
     **/
    ServiceResult scanStaticQRCode(String doorMagnetismNo, String empNo);


    /**
     * 访客扫码开门
     *
     * @param qrCodeReq 请求参数
     * @return 开门结果
     * @author Yang.Lee
     * @date 2021/7/1 15:31
     **/
    ServiceResult guestScan(QrCodeReq qrCodeReq);

    /**
     * 访客扫码开门（静态码）
     * @param qrCodeReq 二维码信息
     * @return 开门结果
     * @author Yang.Lee
     * @date 2021/12/14 9:29
     **/
    ServiceResult gustScanStaticQrcode(QrCodeReq qrCodeReq);

    /**
     * 验证扫码人身份
     *
     * @param empNo         工号
     * @param status
     * @param currentRecord
     * @param nextRecord
     * @return
     */
    boolean checkScanner(String empNo, DoorPlateStatus status, ReservationRecord currentRecord, ReservationRecord nextRecord);

    /**
     * 会议签到
     *
     * @param roomId   会议室ID
     * @param userHrId 用户工号
     * @return 签到结果
     * @author Yang.Lee
     * @date 2022/1/19 10:54
     **/
    ServiceResult meetingSign(String roomId, String userHrId);
}
