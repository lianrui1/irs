package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.AlertSpot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author wei.cheng
 * @date 2022/03/29 13:16
 */
public interface AlertSpotDao extends JpaRepository<AlertSpot, String>, JpaSpecificationExecutor<AlertSpot> {
    /**
     * 查询deviceId = {@code deviceId}的AlertSpot列表
     *
     * @param deviceId
     * @return
     */
    List<AlertSpot> findAllByDeviceId(String deviceId);

    /**
     * 查询该空间下的所有警戒点
     *
     * @author: yanzy
     * @param: [spaceId]
     * @data: 2022/4/13 10:04:39
     */
    List<AlertSpot> findBySpaceId(String spaceId);
}
