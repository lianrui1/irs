package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.HwProductProperties;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface HwProductPropertiesDao extends JpaRepository<HwProductProperties, String>, JpaSpecificationExecutor<HwProductProperties> {

    Optional<HwProductProperties> findByHwProductIdAndDeviceType(String hwProductId, DeviceType deviceType);
}
