package cn.com.cowain.ibms.service.iot;

import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.PageReq;
import cn.com.cowain.ibms.rest.req.iot.DeviceGroupEditReq;
import cn.com.cowain.ibms.rest.req.space.DevicePageReq;
import cn.com.cowain.ibms.rest.resp.iot.DeviceGroupDetailResp;
import cn.com.cowain.ibms.rest.resp.iot.DeviceGroupTreeResp;
import cn.com.cowain.ibms.rest.resp.iot.IotDeviceResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/10/21 13:27
 */
public interface DeviceGroupService {

    /**
     * 创建会议联动
     *
     * @param req 请求参数
     * @return 创建结果，创建成功后object中包含新数据的id
     * @author Yang.Lee
     * @date 2021/10/21 13:27
     **/
    ServiceResult create(DeviceGroupEditReq req);

    /**
     * 获取设备联动数据树
     *
     * @return 联动树
     * @author Yang.Lee
     * @date 2021/10/21 15:52
     **/
    List<DeviceGroupTreeResp> getTree();

    /**
     * 香群组中添加设备
     *
     * @param deviceGroupId 群组ID
     * @param deviceIdList  设备ID集合
     * @return 添加结果
     * @author Yang.Lee
     * @date 2021/10/21 16:23
     **/
    ServiceResult addDevice(String deviceGroupId, List<String> deviceIdList);

    /**
     * 获取设备群组详情
     *
     * @param groupId 群组ID
     * @param req     分页参数
     * @return 详情
     * @author Yang.Lee
     * @date 2021/10/21 17:02
     **/
    DeviceGroupDetailResp get(String groupId, PageReq req);

    /**
     * 删除设备群组
     *
     * @param groupId 群组ID
     * @return 删除结果
     * @author Yang.Lee
     * @date 2021/10/21 18:55
     **/
    ServiceResult delete(String groupId);

    /**
     * 从群组中移除设备
     *
     * @param groupId  群组ID
     * @param deviceId 设备ID
     * @return 操作结果
     * @author Yang.Lee
     * @date 2021/10/25 9:21
     **/
    ServiceResult removeDevice(String groupId, String deviceId);

    /**
     * 编辑设备群组
     *
     * @param groupId 群组ID
     * @param req     编辑参数
     * @return 操作结果
     * @author Yang.Lee
     * @date 2021/10/25 9:34
     **/
    ServiceResult edit(String groupId, DeviceGroupEditReq req);

    /**
     * 查询所有设备信息
     *
     * @param req 查询参数
     * @return 结果
     * @author Yang.Lee
     * @date 2021/10/25 13:38
     **/
    PageBean<IotDeviceResp> getAllDevice(DevicePageReq req);
}
