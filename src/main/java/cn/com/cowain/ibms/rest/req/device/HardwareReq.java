package cn.com.cowain.ibms.rest.req.device;

import cn.com.cowain.ibms.enumeration.IotNodeType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 硬件数据请求实体
 *
 * @author Yang.Lee
 * @date 2021/3/17 16:07
 */
@Data
@ApiModel("硬件数据请求对象")
public class HardwareReq {

    @Length(max = 20)
    @NotBlank
    @ApiModelProperty(value = "硬件名称（中文）", required = true, example = "人脸识别设备", position = 1)
    private String nameZh;

    @Length(max = 20)
    @NotBlank
    @ApiModelProperty(value = "硬件名称（英文）", required = true, example = "DEVICE A", position = 2)
    private String nameEn;

    @NotNull
    @ApiModelProperty(value = "设备类型", required = true, example = "DOOR_PLATE", position = 3)
    private IotNodeType deviceType;

    @NotBlank
    @ApiModelProperty(value = "产品ID", required = true, example = "00112233a", position = 4)
    private String productId;

    @Length(max = 20)
    @ApiModelProperty(value = "品牌", example = "旷世", position = 5)
    private String brand;

    @Length(max = 30)
    @ApiModelProperty(value = "型号", example = "MegEye-W5K-I8", position = 6)
    private String model;

    @ApiModelProperty(value = "硬件sn码", required = true, position = 7)
    private String hardWareSn;
}
