package cn.com.cowain.ibms.rest.resp.qrcode;

import cn.com.cowain.ibms.rest.resp.BaseListResp;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 二维码通行权限响应对象
 * @author Yang.Lee
 * @date 2021/3/12 16:21
 */
@Data
@Builder
@ApiModel("二维码通行权限查询结果对象")
public class QrcodeAccessRuleDetailResp {

    @ApiModelProperty(value = "权限ID", required = true, example = "123abc", position = 1)
    private String id;

    @ApiModelProperty(value = "项目ID", required = true, example = "456def", position = 2)
    private String projectId;

    @ApiModelProperty(value = "项目名称", required = true, example = "昆山工厂", position = 3)
    private String projectName;

    @ApiModelProperty(value = "空间ID", example = "789hij", position = 4)
    private String spaceId;

    @ApiModelProperty(value = "空间名称", example = "789hij", position = 5)
    private String spaceName;

    @ApiModelProperty(value = "设备ID", required = true, example = "0123iop", position = 6)
    private String magneticDeviceId;

    @ApiModelProperty(value = "设备名称", required = true, example = "人脸识别A", position = 7)
    private String magneticDeviceName;

    @ApiModelProperty(value = "人员组", required = true, position = 8)
    private List<BaseListResp> staffGroupList;

    @ApiModelProperty(value = "时间计划ID", required = true, example = "345mnq", position = 9)
    private String timePlanId;

    @ApiModelProperty(value = "时间计划名称", required = true, example = "默认时间计划", position = 10)
    private String timePlanName;

    @ApiModelProperty(value = "设备位置", required = true, example = "设备位置", position = 11)
    private String deviceAddress;
}
