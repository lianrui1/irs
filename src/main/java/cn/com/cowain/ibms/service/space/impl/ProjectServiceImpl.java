package cn.com.cowain.ibms.service.space.impl;

import cn.com.cowain.ibms.dao.AreaDao;
import cn.com.cowain.ibms.dao.RoomDao;
import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.dao.iot.SpaceDao;
import cn.com.cowain.ibms.dao.space.ProjectAdminDao;
import cn.com.cowain.ibms.dao.space.ProjectAreaDao;
import cn.com.cowain.ibms.dao.space.ProjectDao;
import cn.com.cowain.ibms.entity.Area;
import cn.com.cowain.ibms.entity.Room;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.space.Project;
import cn.com.cowain.ibms.entity.space.ProjectAdmin;
import cn.com.cowain.ibms.entity.space.ProjectArea;
import cn.com.cowain.ibms.enumeration.space.ProjectPropertyRightType;
import cn.com.cowain.ibms.exceptions.DataNotFoundException;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.Sorts;
import cn.com.cowain.ibms.rest.req.SysUserBean;
import cn.com.cowain.ibms.rest.req.space.ProjectAreaEditReq;
import cn.com.cowain.ibms.rest.req.space.ProjectPageReq;
import cn.com.cowain.ibms.rest.req.space.ProjectReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.space.ProjectAreaResp;
import cn.com.cowain.ibms.rest.resp.space.ProjectResp;
import cn.com.cowain.ibms.rest.resp.space.SpaceDetailResp;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.ServiceMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.space.ProjectService;
import cn.com.cowain.ibms.utils.ErrConst;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 项目管理逻辑处理实现类
 *
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2020/12/21 13:40
 */
@Slf4j
@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private AreaDao areaDao;

    @Autowired
    private SpaceDao spaceDao;

    @Resource
    private DeviceDao deviceDao;

    @Autowired
    private RoomDao roomDao;

    @Resource
    private ProjectAreaDao projectAreaDao;

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserService;

    @Resource
    private ProjectAdminDao projectAdminDao;

    /*
     *查询所有项目列表
     */
    @Override
    @Transactional
    public List<ProjectResp> searchProjectList() {

        List<Project> projectList = projectDao.findByNoNotOrderByCreatedTimeDesc(DEFAULT_PROJECT_NUMBER);

        List<ProjectResp> projectSearchRespList = new ArrayList<>();
        ProjectResp projectSearchResp = null;

        for (Project project : projectList) {
            projectSearchResp = new ProjectResp();
            BeanUtils.copyProperties(project, projectSearchResp);
            //查看改项目下是否有空间
            String id = project.getId();
            Set<Space> space = spaceDao.findByProjectIdAndIsShow(id, 1);
            Integer spaceSize = space.size();
            if (spaceSize != 0) {
                projectSearchResp.setIncludeSpace(1);
            } else {
                projectSearchResp.setIncludeSpace(0);
            }
            projectSearchRespList.add(projectSearchResp);
        }
        return projectSearchRespList;
    }

    /**
     * 查询项目列表（分页）
     *
     * @param req 请求参数
     * @return 项目列表
     * @author Yang.Lee
     * @date 2021/11/11 14:15
     **/
    @Override
    @Transactional
    public PageBean<ProjectResp> search(ProjectPageReq req) {
        Sort sort = Sort.by(Sort.Direction.DESC, Sorts.CREATED_TIME);
        Pageable pageable = PageRequest.of(req.getPage(), req.getSize(), sort);
        Page<Project> projectPage = projectDao.findAll((root, query, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();

            if (StringUtils.isNotBlank(req.getProjectName())) {
                list.add(criteriaBuilder.like(root.get("projectName"), "%" + req.getProjectName() + "%"));
            }

            if (StringUtils.isNotBlank(req.getProjectAreaId())) {
                list.add(criteriaBuilder.equal(root.get("projectArea").get("id"), req.getProjectAreaId()));
            }

            return criteriaBuilder.and(list.toArray(new Predicate[0]));

        }, pageable);
        PageBean<ProjectResp> pageBean = new PageBean<>();
        //当前页
        pageBean.setPage(pageable.getPageNumber());
        //每页记录数
        pageBean.setSize(pageable.getPageSize());
        //总记录数
        pageBean.setTotalElements(projectPage.getTotalElements());
        //总页数
        pageBean.setTotalPages(projectPage.getTotalPages());

        List<ProjectResp> list = new ArrayList<>();
        //循环
        projectPage.getContent().forEach(project -> {
            ProjectResp projectResp = new ProjectResp();
            BeanUtils.copyProperties(project, projectResp);
            //设置 更新时间
            projectResp.setUpdateTime(project.getUpdatedTime());
            if (null != projectResp.getUpdateTime()) {
                projectResp.setCreatedTime(projectResp.getUpdateTime());
            }
            projectResp.setProjectArea(project.getProjectArea().getName());
            projectResp.setPropertyRightTypeName(Optional.ofNullable(project.getPropertyRightType()).map(ProjectPropertyRightType::getName).orElse(""));

            // 找出所有的一级空间
            List<Space> spaceL1List = spaceDao.findByProjectIdAndLevel(project.getId(), 1);
            projectResp.setSpaceL1(spaceL1List.stream()
                    .map(obj ->
                            SpaceDetailResp.convert(obj))
                    .sorted(Comparator.comparing(SpaceDetailResp::getCreateTime))
                    .collect(Collectors.toList()));
            list.add(projectResp);
        });

        pageBean.setList(list);

        return pageBean;
    }

    /**
     * 新增项目信息
     *
     * @param projectReq
     * @return
     */
    @Override
    @Transactional
    public ServiceResult save(ProjectReq projectReq) {
        // 1. 查询是否有相同名称的项目
        Optional<Project> projectOptional = projectDao.findByProjectName(projectReq.getProjectName());
        if (projectOptional.isPresent()) {
            return ServiceResult.error("该项目名称已被使用");
        }
        Optional<ProjectArea> projectAreaOptional = projectAreaDao.findById(projectReq.getProjectArea());
        if (projectAreaOptional.isEmpty()) {
            return ServiceResult.error("区域ID不存在", ErrConst.E01);
        }

        List<SysUser> adminList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(projectReq.getAdminHrIdList())) {
            // 验证用户是否存在
            for (String adminHrId : projectReq.getAdminHrIdList()) {
                SysUser sysUser = sysUserService.getUserByEmpNo(adminHrId);
                if (sysUser == null) {
                    return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_STAFF, adminHrId), ErrConst.E01);
                }
                adminList.add(sysUser);
            }
        }
        // 2. 创建一个 Project
        Project project = new Project();
        if (StringUtils.isEmpty(projectReq.getVisitCompany())) {
            project.setVisitCompany("");
        }
        //深拷贝
        BeanUtils.copyProperties(projectReq, project);
        //设置项目编号  省级ID+市ID+区ID+两位数递增
        //获取省级编号
        String provinceId = projectReq.getProvinceId();
        Long idProvince = Long.parseLong(provinceId);
        Optional<Area> byProvinceId = areaDao.findById(idProvince);
        if (!byProvinceId.isPresent()) {
            return ServiceResult.error("未找到省数据");
        }
        Area areaProvinceId = byProvinceId.get();
        String provinceCode = areaProvinceId.getCode().substring(areaProvinceId.getCode().length() - 2);
        //获取市级编号
        String cityId = projectReq.getCityId();
        Long idCity = Long.parseLong(cityId);
        Optional<Area> byCityId = areaDao.findById(idCity);
        if (!byCityId.isPresent()) {
            return ServiceResult.error("未找到市数据");
        }
        Area areaCity = byCityId.get();
        String cityCode = areaCity.getCode().substring(areaCity.getCode().length() - 2);
        //获取区级编号
        String districtId = projectReq.getDistrictId();
        Long idDistrict = Long.parseLong(districtId);
        Optional<Area> byDistrictId = areaDao.findById(idDistrict);
        if (!byDistrictId.isPresent()) {
            return ServiceResult.error("未找到区数据");
        }
        Area areaDistrict = byDistrictId.get();
        String districtCode = areaDistrict.getCode().substring(areaDistrict.getCode().length() - 2);
        //获取数据库最大项目编号
        List<Project> projectList = projectDao.findAll();
        if (projectList.isEmpty()) {
            project.setNo(provinceCode + cityCode + districtCode + "10");
        } else {
            Sort sort = Sort.by(Sort.Direction.DESC, Sorts.CREATED_TIME);
            List<Project> projectMaxCreatedTime = projectDao.findAll(sort);
            if (!projectMaxCreatedTime.isEmpty()) {
                Project projectMax = projectMaxCreatedTime.get(0);
                int projectNo = Integer.parseInt(projectMax.getNo().substring(projectMax.getNo().length() - 2));
                int no = ++projectNo;
                project.setNo(provinceCode + cityCode + districtCode.concat(Integer.toString(no)));
            }
        }

        project.setProjectArea(projectAreaOptional.get());
        project.setAreaM2(projectReq.getAreaM2());
        project.setVisitCompany(projectReq.getVisitCompany());
        project.setPropertyRightType(projectReq.getPropertyRightType());
        project.setIntroduction(projectReq.getIntroduction());
        project.setPicture(projectReq.getImg());

        //保存
        projectDao.save(project);

        // 保存项目管理员数据
        projectAdminDao.saveAll(adminList.stream().map(obj -> {

            ProjectAdmin projectAdmin = new ProjectAdmin();
            projectAdmin.setProject(project);
            projectAdmin.setAdminEmpNo(obj.getEmpNo());

            return projectAdmin;
        }).collect(Collectors.toList()));

        return ServiceResult.ok(IdResp.builder().id(project.getId()).build());
    }

    /**
     * 根据项目ID查询项目信息
     *
     * @param id
     * @return
     */
    @Override
    @Transactional
    public ServiceResult get(String id) {
        Optional<Project> projectOptional = projectDao.findById(id);
        if (!projectOptional.isPresent()) {
            return ServiceResult.error("项目ID错误，未查询到结果");
        }

        Project project = projectOptional.get();
        ProjectResp projectResp = new ProjectResp();
        BeanUtils.copyProperties(project, projectResp);

        // 查询空间管理员信息
        List<ProjectAdmin> projectAdminList = projectAdminDao.findByProjectId(project.getId());
        projectResp.setAdminList(projectAdminList.stream()
                .map(obj -> {
                    SysUser sysUser = sysUserService.getUserByEmpNo(obj.getAdminEmpNo());
                    SysUserBean sysUserBean = new SysUserBean();
                    sysUserBean.setEmpNo(sysUser.getEmpNo());
                    sysUserBean.setNameZh(sysUser.getNameZh());
                    sysUserBean.setDeptName(sysUser.getDeptName());
                    return sysUserBean;
                })
                .collect(Collectors.toList())
        );

        projectResp.setProjectArea(project.getProjectArea().getName());
        projectResp.setProjectAreaId(project.getProjectArea().getId());
        projectResp.setPropertyRightTypeName(Optional.ofNullable(project.getPropertyRightType()).map(ProjectPropertyRightType::getName).orElse(""));
        return ServiceResult.ok(projectResp);
    }

    /**
     * 根据项目名称或者编号模糊查询
     *
     * @return
     */
    @Override
    @Transactional
    public List<ProjectResp> findProjectList(String projectName) {
        // 根据项目编号字段no排序
        Sort sort = Sort.by(Sort.Direction.DESC, Sorts.CREATED_TIME);

        List<Project> projectList = projectDao.findAll((root, criteriaQuery, criteriaBuilder) -> {

            List<Predicate> predicateList = new ArrayList<>();
            Predicate and = criteriaBuilder.and(criteriaBuilder.notEqual(root.get("no"), DEFAULT_PROJECT_NUMBER));
            predicateList.add(and);

            if (StringUtils.isNotEmpty(projectName)) {

                String like = "%" + projectName + "%";
                predicateList.add(criteriaBuilder.or(criteriaBuilder.like(root.get("projectName"), like), criteriaBuilder.like(root.get("no"), like)));
            }

            return criteriaQuery.where(predicateList.toArray(new Predicate[0])).getRestriction();

        }, sort);


        List<ProjectResp> projectSearchRespList = new ArrayList<>();
        ProjectResp projectSearchResp = null;

        for (Project project : projectList) {
            projectSearchResp = new ProjectResp();
            BeanUtils.copyProperties(project, projectSearchResp);
            //查看改项目下是否有空间
            String id = project.getId();
            Set<Space> space = spaceDao.findByProjectIdAndIsShow(id, 1);
            Integer spaceSize = space.size();
            if (spaceSize != 0) {
                projectSearchResp.setIncludeSpace(1);
            } else {
                projectSearchResp.setIncludeSpace(0);
            }
            projectSearchRespList.add(projectSearchResp);
        }
        return projectSearchRespList;
    }

    @Override
    public ServiceResult update(String id, ProjectReq projectReq) {
        // 1.先判断是否有记录
        Optional<Project> projectOptional = projectDao.findByProjectName(projectReq.getProjectName());
        //如果存在记录
        if (projectOptional.isPresent()) {
            Project projectTemp = projectOptional.get();
            //并且记录id不是当前id
            if (!id.equals(projectTemp.getId())) {
                return ServiceResult.error("该项目名称已被使用", ErrConst.E01);
            }
        }

        Optional<ProjectArea> projectAreaOptional = projectAreaDao.findById(projectReq.getProjectArea());
        if (projectAreaOptional.isEmpty()) {
            return ServiceResult.error("区域ID不存在", ErrConst.E01);
        }

        if (!CollectionUtils.isEmpty(projectReq.getAdminHrIdList())) {
            // 验证用户是否存在
            for (String adminHrId : projectReq.getAdminHrIdList()) {
                SysUser sysUser = sysUserService.getUserByEmpNo(adminHrId);
                if (sysUser == null) {
                    return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_STAFF, adminHrId), ErrConst.E01);
                }
            }
        }

        // 2.更新 Project 属性
        Project project = projectDao.findById(id).orElseThrow(() -> new DataNotFoundException("project id:" + id));
        //更新项目名称
        project.setProjectName(projectReq.getProjectName());
        //更新项目详细地址
        project.setAddress(projectReq.getAddress());

        // 更新项目区域
        project.setProjectArea(projectAreaOptional.get());

        project.setProvinceId(projectReq.getProvinceId());
        project.setProjectName(projectReq.getProjectName());
        project.setCityId(projectReq.getCityId());
        project.setCityName(projectReq.getCityName());
        project.setDistrictId(projectReq.getDistrictId());
        project.setDistrictName(projectReq.getDistrictName());

        project.setPropertyRightType(projectReq.getPropertyRightType());
        project.setAreaM2(projectReq.getAreaM2());
        project.setIntroduction(projectReq.getIntroduction());
        project.setPicture(projectReq.getImg());

        //保存
        projectDao.save(project);

        // 更改管理员信息
        List<ProjectAdmin> projectAdminList = projectAdminDao.findByProjectId(project.getId());

        Set<String> dbAdminHrIdSet = projectAdminList.stream().map(ProjectAdmin::getAdminEmpNo).collect(Collectors.toSet());
        Set<String> reqAdminHrIdSet = new HashSet<>(projectReq.getAdminHrIdList());

        // 找出所有需要删除的数据
        Set<String> delSet = new HashSet<>(dbAdminHrIdSet);
        delSet.removeAll(reqAdminHrIdSet);

        List<ProjectAdmin> delList = projectAdminList.stream()
                .filter(obj -> delSet.contains(obj.getAdminEmpNo()))
                .map(obj -> {
                    obj.setIsDelete(1);
                    return obj;
                })
                .collect(Collectors.toList());
        projectAdminDao.saveAll(delList);

        // 找到所有需要添加的数据
        Set<String> addSet = new HashSet<>(reqAdminHrIdSet);
        addSet.removeAll(dbAdminHrIdSet);

        List<ProjectAdmin> addList = addSet.stream().map(obj -> {
            ProjectAdmin projectAdmin = new ProjectAdmin();
            projectAdmin.setProject(project);
            projectAdmin.setAdminEmpNo(obj);
            return projectAdmin;
        }).collect(Collectors.toList());

        projectAdminDao.saveAll(addList);

        return ServiceResult.ok();
    }

    @Override
    public List<ProjectAreaResp> searchProjectAreaList() {
        List<Project> projectList = projectDao.findByNoNotOrderByCreatedTimeDesc(DEFAULT_PROJECT_NUMBER);

        List<ProjectAreaResp> projectSearchRespList = new ArrayList<>();
        ProjectAreaResp projectSearchResp;
        for (Project project : projectList) {
            List<Room> roomList = roomDao.findBySpaceProjectId(project.getId());
            if (!roomList.isEmpty()) {
                projectSearchResp = new ProjectAreaResp();
                BeanUtils.copyProperties(project, projectSearchResp);
                projectSearchResp.setProjectArea(project.getProjectArea().getName());
                projectSearchRespList.add(projectSearchResp);
            }
        }

        return projectSearchRespList;
    }

    /**
     * 删除项目
     *
     * @param id 项目ID
     * @return 业务结果
     * @author Yang.Lee
     * @date 2021/3/27 13:48
     **/
    @Override
    @Transactional
    public ServiceResult delete(String id) {

        Optional<Project> projectOptional = projectDao.findById(id);
        if (!projectOptional.isPresent()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_PROJECT, id));
        }

        // 判断项目下是否有空间，有空间不可删除
        Set<Space> space = spaceDao.findByProjectIdAndIsShow(id, 1);
        if (!space.isEmpty()) {
            return ServiceResult.error("该项目下含有空间数据，不可删除");
        }

        // 判断项目下是否有设备，有设备不可删除项目
        List<IotDevice> deviceOptional = deviceDao.findByProjectId(id);
        if (!deviceOptional.isEmpty()) {
            return ServiceResult.error("该项目下含有设备数据，不可删除");
        }

        Project project = projectOptional.get();
        project.setIsDelete(1);
        projectDao.save(project);

        return ServiceResult.ok();
    }

    @Override
    public List<ProjectAreaResp> searchPcProjectAreaList() {
        List<Project> projectList = projectDao.findByNoNotOrderByCreatedTimeDesc(DEFAULT_PROJECT_NUMBER);
        List<ProjectAreaResp> projectSearchRespList = new ArrayList<>();
        ProjectAreaResp projectSearchResp = null;

        for (Project project : projectList) {
            projectSearchResp = new ProjectAreaResp();
            BeanUtils.copyProperties(project, projectSearchResp);
            projectSearchResp.setProjectArea(project.getProjectName());
            projectSearchRespList.add(projectSearchResp);

        }
        return projectSearchRespList;
    }

    /**
     * 创建项目区域
     *
     * @param req 参数
     * @return 结果，成功是返回数据主键id
     * @author Yang.Lee
     * @date 2021/11/9 9:32
     **/
    @Override
    @Transactional
    public ServiceResult createProjectArea(ProjectAreaEditReq req) {

        Optional<ProjectArea> projectAreaOptional = projectAreaDao.findByName(req.getName());
        if (projectAreaOptional.isPresent()) {
            return ServiceResult.error("区域名称已存在", ErrConst.E01);
        }

        ProjectArea projectArea = new ProjectArea();
        projectArea.setName(req.getName());

        projectAreaDao.save(projectArea);

        return ServiceResult.ok(IdResp.builder().id(projectArea.getId()).build());
    }

    /**
     * 获取项目区域列表
     *
     * @return 项目区域列表
     * @author Yang.Lee
     * @date 2021/11/9 9:33
     **/
    @Override
    public List<ProjectAreaResp> getProjectArea() {

        List<ProjectArea> projectAreaList = projectAreaDao.findAll(Sort.by(Sorts.CREATED_TIME));

        return projectAreaList.stream()
                .map(obj ->
                        ProjectAreaResp.builder()
                                .id(obj.getId())
                                .projectArea(obj.getName())
                                .build())
                .collect(Collectors.toList());
    }
}
