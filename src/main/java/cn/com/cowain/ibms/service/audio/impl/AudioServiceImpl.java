package cn.com.cowain.ibms.service.audio.impl;

import cn.com.cowain.ibms.dao.ReservationRecordDao;
import cn.com.cowain.ibms.dao.RoomDao;
import cn.com.cowain.ibms.dao.SysUserDao;
import cn.com.cowain.ibms.dao.audio.AudioDao;
import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.entity.Room;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.voice.AudioRecord;
import cn.com.cowain.ibms.enumeration.ReservationRecordStatus;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.audio.RealtimeUpload;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.ReservationRecordResp;
import cn.com.cowain.ibms.rest.resp.audio.AudioDetailResp;
import cn.com.cowain.ibms.rest.resp.audio.AudioRecordResp;
import cn.com.cowain.ibms.rest.resp.audio.AudioRecordRespPage;
import cn.com.cowain.ibms.rest.resp.audio.RTASTResultResp;
import cn.com.cowain.ibms.service.audio.AudioService;
import cn.com.cowain.ibms.service.audio.AudioTranscriptionService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.utils.DateUtils;
import cn.com.cowain.ibms.utils.FileUtil;
import cn.com.cowain.ibms.utils.RedisUtil;
import cn.com.cowain.ibms.utils.enums.DateUnit;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.csource.common.MyException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Yang.Lee
 * @date 2021/1/4 9:52
 */
@Slf4j
@Service
public class AudioServiceImpl implements AudioService {

    @Autowired
    private AudioDao audioDao;

    @Autowired
    private RoomDao roomDao;
    @Autowired
    private ReservationRecordDao reservationRecordDao;
    @Autowired
    private AudioTranscriptionService lfasrService;
    @Autowired
    private SysUserDao sysUserDao;
    @Autowired
    private RedisUtil redisUtil;

    private RestTemplate restTemplate;

    public AudioServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    private static final String NOT_FOUND = "语音记录id错误，未找到数据";

    private static final String GET_USER_ERROR = "查询用户发起的会议失败！！！  找不到对应的用户信息，用户uc id = {}";

    @Value("${fastdfs.fileUrl}")
    private String fastDfsHttpUrl;

    // 实时文字分段步长,毫秒
    private static final int STEP = 5000;
    // redis key 前置
    private static final String REDIS_PRE_KEY_RTASR = "rtasr-";

    /**
     * 音频记录默认过期时间
     */
    @Value("${cowain.fastdfs.audio.expiredTime}")
    private int audioExpiredTime;



    /**
     * 上传文件到fastdfs服务器
     * @return
     */
    @Override
    @Transactional
    public ServiceResult uploadToFastDFS(String audioRecordId, String path, String endTime) throws IOException, MyException, InterruptedException, URISyntaxException {
        // 检查记录是否存在
        ServiceResult checkResult = recordCheck(audioRecordId);
        if (!checkResult.isSuccess()) {
            return checkResult;
        }
        // 原纪录
        AudioRecord audioRecord = (AudioRecord) checkResult.getObject();

        audioRecord.setPcmFilePath(path);

        audioRecord.setEndTime(DateUtils.parseLocalDateTime(endTime, DateUtils.PATTERN_DATETIME));
        // 保存信息
        audioDao.save(audioRecord);

        // 异步执行转写任务
        lfasrService.translate(audioRecord);

        return ServiceResult.ok();
    }

    /**
     * 记录检查
     *
     * @param audioRecordId
     * @return
     */
    @Transactional
    public ServiceResult recordCheck(String audioRecordId) {
        Optional<AudioRecord> audioRecordOptional = audioDao.findById(audioRecordId);

        if (!audioRecordOptional.isPresent()) {
            return ServiceResult.error(NOT_FOUND);
        }

        return ServiceResult.ok(audioRecordOptional.get());
    }

    @Override
    @Transactional
    public List<ReservationRecordResp> findByRoomIdAndDate(String roomId, LocalDate localDate, String empNo) {

        // 前端传入的sysId为uc登录后从token中解析出的id，该id可能和微信端登录获取的id不同，需要查询出对应的微信创建会议时的id
        Optional<SysUser> sysUserOptional = sysUserDao.findByEmpNo(empNo);
        if(!sysUserOptional.isPresent()){
            log.error(GET_USER_ERROR, empNo);
            return new ArrayList<>();
        }

        Optional<Room> roomOptional = roomDao.findById(roomId);
        List<ReservationRecordResp> reservationList = new ArrayList<>();
        ReservationRecordResp reservationRecordResp = null;
        if (roomOptional.isPresent()) {
            //setp3: 查询 已预约
            List<ReservationRecord> reservationRecordList = reservationRecordDao.findByDateAndRoomIdAndInitiatorEmpNoOrderByFromAsc(localDate, roomId, empNo);
            for (ReservationRecord reservation : reservationRecordList) {

                // 筛选已取消会议
                if(reservation.getStatus().equals(ReservationRecordStatus.CANCEL)){
                    continue;
                }

                reservationRecordResp = new ReservationRecordResp();
                BeanUtils.copyProperties(reservation, reservationRecordResp);
                //设置会议发起人
                String initiatorEmpNo = reservation.getInitiatorEmpNo();
                Optional<SysUser> bySysId = sysUserDao.findByEmpNo(initiatorEmpNo);
                if (bySysId.isPresent()) {
                    reservationRecordResp.setInitiator(bySysId.get().getNameZh());
                } else {
                    reservationRecordResp.setInitiator("会议发起人ID有误");
                }

                //获取开始时间时分秒
                LocalDateTime from = reservation.getFrom();
                String s = from.toString();
                String startMinute = s.substring(11, 16);
                //获取结束时间时分秒
                LocalDateTime to = reservation.getTo();
                String t = to.toString();
                String endMinute = t.substring(11, 16);
                reservationRecordResp.setStartMinute(startMinute);
                reservationRecordResp.setEndMinute(endMinute);
                reservationList.add(reservationRecordResp);
            }
        }
        return reservationList;
    }

    @Override
    @Transactional
    public ServiceResult save(AudioRecordResp audioRecordResp) {
        AudioRecord audioRecord = new AudioRecord();
        BeanUtils.copyProperties(audioRecordResp, audioRecord);
        // 查询会议记录信息
        Optional<ReservationRecord> audioOp = reservationRecordDao.findById(audioRecordResp.getReservationRecordId());
        if (audioOp.isPresent()) {
            LocalDateTime timeStart = LocalDateTime.now();
            audioRecord.setReservationRecord(audioOp.get());
            audioRecord.setStartTime(timeStart);
        } else {
            return ServiceResult.error("会议不存在");
        }
        //保存音频记录
        audioDao.save(audioRecord);
        // 返回主键ID
        IdResp idResp = new IdResp();
        idResp.setId(audioRecord.getId());
        return ServiceResult.ok(idResp);
    }

    @Override
    @Transactional
    public ServiceResult update(String id) {
        Optional<AudioRecord> byId = audioDao.findById(id);
        boolean present = byId.isPresent();
        if (present) {
            LocalDateTime now = LocalDateTime.now();
            AudioRecord audioRecord = byId.get();
            audioRecord.setEndTime(now);
            audioDao.save(audioRecord);
        }
        return ServiceResult.ok();
    }

    /**
     * 获取音频记录详细信息
     *
     * @param id
     * @return
     * @author Yang.Lee
     */
    @Override
    @Transactional
    public ServiceResult get(String id) {

        Optional<AudioRecord> audioRecordOptional = audioDao.findById(id);
        if (!audioRecordOptional.isPresent()) {
            return ServiceResult.ok(new AudioDetailResp());
        }
        AudioDetailResp resp = convertToAudioDetailResp(audioRecordOptional.get());

        return ServiceResult.ok(resp);
    }


    @Override
    @Transactional
    public PageBean<AudioRecordRespPage> findByRoomName (String name, LocalDate localDate, String empNo, PageBean pageBean){

        // 前端传入的sysId为uc登录后从token中解析出的id，该id可能和微信端登录获取的id不同，需要查询出对应的微信创建会议时的id
        Optional<SysUser> sysUserOptional = sysUserDao.findByEmpNo(empNo);

        if(!sysUserOptional.isPresent()){
            log.error(GET_USER_ERROR, empNo);
            PageBean<AudioRecordRespPage> result = new PageBean<>();
            result.setList(new ArrayList<>());
            return result;
        }

        Sort sort = Sort.by(Sort.Direction.DESC, "startTime");
        Pageable pageableAudio = PageRequest.of(pageBean.getPage(), pageBean.getSize(), sort);
        List<AudioRecordRespPage> audioList = new ArrayList<>();
        Page<AudioRecord> audioRecordLists = audioDao.findByReservationRecordRoomNameContainsAndReservationRecordDateAndReservationRecordInitiatorEmpNo(name, localDate, empNo, pageableAudio);
        audioRecordLists.getContent().forEach(it -> {
            AudioRecordRespPage audioRecordResp = new AudioRecordRespPage();
            BeanUtils.copyProperties(it, audioRecordResp);
            //设置会议室预定日期
            audioRecordResp.setDate(it.getReservationRecord().getDate());
            //设置会议记录开始时间
            audioRecordResp.setFrom(it.getReservationRecord().getFrom());
            //设置会议记录结束时间
            audioRecordResp.setTo(it.getReservationRecord().getTo());
            //设置会议主题
            audioRecordResp.setTopic(it.getReservationRecord().getTopic());
            //设置会议室名称
            audioRecordResp.setName(it.getReservationRecord().getRoom().getName());
            audioList.add(audioRecordResp);
        });
        //当前页码
        pageBean.setPage(pageableAudio.getPageNumber());
        //每页记录数
        pageBean.setSize(pageableAudio.getPageSize());
        //总记录数
        pageBean.setTotalElements(audioRecordLists.getTotalElements());
        //总页数
        pageBean.setTotalPages(audioRecordLists.getTotalPages());
        pageBean.setList(audioList);
        return pageBean;
    }


    @Override
    @Transactional
    public PageBean<AudioRecordRespPage> findByName (String name, String empNo, PageBean < AudioRecordRespPage > pageBean){

        // 前端传入的sysId为uc登录后从token中解析出的id，该id可能和微信端登录获取的id不同，需要查询出对应的微信创建会议时的id
        Optional<SysUser> sysUserOptional = sysUserDao.findByEmpNo(empNo);

        if(!sysUserOptional.isPresent()){
            log.error(GET_USER_ERROR, empNo);
            PageBean<AudioRecordRespPage> result = new PageBean<>();
            result.setList(new ArrayList<>());
            return result;
        }

        Sort sort = Sort.by(Sort.Direction.DESC, "startTime");
        Pageable pageableAudio = PageRequest.of(pageBean.getPage(), pageBean.getSize(), sort);
        List<AudioRecordRespPage> audioList = new ArrayList<>();
        Page<AudioRecord> audioRecordLists = audioDao.findByReservationRecordRoomNameContainsAndReservationRecordInitiatorEmpNo(name, empNo, pageableAudio);
        audioRecordLists.getContent().forEach(it -> {
            AudioRecordRespPage audioRecordResp = new AudioRecordRespPage();
            BeanUtils.copyProperties(it, audioRecordResp);
            //设置会议室预定日期
            audioRecordResp.setDate(it.getReservationRecord().getDate());
            //设置会议记录开始时间
            audioRecordResp.setFrom(it.getReservationRecord().getFrom());
            //设置会议记录结束时间
            audioRecordResp.setTo(it.getReservationRecord().getTo());
            //设置会议主题
            audioRecordResp.setTopic(it.getReservationRecord().getTopic());
            //设置会议室名称
            audioRecordResp.setName(it.getReservationRecord().getRoom().getName());
            audioList.add(audioRecordResp);
        });
        //当前页码
        pageBean.setPage(pageableAudio.getPageNumber());
        //每页记录数
        pageBean.setSize(pageableAudio.getPageSize());
        //总记录数
        pageBean.setTotalElements(audioRecordLists.getTotalElements());
        //总页数
        pageBean.setTotalPages(audioRecordLists.getTotalPages());
        pageBean.setList(audioList);
        return pageBean;
    }


    /**
     * 获取实时语音转写信息
     *
     * @param audioRecordId
     * @return
     */
    @Override
    @Transactional
    public ServiceResult getRealTimeAudioRecord (String audioRecordId){

        Optional<AudioRecord> audioRecordOptional = audioDao.findById(audioRecordId);
        if (!audioRecordOptional.isPresent()) {
            return ServiceResult.error("未找到实时语音转写数据");
        }
        AudioRecord audioRecord = audioRecordOptional.get();
        AudioDetailResp result = convertToAudioDetailResp(audioRecord);
        // 会议未结束,则查询redis赋值
        if (audioRecord.getEndTime() == null) {
            // 项目信息
            String key = redisUtil.createRealKey(audioRecordId);

            if (Boolean.TRUE.equals(redisUtil.hasKey(key))) {
                Object obj = redisUtil.get(key);
                result = JSON.parseObject(String.valueOf(obj), AudioDetailResp.class);
            }

            List<RTASTResultResp> resultRespList = null;
            String redisKey = redisUtil.createRealKey(REDIS_PRE_KEY_RTASR + audioRecordId);

            if (Boolean.TRUE.equals(redisUtil.hasKey(redisKey))) {
                resultRespList = JSON.parseArray(String.valueOf(redisUtil.get(redisKey)), RTASTResultResp.class);
            }

            result.setRealtimeText(resultRespList);
        }

        return ServiceResult.ok(result);
    }

    /**
     * 类型转换
     *
     * @param audioRecord
     * @return
     */
    private AudioDetailResp convertToAudioDetailResp(AudioRecord audioRecord) {

        AudioDetailResp resp = new AudioDetailResp();
        resp.setAudioRecordId(audioRecord.getId());
        resp.setAudioFileUrl(audioRecord.getAudioFilePath());
        resp.setDatetime(audioRecord.getReservationRecord().getDate());
        resp.setEndTime(audioRecord.getEndTime());
        resp.setFromTime(audioRecord.getStartTime());
        resp.setRoomName(audioRecord.getReservationRecord().getRoom().getName());
        resp.setTopic(audioRecord.getReservationRecord().getTopic());

        // 判断音频有效期
        boolean notExpired = notExpired(audioRecord);
        resp.setNotExpired(notExpired);

        // 会议是否已结束标志
        boolean isEnd = audioRecord.getEndTime() != null;
        resp.setMeetingEnd(isEnd);

        // 下载语音转写文本内容
        if (audioRecord.getTextFilePath() != null && audioRecord.getTextFilePath().length() > 0) {
            try {
                HttpHeaders headers = new HttpHeaders();
                HttpEntity<Resource> httpEntity = new HttpEntity<>(headers);
                ResponseEntity<byte[]> response = restTemplate.exchange(fastDfsHttpUrl + audioRecord.getTextFilePath(), HttpMethod.GET, httpEntity, byte[].class);
                byte[] bytes = response.getBody();
                if (bytes != null && bytes.length > 0) {
                    resp.setText(new String(bytes, StandardCharsets.UTF_8));
                }
            } catch (Exception e) {
                log.error("从fastdfs服务下载语音转写数据异常！！！， url: " + fastDfsHttpUrl + audioRecord.getTextFilePath(), e);
                resp.setText("");
            }
        } else {
            resp.setText("");
        }

        // 拼接音频文件url，如果已过期，不显示音频
        if (audioRecord.getAudioFilePath() != null && audioRecord.getAudioFilePath().length() > 0 && notExpired) {
            resp.setAudioFileUrl(fastDfsHttpUrl + audioRecord.getAudioFilePath());
        } else {
            resp.setAudioFileUrl("");
        }

        // 如果会议已结束，从数据库中读取实时会议文本，并将其转为对应的list
        if (isEnd && audioRecord.getRtTextFilePath() != null && audioRecord.getRtTextFilePath().length() > 0) {
            try {
                HttpHeaders headers = new HttpHeaders();
                HttpEntity<Resource> httpEntity = new HttpEntity<>(headers);
                ResponseEntity<byte[]> response = restTemplate.exchange(fastDfsHttpUrl + audioRecord.getRtTextFilePath(), HttpMethod.GET, httpEntity, byte[].class);
                byte[] bytes = response.getBody();
                if (bytes != null && bytes.length > 0) {
                    String rtText = new String(bytes, StandardCharsets.UTF_8);
                    List<RTASTResultResp> resultRespList = JSON.parseArray(rtText, RTASTResultResp.class);
                    resp.setRealtimeText(resultRespList);
                } else{
                    resp.setRealtimeText(new ArrayList<>());
                }
            } catch (Exception e) {
                log.error("从fastdfs服务下载实时转写文本数据异常！！！ url : " + fastDfsHttpUrl + audioRecord.getRtTextFilePath(), e);
                resp.setRealtimeText(new ArrayList<>());
            }
        } else {
            resp.setRealtimeText(new ArrayList<>());
        }

        // 设置前端读取事实文本的步长，单位从毫秒转为秒
        resp.setRtStep(STEP / 1000);
        return resp;
    }


    /**
     * 检查上传PCM文件的合法性（包含名称，文件扩展名，文件大小的校验）
     *
     * @param file 上传的文件
     * @return
     */
    @Override
    public ServiceResult checkPcmFile(MultipartFile file) {
        if (file == null) {
            return ServiceResult.error("文件为空");
        }

        String localFileName = file.getOriginalFilename();
        if (localFileName == null || localFileName.length() == 0) {
            return ServiceResult.error("文件格式异常，未知文件名称");
        }
        String ext = FileUtil.getExt(localFileName);
        if (ext == null || !ext.equalsIgnoreCase("pcm")) {
            return ServiceResult.error("文件格式异常，只支持pcm格式文件");
        }

        try {
            byte[] bytes = file.getBytes();
            log.debug("controller 获取到文件大小为：" + bytes.length);
            if (bytes.length <= 0) {
                return ServiceResult.error("上传文件的大小为0");
            }

        } catch (IOException e) {
            log.error(e.getMessage(), e);
            return ServiceResult.error("获取文件大小异常");
        }

        return ServiceResult.ok();
    }

    /**
     * 补传实时语音转写语音文件
     *
     * @param id
     * @param realtimeUpload
     * @return
     */
    @Override
    public ServiceResult realtimeUpload(String id, RealtimeUpload realtimeUpload) {

        // 异步执行转写操作
        lfasrService.realtimeUploadTranslate(id, realtimeUpload);
        return ServiceResult.ok();
    }

    /**
     * 实时转写中断
     *
     * @param audioId
     * @return
     */
    @Override
    public ServiceResult rtasrBreak(String audioId) {
        String redisKey = redisUtil.createRealKey("rtasr-break-" + audioId);
        // 将标记写入redis，默认存在24小时
        redisUtil.set(redisKey, 1, 60 * 60 * 24L);
        return ServiceResult.ok();
    }

    /**
     * 判断音频记录是否过期，使用当前时间减创建时间
     *
     * @param audioRecord
     * @return true: 未过期; false: 已过期
     */
    private boolean notExpired(AudioRecord audioRecord) {
        LocalDateTime createTime = audioRecord.getCreatedTime();
        LocalDateTime now = LocalDateTime.now();

        long diff = DateUtils.betweenTime(createTime, now, DateUnit.DAY);
        return diff <= audioExpiredTime;
    }

}
