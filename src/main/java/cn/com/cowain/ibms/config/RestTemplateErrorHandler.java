package cn.com.cowain.ibms.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * restTemplate 异常处理类
 */
@Slf4j
public class RestTemplateErrorHandler implements ResponseErrorHandler {

    @Override
    public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
        int rawStatusCode = clientHttpResponse.getRawStatusCode();
        HttpStatus statusCode = HttpStatus.resolve(rawStatusCode);

        return statusCode != null ? statusCode.isError() : hasError(rawStatusCode);
    }

    protected boolean hasError(int unknownStatusCode) {
        HttpStatus.Series series = HttpStatus.Series.resolve(unknownStatusCode);
        return series == HttpStatus.Series.CLIENT_ERROR || series == HttpStatus.Series.SERVER_ERROR;
    }


    @Override
    public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {
        log.error("调用API失败。API 返回结果：{}", IOUtils.toString(clientHttpResponse.getBody(), StandardCharsets.UTF_8));
    }
}
