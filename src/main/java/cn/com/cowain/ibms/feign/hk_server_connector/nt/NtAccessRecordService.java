package cn.com.cowain.ibms.feign.hk_server_connector.nt;

import cn.com.cowain.ibms.feign.hk_server_connector.nt.fallback.NtAccessRecordServiceFallBack;
import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.req.distinguish.AuthTimeDataReq;
import cn.com.cowain.ibms.rest.req.hk.FaceRecord;
import cn.com.cowain.ibms.rest.resp.EquipmentDataResp;
import cn.com.cowain.ibms.rest.resp.SearchStaffDataResp;
import cn.com.cowain.ibms.rest.resp.distinguish.AuthTimeDataFailRep;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @Author tql
 * @Description NtAccessRecordService
 * @Date 21-5-21 上午11:01
 * @Version 1.0
 */
@FeignClient(value = "ms-hk-server-connector-nt", url = "${cowain.gateway.address}", fallback = NtAccessRecordServiceFallBack.class)
public interface NtAccessRecordService {

    /**
     * 获取考勤记录
     *
     * @param faceRecord 开始时间/结束时间 example:2021-05-15
     * @return
     */
    @PostMapping("/nt/hk-device/api/v1/faceData/faceRecord")
    JsonResult<List<SearchStaffDataResp>> searchStaff(@RequestBody FaceRecord faceRecord);


    /**
     * 心跳检测
     *
     * @return
     */
    @GetMapping("/nt/hk-device/api/v1/ping")
    JsonResult<String> healthCheck();

    /**
     * 获取设备列表
     *
     * @return
     */
    @GetMapping("/nt/hk-device/api/v1/sysData/sysRecordEquipmentList")
    JsonResult<List<EquipmentDataResp>> getEquipmentList();

    /**
     * 更新海康南通排班信息
     *
     * @param authTimeDataList
     * @return
     */
    @PostMapping("/nt/hk-device/api/v1/auth/update/auth")
    JsonResult<List<AuthTimeDataFailRep>> updateAuth(List<AuthTimeDataReq> authTimeDataList);
}