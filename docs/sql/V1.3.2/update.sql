CREATE TABLE `ibms_uat`.`iot_door_plate_connection`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `created_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `updated_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `version` int(11) NULL DEFAULT NULL COMMENT '版本号',
  `connect_time` datetime(0) NULL DEFAULT NULL COMMENT '通信时间',
  `door_plate_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'FK 门牌ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK7xcj1r67p9rk6772ov80njt23`(`door_plate_id`) USING BTREE,
  CONSTRAINT `FK7xcj1r67p9rk6772ov80njt23` FOREIGN KEY (`door_plate_id`) REFERENCES `ibms_uat`.`iot_door_plate` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '门牌通信' ROW_FORMAT = Dynamic;


ALTER TABLE `ibms_uat`.`iot_door_plate` ADD COLUMN `password` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `device_id`;

ALTER TABLE `ibms_uat`.`iot_door_plate` ADD COLUMN `open_password` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `password`;