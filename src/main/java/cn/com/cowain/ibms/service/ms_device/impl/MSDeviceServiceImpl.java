package cn.com.cowain.ibms.service.ms_device.impl;

import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.req.ms_device.AccessRuleCreateReq;
import cn.com.cowain.ibms.rest.req.ms_device.AccessRuleUpdateReq;
import cn.com.cowain.ibms.service.bean.ServiceMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.ms_device.MSDeviceService;
import cn.com.cowain.ibms.utils.ErrConst;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * ms-device 业务实现类
 *
 * @author Yang.Lee
 * @date 2021/3/19 20:34
 */
@Slf4j
@Service
public class MSDeviceServiceImpl implements MSDeviceService {

    @Resource
    private RestTemplate restTemplate;

    @Value("${cowain.ms-device.url.createRule}")
    private String createRuleUrl;

    @Value("${cowain.ms-device.url.updateRule}")
    private String updateRuleUrl;

    @Value("${cowain.ms-device.url.deleteRule}")
    private String deleteRuleUrl;

    /**
     * 创建通行权限
     *
     * @param req 请求参数
     * @return 请求结果
     * @author Yang.Lee
     * @date 2021/3/19 20:33
     **/
    @Override
    public ServiceResult createAccessRule(AccessRuleCreateReq req) {

        ResponseEntity<JsonResult> responseEntity = restTemplate.postForEntity(createRuleUrl, req, JsonResult.class);
        JsonResult<Object> response = responseEntity.getBody();

        return response != null && response.getStatus() == 1 ?
                ServiceResult.ok() :
                ServiceResult.error(ServiceMessage.API_CALLED_ERROR, ErrConst.E20);
    }

    /**
     * 编辑通行权限
     *
     * @param req 请求参数
     * @return 请求结果
     * @author Yang.Lee
     * @date 2021/3/19 20:33
     **/
    @Override
    public ServiceResult updateAccessRule(AccessRuleUpdateReq req) {
        restTemplate.put(updateRuleUrl, req);
        return ServiceResult.ok();
    }

    /**
     * 删除通行权限
     *
     * @param ksDeviceId 旷世设备ID
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/3/20 18:48
     **/
    @Override
    @Transactional
    public ServiceResult deleteAccessRule(String ksDeviceId) {
        restTemplate.delete(deleteRuleUrl + ksDeviceId);
        return ServiceResult.ok();
    }
}
