package cn.com.cowain.ibms.entity.ota;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.enumeration.ota.Application;
import cn.com.cowain.ibms.enumeration.ota.UpgradeStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author Yang.Lee
 * @date 2021/4/14 18:52
 */
@Data
@Entity
@Table(name = "ibms_ota_upgrade")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "ibms_ota_upgrade", comment = "ota应用升级任务记录表")
public class OTAUpgrade extends BaseEntity {

    @Column(name = "task_name", nullable = false, columnDefinition = "varchar(30) COMMENT '升级任务名称'")
    private String taskName;

    @Enumerated(EnumType.STRING)
    @Column(name = "app", nullable = false, columnDefinition = "varchar(50) COMMENT '升级应用'")
    private Application application;

    @Column(name = "app_package_url", nullable = false, columnDefinition = "varchar(150) COMMENT '安装包地址'")
    private String appPackageUrl;

    @Column(name = "original_file_name", nullable = false, columnDefinition = "varchar(150) COMMENT '文件原名称'")
    private String originalFileName;

    @Column(name = "app_size", nullable = false, columnDefinition = "varchar(50) COMMENT '安装包大小'")
    private String appSize;

    @Column(name = "app_package_md5", nullable = false, columnDefinition = "varchar(50) COMMENT '安装包md5'")
    private String appPackageMD5;

    @Column(name = "upgrade_time", nullable = false, columnDefinition = "datetime COMMENT '升级时间'")
    private LocalDateTime upgradeTime;

    @Column(name = "content", columnDefinition = "varchar(500) COMMENT '更新内容'")
    private String content;

    @Column(name = "is_force_upgrade",nullable = false, columnDefinition = "bit(1) default 0 COMMENT '强制升级.1：是。0否'")
    private boolean forceUpgrade;

    @Column(name = "version_ota", nullable = false, columnDefinition = "varchar(30) COMMENT '版本号'")
    private String versionOTA;

    @Column(name = "download_count", nullable = false, columnDefinition = "int(10) COMMENT '下载次数'")
    private int download;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false, columnDefinition = "varchar(50) COMMENT '任务状态'")
    private UpgradeStatus status;
}
