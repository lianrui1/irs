package cn.com.cowain.ibms.feign.task_center.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * @author wei.cheng
 * @date 2022/03/11 17:55
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UpdateTaskReq {
    /**
     * 待办中心任务ID
     */
    @NotNull
    private Long id;
    /**
     * 流程到达审批人时间，unix时间秒级戳
     */
    private Long arrivalTimestamp;
    /**
     * 发起人视角流程详情地址
     */
    private String claimerUrl;
    /**
     * 根据模板的定义需要传入的数据，格式：K-V
     */
    private Object data;
    /**
     * 审批环节状态，`1`审批中，`2`已批准，`3`已驳回，`4`已撤回
     */
    private Integer operateResult;
    /**
     * 审批任务状态，`1`代表待办，`2`代表已办
     */
    private Integer status;
}
