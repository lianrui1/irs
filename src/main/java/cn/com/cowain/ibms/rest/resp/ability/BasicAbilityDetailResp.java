package cn.com.cowain.ibms.rest.resp.ability;

import cn.com.cowain.ibms.entity.iot.AccessControlSpot;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author feng
 * @title: BasicAbilityDetailResp
 * @projectName ibms
 * @Date 2022/1/11 10:33
 */
@Data
public class BasicAbilityDetailResp implements Serializable {
    @ApiModelProperty(value = "能力id")
    String id;
    @ApiModelProperty(value = "能力名称")
    String abilityName;
    @ApiModelProperty(value = "设备类型：逻辑设备")
    String deviceType;
    @ApiModelProperty(value = "提供能力：查询通行记录")
    String supplyAbility;
    @ApiModelProperty(value = "设备产品：门禁点")
    String product;
    @ApiModelProperty(value = "设备位置")
    String spaceName;
    @ApiModelProperty(value = "空间Id")
    String spaceId;

    BasicAbilityDetailResp(){}
    BasicAbilityDetailResp(AccessControlSpot controlSpot){
        this.id=controlSpot.getId();
        this.spaceName=controlSpot.getAddress();
    }
}
