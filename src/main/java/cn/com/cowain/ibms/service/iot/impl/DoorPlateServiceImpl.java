package cn.com.cowain.ibms.service.iot.impl;

import cn.com.cowain.ibms.dao.OpeningHoursDao;
import cn.com.cowain.ibms.dao.ReservationRecordDao;
import cn.com.cowain.ibms.dao.RoomDao;
import cn.com.cowain.ibms.dao.RoomDeviceDao;
import cn.com.cowain.ibms.dao.iot.*;
import cn.com.cowain.ibms.dao.space.IotProductDao;
import cn.com.cowain.ibms.entity.*;
import cn.com.cowain.ibms.entity.iot.*;
import cn.com.cowain.ibms.entity.space.IotProduct;
import cn.com.cowain.ibms.enumeration.IotNodeType;
import cn.com.cowain.ibms.enumeration.ReservationRecordStatus;
import cn.com.cowain.ibms.enumeration.iot.DataSource;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.enumeration.iot.DoorPlateStatus;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.IdAndSeqBean;
import cn.com.cowain.ibms.rest.req.iot.DoorPlateReq;
import cn.com.cowain.ibms.rest.req.iot.DoorPlateSortReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.iot.DevicePasswordCheckResp;
import cn.com.cowain.ibms.rest.resp.iot.DoorOpenResp;
import cn.com.cowain.ibms.rest.resp.iot.doorplate.DoorPlateReservationRecordResp;
import cn.com.cowain.ibms.rest.resp.iot.doorplate.DoorPlateSearchResp;
import cn.com.cowain.ibms.rest.resp.iot.doorplate.DoorPlateStatusDetailResp;
import cn.com.cowain.ibms.rest.resp.iot.doorplate.DoorPlateStatusResp;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.iot.DoorMagneticControlService;
import cn.com.cowain.ibms.service.iot.DoorPlateService;
import cn.com.cowain.ibms.service.office.SpaceCacheService;
import cn.com.cowain.ibms.service.space.impl.IotDeviceServiceImpl;
import cn.com.cowain.ibms.utils.DateUtils;
import cn.com.cowain.ibms.utils.ThreadPoolUtils;
import cn.com.cowain.ibms.utils.enums.DateUnit;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.time.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author: cuiEnming
 * @title: DoorPlateServceImpl
 * @date 2020/11/24  14:59
 * @since s2
 */
@Slf4j
@Service
public class DoorPlateServiceImpl implements DoorPlateService {


    private DoorPlateDao doorPlateDao;

    private DeviceDao deviceDao;

    private RoomDao roomDao;

    private DeviceToSpaceDao deviceToSpaceDao;

    private ReservationRecordDao reservationRecordDao;

    @Autowired
    private IotProductDao iotProductDao;

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserServiceUC;

    @Autowired
    private DoorMagneticDao doorMagneticDao;

    @Autowired
    private DoorPlateConnDao doorPlateConnDao;

    @Autowired
    private SpaceDao spaceDao;

    @Autowired
    private DeviceDao iotDeviceDao;

    @Resource(name = "doorMagneticMQTTService")
    private DoorMagneticControlService doorMagneticMQTTService;

    @Resource(name = "doorMagneticHWService")
    private DoorMagneticControlService doorMagneticHWService;

    @Resource
    private OpeningHoursDao openingHoursDao;

    @Resource
    private RoomDeviceDao roomDeviceDao;

    @Autowired
    private SpaceCacheService spaceCacheService;

    @Value("${reservation-record.about-to-begin-interval:10}")
    private Long ABOUT_TO_BEGIN_INTERVAL;

    public DoorPlateServiceImpl(DoorPlateDao doorPlateDao, DeviceDao deviceDao,
                                RoomDao roomDao, DeviceToSpaceDao deviceToSpaceDao, ReservationRecordDao reservationRecordDao) {
        this.doorPlateDao = doorPlateDao;
        this.deviceDao = deviceDao;
        this.roomDao = roomDao;
        this.deviceToSpaceDao = deviceToSpaceDao;
        this.reservationRecordDao = reservationRecordDao;
    }

    private static final String NO_DOORPLATE_MSG = "门牌ID错误，未找到门牌信息";

    private static final String NO_ROOM_MSG = "会议室ID错误，未找到会议室信息";

    /**
     * 门牌默认密码
     */
    private static final String COWAIN = "cowain";

    /**
     * 默认门牌产品的产品编号
     */
    private static final int DEFAULT_PRODUCT_NO = -1;
    /**
     * 默认门牌产品的产品名称
     */
    private static final String DEFAULT_PRODUCT_NAME = "DEFAULT_DOORPLATE";

    /**
     * 创建门牌
     *
     * @param doorPlateReq
     * @return
     * @author Yang.Lee
     */
    @Override
    @Transactional
    public ServiceResult save(DoorPlateReq doorPlateReq) {

        //根据roomId获取space信息
        Optional<Room> room = roomDao.findById(doorPlateReq.getRoomId());

        if (!room.isPresent()) {
            return ServiceResult.error(NO_ROOM_MSG);
        }

        // 检测设备是否已经存在了，如已存在则不能新建
         Optional<DoorPlate> doorPlateOptional = doorPlateDao.findByHardwareId(doorPlateReq.getHardwareId());
        //Optional<DoorPlate> doorPlateOptional = iotDeviceCacheService.getDoorPlateToRedis(doorPlateReq.getDoorMagne());
        if (doorPlateOptional.isPresent()) {
            return ServiceResult.error("门牌已存在");
        }


        Space space = room.get().getSpace();

        //如果用户未填写门磁编号，则统一设置为null，避免数据库中出现null与''共存的情况
        if (doorPlateReq.getDoorMagne().length() == 0) {
            doorPlateReq.setDoorMagne(null);
        }

        DoorPlate doorPlate = new DoorPlate();
        //数据赋值
        BeanUtils.copyProperties(doorPlateReq, doorPlate);
        doorPlate.setMagneticNum(doorPlateReq.getDoorMagne());

        //获取门牌排序最大值
        Integer maxSeq = doorPlateDao.findMaxSeq();
        // 如果数据库中有门牌，则排序加1，否则设置为 0
        maxSeq = maxSeq != null ? maxSeq + 1 : 0;
        doorPlate.setSeq(maxSeq);

        //写入device信息
        IotDevice device = new IotDevice();
        device.setStatus(1);
        device.setDeviceType(DeviceType.DOOR_PLATE);
        //创建设备时赋予默认密码
        device.setPassword(COWAIN);
        // 设置数据来源，默认门牌App
        device.setDataSource(DataSource.DOORPLATE);

        // 结合V1.1与v1.2
        // 将绑定的会议室空间赋予门牌
        device.setSpace(space);
        // 查询默认产品，将门牌挂载到默认门牌产品下，默认门牌产品产品编号为-1, 名称为DEFAULT_DOORPLATE
        Optional<IotProduct> iotProductOptional = iotProductDao.findByProductNoAndName(DEFAULT_PRODUCT_NO, DEFAULT_PRODUCT_NAME);
        if (iotProductOptional.isEmpty()) {
            return ServiceResult.error("未找到默认产品数据");
        }

        device.setIotProduct(iotProductOptional.get());
        device.setProject(space.getProject());


        // 查询设备最大排序
        Integer maxSort = deviceDao.findMaxSeq();
        maxSort = maxSort == null ? 0 : maxSort + 1;
        device.setSeq(maxSort);

        // 赋予默认设备值
        device.setIotNodeType(IotNodeType.DEFAULT);
        device.setSpaceControl(1);
        //新增device
        deviceDao.save(device);
        //写入门牌信息
        doorPlate.setDevice(device);
        doorPlateDao.save(doorPlate);
        // 门牌存入redis
//        String doorPlateRedisKey = redisUtil.createDeviceRealKey(IConst.DEVICE_MODULE, String.valueOf(DeviceType.DOOR_PLATE), doorPlate.getId(), RedisUtil.Separator.COLON);
//        Map<String, Object> map = DataUtils.targetObjectCastMap(doorPlate, DoorPlate.class);
//        redisUtil.hmset(doorPlateRedisKey,map,true);
        // 门牌信息保存至redis
//        iotDeviceCacheService.doorPlateRedis(device);
        // 空间设备数加1
        space.setDeviceNum(space.getDeviceNum() + 1);
        spaceDao.save(space);
        //写入space与device关系
        DeviceToSpace d2s = new DeviceToSpace();
        d2s.setDevice(device);
        d2s.setSpace(space);
        deviceToSpaceDao.save(d2s);

        // 将新建门牌的id返回给设备
        IdResp idResp = new IdResp();
        idResp.setId(doorPlate.getId());

        return ServiceResult.ok(idResp);
    }

    /**
     * 更新门牌
     *
     * @param doorPlaceId  门牌ID
     * @param doorPlateReq 门牌信息
     * @return
     * @author Yang.Lee
     */
    @Override
    @Transactional
    public ServiceResult update(String doorPlaceId, DoorPlateReq doorPlateReq, boolean updatePassword) {

        //  根据ID查询数据库中门牌是否存在
        Optional<DoorPlate> sourceDoorPlatOp = doorPlateDao.findById(doorPlaceId);
        if (!sourceDoorPlatOp.isPresent()) {
            return ServiceResult.error(NO_DOORPLATE_MSG);
        }

        // 查询更新数据中的会议室是否存在
        Optional<Room> targetRoom = roomDao.findById(doorPlateReq.getRoomId());
        if (!targetRoom.isPresent()) {
            return ServiceResult.error(NO_ROOM_MSG);
        }

        // 查询更新数据中的会议室门磁是否存在
        if (ObjectUtils.isEmpty(doorPlateReq.getDoorMagne())) {
            return ServiceResult.error(NO_ROOM_MSG);
        }

        //如果用户未填写门磁编号，则统一设置为null，避免数据库中出现null与空字符串共存的情况
        if (doorPlateReq.getDoorMagne().length() == 0) {
            doorPlateReq.setDoorMagne(null);
        }

        // 原数据库中的门牌信息
        DoorPlate sourceDoorPlate = sourceDoorPlatOp.get();
        // 更新数据赋值
        sourceDoorPlate.setMagneticNum(doorPlateReq.getDoorMagne());
        sourceDoorPlate.setName(doorPlateReq.getName());
        // 更新门牌信息
        doorPlateDao.save(sourceDoorPlate);
        // 更改空间设备数
        String deviceId = sourceDoorPlate.getDevice().getId();
        Optional<IotDevice> iotDeviceOptional = iotDeviceDao.findById(deviceId);
        if (iotDeviceOptional.isPresent()) {
            IotDevice device = iotDeviceOptional.get();
            if (ObjectUtils.isNotEmpty(device.getSpace())&&StringUtils.isNotEmpty(device.getSpace().getId())) {
                // 判断编辑后的会议室是否属于之前空间
                Room room = targetRoom.get();
                if (ObjectUtils.isNotEmpty(room.getSpace())&&StringUtils.equals(room.getSpace().getId(),device.getSpace().getId())) {
                    // 原空间设备数减1  更新后空间加1
                    Optional<Space> optionalSpace = spaceDao.findById(device.getSpace().getId());
                    if (optionalSpace.isPresent()) {
                        Space space = optionalSpace.get();
                        space.setDeviceNum(space.getDeviceNum() - 1);
                        spaceDao.save(space);
                        spaceCacheService.setSpaceInRedis(space);
                    }
                    Optional<Space> spaceOption = spaceDao.findById(room.getSpace().getId());
                    if (spaceOption.isPresent()) {
                        Space spaceOp = spaceOption.get();
                        spaceOp.setDeviceNum(spaceOp.getDeviceNum() + 1);
                        spaceDao.save(spaceOp);
                        spaceCacheService.setSpaceInRedis(spaceOp);

                    }

                }
            }
        }

        // TODO 门磁校验逻辑待定

        IotDevice sourceDevice = sourceDoorPlate.getDevice();
        //更新设备密码
        if (updatePassword) {
            sourceDevice.setPassword(doorPlateReq.getPassword());
        }

        deviceDao.save(sourceDevice);

        // 查询数据库中原room信息
        Optional<DeviceToSpace> sourceD2sOp = deviceToSpaceDao.findByDeviceId(sourceDevice.getId());

        Room tRoom = targetRoom.get();
        if (sourceD2sOp.isPresent()) {
            // 如果原来就有关系,则修改原关系
            DeviceToSpace d2s = sourceD2sOp.get();
            d2s.setDevice(sourceDevice);
            d2s.setSpace(tRoom.getSpace());
            deviceToSpaceDao.save(d2s);
        } else {
            // 如果数据库中无关系,写入新的device to space关系
            DeviceToSpace d2s = new DeviceToSpace();
            d2s.setDevice(sourceDevice);
            d2s.setSpace(tRoom.getSpace());
            deviceToSpaceDao.save(d2s);
        }

        return ServiceResult.ok();
    }

    /**
     * 查询当前门牌状态
     *
     * @param doorPlateId 门牌ID
     * @return
     * @author Yang.Lee
     */
    @Override
    @Transactional
    public ServiceResult status(String doorPlateId) {

        // 判断门牌是否存在
        Optional<DoorPlate> doorPlateOp = doorPlateDao.findById(doorPlateId);
        if (!doorPlateOp.isPresent()) {
            return ServiceResult.error(NO_DOORPLATE_MSG);
        }

        DoorPlateStatusResp doorPlateStatusResp = new DoorPlateStatusResp();

        DoorPlate doorPlate = doorPlateOp.get();
        // 根据门牌ID 查询对应的room
        IotDevice sourceDevice = doorPlate.getDevice();
        Optional<DeviceToSpace> d2pOp = deviceToSpaceDao.findByDeviceId(sourceDevice.getId());
        if (d2pOp.isPresent()) {
            String spaceId = d2pOp.get().getSpace().getId();
            Optional<Room> roomOp = roomDao.findBySpaceId(spaceId);
            if (roomOp.isPresent()) {
                Room room = roomOp.get();
                String roomId = room.getId();
                doorPlateStatusResp.setRoomId(roomId);
                doorPlateStatusResp.setRoomName(room.getName());
                doorPlateStatusResp.setRoomImg(Optional.ofNullable(room.getImg()).orElse(""));
                doorPlateStatusResp.setAttentions(Optional.ofNullable(room.getAttentions()).orElse(""));

                Set<RoomDevice> roomDeviceSet = roomDeviceDao.findByRoomId(room.getId());
                List<String> stringListDevice = new ArrayList<>();
                roomDeviceSet.forEach(it -> stringListDevice.add(it.getName()));
                doorPlateStatusResp.setDeviceList(stringListDevice);

                // 根据会议时间和会议室获取会议信息
                LocalDateTime now = LocalDateTime.now();
                List<ReservationRecord> opList = reservationRecordDao.findByToGreaterThanEqualAndFromLessThanEqualAndRoomIdAndStatusOrderByFromAsc(now ,now ,roomId ,ReservationRecordStatus.DEFAULT);
                Optional<ReservationRecord> op = opList.stream().findAny();

                if(op.isPresent()){
                    DoorPlateReservationRecordResp reservationRecordData = getReservationRecordData(op.get());
                    doorPlateStatusResp.setCurrentReservationRecord(reservationRecordData);
                    doorPlateStatusResp.setDoorPlateStatus(DoorPlateStatus.IN_PROCESS);
                }else {
                    doorPlateStatusResp.setDoorPlateStatus(DoorPlateStatus.FREE);
                }
            }
        }

        // 赋值门牌基础信息
        doorPlateStatusResp.setDoorPlateId(doorPlate.getId());
        doorPlateStatusResp.setDoorPlateName(doorPlate.getName());
        doorPlateStatusResp.setMagneticNum(doorPlate.getMagneticNum());
        doorPlateStatusResp.setPassword(sourceDevice.getPassword());
        Runnable runnable=()->{
            // 记录pad端请求日志
            DoorPlateConn doorPlateConn = new DoorPlateConn();
            doorPlateConn.setConnectTime(LocalDateTime.now());
            doorPlateConn.setDoorPlate(doorPlateOp.get());
            doorPlateConnDao.save(doorPlateConn);
        };
        ThreadPoolUtils.EXECUTOR_SERVICE_REDIS.submit(runnable);
        return ServiceResult.ok(doorPlateStatusResp);
    }

    /**
     * 获取会议门牌数据
     *
     * @param reservationRecord 会议预约记录
     * @return
     */
    @Transactional
    public DoorPlateReservationRecordResp getReservationRecordData(ReservationRecord reservationRecord) {

        DoorPlateReservationRecordResp result = new DoorPlateReservationRecordResp();

        result.setFrom(DateUtils.formatLocalDateTime(reservationRecord.getFrom(), DateUtils.PATTERN_TIME1));
        result.setTo(DateUtils.formatLocalDateTime(reservationRecord.getTo(), DateUtils.PATTERN_TIME1));
        // 会议创建人信息
        SysUser sysUser = sysUserServiceUC.getUserByEmpNo(reservationRecord.getInitiatorEmpNo());
        result.setInitiator(sysUser.getNameZh());
        return result;
    }

    /**
     * 查询当前门牌状态
     *
     * @param reservationRecord 当前一条会议室预约记录
     * @return
     */
    @Override
    public DoorPlateStatus checkCurrentRoomStatus(ReservationRecord reservationRecord, ReservationRecord nextRecord) {

        ZoneOffset zoneOffset = ZoneOffset.of("+8");
        LocalDateTime now = LocalDateTime.now();
        long nowTimeStamp = now.toInstant(zoneOffset).toEpochMilli();
        long timeDiffFrom = reservationRecord.getFrom().toInstant(zoneOffset).toEpochMilli() - nowTimeStamp;
        long timeDiffTo = reservationRecord.getTo().toInstant(zoneOffset).toEpochMilli() - nowTimeStamp;

        DoorPlateStatus status = DoorPlateStatus.FREE;

        if (timeDiffTo <= 0) {
            // 该条会议记录已结束, 状态未空闲
            return DoorPlateStatus.END;
        } else if (timeDiffTo > 0 && timeDiffFrom <= 0) {
            // 会议进行中
            status = DoorPlateStatus.IN_PROCESS;
            if (nextRecord != null && !nextRecord.getStatus().equals(ReservationRecordStatus.CANCEL)) {
                timeDiffFrom = nextRecord.getFrom().toInstant(zoneOffset).toEpochMilli() - nowTimeStamp;
                // 如果下一场会议5分钟内就要开始，则修改状态
                if (timeDiffFrom >= 0 && timeDiffFrom <= ABOUT_TO_BEGIN_INTERVAL * 60 * 1000) {
                    status = DoorPlateStatus.NEXT_ABOUT_TO_BEGIN;
                }
            }
        } else if (timeDiffFrom > 0 && timeDiffFrom <= ABOUT_TO_BEGIN_INTERVAL * 60 * 1000) {
            // 尚未到会议开始时间，但离会议开始只剩5分钟了
            // 会议即将开始
            status = DoorPlateStatus.ABOUT_TO_BEGIN;
        }

        return status;
    }

    /**
     * 模糊查询门牌列表(分页)
     *
     * @param doorPlateName 模糊查询关键字
     * @param page          页码
     * @param size          页长
     * @return
     * @author Yang.Lee
     */
    @Override
    @Transactional
    public PageBean<DoorPlateSearchResp> search(String doorPlateName, int page, int size) {

        // 查询按创建时间正序排列
        Sort sort = Sort.by(Sort.Direction.ASC, "seq");
        Pageable pageable = PageRequest.of(page, size, sort);

        Page<DoorPlate> doorPlatePage = doorPlateDao.findByNameContains(doorPlateName, pageable);

        // 数据输出对象
        PageBean<DoorPlateSearchResp> pageBean = new PageBean<>();
        //当前页
        pageBean.setPage(pageable.getPageNumber());
        //每页记录数
        pageBean.setSize(pageable.getPageSize());
        //总记录数
        pageBean.setTotalElements(doorPlatePage.getTotalElements());
        //总页数
        pageBean.setTotalPages(doorPlatePage.getTotalPages());

        List<DoorPlateSearchResp> respList = new ArrayList<>();
        doorPlatePage.getContent().forEach(doorPlate -> {
            // 数据对象转换
            DoorPlateSearchResp doorPlateSearchResp = new DoorPlateSearchResp();
            doorPlateSearchResp.setId(doorPlate.getId());
            doorPlateSearchResp.setName(doorPlate.getName());
            doorPlateSearchResp.setMagneticNum(doorPlate.getMagneticNum() != null ? doorPlate.getMagneticNum() : "");
            // 获取Room信息,再将 Room 数据赋值给 doorPlateSearchResp 对象
            IotDevice device = doorPlate.getDevice();
            doorPlateSearchResp.setPassword(device.getPassword());

            Optional<DeviceToSpace> optional = deviceToSpaceDao.findByDeviceId(device.getId());
            // DeviceToSpace
            if (optional.isPresent()) {
                DeviceToSpace deviceToSpace = optional.get();
                Space space = deviceToSpace.getSpace();
                // Room
                Optional<Room> roomOptional = roomDao.findBySpaceId(space.getId());
                if (roomOptional.isPresent()) {
                    Room room = roomOptional.get();
                    doorPlateSearchResp.setRoomId(room.getId());
                    doorPlateSearchResp.setRoomName(room.getName());
                }
            }

            // 获取最近的通讯时间
            Sort sortDoor = Sort.by(Sort.Direction.DESC, "createdTime");
            Pageable pageableDoor = PageRequest.of(0, 1, sortDoor);
            Page<DoorPlateConn> doorPage = doorPlateConnDao.findByDoorPlate(doorPlate, pageableDoor);
            doorPage.getContent().forEach(door -> doorPlateSearchResp.setConnectTime(door.getConnectTime()));

            respList.add(doorPlateSearchResp);
        });

        pageBean.setList(respList);

        return pageBean;
    }

    /**
     * 对门牌数据进行排序
     *
     * @param doorPlateSortReq
     * @return
     * @author Yang.Lee
     */
    @Override
    @Transactional
    public ServiceResult sort(DoorPlateSortReq doorPlateSortReq) {

        // 创建一个空LIST
        List<DoorPlate> doorPlateList = new ArrayList<>();
        List<IdAndSeqBean> list = doorPlateSortReq.getData();

        // 计算需要添加的页码排序
        int pageSeq = doorPlateSortReq.getPage() * doorPlateSortReq.getSize();

        // 循环
        list.forEach(it -> {
            // 获取id
            String id = it.getId();
            // 获取排序
            Integer seq = it.getSeq() + pageSeq;
            // 根据id查询
            Optional<DoorPlate> doorPlateOptional = doorPlateDao.findById(id);
            // 获取信息
            if (doorPlateOptional.isPresent()) {
                DoorPlate doorPlate = doorPlateOptional.get();
                // 设置排序
                doorPlate.setSeq(seq);
                // 添加空列表内
                doorPlateList.add(doorPlate);
            }
        });

        doorPlateDao.saveAll(doorPlateList);

        return ServiceResult.ok();
    }

    /**
     * 删除门牌信息
     *
     * @param id 门牌ID
     * @return
     * @author Yang.Lee
     */
    @Override
    @Transactional
    public ServiceResult delete(String id) {

        Optional<DoorPlate> doorPlateOp = doorPlateDao.findById(id);
        // 数据验证
        if (!doorPlateOp.isPresent()) {
            return ServiceResult.error(NO_DOORPLATE_MSG);
        }

        DoorPlate doorPlate = doorPlateOp.get();
        doorPlate.setIsDelete(1);
        doorPlateDao.save(doorPlate);
        // 更改空间设备数
        String deviceId = doorPlate.getDevice().getId();
        Optional<IotDevice> iotDeviceOptional = iotDeviceDao.findById(deviceId);
        if (iotDeviceOptional.isPresent()) {
            IotDevice device = iotDeviceOptional.get();
            IotDeviceServiceImpl.updateSpaceNum(device, spaceDao);
        }

        // 同样修改space和device关系表
        Optional<DeviceToSpace> d2sOp = deviceToSpaceDao.findByDeviceId(doorPlate.getDevice().getId());
        if (d2sOp.isPresent()) {
            DeviceToSpace d2s = d2sOp.get();
            d2s.setIsDelete(1);
            deviceToSpaceDao.save(d2s);
        }

        return ServiceResult.ok();
    }

    /**
     * 检查设备密码
     *
     * @param id       门牌ID
     * @param password 设备密码
     * @return
     */
    @Override
    @Transactional
    public ServiceResult checkDevicePassword(String id, String password) {

        Optional<DoorPlate> doorPlateOp = doorPlateDao.findById(id);
        // 数据验证
        if (!doorPlateOp.isPresent()) {
            return ServiceResult.error(NO_DOORPLATE_MSG);
        }

        IotDevice device = doorPlateOp.get().getDevice();
        boolean isValied = false;
        //如果原密码是空，则默认验证成功
        if (device.getPassword() == null || device.getPassword().equals("")) {
            isValied = true;
        } else {
            isValied = device.getPassword().equals(password);
        }
        DevicePasswordCheckResp devicePasswordCheckResp = new DevicePasswordCheckResp(isValied);

        return ServiceResult.ok(devicePasswordCheckResp);
    }

    /**
     * 更新所有门牌密码
     */
    @Override
    public ServiceResult updatePassword(String password) {
        List<DoorPlate> all = doorPlateDao.findAll();
        for (DoorPlate door : all) {
            Optional<DoorPlate> doorOption = doorPlateDao.findById(door.getId());
            if (doorOption.isPresent()) {
                DoorPlate doorPlate = doorOption.get();
                doorPlate.setPassword(password);
                doorPlateDao.save(doorPlate);
            }
        }
        return ServiceResult.ok();
    }

    /**
     * 根据密码打开门磁
     */
    @Override
    public ServiceResult open(DoorOpenResp doorOpenResp) {
        Optional<DoorPlate> doorOption = doorPlateDao.findByIdAndPassword(doorOpenResp.getId(), doorOpenResp.getPassword());
        ServiceResult result = ServiceResult.ok();
        if (!doorOption.isPresent()) {
            return ServiceResult.error("门牌信息有误");
        } else {
            DoorPlate doorPlate = doorOption.get();
            Optional<DoorMagnetic> doorMagneticOp = doorMagneticDao.findByNumber(doorPlate.getMagneticNum());
            if (!doorMagneticOp.isPresent()) {
                return ServiceResult.error("找不到门磁信息");
            }
            DoorMagnetic doorMagnetic = doorMagneticOp.get();

            String hwId = doorMagnetic.getHuaweiId();
            if (hwId != null && hwId.trim().length() > 0) {
                // 发送华为云开门请求
                result = doorMagneticHWService.open(doorPlate.getMagneticNum());
            }
        }
        return result;
    }

    @Override
    @Transactional
    public ServiceResult statusPC(String doorPlateId) {

        // 判断门牌是否存在
        Optional<DoorPlate> doorPlateOp = doorPlateDao.findById(doorPlateId);
        if (!doorPlateOp.isPresent()) {
            return ServiceResult.error(NO_DOORPLATE_MSG);
        }
        DoorPlate doorPlate = doorPlateOp.get();
        // 根据门牌ID 查询对应的room
        IotDevice sourceDevice = doorPlate.getDevice();
        Optional<DeviceToSpace> d2pOp = deviceToSpaceDao.findByDeviceId(sourceDevice.getId());
        if (!d2pOp.isPresent()) {
            return ServiceResult.error("该门牌未绑定会议室");
        }
        String spaceId = d2pOp.get().getSpace().getId();
        Optional<Room> roomOp = roomDao.findBySpaceId(spaceId);
        if (!roomOp.isPresent()) {
            return ServiceResult.error(NO_ROOM_MSG);
        }

        Room room = roomOp.get();
        String roomId = room.getId();
        DoorPlateStatusResp doorPlateStatusResp = new DoorPlateStatusResp();

        // 赋值门牌基础信息
        doorPlateStatusResp.setDoorPlateId(doorPlate.getId());
        doorPlateStatusResp.setRoomId(roomId);
        doorPlateStatusResp.setRoomName(room.getName());
        doorPlateStatusResp.setDoorPlateName(doorPlate.getName());
        doorPlateStatusResp.setMagneticNum(doorPlate.getMagneticNum());
        doorPlateStatusResp.setPassword(sourceDevice.getPassword());

        // 门牌状态对象
        DoorPlateStatus status = DoorPlateStatus.FREE;
        // 获取当前时间
        Date now = new Date();
        Instant instant = now.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        LocalDateTime nowTime = LocalDateTime.ofInstant(instant, zone);
        // 获取当天所有会议记录
        List<ReservationRecord> resultList = reservationRecordDao.findByDateAndRoomIdOrderByFromAsc(nowTime.toLocalDate(), roomId);

        DoorPlateReservationRecordResp currentReservationRecord = new DoorPlateReservationRecordResp();
        DoorPlateReservationRecordResp nextReservationRecord = new DoorPlateReservationRecordResp();

        // 当天无任何会议
        if (resultList.isEmpty()) {
            doorPlateStatusResp.setDoorPlateStatus(status);
            doorPlateStatusResp.setStatusDescribe(status.getDescribe());
            doorPlateStatusResp.setCurrentReservationRecord(currentReservationRecord);
            doorPlateStatusResp.setNextReservationRecord(nextReservationRecord);
            return ServiceResult.ok(doorPlateStatusResp);
        }

        ReservationRecord rr = null;
        for (int i = 0; i < resultList.size(); i++) {

            // 当前会议数据
            rr = resultList.get(i);
            ReservationRecord next = null;
            if (rr.getStatus().equals(ReservationRecordStatus.CANCEL)) {
                // 会议已取消，跳过这条数据
                continue;
            }
            if (i < resultList.size() - 1) {
                next = resultList.get(i + 1);
            }
            // 获取会议状态
            status = checkCurrentRoomStatus(rr, next);
            if (!status.equals(DoorPlateStatus.FREE)) {

                // 当前会议数据
                currentReservationRecord = getReservationRecordData(rr);
                if (status.equals(DoorPlateStatus.NEXT_ABOUT_TO_BEGIN)
                        && next != null
                        && !next.getStatus().equals(ReservationRecordStatus.CANCEL)
                        && next.getFrom().isEqual(rr.getTo())) {
                    // 下场会议数据
                    nextReservationRecord = getReservationRecordData(next);
                }
                break;
            }
        }

        // 逻辑判断结束，将输出对象中的NEXT_ABOUT_TO_BEGIN状态改为ABOUT_TO_BEGIN
        status = status.equals(DoorPlateStatus.NEXT_ABOUT_TO_BEGIN) ? DoorPlateStatus.ABOUT_TO_BEGIN : status;

        // 赋值状态数据
        doorPlateStatusResp.setDoorPlateStatus(status);
        doorPlateStatusResp.setStatusDescribe(status.getDescribe());
        doorPlateStatusResp.setCurrentReservationRecord(currentReservationRecord);
        doorPlateStatusResp.setNextReservationRecord(nextReservationRecord);

        return ServiceResult.ok(doorPlateStatusResp);
    }

    @Override
    @Transactional
    public ServiceResult openDoor(String id) {
        Optional<DoorMagnetic> doorMagneticOp = doorMagneticDao.findByDeviceId(id);
        ServiceResult result = ServiceResult.ok();
        if (!doorMagneticOp.isPresent()) {
            return ServiceResult.error("找不到门磁信息");
        }
        DoorMagnetic doorMagnetic = doorMagneticOp.get();
        String doorMagneticMac = doorMagnetic.getDoorMagneticMac();
        if (doorMagneticMac != null && doorMagneticMac.trim().length() > 0) {
            // 发送消息到mqtt
            result = doorMagneticMQTTService.open(doorMagnetic.getNumber());
        }

        String hwId = doorMagnetic.getHuaweiId();
        if (hwId != null && hwId.trim().length() > 0) {
            // 发送华为云开门请求
            result = doorMagneticHWService.open(doorMagnetic.getNumber());
        }
        return result;
    }

    /**
     * 根据设备硬件id查询当前门牌状态
     *
     * @param hardwareId 设备硬件id
     * @return 查询结果
     * @author Yang.Lee
     */
    @Override
    @Transactional
    public ServiceResult statusByHardwareId(String hardwareId) {

        Optional<DoorPlate> doorPlateOptional = doorPlateDao.findByHardwareId(hardwareId);
        if (!doorPlateOptional.isPresent()) {
            return ServiceResult.error("门牌不存在");
        }

        return status(doorPlateOptional.get().getId());
    }

    /**
     * 获取门牌状态列表
     *
     * @param roomId 门牌ID
     * @param date   时间
     * @return 门牌状态列表
     * @author Yang.Lee
     * @date 2021/4/21 16:29
     **/
    @Override
    @Transactional
    public List<DoorPlateStatusDetailResp> getStatusDetailList(String roomId, LocalDate date) {

        Optional<Room> roomOptional = roomDao.findById(roomId);
        if (!roomOptional.isPresent()) {
            return createAllDayFree();
        }

        Room room = roomOptional.get();

        // 查询该会议室的开放时间
        List<OpeningHours> openingHoursList = openingHoursDao.findByRoomIdOrderByFromAsc(room.getId());
        // 如果该会议室无开放时间，则直接显示空闲
        if (openingHoursList.isEmpty()) {
            return createAllDayFree();
        }

        // 查询会议列表
        List<ReservationRecord> reservationRecordList = reservationRecordDao.findByDateAndRoomIdOrderByFromAscToAsc(date, room.getId());

        if (reservationRecordList.isEmpty()) {
            return createAllDayFree();
        }

        // 剔除已取消会议
        reservationRecordList = reservationRecordList.stream().filter(obj -> !obj.getStatus().equals(ReservationRecordStatus.CANCEL)).collect(Collectors.toList());
        // 如果剔除了所有已取消的会议后，当天无任何会议，则返回全天空闲
        if (reservationRecordList.isEmpty()) {
            return createAllDayFree();
        }

        List<DoorPlateStatusDetailResp> result = new ArrayList<>();

        LocalDateTime now = LocalDateTime.now();
        // 遍历所有会议记录，构造需要的数据格式
        reservationRecordList.forEach(record -> {

            // 查询发起人信息
            SysUser initiator = sysUserServiceUC.getUserByEmpNo(record.getInitiatorEmpNo());

            // 根据会议时间判断会议状态

            DoorPlateStatus status = getStatus(record, now);
            result.add(DoorPlateStatusDetailResp.builder()
                    .statusDescribe(status.getDescribe())
                    .status(status)
                    .from(DateUtils.formatLocalDateTime(record.getFrom(), DateUtils.PATTERN_TIME1))
                    .to(DateUtils.formatLocalDateTime(record.getTo(), DateUtils.PATTERN_TIME1))
                    .initiator(initiator.getNameZh())
                    .initiatorDepartment(initiator.getDeptName())
                    .topic(record.getTopic())
                    .roomName(record.getRoom().getName())
                    .headImg(initiator.getHeadImgUrl())
                    .build());
        });

        return fillFreeRecord(result, "00:00", "23:59");
    }

    /**
     * 填充空闲时间
     *
     * @param sourceList      会议列表
     * @param roomFromTimeStr 会议室开放时间
     * @param roomEndTimeStr  会议室结束时间
     * @return 填充后的会议列表
     * @author Yang.Lee
     * @date 2021/4/25 14:33
     **/
    private List<DoorPlateStatusDetailResp> fillFreeRecord(@NotNull List<DoorPlateStatusDetailResp> sourceList, String roomFromTimeStr, String roomEndTimeStr) {

        List<DoorPlateStatusDetailResp> result = new ArrayList<>();

        if (!sourceList.get(0).getFrom().equals(roomFromTimeStr)) {
            result.add(createFreeStatus(roomFromTimeStr, sourceList.get(0).getFrom()));
        }

        String lastTo = null;

        for (DoorPlateStatusDetailResp record : sourceList) {
            if (lastTo != null) {

                LocalTime lastToTime = LocalTime.parse(lastTo);
                LocalTime fromTime = LocalTime.parse(record.getFrom());


                if(lastToTime.isBefore(fromTime)){
                    result.add(createFreeStatus(lastTo, record.getFrom()));
                }
            }
            result.add(record);
            lastTo = record.getTo();
        }

        if (!sourceList.get(sourceList.size() - 1).getTo().equals(roomEndTimeStr)) {
            result.add(createFreeStatus(sourceList.get(sourceList.size() - 1).getTo(), roomEndTimeStr));
        }

        return result;
    }

    /**
     * 根据指定时间获取会议状态
     *
     * @param record   会议记录
     * @param dateTime 指定时间
     * @return 会议状态
     * @author Yang.Lee
     * @date 2021/4/25 14:02
     **/
    private DoorPlateStatus getStatus(ReservationRecord record, LocalDateTime dateTime) {

        long startDiff = DateUtils.betweenTime(dateTime, record.getFrom(), DateUnit.MINUTE);

        if (startDiff > 0 && startDiff <= ABOUT_TO_BEGIN_INTERVAL){
            return DoorPlateStatus.ABOUT_TO_BEGIN;
        } else if(record.getTo().compareTo(dateTime) < 0) {
            return DoorPlateStatus.END;
        } else if (record.getFrom().compareTo(dateTime) > 0) {
            return DoorPlateStatus.IN_FUTURE;
        } else {
            return DoorPlateStatus.IN_PROCESS;
        }
    }

    /**
     * 创建整天空闲的状态卡集合
     *
     * @return 状态卡集合
     * @author Yang.Lee
     * @date 2021/4/21 17:06
     **/
    private List<DoorPlateStatusDetailResp> createAllDayFree() {

        List<DoorPlateStatusDetailResp> result = new ArrayList<>();
        result.add(createFreeStatus());

        return result;
    }

    /**
     * 创建空闲状态卡（00：00 - 23：59）
     *
     * @return 空闲会议状态对象
     * @author Yang.Lee
     * @date 2021/4/21 17:00
     **/
    private DoorPlateStatusDetailResp createFreeStatus() {

        String from = "00:00";
        String to = "23:59";
        return createFreeStatus(from, to);
    }

    /**
     * 创建空闲状态卡
     *
     * @param from 空闲开始时间
     * @param to   空闲结束时间
     * @return 空闲会议状态对象
     * @author Yang.Lee
     * @date 2021/4/21 17:01
     **/
    private DoorPlateStatusDetailResp createFreeStatus(String from, String to) {

        return DoorPlateStatusDetailResp.builder()
                .from(from)
                .to(to)
                .status(DoorPlateStatus.FREE)
                .statusDescribe(DoorPlateStatus.FREE.getDescribe())
                .build();
    }
}
