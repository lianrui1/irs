package cn.com.cowain.ibms.service.bean.amap.geo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 道路交叉口
 * @author Yang.Lee
 * @date 2021/4/20 10:58
 */
@Data
public class RoadInter {

    /**
     * 交叉路口到请求坐标的距离, 单位：米
     **/
    private String distance;

    /**
     * 方位
     **/
    private String direction;

    /**
     * 路口经纬度
     **/
    private String location;

    /**
     * 第一条道路id
     **/
    @JSONField(name = "first_id")
    private String firstId;

    /**
     * 第一条道路名称
     **/
    @JSONField(name = "first_name")
    private String firstName;

    /**
     * 第二条道路id
     **/
    @JSONField(name = "second_id")
    private String secondId;

    /**
     * 第二条道路名称
     **/
    @JSONField(name = "second_name")
    private String secondName;
}
