package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author Yang.Lee
 * @date 2022/3/17 11:03
 */
@Data
@Entity
@Table(name = "iot_elevator_spot_user")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_elevator_spot_user", comment = "梯控点权限表")
public class ElevatorSpotUser extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "elevator_spot_id", columnDefinition = "varchar(36) COMMENT '梯控点Id'")
    private ElevatorSpot elevatorSpot;

    @Column(name = "user_hr_id", nullable = false, columnDefinition = "varchar(50) COMMENT '用户工号'")
    private String userHrId;

    @Column(name = "user_name", columnDefinition = "varchar(50) COMMENT '用户姓名'")
    private String userName;

    @Column(name = "dept_id", columnDefinition = "varchar(50) COMMENT '部门ID'")
    private String deptId;

    @Column(name = "is_staff", columnDefinition = "int(1) COMMENT '人员类型'")
    private Integer isStaff;

    @Column(name = "access_start_time", nullable = false, columnDefinition = "datetime COMMENT '用户通行生效开始时间'")
    private LocalDateTime accessStartTime;

    @Column(name = "access_end_time", nullable = false, columnDefinition = "datetime COMMENT '用户通行生效结束时间'")
    private LocalDateTime accessEndTime;

    @ManyToOne
    @JoinColumn(name = "space_id", columnDefinition = "varchar(36) COMMENT 'FK'")
    private Space space;

    @Column(name = "face_img",  columnDefinition = "varchar(300) COMMENT '人脸图片'")
    private String faceImg;

}
