package cn.com.cowain.ibms.enumeration.integration_control;

/**
 * 选择设备类型
 *
 * @author: yanzy
 * @date: 2022/9/19 09:54
 */
public enum SelectDeviceType {

    /**
     * 各种灯
     */
    FOUR_WAY_SWITCH("灯"),

    /**
     * 空调
     */
    AIR_CONDITIONER("空调");

    String name;

    SelectDeviceType(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
