package cn.com.cowain.ibms.enumeration.iot;

/**
 * @author lianrui
 * @date 2022/8/19 15:10
 */
public enum AuthType {
    ALL_PASS("0", "全部开放"),

    STAFF_PASS("1", "全员工开放"),

    OTHER("2", "其他");

    private final String value;

    private final String type;

    AuthType(String value, String type) {
        this.value = value;
        this.type = type;
    }

    public String getValue() {
        return this.value;
    }

    public String getDesc() {
        return this.type;
    }

}
