package cn.com.cowain.ibms.service.iot;

import cn.com.cowain.ibms.enumeration.iot.DeviceLinkStatus;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.iot.DeviceLinkEditReq;
import cn.com.cowain.ibms.rest.req.iot.DeviceLinkPageReq;
import cn.com.cowain.ibms.rest.resp.iot.DeviceLinkPageResp;
import cn.com.cowain.ibms.rest.resp.iot.doorplate.DeviceLinkDetailResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;

/**
 * 设备联动相关Service
 */
public interface DeviceLinkService {

    /**
     * 创建联动
     *
     * @param req 请求参数
     * @return 结果，成功时返回新数据主键ID
     */
    ServiceResult create(DeviceLinkEditReq req);

    /**
     * 编辑联动信息
     *
     * @param id  联动ID
     * @param req 请求参数
     * @return 修改结果
     */
    ServiceResult update(String id, DeviceLinkEditReq req);

    /**
     * 删除联动信息
     *
     * @param id 联动ID
     * @return 删除结果
     */
    ServiceResult delete(String id);

    /**
     * 编辑联动状态
     *
     * @param id     联动ID
     * @param status 修改后的状态
     * @return 编辑结果
     */
    ServiceResult updateStatus(String id, DeviceLinkStatus status);

    /**
     * 获取分页数据
     *
     * @param req 请求参数
     * @return 查询结果
     */
    PageBean<DeviceLinkPageResp> getPage(DeviceLinkPageReq req);

    /**
     * 查询联动详情
     *
     * @param id 联动ID
     * @return 查询结果
     */
    DeviceLinkDetailResp getDetail(String id);
}
