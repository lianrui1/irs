package cn.com.cowain.ibms.rest.iot;

import cn.com.cowain.ibms.annotation.Idempotent;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.Sorts;
import cn.com.cowain.ibms.rest.req.distinguish.ImportAddFailedReq;
import cn.com.cowain.ibms.rest.req.iot.*;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.UploadResp;
import cn.com.cowain.ibms.rest.resp.distinguish.ImportAddFailedResultResp;
import cn.com.cowain.ibms.rest.resp.distinguish.StaffGroupResp;
import cn.com.cowain.ibms.rest.resp.iot.*;
import cn.com.cowain.ibms.rest.resp.space.ProjectSpaceTreeResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.iot.AccessControlSpotService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.ExportExcel;
import cn.com.cowain.ibms.utils.PicUtils;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static cn.com.cowain.ibms.utils.IConst.*;

/**
 * 门禁点controller
 *
 * @author Yang.Lee
 * @date 2021/11/7 15:23
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/accessControlSpot")
@Api(tags = MODULE_ACCESS_CONTROL_SPOT)
public class AccessControlSpotController {

    @Resource
    private AccessControlSpotService accessControlSpotService;

    /**
     * 创建门禁点
     *
     * @param req           请求参数
     * @param bindingResult 校验对象
     * @return 创建结果，成功时返回数据主键id
     */
    @Idempotent("/accessControlSpot/post")
    @PostMapping
    @ApiOperation(value = "创建门禁点", tags = {MODULE_ACCESS_CONTROL_SPOT, V20})
    public ResponseEntity<JsonResult<IdResp>> create(@RequestBody @Validated AccessControlSpotEditReq req, BindingResult bindingResult) {

        ServiceResult serviceResult = accessControlSpotService.create(req);
        if (!serviceResult.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(serviceResult.getObject()), null, serviceResult.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.ok((IdResp) serviceResult.getObject()));
    }

    /**
     * 编辑门禁点
     *
     * @param id            门禁点ID
     * @param req           请求参数
     * @param bindingResult 校验对象
     * @return 编辑结果
     * @author Yang.Lee
     * @date 2021/11/7 15:36
     **/
    @PutMapping("/{id}")
    @ApiOperation(value = "编辑门禁点", tags = {MODULE_ACCESS_CONTROL_SPOT, V20})
    public ResponseEntity<JsonResult<String>> update(@PathVariable String id, @RequestBody @Validated AccessControlSpotEditReq req, BindingResult bindingResult) {

        ServiceResult serviceResult = accessControlSpotService.update(id, req);
        if (!serviceResult.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(serviceResult.getObject()), null, serviceResult.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    /**
     * 查询设门禁点分页列表
     *
     * @param req 查询参数
     * @return 查询结果
     */
    @GetMapping("/page")
    @ApiOperation(value = "查询设门禁点分页列表", tags = {MODULE_ACCESS_CONTROL_SPOT, V20})
    public ResponseEntity<JsonResult<PageBean<AccessControlSpotPageResp>>> getPage(AccessControlSpotPageReq req) {

        req.addOrderDesc(Sorts.CREATED_TIME);
        return ResponseEntity.ok(JsonResult.ok(accessControlSpotService.getPage(req)));
    }

    @PostMapping("/approver/page")
    @ApiOperation(value = "分配人查询设门禁点分页列表", tags = {MODULE_ACCESS_CONTROL_SPOT, V26})
    public ResponseEntity<JsonResult<PageBean<AccessControlSpotPageResp>>> getAccessControlSpotPage(@RequestBody AccessControlSpotPageReq req) {

        req.addOrderDesc(Sorts.CREATED_TIME);
        return ResponseEntity.ok(JsonResult.ok(accessControlSpotService.getAccessControlSpotPage(req)));
    }

    @GetMapping("/staffGroup/{id}")
    @ApiOperation(value = "查询门禁点人员组列表", tags = {MODULE_ACCESS_CONTROL_SPOT, V20})
    public ResponseEntity<JsonResult<List<StaffGroupResp>>> getStaffGroupByAccessControlSpotId(@PathVariable @ApiParam("门禁点ID") String id) {

        return ResponseEntity.ok(JsonResult.ok(accessControlSpotService.getStaffGroupByAccessControlSpotId(id)));
    }


    /**
     * 删除门禁点
     *
     * @param id 门禁点 ID
     * @return 删除结果
     * @author Yang.Lee
     * @date 2021/10/21 16:43
     **/
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除门禁点", tags = {MODULE_ACCESS_CONTROL_SPOT, V20})
    public ResponseEntity<JsonResult<String>> delete(@PathVariable String id) {

        ServiceResult serviceResult = accessControlSpotService.delete(id);
        if (!serviceResult.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(serviceResult.getObject()), null, serviceResult.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.ok(null));
    }

    /**
     * 查询门禁点详情
     *
     * @param id 门禁点ID
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/10/21 11:09
     **/
    @GetMapping("/{id}")
    @ApiOperation(value = "查询门禁点详情", tags = {MODULE_ACCESS_CONTROL_SPOT, V20})
    public ResponseEntity<JsonResult<AccessControlSpotDetailResp>> get(@PathVariable String id) {

        return ResponseEntity.ok(JsonResult.ok(accessControlSpotService.get(id)));
    }

    /**
     * 人员通行分页
     *
     * @param personPassPageReq
     * @return
     */
    @GetMapping("/pass/page")
    @ApiOperation(value = "人员通行分页", tags = {MODULE_ACCESS_CONTROL_SPOT, V19})
    public ResponseEntity<JsonResult<PageBean<PersonPassPageResp>>> passPage(@Validated PersonPassPageReq personPassPageReq) {

        return ResponseEntity.ok(JsonResult.ok(null));
    }

    /**
     * 人员通行权限列表导出
     *
     * @param req
     */
    @GetMapping("/download/excel")
    @ApiOperation(value = "人员通行权限列表导出", tags = {MODULE_ACCESS_CONTROL_SPOT, V19})
    public void downloadExcel(@Validated PersonPassPageReq req) {
        String[] fileds = {"硬件SN", "设备名称CH", "设备名称EN", "所属产品", "设备类型", "品牌", "型号", "创建时间"};
        String[] attr = {"hardwareSN", "nameZH", "nameEN", "productName", "deviceType", "brand", "model", "createTime"};
        ExportExcel.setFilesAndAttributes(fileds, attr);
        ExportExcel.writeExcel("deviceList", null);
    }

    /**
     * 部门门禁点权限
     *
     * @param departmentPassReq
     * @return
     */
    @PostMapping("/department/permission")
    @ApiOperation(value = "部门门禁点权限", tags = {MODULE_ACCESS_CONTROL_SPOT, V19})
    public ResponseEntity<JsonResult<String>> departmentPass(@RequestBody @Validated DepartmentPassReq departmentPassReq) {
        return ResponseEntity.ok(JsonResult.ok(null));
    }

    /**
     * @param personPermissionReq
     * @param result
     * @return
     * @description 人员权限列表
     * @author tql
     * @date 21-11-29
     */
    @GetMapping("/person/list")
    @ApiOperation(value = "人员权限列表(分页)", tags = {MODULE_ACCESS_CONTROL_SPOT, V19})
    public ResponseEntity<JsonResult<PageBean<PersonPermissionPageResp>>> personPage(@Validated PersonPermissionReq personPermissionReq, BindingResult result) {
        personPermissionReq.addOrderDesc("deviceAccessRecord.dataTime");
        PageBean<PersonPermissionPageResp> pageBean = accessControlSpotService.personPage(personPermissionReq);
        return ResponseEntity.ok(JsonResult.ok(pageBean));

    }


    /**
     * @param personPermissionReq
     * @param result
     * @return
     * @description 人员权限导出
     * @author tql
     * @date 21-11-29
     */
    @GetMapping("/person/download/excel")
    @ApiOperation(value = "人员权限导出", tags = {MODULE_ACCESS_CONTROL_SPOT, V19})
    public void personDownload(@Validated PersonPermissionReq personPermissionReq, BindingResult result) {
        // 默认根据创建时间倒叙查询数据
        personPermissionReq.addOrderDesc(Sorts.CREATED_TIME);
        PageBean<PersonPermissionPageResp> pageBean = accessControlSpotService.personPage(personPermissionReq);
        log.info("导出参数： {}", pageBean.getList());
        String[] fileds = {"通行人员信息", "人员所属部门", "人员类型", "门禁点", "门禁类型", "门禁点位置", "通行时间", "状态"};
        String[] attr = {"personMsg", "department", "personType", "controlSpot", "controlType", "controlSpotLocation", "passTime", "status"};
        ExportExcel.setFilesAndAttributes(fileds, attr);
        ExportExcel.writeExcel("通行记录", pageBean.getList());
    }


    /**
     * @param departmentPersonReq
     * @param result
     * @return
     * @description 部门人员门禁点权限
     * @author tql
     * @date 21-11-29
     */
    @PostMapping("/department/person")
    @ApiOperation(value = "部门人员门禁点权限", tags = {MODULE_ACCESS_CONTROL_SPOT, V19})
    public ResponseEntity<JsonResult<String>> departPerson(@RequestBody @Validated DepartmentPersonReq departmentPersonReq, BindingResult result) {
        ServiceResult serviceResult = accessControlSpotService.departPerson(departmentPersonReq);
        if (!serviceResult.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(serviceResult.getObject()), null, serviceResult.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @GetMapping("/tree")
    @ApiOperation(value = "PC查询门禁点树", tags = {MODULE_ACCESS_CONTROL_SPOT, V20})
    public ResponseEntity<JsonResult<List<ProjectSpaceTreeResp>>> spaceTree() {

        return ResponseEntity.ok(JsonResult.ok("OK", accessControlSpotService.getListWithTree(true)));
    }

    /**
     * 获取门禁点树接口（只显示有门禁点的树及项目）
     *
     * @return 门禁点树
     * @author Yang.Lee
     * @date 2021/12/13 14:56
     **/
    @GetMapping("/list/tree")
    @ApiOperation(value = "PC查询门禁点列表（树）", tags = {MODULE_ACCESS_CONTROL_SPOT, V20})
    public ResponseEntity<JsonResult<List<ProjectSpaceTreeResp>>> getListByTree() {

        return ResponseEntity.ok(JsonResult.ok("OK", accessControlSpotService.getListWithTree(false)));
    }


    @PostMapping("/user/access/check")
    @ApiOperation(value = "查询用户权限（并开门）", tags = {MODULE_ACCESS_CONTROL_SPOT, V22})
    public ResponseEntity<JsonResult<String>> checkUserAccess(@RequestBody UserAccessCheckReq req) {

        ServiceResult result = accessControlSpotService.checkUserAccess(req.getUserHrId(), req.getDeviceSn());

        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error((String) result.getObject(), null, result.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.ok(null));
    }

    /**
     * 门禁点二维码
     *
     * @author: yanzy
     * @data: 2022/3/8 17:03:23
     */
    @GetMapping("access/qrcode/{id}")
    @ApiOperation(value = "门禁点二维码", tags = {ACCESS_CONTROL_SPOT_APPLICATION, V26})
    public ResponseEntity<JsonResult<String>> accessQrCode(@PathVariable String id) {
        return null;
    }

    /**
     * 查询我对门禁点拥有的权限信息
     *
     * @param token
     * @param spotId
     * @author wei.cheng
     * @date 2022/3/9 4:35 下午
     **/
    @GetMapping("/user/me/permission")
    @ApiOperation(value = "查询我对门禁点拥有的权限信息", tags = {MODULE_ACCESS_CONTROL_SPOT, V26})
    public ResponseEntity<JsonResult<GetMyPermissionOfAccessControlSpotResp>> getMyPermissionOfAccessControlSpot(@RequestHeader(name = "Authorization", required = false) @ApiParam(value = "当前用户token") String token,
                                                                                                                 @RequestParam(value = "spotId", required = false) @ApiParam(value = "门禁点ID") String spotId,
                                                                                                                 @RequestParam(value = "doorMagnetismNo", required = false) @ApiParam(value = "门磁编号") String doorMagnetismNo,
                                                                                                                 @RequestParam(value = "guestMobile", required = false) @ApiParam(value = "访客号码") String guestMobile) {
        ServiceResult serviceResult = accessControlSpotService.getMyPermissionOfAccessControlSpot(token, spotId, doorMagnetismNo, guestMobile);
        if (!serviceResult.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(serviceResult.getObject()), null, serviceResult.getErrorCode()));
        }
        return ResponseEntity.ok(JsonResult.ok((GetMyPermissionOfAccessControlSpotResp) serviceResult.getObject()));
    }

    @GetMapping("/doorMagnetic/openDoor")
    @ApiOperation(value = "门磁开门", tags = {MODULE_QRCODE, V28})
    public ResponseEntity<JsonResult<String>> openDoor(@RequestHeader(name = "Authorization", required = false) @ApiParam(value = "当前用户token") String token,
                                                       @RequestParam(value = "spotId", required = false) @ApiParam(value = "门禁点ID") String spotId,
                                                       @RequestParam(value = "doorMagnetismNo", required = false) @ApiParam(value = "门磁编号") String doorMagnetismNo,
                                                       @RequestParam(value = "guestMobile", required = false) @ApiParam(value = "访客号码") String guestMobile) {

        ServiceResult serviceResult = accessControlSpotService.doorMagneticOpenDoor(token, spotId, doorMagnetismNo, guestMobile);
        if (!serviceResult.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(serviceResult.getObject()), null, serviceResult.getErrorCode()));
        }
        return ResponseEntity.ok(JsonResult.ok((String) serviceResult.getObject()));
    }


    @PutMapping("/user/me/face/update")
    @ApiOperation(value = "更新门禁点中当前用户人脸照片", tags = {MODULE_ACCESS_CONTROL_SPOT, V26})
    public ResponseEntity<JsonResult<UploadResp>> updateUserFace(@RequestHeader(name = "Authorization", required = false) @ApiParam(value = "当前用户token") String token,
                                                                 @RequestParam("spotId") @ApiParam(value = "门禁点ID", required = true) String spotId,
                                                                 @RequestParam(value = "guestMobile", required = false) @ApiParam(value = "访客号码") String guestMobile,
                                                                 MultipartFile file) {
        try {
            if (file.getBytes().length > 200 * 1024) {
                InputStream inputStream = PicUtils.getPhotoFitHIK(file);
                if (Objects.isNull(inputStream)) {
                    return ResponseEntity.ok(JsonResult.error("文件不能为空", null, ErrConst.E01));
                }
                file = new MockMultipartFile(file.getName(), file.getOriginalFilename(), file.getContentType(), IOUtils.toByteArray(inputStream));
            }
        } catch (IOException e) {
            return ResponseEntity.ok(JsonResult.error("文件处理失败", null, ErrConst.E01));
        }
        ServiceResult serviceResult = accessControlSpotService.updateUserFace(token, spotId, guestMobile, file);
        if (!serviceResult.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(serviceResult.getObject()), null, serviceResult.getErrorCode()));
        }
        return ResponseEntity.ok(JsonResult.ok((UploadResp) serviceResult.getObject()));
    }

    @PostMapping("/batch/add")
    @ApiOperation(value = "批量添加人员通行权限", tags = {ACCESS_CONTROL_SPOT_APPROVER, V27})
    public ResponseEntity<JsonResult<Object>> batchAdd(MultipartFile file, String[] spotIds) {
        ServiceResult serviceResult = accessControlSpotService.userBatchAdd(file, Arrays.asList(spotIds));
        if (!serviceResult.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(serviceResult.getObject().toString(), null, serviceResult.getErrorCode()));
        }
        return ResponseEntity.ok(JsonResult.ok(serviceResult.getObject()));
    }

    @PostMapping("/batch/delete")
    @ApiOperation(value = "批量删除人员通行权限", tags = {ACCESS_CONTROL_SPOT_APPROVER, V27})
    public ResponseEntity<JsonResult<Object>> batchDelete(MultipartFile file, String[] spotIds) {
        ServiceResult serviceResult = accessControlSpotService.userBatchDelete(file, Arrays.asList(spotIds));
        if (!serviceResult.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(serviceResult.getObject().toString(), null, serviceResult.getErrorCode()));
        }
        return ResponseEntity.ok(JsonResult.ok(serviceResult.getObject()));
    }

    @GetMapping("/batch/add/result")
    @ApiOperation(value = "查询批量添加导入人员结果", tags = {ACCESS_CONTROL_SPOT_APPROVER, V27})
    public ResponseEntity<JsonResult<ImportAddFailedResultResp>> importAddFailedResult(ImportAddFailedReq req) {

        ServiceResult serviceResult = accessControlSpotService.importAddFailedResult(req);
        if (!serviceResult.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(serviceResult.getObject().toString(), null, serviceResult.getErrorCode()));
        }
        return ResponseEntity.ok(JsonResult.ok((ImportAddFailedResultResp) serviceResult.getObject()));
    }

    @GetMapping("/download/importFailed/add")
    @ApiOperation(value = "下载批量添加导入失败的数据", tags = "${project.sprint}")
    public void getUploadAccessRulesErrorFile(HttpServletResponse response, @RequestParam String errorDataId) {

        byte[] bytes = accessControlSpotService.getImportErrorDataFileByte(errorDataId);

        response.addHeader("content-type", "application/octet-stream;charset=UTF-8");
        response.addHeader("Content-Disposition", "attachment;filename=" + errorDataId + ".xlsx");
        response.addHeader("Content-Length", String.valueOf(bytes.length));
        OutputStream stream = null;
        try {
            stream = response.getOutputStream();
            stream.write(bytes);
            stream.flush();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }

    @PutMapping("/synchronization/{id}")
    @ApiOperation(value = "门禁点数据同步", tags = {MODULE_ACCESS_CONTROL_SPOT, V29})
    public ResponseEntity<JsonResult<String>> synchronization(@PathVariable String id) {

        ServiceResult serviceResult = accessControlSpotService.synchronizationUser(id);
        if (!serviceResult.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(serviceResult.getObject()), null, serviceResult.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }
}
