package cn.com.cowain.ibms.mq.consumer;

import cn.com.cowain.ibms.mq.Topic;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * UserBatchUnBindMessageConsumer
 *
 * @author: yanzy
 * @date: 2022/5/31 15:53
 */
@Slf4j
@RocketMQMessageListener(
        topic = "${rocketmq.env}-" + Topic.USER_BATCH_UNBIND_RETURN,
        consumerGroup = "${rocketmq.env}-" + Topic.USER_BATCH_UNBIND_RETURN
)
@Component
public class UserBatchUnBindMessageConsumer implements RocketMQListener<String> {

//    @Resource
//    private AbilityService abilityService;

    @Override
    public void onMessage(@RequestBody String data) {
//        try {
//            // 解析数据
//            UserBindMessage userBindMessage = JSON.parseObject(data, UserBindMessage.class);
//            log.debug("mq返回结果{}",userBindMessage);
//
//            //修改异常人员删除状态
//            abilityService.changeUserBatchUnBindImportStatus(userBindMessage);
//
//        }catch (Exception e){
//            log.error(e.getMessage(),e);
//        }
    }
}
