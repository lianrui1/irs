package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.AccessControlSpotKSHTMemberGroup;
import cn.com.cowain.ibms.enumeration.ability.Ability;
import cn.com.cowain.ibms.enumeration.iot.KSHTMemberGroupType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * @author Yang.Lee
 * @date 2022/2/14 18:02
 */
public interface AccessControlSpotKSHTMemberGroupDao extends JpaRepository<AccessControlSpotKSHTMemberGroup, String>, JpaSpecificationExecutor<AccessControlSpotKSHTMemberGroup> {

    /**
     * 根据门禁点ID查询员工组列表
     *
     * @param spotId 门禁点ID
     * @return 关系列表
     * @author Yang.Lee
     * @date 2022/2/15 14:08
     **/
    List<AccessControlSpotKSHTMemberGroup> findByAccessControlSpotId(String spotId);


    /**
     * @param ability 开放能力
     * @return
     * @description 根据能力查询
     * @author tql
     * @date 22-5-11
     */
    List<AccessControlSpotKSHTMemberGroup> findByOpenAbilityAbility(Ability ability);

    /**
     * 根据门禁点ID和员工组类型查询
     *
     * @param spotId  门禁点ID
     * @param type    员工组ID
     * @param ability 开放能力
     * @return 关系
     * @author Yang.Lee
     * @date 2022/2/15 15:40
     **/
    Optional<AccessControlSpotKSHTMemberGroup> findByAccessControlSpotIdAndGroupTypeAndOpenAbilityAbility(String spotId, KSHTMemberGroupType type, Ability ability);

    /**
     * 根据门禁点id集合查询
     *
     * @param spotIdCollection 门禁点ID集合
     * @return 关系列表
     * @author Yang.Lee
     * @date 2022/2/15 19:56
     **/
    List<AccessControlSpotKSHTMemberGroup> findByAccessControlSpotIdInAndGroupTypeAndOpenAbilityAbility(Collection<String> spotIdCollection, KSHTMemberGroupType type, Ability ability);

    /**
     * 根据门禁点id集合查询
     *
     * @param spotIdCollection 门禁点ID集合
     * @return 关系列表
     * @author Yang.Lee
     * @date 2022/2/15 19:56
     **/
    List<AccessControlSpotKSHTMemberGroup> findByAccessControlSpotIdInAndGroupType(Collection<String> spotIdCollection, KSHTMemberGroupType type);


    /**
     * 根据开放能力及门禁点查询员工组
     *
     * @param spotId  门禁点
     * @param ability 开放能力
     * @return 员工组信息
     * @author Yang.Lee
     * @date 2022/2/16 13:55
     **/
    List<AccessControlSpotKSHTMemberGroup> findByAccessControlSpotIdAndOpenAbilityAbility(String spotId, Ability ability);


    /**
     * 根据门禁点id集合查询
     *
     * @param spotIdCollection 门禁点ID集合
     * @return 关系列表
     * @author Yang.Lee
     * @date 2022/2/15 19:56
     **/
    List<AccessControlSpotKSHTMemberGroup> findByAccessControlSpotIdIn(Collection<String> spotIdCollection);

    /**
     * 根据门禁点id集合 能力 人员组类型查询
     *
     * @param spotIdCollection
     * @param ability
     * @param type
     * @return
     */
    List<AccessControlSpotKSHTMemberGroup> findByAccessControlSpotIdInAndOpenAbilityAbilityAndGroupType(Collection<String> spotIdCollection, Ability ability, KSHTMemberGroupType type);

    /**
     * 根据设备SN集合拆线呢
     *
     * @param spotDeviceSnCollection 门禁点设备SN集合
     * @return 关系列表
     * @author Yang.Lee
     * @date 2022/3/7 14:20
     **/
    List<AccessControlSpotKSHTMemberGroup> findByAccessControlSpotDeviceSnIn(Collection<String> spotDeviceSnCollection);
}
