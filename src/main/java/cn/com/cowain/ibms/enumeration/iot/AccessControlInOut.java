package cn.com.cowain.ibms.enumeration.iot;

/**
 * @author Yang.Lee
 * @date 2022/2/17 15:10
 */
public enum AccessControlInOut {

    IN("进"),

    OUT("出"),

    OTHER("其他");

    AccessControlInOut (String name){
        this.name = name;
    }
    String name;

    public String getName(){
        return this.name;
    }

}
