package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.enumeration.AlertCaptureorStatus;
import cn.com.cowain.ibms.enumeration.AlertSubType;
import cn.com.cowain.ibms.enumeration.AlertType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author Yang.Lee
 * @date 2022/3/17 11:10
 */
@Data
@Entity
@Table(name = "iot_alert_captureor")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_alert_captureor", comment = "警戒抓拍表")
public class AlertCaptureor extends BaseEntity {

    @Column(name = "name", nullable = false, columnDefinition = "varchar(30) COMMENT '名称'")
    private String name;

    @Column(name = "address", columnDefinition = "varchar(100) COMMENT '具体位置'")
    private String address;

    @ManyToOne
    @JoinColumn(name = "space_id", nullable = false, columnDefinition = "varchar(36) COMMENT '所属空间ID FK'")
    private Space space;

    @ManyToOne
    @JoinColumn(name = "device_id", columnDefinition = "varchar(36) COMMENT '关联设备ID FK'")
    private IotDevice device;

    @ManyToOne
    @JoinColumn(name = "alert_spot_id", columnDefinition = "varchar(36) COMMENT '关联设备ID FK'")
    private AlertSpot alertSpot;

    @Enumerated(EnumType.STRING)
    @Column(name = "alert_type", columnDefinition = "varchar(100) COMMENT '抓拍类型'")
    private AlertType alertType;

    @Enumerated(EnumType.STRING)
    @Column(name = "alert_type2", columnDefinition = "varchar(100) COMMENT '抓拍子类型'")
    private AlertSubType alertType2;

    @Column(name = "alert_time", columnDefinition = "datetime COMMENT '抓拍时间'")
    private LocalDateTime alertTime;

    @Column(name = "snap_picture", columnDefinition = "varchar(300) COMMENT '抓拍快照'")
    private String snapPicture;

    @Column(name = "user_hr_id", columnDefinition = "varchar(100) COMMENT '抓拍用户工号'")
    private String userHrId;

    @Column(name = "user_name", columnDefinition = "varchar(100) COMMENT '抓拍用户姓名'")
    private String userName;

    @Column(name = "user_department", columnDefinition = "varchar(100) COMMENT '抓拍用户部门'")
    private String userDepartment;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", columnDefinition = "varchar(20) COMMENT '查看状态'")
    private AlertCaptureorStatus status;

}
