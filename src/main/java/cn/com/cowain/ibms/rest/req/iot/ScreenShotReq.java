package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author feng
 * @title: MonitorReq
 * @projectName bims
 * @Date 2021/11/25 13:34
 */
@Data
public class ScreenShotReq  {
    @ApiModelProperty(value = "站点id")
  String shotSitId;
    @ApiModelProperty(value = "图片url")
  String imageUrl;
}
