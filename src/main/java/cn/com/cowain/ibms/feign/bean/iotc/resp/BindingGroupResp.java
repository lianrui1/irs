package cn.com.cowain.ibms.feign.bean.iotc.resp;

import lombok.Data;

/**
 * @author feng
 * @title: BindingGroupResp
 * @projectName ibms
 * @Date 2022/1/12 14:31
 */
@Data
public class BindingGroupResp<T> {
    Integer status;
    String msg;
    String code;
    T data;
}
