package cn.com.cowain.ibms.rest.resp.office;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author feng
 * @title: OfficeSpaceResp
 * @projectName bims
 * @Date 2021/12/16 14:15
 */
@Data
public class OfficeSpaceResp {
    @ApiModelProperty(value = "办公室ID", required = true)

    private String officeId;

    @ApiModelProperty(value = "办公室名称", required = true)
    private String officeName;
    @ApiModelProperty(value = "办公室名称", required = true)
    private String alisa;
}
