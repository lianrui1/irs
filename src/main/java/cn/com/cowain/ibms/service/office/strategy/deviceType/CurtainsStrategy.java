package cn.com.cowain.ibms.service.office.strategy.deviceType;

import cn.com.cowain.ibms.dao.iot.CurtainDao;
import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.entity.iot.Curtain;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.rest.resp.device.CurtainResp;
import cn.com.cowain.ibms.rest.resp.device.OfficeCommonDeviceResp;
import cn.com.cowain.ibms.rest.resp.device.centralized.CentralizedControlDeviceFilter;
import cn.com.cowain.ibms.rest.resp.office.CurtainDetailResp;
import cn.com.cowain.ibms.rest.resp.office.ShowDeviceResp;
import cn.com.cowain.ibms.service.office.strategy.DeviceTypeStrategy;
import cn.hutool.extra.spring.SpringUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.Optional;

@Log4j2
public class CurtainsStrategy implements DeviceTypeStrategy {


    private final DeviceDao deviceDao= SpringUtil.getBean(DeviceDao.class);

    private final CurtainDao curtainDao= SpringUtil.getBean(CurtainDao.class);


    @Override
    public void addCommonDeviceResp(ShowDeviceResp resp, List<OfficeCommonDeviceResp> commonDeviceRespList) {
        CurtainDetailResp curtainDetailResp=new CurtainDetailResp();
        // 判断该设备是否存在
        Optional<IotDevice> byHwDeviceId = deviceDao.findByHwDeviceId(resp.getHwDeviceId());
        if (byHwDeviceId.isPresent()) {
            IotDevice device = byHwDeviceId.get();
            Optional<Curtain> byDevice = curtainDao.findByDevice(device);
            // 判断该空调是否是首次操作 是则返回空 不是则返回上次操作命令参数
            CurtainResp curtainResp = new CurtainResp();
            if (byDevice.isPresent()) {
                Curtain curtain = byDevice.get();
                curtainResp.setAction(curtain.getAction());
                curtainDetailResp.setHwStatus(String.valueOf(curtain.getDevice().getHwStatus()));
            } else {
                curtainResp.setAction("");
            }
            BeanUtils.copyProperties(curtainResp,curtainDetailResp);
        } else {
            return;
        }
        curtainDetailResp.setHwDeviceId(resp.getHwDeviceId());
        curtainDetailResp.setId(resp.getDeviceId());
        curtainDetailResp.setDeviceName(resp.getDeviceName());
        commonDeviceRespList.add(new OfficeCommonDeviceResp(curtainDetailResp, resp.getDeviceTypeName()));
    }

    @Override
    public void filterDevicePropertiesStatus(IotDevice iotDevice, CentralizedControlDeviceFilter controlDeviceFilter, String action) {

    }


}
