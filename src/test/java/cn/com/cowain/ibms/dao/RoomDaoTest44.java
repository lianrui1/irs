package cn.com.cowain.ibms.dao;

import cn.com.cowain.ibms.entity.*;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.bean.ParticipantBean;
import cn.com.cowain.ibms.rest.bean.TimeSlotBean;
import cn.com.cowain.ibms.rest.req.OpeningHoursReq;
import cn.com.cowain.ibms.rest.resp.ReservationRecordDetailsResp;
import cn.com.cowain.ibms.rest.resp.RoomSearchResp;
import cn.com.cowain.ibms.utils.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/3 19:37
 */
@Slf4j
@SpringBootTest
public class RoomDaoTest44 {

    @Autowired
    private RoomDao roomDao;
    @Autowired
    private TimeSlotDao timeSlotDao;
    @Autowired
    private OpeningHoursDao openingHoursDao;
    @Autowired
    private RoomDeviceDao roomDeviceDao;
    @Autowired
    private ReservationRecordItemDao reservationRecordItemDao;

    @Autowired SysUserDao sysUserDao;

    @Autowired
    private RoomServiceDao roomServiceDao;

    @Test
    public void test0() {
        Assertions.assertNotNull(roomDao);
    }

    @Test
    public void test2() {
        Sort sort = Sort.by(Sort.Direction.ASC, "seq");
        Pageable pageable = PageRequest.of(2, 2, sort);
        Page<Room> roomPage = roomDao.findByNameContains("会议室", pageable);
        PageBean pageBean = new PageBean();

        pageBean.setPage(pageable.getPageNumber());  //当前页码
        pageBean.setSize(pageable.getPageSize());  //每页记录数
        pageBean.setTotalElements(roomPage.getTotalElements());  //总记录数
        pageBean.setTotalPages(roomPage.getTotalPages());  //总页数
        pageBean.setList(roomPage.getContent());
        System.out.println(pageBean);
    }

    @Test
    public void test33() {
        Integer maxSeq = roomDao.findMaxSeq();
        System.out.println(maxSeq);
    }

    @Test
    public void test336() {
        Sort sort = Sort.by(Sort.Direction.ASC, "seq");
        Pageable pageable = PageRequest.of(0, 2, sort);
        Page<Room> roomPage = roomDao.findByNameContains("", pageable);
        PageBean pageBean = new PageBean();

        pageBean.setPage(pageable.getPageNumber());  //当前页码
        pageBean.setSize(pageable.getPageSize());  //每页记录数
        pageBean.setTotalElements(roomPage.getTotalElements());  //总记录数
        pageBean.setTotalPages(roomPage.getTotalPages());  //总页数

        List<RoomSearchResp> list = new ArrayList<>();
        roomPage.getContent().forEach(room -> {
            RoomSearchResp roomSearchResp = new RoomSearchResp();
            BeanUtils.copyProperties(room, roomSearchResp);
            Set<OpeningHours> openingHoursList = openingHoursDao.findByRoomId(roomSearchResp.getId());
            List<OpeningHoursReq> openingHoursReqList = new ArrayList<>();
            OpeningHoursReq openingHoursReq = new OpeningHoursReq();
            openingHoursList.forEach(it->{
                openingHoursReq.setFrom(it.getFrom());
                openingHoursReq.setTo(it.getTo());
                openingHoursReqList.add(openingHoursReq);
            });
            roomSearchResp.setOpeningHours(openingHoursReqList);

            Set<RoomDevice> roomDeviceSet = roomDeviceDao.findByRoomId(roomSearchResp.getId());
            List<String> stringListDevice = new ArrayList<>();
            roomDeviceSet.forEach(it->{
                stringListDevice.add(it.getName());
            });
            roomSearchResp.setDevices(stringListDevice);

//            roomSearchResp.setOpeningHours();
            //查询 开放时间,set roomSearchResp
            //查询 设备, set roomSearchResp
            list.add(roomSearchResp);
        });

//        pageBean.setList(roomPage.getContent());
        log.info(String.valueOf(pageBean));
        JsonResult jr = JsonResult.ok("OK", pageBean);

    }
    @Test
    public void test3364() {
        List<TimeSlotBean> timeSlotBeans = DateUtil.splitDate("08:00","11:00");
        log.info(String.valueOf(timeSlotBeans));
    }

    @Test
    public void test123123123(){
        String ownerId = "testId111";
        String RecordId = "b9aeb22f-6a06-494f-9071-8d8ed55e5fa6";

        Optional<ReservationRecordItem> reservationRecordItemOptional = reservationRecordItemDao.findByReservationRecordIdAndOwner(RecordId,ownerId);

        Set<ReservationRecordItem> byReservationRecordId = reservationRecordItemDao.findByReservationRecordId(RecordId);

        List<ParticipantBean> participantBeanList = new ArrayList<>();
        for(ReservationRecordItem rri : byReservationRecordId){
            ParticipantBean participantBean = new ParticipantBean();
            participantBean.setOwner(rri.getOwner());
            participantBean.setStatus(rri.getStatus());
            participantBeanList.add(participantBean);
        }

        log.info(String.valueOf(reservationRecordItemOptional));
        boolean present = reservationRecordItemOptional.isPresent();
        ReservationRecordDetailsResp reservationRecordDetailsResp = new ReservationRecordDetailsResp();
        if(present){

            reservationRecordDetailsResp.setId(reservationRecordItemOptional.get().getId()); //id
            reservationRecordDetailsResp.setBuilding(reservationRecordItemOptional.get().getReservationRecord().getRoom().getBuilding()); // 楼栋
            reservationRecordDetailsResp.setFloor(reservationRecordItemOptional.get().getReservationRecord().getRoom().getFloor()); //会议室楼层
            reservationRecordDetailsResp.setTimeFrom(reservationRecordItemOptional.get().getReservationRecord().getFrom()); //开始时间
            reservationRecordDetailsResp.setTimeTo(reservationRecordItemOptional.get().getReservationRecord().getTo());//结束时间
            reservationRecordDetailsResp.setTopic(reservationRecordItemOptional.get().getReservationRecord().getTopic());//会议主题
            reservationRecordDetailsResp.setParticipant(participantBeanList);


        }


    }

    @Test
    public void testsub(){
//        String i = "8";
//        StringBuilder sb = new StringBuilder();
//        sb.append("8");
//        sb.append("0");
//        log.info(String.valueOf(sb));
        String stringTimeFromHigh = "8";
        stringTimeFromHigh = String.valueOf(new StringBuilder("0").append(stringTimeFromHigh));

        log.info(stringTimeFromHigh);
    }


    @Test
    public void testSysUserId(){
        SysUser sysUser = new SysUser();
        sysUser.setSysId("12211");
        sysUser.setNameZh("zhangsan");
        Optional<SysUser> sysUserOptional = sysUserDao.findBySysId(sysUser.getSysId());
        if(sysUserOptional.isPresent()){
            log.debug("已存在用户信息...跳过");
        }
        else{
            sysUserDao.save(sysUser);
            log.debug("未找到用户信息...写入");
        }
        log.info(String.valueOf(sysUserOptional));
    }

    @Test
    public void valid(){
        String timeFrom = "10:00";
        String timeTo = "09:00";
        LocalTime localTimeFrom = LocalTime.parse(timeFrom);
        LocalTime localTimeTo = LocalTime.parse(timeTo);
        if(((localTimeTo.getHour()-localTimeFrom.getHour())*60+localTimeTo.getMinute()-localTimeFrom.getMinute())>30){
            log.info("满足");
        }
        else{
            log.info("不满足");
        }
        Integer i = (localTimeTo.getHour()-localTimeFrom.getHour())*60+localTimeTo.getMinute()-localTimeFrom.getMinute();
        String a= "1";
    }

    @Test
    public void test2112(){
        String s = "0b5eea2c-e703-48ef-8a34-e5a5c92c93c6";
        Optional<RoomService> roomService = roomServiceDao.findById(s);
        log.info(String.valueOf(roomService));
    }

    @Test
    public void test111(){
        List<RoomService> roomServiceList = new ArrayList<>();
        for(int i=1; i<5;i++){

            RoomService roomService = new RoomService();
            if(i==1) {
                roomService.setName("水果");
            }
            if(i==2) {
                roomService.setName("饮料");
            }
            if(i==3) {
                roomService.setName("鲜花");
            }
            if(i==4) {
                roomService.setName("茶水");
            }
            roomServiceList.add(roomService);

        }
        roomServiceDao.saveAll(roomServiceList);
    }
}