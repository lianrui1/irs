package cn.com.cowain.ibms.websocket.bean.xunfei.rtasr;

import lombok.Data;

import java.util.List;

/**
 * 讯飞实时转写词结果
 * @author Yang.Lee
 * @date 2021/2/3 17:40
 */
@Data
public class WSResp {


    /**
     * 解析的词数据列表
     */
    private List<CWResp> cw;
}
