package cn.com.cowain.ibms.rest.resp.device.centralized;

import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;

/**
 * 集控码选择设备返回参数
 *
 * @author: yanzy
 * @date: 2022/9/16 15:00
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("集控码选择设备返回参数")
public class CentralizedControlCodeSelectDeviceResp {

    @ApiModelProperty(value = "设备id", required = true, position = 1)
    private String deviceId;

    @ApiModelProperty(value = "设备Sn", required = true, position = 2)
    private String deviceSn;

    @ApiModelProperty(value = "设备名称", required = true, position = 3)
    private String deviceName;

    @ApiModelProperty(value = "设备属性", required = true, position = 4)
    private DeviceType deviceType;

    @ApiModelProperty(value = "设备状态", required = true, position = 5)
    private DeviceStatus hwStatus;

    @ApiModelProperty(value = "设备类型名称", required = true, position = 6)
    private String deviceTypeDesc;

    /**
     * 对象转换
     *
     * @author: yanzy
     * @data: 2022/9/19 11:09:10
     */

    public static CentralizedControlCodeSelectDeviceResp convent(IotDevice device){

        return CentralizedControlCodeSelectDeviceResp.builder()
                .deviceId(device.getId())
                .deviceSn(device.getSn())
                .deviceName(device.getDeviceName())
                .deviceType(device.getDeviceType())
                .hwStatus(device.getHwStatus())
                .deviceTypeDesc(ObjectUtils.isNotEmpty(device.getDeviceType()) ? device.getDeviceType().getName() : null)
                .build();
    }
}
