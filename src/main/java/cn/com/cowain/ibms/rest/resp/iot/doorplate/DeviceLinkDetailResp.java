package cn.com.cowain.ibms.rest.resp.iot.doorplate;

import cn.com.cowain.ibms.enumeration.iot.DeviceLinkStatus;
import cn.com.cowain.ibms.rest.resp.iot.IotDeviceResp;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@ApiModel("联动详情相应")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DeviceLinkDetailResp {

    @ApiModelProperty(value = "联动ID", required = true, example = "1", position = 1)
    private String id;

    @ApiModelProperty(value = "设备列表", required = true, position = 2)
    private List<IotDeviceResp> deviceRespList;

    @ApiModelProperty(value = "项目ID", required = true, position = 3)
    private String projectId;

    @ApiModelProperty(value = "项目名称", required = true, position = 4)
    private String projectName;

    @ApiModelProperty(value = "空间ID", required = true, position = 5)
    private String spaceId;

    @ApiModelProperty(value = "空间名称", required = true, position = 6)
    private String spaceName;

    @ApiModelProperty(value = "设备地址", position = 7)
    private String address;

    @ApiModelProperty(value = "联动状态", required = true, example = "NORMAL：正常；DISABLE：禁用；INVALID：失效", position = 8)
    private DeviceLinkStatus status;

    @ApiModelProperty(value = "联动状态名称", required = true, example = "正常", position = 9)
    private String statusName;
}
