package cn.com.cowain.ibms.rest.resp.iot;

import cn.com.cowain.ibms.rest.bean.PageBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/4/20 18:30
 */
@Data
public class AccessControlUserFailResp {

    @ApiModelProperty(value = "所有人员")
    private int all;

    @ApiModelProperty(value = "成功人员")
    private int success;

    @ApiModelProperty(value = "同步中")
    private int wait;

    @ApiModelProperty(value = "失败人员")
    private int fail;

    @ApiModelProperty(value = "人员信息")
    private PageBean<AccessControlUserFailPageResp> pageBean;
}
