package cn.com.cowain.ibms.exceptions;

/**
 * 幂等异常类
 * @author Yang.Lee
 * @date 2020/12/16 13:26
 */
public class IdempotentException extends RuntimeException {
    public IdempotentException(String message) {
        super(message);
    }
}
