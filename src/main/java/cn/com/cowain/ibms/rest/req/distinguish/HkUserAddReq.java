package cn.com.cowain.ibms.rest.req.distinguish;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/15 10:36
 */
@Data
@ApiModel("设备添加单个人人员信息")
public class HkUserAddReq {

    @ApiModelProperty(value = "人员工号")
    private List<String> jobNum;

    @ApiModelProperty(value = "设备id")
    private String deviceId;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "通行生效时间")
    private LocalDateTime startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "通行结束时间")
    private LocalDateTime endTime;
}
