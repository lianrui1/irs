package cn.com.cowain.ibms.rest.req.space;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * 空间用途编辑参数
 *
 * @author Yang.Lee
 * @date 2021/11/17 20:06
 */
@Data
@ApiModel("空间用途编辑参数")
public class SpacePurposeEditReq {

    @Length(max = 10)
    @NotBlank
    @ApiModelProperty(value = "区域名称", required = true, position = 1)
    private String name;
}
