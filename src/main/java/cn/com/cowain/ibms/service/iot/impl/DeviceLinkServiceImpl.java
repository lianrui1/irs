package cn.com.cowain.ibms.service.iot.impl;

import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.dao.iot.DeviceLinkDao;
import cn.com.cowain.ibms.dao.iot.DeviceLinkRelationDao;
import cn.com.cowain.ibms.dao.iot.SpaceDao;
import cn.com.cowain.ibms.entity.iot.DeviceLink;
import cn.com.cowain.ibms.entity.iot.DeviceLinkRelation;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.enumeration.iot.DeviceLinkStatus;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.iot.DeviceLinkEditReq;
import cn.com.cowain.ibms.rest.req.iot.DeviceLinkPageReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.iot.DeviceLinkPageResp;
import cn.com.cowain.ibms.rest.resp.iot.IotDeviceResp;
import cn.com.cowain.ibms.rest.resp.iot.doorplate.DeviceLinkDetailResp;
import cn.com.cowain.ibms.service.bean.ServiceMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.iot.DeviceLinkService;
import cn.com.cowain.ibms.service.space.SpaceService;
import cn.com.cowain.ibms.utils.DateUtils;
import cn.com.cowain.ibms.utils.ErrConst;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 设备联动Service实现类
 */
@Service
@Slf4j
public class DeviceLinkServiceImpl implements DeviceLinkService {

    @Resource
    private DeviceDao deviceDao;

    @Resource
    private DeviceLinkDao deviceLinkDao;

    @Resource
    private DeviceLinkRelationDao deviceLinkRelationDao;

    @Resource
    private SpaceDao spaceDao;

    /**
     * 创建联动
     *
     * @param req 请求参数
     * @return 结果，成功时返回新数据主键ID
     */
    @Override
    @Transactional
    public ServiceResult create(DeviceLinkEditReq req) {

        // 判断空间是否存在
        Optional<Space> spaceOptional = spaceDao.findById(req.getSpaceId());
        if (spaceOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_SPACE, req.getSpaceId()));
        }

        // 创建联动基本信息
        DeviceLink deviceLink = new DeviceLink();
        BeanUtils.copyProperties(req, deviceLink);
        deviceLink.setSpace(spaceOptional.get());
        deviceLink.setNumber(getLinkNumber());

        deviceLinkDao.save(deviceLink);

        List<IotDevice> deviceList = new ArrayList<>();
        // 创建联动设备关系
        for (String deviceId : req.getDeviceIdList()) {
            Optional<IotDevice> iotDeviceOptional = deviceDao.findById(deviceId);
            if (iotDeviceOptional.isEmpty()) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_DEVICE, deviceId));
            }

            deviceList.add(iotDeviceOptional.get());
        }

        deviceLinkRelationDao.saveAll(deviceList.stream()
                .map(obj -> DeviceLinkRelation.builder().deviceLink(deviceLink).iotDevice(obj).build())
                .collect(Collectors.toList()
                ));


        return ServiceResult.ok(IdResp.builder().id(deviceLink.getId()).build());
    }

    /**
     * 编辑联动信息
     *
     * @param id  联动ID
     * @param req 请求参数
     * @return 修改结果
     */
    @Override
    @Transactional
    public ServiceResult update(String id, DeviceLinkEditReq req) {

        Optional<DeviceLink> deviceLinkOptional = deviceLinkDao.findById(id);
        if (deviceLinkOptional.isEmpty()) {
            return ServiceResult.error("未找到设备联动信息", ErrConst.E01);
        }

        Optional<Space> spaceOptional = spaceDao.findById(req.getSpaceId());
        if (spaceOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_SPACE, req.getSpaceId()), ErrConst.E01);
        }

        // 数据库中现有的数据
        DeviceLink deviceLinkDB = deviceLinkOptional.get();
        List<DeviceLinkRelation> deviceLinkRelationList = deviceLinkRelationDao.findByDeviceLinkId(id);

        // 找出新增的设备和需要被删除的设备

        Set<String> dbDeviceIdSet = deviceLinkRelationList.stream()
                .map(DeviceLinkRelation::getIotDevice)
                .map(IotDevice::getId)
                .collect(Collectors.toSet());

        Set<String> delDeviceIdSet = new HashSet<>(dbDeviceIdSet);
        delDeviceIdSet.removeAll(req.getDeviceIdList());

        Set<String> addDeviceIdSet = new HashSet<>(req.getDeviceIdList());
        addDeviceIdSet.removeAll(dbDeviceIdSet);

        // 需要被删除的数据
        List<DeviceLinkRelation> delList = deviceLinkRelationList.stream().filter(obj -> delDeviceIdSet.contains(obj.getIotDevice().getId())).map(obj -> {
            obj.setIsDelete(1);
            return obj;
        }).collect(Collectors.toList());

        // 需要新增的对象集合
        List<DeviceLinkRelation> addList = new ArrayList<>();
        // 创建新增关系
        for (String deviceId : addDeviceIdSet) {
            Optional<IotDevice> iotDeviceOptional = deviceDao.findById(deviceId);
            if (iotDeviceOptional.isEmpty()) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_DEVICE, deviceId), ErrConst.E01);
            }

            DeviceLinkRelation deviceLinkRelation = new DeviceLinkRelation();
            deviceLinkRelation.setDeviceLink(deviceLinkDB);
            deviceLinkRelation.setIotDevice(iotDeviceOptional.get());
            addList.add(deviceLinkRelation);
        }

        // 保存设备关系
        deviceLinkRelationDao.saveAll(delList);
        deviceLinkRelationDao.saveAll(addList);

        // 保存联动关系
        deviceLinkDB.setAddress(req.getAddress());
        deviceLinkDB.setSpace(spaceOptional.get());
        deviceLinkDB.setStatus(req.getStatus());
        deviceLinkDao.save(deviceLinkDB);

        return ServiceResult.ok();
    }

    /**
     * 删除联动信息
     *
     * @param id 联动ID
     * @return 删除结果
     */
    @Override
    @Transactional
    public ServiceResult delete(String id) {

        Optional<DeviceLink> deviceLinkOptional = deviceLinkDao.findById(id);
        if (deviceLinkOptional.isEmpty()) {
            return ServiceResult.error("未找到设备联动信息", ErrConst.E01);
        }

        DeviceLink deviceLink = deviceLinkOptional.get();

        // 启用状态不支持删除
        if (DeviceLinkStatus.NORMAL.equals(deviceLink.getStatus())) {
            return ServiceResult.error("设备联动启用中，无法删除", ErrConst.E01);
        }

        deviceLink.setIsDelete(1);
        deviceLinkDao.save(deviceLink);

        // 找到所有的关系，删除
        List<DeviceLinkRelation> relationList = deviceLinkRelationDao.findByDeviceLinkId(deviceLink.getId());
        relationList.forEach(obj -> obj.setIsDelete(1));
        deviceLinkRelationDao.saveAll(relationList);


        return ServiceResult.ok();
    }

    /**
     * 编辑联动状态
     *
     * @param id     联动ID
     * @param status 修改后的状态
     * @return 编辑结果
     */
    @Override
    @Transactional
    public ServiceResult updateStatus(String id, DeviceLinkStatus status) {

        Optional<DeviceLink> deviceLinkOptional = deviceLinkDao.findById(id);
        if (deviceLinkOptional.isEmpty()) {
            return ServiceResult.error("未找到设备联动信息", ErrConst.E01);
        }

        DeviceLink deviceLink = deviceLinkOptional.get();

        // 当禁用状态改成启用时，需进行设备是否为空判断；为空则提示启用失败
        List<DeviceLinkRelation> deviceLinkRelationList = deviceLinkRelationDao.findByDeviceLinkId(id);
        if (deviceLinkRelationList.isEmpty() && DeviceLinkStatus.NORMAL.equals(status)) {
            return ServiceResult.error("设备联动中无设备信息，无法启用", ErrConst.E01);
        }

        deviceLink.setStatus(status);
        deviceLinkDao.save(deviceLink);

        return ServiceResult.ok();
    }

    /**
     * 获取分页数据
     *
     * @param req 请求参数
     * @return 查询结果
     */
    @Override
    @Transactional
    public PageBean<DeviceLinkPageResp> getPage(DeviceLinkPageReq req) {

        Page<DeviceLink> deviceLinkPage = deviceLinkDao.findAll((root, criteriaQuery, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();
            if (req.getStatus() != null) {
                list.add(criteriaBuilder.equal(root.get("status"), req.getStatus()));
            }

            if (StringUtils.isNotBlank(req.getKeyword())) {
                String like = "%" + req.getKeyword() + "%";
                list.add(criteriaBuilder.like(root.get("number"), like));
            }

            if (StringUtils.isNotBlank(req.getSpaceId())) {
                list.add(criteriaBuilder.equal(root.get("space").get("id"), req.getSpaceId()));
            }

            if (StringUtils.isNotBlank(req.getProjectId())) {
                list.add(criteriaBuilder.equal(root.get("space").get("project").get("id"), req.getProjectId()));
            }

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, PageRequest.of(req.getPage(), req.getSize(), Sort.by(req.getOrders())));

        PageBean<DeviceLinkPageResp> respPageBean = new PageBean<>();
        respPageBean.setPage(deviceLinkPage.getPageable().getPageNumber());
        respPageBean.setSize(deviceLinkPage.getPageable().getPageSize());
        respPageBean.setTotalPages(deviceLinkPage.getTotalPages());
        respPageBean.setTotalElements(deviceLinkPage.getTotalElements());
        respPageBean.setList(deviceLinkPage.getContent()
                .stream()
                .map(obj -> {
                    // 将数据库对象转为resp对象
                    List<DeviceLinkRelation> deviceLinkRelationList = deviceLinkRelationDao.findByDeviceLinkId(obj.getId());
                    return DeviceLinkPageResp.builder()
                            .id(obj.getId())
                            .deviceNameList(deviceLinkRelationList.stream()
                                    .map(DeviceLinkRelation::getIotDevice)
                                    .map(IotDevice::getDeviceName)
                                    .collect(Collectors.toList()))
                            .address(obj.getSpace().getProject().getProjectName() + " / " + SpaceService.getFullSpaceName(obj.getSpace(),obj.getSpace().getName()))
                            .createTime(obj.getCreatedTime())
                            .number(obj.getNumber())
                            .status(obj.getStatus())
                            .statusName(obj.getStatus().getName())
                            .build();
                })
                .collect(Collectors.toList()));

        return respPageBean;
    }

    /**
     * 查询联动详情
     *
     * @param id 联动ID
     * @return 查询结果
     */
    @Override
    @Transactional
    public DeviceLinkDetailResp getDetail(String id) {

        Optional<DeviceLink> deviceLinkOptional = deviceLinkDao.findById(id);
        if (deviceLinkOptional.isEmpty()) {
            return DeviceLinkDetailResp.builder().build();
        }

        DeviceLink deviceLink = deviceLinkOptional.get();
        List<DeviceLinkRelation> relationList = deviceLinkRelationDao.findByDeviceLinkId(deviceLink.getId());

        return DeviceLinkDetailResp.builder()
                .id(deviceLink.getId())
                .deviceRespList(relationList.stream().map(obj ->
                        IotDeviceResp.builder()
                                .id(obj.getIotDevice().getId())
                                .deviceName(obj.getIotDevice().getDeviceName())
                                .sn(obj.getIotDevice().getSn())
                                .build())
                        .collect(Collectors.toList()))
                .address(Optional.ofNullable(deviceLink.getAddress()).orElse(""))
                .projectId(deviceLink.getSpace().getProject().getId())
                .projectName(deviceLink.getSpace().getProject().getProjectName())
                .spaceId(deviceLink.getSpace().getId())
                .spaceName(deviceLink.getSpace().getName())
                .status(deviceLink.getStatus())
                .statusName(deviceLink.getStatus().getName())
                .build();
    }

    /**
     * 获取联动规则编码
     *
     * @return 编码
     */
    public String getLinkNumber() {

        return new StringBuffer("LD")
                .append(DateUtils.formatLocalDate(LocalDate.now(), DateUtils.DATE_PATTERN))
                .append(String.format("%04d", deviceLinkDao.getTodayTotalNumber() + 1))
                .toString();
    }
}
