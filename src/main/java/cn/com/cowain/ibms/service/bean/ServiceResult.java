package cn.com.cowain.ibms.service.bean;

import lombok.Builder;
import lombok.Data;


/**
 * 业务逻辑处理结果
 *
 * @author 张宇鑫
 */
@Data
@Builder
public class ServiceResult {

    /**
     * 操作是否成功
     */
    private boolean success;

    /**
     * 内容详情
     */
    private Object object;

    /**
     * 业务处理错误码
     **/
    private String errorCode;

    /**
     * 返回成功结果
     *
     * @return
     */
    public static ServiceResult ok() {
        return ServiceResult.builder().success(true).build();
    }

    /**
     * 返回成功结果
     *
     * @return
     */
    public static ServiceResult ok(Object object) {
        return ServiceResult.builder()
                .success(true)
                .object(object)
                .build();
    }

    /**
     * 返回失败结果
     *
     * @return
     */
    public static ServiceResult error(Object object){
        return ServiceResult.builder()
                .success(false)
                .object(object)
                .build();
    }

    /**
     * 构建业务处理失败时返回的处理结果
     *
     * @param object 业务返回内容
     * @param errorCode 业务错误码
     * @return 业务处理结果
     * @author Yang.Lee
     * @date 2021/3/20 11:16
     **/
    public static ServiceResult error(Object object, String errorCode){
        return ServiceResult.builder()
                .success(false)
                .object(object)
                .errorCode(errorCode)
                .build();
    }

}
