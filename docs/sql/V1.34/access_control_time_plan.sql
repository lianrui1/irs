create table iot_access_control_time_plan
(
    id           varchar(36) not null comment '主键'
        primary key,
    created_by   varchar(36) null comment '创建人',
    created_time datetime    null comment '创建时间',
    is_del       tinyint(1)  not null comment '是否已删除,0:否;1:是',
    updated_by   varchar(36) null comment '更新人',
    updated_time datetime    null comment '更新时间',
    version      int         null comment '版本号',
    spot_id      varchar(36) not null comment '门禁点id',
    user_hr_id   varchar(50) not null comment '用户工号',
    work_days    json        null comment '日程时段'
)
    comment '通行时间计划';

create index iot_access_control_time_plan_user_hr_id_spot_id_index
    on iot_access_control_time_plan (user_hr_id, spot_id);

