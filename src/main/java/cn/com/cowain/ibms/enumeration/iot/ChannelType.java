package cn.com.cowain.ibms.enumeration.iot;

/**
 * @author feng
 * @title: CHANNELTYPE
 * @projectName ibms
 * @Date 2022/1/16 17:46
 */
public enum ChannelType {
    /**
     * 旷世
     **/
    KSHT("KSHT"),

    /**
     * 海康
     **/
    HIK("HIK");

    ChannelType(String name) {
        this.name = name;
    }

    private String name;

    public String getName() {
        return this.name;
    }
}
