package cn.com.cowain.ibms.service;

import cn.com.cowain.ibms.service.bean.ServiceResult;

/**
 * UC权限操作Service
 *
 * @author Yang.Lee
 * @date 2021/12/1 9:40
 */
public interface UCRoleService {

    ServiceResult removeIntelligentOfficeRole(String userHrId, String spaceId);
}
