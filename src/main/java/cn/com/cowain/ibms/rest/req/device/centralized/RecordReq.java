package cn.com.cowain.ibms.rest.req.device.centralized;

import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;

@Data
@ApiModel("集控操作日志列表请求对象")
public class RecordReq extends PageReq {

    @NotEmpty
    @ApiModelProperty(value = "集控码ID")
    private String id;

    @ApiModelProperty(value = "控制人姓名/工号/设备名称")
    private String keyword;

    @ApiModelProperty(value = "开始时间", example = "2022-09-19 15:46")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime startTime;

    @ApiModelProperty(value = "结束时间", example = "2022-09-19 15:46")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime endTime;
}
