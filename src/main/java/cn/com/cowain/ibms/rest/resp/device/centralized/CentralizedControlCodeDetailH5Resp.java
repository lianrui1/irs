package cn.com.cowain.ibms.rest.resp.device.centralized;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * h5端集控码详情返回参数
 *
 * @author wei.cheng
 * @date 2022/09/20 16:48
 */
@Data
@ApiModel(value = "h5端集控码详情返回参数")
public class CentralizedControlCodeDetailH5Resp {

    @ApiModelProperty(value = "集控码id", required = true, position = 1)
    private String id;

    @ApiModelProperty(value = "集控码名称", required = true, position = 2)
    private String name;

    @ApiModelProperty(value = "控制区域", required = true, position = 3)
    private String controlArea;

    @ApiModelProperty(value = "控制空调个数", position = 4)
    private Integer countAir;

    @ApiModelProperty(value = "控制空调中已开启的数量", position = 5)
    private Integer countOpenAir;

    @ApiModelProperty(value = "控制灯个数", position = 6)
    private Integer countLamp;

    @ApiModelProperty(value = "控制灯中已开启的数量", position = 7)
    private Integer countOpenLamp;
}
