package cn.com.cowain.ibms.rest.qrcode;

import cn.com.cowain.ibms.exceptions.UnAuthorizedException;
import cn.com.cowain.ibms.rest.req.iot.QrCodeReq;
import cn.com.cowain.ibms.rest.resp.iot.QrCodeResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.qrcode.QrCodeService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.sfp.comm.utils.JwtUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author Yang.Lee
 * @date 2020/12/2 13:41
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/qrcode")
@Api(tags = IConst.MODULE_QRCODE)
public class QrCodeController {

    @Autowired
    private QrCodeService qrCodeSevice;

    /**
     * 获取二维码图片（base64格式）
     *
     * @param doorPlateId
     * @return
     * @author Yang.Lee
     */
    @GetMapping("/{doorPlateId}")
    @ApiOperation(value = "APP获取base64格式的二维码图片", tags = IConst.MODULE_QRCODE)
    public ResponseEntity<JsonResult<QrCodeResp>> getImage(@ApiParam(name = "doorPlateId", value = "门牌ID", required = true) @PathVariable String doorPlateId) {

        ServiceResult result = qrCodeSevice.create(doorPlateId);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E14));
        }
        QrCodeResp resp = (QrCodeResp) result.getObject();
        return ResponseEntity.ok(JsonResult.ok("ok", resp));
    }

    /**
     * 扫码开门
     *
     * @param qrCodeReq
     * @param bindingResult
     * @return
     */
    @PutMapping
    @ApiOperation(value = "APP扫码开门", tags = IConst.MODULE_QRCODE)
    public ResponseEntity<JsonResult<Object>> openDoor(@RequestBody @Validated QrCodeReq qrCodeReq, BindingResult bindingResult) {

        log.debug("doorPlateId : " + qrCodeReq.getDoorPlateId());
        log.debug("uuid ：" + qrCodeReq.getUuid());
        log.debug("empNo" + qrCodeReq.getEmpNo());

        ServiceResult result = qrCodeSevice.scan(qrCodeReq);
        JsonResult<Object> jsonResult = JsonResult.okNoData("OK");
        if (!result.isSuccess()) {
            jsonResult = JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01);
        }
        return ResponseEntity.ok(jsonResult);
    }

    @GetMapping
    @ApiOperation(value = "APP扫码开门", tags = IConst.MODULE_QRCODE)
    public ResponseEntity<JsonResult<Object>> openDoorGet(@RequestBody @Validated QrCodeReq qrCodeReq, BindingResult bindingResult) {

        log.debug("doorPlateId : " + qrCodeReq.getDoorPlateId());
        log.debug("uuid ：" + qrCodeReq.getUuid());
        log.debug("empNo" + qrCodeReq.getEmpNo());

        ServiceResult result = qrCodeSevice.scan(qrCodeReq);
        JsonResult<Object> jsonResult = JsonResult.okNoData("OK");
        if (!result.isSuccess()) {
            jsonResult = JsonResult.error(String.valueOf(result.getObject()), null, "0");
        }
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 静态二维码开门
     *
     * @param dm 门磁编号
     * @return 开门结果
     * @author Yang.Lee
     * @date 2021/3/22 15:47
     **/
    @PostMapping("/scan/static/{dm}")
    @ApiOperation(value = "H5扫码开门(静态二维码)", tags = IConst.MODULE_QRCODE)
    public ResponseEntity<JsonResult<String>> openDoorStatic(@PathVariable @ApiParam(value = "门磁编号", required = true) String dm,
                                                             @RequestHeader(name = "Authorization", required = false) String token) {

        log.debug("进入静态二维码扫码开门接口，Authorization：{}", token);

        if (StringUtils.isEmpty(token)) {
            throw new UnAuthorizedException("未授权");
        }

        // 获取用户工号
        String empNo = JwtUtil.getHrId(token);

        ServiceResult result = qrCodeSevice.scanStaticQRCode(dm, empNo);
        JsonResult<String> jsonResult = JsonResult.okNoData("OK");
        if (!result.isSuccess()) {
            jsonResult = JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01);
        }
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 访客开门
     *
     * @param qrCodeReq     请求参数
     * @param bindingResult 参数校验
     * @return 业务处理结果
     * @author Yang.Lee
     * @date 2021/7/5 13:35
     **/
    @PutMapping("/guest/scan")
    @ApiOperation(value = "APP 访客开门", tags = IConst.MODULE_QRCODE)
    public ResponseEntity<JsonResult<Object>> guestOpenDoor(@RequestBody @Validated QrCodeReq qrCodeReq, BindingResult bindingResult) {
        ServiceResult result;
        if (StringUtils.isNotBlank(qrCodeReq.getDoorPlateId())) {
            result = qrCodeSevice.guestScan(qrCodeReq);

        }else{
            result = qrCodeSevice.gustScanStaticQrcode(qrCodeReq);
        }
        JsonResult<Object> jsonResult = JsonResult.okNoData("OK");
        if (!result.isSuccess()) {
            jsonResult = JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E18);
        }

        return ResponseEntity.ok(jsonResult);
    }
}
