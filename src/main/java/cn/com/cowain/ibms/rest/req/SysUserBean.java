package cn.com.cowain.ibms.rest.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/4 18:47
 */
@Data
@ApiModel("用户")
public class SysUserBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 用户中心的全局唯一id
     */
    @ApiModelProperty(value = "用户全局唯一id", required = true, position = 0)
    private String sysId;

    /**
     * 员工编号
     */
    @ApiModelProperty(value = "员工编号", required = true, position = 1)
    private String empNo;

    /**
     * 员工姓名 中文
     */
    @ApiModelProperty(value = "员工姓名 中文", required = true, position = 2)
    private String nameZh;

    /**
     * 部门ID
     */
    @ApiModelProperty(value = "部门ID", position = 3)
    private String deptId;

    /**
     * 部门名称
     */
    @ApiModelProperty(value = "部门名称", position = 4)
    private String deptName;

    /**
     * 是否是外部人员
     */
    @ApiModelProperty(value = "是否是访客，1 是； 0 否", position = 5)
    private Integer isGuest;

    /**
     * 是否是外部人员
     */
    @ApiModelProperty(value = "是否是会议组成员，1 是； 0 否", position = 6)
    private Integer isGroup;

}
