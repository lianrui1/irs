ALTER TABLE `ibms_prod`.`iot_product` ADD COLUMN `hw_device_type` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '华为云中的设备类型' AFTER `hw_description`;


ALTER TABLE `ibms_prod`.`iot_product` ADD COLUMN `hw_industry` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '华为云中的设备所属行业' AFTER `hw_device_type`;


ALTER TABLE `ibms_prod`.`iot_product` ADD COLUMN `hw_manufacturer_name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '华为云中的厂商名称' AFTER `hw_industry`;


ALTER TABLE `ibms_prod`.`iot_product` ADD COLUMN `hw_name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '华为云中的产品名称' AFTER `hw_manufacturer_name`;


ALTER TABLE `ibms_prod`.`iot_product` ADD COLUMN `hw_product_id` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '华为云中的设备关联的产品ID' AFTER `hw_name`;


ALTER TABLE `ibms_prod`.`iot_product` ADD COLUMN `hw_protocol_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '华为云中的设备使用的协议类型。取值范围：MQTT，CoAP，HTTP，HTTPS，Modbus，ONVIF。' AFTER `hw_product_id`;


ALTER TABLE `ibms_prod`.`iot_product` ADD COLUMN `hw_service` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '华为云硬件提供的服务能力(json字符串)' AFTER `hw_protocol_type`;

ALTER TABLE `ibms_prod`.`iot_product` ADD COLUMN `remark` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '产品名称备注' AFTER `hw_service`;ALTER TABLE `ibms_prod`.`iot_product` ADD COLUMN `hw_device_type` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '华为云中的设备类型' AFTER `hw_description`;

