package cn.com.cowain.ibms.rest.resp.device;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/15 17:09
 */
@Data
public class CurtainResp {

    private String action;
}
