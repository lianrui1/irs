package cn.com.cowain.ibms.rest.resp.ota;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/4/22 9:12
 */
@Data
public class FileResp {

    @ApiModelProperty(value = "安装任务地址", required = true, example = "x.apk", position = 1)
    private String appPackageUrl;

    @ApiModelProperty(value = "安装包大小", required = true, example = "20M", position = 2)
    private String appSize;

    @ApiModelProperty(value = "MD5", required = true, example = "123456", position = 3)
    private String appPackageMD5;

    @ApiModelProperty(value = "版本号", required = true, example = "1.4.1", position = 4)
    private String versionOTA;

    @ApiModelProperty(value = "安装", required = true, position = 5)
    private String app;
}
