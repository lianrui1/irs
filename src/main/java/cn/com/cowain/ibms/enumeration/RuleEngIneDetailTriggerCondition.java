package cn.com.cowain.ibms.enumeration;

/**
 * 联动规则条件关系
 *
 * @author: yanzy
 * @date: 2022/2/9 18:13
 */
public enum RuleEngIneDetailTriggerCondition {

    WHOLE("全部"),

    ANY("任一");

    RuleEngIneDetailTriggerCondition(String name){
        this.name = name;
    }

    private String name;

    public String getName(){
        return this.name;
    }
}
