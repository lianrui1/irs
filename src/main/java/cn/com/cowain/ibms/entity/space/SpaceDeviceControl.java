package cn.com.cowain.ibms.entity.space;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author Yang.Lee
 * @date 2021/10/18 15:54
 */
@Data
@Entity
@Table(name = "space_device_control")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "space_device_control", comment = "空间内设备控制权限表")
@Where(clause = "is_del=0")
public class SpaceDeviceControl extends BaseEntity {

    @Column(name = "user_empNo", columnDefinition = "varchar(20) COMMENT '用户工号'")
    private String userEmpNo;

    @Column(name = "user_name", columnDefinition = "varchar(30) COMMENT '用户姓名'")
    private String userName;

    @ManyToOne
    @JoinColumn(name = "device_Id", nullable = false, columnDefinition = "varchar(36) COMMENT '设备id'")
    private IotDevice device;

}
