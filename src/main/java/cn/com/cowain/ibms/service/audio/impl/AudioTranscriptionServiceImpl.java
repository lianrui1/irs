package cn.com.cowain.ibms.service.audio.impl;

import cn.com.cowain.ibms.dao.audio.AudioDao;
import cn.com.cowain.ibms.entity.voice.AudioRecord;
import cn.com.cowain.ibms.rest.req.audio.RealtimeUpload;
import cn.com.cowain.ibms.rest.resp.audio.RTASTResultResp;
import cn.com.cowain.ibms.service.audio.AudioTranscriptionService;
import cn.com.cowain.ibms.utils.DateUtils;
import cn.com.cowain.ibms.utils.FastDfsPool;
import cn.com.cowain.ibms.utils.FileUtil;
import cn.com.cowain.ibms.utils.RedisUtil;
import cn.com.cowain.ibms.utils.media.MediaUtil;
import cn.com.cowain.ibms.websocket.client.RTASRClient;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.iflytek.msp.lfasr.LfasrClient;
import com.iflytek.msp.lfasr.model.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 科大讯飞通信业务实现类
 * @author Yang.Lee
 * @date 2021/1/6 13:39
 */
@Slf4j
@Service
public class AudioTranscriptionServiceImpl implements AudioTranscriptionService {

    @Value("${xunfei.appId}")
    private String appId;
    @Value("${xunfei.secretKey}")
    private String secretKey;
    @Value("${xunfei.rt-asr.coreThreads}")
    private int coreThreads;
    @Value("${xunfei.rt-asr.maxThreads}")
    private int maxThreads;
    @Value("${xunfei.rt-asr.maxConn}")
    private int maxConn;
    @Value("${xunfei.rt-asr.connTimeout}")
    private int connTimeout;
    @Value("${xunfei.rt-asr.soTimeout}")
    private int soTimeout;

    @Autowired
    private AudioDao audioDao;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private RestTemplate restTemplate;

    // 实时文字分段步长,毫秒
    private static final int STEP = 5000;

    // redis key 前置
    private static final String REDIS_PRE_KEY_RTASR_END = "rtasr-end-";

    // redis key 前置
    private static final String REDIS_PRE_KEY_RTASR = "rtasr-";

    @Value("${fastdfs.fileUrl}")
    private String fastDfsHttpUrl;

    /**
     * 语音转写
     *
     * @param filePath 语音文件路径
     * @return
     */
    @Override
    public String translateAudio(String filePath) throws InterruptedException {

        log.debug("开始调用讯飞语音转写接口...");
        //1、创建客户端实例, 设置性能参数
        LfasrClient lfasrClient =
                LfasrClient.getInstance(

                        appId,
                        secretKey,
                        coreThreads, //线程池：核心线程数
                        maxThreads, //线程池：最大线程数
                        maxConn, //网络：最大连接数
                        connTimeout, //连接超时时间
                        soTimeout, //响应超时时间
                        null);

        //2、上传
        //2.1、设置业务参数
        Map<String, String> param = new HashMap<>(16);
        //语种： cn-中文（默认）;en-英文（英文不支持热词）
        param.put("language", "cn");
        //是否开启分词：默认 false

        Message task = lfasrClient.upload(filePath, param);
        String taskId = task.getData();
        log.debug("转写任务 taskId：" + taskId);
        //3、查看转写进度
        int status = 0;
        while (status != 9) {
            Message message = lfasrClient.getProgress(taskId);
            if(message.getOk() < 0){
                log.error("讯飞转写失败！！！" + message.toString());
                return null;
            }
            JSONObject object = JSON.parseObject(message.getData());
            status = object.getInteger("status");
            log.debug(message.getData());
            TimeUnit.SECONDS.sleep(2);
        }
        //4、获取结果
        Message result = lfasrClient.getResult(taskId);
        log.debug("转写结果: \n" + result.getData());

        return result.getData();
    }

    /**
     * 进行语音转写
     * @param audioRecord
     * @throws Exception
     */
    @Override
    @Async
    @Transactional
    public void translate(AudioRecord audioRecord) throws IOException, InterruptedException, URISyntaxException {

        log.debug("开始异步执行语音转换");

        HttpHeaders headers = new HttpHeaders();
        HttpEntity<Resource> httpEntity = new HttpEntity<>(headers);
        String pcmUrl = fastDfsHttpUrl + audioRecord.getPcmFilePath();
        ResponseEntity<byte[]> response = restTemplate.exchange(pcmUrl, HttpMethod.GET, httpEntity, byte[].class);
        byte[] bytes = response.getBody();
        if(bytes == null || bytes.length == 0){
            log.error("从fastdfs下载pcm文件异常！！！下载地址：{}", audioRecord.getPcmFilePath());
            return;
        }
        // 临时source文件
        String sourceFileName = UUID.randomUUID().toString() +".pcm";
        File sourceFile = FileUtil.create(sourceFileName, bytes);
        // 将MultipartFile对象转file对象
//        sourceFile = FileUtil.multipartFileToFile(multipartFile, sourceFile);

        // 临时target文件
        File targetFile = new File(UUID.randomUUID().toString() +".mp3");

        MediaUtil.pcmToMp3(sourceFile, targetFile);
        log.debug("文件格式转换成功");

        // 将MP3文件保存到数据库
        String mp3Url = FastDfsPool.upload(targetFile);
        log.debug("mp3上传到fastdfs完成");
        audioRecord.setAudioFilePath(mp3Url);

        String path = targetFile.getAbsolutePath();
        String result = translateAudio(path);
        log.debug("讯飞接口调用完成");
        JSONArray resultArray = JSON.parseArray(result);

        // 文本文件
        File txtFile = null;
        if(resultArray != null && !resultArray.isEmpty()){
            // 拼接转写结果
            StringBuilder stringBuilder = new StringBuilder();
            JSONObject jsonObject = null;
            for(int i = 0; i < resultArray.size(); i++){
                jsonObject = resultArray.getJSONObject(i);
                if(jsonObject.containsKey("onebest")){
                    stringBuilder.append(jsonObject.getString("onebest"));
                }
            }

            // 将转写文本上传到fastdfs服务器
            String txtName = UUID.randomUUID().toString() +".txt";
            txtFile = new File(txtName);
            FileUtil.stringToFile(stringBuilder.toString(), txtFile);
            String txtUrl = FastDfsPool.upload(txtFile);
            log.debug("转写结果上传fastdfs完成");
            audioRecord.setTextFilePath(txtUrl);
        }

        String rtText = null;

        String redisKey = redisUtil.createRealKey("rtasr-break-" + audioRecord.getId());
        if(Boolean.TRUE.equals(redisUtil.hasKey(redisKey))){
            // 有过断网，对整个文件进行重新的转写
            // 对整个源文件进行实时语音转写
            endTranslate(audioRecord.getId(), sourceFile);
            rtText = reorganizeRTTextsNew(audioRecord.getId());

        } else {
            // 没有断网，直接使用实时转写结果
            rtText = reorganizeRTTexts(audioRecord.getId());
        }

        if (rtText != null) {
            File rtTestFile = new File(UUID.randomUUID().toString() + ".txt");
            FileUtil.stringToFile(rtText, rtTestFile);
            String rtTestFileUrls = FastDfsPool.upload(rtTestFile);
            audioRecord.setRtTextFilePath(rtTestFileUrls);
            // 删除临时文件
            FileUtil.deleteFiles(rtTestFile);
        }

        // 保存信息
        audioDao.save(audioRecord);

        // 删除临时文件
        FileUtil.deleteFiles(targetFile, sourceFile, txtFile);
    }

    public String reorganizeRTTexts (String audioId){

        String key = REDIS_PRE_KEY_RTASR + audioId;
        String redisKey = redisUtil.createRealKey(key);

        if (Boolean.FALSE.equals(redisUtil.hasKey(redisKey))) {
            return null;
        }

        String redisValue = String.valueOf(redisUtil.get(redisKey));
        List<RTASTResultResp> resultRespList = JSON.parseArray(redisValue, RTASTResultResp.class);

        if(resultRespList == null){return null;}

        // 重新组合列表，填充空数据
        resultRespList = reList(resultRespList);

        RTASTResultResp lastElement = new RTASTResultResp();
        lastElement.setStart(0);
        List<RTASTResultResp> finalResult = new ArrayList<>();

        // 时长计算中的单位都为毫秒
        // 可用时长
        int availableTime;
        // 已转时长
        int usedTime = 0;
        // 目标时长
        int aimedTime;
        for (RTASTResultResp resp : resultRespList) {

            aimedTime = (finalResult.size() + 1) * STEP;
            availableTime = aimedTime - usedTime;

            // 当前文字的音频时长
            int length = resp.getEnd() - resp.getStart();

//            int currentLength = lastElement.getAudioLength() + length;
            int currentLength = lastElement.getAudioLength();
            if (currentLength >= availableTime) {
                // 超出可用，则加入最终列表
                finalResult.add(lastElement);
                int start = lastElement.getEnd();
                lastElement = new RTASTResultResp();
                lastElement.setStart(start);
                lastElement.setText(resp.getText());
                lastElement.setEnd(start + length);
                lastElement.setAudioLength(length);
                usedTime += length;

            } else {
                lastElement.setAudioLength(lastElement.getAudioLength() + length);
                lastElement.setText((lastElement.getText() != null ? lastElement.getText() : "") + resp.getText());
                usedTime += length;
                lastElement.setEnd(lastElement.getEnd() + length);
            }

            if (resp.equals(resultRespList.get(resultRespList.size() - 1))) {
                finalResult.add(lastElement);
            }

        }

        return JSON.toJSON(finalResult).toString();
    }
    /**
     * 重组讯飞实时转写文本(优化版)
     *
     * @param audioId
     * @return
     */
    public String reorganizeRTTextsNew(String audioId) {
        String key = REDIS_PRE_KEY_RTASR_END + audioId;
        String redisKey = redisUtil.createRealKey(key);

        if (Boolean.FALSE.equals(redisUtil.hasKey(redisKey))) {
            return null;
        }

        String redisValue = String.valueOf(redisUtil.get(redisKey));
        List<RTASTResultResp> resultRespList = JSON.parseArray(redisValue, RTASTResultResp.class);

        if(resultRespList == null){return null;}

        resultRespList = reList(resultRespList);
        RTASTResultResp lastElement = new RTASTResultResp();
        lastElement.setStart(0);
        List<RTASTResultResp> finalResult = new ArrayList<>();

        // 时长计算中的单位都为毫秒
        // 可用时长
        int availableTime;
        // 已转时长
        int usedTime = 0;
        // 目标时长
        int aimedTime;
        for (RTASTResultResp resp : resultRespList) {

            aimedTime = (finalResult.size() + 1) * STEP;
            availableTime = aimedTime - usedTime;

            // 当前文字的音频时长
            int length = resp.getEnd() - resp.getStart();

//            int currentLength = lastElement.getAudioLength() + length;
            int currentLength = length;

            if (currentLength >= availableTime) {
                // 超出可用，则加入最终列表
                finalResult.add(lastElement);
                int start = lastElement.getEnd();
                lastElement = new RTASTResultResp();
                lastElement.setStart(start);
                lastElement.setText(resp.getText());
                lastElement.setEnd(start + length);
                lastElement.setAudioLength(length);
                usedTime += length;

            } else {
                lastElement.setAudioLength(lastElement.getAudioLength() + length);
                lastElement.setText((lastElement.getText() != null ? lastElement.getText() : "") + resp.getText());
                usedTime += length;
                lastElement.setEnd(lastElement.getEnd() + length);
            }

            if (resp.equals(resultRespList.get(resultRespList.size() - 1))) {
                finalResult.add(lastElement);
            }

        }

        return JSON.toJSON(finalResult).toString();
    }
    private List<RTASTResultResp> reList(List<RTASTResultResp> sourceList){

        if (sourceList == null || sourceList.isEmpty()) {
            return sourceList;
        }

        List<RTASTResultResp> result = new ArrayList<>();

        RTASTResultResp lastElement = null;
        for(RTASTResultResp source : sourceList){

            // 如果第一个元素的开始时间不是0，则填充空白元素
            if (lastElement == null) {
                if(source.getStart() != 0){
                    List<RTASTResultResp> tempList = getTempList(0, source.getStart());
                    result.addAll(tempList);
                }

            } else {
                // 如果当前元素开始时间不是紧跟上一元素结束时间，则插入中间元素
                if (lastElement.getEnd() != source.getStart()){

                    List<RTASTResultResp> tempList = getTempList(lastElement.getEnd(), source.getStart());
                    result.addAll(tempList);
                }
            }
            result.add(source);
            lastElement = source;
        }

        return result;
    }
    private List<RTASTResultResp> getTempList(int bg, int ed){

        int interval = 1000;

        List<RTASTResultResp> result = new ArrayList<>();
        int timeInterval = ed - bg;

        // 按1秒来分割，球出当前的数组个数
        int count = timeInterval % interval == 0 ? timeInterval / interval : timeInterval / interval + 1;
        RTASTResultResp rtastResultResp = null;
        for(int i = 0 ; i < count; i ++){
            rtastResultResp = new RTASTResultResp();
            rtastResultResp.setText("");

            rtastResultResp.setStart(bg + i * interval);
            int tempEd = bg + (i + 1) * interval;
            rtastResultResp.setEnd(tempEd > ed ? ed : tempEd);

            rtastResultResp.setAudioLength(rtastResultResp.getEnd() - rtastResultResp.getStart());

            result.add(rtastResultResp);
        }

        return result;
    }


    public void endTranslate(String audioId, File pcmFile) throws URISyntaxException, InterruptedException {

        RTASRClient client =  RTASRClient.getInstance(audioId);
        client.translate2(audioId, pcmFile.getAbsolutePath());

    }

    /**
     * 异步执行实时语音补传转写
     *
     * @param audioId        语音音频
     * @param realtimeUpload 参数（文件，开始时间，结束时间）
     */
    @Async
    @Override
    public void realtimeUploadTranslate(String audioId, RealtimeUpload realtimeUpload) {

        // 临时source文件
        File sourceFile = new File( UUID.randomUUID().toString() +".pcm");
        // 将MultipartFile对象转file对象
        try {
            sourceFile = FileUtil.multipartFileToFile(realtimeUpload.getFile(), sourceFile);
        } catch (IOException e) {
            log.error("multipartFile转File失败！！！", e);
            return ;
        }

        // 临时target文件,讯飞语音转写不支持pcm文件，将pcm源文件转成mp3格式
        File targetFile = new File(UUID.randomUUID().toString() +".mp3");
        MediaUtil.pcmToMp3(sourceFile, targetFile);


        String path = targetFile.getAbsolutePath();
        try {
            // 进行语音转写
            String result = translateAudio(path);
            log.debug("补上传音频转写结果：" + result);
            JSONObject json = new JSONObject();
            json.put("audioId", audioId);
            json.put("startTime", DateUtils.formatLocalTime(realtimeUpload.getStartTime(), DateUtils.PATTERN_TIME));
            json.put("endTime", DateUtils.formatLocalTime(realtimeUpload.getEndTime(), DateUtils.PATTERN_TIME));
            json.put("text", result);

            // 将结果写入缓存
            // 默认保存一天
            String redisKey = redisUtil.createRealKey("realtimeUpload-" + audioId);
            redisUtil.set(redisKey, json, 60 * 60 * 24L);

        } catch (InterruptedException e) {
            log.error("补上传语音转写异常！！！", e);
            Thread.currentThread().interrupt();
        } finally {
            try {
                // 删除临时文件
                FileUtil.deleteFiles(sourceFile, targetFile);
            } catch (IOException e) {
                log.error("文件删除失败！！！", e);
            }
        }
    }

}
