package cn.com.cowain.ibms.rest.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/3 21:16
 */
@Data
public class RoomReq implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @NotBlank(message = "名称不能为空")
    @ApiModelProperty(name = "名称", required = true)
    private String name;

    /**
     * 人数容量
     */
    @NotNull(message = "容量不能为空")
    @Max(value = 999,message = "容纳人数不能大于999")
    @Min(value = 0,message = "容纳人数必须大于0")
    @ApiModelProperty(name = "容纳人数", required = true)
    private Integer capacity;

    /**
     * 是否开放
     * 0:否
     * 1:是
     */
    @ApiModelProperty(name = "是否开放")
    private int isOpen;

    /**
     * 开放时间
     */
    @ApiModelProperty(name = "开放时间", required = true)
    private List<OpeningHoursReq> openingHours;

    /**
     * 包含设备
     */
    @ApiModelProperty(name = "包含设备", required = true)
    private List<String> devices;

    /**
     * 项目ID
     */
    @NotEmpty(message = "项目ID不能为空")
    @ApiModelProperty(name = "项目id", required = true)
    private String projectId;

    @NotEmpty(message = "空间ID不能为空")
    @ApiModelProperty(name = "空间id", required = true)
    private String spaceId;

    @ApiModelProperty(name = "物品清单")
    private List<String> thingList;

    @Length(max = 300)
    @ApiModelProperty(name = "注意事项")
    private String attentions;

    @ApiModelProperty(name = "会议室图片")
    private String img;

    @ApiModelProperty(name = "图片名称")
    private String originalImgName;
}
