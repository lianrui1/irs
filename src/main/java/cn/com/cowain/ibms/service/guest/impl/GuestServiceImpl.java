package cn.com.cowain.ibms.service.guest.impl;

import cn.com.cowain.ibms.dao.meeting.AttendanceDao;
import cn.com.cowain.ibms.dao.meeting.GuestDetailDao;
import cn.com.cowain.ibms.dao.meeting.GuestInvitationRecordDao;
import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.entity.meeting.Attendance;
import cn.com.cowain.ibms.entity.meeting.GuestDetail;
import cn.com.cowain.ibms.entity.meeting.GuestInvitationRecord;
import cn.com.cowain.ibms.enumeration.ReservationRecordItemStatus;
import cn.com.cowain.ibms.enumeration.meeting.GuestInvitationStatus;
import cn.com.cowain.ibms.rest.bean.ParticipantBean;
import cn.com.cowain.ibms.rest.resp.distinguish.StaffPageResp;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.Guest;
import cn.com.cowain.ibms.service.guest.GuestService;
import cn.com.cowain.ibms.utils.RedisUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Yang.Lee
 * @date 2021/12/14 9:52
 */
@Service
@Slf4j
public class GuestServiceImpl implements GuestService {

    private static final String STAFF_KEY = "staffList";
    //员工数据缓存有效秒数
    private static final Long STAFF_VALID_TIME_SECOND = 1860L;
    @Resource
    private RedisUtil redisUtil;
    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserService;
    @Autowired
    private GuestDetailDao detailDao;

    @Autowired
    private GuestInvitationRecordDao recordDao;

    @Resource
    private AttendanceDao attendanceDao;

    @Resource
    private GuestService guestService;

    /**
     * 根据手机号查询访客信息
     *
     * @param mobile 手机号码
     * @return 访客信息
     * @author Yang.Lee
     * @date 2021/12/14 9:51
     **/
    @Override
    public Optional<Guest> getByMobile(String mobile) {

        String realKey = redisUtil.createRealKey(STAFF_KEY);

        List<StaffPageResp> staffList;
        // 判断redis是否存在数据
        if (Boolean.TRUE.equals(redisUtil.hasKey(realKey))) {
            staffList = JSON.parseArray(String.valueOf(redisUtil.get(realKey)), StaffPageResp.class);
            Optional<Guest> guestInCache = getGuest(mobile, staffList);
            if (guestInCache.isPresent()) {
                return guestInCache;
            }
            //缓存中不存在时，为以防缓存数据不是最新的，申请ehr查询最新的访客信息进行查询
            staffList = sysUserService.syncGuestList();
            Optional<Guest> guest = getGuest(mobile, staffList);
            if (guest.isPresent()) {
                return guest;
            }
        } else {
            staffList = sysUserService.syncStaffList();
            redisUtil.set(realKey, staffList, STAFF_VALID_TIME_SECOND);
            Optional<Guest> guest = getGuest(mobile, staffList);
            if (guest.isPresent()) {
                return guest;
            }
        }

        return Optional.empty();
    }

    /**
     * 从访客列表中获取访客
     *
     * @param mobile
     * @param staffList
     * @return
     */
    private Optional<Guest> getGuest(String mobile, List<StaffPageResp> staffList) {
        if (CollectionUtils.isEmpty(staffList)) {
            return Optional.empty();
        }
        for (StaffPageResp obj : staffList) {
            if (StringUtils.equals(obj.getMobile(), mobile)) {
                Guest guest = new Guest();
                guest.setMobile(obj.getMobile());
                guest.setHrId(obj.getEmpNo());
                guest.setName(obj.getName());
                return Optional.of(guest);
            }
        }
        return Optional.empty();
    }

    /**
     * 获取访客列表
     *
     * @return 访客列表
     * @author Yang.Lee
     * @date 2021/12/14 10:48
     **/
    @Override
    public List<StaffPageResp> getGuestList() {

        String realKey = redisUtil.createRealKey(STAFF_KEY);

        List<StaffPageResp> staffList;
        // 判断redis是否存在数据
        if (Boolean.TRUE.equals(redisUtil.hasKey(realKey))) {
            staffList = JSON.parseArray(String.valueOf(redisUtil.get(realKey)), StaffPageResp.class);
        } else {
            staffList = sysUserService.syncStaffList();
            redisUtil.set(realKey, staffList, STAFF_VALID_TIME_SECOND);
        }

        return staffList.stream().filter(obj -> obj.getIsStaff() == 1).collect(Collectors.toList());
    }

    @Override
    public List<ParticipantBean> getGuest(ReservationRecord reservationRecord) {

        List<ParticipantBean> list = new ArrayList<>();
        List<GuestDetail> detailList = detailDao.findByInvitationReservationRecord(reservationRecord);
        for (GuestDetail detail : detailList) {
            if (detail.getIsDelete() == 0) {
                ParticipantBean participantBean = new ParticipantBean();
                // 设置参会人id
                participantBean.setOwner(detail.getMobile());
                //设置参会人姓名
                participantBean.setOwnerName(detail.getName());
                // 设置参会人状态
                participantBean.setStatus(ReservationRecordItemStatus.CONFIRM);
                // 设置部门Id
                participantBean.setDeptId("");
                // 设置部门名称
                participantBean.setDeptName(detail.getCompany());
                // 设置工号
                participantBean.setHrId(detail.getMobile());
                // 微信头像
                participantBean.setHeadImgUrl("");
                // 是否访客
                participantBean.setIsGuest(1);
                participantBean.setCode("");
                list.add(participantBean);
            }
        }
        List<GuestInvitationRecord> recordList = recordDao.findByInvitationCardReservationRecord(reservationRecord);
        for (GuestInvitationRecord record : recordList) {
            if (record.getIsDelete() == 0) {
                ParticipantBean participantBean = new ParticipantBean();
                // 设置参会人id
                participantBean.setOwner(record.getMobile());
                //设置参会人姓名
                participantBean.setOwnerName(record.getName());
                // 设置参会人状态
                if (GuestInvitationStatus.DEFAULT.equals(record.getGuestInvitationStatus())) {
                    participantBean.setStatus(ReservationRecordItemStatus.UNDETERMINED);
                } else if (GuestInvitationStatus.ACCEPTED.equals(record.getGuestInvitationStatus())) {
                    participantBean.setStatus(ReservationRecordItemStatus.CONFIRM);
                } else {
                    participantBean.setStatus(ReservationRecordItemStatus.CANCEL);
                }
                // 设置部门Id
                participantBean.setDeptId("");
                // 设置部门名称
                participantBean.setDeptName("");
                // 设置工号
                participantBean.setHrId(record.getMobile());
                // 微信头像
                participantBean.setHeadImgUrl("");
                // 是否访客
                participantBean.setIsGuest(1);
                participantBean.setCode(record.getCode());
                list.add(participantBean);
            }
        }
        Collections.reverse(list);

        list = list.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(ParticipantBean::getHrId))), ArrayList::new));


        // 判断访客是否已签到
        list.forEach(obj -> {


            Optional<Guest> guestOptional = guestService.getByMobile(obj.getHrId());
            if(guestOptional.isPresent()){

                Guest guest = guestOptional.get();
                Optional<Attendance> attendanceOptional = attendanceDao.findByUserEmpNoAndReservationRecordId(guest.getHrId(), reservationRecord.getId());

                if(attendanceOptional.isPresent()){
                    obj.setIsAttendance(1);
                }
            }

        });

        return list;
    }
}
