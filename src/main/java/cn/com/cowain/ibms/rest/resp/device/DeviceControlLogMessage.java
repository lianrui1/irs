package cn.com.cowain.ibms.rest.resp.device;


import cn.com.cowain.ibms.enumeration.iot.DeviceControlStatus;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel("设备控制日志消息体推送")
public class DeviceControlLogMessage {


    /**
     * 设备id
     */
    private String hwDeviceId;


    /*
    *
    * 设备名称
    * */
    private String hwDeviceName;

    /*
     *
     * 设备位置
     * */
    private String address;


    /**
     * 控制命令参数
     */
    private String command;


    /**
     * 控制人工号
     */
    private String hrId;

    /**
     * 控制人姓名
     */
    private String name;

    /**
     * 场景模式
     */
    private String mode;


    /**
     * 日志信息来源
     */
    private String source;

    /**
     * 设备控制状态
     */
    private DeviceControlStatus deviceControlStatus;

    // 控制结果
    private String result;

    // 控制时间
    private LocalDateTime controlTime;
}
