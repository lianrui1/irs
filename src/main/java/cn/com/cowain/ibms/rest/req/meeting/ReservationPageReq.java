package cn.com.cowain.ibms.rest.req.meeting;

import cn.com.cowain.ibms.enumeration.ReservationRecordStatus;
import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * @author Yang.Lee
 * @date 2021/7/8 13:39
 */
@Data
@ApiModel("会议分页请求参数对象")
public class ReservationPageReq extends PageReq {

    @ApiModelProperty(value = "地区（项目）ID")
    private String projectId;

    @ApiModelProperty(value = "会议室ID")
    private String roomId;

    @ApiModelProperty(value = "会议状态")
    private ReservationRecordStatus status;

    @ApiModelProperty(value = "搜索关键字（姓名，工号，部门，会议主题）")
    private String keyWord;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm" )
    @ApiModelProperty(value = "预约会议日期(开始时间)")
    private LocalDateTime startTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm" )
    @ApiModelProperty(value = "预约会议日期(结束时间)")
    private LocalDateTime endTime;
}
