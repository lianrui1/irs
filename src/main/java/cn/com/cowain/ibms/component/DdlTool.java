package cn.com.cowain.ibms.component;

import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.boot.spi.MetadataImplementor;
import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.schema.TargetType;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.metamodel.ManagedType;
import java.util.EnumSet;
import java.util.Set;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/7/30 16:29
 */
public class DdlTool {

    private EntityManager entityManager;
    private Configuration cfg;
    private MetadataSources metadata;

    public DdlTool(String driver, String url, String username, String password, EntityManager entityManager) {
        this.entityManager = entityManager;
        this.cfg = new Configuration();
        cfg.setProperty("hibernate.connection.driver_class", driver)
                .setProperty("hibernate.connection.url", url)
                .setProperty("hibernate.connection.username", username)
                .setProperty("hibernate.connection.password", password)
                .setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5InnoDBDialect");
    }

    /**
     * 创建表结构
     */
    public void generateTables() {
        SchemaExport schemaExport = new SchemaExport();
        schemaExport.setHaltOnError(true);
        schemaExport.createOnly(EnumSet.of(TargetType.DATABASE), this.metadata.buildMetadata());
    }

    /**
     * 保存ddl文件至指定目录
     *
     * @param path
     * @return
     */
    public String ddl2File(String path) {
        SchemaExport export = new SchemaExport();
        export.setHaltOnError(true);
        export.setFormat(true);
        export.setDelimiter(";");
        String filename = path + "mysql-" + System.currentTimeMillis() + ".sql";
        export.setOutputFile(filename);
        EnumSet<TargetType> enumSet = EnumSet.of(TargetType.SCRIPT);

        MetadataImplementor metadataImplementor = (MetadataImplementor) this.metadata.buildMetadata();
        export.execute(enumSet, SchemaExport.Action.CREATE, metadataImplementor);
        return filename;
    }

    /**
     * 初始化
     *
     * @return 实体类
     */
    @PostConstruct
    public void init() {
        this.metadata = new MetadataSources(
                new StandardServiceRegistryBuilder()
                        .applySettings(cfg.getProperties())
                        .build());

        EntityManagerFactory entityManagerFactory = entityManager.getEntityManagerFactory();
        final Set<ManagedType<?>> managedTypes =
                entityManagerFactory.getMetamodel().getManagedTypes();
        for (ManagedType<?> managedType : managedTypes) {
            final Class<?> javaType = managedType.getJavaType();
            metadata.addAnnotatedClass(javaType);
        }
    }

}
