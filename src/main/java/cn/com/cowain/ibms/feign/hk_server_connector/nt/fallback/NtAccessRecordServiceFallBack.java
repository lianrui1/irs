package cn.com.cowain.ibms.feign.hk_server_connector.nt.fallback;

import cn.com.cowain.ibms.feign.hk_server_connector.nt.NtAccessRecordService;
import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.req.distinguish.AuthTimeDataReq;
import cn.com.cowain.ibms.rest.req.hk.FaceRecord;
import cn.com.cowain.ibms.rest.resp.EquipmentDataResp;
import cn.com.cowain.ibms.rest.resp.SearchStaffDataResp;
import cn.com.cowain.ibms.rest.resp.distinguish.AuthTimeDataFailRep;
import cn.com.cowain.ibms.utils.ErrConst;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author tql
 * @Description NtAccessRecordService 服务降级
 * @Date 21-5-21 上午10:13
 * @Version 1.0
 */
@Component
public class NtAccessRecordServiceFallBack implements NtAccessRecordService {

    /**
     * 获取考勤记录
     *
     * @param faceRecord 开始时间/结束时间 example:2021-05-15
     * @return
     */
    @Override
    public JsonResult<List<SearchStaffDataResp>> searchStaff(FaceRecord faceRecord) {
        return JsonResult.error("请求失败", null, ErrConst.E01);
    }

    /**
     * 健康检测
     *
     * @return
     */
    @Override
    public JsonResult healthCheck() {
        return JsonResult.error("请求失败", null, ErrConst.E01);
    }

    /**
     * 获取设备列表
     *
     * @return
     */
    @Override
    public JsonResult<List<EquipmentDataResp>> getEquipmentList() {
        return JsonResult.error("请求失败", null, ErrConst.E01);
    }

    /**
     * 更新海康南通排班信息
     *
     * @param authTimeDataList
     * @return
     */
    @Override
    public JsonResult<List<AuthTimeDataFailRep>> updateAuth(List<AuthTimeDataReq> authTimeDataList) {
        return JsonResult.error("请求失败", null, ErrConst.E01);
    }
}
