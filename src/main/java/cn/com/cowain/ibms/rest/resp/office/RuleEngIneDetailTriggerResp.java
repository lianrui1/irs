package cn.com.cowain.ibms.rest.resp.office;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalTime;
import java.util.List;

/**
 * 联动规则触发条件返回对象
 *
 * @author: yanzy
 * @date: 2022/2/11 10:07
 */
@Data
@ApiModel("联动规则触发条件返回对象")
public class RuleEngIneDetailTriggerResp {

    @ApiModelProperty(value = "定时 ")
    @JsonFormat(pattern = "HH:mm")
    private LocalTime timing;

    //传感器设备
    @ApiModelProperty(value = "设备ID")
    private String deviceId;

    @ApiModelProperty(value = "传感器设备返回参数")
    private List<RuleEngIneDetailTriggerSensorResp> sensorReqs;

    @ApiModelProperty(value = "传感器返回参数")
    private SensorDeviceResp sensorDeviceResp;

}
