package cn.com.cowain.ibms.rest.resp.office;

import cn.com.cowain.ibms.rest.resp.distinguish.SwitchResp;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author feng
 * @title: AirConditionerStatusDetailResp
 * @projectName ibms
 * @Date 2021/12/21 14:07
 */
@Data
public class SwitchDetailResp extends SwitchResp {
    String id;
    String hwDeviceId;
    String hwStatus;
    String deviceName;

    private String light1;
    private String light2;
    private String light3;
    private String light4;
    private Double electricity;
    @JsonProperty("switch")
    private String isSwitch;
    private String humidity;
    private String luminance;
    private String noise;
    private String pm10;
    private String pm2_5;
    private String temperature;
    private String read = "read";
    private String isPeopleThere;
}
