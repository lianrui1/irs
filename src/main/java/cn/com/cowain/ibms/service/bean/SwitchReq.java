package cn.com.cowain.ibms.service.bean;

import lombok.Data;

import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/16 16:39
 */
@Data
public class SwitchReq {
    private List<Services> services;
}
