package cn.com.cowain.ibms.mq.consumer;

import cn.com.cowain.ibms.feign.bean.iotc.resp.DeviceResp;
import cn.com.cowain.ibms.mq.Topic;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.space.IotDeviceService;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 设备锁定修改设备
 *
 * @author: yanzy
 * @date: 2022/9/8 11:11
 */
@Slf4j
@RocketMQMessageListener(
        topic = "${rocketmq.env}-" + Topic.IOTC_TO_IBMS_UPD_DEVICE,
        consumerGroup = "${rocketmq.group}-" + Topic.IOTC_TO_IBMS_UPD_DEVICE
)
@Component
public class IotcUpdateDeviceConsumer implements RocketMQListener<String> {

    @Autowired
    private IotDeviceService iotDeviceService;

    @Override
    public void onMessage(String message) {
        log.debug("iotcUpdateDeviceConsumer onMessage, message:{}",message);
        try {
            //解析数据
            DeviceResp deviceResp = JSON.parseObject(message, DeviceResp.class);
            ServiceResult serviceResult = iotDeviceService.iotcToIbmsUpdDevice(deviceResp);
            log.debug("result:{}", serviceResult);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
