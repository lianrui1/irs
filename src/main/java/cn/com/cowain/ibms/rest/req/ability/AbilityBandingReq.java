package cn.com.cowain.ibms.rest.req.ability;

import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author feng
 * @title: AbilityReq
 * @projectName ibms
 * @Date 2022/1/11 11:23
 */
@Data
@ApiModel("查询列表")
public class AbilityBandingReq extends PageReq {
    @ApiModelProperty(value = "基础ID", required = true)
    String basicId;
    @ApiModelProperty(value = "设备id列表", required = false)
    List<String> accessControlIds;
}
