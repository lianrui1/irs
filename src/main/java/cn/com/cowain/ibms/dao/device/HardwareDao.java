package cn.com.cowain.ibms.dao.device;

import cn.com.cowain.ibms.entity.iot.Hardware;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface HardwareDao extends JpaRepository<Hardware, String>, JpaSpecificationExecutor<Hardware> {

    // 根据SN码获取硬件信息
    Optional<Hardware> findByHardWareSn(String hardWareSn);

    /**
     * 根据产品ID查询硬件列表
     *
     * @param productId 产品ID
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/4/7 10:50
     **/
    List<Hardware> findByIotProductId(String productId);
}