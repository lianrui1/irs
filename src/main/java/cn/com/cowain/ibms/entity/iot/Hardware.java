package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.space.IotProduct;
import cn.com.cowain.ibms.enumeration.IotNodeType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 硬件实体
 *
 * @author Yang.Lee
 * @date 2021/3/17 15:17
 */
@Data
@Entity
@Table(name = "iot_hardware_list")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_hardware_list", comment = "iot硬件清单")
public class Hardware extends BaseEntity {

    /**
     * 硬件名称，中文
     */
    @Column(name = "name_zh", nullable = false, columnDefinition = "varchar(50) COMMENT '硬件名称，中文'")
    private String nameZh;

    /**
     * 硬件名称，英文
     */
    @Column(name = "name_en", nullable = false, columnDefinition = "varchar(50) COMMENT '硬件名称，英文'")
    private String nameEn;

    /**
     * 设备类型类型
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "device_type", nullable = false, columnDefinition = "varchar(50) COMMENT '设备类型'")
    private IotNodeType deviceType;

    /**
     * 所属产品
     */
    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 设备ID'")
    private IotProduct iotProduct;

    /**
     * 品牌
     */
    @Column(name = "brand", columnDefinition = "varchar(50) COMMENT '品牌'")
    private String brand;

    /**
     * 型号
     */
    @Column(name = "model", columnDefinition = "varchar(50) COMMENT '型号'")
    private String model;

    /**
     * 硬件名称，中文
     */
    @Column(name = "hardWareSn", nullable = false, columnDefinition = "varchar(200) COMMENT '硬件SN码'")
    private String hardWareSn;
}
