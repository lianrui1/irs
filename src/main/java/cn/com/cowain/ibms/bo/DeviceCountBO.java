package cn.com.cowain.ibms.bo;

import cn.com.cowain.ibms.entity.iot.IotDevice;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wei.cheng
 * @date 2022/09/26 14:27
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeviceCountBO {

    @ApiModelProperty(value = "控制设备个数")
    private Integer count = 0;

    @ApiModelProperty(value = "控制设备中已开启的数量")
    private Integer countOpen = 0;

    @ApiModelProperty(value = "设备列表")
    private List<IotDevice> devices = new ArrayList<>();

    @ApiModelProperty(value = "开启的设备id列表")
    private List<String> openDeviceIds = new ArrayList<>();

    public DeviceCountBO(List<IotDevice> devices, List<String> openDeviceIds) {
        this.devices = devices;
        this.openDeviceIds = openDeviceIds;
        this.count = devices.size();
        this.countOpen = openDeviceIds.size();
    }
}
