package cn.com.cowain.ibms.rest.resp.space;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/8 11:28
 */
@Data
@ApiModel("用途列表响应对象")
public class UseResp {

    @ApiModelProperty(value = "用途ID", required = true)
    private String useId;

    @ApiModelProperty(value = "用途名称", required = true)
    private String useName;
}
