package cn.com.cowain.ibms.utils;

import cn.com.cowain.ibms.rest.bean.TimeSlotBean;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

@Slf4j
class DateUtilTest {

    @Test
    void splitDate() {
        List<TimeSlotBean> timeSlotBeans = DateUtil.splitDate("08:00", "11:00");
        log.info(String.valueOf(timeSlotBeans));
    }

    @Test
    void test2() {
        String date = "2020-08-12";
        String from = "08:00";
        LocalDateTime localDateTime = DateUtil.merge(date, from);
        System.out.println(localDateTime);
    }

    @Test
    void test3() {
        // 发起人在会议开始后的半小时内，以及开始前，可以取消会议、编辑会议
        while (true) {
            LocalDateTime now = LocalDateTime.now();
            long lNow = now.toEpochSecond(ZoneOffset.of("+8"));
            System.out.println(lNow);
        }
    }

}
