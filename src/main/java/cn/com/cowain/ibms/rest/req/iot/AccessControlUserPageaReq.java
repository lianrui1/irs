package cn.com.cowain.ibms.rest.req.iot;

import cn.com.cowain.ibms.enumeration.UserImportStatus;
import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/4/25 13:52
 */
@Data
@ApiModel("门禁人员列表请求对象")
public class AccessControlUserPageaReq extends PageReq {

    @ApiModelProperty(value = "门禁ID")
    private String spotId;

    @ApiModelProperty(value = "1同步数据 2设备异常数据")
    private String type;

    @ApiModelProperty(value = "任务ID")
    private String taskId;

    @ApiModelProperty(value = "人员状态")
    private UserImportStatus userImportStatus;
}
