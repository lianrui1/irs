package cn.com.cowain.ibms.rest.resp.device;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/2/26 15:24
 */
@Data
public class HwProductsResp {

    @JsonProperty(value = "app_id")
    private String appId;

    @JsonProperty(value = "app_name")
    private String appName;

    @JsonProperty(value = "product_id")
    private String productId;

    private String name;

    @JsonProperty(value = "device_type")
    private String deviceType;

    @JsonProperty(value = "protocol_type")
    private String protocolType;

    @JsonProperty(value = "data_format")
    private String dataFormat;

    @JsonProperty(value = "manufacturer_name")
    private String manufacturerName;

    private String industry;

    private String description;

    @JsonProperty(value = "create_time")
    private String createTime;
}
