package cn.com.cowain.ibms.rest.resp.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author wei.cheng
 * @date 2022/06/29 09:07
 */
@Data
@ApiModel(value = "通行权限")
public class AccessAuthResp {

    @ApiModelProperty(value = "用户已拥有的权限列表")
    private List<UserAuthResp> authList;

    @ApiModelProperty(value = "用户可申请的权限列表")
    private List<UserAuthResp> applicationList;
}
