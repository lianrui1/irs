package cn.com.cowain.ibms.rest.space;

import cn.com.cowain.ibms.enumeration.iot.DataSource;
import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.iot.HIKFaceMachinePageReq;
import cn.com.cowain.ibms.rest.req.iot.RemoveDeviceControlReq;
import cn.com.cowain.ibms.rest.req.space.DevicePageReq;
import cn.com.cowain.ibms.rest.req.space.IotDeviceCreateReq;
import cn.com.cowain.ibms.rest.req.space.IotDeviceSortReq;
import cn.com.cowain.ibms.rest.resp.BaseListResp;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.StaffResp;
import cn.com.cowain.ibms.rest.resp.iot.HIKAccessRulePageResp;
import cn.com.cowain.ibms.rest.resp.iot.IotDeviceNetworkChangeResp;
import cn.com.cowain.ibms.rest.resp.iot.IotDeviceResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.space.IotDeviceService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author yuchunlei
 * @version 1.0.0
 * @createTime 2020年12月17日 17:36:00
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/iotDevice")
@Api(tags = IConst.IOTDEVICE_MANAGE)
public class IotDeviceController {

    @Autowired
    private IotDeviceService iotDeviceService;


    /**
     * 设备管理-分页查询设备列表
     *
     * @param devicePageReq 请求参数
     * @return 分页数据
     */
    @GetMapping("/search")
    @ApiOperation(value = "分页查询设备列表", tags = {IConst.IOTDEVICE_MANAGE, IConst.V17, IConst.V19})
    public ResponseEntity<JsonResult<PageBean<IotDeviceResp>>> searchAll(DevicePageReq devicePageReq) {
        // 参数：搜索条件, 分页页码,
        // 数据：数据，页码，总页数，
        PageBean<IotDeviceResp> pageBean = iotDeviceService.search(devicePageReq);
        JsonResult<PageBean<IotDeviceResp>> pageBeanJsonResult = JsonResult.ok("OK", pageBean);
        return ResponseEntity.ok(pageBeanJsonResult);
    }


    /**
     * 设备管理-新增设备
     *
     * @param dto
     * @param bindingResult
     * @return
     */
    @PostMapping
    @ApiOperation(value = "添加设备", tags = IConst.IOTDEVICE_MANAGE)
    public ResponseEntity<JsonResult<IdResp>> create(@RequestBody @Validated IotDeviceCreateReq dto,
                                                     BindingResult bindingResult) {

        ServiceResult result = iotDeviceService.create(dto);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.ok("ok", (IdResp) result.getObject()));
    }


    /**
     * 设备管理-编辑设备
     *
     * @param id
     * @param dto
     * @param bindingResult
     * @return
     */
    @PutMapping("/{id}")
    @ApiOperation(value = "编辑设备", tags = IConst.IOTDEVICE_MANAGE)
    public ResponseEntity<JsonResult<String>> update(@PathVariable String id, @RequestBody @Validated IotDeviceCreateReq dto,
                                                     BindingResult bindingResult) {

        ServiceResult serviceResult = iotDeviceService.update(id, dto);
        JsonResult<String> jsonResult = JsonResult.okNoData("OK");
        if (!serviceResult.isSuccess()) {
            jsonResult = JsonResult.error(serviceResult.getObject().toString(), null, ErrConst.E01);
        }
        return ResponseEntity.ok(jsonResult);
    }


    /**
     * 设备管理-查看设备详情
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "查看设备详情", tags = IConst.IOTDEVICE_MANAGE)
    public ResponseEntity<JsonResult<IotDeviceResp>> getIotDeviceInfo(@PathVariable String id,
                                                                      @RequestHeader(name = "Authorization", required = false, defaultValue = "") String token) {
        IotDeviceResp iotDeviceResp = iotDeviceService.getIotDeviceInfo(id, token);
        JsonResult<IotDeviceResp> respJsonResult = JsonResult.ok("OK", iotDeviceResp);
        return ResponseEntity.ok(respJsonResult);
    }


    /**
     * 设备管理-删除设备
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "设备删除", tags = IConst.IOTDEVICE_MANAGE)
    public ResponseEntity<JsonResult<String>> delete(@PathVariable String id) {
        ServiceResult serviceResult = iotDeviceService.deleteById(id);
        JsonResult<String> jsonResult = null;
        if (serviceResult.isSuccess()) {
            jsonResult = JsonResult.ok("OK", null);
        } else {
            jsonResult = JsonResult.error(String.valueOf(serviceResult.getObject()), null, ErrConst.E01);
        }

        return ResponseEntity.ok(jsonResult);
    }


    /**
     * 排序
     *
     * @param iotDeviceSortReq
     * @param bindingResult
     * @return
     * @author Yang.Lee
     */
    @PutMapping("/sort")
    @ApiOperation(value = "PC更新排序", tags = IConst.IOTDEVICE_MANAGE)
    public ResponseEntity<JsonResult<Object>> updateSort(@RequestBody @Validated IotDeviceSortReq iotDeviceSortReq, BindingResult bindingResult) {

        ServiceResult result = iotDeviceService.sort(iotDeviceSortReq);

        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error("操作失败", null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    /**
     * 获取数据来源列表
     *
     * @return 数据来源列表
     * @author Yang.Lee
     * @date 2021/3/30 10:28
     **/
    @GetMapping("/dataSource/list")
    @ApiOperation(value = "获取数据来源列表", tags = IConst.IOTDEVICE_MANAGE)
    public ResponseEntity<JsonResult<List<BaseListResp>>> getDataSourceList() {

        List<BaseListResp> dataSourceList = new ArrayList<>();
        for (DataSource dataSource : DataSource.values()) {
            dataSourceList.add(new BaseListResp(dataSource.toString(), dataSource.getName()));
        }

        return ResponseEntity.ok(JsonResult.ok(dataSourceList));
    }

    /**
     * 获取设备状态列表
     *
     * @return 设别状态列表
     * @author Yang.Lee
     * @date 2021/3/30 10:47
     **/
    @GetMapping("/deviceStatus/list")
    @ApiOperation(value = "获取设备状态列表", tags = IConst.IOTDEVICE_MANAGE)
    public ResponseEntity<JsonResult<List<BaseListResp>>> getDeviceStatusSourceList() {

        List<BaseListResp> deviceStatusList = new ArrayList<>();
        for (DeviceStatus dataSource : DeviceStatus.values()) {
            deviceStatusList.add(new BaseListResp(dataSource.toString(), dataSource.getName()));
        }

        return ResponseEntity.ok(JsonResult.ok(deviceStatusList));
    }

    /**
     * 获取设备类型列表
     *
     * @return 设别类型列表
     * @author Yang.Lee
     * @date 2021/3/30 10:47
     **/
    @GetMapping("/deviceType/list")
    @ApiOperation(value = "获取设备类型列表", tags = IConst.IOTDEVICE_MANAGE)
    public ResponseEntity<JsonResult<List<BaseListResp>>> getDeviceTypeList() {

        List<BaseListResp> deviceTypeList = new ArrayList<>();
        for (DeviceType deviceType : DeviceType.values()) {
            deviceTypeList.add(new BaseListResp(deviceType.toString(), deviceType.getName()));
        }

        return ResponseEntity.ok(JsonResult.ok(deviceTypeList));
    }

    /**
     * 获取设备网路状态变化分页数据
     *
     * @param id   设备ID
     * @param page 页码
     * @param size 页长
     * @return
     * @author Yang.Lee
     * @date 2021/4/29 9:10
     **/
    @GetMapping("/{id}/networkStatus/page")
    @ApiOperation(value = "获取设备网络状态变化列表", tags = IConst.IOTDEVICE_MANAGE)
    public ResponseEntity<JsonResult<PageBean<IotDeviceNetworkChangeResp>>> getDeviceNetworkChangePage(@PathVariable @ApiParam("设备ID") String id,
                                                                                                       @RequestParam(required = false) @ApiParam(value = "设备状态") String deviceStatus,
                                                                                                       @RequestParam(defaultValue = "0", required = false) @ApiParam(value = "页码", defaultValue = "0") int page,
                                                                                                       @RequestParam(defaultValue = "10", required = false) @ApiParam(value = "页长", defaultValue = "10") int size) {

        return ResponseEntity.ok(JsonResult.ok(iotDeviceService.getDeviceNetworkChangedPage(id, deviceStatus, page, size)));
    }

    /**
     * 获取海康面板机分页数据
     *
     * @param req
     * @return
     * @author Yang.Lee
     * @date 2021/7/20 14:34
     **/
    @GetMapping("/hikFaceMachine/page")
    @ApiOperation(value = "获取海康面板机列表", tags = {IConst.IOTDEVICE_MANAGE, IConst.V16})
    public ResponseEntity<JsonResult<PageBean<HIKAccessRulePageResp>>> getHIKFaceMachinePage(HIKFaceMachinePageReq req) {

        return ResponseEntity.ok(JsonResult.ok(iotDeviceService.getHIKFaceMachinePage(req)));
    }

    /**
     * 查询可以控制设备的员工列表
     *
     * @param id      空间ID
     * @param keyword 查询关键字
     * @return 人员列表
     * @author Yang.Lee
     * @date 2021/7/20 14:34
     **/
    @GetMapping("/{id}/staff/list")
    @ApiOperation(value = "查询可以控制设备的员工列表", tags = {IConst.IOTDEVICE_MANAGE, IConst.V19})
    public ResponseEntity<JsonResult<List<StaffResp>>> getDeviceStaff(@PathVariable String id, @RequestParam(required = false) @ApiParam(value = "查询关键字") String keyword) {

        return ResponseEntity.ok(JsonResult.ok(iotDeviceService.getDeviceStaff(id, keyword)));
    }

    /**
     * 移除用户的设备控制权限
     *
     * @param req      请求参数
     * @param deviceId 控制权限
     * @return 操作结果
     * @author Yang.Lee
     * @date 2021/11/22 13:18
     **/
    @PutMapping("/{deviceId}/removeControl")
    @ApiOperation(value = "移除用户的设备控制权限", tags = {IConst.IOTDEVICE_MANAGE, IConst.V19})
    public ResponseEntity<JsonResult<List<StaffResp>>> removeUserDeviceControlAccess(@RequestBody RemoveDeviceControlReq req,
                                                                                     @PathVariable @ApiParam(value = "设备ID") String deviceId) {

        ServiceResult result = iotDeviceService.removeDeviceControlAccess(deviceId, req.getUserHrId());

        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, result.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @GetMapping("/faceMachine/page")
    @ApiOperation(value = "获取面板机门磁列表", tags = {IConst.IOTDEVICE_MANAGE, IConst.V20})
    public ResponseEntity<JsonResult<PageBean<HIKAccessRulePageResp>>> faceMachinePage(HIKFaceMachinePageReq req) {

        return ResponseEntity.ok(JsonResult.ok(iotDeviceService.getFaceMachinePage(req)));
    }

    @GetMapping("/faceDevice/page")
    @ApiOperation(value = "梯控点绑定设备场景-获取面板机列表", tags = {IConst.IOTDEVICE_MANAGE, IConst.V20})
    public ResponseEntity<JsonResult<PageBean<HIKAccessRulePageResp>>> faceDevice(HIKFaceMachinePageReq req) {
        return ResponseEntity.ok(JsonResult.ok(iotDeviceService.faceDevice(req)));
    }
}
