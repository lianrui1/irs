package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.AirConditioner;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AirConditionerDao extends JpaRepository<AirConditioner, String> {

    Optional<AirConditioner> findByDevice(IotDevice iotDevice);

    // 根据华为云设备ID获取空调信息
    Optional<AirConditioner> findByDeviceHwDeviceId(String deviceId);
}
