package cn.com.cowain.ibms.rest.req.office;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2021/12/21 13:19
 */
@Data
public class LoginReq {

    @ApiModelProperty(value = "UC返回的code", required = true, example = "456", position = 1)
    private String code;
}
