package cn.com.cowain.ibms.rest.resp.device.centralized;

import cn.com.cowain.ibms.enumeration.integration_control.ControlDeviceRange;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 集控码详情返回参数
 *
 * @author: yanzy
 * @date: 2022/9/16 15:52
 */
@Data
@ApiModel(value = "集控码详情返回参数")
public class CentralizedControlCodeDetailResp {

    @ApiModelProperty(value = "集控码id",required = true)
    private String id;

    @ApiModelProperty(value = "集控码名称", required = true, position = 1)
    private String name;

    @ApiModelProperty(value = "项目id", required = true, position = 2)
    private String projectId;

    @ApiModelProperty(value = "关联空间id", required = true, position = 3)
    private String spaceId;

    @ApiModelProperty(value = "管理员工号")
    private String adminHrId;

    @ApiModelProperty(value = "管理员姓名")
    private String adminName;

    @ApiModelProperty(value = "控制的设备类型列表", required = true, position = 4)
    private List<DeviceType> controlDeviceTypeList;

    @ApiModelProperty(value = "控制设备范围：ALL：全部,PART：部分", required = true, position = 5)
    private ControlDeviceRange controlDeviceRange;

    @ApiModelProperty(value = "控制部分设备详情")
    private List<CentralizedControlCodeSelectDeviceResp> deviceRespList;

}
