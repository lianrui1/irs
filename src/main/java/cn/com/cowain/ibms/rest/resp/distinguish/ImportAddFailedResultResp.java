package cn.com.cowain.ibms.rest.resp.distinguish;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 批量添加导入结果
 *
 * @author: yanzy
 * @date: 2022/4/8 14:45
 */
@Data
public class ImportAddFailedResultResp {

    @ApiModelProperty(value = "导入数据总个数")
    private int totalCount;

    @ApiModelProperty(value = "导入成功数据个数")
    private int successCount;

    @ApiModelProperty(value = "导入失败数据个数")
    private int errorCount;

    @ApiModelProperty(value = "导入状态")
    private String importStatusResult;

    @ApiModelProperty(value = "错误数据缓存中uuid")
    private String errorDataId;
}
