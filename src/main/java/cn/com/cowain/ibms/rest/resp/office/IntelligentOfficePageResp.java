package cn.com.cowain.ibms.rest.resp.office;

import cn.com.cowain.ibms.enumeration.iot.IntelligentOfficeStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/8 15:53
 */
@Data
@ApiModel("智能办公室响应对象")
public class IntelligentOfficePageResp {

    @ApiModelProperty(value = "ID", required = true)
    private String id;

    @ApiModelProperty(value = "智能办公室名称", required = true)
    private String name;

    @ApiModelProperty(value = "空间ID", required = true)
    private String spaceId;

    @ApiModelProperty(value = "空间编号", required = true)
    private String spaceNumber;

    @ApiModelProperty(value = "空间所在位置")
    private String spaceAddress;

    @ApiModelProperty(value = "使用权所属人", required = true)
    private List<SpaceAdminResp> spaceAdminList;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "创建时间", required = true)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "状态", required = true)
    private IntelligentOfficeStatus status;

    @ApiModelProperty(value = "状态名称", required = true)
    private String statusName;

    @ApiModelProperty(value = "二维码")
    private String qrCodeUrl;

}
