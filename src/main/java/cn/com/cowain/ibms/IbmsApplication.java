package cn.com.cowain.ibms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * IBMS 项目
 *
 * @author Hu Jingling
 * @version 1.0
 * @since 2020-08-03
 */
@EnableAsync //开启异步调用
@SpringBootApplication
@EnableFeignClients(basePackages = {"cn.com.cowain"})
public class IbmsApplication {

    /**
     * 主函数
     *
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(IbmsApplication.class, args);
    }

}
