package cn.com.cowain.ibms.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

import static cn.com.cowain.ibms.utils.IConst.TBL_ROOM_OPEN_HOURS;

/**
 * 房间开放时间段
 *
 * @author 胡荆陵
 * @since 2020-7-30
 */
@Data
@Entity
@Table(name = TBL_ROOM_OPEN_HOURS)
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = TBL_ROOM_OPEN_HOURS, comment = "会议室开放时间")
@Where(clause = "is_del=0")
public class OpeningHours extends BaseEntity {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 从时间
     */
    @Column(name = "from_hour", nullable = false, columnDefinition = "varchar(20) COMMENT '从时间'")
    private String from;

    /**
     * 到时间
     */
    @Column(name = "to_hour", nullable = false, columnDefinition = "varchar(20) COMMENT '到时间'")
    private String to;

    /**
     * 关联Room
     */
    @ManyToOne
    @JoinColumn(name = "room_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK'")
    private Room room;

}
