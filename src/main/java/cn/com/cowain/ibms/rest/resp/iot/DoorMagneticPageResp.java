package cn.com.cowain.ibms.rest.resp.iot;

import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Yang.Lee
 * @date 2021/10/11 9:28
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "门磁列表分页返回对象")
public class DoorMagneticPageResp {

    @ApiModelProperty(value = "设备ID", required = true, example = "1", position = 1)
    private String deviceId;

    @ApiModelProperty(value = "门磁ID", required = true, example = "1", position = 2)
    private String doorMagneticId;

    @ApiModelProperty(value = "设备状态", required = true, example = "ONLINE", position = 3)
    private DeviceStatus status;

    @ApiModelProperty(value = "设备状态名称", required = true, example = "在线", position = 4)
    private String statusName;

    @ApiModelProperty(value = "设备SN", example = "123", position = 5)
    private String sn;

    @ApiModelProperty(value = "门磁编号", required = true, example = "KS00123", position = 6)
    private String number;

    @ApiModelProperty(value = "设备名称", example = "DeviceName", position = 7)
    private String deviceNameZh;

    @ApiModelProperty(value = "所属应用",  example = "IBMS", position = 8)
    private String application;

    @ApiModelProperty(value = "所属项目名称", example = "昆山", position = 9)
    private String projectName;

    @ApiModelProperty(value = "二维码内容", example = "https://a.b.com", position = 10)
    private String qrcodeContent;

    @ApiModelProperty(value = "设备位置", example = "月球", position = 11)
    private String address;

    @ApiModelProperty(value = "控制方式", example = "面板机，二维码", position = 12)
    private String controlType;
}
