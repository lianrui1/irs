package cn.com.cowain.ibms.mq.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author tql
 * @Description EventRequestVo
 * @Date 21-5-10 下午4:19
 * @Version 1.0
 */
@Data
public class EventDataRequestVo {

    /**
     * 人员编号
     */
    @JsonProperty("ExtEventPersonNo")
    @ApiModelProperty(value = "人员编号")
    private String extEventPersonNo;

    /**
     * 卡号
     */
    @JsonProperty("ExtEventCardNo")
    @ApiModelProperty(value = "卡号")
    private String extEventCardNo;

    /**
     * 事件输入通道
     */
    @JsonProperty("ExtEventCaseID")
    @ApiModelProperty(value = "事件输入通道")
    private String extEventCaseID;

    /**
     * 人员身份证信息
     */
    @JsonProperty("ExtEventIdentityCardInfo")
    @ApiModelProperty(value = "人员身份证信息")
    private EventIdentityCardInfoVo extEventIdentityCardInfo;

    /**
     * 人员通道号
     */
    @JsonProperty("ExtAccessChannel")
    @ApiModelProperty(value = "人员通道号")
    private String extAccessChannel;


    /**
     * 报警输入/防区通道
     */
    @JsonProperty("ExtEventAlarmInID")
    @ApiModelProperty(value = "报警输入/防区通道")
    private String extEventAlarmInID;

    /**
     * 报警输出通道
     */
    @JsonProperty("ExtEventAlarmOutID")
    @ApiModelProperty(value = "报警输出通道")
    private String extEventAlarmOutID;


    /**
     * 事件类型代码
     */
    @JsonProperty("ExtEventCode")
    @ApiModelProperty(value = "事件类型代码")
    private String extEventCode;


    /**
     * 通道事件信息
     */
    @JsonProperty("ExtEventCustomerNumInfo")
    @ApiModelProperty(value = "通道事件信息")
    private EventCustomerNumInfoVo extEventCustomerNumInfo;

    /**
     * 门编号
     */
    @JsonProperty("ExtEventDoorID")
    @ApiModelProperty(value = "门编号")
    private String extEventDoorID;

    /**
     * 身份证图片URL
     */
    @JsonProperty("ExtEventIDCardPictureURL")
    @ApiModelProperty(value = "身份证图片URL")
    private String extEventIDCardPictureURL;

    /**
     * 进出方向
     */
    @JsonProperty("ExtEventInOut")
    @ApiModelProperty(value = "进出方向")
    private String extEventInOut;

    /**
     * 就地控制器id
     */
    @JsonProperty("ExtEventLocalControllerID")
    @ApiModelProperty(value = "就地控制器id")
    private String extEventLocalControllerID;

    /**
     * 就地控制器id
     */
    @JsonProperty("ExtEventMainDevID")
    @ApiModelProperty(value = "主设备拨码")
    private String extEventMainDevID;


    /**
     * 图片的url
     */
    @JsonProperty("ExtEventPictureURL")
    @ApiModelProperty(value = "图片的url")
    private String extEventPictureURL;

    /**
     * 读卡器id
     */
    @JsonProperty("ExtEventReaderID")
    @ApiModelProperty(value = "读卡器id")
    private String extEventReaderID;

    /**
     * 读卡器类别
     */
    @JsonProperty("ExtEventReaderKind")
    @ApiModelProperty(value = "读卡器类别")
    private String extEventReaderKind;

    /**
     * 报告上传通道
     */
    @JsonProperty("ExtEventReportChannel")
    @ApiModelProperty(value = "报告上传通道")
    private String extEventReportChannel;


    /**
     * 就地控制器id
     */
    @JsonProperty("ExtEventRoleID")
    @ApiModelProperty(value = "群组编号")
    private String extEventRoleID;

    /**
     * 分控制器硬件ID
     */
    @JsonProperty("ExtEventSubDevID")
    @ApiModelProperty(value = "分控制器硬件ID")
    private String extEventSubDevID;

    /**
     * 刷卡次数
     */
    @JsonProperty("ExtEventSwipNum")
    @ApiModelProperty(value = "刷卡次数")
    private String extEventSwipNum;

    /**
     * 就地控制器id
     */
    @JsonProperty("ExtEventType")
    @ApiModelProperty(value = "事件类型")
    private String extEventType;


    /**
     * 多重认证序号
     */
    @JsonProperty("ExtEventVerifyID")
    @ApiModelProperty(value = "多重认证序号")
    private String extEventVerifyID;


    /**
     * 白名单单号
     */
    @JsonProperty("ExtEventWhiteListNo")
    @ApiModelProperty(value = "白名单单号")
    private String extEventWhiteListNo;


    /**
     * 事件上报驱动的时间
     */
    @JsonProperty("ExtReceiveTime")
    @ApiModelProperty(value = "事件上报驱动的时间")
    private String extReceiveTime;


    /**
     * 事件流水号
     */
    @JsonProperty("Seq")
    @ApiModelProperty(value = "事件流水号")
    private String seq;


    /**
     * 就地控制器id
     */
    @JsonProperty("svrIndexCode")
    @ApiModelProperty(value = "图片服务器唯一编码")
    private String svrIndexCode;
}
