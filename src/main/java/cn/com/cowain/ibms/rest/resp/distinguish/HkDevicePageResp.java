package cn.com.cowain.ibms.rest.resp.distinguish;

import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/3 11:14
 */
@Data
@ApiModel("hk设备响应对象")
public class HkDevicePageResp {

    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "设备状态")
    private DeviceStatus hwStatus;

    @ApiModelProperty(value = "sn")
    private String sn;

    @ApiModelProperty(value = "设备名称")
    private String deviceName;

    @ApiModelProperty(value = "设备位置")
    private String address;

}
