package cn.com.cowain.ibms.websocket.bean;

import cn.com.cowain.ibms.websocket.enumeration.MessageType;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2022/1/6 9:40
 */
@Data
public class MessageResult<T> {

    private int status;

    private String msg;

    private MessageType msgType;

    private T data;

    public static <T> MessageResult<T> ok(String message, T data, MessageType msgType) {
        MessageResult<T> jsonResult = new MessageResult<>();
        jsonResult.setStatus(1);
        jsonResult.setMsg(message);
        jsonResult.setData(data);
        jsonResult.setMsgType(msgType);
        return jsonResult;
    }

    public static <T> MessageResult<T> ok(T data, MessageType msgType) {
        MessageResult<T> jsonResult = new MessageResult<>();
        jsonResult.setStatus(1);
        jsonResult.setMsg("OK");
        jsonResult.setData(data);
        jsonResult.setMsgType(msgType);
        return jsonResult;
    }

    public static <T> MessageResult<T> okNoData(String message, MessageType msgType) {
        MessageResult<T> jsonResult = new MessageResult<>();
        jsonResult.setStatus(1);
        jsonResult.setMsg(message);
        jsonResult.setData(null);
        jsonResult.setMsgType(msgType);
        return jsonResult;
    }

    public static <T> MessageResult<T> error(String message, T data, MessageType msgType) {
        MessageResult<T> jsonResult = new MessageResult<>();
        jsonResult.setStatus(0);
        jsonResult.setMsg(message);
        jsonResult.setData(data);
        jsonResult.setMsgType(msgType);
        return jsonResult;
    }
}
