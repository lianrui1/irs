package cn.com.cowain.ibms.rest.resp.iot;

import cn.com.cowain.ibms.entity.iot.AccessControlSpot;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.enumeration.iot.AccessControlSpotType;
import cn.com.cowain.ibms.enumeration.iot.SpotStatus;
import cn.com.cowain.ibms.service.space.SpaceService;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * @author Yang.Lee
 * @date 2021/11/7 15:41
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("门禁点分页列表响应")
public class AccessControlSpotPageResp {

    @ApiModelProperty(value = "门禁点ID", required = true, position = 1)
    private String id;

    @ApiModelProperty(value = "门禁点名称", required = true, position = 2)
    private String name;

    @ApiModelProperty(value = "门禁点类型", required = true, position = 3)
    private AccessControlSpotType type;

    @ApiModelProperty(value = "门禁点类型名称", required = true, position = 4)
    private String typeName;

    @ApiModelProperty(value = "门禁点地址", required = true, position = 5)
    private String address;

    @ApiModelProperty(value = "设备ID", required = true, position = 6)
    private String deviceId;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "通行开始时间", required = true, position = 7)
    private LocalDateTime startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "通行结束时间", required = true, position = 8)
    private LocalDateTime endTime;

    @ApiModelProperty(value = "设备类型", required = true, position = 9)
    private String deviceType;

    @ApiModelProperty(value = "设备类型名称", required = true, position = 10)
    private String deviceTypeName;

    @ApiModelProperty(value = "空间ID", required = true, position = 11)
    private String spaceId;

    @ApiModelProperty(value = "空间名称", required = true, position = 12)
    private String spaceName;

    @ApiModelProperty(value = "项目ID", required = true, position = 13)
    private String projectId;

    @ApiModelProperty(value = "项目名称", required = true, position = 14)
    private String projectName;

    @ApiModelProperty(value = "进出类型", required = true, position = 15)
    private String inOutName;

    @ApiModelProperty(value = "跳转页面url",required = true, position = 16)
    private String url;

    @ApiModelProperty(value = "门禁状态",required = true, position = 16)
    private SpotStatus spotStatus;


    /**
     * 对象转换
     *
     * @param source 原对象
     * @return 目标对象
     * @author Yang.Lee
     * @date 2021/11/24 10:03
     **/
    public static AccessControlSpotPageResp convert(AccessControlSpot source) {

        String spaceId = "";
        String deviceId = "";
        String deviceType = "";
        String deviceTypeName = "";

        if (null != source.getDevice()) {
            IotDevice device = source.getDevice();
            deviceId = device.getId();
            deviceType = String.valueOf(device.getDeviceType());
            deviceTypeName = device.getDeviceType().getName();
        }
        if (null != source.getSpace()) {
            spaceId = source.getSpace().getId();
        }

        return AccessControlSpotPageResp.builder()
                .id(source.getId())
                .name(source.getName())
                .type(source.getAccessControlSpotType())
                .typeName(Optional.ofNullable(source.getAccessControlSpotType()).map(AccessControlSpotType::getName).orElse(""))
                .address(source.getSpace().getProject().getProjectName() + " / " + SpaceService.getFullSpaceName(source.getSpace(), source.getSpace().getName()) + " / " + source.getAddress())
                .spaceId(spaceId)
                .spaceName(source.getSpace().getName())
                .projectId(source.getSpace().getProject().getId())
                .projectName(source.getSpace().getProject().getProjectName())
                .deviceId(deviceId)
                .deviceType(deviceType)
                .deviceTypeName(deviceTypeName)
                .inOutName(source.getInOut() != null ? source.getInOut().getName() : "")
                .build();
    }

    public static AccessControlSpotPageResp convertUrl(AccessControlSpot source,String environmentAddress) {

        String spaceId = "";
        String deviceId = "";
        String deviceType = "";
        String deviceTypeName = "";
        String url = "access/apply?spotId=";

        if (null != source.getDevice()) {
            IotDevice device = source.getDevice();
            deviceId = device.getId();
            deviceType = String.valueOf(device.getDeviceType());
            deviceTypeName = device.getDeviceType().getName();
        }
        if (null != source.getSpace()) {
            spaceId = source.getSpace().getId();
        }

        return AccessControlSpotPageResp.builder()
                .id(source.getId())
                .name(source.getName())
                .type(source.getAccessControlSpotType())
                .typeName(Optional.ofNullable(source.getAccessControlSpotType()).map(AccessControlSpotType::getName).orElse(""))
                .address(source.getSpace().getProject().getProjectName() + " / " + SpaceService.getFullSpaceName(source.getSpace(), source.getSpace().getName()) + " / " + source.getAddress())
                .spaceId(spaceId)
                .spaceName(source.getSpace().getName())
                .projectId(source.getSpace().getProject().getId())
                .projectName(source.getSpace().getProject().getProjectName())
                .deviceId(deviceId)
                .deviceType(deviceType)
                .deviceTypeName(deviceTypeName)
                .inOutName(source.getInOut() != null ? source.getInOut().getName() : "")
                .url(environmentAddress+url+source.getId())
                .build();
    }
}
