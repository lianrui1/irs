package cn.com.cowain.ibms.service.office;

import cn.com.cowain.ibms.bo.SimpleIntelligentOfficeInitialization;
import cn.com.cowain.ibms.component.IntelligentOfficeRedis;
import cn.com.cowain.ibms.entity.office.IntelligentOffice;
import cn.com.cowain.ibms.entity.office.IntelligentOfficeInitialization;

import java.util.Optional;

public interface IOfficeRedisService {
    //初始化办公室

    Optional<SimpleIntelligentOfficeInitialization> getIOIFromRedis(String hrId, String officeId);

    //将初始化对象放入Redis

    //放入Redis初始化办公室
    Optional<SimpleIntelligentOfficeInitialization> setIOIFromRedis(String hrId, String officeId);

    Optional<SimpleIntelligentOfficeInitialization> setIntelligentOfficeInitRedis(String officeInitKey, String hrId, String officeId);

    //获取办公室初始化对象

    Optional<SimpleIntelligentOfficeInitialization> getIntelligentOfficeInitRedis(String officeInitKey);

    //将办公室取出或放入Redis

    IntelligentOfficeRedis getIntelligentOfficeRedis(String intelligentOffice);

    //将办公室放入Redis
    void setIntelligentOfficeRedis(IntelligentOffice intelligentOffice);

    void delIntelligentOfficeRedis(IntelligentOffice intelligentOffice);

    void delIntelligentOfficeInitRedis(IntelligentOfficeInitialization intelligentOfficeInitialization);
}
