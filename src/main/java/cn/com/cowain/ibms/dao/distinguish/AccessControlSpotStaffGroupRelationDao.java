package cn.com.cowain.ibms.dao.distinguish;

import cn.com.cowain.ibms.entity.distinguish.AccessControlSpotStaffGroupRelation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccessControlSpotStaffGroupRelationDao extends JpaRepository<AccessControlSpotStaffGroupRelation, String>, JpaSpecificationExecutor<AccessControlSpotStaffGroupRelation> {

    // 根据门禁点ID获取人员组
    List<AccessControlSpotStaffGroupRelation> findByAccessControlSpotId(String it);

    // 根据门禁点ID和人员组名称获取人员组
    Optional<AccessControlSpotStaffGroupRelation> findByAccessControlSpotIdAndStaffGroupName(String id, String s);

    // 获取门禁点默认人员组
    List<AccessControlSpotStaffGroupRelation> findByAccessControlSpotIdAndIsDefault(String id, int i);

    Optional<AccessControlSpotStaffGroupRelation> findByAccessControlSpotIdAndStaffGroupId(String id, String id1);

    // 根据人员组ID获取门禁点信息
    List<AccessControlSpotStaffGroupRelation> findByStaffGroupId(String id);


    // 根据人员组ID获取关联门禁点个数
    int countByStaffGroupId(String id);

    // 根据人员ID获取关联的门禁点列表
    Page<AccessControlSpotStaffGroupRelation> findAllByStaffGroupId(String id,Pageable pageable);
}
