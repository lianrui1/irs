package cn.com.cowain.ibms.rest.req.distinguish;

import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * @author Yang.Lee
 * @date 2021/7/14 18:16
 */
@Data
@ApiModel("人员组分页查询请求对象")
public class StaffGroupPageReq extends PageReq {

    @ApiModelProperty(value = "人员组名称")
    private String name;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    @ApiModelProperty(value = "创建时间（开始）")
    private LocalDateTime startTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    @ApiModelProperty(value = "创建时间（结束）")
    private LocalDateTime endTime;
}
