package cn.com.cowain.ibms.rest.resp.audio;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 实时语音翻译结果对象
 * @author Yang.Lee
 * @date 2021/1/14 15:32
 */
@Data
@ApiModel("实时语音转写结果")
public class RTASTResultResp implements Serializable {

    @Min(0)
    @ApiModelProperty(value = "文字对应音频的开始秒数", required = true, example = "0")
    private int start;

    @Min(0)
    @ApiModelProperty(value = "文字对应音频的结束秒数", required = true, example = "10", position = 1)
    private int end;

    @NotBlank
    @ApiModelProperty(value = "语音转写文字结果", required = true, example = "今天晚上不下班", position = 2)
    private String text;

    @Min(0)
    @ApiModelProperty(value = "文本对应语音时长", required = true, example = "今天晚上不下班", position = 3)
    private int audioLength;
}
