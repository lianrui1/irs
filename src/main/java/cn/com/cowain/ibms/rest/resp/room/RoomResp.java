package cn.com.cowain.ibms.rest.resp.room;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author feng
 * @title: RoomResp
 * @projectName ibms
 * @Date 2021/12/28 9:46
 */
@Data
public class RoomResp {
    @ApiModelProperty(value = "会议室id", required = true)
    private String roomId;

    @ApiModelProperty(value = "会议室名字", required = true)
    private String roomName;
    @ApiModelProperty(value = "空间id", required = true)
    private String spaceId;
}
