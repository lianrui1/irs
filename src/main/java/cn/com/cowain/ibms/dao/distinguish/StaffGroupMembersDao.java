package cn.com.cowain.ibms.dao.distinguish;

import cn.com.cowain.ibms.entity.distinguish.StaffGroup;
import cn.com.cowain.ibms.entity.distinguish.StaffGroupMembers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface StaffGroupMembersDao extends JpaRepository<StaffGroupMembers, String>, JpaSpecificationExecutor<StaffGroupMembers> {

    // 根据人员组ID获取人员组成员
    List<StaffGroupMembers> findByGroupId(StaffGroup staff);

    // 根据成员获取人员组
    Set<StaffGroupMembers> findByEmpNo(String personCode);

    /**
     * 根据人员组查询所有员工
     * @param groups 根据人员组查询
     * @return
     * @author Yang.Lee
     * @date 2021/4/9 13:31
     **/
    List<StaffGroupMembers> findByGroupIdIn(StaffGroup... groups);


    List<StaffGroupMembers> findByRealName(String name);

    // 根据人员组ID获取人员组成员
    List<StaffGroupMembers> findByGroupIdId(String id);

    // 根据人员组ID和人员工号获取人员信息
    Optional<StaffGroupMembers> findByGroupIdIdAndEmpNo(String id, String empNo);
}
