package cn.com.cowain.ibms.rest.resp.ability;

import cn.com.cowain.ibms.entity.iot.AccessControlSpot;
import cn.com.cowain.ibms.entity.iot.VideoMonitorSpot;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author feng
 * @title: NodeSit 监控点列表
 * @projectName bims
 * @Date 2021/11/18 11:01
 */
@Data
public class AbilityNodeResp {
    public AbilityNodeResp(){}
    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("设备名称")
    private String name;

    @ApiModelProperty("设备位置")
    private String addres;

    @ApiModelProperty("所属空间ID")
    private String spaceId;

    @ApiModelProperty("设备ID")
    private String deviceId;
    public AbilityNodeResp(VideoMonitorSpot videoMonitorSpot){
        this.id=videoMonitorSpot.getId();
        this.name=videoMonitorSpot.getName();
        this.addres=videoMonitorSpot.getAddress();
        this.spaceId=videoMonitorSpot.getSpace().getId();
        this.deviceId=videoMonitorSpot.getDevice().getId();
    }
    public AbilityNodeResp(AccessControlSpot controlSpot){
        this.id=controlSpot.getId();
        this.name=controlSpot.getName();
        this.addres=controlSpot.getAddress();
        this.spaceId=controlSpot.getSpace().getId();


    }


}
