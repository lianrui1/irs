package cn.com.cowain.ibms.service.bean.amap.weather;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author feng
 * @title: ForcecastsResult
 * @projectName ibms
 * @Date 2022/5/5 18:33
 */
@Data
public class ForcecastsResult {

    String date;
    String week;
    String dayweather;
    String nightweather;
    String daytemp;
    String nighttemp;
    String daywind;
    String nightwind;
    String daypower;
    String nightpower;
    @ApiModelProperty(value = "天气图标", position = 3)
    private WeatherPicture weatherPicture;


}
