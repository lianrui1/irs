package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.AccessControlSpotApprover;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccessControlSpotApproverDao extends JpaRepository<AccessControlSpotApprover, String>, JpaSpecificationExecutor<AccessControlSpotApprover> {

    // 根据工号获取审批人
    Optional<AccessControlSpotApprover> findByUserHrId(String empNo);

    /**
     * 查询userHrId in {@code userHrIdList}的AccessControlSpotApprover列表
     *
     * @param userHrIdList
     * @return
     */
    List<AccessControlSpotApprover> findAllByUserHrIdIn(List<String> userHrIdList);
}
