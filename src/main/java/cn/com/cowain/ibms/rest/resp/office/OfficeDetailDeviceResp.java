package cn.com.cowain.ibms.rest.resp.office;

import cn.com.cowain.ibms.rest.resp.device.OfficeCommonDeviceResp;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/10 14:37
 */
@Data
@ApiModel("办公室详情返回数据")
public class OfficeDetailDeviceResp {
    @ApiModelProperty(value = "当前办公室", required = true)
    OccupyOfficeResp nowOffice;
    @ApiModelProperty(value = "设备信息", required = true)
    List<OfficeCommonDeviceResp> officeCommonDeviceResp=new ArrayList<>();

}