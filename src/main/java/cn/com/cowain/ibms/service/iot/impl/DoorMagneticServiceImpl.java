package cn.com.cowain.ibms.service.iot.impl;

import cn.com.cowain.ibms.dao.ReservationRecordDao;
import cn.com.cowain.ibms.dao.RoomDao;
import cn.com.cowain.ibms.dao.distinguish.DoorMagneticAccessRecordDao;
import cn.com.cowain.ibms.dao.distinguish.HkDeviceDao;
import cn.com.cowain.ibms.dao.iot.*;
import cn.com.cowain.ibms.dao.iotc.DeviceApplicationRelationDao;
import cn.com.cowain.ibms.entity.*;
import cn.com.cowain.ibms.entity.distinguish.DoorMagneticAccessRecord;
import cn.com.cowain.ibms.entity.iot.*;
import cn.com.cowain.ibms.entity.iotc.Application;
import cn.com.cowain.ibms.entity.iotc.DeviceApplicationRelation;
import cn.com.cowain.ibms.entity.space.Project;
import cn.com.cowain.ibms.enumeration.ApprovalStatus;
import cn.com.cowain.ibms.enumeration.PermissionApplicationStatus;
import cn.com.cowain.ibms.enumeration.ReservationRecordStatus;
import cn.com.cowain.ibms.enumeration.iot.*;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.enumeration.meeting.DoorMagneticAccessResult;
import cn.com.cowain.ibms.enumeration.meeting.UserType;
import cn.com.cowain.ibms.feign.bean.pass_v2.PassV2UserResp;
import cn.com.cowain.ibms.rest.bean.During;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.iot.DoorMagneticPageReq;
import cn.com.cowain.ibms.rest.resp.BaseListResp;
import cn.com.cowain.ibms.rest.resp.StaffResp;
import cn.com.cowain.ibms.rest.resp.iot.*;
import cn.com.cowain.ibms.rest.resp.iot.doorMagnetic.DoorMagneticStatusResp;
import cn.com.cowain.ibms.service.MessageSendService;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.*;
import cn.com.cowain.ibms.service.bean.hwCloud.DeviceDetail;
import cn.com.cowain.ibms.service.device.HwProductsService;
import cn.com.cowain.ibms.service.guest.GuestService;
import cn.com.cowain.ibms.service.iot.*;
import cn.com.cowain.ibms.service.qrcode.QrCodeService;
import cn.com.cowain.ibms.service.space.SpaceService;
import cn.com.cowain.ibms.service.task.TaskCenterTaskService;
import cn.com.cowain.ibms.utils.*;
import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.annotation.Nullable;
import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 门磁Service实现类
 *
 * @author Yang.Lee
 * @date 2021/4/19 11:04
 */
@Slf4j
@Service
public class DoorMagneticServiceImpl implements DoorMagneticService {

    @Resource
    private DoorMagneticDao doorMagneticDao;

    @Resource(name = "doorMagneticHWService")
    private DoorMagneticControlService doorMagneticHWService;

    @Resource(name = "doorMagneticMQTTService")
    private DoorMagneticControlService doorMagneticMQTTService;

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserService;

    @Resource
    private DoorMagneticAccessRecordDao doorMagneticAccessRecordDao;

    @Resource
    private DoorMagneticHkDeviceRelationDao doorMagneticHkDeviceRelationDao;

    @Value("${cowain.doorplate.qrcode.url}")
    private String qrcodeUrl;

    @Resource
    private HkDeviceDao hkDeviceDao;

    @Resource
    private DeviceApplicationRelationDao deviceApplicationRelationDao;

    @Resource
    private AccessControlSpotAccessRecordService accessControlSpotAccessRecordService;

    @Autowired
    private HwProductsService hwProductsService;

    @Resource
    private DeviceAdminDao deviceAdminDao;

    @Value("${cowain.device.admins}")
    private String defaultDeviceAdminList;

    @Resource
    private MessageSendService messageSendService;

    @Value("${cowain.send.message.vx.msgTemplateId}")
    private String vxMsgTemplateId;

    @Resource
    private AccessControlSpotUserDao accessControlSpotUserDao;

    @Resource
    private GuestService guestService;

    @Resource
    private DeviceToSpaceDao deviceToSpaceDao;

    @Resource
    private RoomDao roomDao;

    @Resource
    private ReservationRecordDao reservationRecordDao;

    @Resource
    private DoorPlateService doorPlateService;

    @Resource
    private QrCodeService qrCodeService;

    @Resource
    private AccessControlSpotApproverRelationDao accessControlSpotApproverRelationDao;

    @Resource
    private AccessControlSpotApplicationRecordService accessControlSpotApplicationRecordService;

    @Resource
    private AccessControlSpotDao accessControlSpotDao;

    @Resource
    private AccessControlSpotApplicationRecordDao accessControlSpotApplicationRecordDao;

    @Resource
    private TaskCenterTaskService taskCenterTaskService;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private IotDeviceCacheService iotDeviceCacheService;

    @Autowired
    private AccessControlTimePlanService accessControlTimePlanService;

    /**
     * 获取门磁设备列表
     *
     * @param projectId 项目ID
     * @param spaceId   空间ID
     * @return 门磁列表
     * @author Yang.Lee
     * @date 2021/4/19 11:03
     **/
    @Override
    @Transactional
    public List<BaseListResp> getList(@Nullable String projectId, @Nullable String spaceId) {

        List<DoorMagnetic> doorMagneticList = doorMagneticDao.findAll((Specification<DoorMagnetic>) (root, criteriaQuery, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();
            // 根据项目查
            if (StringUtils.isNotEmpty(projectId)) {
                list.add(criteriaBuilder.equal(root.get("device").get("project").get("id"), projectId));
            }
            // 根据空间查
            if (StringUtils.isNotEmpty(spaceId)) {
                list.add(criteriaBuilder.equal(root.get("device").get("space").get("id"), spaceId));
            }

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        });

        // 输出对象转换
        List<BaseListResp> result = new ArrayList<>();
        if (!doorMagneticList.isEmpty()) {
            doorMagneticList.forEach(obj -> result.add(new BaseListResp(obj.getId(), obj.getName())));
        }

        return result;
    }

    /**
     * 门磁开门，优先使用华为云方式开门
     *
     * @param magneticNum 门磁编号
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/5/6 9:38
     **/
    @Override
    public ServiceResult open(String magneticNum, String hrId) {

        Optional<DoorMagnetic> doorMagneticOp = doorMagneticDao.findByNumber(magneticNum);
        if (!doorMagneticOp.isPresent()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUNT_DOOR_MAGNETISM, magneticNum));
        }

        DoorMagnetic doorMagnetic = doorMagneticOp.get();

        // 保存设备操作日志
        // 生成请求参数
        DoorMagneticHWReq doorMagneticHWReq = new DoorMagneticHWReq();
        doorMagneticHWReq.setDeviceId(doorMagnetic.getDevice().getId());
        doorMagneticHWReq.setDevice_id(doorMagnetic.getHuaweiId());
        DoorMagneticHWParasReq doorMagneticHWParasReq = new DoorMagneticHWParasReq();
        doorMagneticHWParasReq.setAction("open");
        doorMagneticHWReq.setParas(doorMagneticHWParasReq);
        String name = "";
        try {
            // 保存人员权限
            name = sysUserService.getUserByEmpNo(hrId).getNameZh();
        } catch (Exception e) {
            log.error(e.getMessage());
            log.error("获取人员信息失败");
        }
        hwProductsService.saveDeviceControlLog(doorMagneticHWReq, hrId, name);

        String hwId = doorMagnetic.getHuaweiId();
        if (hwId != null && hwId.trim().length() > 0) {
            // 发送华为云开门请求
            return doorMagneticHWService.open(magneticNum);
        }

        String macAddress = doorMagnetic.getDoorMagneticMac();
        if (macAddress != null && macAddress.trim().length() > 0) {
            // 发送MQTT-CONNECTOR开门请求
            return doorMagneticMQTTService.open(magneticNum);
        }

        log.warn("门磁开门未调用任何实现方式，门磁编号：{}", magneticNum);

        return ServiceResult.ok();
    }

    /**
     * 保存通行记录
     *
     * @param empNo          工号
     * @param doorMagneticId 门磁ID
     * @param userType       用户类型
     * @param accessResult   通行结果
     * @return
     * @author Yang.Lee
     * @date 2021/8/17 10:32
     **/
    @Override
    @Transactional
    public ServiceResult saveAccessRecord(String empNo, String doorMagneticId, UserType userType, DoorMagneticAccessResult accessResult) {

        Optional<DoorMagnetic> doorMagneticOptional = doorMagneticDao.findById(doorMagneticId);
        if (doorMagneticOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUNT_DOOR_MAGNETISM, doorMagneticId));
        }

        DoorMagneticAccessRecord doorMagneticAccessRecord = new DoorMagneticAccessRecord();
        doorMagneticAccessRecord.setEmpNo(empNo);
        doorMagneticAccessRecord.setAccessState(accessResult);
        doorMagneticAccessRecord.setDataTime(LocalDateTime.now());
        doorMagneticAccessRecord.setDoorMagnetic(doorMagneticOptional.get());

        String name = "";
        String department = "";
        if (userType.equals(UserType.STAFF)) {
            SysUser sysUser = sysUserService.getUserByEmpNo(empNo);
            if (sysUser==null) {
                return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_STAFF_UC, empNo));
            }
            name = sysUser.getNameZh();
            department = sysUser.getFullDepartmentName();
            doorMagneticAccessRecord.setNameZH(sysUser.getNameZh());
        } else {
            name = "陌生人";
            department = "--";
            empNo = "--";
        }

        doorMagneticAccessRecordDao.save(doorMagneticAccessRecord);

        // 将数据写入设备通行权限表。 since V1.20
        DeviceAccessRecord deviceAccessRecord = new DeviceAccessRecord();
        deviceAccessRecord.setRecordType(DeviceAccessRecordType.DOOR_MAGNETIC);
        deviceAccessRecord.setAccessStatus(accessResult.getName());
        deviceAccessRecord.setDataTime(doorMagneticAccessRecord.getDataTime());
        deviceAccessRecord.setIotDevice(doorMagneticOptional.get().getDevice());
        deviceAccessRecord.setNameZh(name);
        deviceAccessRecord.setEmpNo(empNo);
        deviceAccessRecord.setStaffType(userType.equals(UserType.STAFF) ? PersonType.EMPLOYEE : PersonType.VISITOR);
        deviceAccessRecord.setDepartment(department);
        accessControlSpotAccessRecordService.create(deviceAccessRecord);

        return ServiceResult.ok();
    }

    /**
     * 保存访客通行记录
     *
     * @param mobile         手机号
     * @param doorMagneticId 门磁ID
     * @param accessResult   通行结果
     * @return
     * @author Yang.Lee
     * @date 2021/8/31 14:01
     **/
    @Override
    @Transactional
    public ServiceResult saveGuestAccessRecord(String mobile, String doorMagneticId, DoorMagneticAccessResult accessResult) {
        Optional<DoorMagnetic> doorMagneticOptional = doorMagneticDao.findById(doorMagneticId);
        if (doorMagneticOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUNT_DOOR_MAGNETISM, doorMagneticId));
        }

        DoorMagneticAccessRecord doorMagneticAccessRecord = new DoorMagneticAccessRecord();
        doorMagneticAccessRecord.setAccessState(accessResult);
        doorMagneticAccessRecord.setDataTime(LocalDateTime.now());
        doorMagneticAccessRecord.setDoorMagnetic(doorMagneticOptional.get());
        doorMagneticAccessRecord.setNameZH(mobile);
        doorMagneticAccessRecord.setUserType(UserType.GUEST);

        doorMagneticAccessRecordDao.save(doorMagneticAccessRecord);

        // 将数据写入设备通行权限表。 since V1.20
        DeviceAccessRecord deviceAccessRecord = new DeviceAccessRecord();
        deviceAccessRecord.setRecordType(DeviceAccessRecordType.DOOR_MAGNETIC);
        deviceAccessRecord.setAccessStatus(accessResult.getName());
        deviceAccessRecord.setDataTime(doorMagneticAccessRecord.getDataTime());
        deviceAccessRecord.setIotDevice(doorMagneticOptional.get().getDevice());
        deviceAccessRecord.setNameZh(mobile);
        deviceAccessRecord.setStaffType(PersonType.VISITOR);
        deviceAccessRecord.setDepartment("--");
        deviceAccessRecord.setEmpNo("--");

        accessControlSpotAccessRecordService.create(deviceAccessRecord);

        return ServiceResult.ok();
    }

    /**
     * 保存访客通行记录
     *
     * @param hrId           访客工号
     * @param name           访客姓名
     * @param doorMagneticId 门磁ID
     * @param accessResult   通行结果
     * @return
     * @author Yang.Lee
     * @date 2021/8/31 14:01
     **/
    @Override
    public ServiceResult saveGuestAccessRecord(String hrId, String name, String doorMagneticId, DoorMagneticAccessResult accessResult) {
        Optional<DoorMagnetic> doorMagneticOptional = doorMagneticDao.findById(doorMagneticId);
        if (doorMagneticOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUNT_DOOR_MAGNETISM, doorMagneticId));
        }

        DoorMagneticAccessRecord doorMagneticAccessRecord = new DoorMagneticAccessRecord();
        doorMagneticAccessRecord.setAccessState(accessResult);
        doorMagneticAccessRecord.setDataTime(LocalDateTime.now());
        doorMagneticAccessRecord.setDoorMagnetic(doorMagneticOptional.get());
        doorMagneticAccessRecord.setNameZH(name);
        doorMagneticAccessRecord.setUserType(UserType.GUEST);

        doorMagneticAccessRecordDao.save(doorMagneticAccessRecord);

        // 将数据写入设备通行权限表。 since V1.20
        DeviceAccessRecord deviceAccessRecord = new DeviceAccessRecord();
        deviceAccessRecord.setRecordType(DeviceAccessRecordType.DOOR_MAGNETIC);
        deviceAccessRecord.setAccessStatus(accessResult.getName());
        deviceAccessRecord.setDataTime(doorMagneticAccessRecord.getDataTime());
        deviceAccessRecord.setIotDevice(doorMagneticOptional.get().getDevice());
        deviceAccessRecord.setNameZh(name);
        deviceAccessRecord.setStaffType(PersonType.VISITOR);
        deviceAccessRecord.setDepartment("--");
        deviceAccessRecord.setEmpNo(hrId);

        accessControlSpotAccessRecordService.create(deviceAccessRecord);

        return ServiceResult.ok();
    }

    /**
     * 查询门磁列表（分页）
     *
     * @param req 查询参数
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/10/15 14:15
     **/
    @Override
    @Transactional
    public PageBean<DoorMagneticPageResp> getPage(DoorMagneticPageReq req) {

        Page<DoorMagnetic> page = doorMagneticDao.findAll((root, query, criteriaBuilder) -> {

            List<Predicate> listAnd = new ArrayList<>();

            if (StringUtils.isNotBlank(req.getSpaceId())) {
                listAnd.add(criteriaBuilder.equal(root.get("device").get("space").get("id"), req.getSpaceId()));
            }

            if (StringUtils.isNotBlank(req.getApplication())) {
                List<DeviceApplicationRelation> deviceApplicationRelationList = deviceApplicationRelationDao.findByApplicationId(req.getApplication());

                if (!deviceApplicationRelationList.isEmpty()) {
                    CriteriaBuilder.In<String> inInitiator = criteriaBuilder.in(root.get("device").get("id"));
                    deviceApplicationRelationList.forEach(obj -> inInitiator.value(obj.getDevice().getId()));
                    listAnd.add(criteriaBuilder.and(inInitiator));
                }
            }

            if (req.getStatus() != null) {

                if (req.getStatus().equals(DeviceStatus.UNKNOW)) {
                    listAnd.add(criteriaBuilder.or(criteriaBuilder.isNull(root.get("device").get("hwStatus")), criteriaBuilder.equal(root.get("device").get("hwStatus"), req.getStatus())));
                } else {
                    listAnd.add(criteriaBuilder.equal(root.get("device").get("hwStatus"), req.getStatus()));

                }
            }

            if (StringUtils.isNotBlank(req.getProjectId())) {
                listAnd.add(criteriaBuilder.equal(root.get("device").get("space").get("project").get("id"), req.getProjectId()));
            }

            if (StringUtils.isNotBlank(req.getKeyword())) {
                String like = "%" + req.getKeyword() + "%";
                listAnd.add(criteriaBuilder.and(criteriaBuilder.or(criteriaBuilder.like(root.get("device").get("sn"), like), criteriaBuilder.like(root.get("device").get("deviceName"), like))));
            }

            return criteriaBuilder.and(listAnd.toArray(new Predicate[0]));
        }, PageRequest.of(req.getPage(), req.getSize(), Sort.by(req.getOrders())));


        PageBean<DoorMagneticPageResp> pageRespPageBean = new PageBean<>();
        pageRespPageBean.setPage(page.getPageable().getPageNumber());
        pageRespPageBean.setSize(page.getPageable().getPageSize());
        pageRespPageBean.setTotalPages(page.getTotalPages());
        pageRespPageBean.setTotalElements(page.getTotalElements());
        pageRespPageBean.setList(page.getContent().stream().map(obj -> {

            List<DeviceApplicationRelation> relationList = deviceApplicationRelationDao.findByDeviceId(obj.getDevice().getId());

            List<DoorMagneticHKDeviceRelation> relationList1 = doorMagneticHkDeviceRelationDao.findByDoorMagneticId(obj.getId());

            List<String> controlType = new ArrayList<>();
            controlType.add("二维码");
            if (!relationList1.isEmpty()) {
                controlType.add("面板机");
            }

            return DoorMagneticPageResp.builder().doorMagneticId(obj.getId()).deviceId(obj.getDevice().getId()).application(relationList.stream().map(DeviceApplicationRelation::getApplication).map(Application::getName).collect(Collectors.joining(","))).deviceNameZh(obj.getDevice().getDeviceName()).number(obj.getNumber()).sn(obj.getDevice().getSn()).projectName(Optional.ofNullable(obj.getDevice().getProject()).map(Project::getProjectName).orElse("")).status(obj.getDevice().getHwStatus()).address(Optional.ofNullable(obj.getDevice().getAddress()).orElse("")).controlType(controlType.stream().map(String::valueOf).collect(Collectors.joining(","))).qrcodeContent(new StringBuffer(qrcodeUrl).append("?dm=").append(obj.getNumber()).toString()).build();
        }).collect(Collectors.toList()));

        return pageRespPageBean;
    }

    /**
     * 获取门磁详情
     *
     * @param id 门磁ID
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/10/19 13:53
     **/
    @Override
    @Transactional
    public DoorMagneticDetailResp get(String id) {

        Optional<DoorMagnetic> doorMagneticOptional = doorMagneticDao.findById(id);
        if (doorMagneticOptional.isEmpty()) {
            return DoorMagneticDetailResp.builder().build();
        }
        DoorMagnetic doorMagnetic = doorMagneticOptional.get();

        // 查询与门磁关联的面板机列表
        List<DoorMagneticHKDeviceRelation> doorMagneticHKDeviceRelationList = doorMagneticHkDeviceRelationDao.findByDoorMagneticId(id);

        List<IotDeviceResp> hkDeviceList = doorMagneticHKDeviceRelationList.stream().map(DoorMagneticHKDeviceRelation::getHkDevice).map(HkDevice::getDevice).map(IotDeviceResp::convert).collect(Collectors.toList());

        return DoorMagneticDetailResp.builder().magneticId(doorMagnetic.getId()).deviceInfo(IotDeviceResp.convert(doorMagnetic.getDevice())).number(doorMagnetic.getNumber()).qrcodeContent(new StringBuffer(qrcodeUrl).append("?dm=").append(doorMagnetic.getNumber()).toString()).relatedHkDeviceList(hkDeviceList).build();
    }

    /**
     * @param id
     * @param req
     * @return
     * @author Yang.Lee
     * @date 2021/10/19 18:15
     **/
    @Override
    @Transactional
    public ServiceResult update(String id, DoorMagneticEditReq req) {

        Optional<DoorMagnetic> doorMagneticOptional = doorMagneticDao.findById(id);
        if (doorMagneticOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUNT_DOOR_MAGNETISM, id));
        }

        DoorMagnetic doorMagnetic = doorMagneticOptional.get();

        // 查询门磁与面板机关系并修改
        List<DoorMagneticHKDeviceRelation> relationList = doorMagneticHkDeviceRelationDao.findByDoorMagneticId(id);

        Set<String> dbDeviceIdSet = relationList.stream().map(DoorMagneticHKDeviceRelation::getHkDevice).map(HkDevice::getDevice).map(IotDevice::getId).collect(Collectors.toSet());

        Set<String> addSet = new HashSet<>(req.getDeviceIdList());      // 需要添加的数据集合
        addSet.removeAll(dbDeviceIdSet);

        Set<String> delSet = new HashSet<>(dbDeviceIdSet);              // 需要删除的数据集合
        delSet.removeAll(req.getDeviceIdList());

        relationList.forEach(obj -> {
            if (delSet.contains(obj.getHkDevice().getDevice().getId())) {
                obj.setIsDelete(1);
            }
        });

        doorMagneticHkDeviceRelationDao.saveAll(relationList);

        Set<DoorMagneticHKDeviceRelation> addRelationSet = new HashSet<>();
        for (String deviceId : addSet) {

            Optional<HkDevice> deviceOptional = hkDeviceDao.findByDeviceId(deviceId);
            if (deviceOptional.isEmpty()) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_DEVICE, deviceId), ErrConst.E01);
            }

            DoorMagneticHKDeviceRelation relation = new DoorMagneticHKDeviceRelation();
            relation.setDoorMagnetic(doorMagnetic);
            relation.setHkDevice(deviceOptional.get());
            addRelationSet.add(relation);
        }

        doorMagneticHkDeviceRelationDao.saveAll(addRelationSet);

        return ServiceResult.ok();
    }

    /**
     * 根据门磁编号查询设备状态
     *
     * @param dm 门磁编号
     * @return 设备状态, 若查询不到设备或设备状态，则返回Optional.empty()
     * @author Yang.Lee
     * @date 2022/7/13 13:41
     **/
    @Override
    public Optional<DoorMagneticStatusResp> getStatus(String dm) {

        Optional<DoorMagnetic> doorMagneticOptional = doorMagneticDao.findByNumber(dm);
        if (doorMagneticOptional.isEmpty()) {
            return Optional.empty();
        }

        DoorMagnetic doorMagnetic = doorMagneticOptional.get();

        DeviceStatus status;
        // 判断设备是在华为云上还是在南通机房
        if (StringUtils.isNotBlank(doorMagnetic.getDoorMagneticMac())) {
            // 查询南通mqtt
            status = getStatusFromNTMqtt(doorMagnetic.getDoorMagneticMac());
        } else {
            // 查询华为云

            status = getStatusFromHW(doorMagnetic.getHuaweiId());
        }

        Optional<AccessControlSpot> accessControlSpotOptional = accessControlSpotDao.findByDeviceId(doorMagnetic.getDevice().getId());
        String address = "";
        if (accessControlSpotOptional.isPresent()) {
            AccessControlSpot accessControlSpot = accessControlSpotOptional.get();
            address = accessControlSpot.getSpace().getProject().getProjectName() + " / " + SpaceService.getFullSpaceName(accessControlSpot.getSpace(), accessControlSpot.getSpace().getName()) + " / " + accessControlSpot.getAddress();
        }

        return Optional.of(DoorMagneticStatusResp.builder().status(status).deviceId(doorMagnetic.getDevice().getId()).deviceName(doorMagnetic.getDevice().getDeviceName()).sn(doorMagnetic.getDevice().getSn()).address(address).build());
    }

    /**
     * 推送异常消息给管理员
     *
     * @param dm  门磁编号
     * @param uid 上报人ucId
     * @return 消息发送结果
     * @author Yang.Lee
     * @date 2022/7/14 15:48
     **/
    @Override
    public ServiceResult sendErrorMessageToAdmin(String dm, String uid) {

        Optional<DoorMagnetic> doorMagneticOptional = doorMagneticDao.findByNumber(dm);

        if (doorMagneticOptional.isEmpty()) {
            log.error("未能根据门磁编号查找到设备信息，门磁dm: {}", dm);
            return ServiceResult.error(ServiceMessage.NOT_FOUNT_DOOR_MAGNETISM, ErrConst.E07);
        }
        DoorMagnetic doorMagnetic = doorMagneticOptional.get();

        // 查询该设备的管理员
        List<DeviceAdmin> adminList = deviceAdminDao.findByDeviceId(doorMagnetic.getDevice().getId());
        List<String> adminHrId;
        if (adminList.isEmpty()) {
            // 如果该设备没有配置管理员，发给默认管理员
            adminHrId = Arrays.asList(defaultDeviceAdminList.split(","));
        } else {
            adminHrId = adminList.stream().map(DeviceAdmin::getAdminHrId).collect(Collectors.toList());
        }

        // 获取上报用户信息
        Optional<PassV2UserResp> userRespOptional = sysUserService.getUserByUid(uid);

        String reporter;
        if (userRespOptional.isEmpty()) {
            reporter = uid;
        } else {
            PassV2UserResp passV2UserResp = userRespOptional.get();
            reporter = passV2UserResp.getName() + "/" + passV2UserResp.getHrId();
        }


        Optional<AccessControlSpot> accessControlSpotOptional = accessControlSpotDao.findByDeviceId(doorMagnetic.getDevice().getId());
        String address = "";
        if (accessControlSpotOptional.isPresent()) {
            AccessControlSpot accessControlSpot = accessControlSpotOptional.get();
            address = accessControlSpot.getSpace().getProject().getProjectName() + " / " + SpaceService.getFullSpaceName(accessControlSpot.getSpace(), accessControlSpot.getSpace().getName()) + " / " + accessControlSpot.getAddress();
        }

        // 发送消息
        String finalReporter = reporter;
        String finalAddress = address;
        adminHrId.forEach(obj -> {
            SysUser user = sysUserService.getUserByEmpNo(obj);
            MessagePoolRecord messagePoolRecord = new MessagePoolRecord();

            messagePoolRecord.setMsgType(3);
            messagePoolRecord.setPriority(0);
            messagePoolRecord.setSendType(0);
            messagePoolRecord.setReceiveType(501);
            messagePoolRecord.setMsgTitle("门磁扫码故障通知");

            MessageBody messageBody = new MessageBody();
            messageBody.setTitle("你好，" + finalReporter + "给你提报了门磁扫码开门故障，请及时联系并处理！");
            messageBody.setType("扫码开门故障");
            messageBody.setDepart("待处理");
            messageBody.setTime(DateUtils.formatLocalDateTime(LocalDateTime.now(), DateUtils.PATTERN_DATETIME));
            messageBody.setRemark("设备位置：" + finalAddress);
            messagePoolRecord.setMsgContent(JSON.toJSONString(messageBody));

            messagePoolRecord.setReceivePerson(user.getWxOpenId());
            messagePoolRecord.setReceivePersonId(user.getWxOpenId());
            messagePoolRecord.setMsgDetailsUrl("");
            messagePoolRecord.setMsgTemplateId(vxMsgTemplateId);
            messagePoolRecord.setTipType(1);

            messageSendService.send(messagePoolRecord);
        });

        return ServiceResult.ok();
    }

    /**
     * 门磁权限审核(静态权限)
     *
     * @param dm  门磁编号
     * @param uid 上报人ucId
     * @return 检查结果，若用户有权限则ServiceResult.getObject()返回true,无权限返回false
     * @author Yang.Lee
     * @date 2022/7/15 14:04
     **/
    @Override
    @Transactional
    public ServiceResult checkStaticPermission(String dm, String uid) {
        log.info("进入扫码开门，人员uuid:{}",uid);

        Optional<DoorMagnetic> doorMagneticOptional = iotDeviceCacheService.getDoorMagneticToRedis(dm);
        if (doorMagneticOptional.isEmpty()) {
            log.error("未能根据门磁编号查找到设备信息，门磁dm: {}", dm);
            return ServiceResult.error(ServiceMessage.NOT_FOUNT_DOOR_MAGNETISM, ErrConst.E07);
        }
        DoorMagnetic doorMagnetic = doorMagneticOptional.get();
        // 获取用户信息
        Optional<PassV2UserResp> userRespOptional = sysUserService.getUserByUid(uid);
        if (userRespOptional.isEmpty()) {
            return ServiceResult.error("未找到用户信息", ErrConst.E01);
        }

        PassV2UserResp user = userRespOptional.get();

        String hrId;
        // 获取用户工号
        if (PersonType.VISITOR.equals(user.getPersonType())) {
            // 访客工号通过手机号获取

            Optional<Guest> guestOptional = guestService.getByMobile(user.getPhone());
            if (guestOptional.isEmpty()) {
                return ServiceResult.error("未找到用户信息", ErrConst.E01);
            }

            hrId = guestOptional.get().getHrId();
        } else {
            hrId = user.getHrId();
        }
        log.info("员工：{}",user.getPersonType());
        log.info("员工类型：{}",user.getPersonType());
        Optional<AccessControlSpot> accessControlSpotOptional = accessControlSpotDao.findByDevice(doorMagnetic.getDevice());
        AccessControlSpot accessControlSpot;
        if (accessControlSpotOptional.isPresent()) {
            accessControlSpot = accessControlSpotOptional.get();
            if (accessControlSpot.getAuthType() != null && AuthType.ALL_PASS.equals(accessControlSpot.getAuthType())) {
                if (PersonType.EMPLOYEE.equals(user.getPersonType())) {
                    return ServiceResult.ok(true);
                } else {
                    if (sysUserService.isLabour(hrId)) {
                        log.info("人员{}，门磁dm: 【{}】开门成功", hrId,dm);
                        return ServiceResult.ok(true);
                    }
                }
            }
            else if (accessControlSpot.getAuthType() != null && AuthType.STAFF_PASS.equals(accessControlSpot.getAuthType())&&PersonType.EMPLOYEE.equals(user.getPersonType())) {
                log.info("人员{}，门磁dm: 【{}】开门成功", hrId,dm);
                return ServiceResult.ok(true);
            }
        } else {
            log.info("未能根据门磁ID查找到门禁点，门磁dm: {}", dm);
            return ServiceResult.ok(false);
        }

        // 获取权限信息
        //List<AccessControlSpotUser> accessControlSpotUserList = accessControlSpotUserDao.findByAccessControlSpotDeviceIdAndUserHrId(doorMagnetic.getDevice().getId(), hrId);
        List<AccessControlSpotUser> accessControlSpotUserList = accessControlSpotUserDao.findAccessControlSpotUserByUserHrIdAndAccessControlSpot(hrId,accessControlSpot);

        log.info("人员{}，门磁dm: 【{}】需要校验权限开门", hrId,dm);
        if (accessControlSpotUserList.isEmpty()) {
            return ServiceResult.error("用户无权限操作", ErrConst.E401);
        }

        LocalDateTime now = LocalDateTime.now();

        int flag = 0;  // 是否有权限的标记 0 无权限
        for (AccessControlSpotUser spotUser : accessControlSpotUserList) {
            if (now.isAfter(spotUser.getAccessStartTime()) && now.isBefore(spotUser.getAccessEndTime())) {
                flag = 1;
                break;
            }
        }

        if (flag == 1) {
            // 校验用户当前时间是否有权限
            boolean isCurrentTimeOk =  checkTimePlan(hrId, accessControlSpot, now);
            if(isCurrentTimeOk) {
                log.info("人员{}，门磁dm: 【{}】校验权限开门成功！", hrId,dm);
                return ServiceResult.ok(true);
            }else {
                log.info("人员{}，当前时间无权限开启门磁{}！", hrId,dm);
                ServiceResult serviceResult = ServiceResult.ok(false);
                serviceResult.setErrorCode(PermissionApplicationStatus.CURRENT_TIME_NO_PERMISSION.name());
                return serviceResult;
            }
        } else {
            log.info("人员{}，无权限开启门磁{}！", hrId,dm);
            return ServiceResult.ok(false);
        }
    }

    /**
     * 校验时间计划，如果用户在当前时段无法通过门禁点返回false，否则返回true
     * @param hrId
     * @param accessControlSpot
     * @param now
     * @return
     */
    private boolean checkTimePlan(String hrId, AccessControlSpot accessControlSpot, LocalDateTime now) {
        AccessControlTimePlanResp accessControlTimePlanResp = accessControlTimePlanService.getAccessControlTimePlan(accessControlSpot.getId(), hrId);
        if(Objects.isNull(accessControlTimePlanResp)) {
            // 当前用户在门禁点未绑定时间计划，默认全天都可通行
            return true;
        }
        // 获取当前时间，HH:mm格式
        String time =  DateUtils.formatLocalDateTime(now, DateUtils.PATTERN_TIME1);
        if(CollUtil.isNotEmpty(accessControlTimePlanResp.getWorkDays())){
            // 当前星期几
            int week = now.getDayOfWeek().getValue();
            MultiValueMap<Integer, During> weekToDuringMap = new LinkedMultiValueMap<>();
            accessControlTimePlanResp.getWorkDays().forEach(workDay -> weekToDuringMap.addAll(workDay.getWeek(), workDay.getDurings()));
            List<During> durings = weekToDuringMap.get(week);
            if(CollUtil.isEmpty(durings)){
                return false;
            }
            for (During during : durings) {
                if(isInDuring(time, during)){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 判断HH:mm格式的时间是否在一段时间区间内
     *
     * @param time
     * @param during
     * @return
     */
    private boolean isInDuring(String time, During during) {
        String timePrefix = "2022-10-01 ";
        LocalDateTime localDateTime = DateUtils.parseLocalDateTime(timePrefix + time, DateUtils.PATTERN_DATETIME1);
        if(localDateTime.isBefore(DateUtils.parseLocalDateTime(timePrefix + during.getFrom(), DateUtils.PATTERN_DATETIME1))){
            return false;
        }
         if(localDateTime.isAfter(DateUtils.parseLocalDateTime(timePrefix + during.getTo(), DateUtils.PATTERN_DATETIME1))){
            return false;
        }
        return true;
    }

    /**
     * 门磁权限审核(动态权限)
     *
     * @param dm          门磁编号
     * @param uid         上报人ucId
     * @param doorPlateId 门牌ID
     * @return 检查结果，若用户有权限则ServiceResult.getObject()返回true,无权限返回false
     * @author Yang.Lee
     * @date 2022/7/15 14:04
     **/
    @Override
    public ServiceResult checkDynamicPermission(String dm, String uid, String doorPlateId) {

        // 检测门牌是否存在
        Optional<DoorPlate> doorPlateOp = iotDeviceCacheService.getDoorPlateToRedis(doorPlateId);
        if (doorPlateOp.isEmpty()) {
            return ServiceResult.error(ServiceMessage.NOT_FOUND_DOORPLATE);
        }
        DoorPlate doorPlate = doorPlateOp.get();

        // 获取用户信息
        Optional<PassV2UserResp> userRespOptional = sysUserService.getUserByUid(uid);
        if (userRespOptional.isEmpty()) {
            return ServiceResult.error("未找到用户信息", ErrConst.E01);
        }

        PassV2UserResp user = userRespOptional.get();

        String hrId;
        // 获取用户工号
        if (PersonType.VISITOR.equals(user.getPersonType())) {
            // 访客工号通过手机号获取

            Optional<Guest> guestOptional = guestService.getByMobile(user.getPhone());
            if (guestOptional.isEmpty()) {
                return ServiceResult.error("未找到用户信息", ErrConst.E01);
            }

            hrId = guestOptional.get().getHrId();
        } else {
            hrId = user.getHrId();
        }

        Optional<DoorMagnetic> doorMagneticOptional = doorMagneticDao.findByNumber(dm);

        if (doorMagneticOptional.isEmpty()) {
            log.error("未能根据门磁编号查找到设备信息，门磁dm: {}", dm);
            return ServiceResult.error(ServiceMessage.NOT_FOUNT_DOOR_MAGNETISM, ErrConst.E07);
        }

        // 获取设备所属会议室
        // 查询会议室room信息
        String roomId = null;
        Optional<DeviceToSpace> d2s = deviceToSpaceDao.findByDeviceId(doorPlate.getDevice().getId());
        if (d2s.isPresent()) {
            Optional<Room> roomOp = roomDao.findBySpaceId(d2s.get().getSpace().getId());
            if (roomOp.isPresent()) {
                roomId = roomOp.get().getId();
            }
        }

        List<ReservationRecord> resultList = reservationRecordDao.findByDateAndRoomIdOrderByFromAsc(LocalDate.now(), roomId);

        // 根据会议状态进行判断， 如果状体为空闲/进行中/即将开始且无马上开始的下一场会议
        DoorPlateStatus status = DoorPlateStatus.FREE;
        ReservationRecord rr;
        ReservationRecord currentRecord = null;
        ReservationRecord nextRecord = null;

        resultList = resultList.stream().filter(obj -> !obj.getStatus().equals(ReservationRecordStatus.CANCEL)).collect(Collectors.toList());

        for (int i = 0; i < resultList.size(); i++) {

            // 当前会议数据
            rr = resultList.get(i);

            ReservationRecord next = null;
            if (i < resultList.size() - 1) {
                next = resultList.get(i + 1);
            }

            // 获取会议状态
            status = doorPlateService.checkCurrentRoomStatus(rr, next);

            if (status.equals(DoorPlateStatus.END)) {
                continue;
            }

            if (!status.equals(DoorPlateStatus.FREE)) {
                // 非空闲状态，获取当前会议信息，并且判断是否有紧跟的下场会议
                currentRecord = rr;      // 当前会议数据
                boolean hasNext = (resultList.size() - i) > 0;
                if (Boolean.TRUE.equals(hasNext) && next != null) {
                    // 判断下一场有没有被取消
                    boolean isNotCancel = !next.getStatus().equals(ReservationRecordStatus.CANCEL);
//                    boolean isFollow = next.getFrom().isEqual(rr.getTo());
//                    boolean isGetNextRecord = isNotCancel && isFollow;
                    if (Boolean.TRUE.equals(isNotCancel)) {
                        nextRecord = new ReservationRecord();    // 下场会议数据
                        BeanUtils.copyProperties(next, nextRecord);
                    }
                }
                break;
            }
        }

        // 验证权限
        boolean isValid = qrCodeService.checkScanner(hrId, status, currentRecord, nextRecord);

        return ServiceResult.ok(isValid);
    }

    /**
     * 2.0版本门磁开门。（包含权限检查）
     *
     * @param dm          门磁编号
     * @param uid         用户UCid
     * @param openType    开门类型。 static: 静态二维码开门。 dynamic：动态二维码开门
     * @param doorPlateId 门牌ID,用于动态二维码开门
     * @return 开门结果
     * @author Yang.Lee
     * @date 2022/7/18 9:41
     **/
    @Override
    @Transactional
    public ServiceResult openV2(String dm, String uid, String openType, String doorPlateId) {

        Optional<PassV2UserResp> userRespOptional = sysUserService.getUserByUid(uid);
        if (userRespOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.NOT_FOUND_STAFF, ErrConst.E01);
        }

        PassV2UserResp userResp = userRespOptional.get();

        // 权限校验
        ServiceResult permissionCheck;
        if ("static".equals(openType.toLowerCase(Locale.ROOT))) {
            permissionCheck = checkStaticPermission(dm, uid);
        } else {
            permissionCheck = checkDynamicPermission(dm, uid, doorPlateId);
        }

        if (!permissionCheck.isSuccess()) {
            return permissionCheck;
        }

        Optional<DoorMagnetic> doorMagneticOp = doorMagneticDao.findByNumber(dm);
        if (!doorMagneticOp.isPresent()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUNT_DOOR_MAGNETISM, dm), ErrConst.E18);
        }
        DoorMagnetic doorMagnetic = doorMagneticOp.get();

        UserType userType = PersonType.EMPLOYEE.equals(userResp.getPersonType()) ? UserType.STAFF : UserType.GUEST;

        if (!(boolean) permissionCheck.getObject()) {
            // 记录通行结果
            saveAccessRecord(userResp.getHrId(), doorMagnetic.getId(), userType, DoorMagneticAccessResult.NO_ACCESS);
            return ServiceResult.error(ServiceMessage.PERMISSION_DENIED, ErrConst.E18);
        }

        // 记录通行结果
        // saveAccessRecord(userResp.getHrId(), doorMagnetic.getId(), userType, DoorMagneticAccessResult.ACCESS);
        String empNo = userResp.getHrId();
        String doorMagneticId = doorMagnetic.getId();
        Optional<DoorMagnetic> doorMagneticOptional = doorMagneticDao.findById(doorMagneticId);
        if (doorMagneticOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUNT_DOOR_MAGNETISM, doorMagneticId));
        }

        DoorMagneticAccessRecord doorMagneticAccessRecord = new DoorMagneticAccessRecord();
        doorMagneticAccessRecord.setEmpNo(empNo);
        doorMagneticAccessRecord.setAccessState(DoorMagneticAccessResult.ACCESS);
        doorMagneticAccessRecord.setDataTime(LocalDateTime.now());
        doorMagneticAccessRecord.setDoorMagnetic(doorMagneticOptional.get());

        String name = "";
        String department = "";
        if (userType.equals(UserType.STAFF)) {
            SysUser sysUser = sysUserService.getUserByEmpNo(empNo);
            if (sysUser==null) {
                return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_STAFF_UC, empNo));
            }
            name = sysUser.getNameZh();
            department = sysUser.getFullDepartmentName();
            doorMagneticAccessRecord.setNameZH(sysUser.getNameZh());
        } else {
            name = userResp.getName();
            department = "--";
            empNo = "--";
        }

        doorMagneticAccessRecordDao.save(doorMagneticAccessRecord);

        // 将数据写入设备通行权限表。 since V1.20
        DeviceAccessRecord deviceAccessRecord = new DeviceAccessRecord();
        deviceAccessRecord.setRecordType(DeviceAccessRecordType.DOOR_MAGNETIC);
        deviceAccessRecord.setAccessStatus(DoorMagneticAccessResult.ACCESS.getName());
        deviceAccessRecord.setDataTime(doorMagneticAccessRecord.getDataTime());
        deviceAccessRecord.setIotDevice(doorMagneticOptional.get().getDevice());
        deviceAccessRecord.setNameZh(name);
        deviceAccessRecord.setEmpNo(empNo);
        deviceAccessRecord.setStaffType(userType.equals(UserType.STAFF) ? PersonType.EMPLOYEE : PersonType.VISITOR);
        deviceAccessRecord.setDepartment(department);
        accessControlSpotAccessRecordService.create(deviceAccessRecord);

        if ("dynamic".equals(openType.toLowerCase(Locale.ROOT))) {
            // 动态二维码，进行签到动作
            // 查询会议室room信息
            String roomId = null;
            Optional<DeviceToSpace> d2s = deviceToSpaceDao.findByDeviceId(doorMagnetic.getDevice().getId());
            if (d2s.isPresent()) {
                Optional<Room> roomOp = roomDao.findBySpaceId(d2s.get().getSpace().getId());
                if (roomOp.isPresent()) {
                    roomId = roomOp.get().getId();
                }
                qrCodeService.meetingSign(roomId, userResp.getHrId());
            } else {
                log.warn("动态二维码开门，但是未找到会议室！！！！ 门磁编号 {}", dm);
            }
        }

        // 保存设备操作日志
        // 生成请求参数
        DoorMagneticHWReq doorMagneticHWReq = new DoorMagneticHWReq();
        doorMagneticHWReq.setDeviceId(doorMagnetic.getDevice().getId());
        doorMagneticHWReq.setDevice_id(doorMagnetic.getHuaweiId());
        DoorMagneticHWParasReq doorMagneticHWParasReq = new DoorMagneticHWParasReq();
        doorMagneticHWParasReq.setAction("open");
        doorMagneticHWReq.setParas(doorMagneticHWParasReq);

        hwProductsService.saveDeviceControlLog(doorMagneticHWReq, userResp.getHrId(), userResp.getName());

        String hwId = doorMagnetic.getHuaweiId();
        if (StringUtils.isNotEmpty(hwId)) {
            // 发送华为云开门请求
            return doorMagneticHWService.open(dm);
        }

        String macAddress = doorMagnetic.getDoorMagneticMac();
        if (StringUtils.isNotEmpty(macAddress)) {
            // 发送MQTT-CONNECTOR开门请求
            return doorMagneticMQTTService.open(dm);
        }

        log.warn("门磁开门未调用任何实现方式，门磁编号：{}", dm);

        return ServiceResult.ok();
    }

    /**
     * 获取门磁管理员列表
     *
     * @param dm 门磁编号
     * @return 管理员列表
     * @author Yang.Lee
     * @date 2022/7/18 10:38
     **/
    @Override
    public List<StaffResp> getDoorMagneticApprovalAdminList(String dm) {

        //创建门磁key
        DoorMagnetic doorMagnetic;
        String doorMagneticRedisKey = redisUtil.createDeviceRealKey(IConst.DEVICE_MODULE, String.valueOf(DeviceType.DOOR_MAGNETIC), dm, RedisUtil.Separator.COLON);
        if(redisUtil.hasKey(doorMagneticRedisKey)){
            Map<Object, Object> redisMap = redisUtil.hmget(doorMagneticRedisKey);
            doorMagnetic = DataUtils.mapCastTargetObject(redisMap, DoorMagnetic.class);
            if (doorMagnetic == null) {
                return new ArrayList<>();
            }
        }else {
            Optional<DoorMagnetic> doorMagneticOptional = doorMagneticDao.findByNumber(dm);

            if (doorMagneticOptional.isEmpty()) {
                return new ArrayList<>();
            }
            doorMagnetic = doorMagneticOptional.get();
            Map<String, Object> map = DataUtils.targetObjectCastMap(doorMagnetic, DoorMagnetic.class);
            redisUtil.hmset(doorMagneticRedisKey,map,true);
        }

        // 获取关系
        List<AccessControlSpotApproverRelation> relationList = accessControlSpotApproverRelationDao.findAllBySpotDeviceId(doorMagnetic.getDevice().getId());

        if (relationList.isEmpty()) {
            return new ArrayList<>();
        }

        return relationList.stream().map(obj -> sysUserService.getUserByEmpNo(obj.getUserHrId())).map(StaffResp::convert).collect(Collectors.toList());
    }

    /**
     * 查询用户对设备的申请记录
     *
     * @param uid ucId
     * @param dm  门磁编号
     * @return 申请记录
     * @author Yang.Lee
     * @date 2022/7/18 14:26
     **/
    @Override
    public List<AccessControlSpotApplicationRecordResp> getUserApplyRecordList(String uid, String dm) {

        // 查找设备信息
        Optional<DoorMagnetic> doorMagneticOp = doorMagneticDao.findByNumber(dm);
        if (!doorMagneticOp.isPresent()) {
            return new ArrayList<>();
        }
        DoorMagnetic doorMagnetic = doorMagneticOp.get();

        Optional<AccessControlSpot> accessControlSpotOptional = accessControlSpotDao.findByDeviceId(doorMagnetic.getDevice().getId());
        if (accessControlSpotOptional.isEmpty()) {
            return new ArrayList<>();
        }

        // 查找用户信息
        Optional<PassV2UserResp> userRespOptional = sysUserService.getUserByUid(uid);
        if (userRespOptional.isEmpty()) {
            return new ArrayList<>();
        }
        PassV2UserResp userResp = userRespOptional.get();
        String userHrId;

        if (PersonType.EMPLOYEE.equals(userResp.getPersonType())) {
            userHrId = userResp.getHrId();
        } else {
            Optional<Guest> guestOptional = guestService.getByMobile(userResp.getPhone());
            if (guestOptional.isEmpty()) {
                userHrId = userResp.getHrId();
            } else {
                userHrId = guestOptional.get().getHrId();
            }
        }

        // 查找记录
        return accessControlSpotApplicationRecordService.getUserApplyList(userHrId, accessControlSpotOptional.get().getId());
    }

    /**
     * 权限申请
     *
     * @param dm  门磁编号
     * @param uid ucId
     * @return 申请结果
     * @author Yang.Lee
     * @date 2022/7/18 17:22
     **/
    @Override
    @Transactional
    public ServiceResult permissionApply(String dm, String uid) {

        // 查询门禁点及用户信息
        Optional<PassV2UserResp> userRespOptional = sysUserService.getUserByUid(uid);
        if (userRespOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.NOT_FOUND_STAFF, ErrConst.E01);
        }

        Optional<DoorMagnetic> doorMagneticOp = doorMagneticDao.findByNumber(dm);
        if (!doorMagneticOp.isPresent()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUNT_DOOR_MAGNETISM, dm), ErrConst.E18);
        }

        PassV2UserResp userResp = userRespOptional.get();
        DoorMagnetic doorMagnetic = doorMagneticOp.get();

        Optional<AccessControlSpot> spotOptional = accessControlSpotDao.findByDeviceId(doorMagnetic.getDevice().getId());
        if (spotOptional.isEmpty()) {
            return ServiceResult.error("当前设备未绑定门禁点，不可进行权限申请。", ErrConst.E24);
        }

        AccessControlSpot spot = spotOptional.get();

        List<AccessControlSpotUser> accessControlSpotUserList = accessControlSpotUserDao.findByAccessControlSpotIdAndUserHrId(spot.getId(), userResp.getHrId());

        LocalDateTime now = LocalDateTime.now();
        Optional<AccessControlSpotUser> accessOptional = accessControlSpotUserList.stream().filter(obj -> now.isAfter(obj.getAccessStartTime()) || now.isBefore(obj.getAccessEndTime())).findFirst();

        if (accessOptional.isPresent()) {
            return ServiceResult.error("用户当前已拥有权限，不可再次申请", ErrConst.E24);
        }

        // 查询用户是否有正在审批的记录
        List<AccessControlSpotApplicationRecordResp> applicationRecordRespList = getUserApplyRecordList(uid, dm);

        // 查询有没有正在审批中的记录
        List<AccessControlSpotApplicationRecordResp> pendingList = applicationRecordRespList.stream().filter(obj -> ApprovalStatus.PENDING.equals(obj.getStatusType())).collect(Collectors.toList());

        if (!pendingList.isEmpty()) {
            return ServiceResult.error("当前审批正在进行中，请勿重复申请", ErrConst.E24);
        }

        // 进行权限申请

        // 保存申请记录
        AccessControlSpotApplicationRecord applicationRecord = new AccessControlSpotApplicationRecord();
        applicationRecord.setApplicantHrId(userResp.getHrId());
        applicationRecord.setApplicantName(userResp.getName());
        applicationRecord.setStatus(ApprovalStatus.PENDING);
        applicationRecord.setSpotId(spot.getId());
        applicationRecord.setSpotName(spot.getName());
        applicationRecord.setAccessControlSpotType(AccessControlSpotType.DOOR_MAGNETIC);
        applicationRecord.setSpotAddress(AccessControlSpotApplicationRecord.getSpotAddress(spot));

        accessControlSpotApplicationRecordDao.save(applicationRecord);

        // 查询该门禁点的权限审批人
        List<StaffResp> adminList = getDoorMagneticApprovalAdminList(dm);

        adminList.forEach(obj -> {

            SysUser approver = sysUserService.getUserByEmpNo(obj.getHrId());
            // 创建代办任务
            taskCenterTaskService.createAccessControlSpotApplicationTask(approver, applicationRecord);

            // 发送微信消息提示
            accessControlSpotApplicationRecordService.sendMessageToApprover(userResp.toSysUser(), applicationRecord, approver);
        });

        return ServiceResult.ok();
    }

    /**
     * 获取门磁设备管理员列表
     *
     * @param dm 门磁编号
     * @return 管理员列表
     * @author Yang.Lee
     * @date 2022/7/18 10:38
     **/
    @Override
    public List<StaffResp> getDoorMagneticDeviceAdminList(String dm) {

        Optional<DoorMagnetic> doorMagneticOptional = doorMagneticDao.findByNumber(dm);

        if (doorMagneticOptional.isEmpty()) {

            return new ArrayList<>();
        }
        DoorMagnetic doorMagnetic = doorMagneticOptional.get();

        // 查询该设备的管理员
        List<DeviceAdmin> adminList = deviceAdminDao.findByDeviceId(doorMagnetic.getDevice().getId());
        List<String> adminHrId;
        if (adminList.isEmpty()) {
            // 如果该设备没有配置管理员，发给默认管理员
            adminHrId = Arrays.asList(defaultDeviceAdminList.split(","));
        } else {
            adminHrId = adminList.stream().map(DeviceAdmin::getAdminHrId).collect(Collectors.toList());
        }

        return adminHrId.stream().map(obj -> StaffResp.convert(sysUserService.getUserByEmpNo(obj))).collect(Collectors.toList());
    }


    /**
     * 通过华为云查询设备状态
     *
     * @param hwDeviceId 设备华为云ID
     * @return 设备状态
     * @author Yang.Lee
     * @date 2022/7/13 13:52
     **/
    private DeviceStatus getStatusFromHW(String hwDeviceId) {
        Optional<DeviceDetail> deviceDetailOptional = hwProductsService.getDeviceDetail(hwDeviceId);

        if (deviceDetailOptional.isEmpty()) {
            return DeviceStatus.UNKNOW;
        }
        DeviceDetail deviceDetail = deviceDetailOptional.get();

        return deviceDetail.getStatus();
    }

    /**
     * 通过南通mqtt查询设备状态
     *
     * @param doorMagneticMac 设备MAC地址
     * @return 设备状态
     * @author Yang.Lee
     * @date 2022/7/13 13:52
     **/
    private DeviceStatus getStatusFromNTMqtt(String doorMagneticMac) {

        // 暂时不知道如何获取南通mqtt的设备状态。 默认返回在线
        log.warn("设备 {} 向南通门磁发送状态查询请求。暂无查询方法，统一返回在线！！！！！！", doorMagneticMac);
        return DeviceStatus.ONLINE;
    }
}
