-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 10.0.1.232    Database: ibms_uat
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `iot_door_plate`
--

DROP TABLE IF EXISTS `iot_door_plate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `iot_door_plate` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `created_by` varchar(36) DEFAULT NULL COMMENT '创建人',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(36) DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int(11) DEFAULT NULL COMMENT '版本号',
  `magnetic_num` varchar(50) DEFAULT NULL COMMENT '门磁设备编号',
  `name` varchar(20) NOT NULL COMMENT '门牌名称',
  `seq` int(11) DEFAULT NULL COMMENT '排序',
  `device_id` varchar(36) NOT NULL COMMENT 'FK',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FKlfhe6ixbhlh6mcoyfe106pk9b` (`device_id`) USING BTREE,
  CONSTRAINT `FKlfhe6ixbhlh6mcoyfe106pk9b` FOREIGN KEY (`device_id`) REFERENCES `iot_device` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='门牌';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `iot_door_plate`
--

LOCK TABLES `iot_door_plate` WRITE;
/*!40000 ALTER TABLE `iot_door_plate` DISABLE KEYS */;
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`) VALUES ('1e753ed7-d6c2-4dae-b33d-151db09a8535',NULL,'2020-12-31 11:22:46',0,NULL,'2020-12-31 11:22:46',10,'321','花桥会议室JJC',0,'5646bc13-31db-4f7a-85bb-f46dd148873a');
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`) VALUES ('3b9e1a65-59c0-446c-a4e3-bfe6eb07f3ad',NULL,'2021-01-21 16:23:18',0,NULL,'2021-01-21 16:23:18',1,'fm21zu8v','Room',3,'e28739ed-358c-4770-aadd-13095490e5eb');
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`) VALUES ('4d64abd3-079d-4803-a3c0-8e244315cafa',NULL,'2021-01-06 18:41:36',0,NULL,'2021-01-06 18:41:36',0,'fm21zu8v','产品研发中心',4,'2f646c72-111f-4610-9b32-61fe8182de3b');
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`) VALUES ('5c169bf6-0624-451b-93be-ecdd447f376c',NULL,'2021-01-14 15:59:13',0,NULL,'2021-01-14 15:59:13',0,NULL,'天玑会议室',11,'3047883a-43de-4364-880b-320e8f5d50b9');
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`) VALUES ('894988ed-d486-4922-8c6c-223c0851b375',NULL,'2021-01-12 15:42:51',0,NULL,'2021-01-12 15:42:51',0,NULL,'发发发',6,'6288bd48-cd0f-4da0-9a96-b53527289363');
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`) VALUES ('89ec9521-4652-49fc-b9f5-f85ff49c0ee8',NULL,'2020-12-30 20:16:22',1,NULL,'2020-12-30 20:16:22',1,'55667777','test',0,'e1068166-8389-4d87-b67b-4fde4f4f467a');
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`) VALUES ('97ae1fb8-e75a-46eb-a9c8-1944c49977eb',NULL,'2021-01-12 15:45:37',0,NULL,'2021-01-12 15:45:37',0,NULL,'华侨',8,'1fc7e9ac-611f-493d-946d-a7fbbc636266');
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`) VALUES ('a0b193a9-14ee-4394-8226-51880afb4999',NULL,'2020-12-30 20:29:00',0,NULL,'2020-12-30 20:29:00',1,'321','启明',1,'08ce2a99-c807-4ada-8976-9dec5ace9e88');
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`) VALUES ('a31c05a4-4fcb-494b-a75c-c121d7a83d7c',NULL,'2021-01-06 17:45:31',0,NULL,'2021-01-06 17:45:31',0,'fm21zu8v','产品研发中心',2,'8ac1b16d-c38e-4317-8780-702234f8c0d6');
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`) VALUES ('abf091ed-3c6d-49ed-a0b1-1cb4672329a2',NULL,'2021-01-21 16:23:16',0,NULL,'2021-01-21 16:23:16',1,'fm21zu8v','Room',2,'e36cbbbe-665b-4616-a221-8b94a41a855a');
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`) VALUES ('b061218e-b995-45b2-8f7f-1950e66c9ed8',NULL,'2021-01-05 15:23:51',1,NULL,'2021-01-05 15:23:51',2,'fm21zu8v','产品研发中心',3,'511a8015-3993-4fa1-8151-f6543bb48d23');
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`) VALUES ('b43bb4e2-9cd8-4a53-8c8e-6195cbc915ff',NULL,'2021-01-12 15:45:05',0,NULL,'2021-01-12 15:45:05',1,NULL,'fff',7,'6a1cd0d1-55f9-4986-92e8-d7458d9847da');
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`) VALUES ('b4897769-aec2-4352-8fee-11fd58814638',NULL,'2021-01-12 15:45:47',0,NULL,'2021-01-12 15:45:47',0,NULL,'华侨',9,'72aa01a1-7b55-48df-9f7c-c7fc4ccfd3fe');
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`) VALUES ('b8a52db3-b1fc-4500-88a7-32525eb0e113',NULL,'2021-01-08 09:23:18',0,NULL,'2021-01-08 09:23:18',0,'啦啦啦啦','恩久软件',5,'45741b54-c080-40b2-a804-13a6024a0aa2');
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`) VALUES ('dabc499f-f4c4-42d7-a727-de8e85ea1c8f',NULL,'2021-01-13 09:37:50',1,NULL,'2021-01-13 09:37:50',0,'55667777','test',10,'e7065df1-100a-4864-934b-b6e026d3c223');
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`) VALUES ('e49789a6-f16f-4aab-90f7-50052bc3af91',NULL,'2021-01-06 18:08:46',0,NULL,'2021-01-06 18:08:46',0,'fm21zu8v','产品研发中心',3,'4150c508-fc0d-407e-8fe4-4a34a8947cd1');
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`) VALUES ('e840c468-e16a-4583-a509-95242ad43010',NULL,'2021-01-13 15:14:10',0,NULL,'2021-01-13 15:14:10',2,NULL,'天玑会议室',10,'716c30e4-4d09-48ed-b986-c1a922a323e4');
/*!40000 ALTER TABLE `iot_door_plate` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-25 15:53:39
