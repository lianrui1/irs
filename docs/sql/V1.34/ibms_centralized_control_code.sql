create table ibms_centralized_control_code
(
    id                   varchar(36)  not null comment '主键'
        primary key,
    created_by           varchar(36)  null comment '创建人',
    created_time         datetime     null comment '创建时间',
    is_del               tinyint(1)   not null comment '是否已删除,0:否;1:是',
    updated_by           varchar(36)  null comment '更新人',
    updated_time         datetime     null comment '更新时间',
    version              int          null comment '版本号',
    admin_hr_id          varchar(255) not null comment '管理员工号',
    admin_name           varchar(100) not null comment '管理员姓名',
    control_device_range varchar(20)  null comment '控制设备范围',
    control_device_type  varchar(300) not null comment '控制的设备类型，可多个。使用”,“分割',
    name                 varchar(20)  not null comment '集控码名称',
    project_id           varchar(36)  not null comment '关联项目id',
    space_id             varchar(36)  not null comment '关联空间id'
)
    comment '集控码';