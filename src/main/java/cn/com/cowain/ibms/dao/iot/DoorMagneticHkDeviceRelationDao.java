package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.DoorMagneticHKDeviceRelation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/10/19 14:02
 */
public interface DoorMagneticHkDeviceRelationDao extends JpaRepository<DoorMagneticHKDeviceRelation, String>, JpaSpecificationExecutor<DoorMagneticHKDeviceRelation> {

    /**
     * 根据门磁id查询关系
     *
     * @param doorMagneticId 门磁ID
     * @return 关系列表
     * @author Yang.Lee
     * @date 2021/10/19 14:03
     **/
    List<DoorMagneticHKDeviceRelation> findByDoorMagneticId(String doorMagneticId);
}
