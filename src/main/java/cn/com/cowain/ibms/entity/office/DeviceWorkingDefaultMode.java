package cn.com.cowain.ibms.entity.office;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Yang.Lee
 * @date 2021/12/20 10:01
 */
@Data
@Entity
@Table(name = "iot_device_working_default_mode")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_device_working_default_mode", comment = "默认设备工作模式表")
public class DeviceWorkingDefaultMode  extends BaseEntity {

    @Column(name = "name", nullable = false, columnDefinition = "varchar(50) COMMENT '工作模式名称'")
    private String name;

    @Column(name = "img", nullable = false, columnDefinition = "varchar(100) COMMENT '工作模式图标'")
    private String img;

    @Column(name = "purpose", nullable = false, columnDefinition = "varchar(50) COMMENT '工作模式用途'")
    private String purpose;
}
