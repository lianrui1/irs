package cn.com.cowain.ibms.service.meal;

import cn.com.cowain.ibms.mq.bean.OpenAbilityMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;

import java.util.List;

/**
 * @author wei.cheng
 * @date 2022/06/06 18:22
 */
public interface MealService {
    /**
     * 旷视鸿图权限配置
     *
     * @param mealList
     * @return
     */
    ServiceResult permissionConfig(List<OpenAbilityMessage.Meal> mealList);
}
