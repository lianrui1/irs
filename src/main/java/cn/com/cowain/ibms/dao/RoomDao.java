package cn.com.cowain.ibms.dao;

import cn.com.cowain.ibms.entity.Room;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Room Dao
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/3 19:36
 */
@Repository
public interface RoomDao extends JpaRepository<Room, String>, JpaSpecificationExecutor<Room> {

    /**
     * 根据name精确查找
     *
     * @param name
     * @return
     */
    Optional<Room> findByName(String name);

    /**
     * 根据name 模糊查找
     * @param name
     * @return
     */
    Set<Room> findByNameLike(String name);

    /**
     * 根据name模糊查找 分页
     * @param name
     * @param pageable
     * @return
     */
    Page<Room> findByNameContains(String name, Pageable pageable);

    /**
     * 获取会议室排序最大值
     * @return
     */
    @Query("SELECT MAX(room.seq) FROM Room AS room")
    Integer findMaxSeq();

    /**
     * 根据spaceId查询room
     *
     * @param spaceId
     * @return
     * @author Yang.Lee
     */
    Optional<Room> findBySpaceId(String spaceId);

    /**
     * 根据name模糊查找
     * @param name
     * @return
     */
    List<Room> findByNameContainsOrderByCreatedTime(String name);

    /**
     * 根据项目ID查找
     * @return
     */
    Page<Room> findBySpaceProjectIdContains(String projectId, Pageable pageable);

    /**
     * 根据项目ID查找
     * @return
     */
    List<Room> findBySpaceProjectId(String id);

    /**
     * 根据项目ID查询并且Space中isShow=1 room中isOpen=1
     * @Param:
     */
    List<Room> findBySpaceProjectIdAndSpaceIsShowAndIsOpen(String id,int isShow,int isOpen);
    /**
     * 根据用户会议室使用时长查询会议室数据
     *
     * @param empNo
     * @param projectId
     * @return
     * @author Yang.Lee
     * @date 2021/4/25 15:47
     **/
    @Query(value = "SELECT r.* , urut.used_time FROM `ibms_room` r " +
            "LEFT JOIN ibms_user_room_used_time urut ON urut.room_id = r.id AND if (?1 != '',urut.user_emp_no=?1, 1=1 )" +
            "LEFT JOIN iot_space s ON s.id = r.space_id " +
            "LEFT JOIN ibms_project  p ON p.id = s.project_id " +
            "WHERE " +
                "r.is_del = 0 " +
                "AND if(?2 != '',p.id=?2, 1=1 ) " +
                "AND r.is_open = 1 " +
           "ORDER BY urut.used_time DESC , r.seq  ",
            nativeQuery = true)
    List<Room> searchWithUsedTime(String empNo,String projectId);

    @Query(
            value = "select id from ibms_room where is_open = 1 and is_del = 0 order by seq",
            nativeQuery = true
    )
    List<String> findAllIdOrderBySeqAsc();

    @Query(
            value = "select id from ibms_room where space_id in (:spaceIds) and is_open = 1 and is_del = 0 order by seq",
            nativeQuery = true
    )
    List<String> findIdBySpaceIdInOrderSeqAsc(@Param("spaceIds")List<String> spaceIds);

    List<Room> findAllByIdIn(List<String> ids);

    /**
     * 根据会议室名称模糊查询id
     * @param like
     * @return
     */
    @Query(
            value = "select id from ibms_room where room_name like ?1 and is_del = 0",
            nativeQuery = true
    )
    List<String> findIdByNameLike(@Param("like") String like);
}
