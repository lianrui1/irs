package cn.com.cowain.ibms.rest.resp.iot;

import cn.com.cowain.ibms.entity.iot.AlertSpot;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 警戒点返回参数
 *
 * @author: yanzy
 * @date: 2022/4/13 10:48
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("警戒点返回参数")
public class AlertSpotResp {

    @ApiModelProperty(value = "警戒点id", required = true, position = 1)
    private String id;

    @ApiModelProperty(value = "警戒点名称", required = true, position = 2)
    private String name;

    @ApiModelProperty(value = "警戒点地址", position = 3)
    private String address;

    @ApiModelProperty(value = "空间id", position = 4)
    private String spaceId;

    @ApiModelProperty(value = "设备id", position = 5)
    private String deviceId;

    public static AlertSpotResp convert(AlertSpot obj) {
        AlertSpotResp alertSpotResp = new AlertSpotResp();
        alertSpotResp.setId(obj.getId());
        alertSpotResp.setName(obj.getName());
        alertSpotResp.setAddress(obj.getAddress());
        alertSpotResp.setSpaceId(obj.getSpace().getId());
        alertSpotResp.setDeviceId(obj.getDevice().getId());
        return alertSpotResp;
    }
}
