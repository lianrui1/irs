package cn.com.cowain.ibms.feign.iotc.bean;

import cn.com.cowain.ibms.utils.DateUtils;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 设备通行记录对象
 *
 * @author Yang.Lee
 * @date 2022/2/24 21:02
 */
@Data
public class DeviceRecordResp {

    private String doorName;

    private String jobNo;

    private String name;

    private String personId;

    private String type;

    private String personType;

    private String cardNo;

    private String doorId;

    @JSONField(format = DateUtils.PATTERN_DATETIME)
    private LocalDateTime happenTime;
}
