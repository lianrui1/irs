package cn.com.cowain.ibms.job;

import cn.com.cowain.ibms.dao.iot.SpaceDao;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.service.office.SpaceCacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author wei.cheng
 * @date 2022/09/20 16:14
 */
@Slf4j
@Component
public class SpaceParentCacheJob {
    @Autowired
    private SpaceDao spaceDao;
    @Autowired
    private SpaceCacheService spaceCacheService;

    /**
     * 定时任务：刷新缓存中空间的子节点列表数据
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void run() {
        log.info("定时任务开始，刷新缓存中空间的子节点列表数据");
        List<Space> spaceList = spaceDao.findAll();
        spaceList.forEach(space -> spaceCacheService.setSpaceParent(space));
    }
}
