package cn.com.cowain.ibms.service.bean;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/4 14:52
 */
@Data
public class AirConditionerReq {
    private String deviceName;
    private String mode;
    private String setFanAuto;
    private String setPower;
    private int temperature;
    private String windDirection;
    private String windSpeed;
    private String setSwing;
}
