package cn.com.cowain.ibms.websocket.bean.resp;

import cn.com.cowain.ibms.enumeration.iot.DoorPlateStatus;
import cn.com.cowain.ibms.rest.resp.StaffResp;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Yang.Lee
 * @date 2022/1/5 15:17
 */
@Data
@ApiModel
public class MeetingAppResp {

    @ApiModelProperty(value = "当前会议信息", position = 1)
    private MeetingDetail currentMeeting;

    @ApiModelProperty(value = "下一场会议信息", position = 2)
    private MeetingDetail nextMeeting;

    @Data
    public static class MeetingDetail{

        @ApiModelProperty(value = "会议ID", position = 1)
        private String meetingId;

        @ApiModelProperty(value = "会议标题", position = 2)
        private String topic;

        @ApiModelProperty(value = "主持人姓名", position = 3)
        private String hostName;

        @ApiModelProperty(value = "主持人工号", position = 4)
        private String hostHrId;

        @ApiModelProperty(value = "主持人部门", position = 5)
        private String hostDepartmentName;

        @ApiModelProperty(value = "主持人部门全称", position = 6)
        private String hostFullDepartmentName;

        @ApiModelProperty(value = "主持人图片", position = 7)
        private String hostImg;

        @ApiModelProperty(value = "会议状态", position = 8)
        private DoorPlateStatus meetingStatus;

        @ApiModelProperty(value = "会议状态名称", position = 9)
        private String meetingStatusName;

        @ApiModelProperty(value = "会议开始时间", position = 10)
        private LocalDateTime from;

        @ApiModelProperty(value = "会议结束时间", position = 11)
        private LocalDateTime to;

        @ApiModelProperty(value = "参会人信息", position = 12)
        List<MeetingMember> memberList;

        @ApiModelProperty(value = "会议持续时间", position = 13)
        private String duration;

        @ApiModelProperty(value = "欢迎语", position = 14)
        private String welcomeWords;
    }

    @Data
    public static class MeetingMember{

        @ApiModelProperty(value = "参会人信息", position = 1)
        private StaffResp staff;

        @ApiModelProperty(value = "参会人是否已签到。true:已签到；false:未签到", position = 2)
        private boolean hasSignIn;

        @ApiModelProperty(value = "是否是访客。true:是；false:否", position = 3)
        private boolean isGuest;
    }
}
