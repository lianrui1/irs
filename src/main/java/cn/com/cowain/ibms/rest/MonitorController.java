package cn.com.cowain.ibms.rest;

import cn.com.cowain.ibms.entity.iot.VideoScreenshot;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.iot.MonitorReq;
import cn.com.cowain.ibms.rest.req.iot.ScreenShotReq;
import cn.com.cowain.ibms.rest.resp.*;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.monitor.MonitorService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author feng
 * @title: MonitorController
 * @projectName bims
 * @Date 2021/11/18 10:52
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/security")
@Api(tags = IConst.MODULE_SECURITY)
public class MonitorController {
    @Resource
    private  MonitorService monitorService;

    /**
     　　* @title: 监控点管理 --begain
     　　* @description 获取配置好的监控列表
     　　* @author jfsui
     　　* @date 2021/11/18 10:30
     　　*/
    @GetMapping("/sit/getlist")
    @ApiOperation(value = "获取站点树", tags = {IConst.MODULE_SECURITY,IConst.V20})
    public ResponseEntity<JsonResult<List<LocationNodeResp>>>   getListMonitorSitList(){
        List<LocationNodeResp> resultList= monitorService.getListMonitorSitList();
        return  ResponseEntity.ok(JsonResult.ok("ok", resultList));
    }
    @GetMapping("/sit/getDetail/{id}")
    @ApiOperation(value = "获取站点树--这个节点下的所有监控",tags = {IConst.MODULE_SECURITY,IConst.V20})
    public ResponseEntity<JsonResult<PageBean<MonitorSitResp>>>   getListMonitorSitDetail(@PathVariable String id,MonitorReq req){
        PageBean<MonitorSitResp>  result =  monitorService.getMonitorListBySportId(id,req);
        return  ResponseEntity.ok(JsonResult.ok("ok", result));
    }
    @GetMapping("/sit/getmonitordetail/{id}")
    @ApiOperation(value = "获取监控详情",tags = {IConst.MODULE_SECURITY,IConst.V20})
    public ResponseEntity<JsonResult<MonitorLocationResp>>   getmonitordetail(@PathVariable String id){
        MonitorLocationResp  result =  monitorService.getMonitorDetail(id);
        return  ResponseEntity.ok(JsonResult.ok("ok", result));
    }
    @PostMapping("/sit/delete/{id}")
    @ApiOperation(value = "删除站点",tags = {IConst.MODULE_SECURITY,IConst.V20})
    public ResponseEntity<JsonResult<String>> deleteMonitorSit(@PathVariable String id){
        if (Boolean.TRUE.equals(monitorService.deleteSpot(id))) {
            JsonResult<String> jsonResult = JsonResult.okNoData("删除成功");
            return ResponseEntity.ok(jsonResult);
        } else {
            JsonResult<String> jsonResult = JsonResult.error( "删除失败", null, ErrConst.E00002);
            return ResponseEntity.ok(jsonResult);
        }

    }
    @PostMapping("/sit/edit")
    @ApiOperation(value = "编辑站点",tags = {IConst.MODULE_SECURITY,IConst.V20})
    public ResponseEntity<JsonResult<String>> editMonitorSit(@RequestBody MonitorSitResp sitResp){
        if(StringUtils.isEmpty(sitResp.getId())){
            JsonResult<String> jsonResult = JsonResult.error( "编辑失败，未传id", null, ErrConst.E00001);
            return ResponseEntity.ok(jsonResult);
        }
        String ids=monitorService.checkoutName(sitResp.getSiteName());
        if(   StringUtils.isNotEmpty(ids)&&!ids.equals(sitResp.getId())){
            JsonResult<String> jsonResult = JsonResult.error("监控点名称不允许重复", null, ErrConst.E00001);
            return ResponseEntity.ok(jsonResult);
        }

        if(Boolean.TRUE.equals(monitorService.createMonitorSit(sitResp))){
            JsonResult<String> jsonResult = JsonResult.okNoData("编辑成功");
            return ResponseEntity.ok(jsonResult);
        }
        JsonResult<String> jsonResult = JsonResult.error( "编辑失败", null, ErrConst.E00001);
        return ResponseEntity.ok(jsonResult);
    }
    @PostMapping("/sit/create")
    @ApiOperation(value = "新建站点",tags = {IConst.MODULE_SECURITY,IConst.V20})
    public ResponseEntity<JsonResult<String>> createMonitorSit(@RequestBody MonitorSitResp sitResp) {
        sitResp.setId(null);
        if (StringUtils.isEmpty(sitResp.getMonitorId()) || StringUtils.isEmpty(sitResp.getSpaceId())||StringUtils.isEmpty(sitResp.getSiteName())) {
            JsonResult<String> jsonResult = JsonResult.error("参数异常", null, ErrConst.E00001);
            return ResponseEntity.ok(jsonResult);
        }
        if(   StringUtils.isNotEmpty(monitorService.checkoutName(sitResp.getSiteName()))){
            JsonResult<String> jsonResult = JsonResult.error("监控点名称不允许重复", null, ErrConst.E00001);
            return ResponseEntity.ok(jsonResult);
        }
        Boolean isSuccess = monitorService.createMonitorSit(sitResp);
        String result = Boolean.TRUE.equals(isSuccess) ? "新建成功" : "新建失败";
        if (Boolean.TRUE.equals(isSuccess)) {
            JsonResult<String> jsonResult = JsonResult.okNoData(result);
            return ResponseEntity.ok(jsonResult);
        } else {
            JsonResult<String> jsonResult = JsonResult.error(result, null, ErrConst.E00001);
            return ResponseEntity.ok(jsonResult);
        }

    }
    @GetMapping("/sit/monitorlist")
    @ApiOperation(value = "设备列表",tags = {IConst.MODULE_SECURITY,IConst.V20})
    public ResponseEntity<JsonResult<PageBean<MonitorLocationResp>>> getMonitorList(MonitorReq req){
        PageBean<MonitorLocationResp> result=monitorService.getMonitorList(req);
        return  ResponseEntity.ok(JsonResult.ok("ok", result));
    }
    /**
     　　* @title: 监控点管理 --end
     　　* @description
     　　* @author jfsui
     　　* @date 2021/11/18 10:30
     　　*/
    /** 例如：昆山-花桥-汇金大厦-16楼-左侧监控 */
    @GetMapping("/sit/live")
    @ApiOperation(value = "获取在线视频对应的id",tags = {IConst.MODULE_SECURITY,IConst.V20})
    public ResponseEntity<JsonResult<List<MonitorCommonResp>>> getLive(String code){

        return null;
    }
    /** 例如：昆山-花桥-汇金大厦-16楼-左侧监控 */
    @GetMapping("/sit/back")
    @ApiOperation(value = "获取在线视频对应的id",tags = {IConst.MODULE_SECURITY,IConst.V20})
    public ResponseEntity<JsonResult<String>> getBack(String code){

        return null;
    }

    /** 保存图片 ***/
    @PostMapping("/sit/screenshot")
    @ApiOperation(value = "保存截图",tags = {IConst.MODULE_SECURITY,IConst.V20})
    public ResponseEntity<JsonResult<String>> saveCreenshot (@RequestBody ScreenShotReq req){
        String result=monitorService.upLoadPictureSave(req);
        if(StringUtils.isNotEmpty(result)&&result.equals("文件上传成功")){
            return  ResponseEntity.ok(JsonResult.ok("ok", result));
        }else {
            JsonResult<String> jsonResult = JsonResult.error( result, null, ErrConst.E00001);
            return ResponseEntity.ok(jsonResult);
        }

    }

    /** 删除图片 ***/
    @PostMapping("/sit/imagedelete/{id}")
    @ApiOperation(value = "删除截图",tags = {IConst.MODULE_SECURITY,IConst.V20})
    public ResponseEntity<JsonResult<String>> deleteImage (@PathVariable  String id){
        ServiceResult result=monitorService.deleteImage(id);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E00002));
        }
        return ResponseEntity.ok(JsonResult.ok("OK", (String)result.getObject()));
    }
    /** 查询图片列表 ***/
    @PostMapping("/sit/screenshotlist")
    @ApiOperation(value = "获取截图列表",tags = {IConst.MODULE_SECURITY,IConst.V20})
    public ResponseEntity<JsonResult<PageBean<VideoScreenshot>>> creenshotList (@RequestBody MonitorReq req){
        PageBean<VideoScreenshot>  result =monitorService.creenshotList(req);
        return  ResponseEntity.ok(JsonResult.ok("ok", result));
    }



}
