package cn.com.cowain.ibms.service.office.impl;

import cn.com.cowain.ibms.component.SpaceAdminRedis;
import cn.com.cowain.ibms.dao.iot.SpaceDao;
import cn.com.cowain.ibms.dao.space.SpaceAdminDao;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.space.SpaceAdmin;
import cn.com.cowain.ibms.service.office.SpaceCacheService;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.ibms.utils.RedisUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;

@Service
@Log4j2
public class SpaceCacheServiceImpl implements SpaceCacheService {
    public static final int TWO_DAY_SECOND = 2 * 24 * 60 * 60;
    @Autowired
    SpaceDao spaceDao;

    @Autowired
    SpaceAdminDao spaceAdminDao;
    @Autowired
    RedisUtil redisUtil;

    @Override
    public void setSpaceInAdminRedis(SpaceAdmin spaceAdmin) {
        final String spaceListKey = redisUtil.createSpaceAdminRealKey(IConst.SPACE_ADMIN_MODULE, spaceAdmin.getAdminEmpNo(), RedisUtil.Separator.COLON);
        if (redisUtil.hasKey(spaceListKey)) {
          List<SpaceAdminRedis> spaceAdmins = redisUtil.lGet(spaceListKey, 0, -1, SpaceAdminRedis.class);
           if (spaceAdmins.stream().noneMatch(space -> StringUtils.equals(space.getSpaceId(), spaceAdmin.getSpace().getId()))) {
            SpaceAdminRedis spaceAdminRedis = new SpaceAdminRedis();
            BeanUtils.copyProperties(spaceAdmin, spaceAdminRedis);
            spaceAdminRedis.setSpaceId(spaceAdmin.getSpace().getId());
            redisUtil.lSet(spaceListKey, spaceAdminRedis, false);
            }
        } else {
            SpaceAdminRedis spaceAdminRedis = new SpaceAdminRedis();
            BeanUtils.copyProperties(spaceAdmin, spaceAdminRedis);
            spaceAdminRedis.setSpaceId(spaceAdmin.getSpace().getId());
            redisUtil.lSet(spaceListKey, spaceAdminRedis, false);
        }
        setSpaceInRedis(spaceAdmin.getSpace());
    }

    public void delSpaceInAdminRedis(SpaceAdmin spaceAdmin) {
        final String spaceListKey = redisUtil.createSpaceAdminRealKey(IConst.SPACE_ADMIN_MODULE, spaceAdmin.getAdminEmpNo(), RedisUtil.Separator.COLON);
        if (redisUtil.hasKey(spaceListKey)) {
            redisUtil.del(spaceListKey);
        }
    }

    @Override
    public void initSpace() {
        List<SpaceAdmin> spaceAdminList = spaceAdminDao.findAll();
        initSpace(spaceAdminList);
    }

    @Override
    public void initSpace(List<SpaceAdmin> spaceAdminList) {
        if (!CollectionUtils.isEmpty(spaceAdminList)) {
            spaceAdminList.forEach(this::setSpaceInAdminRedis);

        }
    }

    @Override
    public void delSpaceAdmin(List<SpaceAdmin> spaceAdminList) {
        if (!CollectionUtils.isEmpty(spaceAdminList)) {
            spaceAdminList.forEach(this::delSpaceInAdminRedis);

        }
    }

    /**
     * 通过空间ID将space放入Redis缓存
     * 如果存在则先删除再添加
     * 备注：该方法可以用在初始化以及space发生变更时。
     */
    @Override
    public void setSpaceInRedis(Space space) {
        if (space == null) {
            return;
        }
        final String spaceKey = redisUtil.createSpaceAdminRealKey(IConst.SPACE_MODULE, space.getId(), RedisUtil.Separator.COLON);
        if (redisUtil.hasKey(spaceKey)) {
            redisUtil.del(spaceKey);
            redisUtil.set(spaceKey, space);
        } else {
            redisUtil.set(spaceKey, space);
        }
        setSpaceParent(space);
    }

    /**
     * 保存空间到其父节点下，由于空间不支持修改其上级节点，暂不考虑节点的上级节点进行了修改的情况，简单维护一下父节点的子节点列表
     * @param space
     */
    @Override
    public void setSpaceParent(Space space) {
        if(Objects.nonNull(space.getParent())){
            String parentKey = redisUtil.createSpaceAdminRealKey(IConst.SPACE_PARENT_MODULE, space.getParent().getId(), RedisUtil.Separator.COLON);
            redisUtil.sSetAndTime(parentKey, TWO_DAY_SECOND, space.getId());
        }
    }

    /**
     * 从Redis删除空间
     */
    @Override
    public void delSpace(Space space) {
        if (space == null) {
            return;
        }
        final String spaceKey = redisUtil.createSpaceAdminRealKey(IConst.SPACE_MODULE, space.getId(), RedisUtil.Separator.COLON);
        if (redisUtil.hasKey(spaceKey)) {
            redisUtil.del(spaceKey);
        }
        deleteSpaceParent(space);
    }

    /**
     * 将节点从其父节点的子节点列表中删除，并删除节点本身的子节点数据
     * @param space
     */
    private void deleteSpaceParent(Space space) {
        if(Objects.nonNull(space.getParent())){
            redisUtil.setRemove(redisUtil.createSpaceAdminRealKey(IConst.SPACE_PARENT_MODULE, space.getParent().getId(), RedisUtil.Separator.COLON),
                    space.getId());
        }
        redisUtil.del(redisUtil.createSpaceAdminRealKey(IConst.SPACE_PARENT_MODULE, space.getId(), RedisUtil.Separator.COLON));
    }

    public void delSpace(String spaceId) {
        final String spaceKey = redisUtil.createSpaceAdminRealKey(IConst.SPACE_MODULE, spaceId, RedisUtil.Separator.COLON);
        if (redisUtil.hasKey(spaceKey)) {
            redisUtil.del(spaceKey);
        }
    }

    @Override
    public void delSpaces(List<String> ids) {
        ids.forEach(this::delSpace);
    }

//    private void insertSpace(String spaceKey, String id) {
//        Optional<Space> spaceOptional = spaceDao.findById(id);
//        spaceOptional.ifPresent(space -> redisUtil.set(spaceKey, space));
//    }
}
