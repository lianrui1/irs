package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.AccessControlSpotApproverRelation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccessControlSpotApproverRelationDao extends JpaRepository<AccessControlSpotApproverRelation, String>, JpaSpecificationExecutor<AccessControlSpotApproverRelation> {

    // 根据工号获取门禁点-审批人关系
    List<AccessControlSpotApproverRelation> findByUserHrId(String userHrId);

    /**
     * 查询门禁点的所有AccessControlSpotApproverRelation
     *
     * @param spotId
     * @return
     */
    List<AccessControlSpotApproverRelation> findAllBySpotId(String spotId);

    // 根据人员工号和门禁点ID获取关系
    Optional<AccessControlSpotApproverRelation> findByUserHrIdAndSpotId(String empNo, String s);

    // 查询门禁点的所有AccessControlSpotApproverRelation
    List<AccessControlSpotApproverRelation> findAllBySpotIdIn(List<String> spotIdList);


    /**
     * 根据设备ID查询管理员关系
     *
     * @param deviceId 设备ID
     * @return 关系列表
     * @author Yang.Lee
     * @date 2022/7/18 10:49
     **/
    List<AccessControlSpotApproverRelation> findAllBySpotDeviceId(String deviceId);
}
