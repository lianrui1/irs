package cn.com.cowain.ibms.feign.bean.uc;

import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2021/12/21 9:41
 */
@Data
public class CommonResDto {

    private Integer errorcode;
    private String errorMsg;
}
