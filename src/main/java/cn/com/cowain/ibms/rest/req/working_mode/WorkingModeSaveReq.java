package cn.com.cowain.ibms.rest.req.working_mode;

import cn.com.cowain.ibms.enumeration.iot.UsableStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/10/18 14:09
 */
@Data
@ApiModel("新建工作模式对象")
public class WorkingModeSaveReq {

    @NotBlank
    @ApiModelProperty(value = "小图标url", required = true)
    private String imgUrl;


    @ApiModelProperty(value = "场景名称", required = true)
    private String name;

    @Length(max = 40,message = "用途最长为40")
    @ApiModelProperty(value = "用途")
    private String purpose;

    @NotBlank
    @ApiModelProperty(value = "空间ID", required = true)
    private String spaceId;

    @ApiModelProperty(value = "会议室ID", required = true)
    private String officeId;

    @ApiModelProperty(value = "是否启用 DISABLED禁用 ENABLE启用")
    private UsableStatus usableStatus = UsableStatus.ENABLE;

    //@Size(min = 1, message = "设备不能为空")
    @NotEmpty
    @ApiModelProperty(value = "设备状态设置对象", required = true)
    private List<DeviceModeStatusSaveReq> saveReqs;
    @ApiModelProperty(value = "是否是总控，0不是总控，1是总控")
    private Integer showroomCentralize;
    // showroom 用
    @ApiModelProperty(value = "情景模式id")
    private String id;

}
