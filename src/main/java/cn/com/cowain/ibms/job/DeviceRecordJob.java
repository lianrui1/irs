package cn.com.cowain.ibms.job;

import cn.com.cowain.ibms.dao.ability.OpenAbilityAccessControlSpotDao;
import cn.com.cowain.ibms.dao.iot.DeviceAccessRecordDao;
import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.ability.OpenAbilityAccessControlSpot;
import cn.com.cowain.ibms.entity.iot.DeviceAccessRecord;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.enumeration.ability.Ability;
import cn.com.cowain.ibms.enumeration.iot.DeviceAccessRecordType;
import cn.com.cowain.ibms.enumeration.iot.PersonType;
import cn.com.cowain.ibms.feign.iotc.IOTCApi;
import cn.com.cowain.ibms.feign.iotc.bean.DeviceRecordResp;
import cn.com.cowain.ibms.feign.iotc.bean.DeviceRecordSearchReq;
import cn.com.cowain.ibms.feign.iotc.enumeration.MethodCode;
import cn.com.cowain.ibms.feign.iotc.enumeration.ModuleCode;
import cn.com.cowain.ibms.mq.producer.AccessRecordProducer;
import cn.com.cowain.ibms.rest.req.iot.GroupBindingReq;
import cn.com.cowain.ibms.rest.resp.distinguish.WorkTimeCheckResp;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.sfp.comm.PageBean;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Yang.Lee
 * @date 2022/2/24 20:26
 */
@Slf4j
@Component
public class DeviceRecordJob {

    @Resource
    private IOTCApi iotcApi;

    @Resource
    private OpenAbilityAccessControlSpotDao openAbilityAccessControlSpotDao;

    @Resource
    private AccessRecordProducer accessRecordProducer;

    @Resource
    private DeviceDao deviceDao;

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserServiceUC;

    @Resource
    private DeviceAccessRecordDao deviceAccessRecordDao;


    @Scheduled(cron = "0 0/10 * * * ? ")
    public void run() {

        log.info("开始补发考勤数据");

        // 查询所有的考勤设备
        List<OpenAbilityAccessControlSpot> spotList = openAbilityAccessControlSpotDao.findByOpenAbilityAbility(Ability.ATTENDANCE);

        // 获取所有有效设备的SN
        List<String> deviceSnList = spotList.stream().filter(obj ->
                obj.getSpot().getDevice() != null && obj.getSpot().getDevice().getSn() != null)
                .map(obj -> obj.getSpot().getDevice().getSn())
                .collect(Collectors.toList());

        int page = 1;
        // 每次查询1000条记录
        int pageSize = 1000;

        LocalDateTime now = LocalDateTime.now();

        LocalDateTime start = now.minusMinutes(12);
        LocalDateTime end = now;

        // tsdb请求构建参数
        DeviceRecordSearchReq searchReq = DeviceRecordSearchReq.builder()
                .dataType(DeviceRecordSearchReq.DataType.WAITING)
                .doorIds(deviceSnList.stream().collect(Collectors.joining(",")))
                .startTime(start)
                .endTime(end)
                .page(page)
                .size(pageSize)
                .build();

        log.debug("req :  {} ", JSON.toJSONString(searchReq));

        // 构建iotc构建参数
        GroupBindingReq req = GroupBindingReq.builder()
                .moduleCode(ModuleCode.TSDB.getName())
                .channelType("7")
                .jsonArg(JSON.toJSONString(searchReq))
                .chainSite("CLOUD")
                .methodCode(MethodCode.GET_REPLENISH_RECORD.getName())
                .build();
        log.debug("iotc req :  {} ", JSON.toJSONString(req));
        JsonResult<Object> result = iotcApi.postToConn(req);
        log.debug("获取夜补数据结果 :  {} ", JSON.toJSONString(result));
        if (result.getStatus() == 0) {
            log.error("获取设备通行记录补录数据异常 {}", result);
            return;
        }

        // result中的data数据默认是linkedHashMap


        PageBean<DeviceRecordResp> recordPage = JSON.parseObject(JSON.toJSONString(result.getData()), new TypeReference<>(PageBean.class) {
        });

        log.debug("设备通行记录共 {} 条, ", recordPage.getTotalCount());
        List<DeviceRecordResp> totalData = new ArrayList<>(recordPage.getList());

        int totalPages = recordPage.getTotalPages();
        if (totalPages > 1) {

            // 数据有多页，分多次查询结果，将结果合并后处理
            for (int i = page + 1; i <= totalPages; i++) {
                log.debug("开始第{}次获取设备通行记录数据", i);
                searchReq.setPage(i);
                req.setJsonArg(JSON.toJSONString(searchReq));
                log.debug("req :  {} ", JSON.toJSONString(req));
                result = iotcApi.postToConn(req);

                if (result.getStatus() == 0) {
                    log.error("获取设备通行记录补录数据异常 {}", result);
                    return;
                }

                recordPage = JSON.parseObject(JSON.toJSONString(result.getData()), new TypeReference<>(PageBean.class) {
                });

                totalData.addAll(recordPage.getList());
            }
        }

        log.debug("共获得设备通行记录{}条", totalData.size());

        // 过滤工号不对的数据
        totalData = totalData.stream().filter(obj -> StringUtils.isNotBlank(obj.getJobNo())
                && !obj.getJobNo().equals("无组织")).collect(Collectors.toList());

        totalData.forEach(obj -> {


            Optional<IotDevice> deviceOptional = deviceDao.findBySn(obj.getDoorId());
            if (deviceOptional.isEmpty()) {
                return;
            }
            IotDevice device = deviceOptional.get();
            DeviceAccessRecord deviceAccessRecord = new DeviceAccessRecord();
            deviceAccessRecord.setEmpNo(obj.getJobNo());
            // 人名
            deviceAccessRecord.setNameZh(StringUtils.isBlank(obj.getName()) ? "陌生人" : obj.getName());
            if (SysUserService.isVisitor(deviceAccessRecord.getEmpNo())) {
                // 访客
                deviceAccessRecord.setStaffType(PersonType.VISITOR);
                deviceAccessRecord.setDepartment("--");
                // 访客工号设置
                deviceAccessRecord.setEmpNo(Optional.ofNullable(obj.getJobNo()).orElse("--"));
            } else {
                // 人员类型 1 工号开头不是VN 2  工号开头为VN
                deviceAccessRecord.setStaffType(PersonType.EMPLOYEE);
                // 设置部门/工号
                // 根据工号获取人员信息
                // uc人员数据
                SysUser userByEmpNo = sysUserServiceUC.getUserByEmpNo(obj.getJobNo());
                // 部门设置(UC数据异常处理)
                deviceAccessRecord.setDepartment(Optional.ofNullable(userByEmpNo).map(SysUser::getFullDepartmentName).orElse("无部门"));
            }

//            deviceAccessRecord.setAccessStatus(StringUtils.isBlank(obj.getAccessStatus()) ? "通行" : accessRecordMessage.getAccessStatus());
//            // 通行图片
//            deviceAccessRecord.setCapturePicture(accessRecordMessage.getCapturePicture());
//            // 底库图片
//            deviceAccessRecord.setDefaultPicture(accessRecordMessage.getDefaultPicture());
            // 通行时间
            deviceAccessRecord.setDataTime(obj.getHappenTime());
            // 设备id
            deviceAccessRecord.setIotDevice(device);
            // 设备来源
            deviceAccessRecord.setRecordType(DeviceAccessRecordType.HIK);
            // 设备通行记录
            deviceAccessRecordDao.save(deviceAccessRecord);


            accessRecordProducer.push(WorkTimeCheckResp.convert(deviceAccessRecord));
        });


    }

}
