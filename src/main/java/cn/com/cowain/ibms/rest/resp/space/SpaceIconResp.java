package cn.com.cowain.ibms.rest.resp.space;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2020/12/17 14:35
 */
@Data
@ApiModel("空间平面信息响应对象")
public class SpaceIconResp {

    @ApiModelProperty(value = "图标", required = true)
    private String icon;

    @ApiModelProperty(value = "状态", required = true, position = 1)
    private Integer status;

}
