package cn.com.cowain.ibms.service.impl;

import cn.com.cowain.ibms.bo.SimpleReservationRecord;
import cn.com.cowain.ibms.bo.SimpleRoomDevice;
import cn.com.cowain.ibms.bo.SimpleSysUser;
import cn.com.cowain.ibms.bo.SimpleTimeSlot;
import cn.com.cowain.ibms.dao.*;
import cn.com.cowain.ibms.dao.audio.AudioDao;
import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.dao.iot.DeviceToSpaceDao;
import cn.com.cowain.ibms.dao.iot.DoorPlateDao;
import cn.com.cowain.ibms.dao.iot.SpaceDao;
import cn.com.cowain.ibms.dao.meeting.*;
import cn.com.cowain.ibms.dao.meetingfile.MeetingFileDao;
import cn.com.cowain.ibms.dao.space.ProjectDao;
import cn.com.cowain.ibms.entity.*;
import cn.com.cowain.ibms.entity.iot.DeviceToSpace;
import cn.com.cowain.ibms.entity.iot.DoorPlate;
import cn.com.cowain.ibms.entity.meeting.*;
import cn.com.cowain.ibms.entity.space.Project;
import cn.com.cowain.ibms.entity.voice.AudioRecord;
import cn.com.cowain.ibms.enumeration.ReservationRecordItemStatus;
import cn.com.cowain.ibms.enumeration.ReservationRecordStatus;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.enumeration.meeting.FileType;
import cn.com.cowain.ibms.enumeration.meeting.GuestInvitationStatus;
import cn.com.cowain.ibms.exceptions.BizLogicNotMatchException;
import cn.com.cowain.ibms.exceptions.DataNotFoundException;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.bean.ParticipantBean;
import cn.com.cowain.ibms.rest.bean.ReservationRecordBean;
import cn.com.cowain.ibms.rest.bean.TimeSlotBean;
import cn.com.cowain.ibms.rest.req.*;
import cn.com.cowain.ibms.rest.req.meeting.*;
import cn.com.cowain.ibms.rest.req.working_mode.WelcomeWordReq;
import cn.com.cowain.ibms.rest.resp.*;
import cn.com.cowain.ibms.rest.resp.audio.AudioDetailResp;
import cn.com.cowain.ibms.rest.resp.iot.DeviceWechatResp;
import cn.com.cowain.ibms.rest.resp.meeting.*;
import cn.com.cowain.ibms.service.ReservationRecordService;
import cn.com.cowain.ibms.service.SMSService;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.WeChatService;
import cn.com.cowain.ibms.service.audio.AudioService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.guest.GuestService;
import cn.com.cowain.ibms.service.meeting.IntelligentMeetingAppService;
import cn.com.cowain.ibms.service.meeting.InvitationCardService;
import cn.com.cowain.ibms.service.meeting.MeetingFileService;
import cn.com.cowain.ibms.service.meeting.ScheduleService;
import cn.com.cowain.ibms.service.space.SpaceService;
import cn.com.cowain.ibms.utils.*;
import cn.com.cowain.ibms.utils.enums.DateUnit;
import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.zip.CRC32;

import static cn.com.cowain.ibms.enumeration.ReservationRecordStatus.*;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/4 19:00
 */
@Slf4j
@Service
public class ReservationRecordServiceImpl implements ReservationRecordService {

    private static final long HALF_HOUR = 60L * 30;
    private static final long HOUR = 60L * 0;
    private static final String AUTHTOKEN = "token";
    @Autowired
    private RoomDao roomDao;
    @Autowired
    private ReservationRecordDao reservationRecordDao;
    @Autowired
    private ReservationRecordItemDao reservationRecordItemDao;
    @Autowired
    private TimeSlotDao timeSlotDao;
    @Autowired
    private RoomDeviceDao roomDeviceDao;
    @Resource(name = "sysUserService")
    private SysUserService sysUserService;
    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserServiceUC;
    @Autowired
    private WeChatService weChatService;
    @Autowired
    private DeviceToSpaceDao deviceToSpaceDao;
    @Autowired
    private DoorPlateDao doorPlateDao;
    @Autowired
    private OpeningHoursDao openingHoursDao;
    @Autowired
    private SysUserDao sysUserDao;
    @Autowired
    private RoomUsedTimeDao roomUsedTimeDao;

    @Autowired
    private InvitationCardService cardService;
    @Autowired
    private GuestDetailDao detailDao;
    @Autowired
    private GuestInvitationRecordDao recordDao;
    @Autowired
    private SpaceService spaceService;
    @Resource
    private ScheduleService scheduleService;
    @Autowired
    private SMSService smsService;
    @Autowired
    private InvitationCardDao invitationCardDao;
    @Autowired
    private ReservationRecord2CustomGroupDao record2CustomGroupDao;
    @Autowired
    private CustomUserGroupDao customUserGroupDao;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private MeetingFileService meetingFileService;
    @Autowired
    private IntelligentMeetingAppDao intelligentMeetingAppDao;

    @Autowired
    private DeviceDao deviceDao;
    @Autowired
    private SpaceDao spaceDao;

    @Value("${cowain.ehr.invitation.url}")
    private String invitationUrl;
    @Value("${cowain.ehr.cancel.invitation.url}")
    private String cancelUrl;
    @Value("${cowain.ehr.detail.url}")
    private String detailUrl;
    @Value("${cowain.ehr.reject.url}")
    private String rejectUrl;
    @Value("${cowain.ehr.invitation.renew.url}")
    private String updateUrl;
    @Value("${cowain.ehr.invitation.update.url}")
    private String updateTimeUrl;
    @Resource
    private GuestService guestService;

    @Resource
    private IntelligentMeetingAppService intelligentMeetingAppService;

    @Autowired
    private AudioDao audioDao;

    @Autowired
    private AudioService audioService;

    @Autowired
    private AttendanceDao attendanceDao;
    @Resource
    private MeetingFileDao meetingFileDao;

    @Resource
    private ProjectDao projectDao;

    @Resource
    private RedisUtil redisUtil;

    @Autowired
    private RestTemplateUtil restTemplateUtil;

    @Value("${cowain.ehr.visitorType.url}")
    private String visitorTypeUrl;

    private static final String VISITORTYPE = "visitorType";

    public static Date slip(String time) {
        StringTokenizer st = new StringTokenizer(time, ":");
        List<String> list = new ArrayList<>();
        while (st.hasMoreElements()) {
            list.add(st.nextToken());
        }
        int hour = 0;
        int minute = 0;
        hour = Integer.parseInt(list.get(0));
        minute = Integer.parseInt(list.get(1));
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, 0);

        return cal.getTime();
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public ReservationRecord save(ReservationAddReq req, String token) {
        // 判断时间段是否可用
        String roomId = req.getRoomId();
        Set<OpeningHours> byRoomId = openingHoursDao.findByRoomId(roomId);
        List<OpeningHours> openingHours = new ArrayList<>(byRoomId);
        OpeningHours openingHour = openingHours.get(0);
        // 请求的 from,to
        LocalDateTime reqFrom = req.getFrom();
        LocalDateTime reqTo = req.getTo();
        // 格式化组件
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DateTimeFormatter df2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        // 数据库字符串 from,to
        String from = openingHour.getFrom();
        String to = openingHour.getTo();
        // 获得日期
        String dateStr = df.format(reqFrom);
        // 字符串拼接
        from = dateStr + " " + from;
        to = dateStr + " " + to;
        // 数据库 LocalDateTime from,to
        LocalDateTime dbFrom = LocalDateTime.parse(from, df2);
        LocalDateTime dbTo = LocalDateTime.parse(to, df2);
        // 比较时间先后
        if (reqFrom.isBefore(dbFrom) || reqTo.isAfter(dbTo)) {
            throw new BizLogicNotMatchException("悟能,你又粗心了,时间段不可用");
        }
        //保存 reservationRecord
        ReservationRecord reservationRecord = new ReservationRecord();
        BeanUtils.copyProperties(req, reservationRecord);
        reservationRecord.setTo(reservationRecord.getTo());
        //设置发起人
        reservationRecord.setInitiator(req.getInitiator().getSysId());
        // 保存数据使用工号
        reservationRecord.setInitiatorEmpNo(req.getInitiator().getEmpNo());
        //设置会议室房间
        Optional<Room> roomOptional = roomDao.findById(roomId);

        if (roomOptional.isPresent()) {
            Room room = roomOptional.get();
            reservationRecord.setRoom(room);

            // 判断会议室是否存在门磁设备。如果存在则写入免打扰信息,不存在则将免打扰设为null
            ServiceResult serviceResult = ifRoomHasDoorMagnetic(room.getId());
            RoomDoorMagneticResp hasDoorMagnetic = (RoomDoorMagneticResp) serviceResult.getObject();
            if (hasDoorMagnetic.isHasMagnetic()) {
                reservationRecord.setNoDisturb(req.getNoDisturb() == 1);
            } else {
                reservationRecord.setNoDisturb(null);
            }
        }

        //初始化状态
        reservationRecord.setStatus(DEFAULT);

        if (req.getScreenStatus() != null) {
            //设置会议大屏状态
            setScreenStatus(roomId, reservationRecord, req.getScreenStatus());
        }

        //保存
        reservationRecordDao.save(reservationRecord);

        if (!CollectionUtils.isEmpty(req.getMeetingFileList())) {
            //保存会议文档
            meetingFileService.createMeetingFile(reservationRecord, req.getMeetingFileList(), TokenUtils.getToken());
        }

        //保存会议链接
        req.getUrls().forEach(url -> {
            ReservationUploadUrlReq urlReq = new ReservationUploadUrlReq();
            urlReq.setUrlList(url.getUrlList());
            urlReq.setUrlRemark(url.getUrlRemark());
            meetingFileService.createMeetingUrl(reservationRecord, urlReq, TokenUtils.getToken());
        });

        if (StringUtils.isNoneEmpty(reservationRecord.getService())) {
            weChatService.specialRequirementsReminders(reservationRecord);
        }

        // 保存会议邀请卡
        InvitationCard card = new InvitationCard();
        if (!StringUtils.isBlank(req.getReason())) {
            creatInvitation(req, reservationRecord, token, card);
        }

        //获取会议预约 创建人
        SysUserBean initiator = req.getInitiator();
        String initiatorId = initiator.getSysId();
        String initiatorEmpNo = initiator.getEmpNo();
        //获取会议预约 参与人清单

        List<SysUserBean> participantList = req.getParticipantList();
        //循环保存 参与人 会议预约明细
        if (participantList != null) {
            participantList.forEach(it -> {
                ReservationRecordItem rri = new ReservationRecordItem();
                //初始状态是:未确定
                rri.setStatus(ReservationRecordItemStatus.UNDETERMINED);
                //设置创建人
                rri.setInitiatorEmpNo(initiator.getEmpNo());
                rri.setInitiator(initiator.getSysId());
                //设置拥有人
                rri.setOwnerEmpNo(it.getEmpNo());
                String sysId = it.getSysId();
                rri.setOwner(sysId);
                rri.setIsGroup(it.getIsGroup());
                //设置所属预约记录
                rri.setReservationRecord(reservationRecord);
                if (it.getIsGuest() == 0) {
                    //保存用户信息
                    sysUserServiceUC.trySaveIfNeed(it.getEmpNo());
                }

                // 保存访客邀请记录
                if (it.getIsGuest() == 1) {
                    GuestInvitationRecord record = new GuestInvitationRecord();
                    record.setName(it.getNameZh());
                    record.setMobile(it.getEmpNo());
                    record.setGuestInvitationStatus(GuestInvitationStatus.DEFAULT);
                    record.setInvitationCard(card);
                    // 生成访客邀请记录码
                    CRC32 crc32 = new CRC32();
                    crc32.update(UUID.randomUUID().toString().getBytes(StandardCharsets.UTF_8));
                    String value = String.valueOf(crc32.getValue());
                    record.setCode(value);
                    recordDao.save(record);
                    // todo 从user获取发起人信息
                    SysUser user = sysUserServiceUC.getUserByEmpNo(initiator.getEmpNo());
                    smsService.asyncSendMeetingInvitationSMS(user.getNameZh(), user.getMobile(), record.getMobile(), card.getInvitationCode(), record.getCode());
                    //smsService.asyncSendMeetingInvitationSMS("张三" ,"110" ,record.getMobile() ,card.getInvitationCode() ,record.getCode());
                }

                //保存会议预约明细
                if (!initiatorEmpNo.equals(rri.getOwnerEmpNo()) && it.getIsGuest() == 0) {
                    reservationRecordItemDao.save(rri);
                    log.debug("创建会议，向参与人【" + rri.getOwnerEmpNo() + "】发送微信模板消息");
                    weChatService.createMeetingReminders(rri.getOwnerEmpNo(), reservationRecord);
                    log.debug("创建会议，发送微信模板消息完成");

//                    // 保存参会人日程数据
//                    scheduleService.create(reservationRecord, it.getEmpNo());
                }
            });
        }

        //保存 发起人 会议预约明细
        ReservationRecordItem rri = new ReservationRecordItem();
        rri.setStatus(ReservationRecordItemStatus.CONFIRM);

        rri.setInitiator(initiatorId);
        rri.setInitiatorEmpNo(initiatorEmpNo);
        rri.setOwner(initiatorId);
        rri.setOwnerEmpNo(initiatorEmpNo);
        rri.setReservationRecord(reservationRecord);
        //保存用户信息
        log.debug("ReservationRecordServiceImpl的save方法即将调用sysUserService.trySaveIfNeed方法保存会议创建人信息，创建人工号：" + initiatorEmpNo + ", 会议id：" + reservationRecord.getId());

        sysUserService.trySaveIfNeed(initiatorEmpNo);
        //保存会议预约明细
        reservationRecordItemDao.save(rri);

        Set<ReservationRecordItem> reservationRecordItemSet = reservationRecordItemDao.findByReservationRecordId(reservationRecord.getId());
        reservationRecordItemSet.forEach(obj ->
            // 保存参会人日程数据
            scheduleService.create(reservationRecord, obj.getOwnerEmpNo())
        );
        if (!req.getUserGroupList().isEmpty()) {
            req.getUserGroupList().forEach(it -> {
                Optional<CustomUserGroup> byId = customUserGroupDao.findById(it);
                if (byId.isPresent()) {
                    ReservationRecord2CustomGroup record2CustomGroup = new ReservationRecord2CustomGroup();
                    record2CustomGroup.setCustomUserGroup(byId.get());
                    record2CustomGroup.setReservationRecord(reservationRecord);
                    record2CustomGroupDao.save(record2CustomGroup);
                }
            });
        }

        // 推送消息给会议App
        intelligentMeetingAppService.pushMeetingRoomDetail(roomId);

        //返回
        return reservationRecord;
    }

    /**
     * 创建会议邀请函（不向ehr创建邀请函）
     *
     * @param req               请求参数
     * @param reservationRecord 会议信息
     * @param card              邀请函
     * @return
     * @author Yang.Lee
     * @date 2022/4/21 17:09
     **/
    private ServiceResult creatInvitation(ReservationAddReq req, ReservationRecord reservationRecord, InvitationCard card) {
        // 分解事由code
        String[] split = req.getReason().split("_");
        String reason = split[0];
        String reasonCode = split[1];

        String visitorRecordNo = req.getInvitationCardNo();
        log.info("会议邀请函单号 : " + visitorRecordNo);

        BeanUtils.copyProperties(req, card);
        card.setReservationRecord(reservationRecord);
        // 生成唯一code
        if (StringUtils.isBlank(card.getInvitationCode())) {
            CRC32 crc32 = new CRC32();
            crc32.update(UUID.randomUUID().toString().getBytes(StandardCharsets.UTF_8));
            String value = String.valueOf(crc32.getValue());
            card.setInvitationCode(value);
        }
        card.setRemark(req.getInvitationRemark());
        // 保存邀请函单号
        card.setVisitorRecordNo(visitorRecordNo);
        // 事由编码
        card.setReasonCode(reasonCode);
        // 事由
        card.setReason(reason);
        // 访客时间
        card.setStartTime(reservationRecord.getFrom());
        card.setEndTime(reservationRecord.getTo());
        invitationCardDao.save(card);
        return ServiceResult.ok(card);
    }

    private ServiceResult creatInvitation(ReservationAddReq req, ReservationRecord reservationRecord, String token, InvitationCard card) {
        // 向ehr创建访客邀请函
        // json参数
        EhrInvitationReq invitationReq = new EhrInvitationReq();
        // 分解事由code
        String[] split = req.getReason().split("_");
        String reason = split[0];
        String reasonCode = split[1];
        // 地址
        Project project = reservationRecord.getRoom().getSpace().getProject();
        invitationReq.setAddress(project.getVisitCompany() + "|" + project.getProvinceName() + project.getCityName() + project.getDistrictName() + project.getAddress());
        invitationReq.setEndTime(DateUtils.formatLocalDateTime(req.getEndTime(), DateUtils.PATTERN_DATETIME1));
        invitationReq.setStartTime(DateUtils.formatLocalDateTime(req.getStartTime(), DateUtils.PATTERN_DATETIME1));
        invitationReq.setReasonCode(reasonCode);
        invitationReq.setInviteMethod("1");
        invitationReq.setCompanyId(project.getVisitor());
//        invitationReq.setIsVip(req.getIsVip().toString());
        //访客类型code
        invitationReq.setRoleCode(req.getRoleCode());
        //会议室信息
        invitationReq.setMeeting(null);
        invitationReq.setRemarks(req.getInvitationRemark());
        String requestParam = JSON.toJSONString(invitationReq);
        log.debug("json参数 ： " + requestParam);
        // 发送Http请求 下发控制命令
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(AUTHTOKEN, token);
        HttpEntity<String> requestEntity = new HttpEntity<>(requestParam, headers);
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = restTemplate.postForEntity(invitationUrl, requestEntity, String.class);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ServiceResult.error("邀请函创建失败");
        }
        // 解析请求结果
        String responseBody = responseEntity.getBody();
        JSONObject jsonResult = JSON.parseObject(responseBody);
        log.debug("创建邀请函解析结果{}", jsonResult);
        String codeEhr = jsonResult.getString("code");
        if (!codeEhr.equals("0")) {
            return ServiceResult.error(jsonResult.getString("msg"));
        }
        String visitorRecordNo = jsonResult.getString("visitorRecordNo");
        log.info("会议邀请函单号 : " + visitorRecordNo);

        BeanUtils.copyProperties(req, card);
        card.setReservationRecord(reservationRecord);
        // 生成唯一code
        if (StringUtils.isBlank(card.getInvitationCode())) {
            CRC32 crc32 = new CRC32();
            crc32.update(UUID.randomUUID().toString().getBytes(StandardCharsets.UTF_8));
            String value = String.valueOf(crc32.getValue());
            card.setInvitationCode(value);
        }
        card.setRemark(req.getInvitationRemark());
        // 保存邀请函单号
        card.setVisitorRecordNo(visitorRecordNo);
        // 事由编码
        card.setReasonCode(reasonCode);
        // 事由
        card.setReason(reason);
        // 访客时间
        card.setStartTime(req.getStartTime());
        card.setEndTime(req.getEndTime());
        //访客类型代码code
        card.setRoleCode(req.getRoleCode());
        invitationCardDao.save(card);
        return ServiceResult.ok(card);
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public ServiceResult update(String reservationId, ReservationUpdateReq reservationUpdateReq, String token) {
        List<GuestInvitationRecord> records = new ArrayList<>();
        //根据 会议记录id 获取会议记录
        Optional<ReservationRecord> reservationRecordOptional = reservationRecordDao.findById(reservationId);
        //获取 开始时间
        LocalDateTime localDateTimeFrom = reservationUpdateReq.getFrom();
        //获取结束时间
        LocalDateTime localDateTimeTo = reservationUpdateReq.getTo();
        //获取 房间号
        String roomId = reservationUpdateReq.getRoomId();

        // 原数据中所有参会人集合
        Set<String> userSet = new HashSet<>();
        Set<ReservationRecordItem> reservationRecordItemSet = reservationRecordItemDao.findByReservationRecordId(reservationId);
        reservationRecordItemSet.forEach(it ->
                userSet.add(it.getOwnerEmpNo())
        );
        //判断是否存在
        // 访客list
        List<EhrUserAuth> ehrUserAuths = new ArrayList<>();
        boolean present = reservationRecordOptional.isPresent();
        if (present) {
            log.info(String.valueOf(reservationRecordOptional));
            ReservationRecord reservationRecord = reservationRecordOptional.get();
            // 在会议结束前，才可以编辑会议
            LocalDateTime to = reservationRecord.getTo();
            LocalDateTime now = LocalDateTime.now();
            if (DateUtil.isExceed(to, now, HOUR)) {
                return ServiceResult.error("在会议结束前，才可以编辑会议");
            }
            // 判断时间是否有改动
            if (!reservationRecord.getFrom().equals(localDateTimeFrom) || !reservationRecord.getTo().equals(localDateTimeTo)) {
                log.info("开始时间 或 结束时间 不相同");
                log.info("更新。。。。。");
                reservationRecord.setDate(reservationUpdateReq.getDate());
                reservationRecord.setFrom(reservationUpdateReq.getFrom());
                reservationRecord.setTo(reservationUpdateReq.getTo());
            } else {
                log.info("时间相同");
            }
            //判断会议主题是否有改动
            if (!reservationRecord.getTopic().equals(reservationUpdateReq.getTopic())) {
                //设置主题
                reservationRecord.setTopic(reservationUpdateReq.getTopic());
            }
            //判断 会议室房间是否有改动
            if (!reservationRecord.getRoom().getId().equals(roomId)) {
                Optional<Room> room = roomDao.findById(roomId);
                boolean roomPresent = room.isPresent();
                if (roomPresent) {
                    // 更新房间
                    reservationRecord.setRoom(room.get());
                }
            }

            // 判断会议室是否存在门磁设备。如果存在则写入免打扰信息,不存在则将免打扰设为null
            ServiceResult serviceResult = ifRoomHasDoorMagnetic(reservationUpdateReq.getRoomId());
            RoomDoorMagneticResp hasDoorMagnetic = (RoomDoorMagneticResp) serviceResult.getObject();
            if (hasDoorMagnetic.isHasMagnetic()) {
                reservationRecord.setNoDisturb(reservationUpdateReq.getNoDisturb() == 1);
            } else {
                reservationRecord.setNoDisturb(null);
            }

            reservationRecord.setRemark(reservationUpdateReq.getRemark());
            reservationRecord.setOriginalTo(reservationRecord.getTo());

            // 设置欢迎语
            reservationRecord.setWelcomeWords(reservationUpdateReq.getWelcomeWords());

            //设置会议大屏状态
            setScreenStatus(roomId, reservationRecord, reservationUpdateReq.getScreenStatus());

            //更新 会议记录
            reservationRecordDao.save(reservationRecord);

            // 更新会议文档
            meetingFileService.updateMeetingFile(reservationRecord, reservationUpdateReq.getMeetingFileList(), TokenUtils.getToken());

            //会议链接
            //保存会议链接
            meetingFileDao.findByReservationRecordIdAndFileType(reservationId, FileType.URL).forEach(it -> {
                it.setIsDelete(1);
                meetingFileDao.save(it);
            });
            reservationUpdateReq.getUrls().forEach(url -> {
                ReservationUploadUrlReq urlReq = new ReservationUploadUrlReq();
                urlReq.setUrlList(url.getUrlList());
                urlReq.setUrlRemark(url.getUrlRemark());
                meetingFileService.createMeetingUrl(reservationRecord, urlReq, TokenUtils.getToken());
            });

            // 更新邀请函
            InvitationCard card = new InvitationCard();
            Optional<InvitationCard> cardOptional = invitationCardDao.findByReservationRecord(reservationRecord);
            if (reservationUpdateReq.getEndTime() != null) {
                if (cardOptional.isPresent()) {

                    card = cardOptional.get();

                    // 向ehr查询邀请函信息
                    ServiceResult result = findVistitor(card, token);
                    if (!result.isSuccess()) {
                        return ServiceResult.error(result.getObject());
                    }
                    // 响应信息
                    VisitorResp visitorResp = (VisitorResp) result.getObject();
                    ehrUserAuths = visitorResp.getUserAuthList();


                    if ("50".equals(visitorResp.getRecordInfoDto().getVisitorUserStatus()) && !DateUtils.formatLocalDateTime(card.getStartTime(), DateUtils.PATTERN_DATETIME1).equals(DateUtils.formatLocalDateTime(reservationUpdateReq.getStartTime(), DateUtils.PATTERN_DATETIME1))) {
                        return ServiceResult.error("访客在访问中状态，不可修改开始时间！");
                    }

                    card.setStartTime(reservationUpdateReq.getStartTime());
                    card.setEndTime(reservationUpdateReq.getEndTime());
                    invitationCardDao.save(card);

                    //当访客状态为：50(访问中)只能修改邀请函到访结束时间
                    if ("50".equals(visitorResp.getRecordInfoDto().getVisitorUserStatus())) {
                        // 向ehr更新邀请函时间
                        ServiceResult result1 = updateVisitorTime(visitorResp.getRecordInfoDto().getId(), card, reservationUpdateReq, token);
                        if (!result1.isSuccess()) {
                            return ServiceResult.error(result1.getObject());
                        }
                        log.debug("邀请函到访结束时间延迟成功");
                    } else {
                        //整单修改邀请函
                        ServiceResult result1 = updateVisitor(reservationUpdateReq, token, visitorResp.getRecordInfoDto());
                        if (!result1.isSuccess()) {
                            return ServiceResult.error(result1.getObject());
                        }
                        log.debug("整单邀请函修改成功");
                    }

                } else {
                    // 向ehr创建邀请函
                    ReservationAddReq req = new ReservationAddReq();
                    BeanUtils.copyProperties(reservationUpdateReq, req);
                    ServiceResult result = creatInvitation(req, reservationRecord, token, card);
                    if (!result.isSuccess()) {
                        return ServiceResult.error(result.getObject());
                    }
                    card = (InvitationCard) result.getObject();
                }
            }
            List<ReservationRecordItem> reservationRecordItemList = new ArrayList<>();

            List<UserStatusBean> list = reservationUpdateReq.getParticipantList();
            Set<String> reqSet = new HashSet<>();
            if (Objects.nonNull(list)) {
                for (UserStatusBean userStatusBean : list) {
                    reqSet.add(userStatusBean.getEmpNo());
                }
            }
            //获取会议发起人ID
            SysUserBean initiator = reservationUpdateReq.getInitiator();
            String initiatorEmpNo = initiator.getEmpNo();

            // 变更发起人的日程状态
            scheduleService.update(reservationRecord, initiatorEmpNo);

            reqSet.add(initiatorEmpNo);
            //获取删除的参会人员
            Set<String> result = new HashSet<>();
            result.addAll(userSet);
            result.removeAll(reqSet);
            if (result != null && !result.isEmpty()) {
                for (String str : result) {
                    log.info("删除的参会人员:" + str);
                    Optional<ReservationRecordItem> reservationRecordItemOptional = reservationRecordItemDao.findByReservationRecordIdAndOwnerEmpNo(reservationId, str);
                    if (reservationRecordItemOptional.isPresent()) {
                        //获取被删除的参会人员记录
                        ReservationRecordItem recordItem = reservationRecordItemOptional.get();
                        recordItem.setStatus(ReservationRecordItemStatus.CANCEL);
                        recordItem.setIsDelete(1);
                        reservationRecordItemDao.save(recordItem);
                        //给删除的参会人员发送取消会议消息
                        log.info("给删除的参会人员=" + str + ",发送取消会议消息");
                        //会议主题
                        String topic = reservationUpdateReq.getTopic();
                        weChatService.channelRequirementsReminders(recordItem, topic);

                        // 给删除的人取消日程数据
                        scheduleService.cancel(reservationRecord, str);
                    }
                }
            }

            SysUser user = sysUserServiceUC.getUserByEmpNo(initiator.getEmpNo());
            // 更新 预约 item 表记录
            InvitationCard finalCard = card;
            if (Objects.nonNull(reservationUpdateReq.getParticipantList())) {
                reservationUpdateReq.getParticipantList().forEach(it -> {
                    // 判断是否访客
                    if (it.getIsGuest() == 0) {
                        // 根据 item id 和 参会人工号 获取记录
                        Optional<ReservationRecordItem> reservationRecordItemOptional = reservationRecordItemDao.findByReservationRecordIdAndOwnerEmpNo(reservationId, it.getEmpNo());
                        boolean reservationRecordItemOptionalPresent = reservationRecordItemOptional.isPresent();
                        if (reservationRecordItemOptionalPresent) {
                            //过滤到会议发起人
                            if (!it.getEmpNo().equals(reservationUpdateReq.getInitiator().getEmpNo()) && reservationRecordItemOptional.get().getStatus() != it.getStatus()) {
                                //修改参会人状态
                                //若发现是已有人员 更新状态

                                reservationRecordItemList.add(reservationRecordItemOptional.get());
                                //推送给 状态更新人
                                Set<String> changeUserSet = new HashSet<>();
                                changeUserSet.add(it.getEmpNo());
                                log.info("给" + it.getSysId() + "，发送状态变更消息");
                                weChatService.changeMeetingReminders(reservationRecord, changeUserSet);

                                // 给变更的人修改他的日程数据
                                scheduleService.update(reservationRecord, it.getEmpNo());
                            }

                        } else {
                            //若 发现是新增人员 新增参会人 并将状态设置成 UNDETERMINED
                            ReservationRecordItem reservationRecordItem = new ReservationRecordItem();
                            reservationRecordItem.setStatus(ReservationRecordItemStatus.UNDETERMINED);
                            reservationRecordItem.setOwner(it.getSysId());
                            reservationRecordItem.setOwnerEmpNo(it.getEmpNo());
                            reservationRecordItem.setInitiator(reservationUpdateReq.getInitiator().getSysId());
                            reservationRecordItem.setInitiatorEmpNo(reservationUpdateReq.getInitiator().getEmpNo());
                            reservationRecordItem.setReservationRecord(reservationRecord);
                            reservationRecordItem.setIsGroup(it.getIsGroup());
                            reservationRecordItemList.add(reservationRecordItem);
                            //推送给新增人 创建会议推送消息
                            log.info("给新增人" + it.getEmpNo() + "，发送创建会议消息");
                            weChatService.createMeetingReminders(it.getEmpNo(), reservationRecord);

                            // 给新加入的人添加一条日程数据
                            scheduleService.create(reservationRecord, it.getEmpNo());
                        }
                    }
                    // 判断是否是访客
                    if (it.getIsGuest() == 1) {
                        // 根据会议记录和手机号获取访客邀请记录
                        Optional<GuestInvitationRecord> guestOp = recordDao.findByInvitationCardReservationRecordIdAndMobile(reservationId, it.getEmpNo());
                        if (guestOp.isPresent()) {
                            GuestInvitationRecord record = guestOp.get();
                            record.setMobile(it.getEmpNo());
                            record.setName(it.getNameZh());
                            records.add(record);
                        } else {
                            GuestInvitationRecord record = new GuestInvitationRecord();
                            record.setName(it.getNameZh());
                            record.setMobile(it.getEmpNo());
                            record.setGuestInvitationStatus(GuestInvitationStatus.DEFAULT);
                            record.setInvitationCard(finalCard);
                            // 生成访客邀请记录码
                            CRC32 crc32 = new CRC32();
                            crc32.update(UUID.randomUUID().toString().getBytes(StandardCharsets.UTF_8));
                            String value = String.valueOf(crc32.getValue());
                            record.setCode(value);
                            // 给新增人员发送短信
                            smsService.asyncSendMeetingInvitationSMS(user.getNameZh(), user.getMobile(), record.getMobile(), finalCard.getInvitationCode(), record.getCode());
                            records.add(record);
                        }
                    }
                });
            }

            // 变更已选会议组信息
            // 删除之前所以已选
            List<ReservationRecord2CustomGroup> record2CustomGroupList = new ArrayList<>();
            List<ReservationRecord2CustomGroup> byReservationRecord = record2CustomGroupDao.findByReservationRecord(reservationRecord);
            byReservationRecord.forEach(it -> {
                it.setIsDelete(1);
                record2CustomGroupList.add(it);
            });
            if (!reservationUpdateReq.getUserGroupList().isEmpty()) {
                reservationUpdateReq.getUserGroupList().forEach(it -> {
                    Optional<CustomUserGroup> byId = customUserGroupDao.findById(it);
                    if (byId.isPresent()) {
                        ReservationRecord2CustomGroup record2CustomGroup = new ReservationRecord2CustomGroup();
                        record2CustomGroup.setCustomUserGroup(byId.get());
                        record2CustomGroup.setReservationRecord(reservationRecord);
                        record2CustomGroupList.add(record2CustomGroup);
                    }
                });
            }
            record2CustomGroupDao.saveAll(record2CustomGroupList);
            reservationRecordItemDao.saveAll(reservationRecordItemList);
        }
        log.info(String.valueOf(reservationUpdateReq));
        List<String> strings = new ArrayList<>();
        // 删除变更访客人员
        List<GuestDetail> details = new ArrayList<>();
        List<GuestInvitationRecord> recordList = recordDao.findByInvitationCardReservationRecord(reservationRecordOptional.get());
        for (GuestInvitationRecord record : recordList) {
            if (!records.toString().contains(record.getMobile())) {
                record.setIsDelete(1);
                records.add(record);
                if (!StringUtils.isBlank(record.getInviterCode())) {
                    deleteRecord(record, records);
                }
                Optional<GuestDetail> detailOp = detailDao.findByInvitationAndMobile(record.getInvitationCard(), record.getMobile());
                if (detailOp.isPresent()) {
                    GuestDetail detail = detailOp.get();
                    strings.add(detail.getJobNum());
                    detail.setIsDelete(1);
                    details.add(detail);
                }
            }
        }
        detailDao.saveAll(details);
        List<String> userAuths = new ArrayList<>();
        for (String jobNum : strings) {
            for (EhrUserAuth user : ehrUserAuths) {
                if (user.getJobNum().equals(jobNum)) {
                    userAuths.add(user.getVruid());
                }
            }
        }
        // 循环向ehr拒绝访客
        for (String it : userAuths) {
            ServiceResult result = reject(it, token);
            if (!result.isSuccess()) {
                return ServiceResult.error(result.getObject());
            }
        }

        recordDao.saveAll(records);

        // 推送消息给会议App
        intelligentMeetingAppService.pushMeetingRoomDetail(roomId);

        return ServiceResult.ok();
    }

    /**
     * 设置会议预约的会议大屏状态
     *
     * @param roomId
     * @param reservationRecord
     * @param screenStatusInReq
     */
    private void setScreenStatus(String roomId, ReservationRecord reservationRecord, Integer screenStatusInReq) {
        if (ObjectUtils.isNotEmpty(screenStatusInReq) && 1 == screenStatusInReq) {
            if (!intelligentMeetingAppDao.existsByRoomId(roomId)) {
                throw new BizLogicNotMatchException("会议室没有会议大屏，无法开启会议大屏");
            }
            reservationRecord.setScreenStatus(1);
        } else {
            reservationRecord.setScreenStatus(2);
        }
    }

    private void deleteRecord(GuestInvitationRecord record, List<GuestInvitationRecord> records) {
        List<GuestInvitationRecord> recordList1 = recordDao.findByInviterCode(record.getInviterCode());
        for (GuestInvitationRecord record1 : recordList1) {
            record1.setIsDelete(1);
            if (!StringUtils.isBlank(record1.getInviterCode())) {
                this.deleteRecord(record1, records);
            }
            records.add(record);
        }
    }

    // 向ehr拒绝访客
    private ServiceResult reject(String it, String token) {
        VisitorRejectReq rejectReq = new VisitorRejectReq();
        rejectReq.setId(it);
        rejectReq.setVisitorStatus(30);
        String requestParam = JSON.toJSONString(rejectReq);
        log.debug("json参数 ： " + requestParam);
        // 发送Http请求 下发控制命令
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(AUTHTOKEN, token);
        HttpEntity<String> requestEntity = new HttpEntity<>(requestParam, headers);
        ResponseEntity<String> responseEntity;
        try {
            log.debug("拒绝访客 : " + it);
            responseEntity = restTemplate.postForEntity(rejectUrl, requestEntity, String.class);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ServiceResult.error("ehr拒绝访客失败");
        }
        // 解析请求结果
        String responseBody = responseEntity.getBody();
        JSONObject jsonResult = JSON.parseObject(responseBody);
        String codeEhr = jsonResult.getString("code");
        if (!codeEhr.equals("0")) {
            return ServiceResult.error(jsonResult.getString("msg"));
        }
        return ServiceResult.ok();
    }

    // 更新邀请函时间
    private ServiceResult updateVisitorTime(String id, InvitationCard card, ReservationUpdateReq reservationUpdateReq, String token) {
        InvitationUpdateTimeReq timeReq = new InvitationUpdateTimeReq();
        BeanUtils.copyProperties(card, timeReq);
        timeReq.setId(id);
        timeReq.setEndTime(DateUtils.formatLocalDateTime(reservationUpdateReq.getEndTime(), DateUtils.PATTERN_DATETIME1));
        timeReq.setStartTime(DateUtils.formatLocalDateTime(reservationUpdateReq.getStartTime(), DateUtils.PATTERN_DATETIME1));
        String requestParam1 = JSON.toJSONString(timeReq);
        log.debug("json参数 ： " + requestParam1);
        // 发送Http请求 下发控制命令
        HttpHeaders headers1 = new HttpHeaders();
        headers1.setContentType(MediaType.APPLICATION_JSON);
        headers1.add(AUTHTOKEN, token);
        HttpEntity<String> requestEntity1 = new HttpEntity<>(requestParam1, headers1);
        ResponseEntity<String> responseEntity1;
        try {
            responseEntity1 = restTemplate.postForEntity(updateTimeUrl, requestEntity1, String.class);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ServiceResult.error("ehr更新邀请函失败");
        }
        // 解析请求结果
        String responseBody1 = responseEntity1.getBody();
        JSONObject jsonResult1 = JSON.parseObject(responseBody1);
        log.debug("ehr更新邀请函请求结果{}", jsonResult1);
        String codeEhr1 = jsonResult1.getString("code");
        if (!codeEhr1.equals("0")) {
            return ServiceResult.error(jsonResult1.getString("msg"));
        }
        return ServiceResult.ok();
    }

    // 整单更新邀请函
    private ServiceResult updateVisitor(ReservationUpdateReq reservationUpdateReq, String token, EhrVisitor ehrVisitor) {

        InvitationUpdateReq timeReq = new InvitationUpdateReq();
        timeReq.setAddress(ehrVisitor.getAddress());
        timeReq.setAssginee(ehrVisitor.getAssginee());
        timeReq.setCompanyId(ehrVisitor.getCompanyId());
        timeReq.setEndTime(DateUtils.formatLocalDateTime(reservationUpdateReq.getEndTime(), DateUtils.PATTERN_DATETIME1));
        timeReq.setMeeting(null);
        timeReq.setMeetingOperate(2);
        timeReq.setReasonCode(ehrVisitor.getReasonCode());
        timeReq.setRemarks(ehrVisitor.getRemarks());
        timeReq.setRoleCode(ehrVisitor.getRoleCode());
        timeReq.setStartTime(DateUtils.formatLocalDateTime(reservationUpdateReq.getStartTime(), DateUtils.PATTERN_DATETIME1));
        timeReq.setVisitorRecordNo(ehrVisitor.getVisitorRecordNo());

        String requestParam1 = JSON.toJSONString(timeReq);
        log.debug("json参数 ： " + requestParam1);
        // 发送Http请求 下发控制命令
        HttpHeaders headers1 = new HttpHeaders();
        headers1.setContentType(MediaType.APPLICATION_JSON);
        headers1.add(AUTHTOKEN, token);
        HttpEntity<String> requestEntity1 = new HttpEntity<>(requestParam1, headers1);
        ResponseEntity<String> responseEntity1;
        try {
            responseEntity1 = restTemplate.postForEntity(updateUrl, requestEntity1, String.class);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ServiceResult.error("ehr更新邀请函失败");
        }
        // 解析请求结果
        String responseBody1 = responseEntity1.getBody();
        JSONObject jsonResult1 = JSON.parseObject(responseBody1);
        log.debug("ehr更新邀请函请求结果{}", jsonResult1);
        String codeEhr1 = jsonResult1.getString("code");
        if (!codeEhr1.equals("0")) {
            return ServiceResult.error(jsonResult1.getString("msg"));
        }
        return ServiceResult.ok();
    }

    // 查询邀请函信息
    private ServiceResult findVistitor(InvitationCard card, String token) {
        // 获取ehr邀请函下所有访客
        InvitationDetailReq detailReq = new InvitationDetailReq();
        detailReq.setVisitorRecordNo(card.getVisitorRecordNo());
        String requestParam = JSON.toJSONString(detailReq);
        log.debug("json参数 ： " + requestParam);
        // 发送Http请求 下发控制命令
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(AUTHTOKEN, token);
        HttpEntity<String> requestEntity = new HttpEntity<>(requestParam, headers);
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = restTemplate.postForEntity(detailUrl, requestEntity, String.class);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ServiceResult.error("查询邀请函失败");
        }
        // 解析请求结果
        String responseBody = responseEntity.getBody();
        JSONObject jsonResult = JSON.parseObject(responseBody);
        log.debug("查询邀请函结果{}", jsonResult);
        String codeEhr = jsonResult.getString("code");
        if (!codeEhr.equals("0")) {
            return ServiceResult.error(jsonResult.getString("msg"));
        }
        JSONObject jsonData = jsonResult.getJSONObject("page");
        VisitorResp visitorResp = JSON.toJavaObject(jsonData, VisitorResp.class);
        return ServiceResult.ok(visitorResp);
        // 用户List
        /*String jsonIndex = jsonData.getString("userAuthList");
        ehrUserAuths = JSONArray.parseArray(jsonIndex, EhrUserAuth.class);
        // 邀请函信息
        JSONObject recordInfoDto = jsonData.getJSONObject("recordInfoDto");
        JSON.toJavaObject(recordInfoDto, EhrVisitor.class);*/
    }

    @Override
    @Transactional
    public PageBean findByHrId(String hrId, String projectId, LocalDate date, PageBean pageBean) {
        //todo: step0 hrId 处理业务
        //step1: 查询 Room 分页数据(按 seq 排序)
        // 获取会议室id列表
        List<String> roomIds = getRoomIds(projectId);
        if(CollUtil.isEmpty(roomIds)){
            return new PageBean(pageBean.getPage(), pageBean.getSize());
        }
        // 会议室根据用户的使用时间进行降序排列
        roomSortByUserTimDesc(hrId, roomIds);

        int roomListSize = roomIds.size();
        int page = pageBean.getPage();
        int size = pageBean.getSize();

        // 手动对查询结果进行分页
        List<String> needReturnRoomIds = pageRoomIds(roomIds, roomListSize, page, size);

        List<Room> pageList = getRoomListFirstByRedis(needReturnRoomIds);


        // 循环处理room
        List<RoomWithReservationResp> roomRespList = new ArrayList<>();

        List<String> returnRoomIds = pageList.stream().map(Room::getId).collect(Collectors.toList());
        if(CollUtil.isEmpty(roomIds)){
            return new PageBean(pageBean.getPage(), pageBean.getSize());
        }
        List<SimpleRoomDevice> simpleRoomDeviceList = roomDeviceDao.findSimpleRoomDeviceByRoomIds(returnRoomIds);
        MultiValueMap<String, String> roomIdToDeviceNameListMap = new LinkedMultiValueMap<>();
        simpleRoomDeviceList.forEach(roomDevice-> roomIdToDeviceNameListMap.add(roomDevice.getRoomId(), roomDevice.getName()));
        for (Room room : pageList) {
            // 封装为 resp
            RoomWithReservationResp roomResp = new RoomWithReservationResp();
            BeanUtils.copyProperties(room, roomResp);
            roomResp.setLocation(SpaceService.getFullSpaceName(room.getSpace(), room.getSpace().getName()));
            String roomId = room.getId();
            roomResp.setRoomId(roomId);
            //设置日期
            roomResp.setDate(date);
            List<String> deviceNameList = roomIdToDeviceNameListMap.get(roomId);
            if(Objects.isNull(deviceNameList)) {
                deviceNameList = new ArrayList<>();
            }
            deviceNameList.sort(ObjectUtils::compare);
            roomResp.setDeviceList(deviceNameList);
            roomResp.setTimeSlotResultRespList(getBlankTimeSlotList(roomId, date));
            roomResp.getTimeSlotResultRespList().forEach(obj -> obj.setDate(date));
            roomRespList.add(roomResp);
        }

        //step2: timeSlotDao 查询 timeSlot (半小时间隔)
        //按房间循环
        List<SimpleTimeSlot> simpleTimeSlots = timeSlotDao.findSimpleTimeSlotByRoomIds(returnRoomIds);
        MultiValueMap<String, SimpleTimeSlot> roomIdToTimeSlotMap = new LinkedMultiValueMap<>();
        simpleTimeSlots.forEach(timeSlot-> roomIdToTimeSlotMap.add(timeSlot.getRoomId(), timeSlot));
        for (RoomWithReservationResp roomResp : roomRespList) {
            List<SimpleTimeSlot> simpleTimeSlotList = roomIdToTimeSlotMap.get(roomResp.getRoomId());
            if(CollUtil.isNotEmpty(simpleTimeSlotList)){
                setStatusByTimeSlot(roomResp, simpleTimeSlotList.stream().map(s->{
                    TimeSlot timeSlot = new TimeSlot();
                    timeSlot.setFrom(s.getFrom());
                    timeSlot.setTo(s.getTo());
                    return timeSlot;
                }).collect(Collectors.toList()));
            }
        }

        //step3: 查询标记 会议室已预约
        //按房间循环
        List<SimpleReservationRecord> simpleReservationRecords = reservationRecordDao.findByDateAndRoomIdIdsOrderByFromAsc(date, returnRoomIds);
        if(CollUtil.isNotEmpty(simpleReservationRecords)){
            MultiValueMap<String, SimpleReservationRecord> roomIdToReservationRecordMap = new LinkedMultiValueMap<>();
            List<String> initiatorEmpNoList = simpleReservationRecords.stream().map(SimpleReservationRecord::getInitiatorEmpNo)
                .filter(StringUtils::isNotBlank).distinct().collect(Collectors.toList());
            Map<String, SimpleSysUser> empNoToUseMap=new HashMap<>();
            if(CollUtil.isNotEmpty(initiatorEmpNoList)) {
                List<SimpleSysUser> simpleSysUsers = sysUserDao.findSimpleSysUserByEmpNos(initiatorEmpNoList);
                simpleSysUsers.forEach(s -> empNoToUseMap.put(s.getEmpNo(), s));
            }
            simpleReservationRecords.forEach(reservationRecord-> roomIdToReservationRecordMap.add(reservationRecord.getRoomId(), reservationRecord));
            for (RoomWithReservationResp roomResp : roomRespList) {
                setStatusByReservationRecordSet(roomResp, roomIdToReservationRecordMap.get(roomResp.getRoomId()), empNoToUseMap);
            }
        }
        //当前页码
        pageBean.setPage(pageBean.getPage());
        //每页记录数
        pageBean.setSize(pageBean.getSize());
        pageBean.setTotalElements(roomListSize);
        pageBean.setTotalPages(roomListSize / size + 1);
        pageBean.setList(roomRespList);

        return pageBean;
    }

    /**
     * 会议室id列表手动分页
     *
     * @param roomIds
     * @param roomListSize
     * @param page
     * @param size
     * @return
     */
    private List<String> pageRoomIds(List<String> roomIds, int roomListSize, int page, int size) {
        int subStart = page * size;
        int subEnd = subStart + size;

        if (subStart > roomListSize) {
            subStart = roomListSize;
        }

        if (subEnd > roomListSize) {
            subEnd = roomListSize;
        }

        return roomIds.subList(subStart, subEnd);
    }

    /**
     * 获取会议室id列表
     *
     * @param projectId
     * @return
     */
    private List<String> getRoomIds(String projectId) {
        List<String> roomIds;
        if(StringUtils.isNotBlank(projectId)){
            List<String> spaceIds = spaceDao.findIdByProjectId(projectId);
            if(CollUtil.isNotEmpty(spaceIds)){
                roomIds = roomDao.findIdBySpaceIdInOrderSeqAsc(spaceIds);
            }else {
                roomIds = new ArrayList<>();
            }
        }else {
            roomIds = roomDao.findAllIdOrderBySeqAsc();
        }
        return roomIds;
    }

    /**
     * 会议室根据用户的使用时间进行降序排列
     *
     * @param hrId
     * @param roomIds
     */
    private void roomSortByUserTimDesc(String hrId, List<String> roomIds) {
        if(CollUtil.isEmpty(roomIds)|| roomIds.size() < 2){
            return;
        }
        String key = redisUtil.createRoomUsedTimeKey(IConst.ROOM_USED_TIME_MODULE, hrId, RedisUtil.Separator.COLON);
        List<String> sortedRoomIds = redisUtil.zSReverseRange(key, 0, -1, String.class);
        roomIds.sort((r1, r2)->{
             int r1Index = sortedRoomIds.indexOf(r1);
             int r2Index = sortedRoomIds.indexOf(r2);
             if(r1Index == r2Index){
                 return 0;
             }else {
                 if(r1Index < 0){
                     return  1;
                 }
                 if(r2Index < 0){
                     return  -1;
                 }
                 return ObjectUtils.compare(r1Index , r2Index);
             }
        });
    }

    /**
     * 优先从redis批量查询会议室列表
     *
     * @param roomIds
     * @return
     */
    private List<Room> getRoomListFirstByRedis(List<String> roomIds) {
        if(CollUtil.isEmpty(roomIds)){
            return new ArrayList<>();
        }
        List<String> keys = roomIds.stream().map(roomId -> redisUtil.createRoomKey(IConst.ROOM_MODULE, roomId, RedisUtil.Separator.COLON))
                .collect(Collectors.toList());
        List<Object> objects = redisUtil.multiGet(keys);
        Map<String,Room> idToRoomMap = objects.stream().map(room-> JSON.parseObject(JSON.toJSONString(room), Room.class)).collect(Collectors.toMap(Room::getId, r->r));
        List<Room> list = new ArrayList<>();
        roomIds.forEach(roomId->{
            Room room = idToRoomMap.get(roomId);
            if(Objects.nonNull(room)){
                list.add(room);
            }else {
                Optional<Room> roomOptional = roomDao.findById(roomId);
                roomOptional.ifPresent(r->{
                    list.add(r);
                    redisUtil.set(redisUtil.createRoomKey(IConst.ROOM_MODULE, roomId, RedisUtil.Separator.COLON), r, RedisUtil.TWO_DAY_SECOND);
                });
            }
        });
        return list;
    }

    /**
     * 设置会议室的时间段 是否已经预定
     *
     * @param roomResp
     * @param reservationRecordList
     */
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void setStatusByReservationRecordSet(RoomWithReservationResp roomResp,
                                                List<ReservationRecord> reservationRecordList) {
        // 预约记录循环
        for (ReservationRecord rr : reservationRecordList) {
            // 预约会议室,被取消的,不计算入资源占用
            if (CANCEL.equals(rr.getStatus())) {
                continue;
            }
            // 取出每个预约记录的开始,结束时间
            String from = rr.getFrom().toString().substring(11, 16);
            String to = rr.getTo().toString().substring(11, 16);
            //将 ReservationRecord 开始 - 结束时间,按半小时进行拆分
            List<TimeSlotBean> timeSlotBeanList = DateUtil.splitDate(from, to);
            //按  TimeSlotBean 循环
            for (TimeSlotBean tsb : timeSlotBeanList) {
                roomResp.getTimeSlotResultRespList()
                        .stream()
                        .filter(it ->
                                it.getDate().equals(rr.getDate())
                                        && it.getFrom().equals(tsb.getFrom())
                                        && it.getTo().equals(tsb.getTo()))
                        .forEach(it -> {
                            //如果ts可用
                            it.setStatus(2);
                            it.setRecordId(rr.getId());
                            it.setTopic(rr.getTopic());
                            //设置预定人员信息
                            String initiatorEmpNo = rr.getInitiatorEmpNo();
                            Optional<SysUser> sysUserOptional = sysUserDao.findByEmpNo(initiatorEmpNo);
                            if (sysUserOptional.isPresent()) {
                                SysUser sysUser = sysUserOptional.get();
                                EhrStaffResp ehrStaff = new EhrStaffResp();
                                ehrStaff.setSysId(sysUser.getSysId());
                                ehrStaff.setHrId(initiatorEmpNo);
                                ehrStaff.setStDeptName(sysUser.getFullDepartmentName());
                                BeanUtils.copyProperties(sysUser, ehrStaff);
                                it.setEhrStaff(ehrStaff);
                            }
                        });
            }
        }
    }

    /**
     * 设置会议室的时间段 是否已经预定
     *
     * @param roomResp
     * @param reservationRecordList
     */
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void setStatusByReservationRecordSet(RoomWithReservationResp roomResp,
                                                List<SimpleReservationRecord> reservationRecordList, Map<String, SimpleSysUser> empNoToUseMap) {
        if(CollUtil.isEmpty(reservationRecordList)){
            return;
        }
        Map<String,Object> hrIdToUserMap = new HashMap<>();
        // 预约记录循环
        for (SimpleReservationRecord rr : reservationRecordList) {
            // 预约会议室,被取消的,不计算入资源占用
            if (CANCEL.equals(rr.getStatus())) {
                continue;
            }
            // 取出每个预约记录的开始,结束时间
            String from = rr.getFrom().toString().substring(11, 16);
            String to = rr.getTo().toString().substring(11, 16);
            //将 ReservationRecord 开始 - 结束时间,按半小时进行拆分
            List<TimeSlotBean> timeSlotBeanList = DateUtil.splitDate(from, to);
            //按  TimeSlotBean 循环
            for (TimeSlotBean tsb : timeSlotBeanList) {
                roomResp.getTimeSlotResultRespList()
                        .stream()
                        .filter(it ->
                                it.getDate().equals(rr.getDate())
                                        && it.getFrom().equals(tsb.getFrom())
                                        && it.getTo().equals(tsb.getTo()))
                        .forEach(it -> {
                            //如果ts可用
                            it.setStatus(2);
                            it.setRecordId(rr.getId());
                            it.setTopic(rr.getTopic());
                            //设置预定人员信息
                            String initiatorEmpNo = rr.getInitiatorEmpNo();
                            Object user =  hrIdToUserMap.get(initiatorEmpNo);
                            if(Objects.isNull(user)){
                                SimpleSysUser sysUser = empNoToUseMap.get(initiatorEmpNo);
                                if (Objects.nonNull(sysUser)) {
                                    EhrStaffResp ehrStaff = SimpleSysUser.convert(sysUser);
                                    ehrStaff.setSysId(sysUser.getSysId());
                                    ehrStaff.setHrId(initiatorEmpNo);
                                    ehrStaff.setStDeptName(sysUser.getFullDepartmentName());
                                    it.setEhrStaff(ehrStaff);
                                    hrIdToUserMap.put(initiatorEmpNo, ehrStaff);
                                }else {
                                    hrIdToUserMap.put(initiatorEmpNo, StringUtils.EMPTY);
                                }
                            }else {
                                if (!StringUtils.EMPTY.equals(user.toString())) {
                                    it.setEhrStaff((EhrStaffResp) user);
                                }
                            }
                        });
            }
        }
    }

    /**
     * 设置会议室的 timeSlot 状态
     *
     * @param roomResp
     * @param timeSlotList
     */
    private void setStatusByTimeSlot(RoomWithReservationResp roomResp, List<TimeSlot> timeSlotList) {
        if(CollUtil.isEmpty(timeSlotList)){
            return;
        }
        for (TimeSlot timeSlot : timeSlotList) {
            //根据房间开放时间 timeSlot 判断状态
            String from = timeSlot.getFrom();
            String to = timeSlot.getTo();
            roomResp.getTimeSlotResultRespList().stream()
                    .filter(tsResp ->
                            tsResp.getFrom().equals(from) && tsResp.getTo().equals(to))
                    .forEach(tsResp -> tsResp.setStatus(1));
        }

    }

    private List<TimeSlotResultResp> getBlankTimeSlotList(String roomId) {
        List<TimeSlotResultResp> timeSlotResultRespList = new ArrayList<>();
        // 从小时
        int start = 6;
        // 到小时
        int max = 24;
        for (int i = start; i < max; i++) {
            String half1 = i + "";
            int half2 = i + 1;
            String half2Str = half2 + "";
            if (String.valueOf(i).length() == 1) {
                half1 = "0" + half1;
            }
            if (String.valueOf(half2).length() == 1) {
                half2Str = "0" + half2Str;
            }
            TimeSlotResultResp ts1 = new TimeSlotResultResp(half1 + ":00", half1 + ":30", 0);
            ts1.setRoomId(roomId);
            TimeSlotResultResp ts2 = new TimeSlotResultResp(half1 + ":30", half2Str + ":00", 0);
            ts2.setRoomId(roomId);

            timeSlotResultRespList.add(ts1);
            timeSlotResultRespList.add(ts2);
        }
        return timeSlotResultRespList;
    }

    private List<TimeSlotResultResp> getBlankTimeSlotList(String roomId, LocalDate date) {
        List<TimeSlotResultResp> timeSlotResultRespList = new ArrayList<>();
        // 从小时
        int start = 6;
        // 到小时
        int max = 24;
        for (int i = start; i < max; i++) {
            String half1 = i + "";
            int half2 = i + 1;
            String half2Str = half2 + "";
            if (String.valueOf(i).length() == 1) {
                half1 = "0" + half1;
            }
            if (String.valueOf(half2).length() == 1) {
                half2Str = "0" + half2Str;
            }
            TimeSlotResultResp ts1 = new TimeSlotResultResp(half1 + ":00", half1 + ":30", 0);
            ts1.setRoomId(roomId);
            TimeSlotResultResp ts2 = new TimeSlotResultResp(half1 + ":30", half2Str + ":00", 0);
            ts2.setRoomId(roomId);
            ts1.setDate(date);
            ts2.setDate(date);
            timeSlotResultRespList.add(ts1);
            timeSlotResultRespList.add(ts2);
        }
        return timeSlotResultRespList;
    }


    /**
     * 创建会议室空白时间槽
     *
     * @param roomId    会议室id
     * @param startDate 时间槽开始日期
     * @param endDate   时间槽结束日期
     * @return 时间槽列表
     * @author Yang.Lee
     * @date 2022/4/20 17:37
     **/
    private List<TimeSlotResultResp> getBlankTimeSlotList(String roomId, LocalDateTime startDate, LocalDateTime endDate) {

        // 计算需要获取几天的时间槽
        long days = DateUtils.betweenTime(startDate, endDate, DateUnit.DAY);

        // 查询该会议室的开放时间
        List<TimeSlot> timeSlotsSet = timeSlotDao.findByRoomIdOrderBySeqAsc(roomId);

        // 创建时间槽列表
        List<TimeSlotResultResp> timeSlotResultRespList = new ArrayList<>();

        // 进行数据创建
        // 时间槽日期
        LocalDate date = startDate.toLocalDate();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DateUtils.PATTERN_TIME1);

        for (int i = 0; i <= days; i++) {
            // List<TimeSlot> 转成 List<TimeSlotResultResp>
            LocalDate finalDate = date;
            List<TimeSlotResultResp> list = timeSlotsSet.stream()
                    .filter(obj -> {

                        // 进行时间段筛选
                        LocalTime fromTime = LocalTime.parse(obj.getFrom(), formatter);
                        LocalTime toTime = LocalTime.parse(obj.getTo(), formatter);

                        LocalDateTime fromDateTime = LocalDateTime.of(finalDate, fromTime);
                        LocalDateTime toDateTime = LocalDateTime.of(finalDate, toTime);

                        if (DateUtils.isTimeInRange(fromDateTime, toDateTime, startDate, endDate) == 1) {
                            return true;
                        } else {
                            return false;
                        }

                    })
                    .map(obj -> new TimeSlotResultResp(obj.getFrom(), obj.getTo(), finalDate, 0))
                    .collect(Collectors.toList());
            timeSlotResultRespList.addAll(list);
            // 计数器及日期+1
            date = date.plusDays(1);
        }
//
//        do {
//            // List<TimeSlot> 转成 List<TimeSlotResultResp>
//            List<TimeSlotResultResp> list = timeSlotsSet.stream()
//                    .filter(obj -> {
//
//                        // 进行时间段筛选
//                        LocalTime fromTime = LocalTime.parse(obj.getFrom(), formatter);
//                        LocalTime toTime = LocalTime.parse(obj.getTo(), formatter);
//
//                        LocalDateTime fromDateTime = LocalDateTime.of(date, fromTime);
//                        LocalDateTime toDateTime = LocalDateTime.of(date, toTime);
//
//                        if (DateUtils.isTimeInRange(fromDateTime, toDateTime, startDate, endDate) == 1) {
//                            return true;
//                        } else {
//                            return false;
//                        }
//
//                    })
//                    .map(obj -> new TimeSlotResultResp(obj.getFrom(), obj.getTo(), date, 0))
//                    .collect(Collectors.toList());
//            timeSlotResultRespList.addAll(list);
//            // 计数器及日期+1
//            times++;
//            date.plusDays(1);
//        } while (times <= days);


        return timeSlotResultRespList;
    }

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public PageBean<ReservationRecordBean> getReservationRecordItemPage(ReservationGetReq reservationGetReq) {
        // 基于ReservationRecordBean 创建一个空列表
        List reservationRecordItemList = new ArrayList<>();

        Map<String, Map<String, List<ReservationRecordBean>>> map = new TreeMap(Comparator.reverseOrder());
        // 获取参数
        Integer page = reservationGetReq.getPage();
        Integer size = reservationGetReq.getSize();
        // 分页
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "r.r_from"));

        // 参会人
        String ownerEmpNo = reservationGetReq.getEmpNo();
        String topicLike = "";
        List<String> topicLikeOrRoomIds = null;
        String from = "";
        String to = "";
        List<String> roomIds = null;

        // 会议标题/会议室名称模糊查
        if (StringUtils.isNotBlank(reservationGetReq.getKeyword())) {
            topicLike = "%" + reservationGetReq.getKeyword() + "%";
            topicLikeOrRoomIds = roomDao.findIdByNameLike(topicLike);
            if(CollUtil.isEmpty(topicLikeOrRoomIds)) {
                topicLikeOrRoomIds = null;
            }
        }
        // 会议记录日期范围
        if (StringUtils.isNotBlank(reservationGetReq.getStart()) && StringUtils.isNotBlank(reservationGetReq.getEnd())) {
            from = reservationGetReq.getStart() + " 00:00:00";
            to = reservationGetReq.getEnd() + " 23:59:59";
        }

        // 项目ID
        if (StringUtils.isNotBlank(reservationGetReq.getProjectId())) {
            // 获取会议室id列表
             roomIds = getRoomIds(reservationGetReq.getProjectId());
            if(CollUtil.isEmpty(roomIds)){
                return PageBean.createPageBeanByAllData(new ArrayList<>(), page, size);
            }else {
                roomIds = null;
            }
        }

        Page<Map<String,Object>> selectPage = reservationRecordItemDao.selectPage(ownerEmpNo, topicLike, topicLikeOrRoomIds, from ,to ,roomIds, pageable);


        PageBean<ReservationRecordBean> pageBean = new PageBean<>();
        // 基于ReservationRecordBean 创建一个空列表
        //根据消息 for 循环

        String inProcess = "inProcess";
        String free = "free";
        String finished = "finished";
        String canceled = "canceled";

        // 批量获取发起人的姓名
        Map<String,String> empNoToNameMap = sysUserServiceUC.batchGetNameByEmpNos(selectPage.getContent().stream()
                .map(o-> getMapStringValue(o, "initiatorEmpNo")).distinct().collect(Collectors.toList()));
        List<ReservationRecordBean> reservationRecordBeanList = new ArrayList<>();
        selectPage.getContent().forEach(it -> {

            //创建一个新的 ReservationRecordBean 单元
            ReservationRecordBean reservationRecordBean = new ReservationRecordBean();
            //获取会议主题
            reservationRecordBean.setTopic(getMapStringValue(it, "topic"));
            //获取会议开始时间
            reservationRecordBean.setTimeFrom(getMapLocalDateTimeValue(it, "rFrom"));
            //获取会议结束时间
            reservationRecordBean.setTimeTo(getMapLocalDateTimeValue(it, "rTo"));
            // 会议室id
            reservationRecordBean.setRoomId(getMapStringValue(it, "roomId"));
            //获取会议发起人id
            reservationRecordBean.setInitiator(getMapStringValue(it, "initiator"));
            reservationRecordBean.setInitiatorEmpNo(getMapStringValue(it, "initiatorEmpNo"));
            //获取会议室发起人姓名
            reservationRecordBean.setInitiatorName(empNoToNameMap.get(getMapStringValue(it, "initiatorEmpNo")));
            //设置人员状态
            reservationRecordBean.setUserStatus(getMapStringValue(it, "status"));
            //获取会议状态
            ReservationRecordStatus reservationSqlStatus = ReservationRecordStatus.valueOf(getMapStringValue(it, "rStatus"));

            String belong = null;
            if (reservationSqlStatus.equals(CANCEL)) {
                //若会议已取消 直接设置状态为CANCEL
                reservationRecordBean.setReservationRecordStatus(CANCEL);
                belong = canceled;
            } else {
                //会议未取消
                LocalDateTime localDateTimeNow = LocalDateTime.now();
                LocalDateTime timeFrom = reservationRecordBean.getTimeFrom();
                LocalDateTime timeTo = reservationRecordBean.getTimeTo();
                //若当前时间未到达会议开始 状态为DEFAULT
                if (localDateTimeNow.isBefore(timeFrom)) {
                    reservationRecordBean.setReservationRecordStatus(DEFAULT);
                    belong = free;
                }
                //若当前时间大于会议结束时间 状态为FINISH
                else if (localDateTimeNow.isAfter(timeTo)) {
                    reservationRecordBean.setReservationRecordStatus(FINISH);
                    belong = finished;
                }
                //否则 当前时间在会议开始时间和会议结束时间之间  状态设置为PENDING
                else {
                    reservationRecordBean.setReservationRecordStatus(PENDING);
                    belong = inProcess;
                }
            }

            //获取会议备注
            reservationRecordBean.setRemark(getMapStringValue(it, "remark"));
            reservationRecordBean.setId(getMapStringValue(it, "rId")) ;
            //判断是否为本人发起 0否 1是
            if (reservationRecordBean.getInitiatorEmpNo().equals(reservationGetReq.getEmpNo())) {
                reservationRecordBean.setIsOwn(1);
                reservationRecordBean.setService(getMapStringValue(it, "service"));
            }

            DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate date = ((java.sql.Date) it.get("rDate")).toLocalDate();
            String localTime = df.format(date);


            if (!map.containsKey(localTime)) {

                Map<String, List<ReservationRecordBean>> beanMap = new TreeMap(Comparator.reverseOrder());
                beanMap.put(inProcess, new ArrayList<>());
                beanMap.put(finished, new ArrayList<>());
                beanMap.put(canceled, new ArrayList<>());
                beanMap.put(free, new ArrayList<>());
                map.put(localTime, beanMap);


            }

            map.get(localTime).get(belong).add(reservationRecordBean);
            reservationRecordBeanList.add(reservationRecordBean);
        });
        List<String> returnRoomIds = reservationRecordBeanList.stream().filter(r->Objects.nonNull(r.getRoomId())).map(ReservationRecordBean::getRoomId)
                .distinct().collect(Collectors.toList());
        List<Room> roomList = getRoomListFirstByRedis(returnRoomIds);
        Map<String, Room> roomIdToRoomMap = roomList.stream().collect(Collectors.toMap(Room::getId, r->r));
        reservationRecordBeanList.forEach(reservationRecordBean -> {
            if(StringUtils.isNotBlank(reservationRecordBean.getRoomId())){
                Room room = roomIdToRoomMap.get(reservationRecordBean.getRoomId());
                if(Objects.nonNull(room)){
                    //获取会议室名称
                    reservationRecordBean.setRoomName(room.getName());
                    //获取会议室楼栋
                    reservationRecordBean.setRoomBuilding(room.getBuilding());
                    //获取会议室楼层
                    reservationRecordBean.setFloor(room.getFloor());
                    reservationRecordBean.setLocation(SpaceService.getFullSpaceName(room.getSpace(), room.getSpace().getName()));
                }
            }
        });

        reservationRecordItemList.add(map);
        // 获取当前页码
        pageBean.setPage(page);
        //每页记录数
        pageBean.setSize(size);
        //总记录数
        pageBean.setTotalElements(selectPage.getTotalElements());
        //总页数
        pageBean.setTotalPages(selectPage.getTotalPages());
        //将reservationRecordItemList 写入pageBean 的 list中
        pageBean.setList(reservationRecordItemList);
        return pageBean;
    }

    /**
     * 获取map的LocalDateTime value
     * @param map
     * @param key
     * @return
     */
    private LocalDateTime getMapLocalDateTimeValue(Map<String, Object> map, String key) {
        Object o = map.get(key);
        if(Objects.isNull(o)){
            return null;
        }
        return ((Timestamp) o).toLocalDateTime();
    }

    /**
     * 获取map的string value
     * @param map
     * @param key
     * @return
     */
    private String getMapStringValue(Map<String, Object> map, String key) {
        Object o = map.get(key);
        if(Objects.isNull(o)){
            return null;
        }
        return (String) o;
    }

    @Override
    @Transactional
    public RoomWithReservationResp findBySysUserAndRoomId(String empNo, String roomId, LocalDate date) {
        //todo: step0 根据empNo 处理业务
        Optional<Room> roomOptional = roomDao.findById(roomId);
        // 封装为 resp
        RoomWithReservationResp roomResp = new RoomWithReservationResp();
        if (roomOptional.isPresent()) {
            Room room = roomOptional.get();
            //step1: 查询和设置所有时间段
            BeanUtils.copyProperties(room, roomResp);
            roomResp.setLocation(SpaceService.getFullSpaceName(room.getSpace(), room.getSpace().getName()));
            roomResp.setRoomId(roomId);
            //设置日期
            roomResp.setDate(date);
            //todo:添加设备
            List<TimeSlotResultResp> timeSlotResultRespList = getBlankTimeSlotList(roomId);

            timeSlotResultRespList.forEach(obj -> obj.setDate(date));
            roomResp.setTimeSlotResultRespList(timeSlotResultRespList);

            log.debug("add roomResp:" + roomResp);


            //step2: 查询可用 timeSlot (半小时间隔)
            List<TimeSlot> timeSlotList = timeSlotDao.findByRoomIdOrderBySeqAsc(roomId);
            setStatusByTimeSlot(roomResp, timeSlotList);

            //当前时间之前的时间段不可预约

            if (LocalDate.now().isBefore(date)) {
                for (TimeSlotResultResp resultResp : roomResp.getTimeSlotResultRespList()) {
                    resultResp.setUsed(true);
                }
            } else {
                for (TimeSlotResultResp resultResp : roomResp.getTimeSlotResultRespList()) {
                    // 2021-01-06: 根据新需求，会议室预约时可选择当前时间半小时前的时段
                    long nowTs = System.currentTimeMillis();
                    long fromTs = slip(resultResp.getFrom()).getTime();

                    resultResp.setUsed(fromTs - nowTs >= -1800000);
                }
            }

            //step3: 查询 已预约
            List<ReservationRecord> reservationRecordList = reservationRecordDao.findByDateAndRoomIdOrderByFromAsc(date, roomId);
            setStatusByReservationRecordSet(roomResp, reservationRecordList);
        }


        return roomResp;
    }

    @Override
    @Transactional
    public ServiceResult getReservationRecordDetails(String ownerEmpNo, String recordId, String token) {
        ReservationRecordDetailsResp reservationRecordDetailsResp = new ReservationRecordDetailsResp();

        // 判断会议记录时分存在
        if (reservationRecordDao.findById(recordId).isEmpty()) {
            return ServiceResult.error("id有误,会议记录不存在", ErrConst.E01);
        }

        /*//判断查看人员是否为参会人员
        List<ReservationRecordItem> reservationRecordItemList = reservationRecordItemDao.findByReservationRecordIdOrderByOwner(recordId);
        for (ReservationRecordItem recordItem : reservationRecordItemList) {
            if (recordItem.getOwnerEmpNo().equals(ownerEmpNo)) {*/
        //根据会议编号 与 查询人id 查找会议详情
        Optional<ReservationRecordItem> reservationRecordItemOptional = reservationRecordItemDao.findByReservationRecordIdAndOwnerEmpNo(recordId, ownerEmpNo);

        // 根据会议编号 获取会议
        Set<ReservationRecordItem> byReservationRecordId = reservationRecordItemDao.findByReservationRecordId(recordId);
        // 参会人list
        List<ParticipantBean> participantBeanList = getParticipantBeanList(byReservationRecordId);
        if (reservationRecordItemOptional.isPresent()) {

            ReservationRecordItem reservationRecordItem = reservationRecordItemOptional.get();

            // 邀请卡信息
            Optional<InvitationCard> op = invitationCardDao.findByReservationRecord(reservationRecordItem.getReservationRecord());
            if (op.isPresent()) {
                        /*// 获取ehr邀请函信息
                        ServiceResult result = findVistitor(op.get(), token);
                        if(result.isSuccess()){
                            // 响应信息
                            VisitorResp visitorResp = (VisitorResp)result.getObject();
                            // 如果ehr邀请函取消则删除邀请卡及访客
                            if("0".equals(visitorResp.getRecordInfoDto().getStatus())){
                                // 删除邀请函及所有信息
                                // ServiceResult result1 = cardService.cancel(op.get());
                                *//*if(result1.isSuccess()){
                                    reservationRecordDetailsResp.setCardResp(null);
                                }
                            }else {*//*
                                ServiceResult detail = cardService.getDetail(op.get().getInvitationCode() ,null);
                                if(detail.isSuccess()){
                                    // 邀请含信息
                                    reservationRecordDetailsResp.setCardResp((InvitationCardResp)detail.getObject());
                                    // 访客list
                                    List<ParticipantBean> guestList = getGuest(recordItem.getReservationRecord());
                                    participantBeanList.addAll(guestList);
                                }
                            }else {*/
                ServiceResult detail = cardService.getDetail(op.get().getInvitationCode(), null);
                if (detail.isSuccess()) {
                    // 邀请含信息
                    reservationRecordDetailsResp.setCardResp((InvitationCardResp) detail.getObject());
                    // 访客list
                    List<ParticipantBean> guestList = guestService.getGuest(reservationRecordItem.getReservationRecord());
                    participantBeanList.addAll(guestList);
                }
                // }
                //}

            }
            //获取参会人姓名
            Optional<SysUser> sysOptional = sysUserDao.findByEmpNo(reservationRecordItem.getReservationRecord().getInitiatorEmpNo());
            //获取会议室名称
            Optional<Room> roomOptional = roomDao.findById(reservationRecordItem.getReservationRecord().getRoom().getId());
            // 数据赋值
            copyValue(reservationRecordDetailsResp, reservationRecordItem);
            // 会议组
            List<CustomUserGroupResp2> groups = new ArrayList<>();
            List<ReservationRecord2CustomGroup> list = record2CustomGroupDao.findByReservationRecord(reservationRecordItem.getReservationRecord());
            list.forEach(it -> {
                CustomUserGroupResp2 groupResp2 = new CustomUserGroupResp2();
                groupResp2.setId(it.getCustomUserGroup().getId());
                groupResp2.setName(it.getCustomUserGroup().getName());
                groups.add(groupResp2);
            });
            reservationRecordDetailsResp.setUserGroupList(groups);
            //会议名称
            String roomName = roomOptional.isPresent() ? roomOptional.get().getName() : null;
            reservationRecordDetailsResp.setRoomName(roomName);
            // 项目ID
            String projectId = roomOptional.isPresent() ? roomOptional.get().getSpace().getProject().getId() : null;
            reservationRecordDetailsResp.setProjectId(projectId);
            // 项目名称
            String projectName = roomOptional.isPresent() ? roomOptional.get().getSpace().getProject().getProjectArea().getName() : null;
            reservationRecordDetailsResp.setProjectName(projectName);
            //空间ID
            String spaceId = roomOptional.isPresent() ? roomOptional.get().getSpace().getId() : null;
            reservationRecordDetailsResp.setSpaceId(spaceId);
            //特殊要求
            reservationRecordDetailsResp.setService(reservationRecordItem.getReservationRecord().getService());
            //参会人状态
            reservationRecordDetailsResp.setUserStatus(reservationRecordItem.getStatus());
            // 欢迎语
            reservationRecordDetailsResp.setWelcomeWords(reservationRecordItem.getReservationRecord().getWelcomeWords());
            // 是否有会议大屏
            reservationRecordDetailsResp.setIsScreen(intelligentMeetingAppDao.existsByRoomId(reservationRecordItem.getReservationRecord().getRoom().getId()) ? 1 : 0);
            // 大屏状态
            reservationRecordDetailsResp.setScreenStatus(reservationRecordItem.getReservationRecord().getScreenStatus());
            // 空间下是否有设备
            reservationRecordDetailsResp.setDeviceControl(deviceDao.findBySpaceId(spaceId).size() > 0 ? 1 : 2);
            // 会议文档
            reservationRecordDetailsResp.setMeetingFileList(meetingFileService.getMeetingFileByRecordId(recordId));
            // 会议链接
            reservationRecordDetailsResp.setMeetingFileUrlList(meetingFileService.getMeetingFileUrlByRecordId(recordId));
            //会议发起人
            if (sysOptional.isPresent()) {
                // 发起人数据
                SysUser sysUser = sysOptional.get();
                reservationRecordDetailsResp.setInitiatorName(sysUser.getNameZh());
                reservationRecordDetailsResp.setInitiatorEmpNo(sysUser.getEmpNo());
            }

            //预定记录状态
            reservationRecordDetailsResp.setReservationRecordStatus(reservationRecordItem.getReservationRecord().getStatus());
            //是否是自己发起的会议
            if (reservationRecordItem.getReservationRecord().getInitiatorEmpNo().equals(ownerEmpNo)) {
                reservationRecordDetailsResp.setIsOwn(1);
            }

            // 过滤数据，输出数据时参会人列表不包含发起人
            reservationRecordDetailsResp.setParticipant(participantBeanList);
            // 是否可编辑
            // 发起人在会议取消与结束前，可以取消会议、编辑会议
            ReservationRecord reservationRecord = reservationRecordItem.getReservationRecord();
            // 判断是否为发起人
            LocalDateTime now = LocalDateTime.now();
            boolean status = !reservationRecordItem.getReservationRecord().getStatus().equals(CANCEL);
            boolean time = now.isAfter(reservationRecord.getTo());
            if (reservationRecordDetailsResp.getIsOwn() == 1) {
                boolean b = status && !time;
                reservationRecordDetailsResp.setEditable(b);
            } else {
                // 参会人在会议发起  开始前可编辑
                LocalDateTime from = reservationRecord.getFrom();
                boolean exceedFlag = DateUtil.isExceed(from, now, HOUR);
                boolean b = status && !exceedFlag;
                reservationRecordDetailsResp.setEditable(b);
            }

            reservationRecordDetailsResp.setNoDisturb(Boolean.TRUE.equals(reservationRecord.getNoDisturb()));

            reservationRecordDetailsResp.setLocation(SpaceService.getFullSpaceName(reservationRecord.getRoom().getSpace(), reservationRecord.getRoom().getSpace().getName()));

            reservationRecordDetailsResp.setStatus(getReservationStatus(reservationRecord));
            reservationRecordDetailsResp.setReservationRecordStatus(getReservationStatus(reservationRecord));
        } else {
            return ServiceResult.error("无权限", ErrConst.E401);
        }
            /*}
        }*/
        return ServiceResult.ok(reservationRecordDetailsResp);

    }


    @Override
    @Transactional
    public ReservationRecordDetailsResp getAdministrativeReservationRecordDetails(String recordId) {
        Optional<ReservationRecord> byId = reservationRecordDao.findById(recordId);
        if (!byId.isPresent()) {
            return new ReservationRecordDetailsResp();
        }
        //根据会议编号 与 查询人id 查找会议详情
        List<ReservationRecordItem> reservationRecordItemOptional = reservationRecordItemDao.findByReservationRecord(byId.get());

        // 根据会议编号 获取会议
        Set<ReservationRecordItem> byReservationRecordId = reservationRecordItemDao.findByReservationRecordId(recordId);
        List<ParticipantBean> participantBeanList = getParticipantBeanList(byReservationRecordId);

        ReservationRecordDetailsResp reservationRecordDetailsResp = new ReservationRecordDetailsResp();
        //todo swh wanshan
        if (!reservationRecordItemOptional.isEmpty()) {
            ReservationRecordItem reservationRecordItem = reservationRecordItemOptional.get(0);
            //获取参会人姓名
            Optional<SysUser> sysOptional = sysUserDao.findByEmpNo(reservationRecordItem.getReservationRecord().getInitiatorEmpNo());
            //获取会议室名称
            Optional<Room> roomOptional = roomDao.findById(reservationRecordItem.getReservationRecord().getRoom().getId());

            //会议名称
            String roomName = roomOptional.isPresent() ? roomOptional.get().getName() : null;
            reservationRecordDetailsResp.setRoomName(roomName);
            // 数据赋值
            copyValue(reservationRecordDetailsResp, reservationRecordItem);

            if (sysOptional.isPresent()) {
                // 发起人数据
                SysUser sysUser = sysOptional.get();
                reservationRecordDetailsResp.setInitiatorName(sysUser.getNameZh());
                reservationRecordDetailsResp.setInitiatorEmpNo(sysUser.getEmpNo());
            }

            //预定记录状态
            reservationRecordDetailsResp.setReservationRecordStatus(reservationRecordItem.getReservationRecord().getStatus());
            reservationRecordDetailsResp.setParticipant(participantBeanList);
            // 是否可编辑
            // 发起人在会议开始后的半小时内，以及开始前，可以取消会议、编辑会议
            ReservationRecord reservationRecord = reservationRecordItem.getReservationRecord();
            LocalDateTime from = reservationRecord.getFrom();
            LocalDateTime now = LocalDateTime.now();
            boolean exceedFlag = DateUtil.isExceed(from, now, HALF_HOUR);
            reservationRecordDetailsResp.setEditable(!exceedFlag);

        }
        return reservationRecordDetailsResp;
    }

    /**
     * 数据赋值
     *
     * @param reservationRecordDetailsResp: reservationRecordDetailsResp对象
     * @param reservationRecordItem:        reservationRecordItem对象
     * @author Yang.Lee
     * @date 2021/3/3 18:41
     **/
    private void copyValue(ReservationRecordDetailsResp reservationRecordDetailsResp, ReservationRecordItem reservationRecordItem) {
        //获取 个人会议id
        reservationRecordDetailsResp.setId(reservationRecordItem.getReservationRecord().getId());
        // 楼栋
        reservationRecordDetailsResp.setBuilding(reservationRecordItem.getReservationRecord().getRoom().getBuilding());
        //会议室楼层
        reservationRecordDetailsResp.setFloor(reservationRecordItem.getReservationRecord().getRoom().getFloor());
        //开始时间
        reservationRecordDetailsResp.setTimeFrom(reservationRecordItem.getReservationRecord().getFrom());
        //结束时间
        reservationRecordDetailsResp.setTimeTo(reservationRecordItem.getReservationRecord().getTo());
        //会议主题
        reservationRecordDetailsResp.setTopic(reservationRecordItem.getReservationRecord().getTopic());
        //会议室id
        reservationRecordDetailsResp.setRoomId(reservationRecordItem.getReservationRecord().getRoom().getId());
        //会议备注
        reservationRecordDetailsResp.setRemark(reservationRecordItem.getReservationRecord().getRemark());

        //特殊要求
        reservationRecordDetailsResp.setService(reservationRecordItem.getReservationRecord().getService());
    }

    /**
     * 把Set<ReservationRecordItem>转换成List<ParticipantBean>对象
     *
     * @param byReservationRecordId: 入参
     * @return List<ParticipantBean>对象
     * @author Yang.Lee
     * @date 2021/3/3 17:21
     **/
    @Transactional
    public List<ParticipantBean> getParticipantBeanList(Set<ReservationRecordItem> byReservationRecordId) {
        List<ParticipantBean> participantBeanList = new ArrayList<>();

        //循环找出状态为未删除的 参会人和 参会人状态
        for (ReservationRecordItem rri : byReservationRecordId) {
            if (rri.getIsDelete() == 0) {

                ParticipantBean participantBean = new ParticipantBean();

                Optional<Attendance> attendanceOp = attendanceDao.findByUserEmpNoAndReservationRecordId(rri.getOwnerEmpNo(), rri.getReservationRecord().getId());
                participantBean.setIsAttendance(2);
                if (attendanceOp.isPresent()) {
                    // 设置参会人是否签到
                    participantBean.setIsAttendance(1);
                }

                // 设置参会人id
                participantBean.setOwner(rri.getOwner());
                SysUser sysUser = sysUserServiceUC.getUserByEmpNo(rri.getOwnerEmpNo());
                //设置参会人姓名
                participantBean.setOwnerName(sysUser.getNameZh());
                //设置部门id
                participantBean.setDeptId(sysUser.getDeptId());
                //设置部门名称
                participantBean.setDeptName(sysUser.getDeptName());
                // 设置参会人状态
                participantBean.setStatus(rri.getStatus());
                //设置参与人工号
                participantBean.setHrId(sysUser.getEmpNo());
                // 设置参与人头像
                participantBean.setHeadImgUrl(sysUser.getHeadImgUrl());
                // 是否外部访客
                participantBean.setIsGuest(0);
                participantBean.setCode("");
                participantBean.setIsGroup(rri.getIsGroup());
                participantBeanList.add(participantBean);
            }
        }

        return participantBeanList;
    }

    /**
     * 根据工号获取部门名称
     *
     * @param jobNames 工号
     * @return 部门map对象, key为工号，val为部门名称
     */
    @Override
    public Map<String, String> getUserDepartmentName(String... jobNames) {

        if (jobNames == null || jobNames.length == 0) {
            return new HashMap<>();
        }
        Map<String, String> resultMap = new HashMap<>();
        for (String name : jobNames) {
            if (!resultMap.containsKey(name)) {
                resultMap.put(name, sysUserServiceUC.getUserByEmpNo(name).getDeptName());
            }
        }

        return resultMap;
    }

    @Override
    @Transactional
    public void findRoomUsedTime() {
        // 获取会议发起人工号
        Set<String> empNos = reservationRecordDao.findGroupByInitiatorEmpNo();

        if (empNos.isEmpty()) {
            return;
        }

        // 查询所有会议室
        List<Room> allRoomList = roomDao.findAll();
        if (allRoomList.isEmpty()) {
            return;
        }

        for (String empNo : empNos) {
            // 遍历所有会议室
            allRoomList.forEach(room -> {
                // 查询发起人在某一会议室发起的所有会议,忽略已取消的会议
                List<ReservationRecord> recordList = reservationRecordDao.findByInitiatorEmpNoAndStatusNotAndRoom(empNo, CANCEL, room);
                if (recordList.isEmpty()) {
                    return;
                }

                // 计算使用当前会议室的使用时长,单位为分钟
                Long usedTime = recordList
                        .stream()
                        .map(record -> Duration.between(record.getFrom(), record.getTo()).toMinutes())
                        .collect(Collectors.toSet())
                        .stream()
                        .reduce(Long::sum)
                        .get();

                // 查询数据库中的使用时长
                RoomUsedTime roomUsedTime = roomUsedTimeDao.findByEmpNoAndRoom(empNo, room).orElse(new RoomUsedTime());

                // 如果时间时间不一样，则修改
                if (StringUtils.isEmpty(roomUsedTime.getEmpNo())) {
                    roomUsedTime.setRoom(room);
                    roomUsedTime.setEmpNo(empNo);
                }
                if (roomUsedTime.getUsedTime() != usedTime) {
                    roomUsedTime.setUsedTime(usedTime);
                    roomUsedTimeDao.save(roomUsedTime);
                }
                String key = redisUtil.createRoomUsedTimeKey(IConst.ROOM_USED_TIME_MODULE, empNo, RedisUtil.Separator.COLON);
                redisUtil.zSSet(key, room.getId(), usedTime);
            });
        }
    }

    /**
     * 获取会议状态
     *
     * @param reservationRecord 会议对象
     * @return 会议状态
     * @author Yang.Lee
     * @date 2021/6/15 15:21
     **/
    @Override
    public ReservationRecordStatus getReservationStatus(ReservationRecord reservationRecord) {

        if (ReservationRecordStatus.CANCEL.equals(reservationRecord.getStatus())) {
            return CANCEL;
        } else {
            //会议未取消
            LocalDateTime localDateTimeNow = LocalDateTime.now();
            LocalDateTime timeFrom = reservationRecord.getFrom();
            LocalDateTime timeTo = reservationRecord.getTo();
            //若当前时间未到达会议开始 状态为DEFAULT
            if (localDateTimeNow.isBefore(timeFrom)) {
                return DEFAULT;
            }
            //若当前时间大于会议结束时间 状态为FINISH
            else if (localDateTimeNow.isAfter(timeTo)) {
                return FINISH;
            }
            //否则 当前时间在会议开始时间和会议结束时间之间  状态设置为PENDING
            else {
                return PENDING;
            }
        }

    }

    /**
     * 查询用户正在进行中的会议列表
     *
     * @param empNo 员工工号
     * @return 会议列表
     * @author Yang.Lee
     * @date 2021/6/18 10:59
     **/
    @Override
    @Transactional(readOnly = true)
    public List<OngoingMeetingResp> getOngoingMeetingList(String empNo) {

        List<ReservationRecordItem> reservationRecordItems = reservationRecordItemDao.findAll((root, criteriaQuery, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();
            list.add(criteriaBuilder.equal(root.get("ownerEmpNo"), empNo));
            String nowTime = DateUtils.formatLocalDateTime(LocalDateTime.now(), DateUtils.PATTERN_DATETIME);

            list.add(criteriaBuilder.lessThanOrEqualTo(
                    root.get("reservationRecord").get("from").as(String.class),
                    nowTime));
            list.add(criteriaBuilder.greaterThanOrEqualTo(
                    root.get("reservationRecord").get("to").as(String.class),
                    nowTime));


            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        });

        // 如果无会议，则返回空数据
        if (reservationRecordItems.isEmpty()) {
            return new ArrayList<>();
        }

        List<OngoingMeetingResp> result = new ArrayList<>();

        for (ReservationRecordItem obj : reservationRecordItems) {

            // 过滤已取消的会议，和已拒绝的会议跳过
            if (ReservationRecordStatus.CANCEL.equals(obj.getReservationRecord().getStatus()) || ReservationRecordItemStatus.CANCEL.equals(obj.getStatus())) {
                continue;
            }

            OngoingMeetingResp ongoingMeetingResp = new OngoingMeetingResp();
            ReservationRecordDetailsResp convert = convert(obj);
            // 访客list
            List<ParticipantBean> guestList = guestService.getGuest(obj.getReservationRecord());
            convert.getParticipant().addAll(guestList);
            ongoingMeetingResp.setReservationDetail(convert);
            // 查询该会议室的所有设备信息
            ServiceResult result1 = spaceService.getDeviceListAll(obj.getReservationRecord().getRoom().getSpace().getId(), ongoingMeetingResp);
            List<DeviceWechatResp> object = (List<DeviceWechatResp>) result1.getObject();
            // 只返回空调 窗帘 四路开关
            List<DeviceWechatResp> respList = new ArrayList<>();
            for (DeviceWechatResp device : object) {
                if (DeviceType.FOUR_WAY_SWITCH.equals(device.getType()) || DeviceType.AIR_CONDITIONER.equals(device.getType()) || DeviceType.CURTAINS.equals(device.getType())) {
                    respList.add(device);
                }
            }
            ongoingMeetingResp.setDeviceWechatList(respList);

            result.add(ongoingMeetingResp);
        }

        return result;
    }

    /**
     * 获取会议预约记录（分页）
     *
     * @param req 请求参数
     * @return 分页查询结果
     * @author Yang.Lee
     * @date 2021/7/15 9:46
     **/
    @Override
    @Transactional
    public PageBean<ReservationPageResp> getReservationPage(ReservationPageReq req) {

        String like = "%" + req.getKeyWord() + "%";

        // 首先根据查询条件查找出需要的员工工号集合
        Map<String, SysUser> staffEmpNoMap = new HashMap<>();
        if (StringUtils.isNotBlank(req.getKeyWord())) {
            List<SysUser> sysUserList = sysUserDao.findAll((root, query, criteriaBuilder) -> {

                // 构造查询条件,根据员工姓名，工号，部门模糊查询
                Predicate empNoPre = criteriaBuilder.like(root.get("empNo").as(String.class), like);
                Predicate namePre = criteriaBuilder.like(root.get("nameZh").as(String.class), like);
                Predicate departmentPre = criteriaBuilder.like(root.get("fullDepartmentName").as(String.class), like);
                return criteriaBuilder.or(empNoPre, namePre, departmentPre);
            });
            sysUserList.forEach(obj -> staffEmpNoMap.put(obj.getEmpNo(), obj));
        }

        Pageable pageable = PageRequest.of(req.getPage(), req.getSize(), Sort.by(req.getOrders()));
        // 查询预约记录
        Page<ReservationRecord> reservationRecordPage = reservationRecordDao.findAll((root, query, criteriaBuilder) -> {
            // 构造查询条件
            List<Predicate> list = new ArrayList<>();

            // 会议室地区
            if (StringUtils.isNotEmpty(req.getProjectId())) {
                list.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("room").get("space").get("project").get("id").as(String.class), req.getProjectId())));
            }
            // 会议室
            if (StringUtils.isNotEmpty(req.getRoomId())) {
                list.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("room").get("id").as(String.class), req.getRoomId())));
            }

            if (req.getStartTime() != null) {
                list.add(criteriaBuilder.greaterThanOrEqualTo(root.get("from"),
                        req.getStartTime()));
            }

            if (req.getEndTime() != null) {
                list.add(criteriaBuilder.lessThanOrEqualTo(root.get("from"),
                        req.getEndTime()));
            }

            // 根据会议状态筛选
            if (req.getStatus() != null) {
                switch (req.getStatus()) {

                    case DEFAULT:
                        list.add(criteriaBuilder.and(
                                criteriaBuilder.greaterThanOrEqualTo(root.get("from"), LocalDateTime.now()),
                                criteriaBuilder.notEqual(root.get("status"), CANCEL))
                        );
                        break;
                    case PENDING:
                        list.add(criteriaBuilder.and(criteriaBuilder.lessThanOrEqualTo(root.get("from"), LocalDateTime.now()),
                                criteriaBuilder.greaterThanOrEqualTo(root.get("to"), LocalDateTime.now()),
                                criteriaBuilder.notEqual(root.get("status"), CANCEL)));
                        break;
                    case FINISH:
                        list.add(criteriaBuilder.and(criteriaBuilder.lessThanOrEqualTo(root.get("to"), LocalDateTime.now()),
                                criteriaBuilder.notEqual(root.get("status"), CANCEL)));
                        break;
                    case CANCEL:
                        list.add(criteriaBuilder.equal(root.get("status"), CANCEL));
                        break;
                    default:
                }
            }


            // 会议主题
            if (StringUtils.isNotBlank(req.getKeyWord())) {

                CriteriaBuilder.In<Object> inInitiator = criteriaBuilder.in(root.get("initiatorEmpNo"));
                staffEmpNoMap.forEach((k, v) -> inInitiator.value(k));
                list.add(criteriaBuilder.or(criteriaBuilder.and(inInitiator), criteriaBuilder.like(root.get("topic").as(String.class), like)));
            }
            Predicate preAnd = criteriaBuilder.and(list.toArray(new Predicate[0]));

            return query.where(preAnd).getRestriction();
        }, pageable);


        List<ReservationRecord> reservationRecordList = reservationRecordPage.getContent();
        List<ReservationPageResp> resultList = new ArrayList<>();

        // 类型转换
        reservationRecordList.forEach(obj -> {

            SysUser initiator = sysUserServiceUC.getUserByEmpNo(obj.getInitiatorEmpNo());
            ReservationRecordStatus status = getReservationStatus(obj);
            resultList.add(ReservationPageResp.builder()
                    .department(initiator.getFullDepartmentName())
                    .empNo(initiator.getEmpNo())
                    .name(initiator.getNameZh())
                    .startTime(obj.getFrom().toLocalTime())
                    .endTime(obj.getTo().toLocalTime())
                    .reservationId(obj.getId())
                    .meetingDate(obj.getDate())
                    .reservationDatetime(obj.getCreatedTime())
                    .projectName(obj.getRoom().getSpace().getProject().getProjectName())
                    .spaceName(SpaceService.getFullSpaceName(obj.getRoom().getSpace(), obj.getRoom().getSpace().getName()))
                    .roomName(obj.getRoom().getName())
                    .status(status)
                    .statusName(status.getName())
                    .topic(obj.getTopic())
                    .build());
        });

        PageBean<ReservationPageResp> pageBean = new PageBean<>();
        pageBean.setPage(req.getPage());
        pageBean.setSize(req.getSize());
        pageBean.setList(resultList);
        pageBean.setTotalPages(reservationRecordPage.getTotalPages());
        pageBean.setTotalElements(reservationRecordPage.getTotalElements());

        return pageBean;
    }

    /**
     * 获取会议统计数据
     *
     * @param projectId 项目ID
     * @return 统计结果
     * @author Yang.Lee
     * @date 2021/9/27 19:51
     **/
    @Override
    @Transactional
    public MeetingStatisticsResp getTodayMeetingNumberStatistics(String projectId) {

        // 查询当天所有会议
        List<ReservationRecord> reservationRecordList =
                reservationRecordDao.findAll((root, query, criteriaBuilder) -> {

                    List<Predicate> list = new ArrayList<>();

                    list.add(criteriaBuilder.equal(root.get("room").get("space").get("project").get("id"), projectId));
                    list.add(criteriaBuilder.equal(root.get("date"), LocalDate.now()));
                    list.add(criteriaBuilder.notEqual(root.get("status"), CANCEL));
                    return criteriaBuilder.and(list.toArray(new Predicate[0]));
                });

        MeetingStatisticsResp resp = new MeetingStatisticsResp();

        LocalDateTime now = LocalDateTime.now();

        // 会议总数
        resp.setTotalNumber(reservationRecordList.size());
        // 正在进行会议数
        resp.setInProgressNumber(reservationRecordList.stream()
                .filter(obj ->
                        now.isAfter(obj.getFrom()) && now.isBefore(obj.getTo()))
                .collect(Collectors.toList())
                .size());
        // 已结束会议数
        resp.setEndedNumber(reservationRecordList.stream()
                .filter(obj -> now.isAfter(obj.getTo()))
                .collect(Collectors.toList()).size()
        );

        return resp;
    }

    /**
     * 获取当日会议时间分布统计数据
     *
     * @param projectId 项目ID
     * @return 统计结果
     * @author Yang.Lee
     * @date 2021/9/27 20:31
     **/
    @Override
    @Transactional
    public List<ChartDataResp> getTodayMeetingTimePeriodStatistics(String projectId) {

        // 查询当天所有会议
        List<ReservationRecord> reservationRecordList =
                reservationRecordDao.findAll((root, query, criteriaBuilder) -> {

                    List<Predicate> list = new ArrayList<>();

                    list.add(criteriaBuilder.equal(root.get("room").get("space").get("project").get("id"), projectId));
                    list.add(criteriaBuilder.equal(root.get("date"), LocalDate.now()));
                    list.add(criteriaBuilder.notEqual(root.get("status"), CANCEL));
                    return criteriaBuilder.and(list.toArray(new Predicate[0]));
                });

        // 获取8-20点的数据

        Map<Integer, Integer> hoursMap = new HashMap();
        for (int i = 0; i <= 24; i++) {
            hoursMap.put(i, 0);
        }

        reservationRecordList.forEach(obj -> {

            Duration between = Duration.between(obj.getFrom(), obj.getTo());
            long durationMinutes = between.toMinutes();

            int hour = obj.getFrom().getHour();

            if (durationMinutes <= 30) {
                // 会议小于半个小时，在会议开始的小时上会议数+1
                hoursMap.put(hour, hoursMap.get(hour) + 1);
            } else {
                // 判断会议进行了多少个30分钟
                long halfHours = durationMinutes / 30;

                // 会议大于半个小时，判断会议是30分开始还是0分开始
                // 如果是0分开始的会议，则持续的小时数是 halfHours / 2, 有余数则加1
                int minutes = obj.getFrom().getMinute();

                long rest = 0;
                if (minutes == 0) {

                    long reminder = halfHours % 2;
                    rest = reminder == 0 ? halfHours / 2 : halfHours / 2 + 1;
                } else if (minutes == 30) {
                    // 如果会议是30分开始的，则
                    rest = halfHours / 2 + 1;
                }

                for (int i = 0; i < rest; i++) {
                    hoursMap.put(hour + i, hoursMap.get(hour + i) + 1);
                }
            }

        });

        List<ChartDataResp> chartDataRespList = new ArrayList<>();
        hoursMap.forEach((k, v) -> {
            // 过滤掉8点前后20点后的数据
            if (k > 20 || k < 8) {
                return;
            }
            chartDataRespList.add(ChartDataResp.builder().x(String.valueOf(k)).y(v).build());

        });

        return chartDataRespList;
    }

    /**
     * 对象转换
     *
     * @param source 对象转换
     * @return
     * @author Yang.Lee
     * @date 2021/6/18 13:46
     **/
    @Transactional
    public ReservationRecordDetailsResp convert(ReservationRecordItem source) {

        ReservationRecordDetailsResp obj = new ReservationRecordDetailsResp();

        ReservationRecord record = source.getReservationRecord();

        obj.setReservationRecordStatus(getReservationStatus(record));
        obj.setId(record.getId());
        obj.setTopic(record.getTopic());
        obj.setInitiatorEmpNo(record.getInitiatorEmpNo());
        obj.setInitiatorName(record.getInitiator());
        obj.setLocation(SpaceService.getFullSpaceName(record.getRoom().getSpace(), record.getRoom().getSpace().getName()));
        obj.setRemark(record.getRemark());
        obj.setRoomId(record.getRoom().getId());
        obj.setRoomName(record.getRoom().getName());
        obj.setTimeFrom(record.getFrom());
        obj.setTimeTo(record.getTo());
        obj.setSpaceId(record.getRoom().getSpace().getId());
        obj.setSpaceName(record.getRoom().getSpace().getName());
        obj.setProjectId(record.getRoom().getSpace().getProject().getId());
        obj.setProjectName(record.getRoom().getSpace().getProject().getProjectArea().getName());

        Set<ReservationRecordItem> reservationRecordItemSet = reservationRecordItemDao.findByReservationRecordId(record.getId());
        obj.setParticipant(getParticipantBeanList(reservationRecordItemSet));

        return obj;
    }


    @Override
    @Transactional
    public ServiceResult cancel(String id, String token) {
        // 根据 id 查询会议预约记录
        Optional<ReservationRecord> recordOptional = reservationRecordDao.findById(id);
        boolean present = recordOptional.isPresent();
        // 判断是否存在
        if (present) {

            ReservationRecord reservationRecord = recordOptional.get();

            // 如果已经被取消则不要再次执行取消操作
            if (reservationRecord.getStatus().equals(CANCEL)) {
                return ServiceResult.error("会议已取消，请勿重复操作");
            }

            //会议主题
            String topic = reservationRecord.getTopic();
            // 发起人在会议开始后的半小时内，以及开始前，可以取消会议、编辑会议
            // 2021-04-22 ： 新需求，会议结束之前都可以取消会议
            LocalDateTime to = reservationRecord.getTo();
            LocalDateTime now = LocalDateTime.now();

            if (now.compareTo(to) > 0) {
                return ServiceResult.error("会议已结束,无法取消");
            }

            // 取消会议
            reservationRecord.setStatus(CANCEL);
            Set<ReservationRecordItem> reservationRecordItemSet = reservationRecordItemDao.findByReservationRecordId(id);
            reservationRecordItemSet.forEach(it -> {

                // 2021-04-21 ： 前端重构后该需求发生变化，  会议取消后，参会人状态不变，不再默认修改为“CANCEL”状态

                log.debug("此处推送 取消会议消息给用户: " + it.getOwnerEmpNo());
                weChatService.channelRequirementsReminders(it, topic);

                // 取消参会人日程数据
                scheduleService.cancel(reservationRecord, it.getOwnerEmpNo());
            });
            reservationRecordDao.save(reservationRecord);

            // 取消所有访客通行权限
            // 根据会议记录获取邀请函
            Optional<InvitationCard> invitationCard = invitationCardDao.findByReservationRecord(reservationRecord);
            if (invitationCard.isPresent()) {
                // 向ehr删除访客数据
                EhrCancelInvitation cancelInvitation = new EhrCancelInvitation();
                cancelInvitation.setVisitorRecordNo(invitationCard.get().getVisitorRecordNo());
                cancelInvitation.setStatus("0");
                String requestParam = JSON.toJSONString(cancelInvitation);
                log.debug("json参数 ： " + requestParam);
                // 发送Http请求 下发控制命令
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                headers.add(AUTHTOKEN, token);
                HttpEntity<String> requestEntity = new HttpEntity<>(requestParam, headers);
                ResponseEntity<String> responseEntity;
                try {
                    responseEntity = restTemplate.postForEntity(cancelUrl, requestEntity, String.class);
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                    return ServiceResult.error("ehr删除访客数据失败");
                }
                // 解析请求结果
                String responseBody = responseEntity.getBody();
                JSONObject jsonResult = JSON.parseObject(responseBody);
                String codeEhr = jsonResult.getString("code");
                if (!codeEhr.equals("0")) {
                    return ServiceResult.error(jsonResult.getString("msg"));
                }
                log.info("ehr取消邀请函成功 邀请函单号: " + invitationCard.get().getVisitorRecordNo());

                log.info("访客已取消 邀请函id : " + invitationCard.get().getId());
            }
            log.info("数据库更新完成！！");

            // 推送消息给会议App
            intelligentMeetingAppService.pushMeetingRoomDetail(reservationRecord.getRoom().getId());
        } else {
            return ServiceResult.error("会议记录不存在");
        }
        log.info(String.valueOf(recordOptional));


        return ServiceResult.ok();
    }

    @Override
    @Transactional
    public void updateInviteeStatus(String id, InviteeStatusReq inviteeStatusReq) {
        //根据会议id 参会人工号  参会人状态 更新状态
        Optional<ReservationRecordItem> byReservationRecordIdAndOwner = reservationRecordItemDao.findByReservationRecordIdAndOwnerEmpNo(id, inviteeStatusReq.getEmpNo());
        boolean present = byReservationRecordIdAndOwner.isPresent();
        if (present) {
            //更新状态
            ReservationRecordItem reservationRecordItem = byReservationRecordIdAndOwner.get();

            ReservationRecordItemStatus oldStatus = reservationRecordItem.getStatus();

            reservationRecordItem.setStatus(inviteeStatusReq.getStatus());
            reservationRecordItemDao.save(reservationRecordItem);
            if (inviteeStatusReq.getStatus().equals(ReservationRecordItemStatus.CANCEL)) {
                log.info("此处可能要写一些推送的消息");
                weChatService.channelMeetingReminders(id, inviteeStatusReq);

                // 拒绝则同时删除用户的日程
                scheduleService.cancel(reservationRecordItem.getReservationRecord(), inviteeStatusReq.getEmpNo());
            }

            // 如果之前已拒绝，然后再次接受邀请，则创新新日程
            if (ReservationRecordItemStatus.CANCEL.equals(oldStatus) && !ReservationRecordItemStatus.CANCEL.equals(inviteeStatusReq.getStatus())) {
                scheduleService.create(reservationRecordItem.getReservationRecord(), inviteeStatusReq.getEmpNo());
            }
        }
    }

    @Override
    public List<ReservationConflictResp> conflictDetect(ReservationConflictReq reservationConflictReq) {

        // 用户工号集合
        Set<String> userEmpNos = reservationConflictReq.getEmpNos();
        // 需要检测的日期
        String strDate = reservationConflictReq.getDate();
        LocalDate date = DateUtil.convert(strDate);
        // 需要检测的时间段
        String timeFrom = reservationConflictReq.getTimeFrom();
        String timeTo = reservationConflictReq.getTimeTo();
        LocalDateTime detectFrom = DateUtil.merge(strDate, timeFrom);
        LocalDateTime detectTo = DateUtil.merge(strDate, timeTo);
        //初始化 ReservationConflictResp 列表
        List<ReservationConflictResp> reservationConflictRespList = new ArrayList<>();
        // 用户循环
        for (String userEmpNo : userEmpNos) {
            //根据用户,timeSlot查询
            List<ReservationRecordItem> itemList = reservationRecordItemDao.findByOwnerEmpNoAndReservationRecordDate(userEmpNo, date);
            //初始化状态:无冲突
            int status = 0;
            ReservationRecordResp reservationRecordResp = new ReservationRecordResp();
            //根据 会议预约明细 循环
            for (ReservationRecordItem item : itemList) {
                ReservationRecord reservationRecord = item.getReservationRecord();
                LocalDateTime from = reservationRecord.getFrom();
                LocalDateTime to = reservationRecord.getTo();
                // 忽略已取消的会议预约记录, 忽略自己的冲突
                if (CANCEL.equals(reservationRecord.getStatus()) || reservationRecord.getId().equals(reservationConflictReq.getMeetingId())) {
                    continue;
                }
                //检测会议冲突
                boolean isConflict = conflictDetectByTime(detectFrom, detectTo, from, to);
                //如果有冲突
                if (isConflict) {
                    status = 1;
                    reservationRecordResp.setTopic(item.getReservationRecord().getTopic());
                    reservationRecordResp.setFrom(item.getReservationRecord().getFrom());
                    reservationRecordResp.setTo(item.getReservationRecord().getTo());
                    break;
                }
            }
            ReservationConflictResp reservationConflictResp = new ReservationConflictResp();
            reservationConflictResp.setEmpNo(userEmpNo);
            reservationConflictResp.setStatus(status);
            reservationConflictResp.setFrom(detectFrom);
            reservationConflictResp.setTo(detectTo);
            if (status == 1) {
                //设置冲突信息
                reservationConflictResp.setReservationRecordResp(reservationRecordResp);
            }
            reservationConflictRespList.add(reservationConflictResp);
        }

        return reservationConflictRespList;
    }

    /**
     * 会议冲突检测
     *
     * @param detectFrom 检测开始时间
     * @param detectTo   检测结束时间
     * @param from       已预约开始时间
     * @param to         已预约结束时间
     * @return true:有冲突;false:无冲突
     */
    private boolean conflictDetectByTime(LocalDateTime detectFrom, LocalDateTime detectTo, LocalDateTime from, LocalDateTime to) {
        //校验
        if (detectFrom.isEqual(detectTo) || detectFrom.isAfter(detectTo) || from.isEqual(to) || from.isAfter(to)) {
            throw new BizLogicNotMatchException("时间先后顺序不匹配");
        }
        //开始判断
        if (from.isAfter(detectTo) || from.isEqual(detectTo)) {
            return false;
        }
        if (to.isBefore(detectFrom) || to.isEqual(detectFrom)) {
            return false;
        }
        return true;
    }


    @Override
    @Transactional
    public List<String> findRecentListByUserEmpNo(String empNo) {

        //查询发起人发起的会议预约的参与人
        String initiatorEmpNo = empNo;
        Sort sort = Sort.by(Sort.Direction.DESC, "createdTime");
        Set<String> fullSet = new LinkedHashSet<>();
        //分页参数
        int pageNo = IConst.DEFAULT_PAGE_NO;
        Pageable pageable = PageRequest.of(pageNo, IConst.RECENT_QUERY_PAGE_SIZE, sort);
        Page<ReservationRecordItem> page = reservationRecordItemDao.findByInitiatorEmpNoEqualsAndOwnerEmpNoIsNot(initiatorEmpNo, initiatorEmpNo, pageable);
        List<String> tempList = page.get().map(ReservationRecordItem::getOwnerEmpNo).distinct().collect(Collectors.toList());
        fullSet.addAll(tempList);
        while (fullSet.size() < IConst.RECENT_COUNT_REQUIRED) {
            //判断是否有分页
            if (page.hasNext()) {
                pageNo++;
                pageable = PageRequest.of(pageNo, IConst.RECENT_QUERY_PAGE_SIZE, sort);
                page = reservationRecordItemDao.findByInitiatorEmpNoEqualsAndOwnerEmpNoIsNot(initiatorEmpNo, initiatorEmpNo, pageable);
                tempList = page.get().map(ReservationRecordItem::getOwnerEmpNo).distinct().collect(Collectors.toList());
                fullSet.addAll(tempList);
            } else {
                break;
            }
        }
        //准备 finalList
        List<String> finalList = new ArrayList<>();
        Iterator<String> it = fullSet.iterator();
        while (it.hasNext()) {
            finalList.add(it.next());
            //如果超出上限,提前返回
            if (finalList.size() == IConst.RECENT_COUNT_REQUIRED) {
                break;
            }
        }
        log.debug(finalList.toString());

        //再EHR查询员工数据

        return finalList;
    }

    @Override
    @Transactional
    public ServiceResult checkReservation(ReservationAddReq req) {
        log.debug("checkReservation...req: " + req);
        String roomId = req.getRoomId();
        LocalDate date = req.getDate();
        String strDate = DateUtil.convert2(date);
        LocalDateTime from = req.getFrom();
        LocalDateTime to = req.getTo();
        //检查: 会议室在时间段是否存在预约记录
        RoomWithReservationResp roomWithReservation = this.findBySysUserAndRoomId("", roomId, date);
        log.debug("roomWithReservation:" + roomWithReservation);
        List<TimeSlotResultResp> collect = roomWithReservation.getTimeSlotResultRespList()
                .stream()
                .filter(it -> matchTimeSlot(it, strDate, from, to))
                .filter(it -> it.getStatus() == 2)
                .collect(Collectors.toList());

        if (collect.isEmpty()) {
            return ServiceResult.ok();
        } else {
            return ServiceResult.error(collect);
        }
    }

    private boolean matchTimeSlot(TimeSlotResultResp timeSlotResult, String strDate, LocalDateTime from, LocalDateTime to) {
        log.debug("matchTimeSlot()...timeSlotResult:" + timeSlotResult);
        String strFrom = timeSlotResult.getFrom();
        String strTo = timeSlotResult.getTo();
        LocalDateTime dateFrom = DateUtil.merge(strDate, strFrom);
        LocalDateTime dateTo = DateUtil.merge(strDate, strTo);

        if (dateFrom.isEqual(from) || dateTo.isEqual(to)) {
            return true;
        } else if (dateFrom.isAfter(from) && dateTo.isBefore(to)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    @Transactional
    public ServiceResult deleteById(String id, String empNo) {
        // 1.检查状态
        ReservationRecord rr = reservationRecordDao.findById(id).orElseThrow(() -> new DataNotFoundException("id:" + id));

        if (rr.getInitiatorEmpNo().equals(empNo)) {
            return ServiceResult.error("非发起人无删除权限");
        }

        ReservationRecordStatus status = rr.getStatus();
        if (!status.equals(CANCEL)) {
            throw new BizLogicNotMatchException("数据状态不正确");
        }
        // 2.逻辑删除会议预约明细
        Set<ReservationRecordItem> rriSet = reservationRecordItemDao.findByReservationRecordId(id);
        for (ReservationRecordItem rri : rriSet) {
            rri.setIsDelete(1);
            reservationRecordItemDao.save(rri);
        }
        // 3.逻辑删除会议预约
        rr.setIsDelete(1);
        reservationRecordDao.save(rr);

        // 推送消息给会议App
        intelligentMeetingAppService.pushMeetingRoomDetail(rr.getRoom().getId());

        return ServiceResult.ok("已成功删除ID:" + id);
    }


    @Override
    @Transactional
    public AppointmentsTimeCanBeMdeResp nextValidTime(String roomId, String date) {
        //        正常情况，会议默认时间是 当前时刻后的整点（半点）开始的一小时；   2021-01-06： 需求变更，会议默认时时间时当前时间向前寻找半小时，ep：当前18：37，可用时间从18：30开始
        //        如果当前时刻后的一小时被占用或者不开放，则继续向后顺延；
        //        如果当天可预订时间已不足一小时，则默认会议时间半小时；
        //        如果当天可预订时间不足半小时，则默认时间为空；
        AppointmentsTimeCanBeMdeResp appointmentsTimeCanBeMde = new AppointmentsTimeCanBeMdeResp();
        Optional<Room> roomOptional = roomDao.findById(roomId);
        // 1. 查询房间是否存在
        if (roomOptional.isPresent()) {
            LocalDate today = DateUtils.parseLocalDate(date, DateUtils.PATTERN_DATE);
            LocalTime localTimeNow = LocalTime.now();
            //获取会议室当天预约时间段详情 todo
            RoomWithReservationResp roomWithReservationResp = this.findBySysUserAndRoomId(null, roomId, today);
            //根据status 和 当前时间 获取 status为可预约状态并且开始时间大于当前时间 删选
            List<AppointmentsTimeCanBeMdeResp> appointmentsTimeCanBeMdeRespList = getStatusByNow(roomWithReservationResp, today);
            LocalTime localTimeLastOneFrom = localTimeNow;
            LocalTime localTimeLastOneTo = localTimeNow;
            Boolean setBoolean = false;
            if (!appointmentsTimeCanBeMdeRespList.isEmpty()) {
                for (AppointmentsTimeCanBeMdeResp it : appointmentsTimeCanBeMdeRespList) {
                    // 上一条的 到 时间 等于 这一条的从时间  说明有连续1小时的 可预约
                    if (it.getFrom().equals(localTimeLastOneTo)) {
                        appointmentsTimeCanBeMde.setFrom(localTimeLastOneFrom);
                        appointmentsTimeCanBeMde.setTo(it.getTo());
                        setBoolean = true;
                        break;
                    }
                    localTimeLastOneFrom = it.getFrom();
                    localTimeLastOneTo = it.getTo();
                }
                // 没找到连续的1小时
                if (Boolean.FALSE.equals(setBoolean)) {
                    appointmentsTimeCanBeMde.setFrom(appointmentsTimeCanBeMdeRespList.get(0).getFrom());
                    appointmentsTimeCanBeMde.setTo(appointmentsTimeCanBeMdeRespList.get(0).getTo());
                }
                appointmentsTimeCanBeMde.setYtd(today);
            } else {
                appointmentsTimeCanBeMde.setFrom(null);
                appointmentsTimeCanBeMde.setTo(null);
                appointmentsTimeCanBeMde.setYtd(null);
            }
        }
        return appointmentsTimeCanBeMde;
    }

    private List<AppointmentsTimeCanBeMdeResp> getStatusByNow(RoomWithReservationResp roomWithReservationResp, LocalDate today) {
        LocalTime localTimeNow = LocalTime.now();
        //获取当前年月日
        LocalTime localTime = LocalTime.now();
        LocalDate localDate = LocalDate.now();
        LocalDateTime localDateTime = LocalDateTime.of(localDate, localTime);
        ZoneId zone = ZoneId.systemDefault();
        Instant instant = localDateTime.atZone(zone).toInstant();
        Date date1 = Date.from(instant);
        Instant instant1 = date1.toInstant();
        ZoneId zone1 = ZoneId.systemDefault();
        LocalDateTime localDateTime1 = LocalDateTime.ofInstant(instant1, zone1);
        LocalDate ytdNow = localDateTime1.toLocalDate();


        List<AppointmentsTimeCanBeMdeResp> appointmentsTimeCanBeMdeRespList = new ArrayList<>();
        for (TimeSlotResultResp it : roomWithReservationResp.getTimeSlotResultRespList()) {
            AppointmentsTimeCanBeMdeResp appointmentsTimeCanBeMdeResp = new AppointmentsTimeCanBeMdeResp();
            //获取开始时间   结束时间
            String timeFrom = it.getFrom();
            if (it.getStatus() == 1) {
                LocalTime localTimeFrom = LocalTime.parse(it.getFrom());
                if (today.equals(ytdNow)) {
                    if (DateUtil.isTimeAvailable(localTimeFrom, localTimeNow)) {
                        appointmentsTimeCanBeMdeResp.setFrom(localTimeFrom);
                        if ("23:30".equals(timeFrom)) {
                            appointmentsTimeCanBeMdeResp.setTo(LocalTime.parse("23:59"));
                        } else {
                            appointmentsTimeCanBeMdeResp.setTo(LocalTime.parse(it.getTo()));
                        }
                        appointmentsTimeCanBeMdeRespList.add(appointmentsTimeCanBeMdeResp);
                    }
                } else {
                    appointmentsTimeCanBeMdeResp.setFrom(localTimeFrom);
                    if ("23:30".equals(timeFrom)) {
                        appointmentsTimeCanBeMdeResp.setTo(LocalTime.parse("23:59"));
                    } else {
                        appointmentsTimeCanBeMdeResp.setTo(LocalTime.parse(it.getTo()));
                    }
                    appointmentsTimeCanBeMdeRespList.add(appointmentsTimeCanBeMdeResp);
                }
            }
        }

        return appointmentsTimeCanBeMdeRespList;
    }

    /**
     * 判断会议室是否有门磁设备
     *
     * @param roomId 会议室ID
     * @return
     * @author Yang.Lee
     */
    @Override
    @Transactional
    public ServiceResult ifRoomHasDoorMagnetic(String roomId) {
        //根据id查询会议室信息
        Optional<Room> roomOptional = roomDao.findById(roomId);

        // 判断会议室是否存在
        if (!roomOptional.isPresent()) {
            return ServiceResult.error("会议室ID错误，未查询到会议室信息");
        }

        Room room = roomOptional.get();
        // 查询该room是否有门磁
        AtomicBoolean hasDoorMagnetic = new AtomicBoolean(false);
        if (null != room.getSpace()) {
            List<DeviceToSpace> d2sList = deviceToSpaceDao.findAllBySpaceId(room.getSpace().getId());
            if (!d2sList.isEmpty()) {
                d2sList.forEach(d2s -> {
                    Optional<DoorPlate> doorPlate = doorPlateDao.findByDeviceId(d2s.getDevice().getId());
                    // 根据门牌信息中的门磁编号确定是否存在门磁
                    if (doorPlate.isPresent() && doorPlate.get().getMagneticNum() != null) {
                        hasDoorMagnetic.set(true);
                    }
                });
            }
        }
        return ServiceResult.ok(new RoomDoorMagneticResp(hasDoorMagnetic.get()));
    }

    /**
     * @author yanzy
     * @date 2021/12/31
     * 欢迎语设置
     */
    @Override
    @Transactional
    public ServiceResult welcome(WelcomeWordReq req) {

        Optional<ReservationRecord> reservationRecordOp = reservationRecordDao.findById(req.getRecordId());
        if (reservationRecordOp.isEmpty()) {
            return ServiceResult.error("设置失败，未找到会议信息");
        }
        ReservationRecord reservationRecord = reservationRecordOp.get();
        reservationRecord.setWelcomeWords(req.getWelcomeWords());
        reservationRecordDao.save(reservationRecord);
        return ServiceResult.ok(reservationRecord.getWelcomeWords());
    }

    @Override
    public List<AudioDetailResp> getReservationAudioList(String id) {

        List<AudioDetailResp> audioDetailRespList = new ArrayList<>();
        // 根据会议记录ID获取音频
        List<AudioRecord> audioRecordList = audioDao.findByReservationRecordIdAndAudioFilePathNotNullOrderByCreatedTimeDesc(id);
        for (AudioRecord audioRecord : audioRecordList) {
            ServiceResult result = audioService.getRealTimeAudioRecord(audioRecord.getId());
            if (result.isSuccess()) {
                AudioDetailResp audioDetailResp = (AudioDetailResp) result.getObject();
                audioDetailRespList.add(audioDetailResp);
            }
        }

        return audioDetailRespList;
    }

    @Override
    public ServiceResult finish(String id, String hrId) {
        // 根据 id 查询会议预约记录
        Optional<ReservationRecord> recordOp = reservationRecordDao.findById(id);
        // 判断是否存在
        if (recordOp.isPresent()) {

            ReservationRecord reservationRecord = recordOp.get();

            if (!reservationRecord.getInitiatorEmpNo().equals(hrId)) {
                return ServiceResult.error("不是发起人,无权限操作");
            }

            // 如果已经被取消则不要再次执行取消操作
            if (reservationRecord.getStatus().equals(FINISH)) {
                return ServiceResult.error("会议已结束，请勿重复操作");
            }

            LocalDateTime from = reservationRecord.getFrom();
            LocalDateTime to = reservationRecord.getTo();
            LocalDateTime now = LocalDateTime.now();

            if (now.compareTo(from) < 0) {
                return ServiceResult.error("会议未开始");
            }

            if (now.compareTo(to) > 0) {
                return ServiceResult.error("会议已结束");
            }

            // 结束会议
            reservationRecord.setTo(LocalDateTime.now());
            reservationRecord.setStatus(FINISH);
            reservationRecordDao.save(reservationRecord);

            log.info("数据库更新完成！！");

            // 推送消息给会议App
            intelligentMeetingAppService.pushMeetingRoomDetail(reservationRecord.getRoom().getId());
        } else {
            return ServiceResult.error("会议记录不存在");
        }
        log.info(String.valueOf(recordOp));


        return ServiceResult.ok();
    }

    @Override
    public ServiceResult uploadUrl(ReservationUploadUrlReq reservationUploadUrlReq) {

        Optional<ReservationRecord> recordOptional = reservationRecordDao.findById(reservationUploadUrlReq.getReservationRecordId());
        if (recordOptional.isEmpty()) {
            return ServiceResult.error("ID有误,记录不存在");
        }

        //会议链接
        meetingFileService.createMeetingUrl(recordOptional.get(), reservationUploadUrlReq, TokenUtils.getToken());

        return ServiceResult.ok();
    }

    /**
     * 创建会议（仅创建会议，不包含访客、消息等业务逻辑）
     *
     * @param req 请求参数
     * @return 创建结果，创建成功返回会议id(IdResp对象)
     * @author Yang.Lee
     * @date 2022/4/11 10:21
     **/
    @Override
    @Transactional
    public ServiceResult createMeeting(ReservationAddReq req) {

        //校验参会人是否大于会议室人数容量
        List<SysUserBean> participantList = req.getParticipantList();
        if (Objects.nonNull(participantList) && req.getIgnoreCapacity() != 1) {
            Optional<Room> byId = roomDao.findById(req.getRoomId());
            if (byId.isPresent()) {
                Room room = byId.get();
                if (room.getCapacity() < participantList.size()) {
                    //返回错误信息
                    return ServiceResult.error("会议邀请人数超出会议室容纳上限", ErrConst.E01);
                }
            }
        }

        // 判断时间段是否可用
        String roomId = req.getRoomId();
        Set<OpeningHours> byRoomId = openingHoursDao.findByRoomId(roomId);
        List<OpeningHours> openingHours = new ArrayList<>(byRoomId);
        OpeningHours openingHour = openingHours.get(0);
        // 请求的 from,to
        LocalDateTime reqFrom = req.getFrom();
        LocalDateTime reqTo = req.getTo();
        // 格式化组件
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DateTimeFormatter df2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        // 数据库字符串 from,to
        String from = openingHour.getFrom();
        String to = openingHour.getTo();
        // 获得日期
        String dateStr = df.format(reqFrom);
        // 字符串拼接
        from = dateStr + " " + from;
        to = dateStr + " " + to;
        // 数据库 LocalDateTime from,to
        LocalDateTime dbFrom = LocalDateTime.parse(from, df2);
        LocalDateTime dbTo = LocalDateTime.parse(to, df2);
        // 比较时间先后
        if (reqFrom.isBefore(dbFrom) || reqTo.isAfter(dbTo)) {
            throw new BizLogicNotMatchException("悟能,你又粗心了,时间段不可用");
        }
        //保存 reservationRecord
        ReservationRecord reservationRecord = new ReservationRecord();
        BeanUtils.copyProperties(req, reservationRecord);
        reservationRecord.setTo(reservationRecord.getTo());
        //设置发起人
        reservationRecord.setInitiator(req.getInitiator().getSysId());
        // 保存数据使用工号
        reservationRecord.setInitiatorEmpNo(req.getInitiator().getEmpNo());
        //设置会议室房间
        Optional<Room> roomOptional = roomDao.findById(roomId);

        if (roomOptional.isPresent()) {
            Room room = roomOptional.get();
            reservationRecord.setRoom(room);

            // 判断会议室是否存在门磁设备。如果存在则写入免打扰信息,不存在则将免打扰设为null
            ServiceResult serviceResult = ifRoomHasDoorMagnetic(room.getId());
            RoomDoorMagneticResp hasDoorMagnetic = (RoomDoorMagneticResp) serviceResult.getObject();
            if (hasDoorMagnetic.isHasMagnetic() && req.getNoDisturb() != null) {
                reservationRecord.setNoDisturb(req.getNoDisturb() == 1);
            } else {
                reservationRecord.setNoDisturb(null);
            }
        }

        //初始化状态
        reservationRecord.setStatus(DEFAULT);

        //设置会议大屏状态
        if (req.getScreenStatus() != null) {
            setScreenStatus(roomId, reservationRecord, req.getScreenStatus());
        }

        //保存
        reservationRecordDao.save(reservationRecord);

        //保存会议文档
        if (!CollectionUtils.isEmpty(req.getMeetingFileList())) {
            meetingFileService.createMeetingFile(reservationRecord, req.getMeetingFileList(), TokenUtils.getToken());
        }


        if (StringUtils.isNoneEmpty(reservationRecord.getService())) {
            weChatService.specialRequirementsReminders(reservationRecord);
        }

        // 保存会议邀请卡
        InvitationCard card = new InvitationCard();
        if (!StringUtils.isBlank(req.getReason())) {
            card.setInvitationCode(req.getInvitationCardNo());
            creatInvitation(req, reservationRecord, card);
        }

        //获取会议预约 创建人
        SysUserBean initiator = req.getInitiator();
        String initiatorId = initiator.getSysId();
        String initiatorEmpNo = initiator.getEmpNo();
        //获取会议预约 参与人清单

        //循环保存 参与人 会议预约明细
        if (participantList != null) {
            participantList.forEach(it -> {
                ReservationRecordItem rri = new ReservationRecordItem();
                //初始状态是:未确定
                rri.setStatus(ReservationRecordItemStatus.UNDETERMINED);
                //设置创建人
                rri.setInitiatorEmpNo(initiator.getEmpNo());
                rri.setInitiator(initiator.getSysId());
                //设置拥有人
                rri.setOwnerEmpNo(it.getEmpNo());
                String sysId = it.getSysId();
                rri.setOwner(sysId);
                rri.setIsGroup(it.getIsGroup());
                //设置所属预约记录
                rri.setReservationRecord(reservationRecord);

                // 保存访客邀请记录
                if (it.getIsGuest() == 1) {
                    GuestInvitationRecord record = new GuestInvitationRecord();
                    record.setName(it.getNameZh());
                    record.setMobile(it.getEmpNo());
                    record.setGuestInvitationStatus(GuestInvitationStatus.DEFAULT);
                    record.setInvitationCard(card);
                    // 生成访客邀请记录码
                    CRC32 crc32 = new CRC32();
                    crc32.update(UUID.randomUUID().toString().getBytes(StandardCharsets.UTF_8));
                    String value = String.valueOf(crc32.getValue());
                    record.setCode(value);
                    recordDao.save(record);
                    // 根据邀请函和手机号获取会议访客详情
                    Optional<GuestDetail> op = detailDao.findByInvitationAndMobile(card, it.getEmpNo());
                    if (op.isPresent()) {
                        GuestDetail guestDetail = op.get();
                        guestDetail.setInvitation(card);
                        guestDetail.setName(it.getNameZh());
                        guestDetail.setMobile(it.getEmpNo());
                        detailDao.save(guestDetail);
                    } else {
                        GuestDetail guestDetail = new GuestDetail();
                        guestDetail.setInvitation(card);
                        guestDetail.setCompany("");
                        guestDetail.setName(it.getNameZh());
                        guestDetail.setPhotoUrl("");
                        guestDetail.setJobNum("");
                        guestDetail.setMobile(it.getEmpNo());
                        detailDao.save(guestDetail);
                    }

                }

                //保存会议预约明细
                if (!initiatorEmpNo.equals(rri.getOwnerEmpNo()) && it.getIsGuest() == 0) {
                    reservationRecordItemDao.save(rri);
                }
            });
        }

        //保存 发起人 会议预约明细
        ReservationRecordItem rri = new ReservationRecordItem();
        rri.setStatus(ReservationRecordItemStatus.CONFIRM);

        rri.setInitiator(initiatorId);
        rri.setInitiatorEmpNo(initiatorEmpNo);
        rri.setOwner(initiatorId);
        rri.setOwnerEmpNo(initiatorEmpNo);
        rri.setReservationRecord(reservationRecord);
        //保存用户信息

        sysUserService.trySaveIfNeed(initiatorEmpNo);
        //保存会议预约明细
        reservationRecordItemDao.save(rri);

        Set<ReservationRecordItem> reservationRecordItemSet = reservationRecordItemDao.findByReservationRecordId(reservationRecord.getId());
        if (req.getIgnoreSchedule() != null && req.getIgnoreSchedule() != 1) {
            reservationRecordItemSet.forEach(obj -> {
                // 保存参会人日程数据
                scheduleService.create(reservationRecord, obj.getOwnerEmpNo());
            });
        }

        if (!CollectionUtils.isEmpty(req.getUserGroupList())) {
            req.getUserGroupList().forEach(it -> {
                Optional<CustomUserGroup> byId = customUserGroupDao.findById(it);
                if (byId.isPresent()) {
                    ReservationRecord2CustomGroup record2CustomGroup = new ReservationRecord2CustomGroup();
                    record2CustomGroup.setCustomUserGroup(byId.get());
                    record2CustomGroup.setReservationRecord(reservationRecord);
                    record2CustomGroupDao.save(record2CustomGroup);
                }
            });
        }

        // 推送消息给会议App
        intelligentMeetingAppService.pushMeetingRoomDetail(roomId);

        return ServiceResult.ok(new IdResp(reservationRecord.getId()));
    }

    /**
     * 更新会议信息（仅更新会议，不包含访客、消息等业务逻辑））
     *
     * @param reservationId        会议ID
     * @param reservationUpdateReq 参数
     * @return 更新结果
     * @author Yang.Lee
     * @date 2022/4/11 13:47
     **/
    @Override
    @Transactional
    public ServiceResult updateMeeting(String reservationId, ReservationUpdateReq reservationUpdateReq) {

        List<GuestInvitationRecord> records = new ArrayList<>();
        //根据 会议记录id 获取会议记录
        Optional<ReservationRecord> reservationRecordOptional = reservationRecordDao.findById(reservationId);
        //获取 开始时间
        LocalDateTime localDateTimeFrom = reservationUpdateReq.getFrom();
        //获取结束时间
        LocalDateTime localDateTimeTo = reservationUpdateReq.getTo();
        //获取 房间号
        String roomId = reservationUpdateReq.getRoomId();

        // 原数据中所有参会人集合
        Set<String> userSet = new HashSet<>();
        Set<ReservationRecordItem> reservationRecordItemSet = reservationRecordItemDao.findByReservationRecordId(reservationId);
        reservationRecordItemSet.forEach(it ->
                userSet.add(it.getOwnerEmpNo())
        );

        boolean present = reservationRecordOptional.isPresent();
        if (present) {

            ReservationRecord reservationRecord = reservationRecordOptional.get();
            // 在会议结束前，才可以编辑会议
            LocalDateTime to = reservationRecord.getTo();
            LocalDateTime now = LocalDateTime.now();
            if (DateUtil.isExceed(to, now, HOUR)) {
                return ServiceResult.error("在会议结束前，才可以编辑会议");
            }
            // 判断时间是否有改动
            if (!reservationRecord.getFrom().equals(localDateTimeFrom) || !reservationRecord.getTo().equals(localDateTimeTo)) {
                log.info("开始时间 或 结束时间 不相同");
                log.info("更新。。。。。");
                reservationRecord.setDate(reservationUpdateReq.getDate());
                reservationRecord.setFrom(reservationUpdateReq.getFrom());
                reservationRecord.setTo(reservationUpdateReq.getTo());

            }

            //判断会议主题是否有改动
            if (!reservationRecord.getTopic().equals(reservationUpdateReq.getTopic())) {
                //设置主题
                reservationRecord.setTopic(reservationUpdateReq.getTopic());
            }
            //判断 会议室房间是否有改动
            if (!reservationRecord.getRoom().getId().equals(roomId)) {
                Optional<Room> room = roomDao.findById(roomId);
                boolean roomPresent = room.isPresent();
                if (roomPresent) {
                    reservationRecord.setRoom(room.get());
                }
            }

            // 判断会议室是否存在门磁设备。如果存在则写入免打扰信息,不存在则将免打扰设为null
            ServiceResult serviceResult = ifRoomHasDoorMagnetic(reservationUpdateReq.getRoomId());
            RoomDoorMagneticResp hasDoorMagnetic = (RoomDoorMagneticResp) serviceResult.getObject();
            if (hasDoorMagnetic.isHasMagnetic() && reservationUpdateReq.getNoDisturb() != null) {
                reservationRecord.setNoDisturb(reservationUpdateReq.getNoDisturb() == 1);
            } else {
                reservationRecord.setNoDisturb(null);
            }

            reservationRecord.setRemark(reservationUpdateReq.getRemark());
            reservationRecord.setOriginalTo(reservationRecord.getTo());

            // 设置欢迎语
            reservationRecord.setWelcomeWords(reservationUpdateReq.getWelcomeWords());

            if (reservationUpdateReq.getScreenStatus() != null) {
                //设置会议大屏状态
                setScreenStatus(roomId, reservationRecord, reservationUpdateReq.getScreenStatus());
            }

            //更新 会议记录
            reservationRecordDao.save(reservationRecord);

            if (!CollectionUtils.isEmpty(reservationUpdateReq.getMeetingFileList())) {
                // 更新会议文档
                meetingFileService.updateMeetingFile(reservationRecord, reservationUpdateReq.getMeetingFileList(), TokenUtils.getToken());
            }

            List<ReservationRecordItem> reservationRecordItemList = new ArrayList<>();

            List<UserStatusBean> list = reservationUpdateReq.getParticipantList();
            Set<String> reqSet = new HashSet<>();
            if (Objects.nonNull(list)) {
                for (UserStatusBean userStatusBean : list) {
                    reqSet.add(userStatusBean.getEmpNo());
                }
            }
            //获取会议发起人ID
            SysUserBean initiator = reservationUpdateReq.getInitiator();
            String initiatorEmpNo = initiator.getEmpNo();

            if (reservationUpdateReq.getIgnoreSchedule() != null && reservationUpdateReq.getIgnoreSchedule() != 1) {
                // 变更发起人的日程状态
                scheduleService.update(reservationRecord, initiatorEmpNo);
            }


            reqSet.add(initiatorEmpNo);
            //获取删除的参会人员
            Set<String> result = new HashSet<>();
            result.addAll(userSet);
            result.removeAll(reqSet);
            if (result != null && !result.isEmpty()) {
                for (String str : result) {
                    log.info("删除的参会人员:" + str);
                    Optional<ReservationRecordItem> reservationRecordItemOptional = reservationRecordItemDao.findByReservationRecordIdAndOwnerEmpNo(reservationId, str);
                    if (reservationRecordItemOptional.isPresent()) {
                        //获取被删除的参会人员记录
                        ReservationRecordItem recordItem = reservationRecordItemOptional.get();
                        recordItem.setStatus(ReservationRecordItemStatus.CANCEL);
                        recordItem.setIsDelete(1);
                        reservationRecordItemDao.save(recordItem);

                        if (reservationUpdateReq.getIgnoreSchedule() != null && reservationUpdateReq.getIgnoreSchedule() != 1) {
                            // 给删除的人取消日程数据
                            scheduleService.cancel(reservationRecord, str);
                        }

                    }
                }
            }

            // 获取邀请函记录
            // 更新 预约 item 表记录
            if (Objects.nonNull(reservationUpdateReq.getParticipantList())) {

                for (UserStatusBean it : reservationUpdateReq.getParticipantList()) {

                    // 判断是否访客
                    if (it.getIsGuest() == 0) {
                        // 根据 item id 和 参会人工号 获取记录
                        Optional<ReservationRecordItem> reservationRecordItemOptional = reservationRecordItemDao.findByReservationRecordIdAndOwnerEmpNo(reservationId, it.getEmpNo());
                        boolean reservationRecordItemOptionalPresent = reservationRecordItemOptional.isPresent();
                        if (reservationRecordItemOptionalPresent) {
                            //过滤到会议发起人
                            if (!it.getEmpNo().equals(reservationUpdateReq.getInitiator().getEmpNo()) && reservationRecordItemOptional.get().getStatus() != it.getStatus()) {
                                //修改参会人状态
                                //若发现是已有人员 更新状态

                                reservationRecordItemList.add(reservationRecordItemOptional.get());

                                if (reservationUpdateReq.getIgnoreSchedule() != null && reservationUpdateReq.getIgnoreSchedule() != 1) {
                                    // 给变更的人修改他的日程数据
                                    scheduleService.update(reservationRecord, it.getEmpNo());
                                }

                            }

                        } else {
                            //若 发现是新增人员 新增参会人 并将状态设置成 UNDETERMINED
                            ReservationRecordItem reservationRecordItem = new ReservationRecordItem();
                            reservationRecordItem.setStatus(ReservationRecordItemStatus.UNDETERMINED);
                            reservationRecordItem.setOwner(it.getSysId());
                            reservationRecordItem.setOwnerEmpNo(it.getEmpNo());
                            reservationRecordItem.setInitiator(reservationUpdateReq.getInitiator().getSysId());
                            reservationRecordItem.setInitiatorEmpNo(reservationUpdateReq.getInitiator().getEmpNo());
                            reservationRecordItem.setReservationRecord(reservationRecord);
                            reservationRecordItem.setIsGroup(it.getIsGroup());
                            reservationRecordItemList.add(reservationRecordItem);

                            if (reservationUpdateReq.getIgnoreSchedule() != null && reservationUpdateReq.getIgnoreSchedule() != 1) {
                                // 给新加入的人添加一条日程数据
                                scheduleService.create(reservationRecord, it.getEmpNo());
                            }

                        }
                    }
                    // 判断是否是访客
                    if (it.getIsGuest() == 1) {

                        Optional<InvitationCard> invitationCardOptional = invitationCardDao.findByInvitationCode(reservationUpdateReq.getInvitationCardNo());
                        if (invitationCardOptional.isEmpty()) {
                            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                            return ServiceResult.error("未找到访客邀请卡");
                        }

                        InvitationCard finalCard = invitationCardOptional.get();

                        // 根据会议记录和手机号获取访客邀请记录
                        Optional<GuestInvitationRecord> guestOp = recordDao.findByInvitationCardReservationRecordIdAndMobile(reservationId, it.getEmpNo());
                        if (guestOp.isPresent()) {
                            GuestInvitationRecord record = guestOp.get();
                            record.setMobile(it.getEmpNo());
                            record.setName(it.getNameZh());
                            records.add(record);
                        } else {
                            GuestInvitationRecord record = new GuestInvitationRecord();
                            record.setName(it.getNameZh());
                            record.setMobile(it.getEmpNo());
                            record.setGuestInvitationStatus(GuestInvitationStatus.DEFAULT);
                            record.setInvitationCard(finalCard);
                            // 生成访客邀请记录码
                            CRC32 crc32 = new CRC32();
                            crc32.update(UUID.randomUUID().toString().getBytes(StandardCharsets.UTF_8));
                            String value = String.valueOf(crc32.getValue());
                            record.setCode(value);
                            // 给新增人员发送短信
//                            smsService.asyncSendMeetingInvitationSMS(user.getNameZh(), user.getMobile(), record.getMobile(), finalCard.getInvitationCode(), record.getCode());
                            records.add(record);
                        }

                        // 根据邀请函和手机号获取会议访客详情
                        Optional<GuestDetail> op = detailDao.findByInvitationAndMobile(finalCard, it.getEmpNo());
                        if (op.isPresent()) {
                            GuestDetail guestDetail = op.get();
                            guestDetail.setInvitation(finalCard);
                            guestDetail.setName(it.getNameZh());
                            guestDetail.setMobile(it.getEmpNo());
                            detailDao.save(guestDetail);
                        } else {
                            GuestDetail guestDetail = new GuestDetail();
                            guestDetail.setInvitation(finalCard);
                            guestDetail.setCompany("");
                            guestDetail.setName(it.getNameZh());
                            guestDetail.setPhotoUrl("");
                            guestDetail.setJobNum("");
                            guestDetail.setMobile(it.getEmpNo());
                            detailDao.save(guestDetail);
                        }
                    }

                }

            }

            // 变更已选会议组信息
            // 删除之前所以已选
            List<ReservationRecord2CustomGroup> record2CustomGroupList = new ArrayList<>();
            List<ReservationRecord2CustomGroup> byReservationRecord = record2CustomGroupDao.findByReservationRecord(reservationRecord);
            byReservationRecord.forEach(it -> {
                it.setIsDelete(1);
                record2CustomGroupList.add(it);
            });
            if (!CollectionUtils.isEmpty(reservationUpdateReq.getUserGroupList())) {
                reservationUpdateReq.getUserGroupList().forEach(it -> {
                    Optional<CustomUserGroup> byId = customUserGroupDao.findById(it);
                    if (byId.isPresent()) {
                        ReservationRecord2CustomGroup record2CustomGroup = new ReservationRecord2CustomGroup();
                        record2CustomGroup.setCustomUserGroup(byId.get());
                        record2CustomGroup.setReservationRecord(reservationRecord);
                        record2CustomGroupList.add(record2CustomGroup);
                    }
                });
            }
            record2CustomGroupDao.saveAll(record2CustomGroupList);
            reservationRecordItemDao.saveAll(reservationRecordItemList);
        }

        // 删除变更访客人员
        List<GuestDetail> details = new ArrayList<>();
        List<GuestInvitationRecord> recordList = recordDao.findByInvitationCardReservationRecord(reservationRecordOptional.get());
        for (GuestInvitationRecord record : recordList) {
            if (!records.toString().contains(record.getMobile())) {
                record.setIsDelete(1);
                records.add(record);
                if (!StringUtils.isBlank(record.getInviterCode())) {
                    deleteRecord(record, records);
                }
                Optional<GuestDetail> detailOp = detailDao.findByInvitationAndMobile(record.getInvitationCard(), record.getMobile());
                if (detailOp.isPresent()) {
                    GuestDetail detail = detailOp.get();
                    detail.setIsDelete(1);
                    details.add(detail);
                }
            }
        }
        detailDao.saveAll(details);
        recordDao.saveAll(records);

        // 推送消息给会议App
        intelligentMeetingAppService.pushMeetingRoomDetail(roomId);

        return ServiceResult.ok();
    }

    /**
     * 取消会议（仅取消会议，不包含访客、消息等业务逻辑））
     *
     * @param id 会议ID
     * @return 取消会议
     * @author Yang.Lee
     * @date 2022/4/11 13:47
     **/
    @Override
    @Transactional
    public ServiceResult cancelMeeting(String id) {

        // 根据 id 查询会议预约记录
        Optional<ReservationRecord> recordOptional = reservationRecordDao.findById(id);
        boolean present = recordOptional.isPresent();
        // 判断是否存在
        if (present) {

            ReservationRecord reservationRecord = recordOptional.get();

            // 如果已经被取消则不要再次执行取消操作
            if (reservationRecord.getStatus().equals(CANCEL)) {
                return ServiceResult.error("会议已取消，请勿重复操作");
            }

            // 发起人在会议开始后的半小时内，以及开始前，可以取消会议、编辑会议
            // 2021-04-22 ： 新需求，会议结束之前都可以取消会议

            LocalDateTime to = reservationRecord.getTo();
            LocalDateTime now = LocalDateTime.now();

            if (now.compareTo(to) > 0) {
                return ServiceResult.error("会议已结束,无法取消");
            }

            // 取消会议
            reservationRecord.setStatus(CANCEL);
            Set<ReservationRecordItem> reservationRecordItemSet = reservationRecordItemDao.findByReservationRecordId(id);
            reservationRecordItemSet.forEach(it ->
                // 2021-04-21 ： 前端重构后该需求发生变化，  会议取消后，参会人状态不变，不再默认修改为“CANCEL”状态
                // 取消参会人日程数据
                scheduleService.cancel(reservationRecord, it.getOwnerEmpNo())
            );
            reservationRecordDao.save(reservationRecord);

            // 推送消息给会议App
            intelligentMeetingAppService.pushMeetingRoomDetail(reservationRecord.getRoom().getId());
        } else {
            return ServiceResult.error("会议记录不存在");
        }

        return ServiceResult.ok();

    }

    /**
     * 获取空闲会议室列表
     *
     * @param req 查询参数
     * @return 空闲会议室列表
     * @author Yang.Lee
     * @date 2022/4/11 16:34
     **/
    @Override
    @Transactional
    public List<RoomWithReservationResp> getEmptyRoom(MeetingRoomSearchReq req) {

        // 查询会议室列表
        List<Room> roomList = roomDao.findAll((root, query, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();

            // 只查询开放的会议室
            list.add(criteriaBuilder.equal(root.get("isOpen"), 1));

            if (StringUtils.isNotBlank(req.getProjectArea())) {
                list.add(criteriaBuilder.equal(root.get("space").get("project").get("projectArea").get("name"), req.getProjectArea()));
            }

            if (req.getNumber() != null) {
                list.add(criteriaBuilder.greaterThanOrEqualTo(root.get("capacity"), req.getNumber()));
            }

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        });

        // 循环处理room
        List<RoomWithReservationResp> roomRespList = new ArrayList<>();

        for (Room room : roomList) {
            // 封装为 resp
            RoomWithReservationResp roomResp = new RoomWithReservationResp();
            BeanUtils.copyProperties(room, roomResp);
            roomResp.setLocation(SpaceService.getFullSpaceName(room.getSpace(), room.getSpace().getName()));
            String roomId = room.getId();
            roomResp.setRoomId(roomId);
            //设置日期
            List<RoomDevice> deviceList = roomDeviceDao.findByRoomIdOrderByName(roomId);
            List<String> deviceNameList = deviceList.stream().map(RoomDevice::getName).collect(Collectors.toList());
            roomResp.setDeviceList(deviceNameList);

            if (req.getStartTime() != null && req.getEndTime() != null) {
                roomResp.setTimeSlotResultRespList(getBlankTimeSlotList(roomId, req.getStartTime(), req.getEndTime()));

            } else {
                roomResp.setTimeSlotResultRespList(getBlankTimeSlotList(roomId));

            }

            roomRespList.add(roomResp);
        }

//        //step2: timeSlotDao 查询 timeSlot (半小时间隔)
//        //按房间循环
        for (RoomWithReservationResp roomResp : roomRespList) {
            List<TimeSlot> timeSlotList = timeSlotDao.findByRoomIdOrderBySeqAsc(roomResp.getRoomId());
            setStatusByTimeSlot(roomResp, timeSlotList);
        }

        //按房间循环
        for (RoomWithReservationResp roomResp : roomRespList) {
            //根据当前日期查询 findByDateAndRoomIdOrderByFromAsc
            List<ReservationRecord> reservationRecordList;
            if (req.getStartTime() != null && req.getEndTime() != null) {
                reservationRecordList = reservationRecordDao.findByDateBetweenAndRoomIdOrderByFromAscToAsc(req.getStartTime().toLocalDate(), req.getEndTime().toLocalDate(), roomResp.getRoomId());
            } else {
                reservationRecordList = reservationRecordDao.findByDateAndRoomIdOrderByFromAscToAsc(LocalDate.now(), roomResp.getRoomId());
            }

            setStatusByReservationRecordSet(roomResp, reservationRecordList);
        }

        // 1. 筛选空闲会议室, 将已有会议的数据移除
        if (req.getEmptyOnly() == 1) {

            // 计算请求需要多个时间槽

            Long timeslotNumber = DateUtils.betweenTime(req.getStartTime(), req.getEndTime(), DateUnit.MINUTE) / 30;

            Iterator<RoomWithReservationResp> iterator = roomRespList.iterator();

            while (iterator.hasNext()) {
                RoomWithReservationResp room = iterator.next();
                Iterator<TimeSlotResultResp> timeSlotIterator = room.getTimeSlotResultRespList().iterator();

                while (timeSlotIterator.hasNext()) {

                    TimeSlotResultResp timeSlot = timeSlotIterator.next();

                    if(StringUtils.isNotBlank(req.getReservationId()) && req.getReservationId().equals(timeSlot.getRecordId())){
                        continue;
                    }

                    if (StringUtils.isNotBlank(timeSlot.getRecordId()) ) {
                        timeSlotIterator.remove();
                    }
                }

                if (room.getTimeSlotResultRespList().size() < timeslotNumber) {
                    iterator.remove();
                }
            }
        }

        return roomRespList;
    }

    @Override
    public ServiceResult getMeetingDeadline() {
        //查询所有会议开发时间，并按结束时间倒叙排列
        List<OpeningHours> openingHoursList = openingHoursDao.findAllByRoomIsOpenOrderByToDesc(1);
        if (openingHoursList.isEmpty()) {
            return ServiceResult.error("未找到会议室开发时间");
        }
        //获取最晚开发时间
        OpeningHours openingHours = openingHoursList.get(0);
        return ServiceResult.ok(openingHours.getTo());
    }

    @Override
    @Transactional
    public PageBean<RoomWithReservationResp> getRecommendMeeting(PageBean<RoomWithReservationResp> pageBean, RecommendMeetingReq req) {

        Optional<Project> projectOp = projectDao.findById(req.getProjectId());
        if (projectOp.isEmpty()) {
            PageBean<RoomWithReservationResp> result = new PageBean<>();
            result.setList(new ArrayList<>());
            return result;
        }
        Project project = projectOp.get();

        MeetingRoomSearchReq meetingRoomSearchReq = new MeetingRoomSearchReq();
        meetingRoomSearchReq.setProjectArea(project.getProjectArea().getName());
        meetingRoomSearchReq.setStartTime(req.getStartTime());
        meetingRoomSearchReq.setEndTime(req.getEndTime());
        if (req.getNumber() != null) {
            meetingRoomSearchReq.setNumber(req.getNumber());
        }

        int durationSize = req.getMeetingDuration() / 30;

        //调用 获取空闲会议室列表 方法
        List<RoomWithReservationResp> emptyRoom = getEmptyRoom(meetingRoomSearchReq);

        //获取roomId
        List<String> roomIdList = emptyRoom.stream()
                .map(RoomWithReservationResp::getRoomId)
                .collect(Collectors.toList());

        for (RoomWithReservationResp roomWithReservationResp : emptyRoom) {
            //该时间段未查到会议记录
            if (CollUtil.isEmpty(roomWithReservationResp.getTimeSlotResultRespList())) {
                roomIdList.remove(roomWithReservationResp.getRoomId());
            }

            roomWithReservationResp.getTimeSlotResultRespList().forEach(it -> {
                //该时间段有会议预约
                if (it.getStatus() == 2 || roomWithReservationResp.getTimeSlotResultRespList().size() != durationSize) {
                    roomIdList.remove(roomWithReservationResp.getRoomId());
                }
            });
        }

        List<Room> roomList = roomDao.findAllByIdIn(roomIdList);

        int roomsSize = roomList.size();
        int page = pageBean.getPage();
        int size = pageBean.getSize();

        // 手动对查询结果进行分页
        int subStart = page * size;
        int subEnd = subStart + size;

        if (subStart > roomsSize) {
            subStart = roomsSize;
        }

        if (subEnd > roomsSize) {
            subEnd = roomsSize;
        }
        List<Room> pageList = roomList.subList(subStart, subEnd);

        // 循环处理rooms
        List<RoomWithReservationResp> roomRespList = new ArrayList<>();

        for (Room room : pageList) {
            // 封装为 resp
            RoomWithReservationResp roomResp = new RoomWithReservationResp();
            BeanUtils.copyProperties(room, roomResp);
            roomResp.setLocation(SpaceService.getFullSpaceName(room.getSpace(), room.getSpace().getName()));
            String roomId = room.getId();
            roomResp.setRoomId(roomId);
            //设置日期
            roomResp.setDate(req.getDate());
            //设备名称
            List<RoomDevice> deviceList = roomDeviceDao.findByRoomIdOrderByName(roomId);
            List<String> deviceNameList = deviceList.stream().map(RoomDevice::getName).collect(Collectors.toList());
            roomResp.setDeviceList(deviceNameList);

            roomResp.setTimeSlotResultRespList(getBlankTimeSlotList(roomId, req.getDate()));

            roomRespList.add(roomResp);
        }

        //按房间循环
        for (RoomWithReservationResp roomResp : roomRespList) {
            List<TimeSlot> timeSlotList = timeSlotDao.findByRoomIdOrderBySeqAsc(roomResp.getRoomId());
            setStatusByTimeSlot(roomResp, timeSlotList);
        }

        //按房间循环
        for (RoomWithReservationResp roomResp : roomRespList) {
            //根据当前日期查询 findByDateAndRoomIdOrderByFromAsc
            List<ReservationRecord> reservationRecordList = reservationRecordDao.findByDateAndRoomIdOrderByFromAscToAsc(roomResp.getDate(), roomResp.getRoomId());
            setStatusByReservationRecordSet(roomResp, reservationRecordList);
        }

        pageBean.setPage(page);
        pageBean.setSize(size);
        pageBean.setTotalElements(roomsSize);
        pageBean.setTotalPages(roomsSize / size);
        pageBean.setList(roomRespList);

        return pageBean;

    }

    @Override
    @Transactional
    public ServiceResult getRecentTime(RecommendMeetingReq req) {

        Optional<Project> projectOptional = projectDao.findById(req.getProjectId());
        if (projectOptional.isEmpty()) {
            return ServiceResult.error("你还未选择“会议地区”");
        }
        Project project = projectOptional.get();

        //会议时长几个半小时
        int size = req.getMeetingDuration() / 30;

        for (int i = 0; i < 7; i++) {
            //获取预约日期
            //预约日期DateTime转LocalDateTime
            LocalDateTime dateTime = req.getDate().atStartOfDay();
            //预约日期
            LocalDateTime date = DateUtil.plus(dateTime, i, ChronoUnit.DAYS);
            String format = date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

            for (int z = 0; z < 35 - size; z++) {
                //获取会议开始时间
                LocalDateTime time = DateUtil.merge(format, "06:00");
                //会议开始时间
                LocalDateTime startTime = DateUtil.plus(time, 30 * z, ChronoUnit.MINUTES);
                //会议结束时间
                LocalDateTime endTime = DateUtil.plus(startTime, req.getMeetingDuration(), ChronoUnit.MINUTES);
                //调用getEmptyRoom()方法 获取空闲会议室列表
                //创建MeetingRoomSearchReq
                MeetingRoomSearchReq meetingRoomSearchReq = new MeetingRoomSearchReq();
                meetingRoomSearchReq.setProjectArea(project.getProjectArea().getName());
                meetingRoomSearchReq.setStartTime(startTime);
                meetingRoomSearchReq.setEndTime(endTime);
                if (req.getNumber() != null) {
                    meetingRoomSearchReq.setNumber(req.getNumber());
                }
                log.debug("meetingRoomSearchReq:{}", meetingRoomSearchReq);

                List<RoomWithReservationResp> emptyRoom = this.getEmptyRoom(meetingRoomSearchReq);
                log.debug("emptyRoom:{}", emptyRoom);
                if (emptyRoom.isEmpty()) {
                    return ServiceResult.error("未找到合适的会议室");
                }
                //循环emptyRoom
                for (RoomWithReservationResp roomWithReservationResp : emptyRoom) {
                    if (!roomWithReservationResp.getTimeSlotResultRespList().isEmpty()) {
                        MeetingDateResp meetingDateResp = new MeetingDateResp();
                        //获取所有状态
                        List<Integer> status = roomWithReservationResp.getTimeSlotResultRespList().stream().map(TimeSlotResultResp::getStatus).collect(Collectors.toList());
                        long count = status.stream().distinct().count();

                        if (count == 1 && LocalDateTime.now().isBefore(startTime) && status.size() == size && status.get(0) == 1) {
                            //获取会议室开放截止时间
                            List<TimeSlot> timeSlotList = timeSlotDao.findByRoomIdOrderBySeqDesc(roomWithReservationResp.getRoomId());
                            //会议室截止时间
                            LocalDateTime meetingRoomDeadline = DateUtil.merge(format, timeSlotList.get(0).getTo());
                            //获取会议开始时间
                            List<String> startTimeResp = roomWithReservationResp.getTimeSlotResultRespList().stream().map(TimeSlotResultResp::getFrom).collect(Collectors.toList());
                            //获取会议结束时间
                            List<String> endTimeResp = roomWithReservationResp.getTimeSlotResultRespList().stream().map(TimeSlotResultResp::getTo).collect(Collectors.toList());
                            //会议结束时间
                            LocalDateTime meetingDeadline = DateUtil.merge(format, endTimeResp.get(size - 1));
                            if (meetingDeadline.isBefore(meetingRoomDeadline)) {
                                meetingDateResp.setStartDate(format);
                                log.debug("推荐日期{}", format);
                                meetingDateResp.setStartTime(startTimeResp.get(0));
                                log.debug("推荐会议开始时间{}", startTimeResp.get(0));
                            }
                        }
                        if (meetingDateResp.getStartTime() != null) {
                            return ServiceResult.ok(meetingDateResp);
                        }
                    }

                }

            }
        }
        return ServiceResult.error("最近7天无可约会议室");
    }

    @Override
    public ServiceResult getVisitorAddress(String projectId) {

        //根据id查询
        Optional<Project> projectOp = projectDao.findById(projectId);
        if (projectOp.isEmpty()) {
            return ServiceResult.error("未找到对应的项目");
        }

        Project project = projectOp.get();
        //获取访客系统对应的地址
        String visitCompany = project.getVisitCompany();
        return ServiceResult.ok(visitCompany);
    }

    @Override
    public List<VisitorTypeResp> getVisitorType() {

        List<VisitorTypeResp> visitorTypeRespList = new ArrayList<>();

        String realKey = redisUtil.createRealKey(VISITORTYPE);
        //查看redis中是否已存在访客类型
        if (redisUtil.hasKey(realKey)) {
            Object date = redisUtil.get(realKey);
            List<VisitorTypeResp> visitorTypeResps = JSON.parseArray(JSON.toJSONString(date), VisitorTypeResp.class);
            log.debug("从缓存中获取访客类型{}", visitorTypeResps);
            return visitorTypeResps;
        }

        try {
            log.debug("向EHR获取访客类型");
            String data = restTemplateUtil.get(visitorTypeUrl);
            JSONObject jsonData = JSON.parseObject(data);
            JSONArray jsonIndex = jsonData.getJSONArray("data");
            log.debug("从ehr中获取的访客类型{}", JSON.parseArray(jsonIndex.toJSONString(), VisitorTypeResp.class));
            visitorTypeRespList.addAll(JSON.parseArray(jsonIndex.toJSONString(), VisitorTypeResp.class));

        } catch (Exception e) {
            log.error("向EHR获取访客类型");
            log.error(e.getMessage());
        }

        //保存访客类型到redis
        log.debug("保存访客类型到redis");

        redisUtil.set(realKey, visitorTypeRespList, 1800);

        return visitorTypeRespList;
    }

    @Override
    public ServiceResult inviteParticipants(AttendeeReq req, String token) {

        List<GuestInvitationRecord> records = new ArrayList<>();

        //查询会议预约记录
        Optional<ReservationRecord> reservationRecordOp = reservationRecordDao.findById(req.getReservationId());
        if (reservationRecordOp.isEmpty()) {
            return ServiceResult.error("未找到会议预约记录");
        }
        ReservationRecord reservationRecord = reservationRecordOp.get();

        //获取会议预约 创建人
        String initiatorId = reservationRecord.getInitiator();
        String initiatorEmpNo = reservationRecord.getInitiatorEmpNo();

        // 原数据中所有参会人集合
        Set<String> userSet = new HashSet<>();
        Set<ReservationRecordItem> reservationRecordItemSet = reservationRecordItemDao.findByReservationRecordId(reservationRecord.getId());
        reservationRecordItemSet.forEach(it ->
                userSet.add(it.getOwnerEmpNo())
        );

        List<ReservationRecordItem> reservationRecordItemList = new ArrayList<>();

        List<SysUserBean> list = req.getParticipantList();
        Set<String> reqSet = new HashSet<>();
        if (Objects.nonNull(list)) {
            for (SysUserBean SysUserBean : list) {
                reqSet.add(SysUserBean.getEmpNo());
            }
        }

        // 变更发起人的日程状态
        scheduleService.update(reservationRecord, initiatorEmpNo);

        reqSet.add(initiatorEmpNo);
        //获取删除的参会人员
        Set<String> result = new HashSet<>();
        result.addAll(userSet);
        result.removeAll(reqSet);
        if (result != null && !result.isEmpty()) {
            for (String str : result) {
                log.info("删除的参会人员:" + str);
                Optional<ReservationRecordItem> reservationRecordItemOptional = reservationRecordItemDao.findByReservationRecordIdAndOwnerEmpNo(reservationRecord.getId(), str);
                if (reservationRecordItemOptional.isPresent()) {
                    //获取被删除的参会人员记录
                    ReservationRecordItem recordItem = reservationRecordItemOptional.get();
                    recordItem.setStatus(ReservationRecordItemStatus.CANCEL);
                    recordItem.setIsDelete(1);
                    reservationRecordItemDao.save(recordItem);
                    //给删除的参会人员发送取消会议消息
                    log.info("给删除的参会人员=" + str + ",发送取消会议消息");
                    //会议主题
                    String topic = reservationRecord.getTopic();
                    weChatService.channelRequirementsReminders(recordItem, topic);

                    // 给删除的人取消日程数据
                    scheduleService.cancel(reservationRecord, str);
                }
            }
        }

        sysUserServiceUC.getUserByEmpNo(initiatorEmpNo);

        if (Objects.nonNull(req.getParticipantList())) {
            req.getParticipantList().forEach(it -> {
                // 根据 item id 和 参会人工号 获取记录
                Optional<ReservationRecordItem> reservationRecordItemOptional = reservationRecordItemDao.findByReservationRecordIdAndOwnerEmpNo(reservationRecord.getId(), it.getEmpNo());
                boolean reservationRecordItemOptionalPresent = reservationRecordItemOptional.isPresent();
                if (!reservationRecordItemOptionalPresent) {
                    if (it.getIsGuest() == 0) {
                        //若 发现是新增人员 新增参会人 并将状态设置成 UNDETERMINED
                        ReservationRecordItem reservationRecordItem = new ReservationRecordItem();
                        reservationRecordItem.setStatus(ReservationRecordItemStatus.UNDETERMINED);
                        reservationRecordItem.setOwner(it.getSysId());
                        reservationRecordItem.setOwnerEmpNo(it.getEmpNo());
                        reservationRecordItem.setInitiator(initiatorId);
                        reservationRecordItem.setInitiatorEmpNo(initiatorEmpNo);
                        reservationRecordItem.setReservationRecord(reservationRecord);
                        reservationRecordItem.setIsGroup(it.getIsGroup());
                        reservationRecordItemList.add(reservationRecordItem);
                        //推送给新增人 创建会议推送消息
                        log.info("给新增人" + it.getEmpNo() + "，发送创建会议消息");
                        weChatService.createMeetingReminders(it.getEmpNo(), reservationRecord);

                        // 给新加入的人添加一条日程数据
                        scheduleService.create(reservationRecord, it.getEmpNo());
                    }
                    //有访客
                    if (it.getIsGuest() == 1) {
                        SysUser user = sysUserServiceUC.getUserByEmpNo(initiatorEmpNo);
                        //查询邀请函
                        Optional<InvitationCard> cardOptional = invitationCardDao.findByReservationRecord(reservationRecord);
                        if (cardOptional.isEmpty()) {
                            log.error("未找到会议id{}访客邀请函", req.getReservationId());
                            return;
                        }
                        InvitationCard card = cardOptional.get();

                        // 根据会议记录和手机号获取访客邀请记录
                        Optional<GuestInvitationRecord> guestOp = recordDao.findByInvitationCardReservationRecordIdAndMobile(req.getReservationId(), it.getEmpNo());
                        if (guestOp.isPresent()) {
                            GuestInvitationRecord record = guestOp.get();
                            record.setMobile(it.getEmpNo());
                            record.setName(it.getNameZh());
                            records.add(record);
                        } else {
                            GuestInvitationRecord record = new GuestInvitationRecord();
                            record.setName(it.getNameZh());
                            record.setMobile(it.getEmpNo());
                            record.setGuestInvitationStatus(GuestInvitationStatus.DEFAULT);
                            record.setInvitationCard(card);
                            // 生成访客邀请记录码
                            CRC32 crc32 = new CRC32();
                            crc32.update(UUID.randomUUID().toString().getBytes(StandardCharsets.UTF_8));
                            String value = String.valueOf(crc32.getValue());
                            record.setCode(value);
                            // 给新增人员发送短信
                            smsService.asyncSendMeetingInvitationSMS(user.getNameZh(), user.getMobile(), record.getMobile(), card.getInvitationCode(), record.getCode());
                            records.add(record);
                        }
                    }

                }
            });
        }

        // 变更已选会议组信息
        // 删除之前所以已选
        List<ReservationRecord2CustomGroup> record2CustomGroupList = new ArrayList<>();
        List<ReservationRecord2CustomGroup> byReservationRecord = record2CustomGroupDao.findByReservationRecord(reservationRecord);
        byReservationRecord.forEach(it -> {
            it.setIsDelete(1);
            record2CustomGroupList.add(it);
        });
        if (!req.getUserGroupList().isEmpty()) {
            req.getUserGroupList().forEach(it -> {
                Optional<CustomUserGroup> byId = customUserGroupDao.findById(it);
                if (byId.isPresent()) {
                    ReservationRecord2CustomGroup record2CustomGroup = new ReservationRecord2CustomGroup();
                    record2CustomGroup.setCustomUserGroup(byId.get());
                    record2CustomGroup.setReservationRecord(reservationRecord);
                    record2CustomGroupList.add(record2CustomGroup);
                }
            });
        }
        record2CustomGroupDao.saveAll(record2CustomGroupList);
        reservationRecordItemDao.saveAll(reservationRecordItemList);

        List<EhrUserAuth> ehrUserAuths = new ArrayList<>();
        List<String> strings = new ArrayList<>();
        // 删除变更访客人员
        List<GuestDetail> details = new ArrayList<>();
        List<GuestInvitationRecord> recordList = recordDao.findByInvitationCardReservationRecord(reservationRecord);
        for (GuestInvitationRecord record : recordList) {
            if (!records.toString().contains(record.getMobile())) {
                record.setIsDelete(1);
                records.add(record);
                if (!StringUtils.isBlank(record.getInviterCode())) {
                    deleteRecord(record, records);
                }
                Optional<GuestDetail> detailOp = detailDao.findByInvitationAndMobile(record.getInvitationCard(), record.getMobile());
                if (detailOp.isPresent()) {
                    GuestDetail detail = detailOp.get();
                    strings.add(detail.getJobNum());
                    detail.setIsDelete(1);
                    details.add(detail);
                }
            }
        }
        detailDao.saveAll(details);
        List<String> userAuths = new ArrayList<>();
        for (String jobNum : strings) {
            for (EhrUserAuth user : ehrUserAuths) {
                if (user.getJobNum().equals(jobNum)) {
                    userAuths.add(user.getVruid());
                }
            }
        }
        // 循环向ehr拒绝访客
        for (String it : userAuths) {
            ServiceResult result2 = reject(it, token);
            if (!result2.isSuccess()) {
                return ServiceResult.error(result2.getObject());
            }
        }

        recordDao.saveAll(records);

        return ServiceResult.ok(reservationRecord.getRoom().getId());
    }
}
