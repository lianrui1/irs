package cn.com.cowain.ibms.dao.device;

import cn.com.cowain.ibms.entity.centralized.CentralizedControlCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @author wei.cheng
 * @date 2022/09/16 09:18
 */
@Repository
public interface CentralizedControlCodeDao extends JpaRepository<CentralizedControlCode, String>, JpaSpecificationExecutor<CentralizedControlCode> {
    /**
     * 判断是否有名字为{@code name}的集控码
     *
     * @param name
     * @return
     */
    Boolean existsByName(String name);
}
