package cn.com.cowain.ibms.service.bean.amap.poi;

import cn.com.cowain.ibms.service.bean.amap.AMapResp;
import lombok.Data;

/**
 * 坐标转换对象
 *
 * @author Yang.Lee
 * @date 2021/4/19 19:30
 */
@Data
public class CoordinateConvert extends AMapResp {

    /**
     * 转换之后的坐标。若有多个坐标，则用 “;”进行区分和间隔。经度在前，纬度在后
     **/
    private String locations;
}
