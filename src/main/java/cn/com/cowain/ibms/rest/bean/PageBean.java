package cn.com.cowain.ibms.rest.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * 分页组件 类
 *
 * @author Hu Jingling
 * @since 2020-08-04
 */
@Data
@ApiModel("分页组件")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PageBean<E> {

    /**
     * 对list手动分页
     *
     * @param data list数据
     * @param page 页码
     * @param size 页长
     * @return 分页数据
     * @author Yang.Lee
     * @date 2021/9/18 16:49
     **/
    public static <E> PageBean<E> createPageBeanByAllData(List<E> data, int page, int size) {

        PageBean<E> pageBean = new PageBean<>();
        pageBean.setPage(page);
        pageBean.setSize(size);
        pageBean.setTotalElements(data.size());
        pageBean.setList(data);

        int totalPages = 1;
        if (size * page > data.size()) {
            pageBean.setList(new ArrayList<>());
        } else {
            if (data.size() > size) {
                int left = data.size() % size;
                int right = data.size() / size;
                totalPages = left == 0 ? right : right + 1;

                int subStartIndex = page * size;
                int subEndIndex = (page + 1) * size;

                // page = 0 -> 0,2
                // page = 1 -> 2,4
                // page = 2 -> 4,6
                if (subEndIndex > data.size()) {
                    subEndIndex = data.size();
                }

                List<E> pageList = data.subList(subStartIndex, subEndIndex);
                pageBean.setList(pageList);

            }
        }

        pageBean.setTotalPages(totalPages);

        return pageBean;
    }

    /**
     * 有参构造
     *
     * @param page
     * @param size
     */
    public PageBean(int page, int size) {
        this.page = page;
        this.size = size;
    }

    /**
     * 当前页码，初始值0
     * current
     */
    @ApiModelProperty("当前页码，初始值0")
    private int page = 0;

    /**
     * 每页记录数, 初始值10
     * size
     */
    @ApiModelProperty("每页记录数, 初始值10")
    private int size = 10;

    /**
     * 总页数， 初始值0
     * pages
     */
    @ApiModelProperty("总页数， 初始值0")
    private int totalPages;

    /**
     * 总记录数，初始值0
     * total
     */
    @ApiModelProperty("总记录数，初始值0")
    private long totalElements = 0;

    /**
     * 数据List，用于存放分页数据
     * records
     */
    @ApiModelProperty("数据List，用于存放分页数据")
    private List<E> list;

}
