package cn.com.cowain.ibms.enumeration;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/21 14:16
 */
public enum  HkAddress {


    // 南通
    NT,

    // 昆山
    KS
}
