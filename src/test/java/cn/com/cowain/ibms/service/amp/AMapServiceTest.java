package cn.com.cowain.ibms.service.amp;

import cn.com.cowain.ibms.service.amap.AMapService;
import cn.com.cowain.ibms.service.bean.amap.enumeration.CoordinateSystem;
import cn.com.cowain.ibms.service.bean.amap.geo.ReGeoCode;
import cn.com.cowain.ibms.service.bean.amap.poi.LonLat;
import cn.com.cowain.ibms.service.bean.amap.weather.Lives;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Yang.Lee
 * @date 2021/4/19 16:00
 */
@Slf4j
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AMapServiceTest {

    @Resource
    private AMapService aMapService;

    @Test
    public void testLiveWeather(){

        String cityCode = "110101";
        Lives weather = aMapService.getLiveWeather(cityCode);

    }


    @Test
    public void testCoorConvert(){

        List<LonLat> lonLatList1 = aMapService.coordinateConvert(CoordinateSystem.GPS, new LonLat(121.111222, 27.009821), new LonLat(118.111222, 32.009821));

        log.info(lonLatList1.size()+"");
    }

    @Test
    public void testReGeo(){

        Map<String, Object> params = new HashMap<>();
        params.put("extensions", "base");
        params.put("batch", true);
        List<ReGeoCode> reGeoCode = aMapService.reGeo(params, new LonLat(121.07144, 31.29209), new LonLat(122.0987, 30.998));

        log.info("city code : {}", reGeoCode.get(0).getAddressComponent().getAdCode());
    }
}
