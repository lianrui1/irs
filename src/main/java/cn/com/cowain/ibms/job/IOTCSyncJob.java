package cn.com.cowain.ibms.job;

import cn.com.cowain.ibms.entity.space.IotProduct;
import cn.com.cowain.ibms.feign.bean.iotc.resp.ApplicationResp;
import cn.com.cowain.ibms.feign.bean.iotc.resp.DeviceResp;
import cn.com.cowain.ibms.service.iotc.ApplicationService;
import cn.com.cowain.ibms.service.space.IotDeviceService;
import cn.com.cowain.ibms.service.space.IotProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * IOTC数据同步任务
 *
 * @author Yang.Lee
 * @date 2021/10/20 9:24
 **/
@Slf4j
@Component
public class IOTCSyncJob {

    @Resource
    private ApplicationService applicationService;

    @Resource
    private IotProductService productService;

    @Resource
    private IotDeviceService iotDeviceService;

    @Scheduled(cron = "0 0/5 * * * ?")
    public void run() {
        log.info("开始同步IOTC数据");

        try {
            applicationSync();
        } catch (Exception e) {
            log.error("同步应用数据异常");
            log.error(e.getMessage(), e);
        }

        try {
            productSync();
        } catch (Exception e) {
            log.error("同步产品数据异常");
            log.error(e.getMessage(), e);
        }

        try {
            deviceSync();
        } catch (Exception e) {
            log.error("同步设备数据异常");
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 同步应用数据
     *
     * @author Yang.Lee
     * @date 2021/10/20 9:27
     **/
    public void applicationSync() {
        log.info("开始同步应用列表");
        List<ApplicationResp> applicationRespList = applicationService.getApplicationListFromIOTC();
        applicationService.save(applicationRespList);
        log.info("应用列表同步完成");
    }

    /**
     * 同步产品数据
     *
     * @author Yang.Lee
     * @date 2021/10/25 11:24
     **/
    public void productSync() {
        log.info("开始同步产品列表");
        List<IotProduct> productList = productService.getListFromIOTC();
        productService.saveProduct(productList);
        log.info("产品列表同步完成");
    }

    /**
     * 同步设备数据
     *
     * @author Yang.Lee
     * @date 2021/10/27 16:07
     **/
    public void deviceSync() {

        log.info("开始同步设备列表");
        List<DeviceResp> deviceRespList = iotDeviceService.getDeviceListFromIOTC();
        iotDeviceService.saveDeviceList(deviceRespList);
        log.info("设备列表同步完成");
    }
}
