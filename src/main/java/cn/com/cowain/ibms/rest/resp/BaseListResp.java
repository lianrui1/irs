package cn.com.cowain.ibms.rest.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2021/3/11 13:31
 */
@Data
@ApiModel("基础列表数据输出对象")
public class BaseListResp {

    public BaseListResp(){}

    public BaseListResp(String id, String name){
        this.id = id;
        this.name = name;
    }

    @ApiModelProperty(value = "id", example = "Id", required = true)
    private String id;

    @ApiModelProperty(value = "名称", example = "Name", required = true)
    private String name;
}
