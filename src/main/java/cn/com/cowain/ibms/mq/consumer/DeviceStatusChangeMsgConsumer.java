package cn.com.cowain.ibms.mq.consumer;

import cn.com.cowain.ibms.mq.bean.DeviceStatusChangeMsgPushMessage;
import cn.com.cowain.ibms.service.space.IotDeviceService;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/1/19 15:51
 */
@Slf4j
@RocketMQMessageListener(
        topic = "${rocketmq.env}"+ "-device-status-change",
        consumerGroup = "device-status-change"
)
@Component
public class DeviceStatusChangeMsgConsumer implements RocketMQListener<String> {

    @Autowired
    private IotDeviceService iotDeviceService;

    @Override
    public void onMessage(String message) {

        // 发送设备状态变化推送消息
        iotDeviceService.deviceStatusChangeMsgPush(JSON.parseObject(message, DeviceStatusChangeMsgPushMessage.class));
    }
}
