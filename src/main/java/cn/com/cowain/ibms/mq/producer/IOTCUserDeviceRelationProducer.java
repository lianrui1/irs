package cn.com.cowain.ibms.mq.producer;

import cn.com.cowain.ibms.mq.Topic;
import cn.com.cowain.ibms.mq.bean.AccessPermissions;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.utils.DateUtils;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Objects;

import static cn.com.cowain.ibms.utils.DateUtils.PATTERN_DATETIME_WITH_ZONE;

/**
 * 权限-访客，入职权限，请求iotc
 *
 * @author Yang.Lee
 * @date 2021/6/17 13:42
 */
@Slf4j
@Component
public class IOTCUserDeviceRelationProducer {

    @Value("${rocketmq.env}")
    private String env;
    @Value("${max-permission-end-time:2029-12-31T23:59:59+08:00}")
    private String maxPermissionEndTime;

    @Resource
    private RocketMQTemplate rocketMQTemplate;

    private LocalDateTime MAX_PERMISSION_TIME;

    /**
     * 发送消息
     *
     * @param accessPermissions 消息内容
     * @return 发送结果
     * @author Yang.Lee
     * @date 2022/1/27 16:20
     **/
    public ServiceResult push(AccessPermissions accessPermissions) {

        fixUserEndTime(accessPermissions);

        String messageStr = JSON.toJSONString(accessPermissions);
        String topic = env + "-" + Topic.BIZ_TO_IOTC;

        log.debug("准备发送消息到mq, topic {}, message {}", topic, messageStr);

        Message<String> message = new GenericMessage<>(messageStr);
        SendResult sendResult = rocketMQTemplate.syncSend(topic, message);

        if (!sendResult.getSendStatus().equals(SendStatus.SEND_OK)) {
            log.error("消息发送失败。{}", sendResult);
            return ServiceResult.error(sendResult.toString());
        }
        return ServiceResult.ok();
    }

    /**
     * 由于海康用户权限存在最大时间的限制，需将时间超过最大时间的结束时间改为最大时间
     *
     * @param accessPermissions
     */
    private void fixUserEndTime(AccessPermissions accessPermissions) {
        if (CollectionUtils.isNotEmpty(accessPermissions.getUserList())) {
            for (AccessPermissions.User user : accessPermissions.getUserList()) {
                if (StringUtils.isNotBlank(user.getEndTime())) {
                    LocalDateTime endTime = DateUtils.parseLocalDateTime(user.getEndTime(), PATTERN_DATETIME_WITH_ZONE);
                    if (Objects.isNull(MAX_PERMISSION_TIME)) {
                        MAX_PERMISSION_TIME = DateUtils.parseLocalDateTime(maxPermissionEndTime, PATTERN_DATETIME_WITH_ZONE);
                    }
                    if (endTime.isAfter(MAX_PERMISSION_TIME)) {
                        user.setEndTime(maxPermissionEndTime);
                    }
                }
            }
        }
    }
}
