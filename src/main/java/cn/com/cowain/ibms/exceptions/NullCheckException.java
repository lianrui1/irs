package cn.com.cowain.ibms.exceptions;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/11/20
 */
public class NullCheckException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public NullCheckException(String message) {
        super(message);
    }
}
