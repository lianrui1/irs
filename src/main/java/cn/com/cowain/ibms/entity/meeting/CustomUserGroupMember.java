package cn.com.cowain.ibms.entity.meeting;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 自定义用户组成员对象
 *
 * @author Yang.Lee
 * @date 2021/6/8 18:01
 */
@Data
@Entity
@Table(name = "ibms_custom_user_group_member")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "ibms_custom_user_group_member", comment = "用户自定义用户组成员表")
public class CustomUserGroupMember extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "custom_user_group_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 用户组id'")
    private CustomUserGroup customUserGroup;

    @Column(name = "member_emp_no", nullable = false, columnDefinition = "varchar(36) COMMENT '成员工号'")
    private String memberEmpNo;
}
