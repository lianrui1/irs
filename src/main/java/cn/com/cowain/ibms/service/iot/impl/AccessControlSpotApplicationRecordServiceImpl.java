package cn.com.cowain.ibms.service.iot.impl;

import cn.com.cowain.ibms.dao.ability.OpenAbilityDao;
import cn.com.cowain.ibms.dao.iot.AccessControlSpotApplicationRecordDao;
import cn.com.cowain.ibms.dao.iot.AccessControlSpotDao;
import cn.com.cowain.ibms.dao.iot.AccessControlSpotKSHTMemberGroupDao;
import cn.com.cowain.ibms.dao.iot.AccessControlSpotUserDao;
import cn.com.cowain.ibms.dao.task.TaskCenterTaskDao;
import cn.com.cowain.ibms.entity.MessageBody;
import cn.com.cowain.ibms.entity.MessagePoolRecord;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.ability.OpenAbility;
import cn.com.cowain.ibms.entity.iot.*;
import cn.com.cowain.ibms.entity.task.TaskCenterTask;
import cn.com.cowain.ibms.enumeration.ApprovalStatus;
import cn.com.cowain.ibms.enumeration.ability.Ability;
import cn.com.cowain.ibms.enumeration.iot.AccessControlSpotType;
import cn.com.cowain.ibms.enumeration.iot.AccessPermissionDistributionStatus;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.enumeration.iot.KSHTMemberGroupType;
import cn.com.cowain.ibms.enumeration.task.TaskOperateResult;
import cn.com.cowain.ibms.enumeration.task.TaskStatus;
import cn.com.cowain.ibms.enumeration.task.TaskType;
import cn.com.cowain.ibms.exceptions.BizLogicNotMatchException;
import cn.com.cowain.ibms.exceptions.RemoteServiceUnavailableException;
import cn.com.cowain.ibms.feign.iotc.CheckFaceImageApi;
import cn.com.cowain.ibms.feign.iotc.bean.CreateHTMemberGroupReq;
import cn.com.cowain.ibms.feign.task_center.bean.UpdateTaskReq;
import cn.com.cowain.ibms.mq.Topic;
import cn.com.cowain.ibms.mq.bean.AccessPermissions;
import cn.com.cowain.ibms.mq.bean.BizReturnMessage;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.iot.*;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.StaffResp;
import cn.com.cowain.ibms.rest.resp.iot.AccessControlSpotApplicationRecordResp;
import cn.com.cowain.ibms.service.MessageSendService;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.ability.IotcConnectService;
import cn.com.cowain.ibms.service.bean.ServiceMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.impl.UploadServiceImpl;
import cn.com.cowain.ibms.service.iot.AccessControlSpotApplicationRecordService;
import cn.com.cowain.ibms.service.iot.AccessControlSpotService;
import cn.com.cowain.ibms.service.iotc.IOTCService;
import cn.com.cowain.ibms.service.space.IotDeviceService;
import cn.com.cowain.ibms.service.task.TaskCenterTaskService;
import cn.com.cowain.ibms.utils.DateUtils;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.RandomCode;
import cn.com.cowain.ibms.utils.RedisUtil;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.sfp.comm.utils.JwtUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author: yanzy
 * @date: 2022/3/9 10:31
 */
@Service
@Slf4j
public class AccessControlSpotApplicationRecordServiceImpl implements AccessControlSpotApplicationRecordService {
    @Resource
    private AccessControlSpotApplicationRecordDao accessControlSpotApplicationRecordDao;
    @Resource
    private AccessControlSpotDao accessControlSpotDao;
    @Resource
    private TaskCenterTaskDao taskCenterTaskRecordDao;
    @Resource
    private AccessControlSpotService accessControlSpotService;
    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserService;
    @Resource
    private TaskCenterTaskService taskCenterTaskService;
    @Resource
    private RedisUtil redisUtil;
    @Resource
    private AccessControlSpotUserDao accessControlSpotUserDao;
    @Resource
    private IOTCService iotcService;
    @Resource
    private CheckFaceImageApi checkFaceImageApi;
    @Resource
    private AccessControlSpotKSHTMemberGroupDao accessControlSpotKSHTMemberGroupDao;
    @Resource
    private OpenAbilityDao openAbilityDao;
    @Resource
    private IotcConnectService iotcConnectService;
    @Value("${iotc.timeplan:e62817832b764a088f6d52033f9c9db1}")
    private String timePlan;
    @Resource
    private MessageSendService messageSendService;
    @Value("${ms-task-center.access-control-spot-application.approver-h5-url}")
    private String accessControlSpotApplicationUrl;
    @Value("${ms-task-center.access-control-spot-application.applicant-h5-url}")
    private String accessControlSpotApplicationClaimerUrl;
    @Value("${cowain.send.message.vx.msgTemplateId}")
    private String vxMsgTemplateId;
    @Value("#{'${auth-application-handling.fail-alert}'.split(',')}")
    private List<String> failAlertHrIds;


    @Transactional
    @Override
    public ServiceResult createAccessControlSpotApplicationRecord(String token, AccessControlSpotApplicationRecordReq req) {
        log.info("start to create accessControlSpotApplicationRecord, token:{}, req:{}", token, JSONObject.toJSONString(req));

        SysUser applicant = sysUserService.getUserByToken(token);
        if (Objects.isNull(applicant)) {
            return ServiceResult.error("无权限", ErrConst.E01);
        }
        String hrId = applicant.getEmpNo();
        //判断用户是否是访客类型
        boolean isVistor = SysUserService.isVisitor(hrId);
        if (isVistor) {
            // 访客无权限申请
            return ServiceResult.error("访客无权限", ErrConst.E01);
        }

        //获取门禁点
        Optional<AccessControlSpot> accessControlSpotOptional = accessControlSpotDao.findById(req.getSpotId());
        if (accessControlSpotOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_ACCESS_CONTROL_SPOT, req.getSpotId()), ErrConst.E01);
        }
        AccessControlSpot accessControlSpot = accessControlSpotOptional.get();
        if (accessControlSpotService.isAccess(req.getSpotId(), hrId)) {
            return ServiceResult.error("用户已有门禁点的权限，无需申请", ErrConst.E01);
        }
        if (Objects.isNull(accessControlSpot.getDevice())) {
            return ServiceResult.error("门禁点未有关联设备", ErrConst.E01);
        }
        //获取用户对门禁点最近一次申请
        AccessControlSpotApplicationRecord latestApplicationRecord = accessControlSpotApplicationRecordDao.
                findFirstByApplicantHrIdAndSpotIdOrderByCreatedTimeDesc(hrId, req.getSpotId());
        if (Objects.nonNull(latestApplicationRecord) && Objects.equals(ApprovalStatus.PENDING, latestApplicationRecord.getStatus())) {
            return ServiceResult.error("您已申请，请等待分配人员分配");
        }
        List<SysUser> approverList = accessControlSpotService.getAccessControlSpotApproverList(req.getSpotId(), hrId);
        if (CollectionUtils.isEmpty(approverList)) {
            return ServiceResult.error("未分配审批人");
        }
        if (Objects.equals(AccessControlSpotType.FACE_MACHINE, accessControlSpotOptional.get().getAccessControlSpotType())) {
            if (StringUtils.isEmpty(req.getFaceImg())) {
                return ServiceResult.error("当门禁点类型为面板机时，人脸图片必传", ErrConst.E01);
            }
            // 校验人脸照片是否有效
            String redisKey = redisUtil.createRealKey(UploadServiceImpl.VALID_FACE_URL_PREFIX + req.getFaceImg());
            if (!redisUtil.hasKey(redisKey)) {
                try {
                    JsonResult<Object> resp = checkFaceImageApi.checkFaceImage(req.getFaceImg());
                    if (resp.getStatus() == 0) {
                        log.error("IOTC 校验人脸照片失败，imageUrl: {}，结果：{}", req.getFaceImg(), resp);
                        return ServiceResult.error("校验人脸照片失败：" + resp.getMsg(), resp.getCode());
                    }
                } catch (Exception e) {
                    throw new RemoteServiceUnavailableException("IOTC 校验人脸照片失败");
                }
            }
        } else {
            req.setFaceImg(null);
        }
        //创建申请
        AccessControlSpotApplicationRecord applicationRecord = AccessControlSpotApplicationRecord.buildAccessControlSpotApplicationRecord(req.getFaceImg(), applicant, accessControlSpot);
        accessControlSpotApplicationRecordDao.save(applicationRecord);
        boolean isCreateTaskAllFailed = true;
        for (SysUser approver : approverList) {
            TaskCenterTask taskCenterTask = taskCenterTaskService.createAccessControlSpotApplicationTask(approver, applicationRecord);
            if (Objects.nonNull(taskCenterTask)) {
                isCreateTaskAllFailed = false;
            }
        }
        if (isCreateTaskAllFailed) {
            // 业务处理失败，手动回滚事务,返回异常
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return ServiceResult.error("对接待办中心失败", ErrConst.E13);
        }
        for (SysUser approver : approverList) {
            sendMessageToApprover(applicant, applicationRecord, approver);
        }
        return ServiceResult.ok(new IdResp(applicationRecord.getId()));
    }

    @Override
    public void sendMessageToApprover(SysUser applicant, AccessControlSpotApplicationRecord applicationRecord, SysUser approver) {
        if (StringUtils.isEmpty(approver.getWxOpenId())) {
            log.error("分配人不存在openID，hrId：{}", approver.getEmpNo());
            return;
        }
        MessagePoolRecord messagePoolRecord = new MessagePoolRecord();
        messagePoolRecord.setMsgFromSys(8);
        messagePoolRecord.setMsgType(3);
        messagePoolRecord.setPriority(0);
        messagePoolRecord.setSendType(0);
        messagePoolRecord.setReceiveType(501);
        messagePoolRecord.setMsgTitle("通行权限分配提醒");
        MessageBody messageBody = new MessageBody();
        messageBody.setTitle(String.format("您好，收到%s的通行权限申请，需要您快速处理，请知悉！", applicant.getNameZh()));
        messageBody.setType("通行权限分配");
        messageBody.setDepart("待分配");
        messageBody.setTime(DateUtils.formatLocalDateTime(LocalDateTime.now(), DateUtils.PATTERN_DATETIME));
        messageBody.setRemark(getRemark(applicationRecord, applicant));
        messagePoolRecord.setMsgContent(JSON.toJSONString(messageBody));
        messagePoolRecord.setReceivePerson(approver.getWxOpenId());
        messagePoolRecord.setReceivePersonId(approver.getWxOpenId());
        messagePoolRecord.setMsgDetailsUrl(accessControlSpotApplicationUrl + applicationRecord.getId());
        messagePoolRecord.setMsgTemplateId(vxMsgTemplateId);
        messagePoolRecord.setTipType(1);
        try {
            messageSendService.send(messagePoolRecord);
        } catch (Exception e) {
            log.error("发送消息中心失败", e);
        }
    }

    /**
     * 查询用户对于门禁点的权限申请记录
     *
     * @param userHrId 用户工号
     * @param stopIds  门禁点列表
     * @return 申请记录列表
     * @author Yang.Lee
     * @date 2022/7/18 14:18
     **/
    @Override
    public List<AccessControlSpotApplicationRecordResp> getUserApplyList(String userHrId, String... stopIds) {

        return accessControlSpotApplicationRecordDao.findAllByApplicantHrIdAndSpotIdIn(userHrId, Arrays.asList(stopIds))
                .stream()
                .map(AccessControlSpotApplicationRecordResp::convert)
                .collect(Collectors.toList());
    }

    @Override
    public PageBean<AccessControlSpotApplicationRecordResp> getPage(AccessControlSpotApplicationRecordParamReq req) {

        //封装and语句
        List<Predicate> listAnd = new ArrayList<>();
        //封装or语句
        List<Predicate> listOr = new ArrayList<>();

        Page<AccessControlSpotApplicationRecord> accessControlSpotApplicationRecords = accessControlSpotApplicationRecordDao.findAll((root, query, criteriaBuilder) -> {

            //审批人工号
            if (StringUtils.isNotBlank(req.getApprover())) {
                String like = "%" + req.getApprover() + "%";
                listOr.add(criteriaBuilder.like(root.get("approverHrId"), like));
            }

            //审批人姓名
            if (StringUtils.isNotBlank(req.getApprover())) {
                String like = "%" + req.getApprover() + "%";
                listOr.add(criteriaBuilder.like(root.get("approverName"), like));
            }

            //审批状态
            if (ObjectUtils.isNotEmpty(req.getStatus())) {
                listAnd.add(criteriaBuilder.equal(root.get("status"), req.getStatus()));
            }

            //申请人工号
            if (StringUtils.isNotBlank(req.getApplicant())) {
                String like = "%" + req.getApplicant() + "%";
                listOr.add(criteriaBuilder.like(root.get("applicantHrId"), like));
            }

            //申请人姓名
            if (StringUtils.isNotBlank(req.getApplicant())) {
                String like = "%" + req.getApplicant() + "%";
                listOr.add(criteriaBuilder.like(root.get("applicantName"), like));
            }

            //申请时间
            if (ObjectUtils.isNotEmpty(req.getApplicationStartTime()) && ObjectUtils.isNotEmpty(req.getApplicationEndTime())) {
                listAnd.add(criteriaBuilder.greaterThanOrEqualTo(root.get("createdTime"), req.getApplicationStartTime()));
                listAnd.add(criteriaBuilder.lessThanOrEqualTo(root.get("createdTime"), req.getApplicationEndTime()));
            }
            Predicate[] array_and = new Predicate[listAnd.size()];
            Predicate Pre_And = criteriaBuilder.and(listAnd.toArray(array_and));
            Predicate[] array_or = new Predicate[listAnd.size()];
            Predicate Pre_Or = criteriaBuilder.or(listOr.toArray(array_or));
            if (!org.springframework.util.CollectionUtils.isEmpty(listOr)) {
                return query.where(Pre_And, Pre_Or).getRestriction();
            } else {
                return query.where(Pre_And).getRestriction();
            }

        }, PageRequest.of(req.getPage(), req.getSize(), Sort.by(req.getOrders())));

        PageBean<AccessControlSpotApplicationRecordResp> pageBean = new PageBean<>();
        pageBean.setPage(accessControlSpotApplicationRecords.getPageable().getPageNumber());
        pageBean.setSize(accessControlSpotApplicationRecords.getPageable().getPageSize());
        pageBean.setTotalPages(accessControlSpotApplicationRecords.getTotalPages());
        pageBean.setTotalElements(accessControlSpotApplicationRecords.getTotalElements());
        pageBean.setList(accessControlSpotApplicationRecords.getContent()
                .stream()
                .map(AccessControlSpotApplicationRecordResp::convert)
                .collect(Collectors.toList()));
        return pageBean;
    }

    @Override
    public ServiceResult getAccessControlSpotApplicationRecord(String id) {
        log.info("start to get accessControlSpotApplicationRecord, id:{}", id);
        Optional<AccessControlSpotApplicationRecord> accessControlSpotApplicationRecordOptional = accessControlSpotApplicationRecordDao.findById(id);
        if (accessControlSpotApplicationRecordOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_ACCESS_CONTROL_SPOT_APPLICATION_RECORD, id));
        }

        //获取门禁授权申请
        AccessControlSpotApplicationRecord accessControlSpotApplicationRecord = accessControlSpotApplicationRecordOptional.get();
        AccessControlSpotApplicationRecordResp resp = AccessControlSpotApplicationRecordResp.convert(accessControlSpotApplicationRecord);

        //设置分配人列表
        resp.setApproverList(getApproverList(accessControlSpotApplicationRecord));
        return ServiceResult.ok(resp);
    }

    /**
     * 获取门禁点授权申请的分配人列表
     *
     * @param record
     * @return
     */
    private List<StaffResp> getApproverList(AccessControlSpotApplicationRecord record) {
        List<String> hrIdList = new ArrayList<>();
        if (StringUtils.isNotEmpty(record.getApproverHrId())) {
            hrIdList.add(record.getApproverHrId());
        } else {
            List<TaskCenterTask> taskRecordList = taskCenterTaskRecordDao.findAllByTypeAndStatusAndTaskId(TaskType.ACCESS_CONTROL_SPOT_APPLICATION,
                    TaskStatus.PENDING, record.getId());
            if (CollectionUtils.isNotEmpty(taskRecordList)) {
                taskRecordList.forEach(taskRecord -> {
                    if (!hrIdList.contains(taskRecord.getUcHrId())) {
                        hrIdList.add(taskRecord.getUcHrId());
                    }
                });
            }
        }
        if (CollectionUtils.isNotEmpty(hrIdList)) {
            return hrIdList.stream().map(hrId -> sysUserService.getUserByEmpNo(hrId)).filter(Objects::nonNull).map(StaffResp::convert)
                    .collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    @Transactional
    @Override
    public ServiceResult handlingAccessControlSpotApplication(String token, String id, HandlingAccessControlSpotApplicationRecordReq req) {
        log.info("start to handle accessControlSpotApplication, token:{}, id:{}, req:{}", token, id, JSONObject.toJSONString(req));
        String hrId = JwtUtil.getHrId(token);
        SysUser sysUser = sysUserService.getUserByEmpNo(hrId);
        //获取申请
        Optional<AccessControlSpotApplicationRecord> accessControlSpotApplicationRecordOptional = accessControlSpotApplicationRecordDao.findById(id);
        if (accessControlSpotApplicationRecordOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_ACCESS_CONTROL_SPOT_APPLICATION_RECORD, id));
        }
        AccessControlSpotApplicationRecord application = accessControlSpotApplicationRecordOptional.get();
        if (!Objects.equals(ApprovalStatus.PENDING, application.getStatus())) {
            return ServiceResult.error("申请已审批", ErrConst.E01);
        }
        //获取门禁点
        Optional<AccessControlSpot> accessControlSpotOptional = accessControlSpotDao.findById(application.getSpotId());
        if (accessControlSpotOptional.isEmpty()) {
            return ServiceResult.error("门禁点不存在", ErrConst.E01);
        }
        if (Objects.isNull(accessControlSpotOptional.get().getDevice())) {
            return ServiceResult.error("门禁点还没有绑定设备", ErrConst.E01);
        }
        //获取可以为用户分配门禁点权限的用户列表
        List<SysUser> sysUserList = accessControlSpotService.getAccessControlSpotApproverList(application.getSpotId(), application.getApplicantHrId());
        List<String> approverIdList = sysUserList.stream().map(SysUser::getEmpNo).collect(Collectors.toList());
        if (!approverIdList.contains(hrId)) {
            return ServiceResult.error(ServiceMessage.PERMISSION_DENIED, ErrConst.E401);
        }
        application.setApproverHrId(sysUser.getEmpNo());
        application.setApproverName(sysUser.getNameZh());
        application.setApproverDepartmentFullName(sysUser.getFullDepartmentName());
        application.setApproverTime(LocalDateTime.now());
        if (Objects.equals(ApprovalStatus.APPROVED.name(), req.getStatus())) {
            if (Objects.isNull(req.getAccessStartTime()) || Objects.isNull(req.getAccessEndTime())) {
                return ServiceResult.error("同意申请时，权限开始或者结束时间不能为空", ErrConst.E01);
            }
            if (req.getAccessStartTime().isAfter(req.getAccessEndTime())) {
                return ServiceResult.error("权限开始时间不能大于权限结束时间", ErrConst.E01);
            }
            application.setAccessStartTime(req.getAccessStartTime().atStartOfDay());
            application.setAccessEndTime(req.getAccessEndTime().atTime(23, 59, 59));
            application.setStatus(ApprovalStatus.APPROVED);
        } else if (Objects.equals(ApprovalStatus.DENIED.name(), req.getStatus())) {
            application.setStatus(ApprovalStatus.DENIED);
            application.setDeniedReason(req.getDeniedReason());
        } else {
            return ServiceResult.error("status is invalid", ErrConst.E01);
        }
        accessControlSpotApplicationRecordDao.save(application);
        //更新待办中心状态
        List<TaskCenterTask> taskCenterTaskList = taskCenterTaskRecordDao.findAllByTypeAndTaskId(TaskType.ACCESS_CONTROL_SPOT_APPLICATION, application.getId());
        boolean isExistUpdateTaskFailed = false;
        if (Objects.equals(ApprovalStatus.APPROVED.name(), req.getStatus())) {
            for (TaskCenterTask taskCenterTask : taskCenterTaskList) {
                UpdateTaskReq updateTaskReq = new UpdateTaskReq();
                updateTaskReq.setId(taskCenterTask.getTaskCenterId());
                updateTaskReq.setOperateResult(TaskOperateResult.PASSED.getValue());
                updateTaskReq.setStatus(TaskStatus.DONE.getValue());
                ServiceResult updateTaskResult = taskCenterTaskService.updateTask(taskCenterTask, updateTaskReq);
                if (!updateTaskResult.isSuccess()) {
                    isExistUpdateTaskFailed = true;
                }
            }
        } else {
            for (TaskCenterTask taskCenterTask : taskCenterTaskList) {
                UpdateTaskReq updateTaskReq = new UpdateTaskReq();
                updateTaskReq.setId(taskCenterTask.getTaskCenterId());
                updateTaskReq.setOperateResult(TaskOperateResult.DECLINED.getValue());
                updateTaskReq.setStatus(TaskStatus.DONE.getValue());
                ServiceResult updateTaskResult = taskCenterTaskService.updateTask(taskCenterTask, updateTaskReq);
                if (!updateTaskResult.isSuccess()) {
                    isExistUpdateTaskFailed = true;
                }
            }
        }
        if (isExistUpdateTaskFailed) {
            // 业务处理失败，手动回滚事务,返回异常
            rollbackTaskCenterTask(taskCenterTaskList);
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return ServiceResult.error("对接待办中心失败", ErrConst.E13);
        }
        if (Objects.equals(ApprovalStatus.APPROVED.name(), req.getStatus())) {
            ServiceResult serviceResult = addUserToAccessControlSpot(accessControlSpotOptional.get(), accessControlSpotApplicationRecordOptional.get(),
                    application.getAccessStartTime(), application.getAccessEndTime());
            if (!serviceResult.isSuccess()) {
                // 门禁点添加用户权限失败
                rollbackTaskCenterTask(taskCenterTaskList);
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error("门禁点添加用户权限失败", ErrConst.E01);
            }
            //由于系统暂未保存员工的人脸数据，暂临时保存在redis
            String faceKey = redisUtil.createRealKey("face-image-data:" + application.getApplicantHrId());
            redisUtil.set(faceKey, application.getFaceImg());
        }
        sendMessageToApplicant(application);
        return ServiceResult.ok("已审批");
    }

    private void sendMessageToApplicant(AccessControlSpotApplicationRecord applicationRecord) {
        SysUser applicant = sysUserService.getUserByEmpNo(applicationRecord.getApplicantHrId());
        if (Objects.isNull(applicant) || StringUtils.isEmpty(applicant.getWxOpenId())) {
            log.error("申请人微信openId不存在，hrId:{}", applicationRecord.getApplicantHrId());
            return;
        }
        MessagePoolRecord messagePoolRecord = new MessagePoolRecord();
        messagePoolRecord.setMsgFromSys(8);
        messagePoolRecord.setMsgType(3);
        messagePoolRecord.setPriority(0);
        messagePoolRecord.setSendType(0);
        messagePoolRecord.setReceiveType(501);
        messagePoolRecord.setMsgTitle("通行权限分配提醒");
        MessageBody messageBody = new MessageBody();
        messageBody.setTitle("您好，您申请的门禁通行权限已处理，请知悉！");
        messageBody.setType("通行权限分配");
        messageBody.setDepart(ApprovalStatus.APPROVED.equals(applicationRecord.getStatus()) ? "已分配" : "已拒绝");
        messageBody.setTime(DateUtils.formatLocalDateTime(LocalDateTime.now(), DateUtils.PATTERN_DATETIME));
        messageBody.setRemark(ApprovalStatus.APPROVED.equals(applicationRecord.getStatus()) ? getRemark(applicationRecord, applicant) : applicationRecord.getDeniedReason());
        messagePoolRecord.setMsgContent(JSON.toJSONString(messageBody));
        messagePoolRecord.setReceivePerson(applicant.getWxOpenId());
        messagePoolRecord.setReceivePersonId(applicant.getWxOpenId());
        messagePoolRecord.setMsgDetailsUrl(accessControlSpotApplicationClaimerUrl + applicationRecord.getId());
        messagePoolRecord.setMsgTemplateId(vxMsgTemplateId);
        messagePoolRecord.setTipType(1);
        try {
            messageSendService.send(messagePoolRecord);
        } catch (Exception e) {
            log.error("发送消息中心失败", e);
        }
    }

    /**
     * 获取申请消息备注
     *
     * @param applicationRecord
     * @param applicant
     * @return
     */
    private String getRemark(AccessControlSpotApplicationRecord applicationRecord, SysUser applicant) {
        return String.format("申请人：%s;%n人员类型：%s%n通行门禁：%s;%n门禁位置：%s", getApplicantName(applicant),
                SysUserService.isVisitor(applicant.getEmpNo()) ? "劳务" : "员工", applicationRecord.getSpotName().trim(),
                applicationRecord.getSpotAddress());
    }

    /**
     * 获取申请人的名称
     *
     * @param applicant
     * @return
     */
    private String getApplicantName(SysUser applicant) {
        if (StringUtils.isBlank(applicant.getDeptName())) {
            return applicant.getNameZh() + "/" + applicant.getEmpNo();
        } else {
            return applicant.getNameZh() + "/" + applicant.getEmpNo() + "/" + applicant.getDeptName();
        }
    }

    /**
     * 待办中心任务状态回滚
     *
     * @param taskCenterTaskList
     */
    private void rollbackTaskCenterTask(List<TaskCenterTask> taskCenterTaskList) {
        if (CollectionUtils.isEmpty(taskCenterTaskList)) {
            return;
        }
        for (TaskCenterTask taskCenterTask : taskCenterTaskList) {
            UpdateTaskReq updateTaskReq = new UpdateTaskReq();
            updateTaskReq.setId(taskCenterTask.getTaskCenterId());
            updateTaskReq.setOperateResult(TaskOperateResult.PENDING.getValue());
            updateTaskReq.setStatus(TaskStatus.PENDING.getValue());
            taskCenterTaskService.updateTask(taskCenterTask, updateTaskReq);
        }
    }

    /**
     * 向门禁点设备添加用户权限
     *
     * @param accessControlSpot
     * @param accessControlSpotApplicationRecord
     * @param accessStartTime
     * @param accessEndTime
     * @author wei.cheng
     * @date 2022/3/14 1:40 下午
     **/
    private ServiceResult addUserToAccessControlSpot(AccessControlSpot accessControlSpot,
                                                     AccessControlSpotApplicationRecord accessControlSpotApplicationRecord,
                                                     LocalDateTime accessStartTime, LocalDateTime accessEndTime) {
        ServiceResult serviceResult;
        AccessControlSpotUserReq accessControlSpotUserReq = new AccessControlSpotUserReq();
        accessControlSpotUserReq.setEmpNo(accessControlSpotApplicationRecord.getApplicantHrId());
        accessControlSpotUserReq.setName(accessControlSpotApplicationRecord.getApplicantName());
        accessControlSpotUserReq.setStartTime(accessControlSpotApplicationRecord.getAccessStartTime());
        accessControlSpotUserReq.setEndTime(accessControlSpotApplicationRecord.getAccessEndTime());
        accessControlSpotUserReq.setVip(false);
        accessControlSpotService.addUsers(Lists.newArrayList(accessControlSpotUserReq), Ability.OTHER, accessControlSpot);
        if (Objects.equals(AccessControlSpotType.DOOR_MAGNETIC, accessControlSpot.getAccessControlSpotType())) {
            accessControlSpotApplicationRecord.setAccessPermissionDistributionStatus(AccessPermissionDistributionStatus.SUCCESS);
            serviceResult = ServiceResult.ok();
        } else if (Objects.equals(AccessControlSpotType.FACE_MACHINE, accessControlSpot.getAccessControlSpotType())) {
            AccessPermissions message = new AccessPermissions();
            message.setReturnTopic(Topic.get(Topic.ACCESS_APPLICATION_HANDLING_UPDATE_USER_TO_IOTC_MESSAGE_BACK));
            message.setSourceName("IBMS");
            message.setResourceId(accessControlSpotApplicationRecord.getId());
            message.setMethodName(AccessPermissions.Method.UPDATE.getName());
            message.setModuleName(AccessPermissions.Model.PERSON.getName());
            //获取用户所有绑定门禁点设备号列表
            List<String> deviceSnList = getAllBindDeviceSnList(accessControlSpotApplicationRecord.getApplicantHrId());
            if (!deviceSnList.contains(accessControlSpot.getDevice().getSn())) {
                deviceSnList.add(accessControlSpot.getDevice().getSn());
            }
            message.setDeviceCodeList(deviceSnList);
            AccessPermissions.User user = new AccessPermissions.User();
            user.setJobNo(accessControlSpotApplicationRecord.getApplicantHrId());
            user.setName(accessControlSpotApplicationRecord.getApplicantName());
            user.setPhoto(accessControlSpotApplicationRecord.getFaceImg());
            user.setVisitedWorkNo("");
            user.setCreateTime(DateUtils.formatLocalDateTime(accessStartTime, DateUtils.PATTERN_DATETIME));
            user.setStartTime(DateUtils.formatZoneDateTime(accessStartTime.atZone(ZoneId.of(DateUtils.GMT8)), DateUtils.PATTERN_DATETIME_WITH_ZONE));
            user.setEndTime(DateUtils.formatZoneDateTime(accessEndTime.atZone(ZoneId.of(DateUtils.GMT8)), DateUtils.PATTERN_DATETIME_WITH_ZONE));
            user.setPersonType(SysUserService.isVisitor(accessControlSpotApplicationRecord.getApplicantHrId()) ? AccessPermissions.PersonType.VISITOR : AccessPermissions.PersonType.EMPLOYEE);
            List<String> groupIdList = null;
            if (DeviceType.FACE_RECOGNITION.equals(accessControlSpot.getDevice().getDeviceType())) {
                if (IotDeviceService.isKSHTDevice(accessControlSpot.getDevice())) {
                    ServiceResult getGroupIdListResult = getKsHtGroupIdListOfUser(accessControlSpot, accessControlSpotApplicationRecord.getApplicantHrId());
                    if (getGroupIdListResult.isSuccess()) {
                        groupIdList = (List<String>) getGroupIdListResult.getObject();
                    } else {
                        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                        return getGroupIdListResult;
                    }
                } else {
                    log.debug("旷视九霄人员组暂不处理");
                    groupIdList = Lists.newArrayList();
                }
            }
            user.setGroupIds(groupIdList);
            message.setUserList(Lists.newArrayList(user));
            message.setOver("1");
            serviceResult = iotcService.updateFace(message);
            accessControlSpotApplicationRecord.setAccessPermissionDistributionStatus(AccessPermissionDistributionStatus.DOING);
        } else {
            accessControlSpotApplicationRecord.setAccessPermissionDistributionStatus(AccessPermissionDistributionStatus.SUCCESS);
            serviceResult = ServiceResult.ok();
        }
        accessControlSpotApplicationRecordDao.save(accessControlSpotApplicationRecord);
        return serviceResult;
    }

    /**
     * 获取用户所有绑定门禁点设备号列表
     *
     * @param hrId
     * @return
     */
    private List<String> getAllBindDeviceSnList(String hrId) {
        List<AccessControlSpotUser> accessControlSpotUserList = accessControlSpotUserDao.findByUserHrId(hrId);
        // 获取所有门禁点数据，可能有重复数据
        List<AccessControlSpot> accessControlSpotAllList = accessControlSpotUserList.stream()
                .map(AccessControlSpotUser::getAccessControlSpot)
                .collect(Collectors.toList());
        // 门禁点去重
        List<AccessControlSpot> accessControlSpotList = accessControlSpotAllList.stream()
                .collect(Collectors.collectingAndThen(Collectors.toCollection(
                        () -> new TreeSet<>(Comparator.comparing(AccessControlSpot::getId))), ArrayList::new));
        // 获取所有的设备号集合
        return accessControlSpotList.stream()
                .map(AccessControlSpot::getDevice)
                .filter(device -> device != null)
                .map(IotDevice::getSn).distinct().collect(Collectors.toList());
    }

    /**
     * 获取用户需要绑定的旷世鸿图人员组
     *
     * @param accessControlSpot
     * @param hrId
     * @return
     */
    private ServiceResult getKsHtGroupIdListOfUser(AccessControlSpot accessControlSpot, String hrId) {
        List<AccessControlSpotKSHTMemberGroup> coverGroupList = accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotId(
                accessControlSpot.getId());
        boolean isVisitor = SysUserService.isVisitor(hrId);
        coverGroupList.removeIf(g -> {
            if (isVisitor) {
                return !KSHTMemberGroupType.VISITOR.equals(g.getGroupType());
            } else {
                return !KSHTMemberGroupType.STAFF.equals(g.getGroupType());
            }
        });
        if (CollectionUtils.isEmpty(coverGroupList)) {
            Optional<OpenAbility> openAbilityOptional = openAbilityDao.findByAbility(Ability.OTHER);
            OpenAbility openAbility;
            if (openAbilityOptional.isEmpty()) {
                openAbility = new OpenAbility();
                openAbility.setAbility(Ability.OTHER);
                openAbility.setDescribe("其它");
                openAbilityDao.save(openAbility);
            } else {
                openAbility = openAbilityOptional.get();
            }
            AccessControlSpotKSHTMemberGroup memberGroup = new AccessControlSpotKSHTMemberGroup();
            memberGroup.setGroupType(isVisitor ? KSHTMemberGroupType.VISITOR : KSHTMemberGroupType.STAFF);
            memberGroup.setGroupName(accessControlSpot.getName() + "_申请授权" + memberGroup.getGroupType().getName());
            memberGroup.setAccessControlSpot(accessControlSpot);
            memberGroup.setOpenAbility(openAbility);
            accessControlSpotKSHTMemberGroupDao.save(memberGroup);
            CreateHTMemberGroupReq.Param param = CreateHTMemberGroupReq.Param.builder()
                    .id(memberGroup.getId())
                    .type(memberGroup.getGroupType().getCode())
                    .name(memberGroup.getGroupName())
                    .build();

            // 向iotc发送创建员工组消息
            ServiceResult result = iotcService.createStaffHTMemberGroup(CreateHTMemberGroupReq.builder().groupList(Lists.newArrayList(param)).build());
            if (!result.isSuccess()) {
                log.warn("创建旷世人员组:{}失败", param.getName());
                memberGroup.setGroupName(memberGroup.getGroupName() + RandomCode.getRandom(3));
                accessControlSpotKSHTMemberGroupDao.save(memberGroup);
                param.setName(memberGroup.getGroupName());
                // 以防旷世已存在重名的人员组，修改人员组名后，再次向iotc发送创建员工组消息
                ServiceResult result2 = iotcService.createStaffHTMemberGroup(CreateHTMemberGroupReq.builder().groupList(Lists.newArrayList(param)).build());
                if (!result2.isSuccess()) {
                    log.warn("创建旷世人员组:{}失败", param.getName());
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return result2;
                }
            }
            // 像iotc发送创建员工组消息
            PermisssionReq permisssionReq = new PermisssionReq();
            String[] groupUuidList = new String[]{memberGroup.getId()};
            String[] deviceUuidList = new String[]{memberGroup.getAccessControlSpot().getDevice().getSn()};
            permisssionReq.setDeviceUuidList(deviceUuidList);
            permisssionReq.setGroupUuidList(groupUuidList);
            permisssionReq.setTimePlanId(timePlan);
            result = iotcConnectService.bindingDevice(permisssionReq, "2", "KS");
            //失败
            if (!result.isSuccess()) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return result;
            }
            coverGroupList = Lists.newArrayList(memberGroup);
        }
        List<String> groupIdList = coverGroupList.stream().map(AccessControlSpotKSHTMemberGroup::getId).collect(Collectors.toList());
        List<AccessControlSpotUser> accessControlSpotUserList = accessControlSpotUserDao.findByUserHrId(hrId);
        if (CollectionUtils.isNotEmpty(accessControlSpotUserList)) {
            List<String> accessControlSpotIdList = accessControlSpotUserList.stream().map(AccessControlSpotUser::getAccessControlSpot)
                    .map(AccessControlSpot::getId).distinct().collect(Collectors.toList());
            List<AccessControlSpotKSHTMemberGroup> allCoverGroupList = accessControlSpotKSHTMemberGroupDao.
                    findByAccessControlSpotIdIn(accessControlSpotIdList);
            allCoverGroupList.removeIf(g -> {
                if (isVisitor) {
                    return KSHTMemberGroupType.STAFF.equals(g.getGroupType()) || KSHTMemberGroupType.MEAL_STAFF.equals(g.getGroupType());
                } else {
                    return KSHTMemberGroupType.VISITOR.equals(g.getGroupType()) || KSHTMemberGroupType.MEAL_VISITOR.equals(g.getGroupType());
                }
            });
            if (CollectionUtils.isNotEmpty(allCoverGroupList)) {
                for (AccessControlSpotKSHTMemberGroup g : allCoverGroupList) {
                    if (!groupIdList.contains(g.getId())) {
                        groupIdList.add(g.getId());
                    }
                }
            }
        }
        return ServiceResult.ok(groupIdList);
    }

    @Override
    public ServiceResult saveReturnMessage(BizReturnMessage reMessage, String topic) {
        Optional<AccessControlSpotApplicationRecord> accessControlSpotApplicationRecordOptional =
                accessControlSpotApplicationRecordDao.findById(reMessage.getResourceId());
        if (accessControlSpotApplicationRecordOptional.isEmpty()) {
            log.error("未找到任务信息，返回结果：{}", JSON.toJSONString(reMessage));
            return ServiceResult.error("未找到任务信息，返回结果：" + JSON.toJSONString(reMessage), ErrConst.E01);
        }
        AccessControlSpotApplicationRecord accessControlSpotApplicationRecord = getAccessControlSpotApplicationRecordOfSaveReturnMessage(
                reMessage.getResourceId(), 0);
        if ("0".equals(reMessage.getFailCode()) || "200".equals(reMessage.getFailCode())) {
            if (Objects.equals(AccessPermissionDistributionStatus.SUCCESS, accessControlSpotApplicationRecord.getAccessPermissionDistributionStatus())) {
                log.warn("门禁点权限申请通行权限已下发，id:{}", accessControlSpotApplicationRecord.getId());
                return ServiceResult.error("门禁点权限申请通行权限已下发", ErrConst.E01);
            }
            Optional<AccessControlSpot> accessControlSpotOptional = accessControlSpotDao.findById(accessControlSpotApplicationRecord.getSpotId());
            if (accessControlSpotOptional.isEmpty()) {
                log.error("门禁点授权申请的门禁点不存在，applicationId:{}", accessControlSpotApplicationRecord.getId());
                return ServiceResult.error("门禁点授权申请的门禁点不存在", ErrConst.E01);
            }
            AccessControlSpot accessControlSpot = accessControlSpotOptional.get();
            if (IotDeviceService.isKSHTDevice(accessControlSpot.getDevice()) &&
                    Topic.ACCESS_APPLICATION_HANDLING_UPDATE_USER_TO_IOTC_MESSAGE_BACK.equals(topic)) {
                AccessPermissions message = new AccessPermissions();
                message.setReturnTopic(Topic.get(Topic.ACCESS_APPLICATION_HANDLING_BIND_USER_TO_IOTC_MESSAGE_BACK));
                message.setSourceName("IBMS");
                message.setResourceId(accessControlSpotApplicationRecord.getId());
                message.setMethodName(AccessPermissions.Method.INCREMENT_BIND.getName());
                message.setModuleName(AccessPermissions.Model.PERSON.getName());
                message.setDeviceCodeList(Lists.newArrayList(accessControlSpot.getDevice().getSn()));
                AccessPermissions.User user = new AccessPermissions.User();
                user.setJobNo(accessControlSpotApplicationRecord.getApplicantHrId());
                user.setName(accessControlSpotApplicationRecord.getApplicantName());
                user.setPhoto(accessControlSpotApplicationRecord.getFaceImg());
                user.setVisitedWorkNo("");
                user.setCreateTime(DateUtils.formatLocalDateTime(accessControlSpotApplicationRecord.getAccessStartTime(), DateUtils.PATTERN_DATETIME));
                user.setStartTime(DateUtils.formatZoneDateTime(accessControlSpotApplicationRecord.getAccessStartTime().atZone(ZoneId.of(DateUtils.GMT8)),
                        DateUtils.PATTERN_DATETIME_WITH_ZONE));
                user.setEndTime(DateUtils.formatZoneDateTime(accessControlSpotApplicationRecord.getAccessEndTime().atZone(ZoneId.of(DateUtils.GMT8)),
                        DateUtils.PATTERN_DATETIME_WITH_ZONE));
                user.setPersonType(SysUserService.isVisitor(accessControlSpotApplicationRecord.getApplicantHrId()) ? AccessPermissions.PersonType.VISITOR :
                        AccessPermissions.PersonType.EMPLOYEE);
                ServiceResult getGroupIdListResult = getKsHtGroupIdListOfUser(accessControlSpot, accessControlSpotApplicationRecord.getApplicantHrId());
                if (getGroupIdListResult.isSuccess()) {
                    user.setGroupIds((List<String>) getGroupIdListResult.getObject());
                } else {
                    return ServiceResult.error("获取人员组失败");
                }
                message.setUserList(Lists.newArrayList(user));
                iotcService.bindUserWithDevices(message);
            } else {
                accessControlSpotApplicationRecord.setAccessPermissionDistributionStatus(AccessPermissionDistributionStatus.SUCCESS);
            }
        } else {
            log.warn("通行权限下发失败，reMessage：{}", JSON.toJSONString(reMessage));
            // 下发异常告警
            failAlert(accessControlSpotApplicationRecord);
            if (!Objects.equals(AccessPermissionDistributionStatus.DOING, accessControlSpotApplicationRecord.getAccessPermissionDistributionStatus())) {
                log.error("门禁点权限申请通行下发状态不是执行中，id:{}, status is {}, ", accessControlSpotApplicationRecord.getId(),
                        accessControlSpotApplicationRecord.getAccessPermissionDistributionStatus());
                return ServiceResult.error("门禁点权限申请通行下发状态不是执行中", ErrConst.E01);
            }
            accessControlSpotApplicationRecord.setAccessPermissionDistributionStatus(AccessPermissionDistributionStatus.FAILED);
        }
        accessControlSpotApplicationRecordDao.save(accessControlSpotApplicationRecord);
        return ServiceResult.ok();
    }

    /**
     * 通行权限下发失败通知
     *
     * @param accessControlSpotApplicationRecord
     */
    private void failAlert(AccessControlSpotApplicationRecord accessControlSpotApplicationRecord) {
        if (CollectionUtils.isEmpty(failAlertHrIds)) {
            return;
        }
        List<SysUser> users = failAlertHrIds.stream().map(hrId -> sysUserService.getUserByEmpNo(hrId))
                .filter(s -> Objects.nonNull(s) && StringUtils.isNotBlank(s.getWxOpenId())).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(users)) {
            log.warn("根据工号无法找到对应人员的wxOpenId");
            return;
        }
        SysUser applicant = sysUserService.getUserByEmpNo(accessControlSpotApplicationRecord.getApplicantHrId());
        try {
            users.forEach(user -> {
                MessagePoolRecord messagePoolRecord = new MessagePoolRecord();
                messagePoolRecord.setMsgFromSys(8);
                messagePoolRecord.setMsgType(3);
                messagePoolRecord.setPriority(0);
                messagePoolRecord.setSendType(0);
                messagePoolRecord.setReceiveType(501);
                messagePoolRecord.setMsgTitle("通行权限下发失败通知");
                MessageBody messageBody = new MessageBody();
                messageBody.setTitle(String.format("您好，%s申请通行权限下发失败请及时处理！",
                        applicant.getNameZh() + "/" + applicant.getEmpNo() + "/" + applicant.getDeptName()));
                messageBody.setType("通行权限下发");
                messageBody.setDepart("下发失败");
                messageBody.setTime(DateUtils.formatLocalDateTime(LocalDateTime.now(), DateUtils.PATTERN_DATETIME));
                messageBody.setRemark("失败门禁：" + accessControlSpotApplicationRecord.getSpotName());
                messagePoolRecord.setMsgContent(JSON.toJSONString(messageBody));
                messagePoolRecord.setMsgDetailsUrl("");
                messagePoolRecord.setMsgTemplateId(vxMsgTemplateId);
                messagePoolRecord.setTipType(1);
                messagePoolRecord.setReceivePerson(user.getWxOpenId());
                messagePoolRecord.setReceivePersonId(user.getWxOpenId());
                messageSendService.send(messagePoolRecord);
            });
        } catch (Exception e) {
            log.error("发送消息中心失败", e);
        }
    }

    private AccessControlSpotApplicationRecord getAccessControlSpotApplicationRecordOfSaveReturnMessage(String id, int recursionTime) {
        if (recursionTime > 5) {
            throw new BizLogicNotMatchException("门禁授权申请未审批");
        }
        Optional<AccessControlSpotApplicationRecord> accessControlSpotApplicationRecordOptional = accessControlSpotApplicationRecordDao.findById(
                id);
        if (accessControlSpotApplicationRecordOptional.isEmpty()) {
            throw new BizLogicNotMatchException("门禁授权申请未找到，id：" + id);
        }
        AccessControlSpotApplicationRecord accessControlSpotApplicationRecord = accessControlSpotApplicationRecordOptional.get();
        if (Objects.isNull(accessControlSpotApplicationRecord.getAccessPermissionDistributionStatus())) {
            log.warn("审批申请事务还未提交，id:{}", id);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                log.error("fail to sleep", e);
                Thread.currentThread().interrupt();
            }
            return getAccessControlSpotApplicationRecordOfSaveReturnMessage(id, ++recursionTime);
        }
        return accessControlSpotApplicationRecord;
    }
}
