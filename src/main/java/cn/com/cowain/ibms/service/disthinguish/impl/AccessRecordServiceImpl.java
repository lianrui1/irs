package cn.com.cowain.ibms.service.disthinguish.impl;

import cn.com.cowain.ibms.dao.ability.OpenAbilityAccessControlSpotDao;
import cn.com.cowain.ibms.dao.distinguish.AccessRecordDao;
import cn.com.cowain.ibms.dao.distinguish.DeviceCloudDao;
import cn.com.cowain.ibms.dao.iot.AccessControlSpotDao;
import cn.com.cowain.ibms.dao.temp.AttendanceEquipmentDao;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.ability.OpenAbilityAccessControlSpot;
import cn.com.cowain.ibms.entity.distinguish.AccessRecord;
import cn.com.cowain.ibms.entity.distinguish.AccessRecordMessage;
import cn.com.cowain.ibms.entity.distinguish.DeviceCloud;
import cn.com.cowain.ibms.entity.iot.AccessControlSpot;
import cn.com.cowain.ibms.entity.iot.DeviceAccessRecord;
import cn.com.cowain.ibms.entity.temp.AttendanceEquipment;
import cn.com.cowain.ibms.enumeration.ability.Ability;
import cn.com.cowain.ibms.enumeration.iot.DeviceAccessRecordType;
import cn.com.cowain.ibms.enumeration.iot.PersonType;
import cn.com.cowain.ibms.feign.hk_server_connector.ks.HqAccessRecordService;
import cn.com.cowain.ibms.feign.hk_server_connector.nt.NtAccessRecordService;
import cn.com.cowain.ibms.feign.oa.OAApi;
import cn.com.cowain.ibms.mq.producer.AccessRecordProducer;
import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.distinguish.AccessRecordPageReq;
import cn.com.cowain.ibms.rest.req.hk.FaceRecord;
import cn.com.cowain.ibms.rest.resp.EquipmentDataResp;
import cn.com.cowain.ibms.rest.resp.SearchStaffDataResp;
import cn.com.cowain.ibms.rest.resp.distinguish.AccessRecordPageResp;
import cn.com.cowain.ibms.rest.resp.distinguish.WorkTimeCheckResp;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.disthinguish.AccessRecordHqService;
import cn.com.cowain.ibms.service.iot.AccessControlSpotAccessRecordService;
import cn.com.cowain.ibms.utils.DateUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 通行记录service逻辑实现类
 *
 * @author Yang.Lee
 * @date 2021/3/15 13:10
 */
@Slf4j
@Service
public class AccessRecordServiceImpl implements AccessRecordHqService {

    @Resource
    private AccessRecordDao accessRecordDao;

    @Resource
    private HqAccessRecordService hqAccessRecordService;

    @Resource
    private NtAccessRecordService ntAccessRecordService;

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserService;

    @Resource
    private DeviceCloudDao deviceCloudDao;

    @Resource
    private AccessControlSpotAccessRecordService accessControlSpotAccessRecordService;

    @Resource
    private AttendanceEquipmentDao attendanceEquipmentDao;

    @Resource
    private AccessRecordProducer accessRecordProducer;

    @Resource
    private AccessControlSpotDao accessControlSpotDao;

    @Resource
    private OpenAbilityAccessControlSpotDao openAbilityAccessControlSpotDao;

    @Resource
    private OAApi oaApi;

    /**
     * 查询通行记录分页数据
     *
     * @param req 查询参数
     * @return 分页数据
     * @author Yang.Lee
     * @date 2021/3/15 12:57
     **/
    @Override
    @Transactional
    public PageBean<AccessRecordPageResp> getPage(AccessRecordPageReq req) {

        int page = req.getPage();
        int size = req.getSize();
        // 创建分页查询对象
        Pageable pageable = PageRequest.of(page, size, Sort.by(req.getOrders()));

        Page<AccessRecord> accessRecordPage = accessRecordDao.findAll((root, criteriaQuery, criteriaBuilder) -> {

            // 构造查询条件
            List<Predicate> list = new ArrayList<>();

            // 员工名称
            String staffName = req.getStaffName();
            if (StringUtils.isNotEmpty(staffName)) {
                list.add(criteriaBuilder.like(root.get("nameZh").as(String.class), "%" + staffName + "%"));
            }
            // 设备名称
            String deviceName = req.getDeviceName();
            if (StringUtils.isNotEmpty(deviceName)) {
                list.add(criteriaBuilder.like(root.get("deviceName").as(String.class), "%" + deviceName + "%"));
            }

            // 通行时间
            if (req.getFrom() != null && req.getTo() != null) {
                list.add(criteriaBuilder.between(root.get("dataTime").as(String.class),
                        DateUtils.formatLocalDateTime(req.getFrom(), DateUtils.PATTERN_DATETIME),
                        DateUtils.formatLocalDateTime(req.getTo(), DateUtils.PATTERN_DATETIME)));
            }

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, pageable);

        // 数据对象转换
        List<AccessRecordPageResp> contentList = convertList(accessRecordPage.getContent());

        PageBean<AccessRecordPageResp> pageBean = new PageBean<>();

        pageBean.setPage(page);
        pageBean.setSize(size);
        pageBean.setList(contentList);
        pageBean.setTotalPages(accessRecordPage.getTotalPages());
        pageBean.setTotalElements(accessRecordPage.getTotalElements());

        return pageBean;
    }

    /**
     * 对象转换
     *
     * @param recordList 待转对象
     * @return 目标对象
     * @author Yang.Lee
     * @date 2021/3/15 13:12
     **/
    @Override
    public List<AccessRecordPageResp> convertList(@NotNull List<AccessRecord> recordList) {

        List<AccessRecordPageResp> result = new ArrayList<>();
        if (recordList.isEmpty()) {
            return result;
        }

        recordList.forEach(obj -> result.add(convert(obj)));

        return result;
    }

    /**
     * 对象转换
     *
     * @param record 待转对象
     * @return 目标对象
     * @author Yang.Lee
     * @date 2021/3/15 13:11
     **/
    @Override
    public AccessRecordPageResp convert(AccessRecord record) {

        return AccessRecordPageResp.builder()
                .deviceName(record.getDeviceName())
                .deviceAddress(record.getDeviceAddress())
                .accessState(record.getAccessStatus())
                .capturePicture(record.getCapturePicture())
                .dataTime(record.getDataTime())
                .defaultPicture(record.getDefaultPicture())
                .staffName(record.getNameZh())
                .staffType(record.getStaffType())
                .temperature(record.getTemperature())
                .build();
    }

    @Override
    @Transactional
    public void save(JSONObject accessRecordJson) {
        log.debug("通行记录接收: " + accessRecordJson);
        AccessRecord accessRecord = new AccessRecord();
        // 将accessRecordJson转AccessRecord
        AccessRecordMessage ksRecord = JSON.toJavaObject(accessRecordJson, AccessRecordMessage.class);
        // 赋值
        // 通行时间
        String passTime = ksRecord.getPassTime();
        long time = Long.parseLong(passTime);
        LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneOffset.of("+8"));
        accessRecord.setDataTime(localDateTime);
        // 通行状态
        accessRecord.setAccessStatus(ksRecord.getPassType() == 1 ? "通行" : "未通行");
        // 抓拍照片
        accessRecord.setCapturePicture(ksRecord.getSnapshotUrl());
        // 底库照片
        accessRecord.setDefaultPicture(ksRecord.getPersonImageUrl());
        // 设备ID
        accessRecord.setDeviceCloudId(ksRecord.getSnCode());
        // 设备地址
        accessRecord.setDeviceAddress(ksRecord.getDeviceLocation());
        // 设备名称
        accessRecord.setDeviceName(ksRecord.getDeviceName());
        // 员工工号
        accessRecord.setEmpNo(ksRecord.getPersonCode());
        accessRecord.setNo(ksRecord.getPersonCode());
        // 员工姓名
        accessRecord.setNameZh(ksRecord.getPersonName());
        // 员工类型
        String staffType;
        PersonType personType;
        switch (ksRecord.getRecognitionType()) {
            case 1:
                staffType = "员工";
                personType = PersonType.EMPLOYEE;
                break;
            case 2:
                staffType = "访客";
                personType = PersonType.VISITOR;
                break;
            case 3:
                staffType = "重点人员";
                personType = PersonType.UNKNOWN;
                break;
            case 4:
                staffType = "陌生人";
                personType = PersonType.UNKNOWN;
                break;
            default:
                staffType = "未识别";
                personType = PersonType.UNKNOWN;
        }
        accessRecord.setStaffType(staffType);
        // 体温
        accessRecord.setTemperature(ksRecord.getTemperature());

        // 保存
        if (accessRecord.getEmpNo() == null || accessRecord.getEmpNo().isEmpty()) {
            accessRecord.setEmpNo("");
            accessRecord.setNo("");
            accessRecord.setNameZh("");
        }
        accessRecordDao.save(accessRecord);

        // 将数据写入设备通行权限表。 since V1.20

        List<DeviceCloud> deviceCloudList = deviceCloudDao.findByCloudDeviceId(accessRecord.getDeviceCloudId());
        if (deviceCloudList.isEmpty()) {
            log.error("旷世通行记录异常，未找到对应设备。通行记录：{}", accessRecordJson);
            return;
        }

        DeviceAccessRecord deviceAccessRecord = new DeviceAccessRecord();
        if (PersonType.EMPLOYEE.equals(personType)) {
            SysUser sysUser = sysUserService.getUserByEmpNo(ksRecord.getPersonCode());
            deviceAccessRecord.setEmpNo(sysUser.getEmpNo());
            deviceAccessRecord.setNameZh(sysUser.getNameZh());
            deviceAccessRecord.setDepartment(sysUser.getFullDepartmentName());
        } else {
            deviceAccessRecord.setEmpNo("--");
            deviceAccessRecord.setNameZh("陌生人");
            deviceAccessRecord.setDepartment("--");
        }

        deviceAccessRecord.setRecordType(DeviceAccessRecordType.MEGVII);
        deviceAccessRecord.setAccessStatus(accessRecord.getAccessStatus());
        deviceAccessRecord.setDataTime(localDateTime);
        Optional<DeviceCloud> deviceCloudOptional = deviceCloudList.stream().findFirst();
        if (deviceCloudOptional.isPresent()) {
            deviceAccessRecord.setIotDevice(deviceCloudOptional.get().getIotDevice());
        }

        deviceAccessRecord.setStaffType(personType);
        deviceAccessRecord.setDefaultPicture(accessRecord.getDefaultPicture());
        deviceAccessRecord.setCapturePicture(accessRecord.getCapturePicture());
        deviceAccessRecord.setTemperature(accessRecord.getTemperature());

        accessControlSpotAccessRecordService.create(deviceAccessRecord);

        // 将数据推送到考勤mq
        try {
            // 通行的数据再发给ehr
            if (ksRecord.getPassType() == 1) {
                accessRecordProducer.push(convertWorkTimeCheckResp(accessRecord));
            }
        } catch (Exception e) {
            log.error("考勤数据推送失败");
            log.error(e.getMessage(), e);
        }

        // 推送报餐回调数据
        // 判断设备是否关联到门禁点
        Optional<AccessControlSpot> accessControlSpotOptional = accessControlSpotDao.findByDeviceId(deviceAccessRecord.getIotDevice().getId());
        if (accessControlSpotOptional.isPresent()) {
            // 查询门禁点是否再报餐中
            List<OpenAbilityAccessControlSpot> openAbilityAccessControlSpotList =
                    openAbilityAccessControlSpotDao.findBySpotId(accessControlSpotOptional.get().getId());

            List<OpenAbilityAccessControlSpot> mealSpotList =
                    openAbilityAccessControlSpotList.stream()
                            .filter(obj -> obj.getOpenAbility().getAbility().equals(Ability.MEAL))
                            .collect(Collectors.toList());

            if (!CollectionUtils.isEmpty(mealSpotList)) {
                // 将数据推送给ehr
                String result = oaApi.mealReturn(deviceAccessRecord.getEmpNo(),
                        DateUtils.formatLocalDateTime(deviceAccessRecord.getDataTime(),
                                DateUtils.PATTERN_DATETIME));
                log.info("报餐回调ehr返回结果：{}", result);
            }
        }
    }

    /**
     * 获取考勤数据
     *
     * @param startTime 查询开始时间
     * @param endTime   查询结束时间
     * @return 考勤列表
     */
    @Override
    @Transactional
    public List<WorkTimeCheckResp> getWorkTimeCheckList(LocalDateTime startTime, LocalDateTime endTime) {

        List<AccessRecord> accessRecordList = accessRecordDao.findAll((root, criteriaQuery, criteriaBuilder) -> {
            // 构造查询条件
            List<Predicate> list = new ArrayList<>();

            list.add(criteriaBuilder.between(root.get("dataTime"), startTime, endTime));

            list.add(criteriaBuilder.equal(root.get("accessStatus").as(String.class), "通行"));
            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, Sort.by(Sort.Direction.DESC, "dataTime"));

        return convertWorkTimeCheckRespList(accessRecordList);
    }

    /**
     * 获取总部考勤数据
     *
     * @param startTime 查询开始时间
     * @param endTime   查询结束时间
     * @return
     */
    @Override
    public List<SearchStaffDataResp> getHqWorkTimeCheckList(LocalDate startTime, LocalDate endTime) {
        FaceRecord faceRecord = new FaceRecord();
        faceRecord.setStartDate(startTime.toString());
        faceRecord.setEndDate(endTime.toString());
        faceRecord.setType("automatic");
        // 获取总部通行记录数据
        JsonResult<List<SearchStaffDataResp>> searchStaffResp = hqAccessRecordService.searchStaff(faceRecord);
        // 获取南通通行记录数据
        JsonResult<List<SearchStaffDataResp>> searchStaffResp1 = ntAccessRecordService.searchStaff(faceRecord);

        if ("0".equals(searchStaffResp.getCode()) && "0".equals(searchStaffResp1.getCode())) {
            log.info("返回数据为:{}", searchStaffResp.getData().toString());
            log.info("返回数据为:{}", searchStaffResp1.getData().toString());

            List<SearchStaffDataResp> data = searchStaffResp.getData();
            data.addAll(searchStaffResp1.getData());
            return data;
        }
        return null;
    }

    /**
     * 获取设备列表
     *
     * @return
     */
    @Override
    public List<EquipmentDataResp> getHqEquipmentList() {

        // 获取总部设备记录数据
        JsonResult<List<EquipmentDataResp>> hqEquipmentDataResp = hqAccessRecordService.getEquipmentList();
        // 获取南通设备记录数据
        JsonResult<List<EquipmentDataResp>> ntEquipmentDataResp = ntAccessRecordService.getEquipmentList();

        if ("0".equals(hqEquipmentDataResp.getCode()) && "0".equals(ntEquipmentDataResp.getCode())) {
            log.info("返回数据为:{}", hqEquipmentDataResp.getData().toString());
            log.info("返回数据为:{}", ntEquipmentDataResp.getData().toString());

            List<EquipmentDataResp> data = hqEquipmentDataResp.getData();
            data.addAll(ntEquipmentDataResp.getData());

            // 获取本地的数据
            List<AttendanceEquipment> list = attendanceEquipmentDao.findAll();
            data.addAll(list.stream().map(obj -> {
                EquipmentDataResp equipmentDataResp = new EquipmentDataResp();
                BeanUtils.copyProperties(obj, equipmentDataResp);
                return equipmentDataResp;
            }).collect(Collectors.toList()));
            return data;
        }
        return null;
    }

    /**
     * 對象轉換
     *
     * @param accessRecordList 待转对象
     * @return 结果对象
     */
    public List<WorkTimeCheckResp> convertWorkTimeCheckRespList(List<AccessRecord> accessRecordList) {
        List<WorkTimeCheckResp> result = new ArrayList<>();
        if (accessRecordList == null || accessRecordList.isEmpty()) {
            return result;
        }

        accessRecordList.forEach(obj -> result.add(convertWorkTimeCheckResp(obj)));

        return result;
    }

    /**
     * 對象轉換
     *
     * @param accessRecord 待转对象
     * @return 结果对象
     */
    public WorkTimeCheckResp convertWorkTimeCheckResp(AccessRecord accessRecord) {

        // 判断出入类型，判断规则：
        // 05:00 - 17:00 算做入，17:00 - 05:00 算做出
//        int dataHour = accessRecord.getDataTime().getHour();
//        int inOutFlag;
//        if (dataHour >= 5 && dataHour < 17) {
//            inOutFlag = 2;
//        } else {
//            inOutFlag = 1;
//        }

        int personType = "员工".equals(accessRecord.getStaffType()) ? 1 : 3;

        return WorkTimeCheckResp.builder()
                .ccDeviceId(accessRecord.getDeviceName())
                .checkinSite(accessRecord.getDeviceAddress())
                .hrId(accessRecord.getEmpNo())
                .checkinTime(accessRecord.getDataTime())
                .personType(personType)
                .type(0)
                .ccDeviceCode(accessRecord.getDeviceCloudId())
                .build();
    }
}
