package cn.com.cowain.ibms.entity.meeting;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.ReservationRecord;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/12 18:27
 */
@Data
@Entity
@Table(name = "ibms_reservation_record_2_custom_user_group")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "ibms_reservation_record_2_custom_user_group", comment = "会议记录2用户组")
public class ReservationRecord2CustomGroup extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "record_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK'")
    private ReservationRecord reservationRecord;

    @ManyToOne
    @JoinColumn(name = "custom_user_group_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 用户组id'")
    private CustomUserGroup customUserGroup;
}
