package cn.com.cowain.ibms.service.ability;

import cn.com.cowain.ibms.entity.meal.MealPerson;
import cn.com.cowain.ibms.enumeration.ability.Ability;
import cn.com.cowain.ibms.enumeration.ability.MethodType;
import cn.com.cowain.ibms.mq.bean.BizReturnMessage;
import cn.com.cowain.ibms.mq.bean.OpenAbilityMessage;
import cn.com.cowain.ibms.mq.bean.UserBindMessage;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.ability.AbilityReq;
import cn.com.cowain.ibms.rest.resp.EquipmentDataResp;
import cn.com.cowain.ibms.rest.resp.ability.AbilityDetailResp;
import cn.com.cowain.ibms.rest.resp.ability.AbilityNodeResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;

import java.util.List;

/**
 * @author feng
 * @title: AbilityService
 * @projectName ibms
 * @Date 2022/1/12 9:41
 */
public interface AbilityService {


    /**
     * 获取开放能力列表
     *
     * @return 能力列表
     * @author Yang.Lee
     * @date 2022/1/14 10:52
     **/
    List<AbilityDetailResp> getList();

    ServiceResult deleteBindAbility(String id);

    PageBean<AbilityNodeResp> getAccessSpotController(AbilityReq req, Boolean hasBinding);

    ServiceResult findDetail(String id);

    /**
     * 处理开放能力消息
     *
     * @param message 消息
     * @return 处理结果
     * @author Yang.Lee
     * @date 2022/1/26 16:23
     **/
    ServiceResult process(OpenAbilityMessage message);

    /**
     * 开放能力绑定门禁点
     *
     * @param req 请求参数
     * @return 结果
     * @author Yang.Lee
     * @date 2022/2/14 9:46
     **/
    ServiceResult bindingSpot(List<AbilityReq> req);

    /**
     * 开放能力绑定门禁点
     *
     * @param ability    开放能力
     * @param spotIdList 门禁点id集合
     * @return 结果
     * @author Yang.Lee
     * @date 2022/2/14 9:59
     **/
    ServiceResult bindingSpot(Ability ability, List<String> spotIdList);

    /**
     * 将用户从鸿图的员工组中移除
     *
     * @param userIdList
     * @param kshtGroupId
     * @return 结果
     * @author Yang.Lee
     * @date 2022/2/17 13:21
     **/
    ServiceResult unbindKSHT(String kshtGroupId, List<String> userIdList);

    /**
     * 保存返回的信息
     *
     * @param message 返回消息
     * @return 操作结果
     * @author Yang.Lee
     * @date 2022/3/3 10:03
     **/
    ServiceResult saveReturnMessage(BizReturnMessage message);

    /**
     * 改变人员导入状态
     *
     * @return cn.com.cowain.ibms.service.bean.ServiceResult
     * @author: yanzy
     * @data: 2022/4/8 14:04:44
     */
    ServiceResult changeUserImportStatus(UserBindMessage userBindMessage);

    ServiceResult changeUserUnBindImportStatus(UserBindMessage userBindMessage);

    ServiceResult changeUserBatchUnBindImportStatus(UserBindMessage userBindMessage);
    /**
     * 获取某能力下的设备列表
     *
     * @param ability 能力
     * @return 设备列表
     * @author Yang.Lee
     * @date 2022/5/4 17:18
     **/
    List<EquipmentDataResp> getAbilityEquipmentList(Ability ability);

    /**
     * 报餐人员下发
     *
     * @param type
     * @param personDaoAll
     */
    void mealUserDownload(MethodType type, List<MealPerson> personDaoAll);
}
