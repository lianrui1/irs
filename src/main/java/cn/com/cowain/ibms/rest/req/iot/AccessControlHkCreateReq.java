package cn.com.cowain.ibms.rest.req.iot;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/18 18:27
 */
@Data
@ApiModel("hk添加人员门禁点权限请求对象")
public class AccessControlHkCreateReq {

    @ApiModelProperty(value = "人员信息")
    private List<UserReq> users;

    @ApiModelProperty(value = "门禁点ID")
    private List<String> accessControlIds;

    @ApiModelProperty(value = "设备ID")
    private List<String> deviceIds;

    @ApiModelProperty(value = "部门id")
    private List<String> departmentIds;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "通行开始时间", required = true, position = 7)
    private LocalDateTime startTime;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "通行结束时间", required = true, position = 8)
    private LocalDateTime endTime;
}
