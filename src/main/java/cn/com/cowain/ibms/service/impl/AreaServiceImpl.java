package cn.com.cowain.ibms.service.impl;

import cn.com.cowain.ibms.dao.AreaDao;
import cn.com.cowain.ibms.entity.Area;
import cn.com.cowain.ibms.rest.resp.AreaResp;
import cn.com.cowain.ibms.service.AreaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2020/12/22 15:45
 */
@Slf4j
@Service
public class AreaServiceImpl implements AreaService {

    @Autowired
    private AreaDao areaDao;

    /**
     * 获取所有省级信息
     * @return
     */
    @Override
    @Transactional
    public List<AreaResp> searchAreaProvinceList() {
        List<Area> areaList = areaDao.findByLevel(1);

        List<AreaResp> areaSearchRespList = new ArrayList<>();
        AreaResp areatSearchResp = null;

        for (Area area : areaList) {
            areatSearchResp = new AreaResp();
            BeanUtils.copyProperties(area, areatSearchResp);
            areaSearchRespList.add(areatSearchResp);
        }
        return areaSearchRespList;
    }

    /**
     * 根据省级id获取市列表
     *
     * @param id
     * @return
     */
    @Override
    @Transactional
    public List<AreaResp> searchAreaCityList(Long id) {
        List<Area> areaList = areaDao.findByLevelAndParentId(2,id);

        List<AreaResp> areaSearchRespList = new ArrayList<>();
        AreaResp areatSearchResp = null;

        for (Area area : areaList) {
            areatSearchResp = new AreaResp();
            BeanUtils.copyProperties(area, areatSearchResp);
            areaSearchRespList.add(areatSearchResp);
        }
        return areaSearchRespList;
    }

    /**
     * 根据市级id获取区列表
     *
     * @param id
     * @return
     */
    @Override
    @Transactional
    public List<AreaResp> searchAreaDistrictList(Long id) {
        List<Area> areaList = areaDao.findByLevelAndParentId(3,id);

        List<AreaResp> areaSearchRespList = new ArrayList<>();
        AreaResp areatSearchResp = null;

        for (Area area : areaList) {
            areatSearchResp = new AreaResp();
            BeanUtils.copyProperties(area, areatSearchResp);
            areaSearchRespList.add(areatSearchResp);
        }
        return areaSearchRespList;
    }
}
