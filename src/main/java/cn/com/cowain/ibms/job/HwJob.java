package cn.com.cowain.ibms.job;

import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.rest.resp.distinguish.SwitchResp;
import cn.com.cowain.ibms.service.bean.DoorMagneticHWReq;
import cn.com.cowain.ibms.service.device.HwProductsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 每5分钟获取华为云设备 产品列表
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/2/26 14:59
 */
@Slf4j
@Component
public class HwJob {

    @Autowired
    private HwProductsService hwProductsService;
    @Autowired
    private DeviceDao deviceDao;

//    @Scheduled(cron = "0 0/5 * * * ? ")
    public void findHw(){
        // 获取华为云产品列表
        hwProductsService.findHwProducts();
        // 获取华为云设备列表
        hwProductsService.findHwDevices();
    }

    // @Scheduled(cron = "0  59  23  *  *  ?")
    public void intelligentCircuitBreaker(){
        // 每天十一点59获取指定断路器数据
        List<IotDevice> deviceList = deviceDao.findByDeviceType(DeviceType.CIRCUIT_BREAKER);
            for(IotDevice device : deviceList){
                DoorMagneticHWReq hwReq = hwProductsService.findProduct(device.getHwProductId());
                SwitchResp data = hwProductsService.findProperties(device.getHwDeviceId(), hwReq.getService_id());
                hwProductsService.saveCircuitBreaker(device ,data);
            }
    }

    /**
     * 在每分钟的0s 通过毫米波传感器判断是否有人
     */
//    @Scheduled(cron = "0 * * * * ?")
    public void run(){
        // 根据毫米波产品id获取其下所有毫米波传感器
        List<IotDevice> deviceList = deviceDao.findByDeviceTypeAndSpaceNotNull(DeviceType.SENSOR_BODY_SENSING);
        // 根据毫米波传感器控制设备
        hwProductsService.sensorBodySensing(deviceList);
    }
}
