package cn.com.cowain.ibms.rest.req.iot;

import cn.com.cowain.ibms.enumeration.ApprovalStatus;
import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 申请|分配记录查询参数
 *
 * @author: yanzy
 * @date: 2022/3/8 13:42
 */
@Data
@ApiModel("申请|分配记录分页查询请求")
public class AccessControlSpotApplicationRecordParamReq extends PageReq {

    @ApiModelProperty(value = "审批人",position = 1)
    private String approver;

    @ApiModelProperty(value = "申请人",position = 2)
    private String applicant;

    @ApiModelProperty(value = "审批状态",position = 3)
    private ApprovalStatus status;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "申请开始时间",position = 4)
    private LocalDateTime  applicationStartTime ;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "申请结束时间",position = 5)
    private LocalDateTime applicationEndTime;
}
