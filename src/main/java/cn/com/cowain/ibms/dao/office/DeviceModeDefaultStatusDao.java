package cn.com.cowain.ibms.dao.office;


import cn.com.cowain.ibms.entity.office.DeviceModeDefaultStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeviceModeDefaultStatusDao extends JpaRepository<DeviceModeDefaultStatus, String>, JpaSpecificationExecutor<DeviceModeDefaultStatus> {

    // 根据场景模式ID获取设备状态
    List<DeviceModeDefaultStatus> findByModeId(String id);
}
