package cn.com.cowain.ibms.rest.bean;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 自定义 JSON 结果类
 *
 * @author Hu Jingling
 * @version 1.1
 * @since 2020-08-0
 */
@Data
@ApiModel("Json响应结果")
public class JsonResult<T> implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 状态码 0:操作失败;1操作成功
     */
    @ApiModelProperty(value = "状态码 0:操作失败;1操作成功", required = true, position = 0)
    private int status;

    /**
     * 文中提示信息，由服务器返回后，在H5显示
     */
    @ApiModelProperty(value = "提示信息", required = true, position = 1)
    private String msg;

    /**
     * 错误代码，系统全局唯一
     */
    @ApiModelProperty(value = "服务端错误代码", position = 2)
    private String code;

    /**
     * 返回json 的数据部分
     */
    @ApiModelProperty(value = "数据部分", position = 3)
    private T data;

    /**
     * 无参构造
     */
    public JsonResult() {
    }

    /**
     * 有参构造
     *
     * @param status 状态参数
     * @param msg    返回语句
     * @param data   返回数据
     */
    public JsonResult(int status, String msg, T data) {
        this.status = status;
        this.msg = msg;
        this.data = data;
    }


    /**
     * 创建一个操作成功的 JsonResult
     *
     * @param message 消息字符串
     * @param data    数据
     * @return 操作成功的 JsonResult
     */
    public static <T> JsonResult<T> ok(String message, T data) {
        JsonResult<T> jsonResult = new JsonResult<>();
        jsonResult.setStatus(1);
        jsonResult.setMsg(message);
        jsonResult.setData(data);
        /*
        * todo 暂时先用0来表示ok
        * */
        jsonResult.setCode("0");
        return jsonResult;
    }

    /**
     * 创建一个操作成功的 JsonResult
     *
     * @param message 消息字符串
     * @return 操作成功的 JsonResult
     */
    public static <T> JsonResult<T> okNoData(String message) {
        JsonResult<T> jsonResult = new JsonResult<>();
        jsonResult.setStatus(1);
        jsonResult.setMsg(message);
        jsonResult.setData(null);
        /*
         * todo 暂时先用0来表示
         * */
        jsonResult.setCode("0");
        return jsonResult;
    }

    /**
     * 创建一个操作失败的 JsonResult
     *
     * @param message 消息字符串
     * @param data    数据
     * @param errCode 错误代码
     * @return 操作成功的 JsonResult
     */
    public static <T> JsonResult<T> error(String message, T data, String errCode) {
        JsonResult<T> jsonResult = new JsonResult<>();
        jsonResult.setStatus(0);
        jsonResult.setMsg(message);
        jsonResult.setData(data);
        jsonResult.setCode(errCode);
        return jsonResult;
    }

}
