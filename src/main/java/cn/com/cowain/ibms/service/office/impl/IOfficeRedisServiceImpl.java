package cn.com.cowain.ibms.service.office.impl;

import cn.com.cowain.ibms.bo.SimpleIntelligentOfficeInitialization;
import cn.com.cowain.ibms.component.IntelligentOfficeRedis;
import cn.com.cowain.ibms.dao.office.IntelligentOfficeDao;
import cn.com.cowain.ibms.dao.office.IntelligentOfficeInitializationDao;
import cn.com.cowain.ibms.entity.office.IntelligentOffice;
import cn.com.cowain.ibms.entity.office.IntelligentOfficeInitialization;
import cn.com.cowain.ibms.service.office.IOfficeRedisService;
import cn.com.cowain.ibms.utils.DataUtils;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.ibms.utils.RedisUtil;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class IOfficeRedisServiceImpl implements IOfficeRedisService {
    @Autowired
    RedisUtil redisUtil;
    @Autowired
    private IntelligentOfficeInitializationDao intelligentOfficeInitializationDao;
    @Autowired
    private IntelligentOfficeDao intelligentOfficeDao;

    @Override
    //得到Redis初始化办公室
    public Optional<SimpleIntelligentOfficeInitialization> getIOIFromRedis(String hrId, String officeId) {
        String officeInitKey = redisUtil.createOfficeRealKey(IConst.OFFICE_USER_MODULE, officeId, hrId, RedisUtil.Separator.COLON);
        if (redisUtil.hasKey(officeInitKey)) {
            return getIntelligentOfficeInitRedis(officeInitKey);
        } else {
            return setIntelligentOfficeInitRedis(officeInitKey, hrId, officeId);
        }
    }

    @Override
    //放入Redis初始化办公室
    public Optional<SimpleIntelligentOfficeInitialization> setIOIFromRedis(String hrId, String officeId) {
        String officeInitKey = redisUtil.createOfficeRealKey(IConst.OFFICE_USER_MODULE, officeId, hrId, RedisUtil.Separator.COLON);
        if (redisUtil.hasKey(officeInitKey)) {
            redisUtil.del(officeInitKey);
            return setIntelligentOfficeInitRedis(officeInitKey, hrId, officeId);
        }
        return Optional.empty();
    }

    @Override
    //将初始化对象放入Redis
    public Optional<SimpleIntelligentOfficeInitialization> setIntelligentOfficeInitRedis(String officeInitKey, String hrId, String officeId) {
        SimpleIntelligentOfficeInitialization initialization = intelligentOfficeInitializationDao.findSimpleIntelligentOfficeInitializationByUserHrIdAndIntelligentOfficeId(hrId, officeId);
        if (Objects.nonNull(initialization)) {
            redisUtil.hmset(officeInitKey, DataUtils.targetObjectCastMap(initialization, SimpleIntelligentOfficeInitialization.class), false);
            return Optional.of(initialization);
        }
        return Optional.empty();

    }

    @Override
    //获取办公室初始化对象
    public Optional<SimpleIntelligentOfficeInitialization> getIntelligentOfficeInitRedis(String officeInitKey) {
        SimpleIntelligentOfficeInitialization intelligentOfficeInitRedis = DataUtils.mapCastTargetObject(redisUtil.hmget(officeInitKey), SimpleIntelligentOfficeInitialization.class);
        if (intelligentOfficeInitRedis != null && !StringUtils.isEmpty(intelligentOfficeInitRedis.getIntelligentOffice())) {
            return Optional.of(intelligentOfficeInitRedis);
        }
        return Optional.empty();
    }

    @Override
    //将办公室取出或放入Redis
    public IntelligentOfficeRedis getIntelligentOfficeRedis(String intelligentOffice) {
        IntelligentOfficeRedis intelligentOfficeRedis;
        String officeInitKey = redisUtil.createOfficeRealKey(IConst.OFFICE_MODULE, intelligentOffice, null, RedisUtil.Separator.COLON);
        if (redisUtil.hasKey(officeInitKey)) {
            intelligentOfficeRedis = DataUtils.mapCastTargetObject(redisUtil.hmget(officeInitKey), IntelligentOfficeRedis.class);
            return intelligentOfficeRedis;
        } else {
            Optional<IntelligentOffice> officeOpt = intelligentOfficeDao.findById(intelligentOffice);
            if (officeOpt.isPresent()) {
                intelligentOfficeRedis = new IntelligentOfficeRedis();
                IntelligentOffice office = officeOpt.get();
                BeanUtils.copyProperties(office, intelligentOfficeRedis);
                intelligentOfficeRedis.setSpace(office.getSpace().getId());
                redisUtil.hmset(officeInitKey, DataUtils.targetObjectCastMap(intelligentOfficeRedis, IntelligentOfficeRedis.class), false);
                return intelligentOfficeRedis;
            }
        }
        return null;
    }

    @Override
    //将办公室放入Redis
    public void setIntelligentOfficeRedis(IntelligentOffice intelligentOffice) {
        String officeInitKey = redisUtil.createOfficeRealKey(IConst.OFFICE_MODULE, intelligentOffice.getId(), null, RedisUtil.Separator.COLON);
        if (redisUtil.hasKey(officeInitKey)) {
            redisUtil.del(officeInitKey);
            setOfficeRedis(officeInitKey, intelligentOffice);
        } else {
            setOfficeRedis(officeInitKey, intelligentOffice);
        }
    }

    @Override
    public void delIntelligentOfficeRedis(IntelligentOffice intelligentOffice) {
        String officeInitKey = redisUtil.createOfficeRealKey(IConst.OFFICE_MODULE, intelligentOffice.getId(), null, RedisUtil.Separator.COLON);
        if (redisUtil.hasKey(officeInitKey)) {
            redisUtil.del(officeInitKey);
        }
    }

    @Override
    public void delIntelligentOfficeInitRedis(IntelligentOfficeInitialization intelligentOfficeInitialization) {
        String officeId=intelligentOfficeInitialization.getIntelligentOffice().getId();
        String hrId=intelligentOfficeInitialization.getUserHrId();
        String officeInitKey = redisUtil.createOfficeRealKey(IConst.OFFICE_USER_MODULE, officeId, hrId, RedisUtil.Separator.COLON);
        if (redisUtil.hasKey(officeInitKey)) {
            redisUtil.del(officeInitKey);
        }
    }

    private void setOfficeRedis(String officeInitKey, IntelligentOffice intelligentOffice) {
        IntelligentOfficeRedis intelligentOfficeRedis = new IntelligentOfficeRedis();
        BeanUtils.copyProperties(intelligentOffice, intelligentOfficeRedis);
        intelligentOfficeRedis.setSpace(intelligentOffice.getSpace().getId());
        redisUtil.hmset(officeInitKey, DataUtils.targetObjectCastMap(intelligentOfficeRedis, IntelligentOfficeRedis.class), false);
        log.info("将办公室：{}放入Redis",intelligentOfficeRedis);
    }
}
