package cn.com.cowain.ibms.rest.resp.device.centralized;

import cn.com.cowain.ibms.entity.iot.IotDevice;
import lombok.Data;

import java.util.List;

@Data
public class CentralizedControlDeviceFilter {
    //有效设备列表
    private List<IotDevice> validDevices;
    //离线设备列表
    private List<IotDevice> offLineDevices;
    //无效设备列表：状态已经是开或闭的设备
    private List<IotDevice> invalidDevices;

}
