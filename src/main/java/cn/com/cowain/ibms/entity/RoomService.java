package cn.com.cowain.ibms.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/7/31 13:47
 */
@Data
@Entity
@Table(name = "ibms_room_service")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_room_service", comment = "会议室服务")
public class RoomService extends BaseEntity {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    @Column(name = "rs_name", nullable = false, columnDefinition = "varchar(50) COMMENT '名称'")
    private String name;

    /**
     * 规格，预留
     */
    @Column(name = "rs_spec", columnDefinition = "varchar(100) COMMENT '规格，预留'")
    private String spec;

}
