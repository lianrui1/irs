package cn.com.cowain.ibms.rest.req.space;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author Yang.Lee
 * @date 2021/11/9 9:29
 */
@Data
@ApiModel("项目区域编辑请求")
public class ProjectAreaEditReq {

    @Length(max = 10)
    @NotBlank
    @ApiModelProperty(value = "区域名称", required = true, position = 1)
    private String name;
}
