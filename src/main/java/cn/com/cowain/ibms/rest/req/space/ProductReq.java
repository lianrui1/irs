package cn.com.cowain.ibms.rest.req.space;

import cn.com.cowain.ibms.enumeration.IotNodeType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.List;

/**
 * @author yuchunlei
 * @version 1.0.0
 * @createTime 2020年12月17日 11:30:00
 */
@Data
@ApiModel("产品信息")
public class ProductReq implements Serializable {

    private static final long serialVersionUID = 8221601774855330853L;

    @NotBlank(message = "产品名称不能为空")
    @Pattern(regexp = "^[a-z0-9A-Z\u4e00-\u9fa5]{1,15}$", message = "产品名称校验失败，只允许输入中文英文数字，且长度不超过15")
    @ApiModelProperty(value = "产品名称", required = true, position = 0)
    private String name;

    @NotNull(message = "节点类型不能为空")
    @ApiModelProperty(value = "IOT网络节点 类型", required = true, position = 1)
    private IotNodeType iotNodeType;

    @ApiModelProperty(value = "关联数据点", position = 2)
    private List<String> iotAttributes;

    @Pattern(regexp = "^[a-z0-9A-Z\u4e00-\u9fa5]{0,15}$", message = "产品名称校验失败，只允许输入中文英文数字，且长度不超过15")
    @ApiModelProperty(value = "产品名称备注", required = true, position = 3)
    private String remark;

    @ApiModelProperty(value = "iot产品名称En", required = true, position = 4)
    private String nameEn;
}
