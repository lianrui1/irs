package cn.com.cowain.ibms.utils;

import lombok.extern.slf4j.Slf4j;
import org.csource.common.MyException;
import org.csource.common.NameValuePair;
import org.csource.fastdfs.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;

/**
 * @author Yang.Lee
 * @date 2020/12/24 17:48
 */
@Slf4j
@Component
public class FastDFSUtil {

    @Value("${fastdfs.connect_timeout}")
    private String connecTimeout;
    @Value("${fastdfs.network_timeout}")
    private String networkTimeout;
    @Value("${fastdfs.charset}")
    private String charset;
    @Value("${fastdfs.http.tracker_http_port}")
    private String trackerHttpPort;
    @Value("${fastdfs.http.anti_steal_token}")
    private String antiStealToken;
    @Value("${fastdfs.http.secret_key}")
    private String secretKey;
    @Value("${fastdfs.tracker_server}")
    private String trackerServerAddress;

    private static final int DEFAULT_CONNECT_TIMEOUT = 5;

    private static final int DEFAULT_NETWORK_TIMEOUT = 30;

    /**
     * 初始化配置
     * @throws MyException
     */
    @PostConstruct
    public void init() throws MyException {

        // springboot 读取配置文件失败，因此手动实现服务配置

        int gConnectTimeout = connecTimeout == null ? DEFAULT_CONNECT_TIMEOUT : Integer.parseInt(connecTimeout);
        if(gConnectTimeout < 0){
            gConnectTimeout = DEFAULT_CONNECT_TIMEOUT;
        }
        gConnectTimeout *= 1000;
        ClientGlobal.setG_connect_timeout(gConnectTimeout);

        int gNetworkTimeout = networkTimeout == null ? DEFAULT_NETWORK_TIMEOUT : Integer.parseInt(networkTimeout);
        if(gNetworkTimeout < 0){
            gNetworkTimeout = DEFAULT_NETWORK_TIMEOUT;
        }
        gNetworkTimeout *= 1000;
        ClientGlobal.setG_network_timeout(gNetworkTimeout);

        String gCharset = charset == null || charset.length() == 0 ? "ISO8859-1" : charset;
        ClientGlobal.setG_charset(gCharset);

        if(trackerServerAddress == null){
            throw new MyException("item \"tracker_server\" not found");
        } else{
            String[] szTrackerServers = trackerServerAddress.split(";");
            InetSocketAddress[] trackerervers = new InetSocketAddress[szTrackerServers.length];

            for(int i = 0; i < szTrackerServers.length; ++i) {
                String[] parts = szTrackerServers[i].split("\\:", 2);
                if (parts.length != 2) {
                    throw new MyException("the value of item \"tracker_server\" is invalid, the correct format is host:port");
                }

                trackerervers[i] = new InetSocketAddress(parts[0].trim(), Integer.parseInt(parts[1].trim()));
            }

            ClientGlobal.setG_tracker_group(new TrackerGroup(trackerervers));
            int gTrackerHttpPort = trackerHttpPort == null ? 80 : Integer.parseInt(trackerHttpPort);
            ClientGlobal.setG_tracker_http_port(gTrackerHttpPort);

            boolean gAntiStealToken = false;
            if(antiStealToken != null){
                gAntiStealToken = antiStealToken.equalsIgnoreCase("yes") || antiStealToken.equalsIgnoreCase("on")
                        || antiStealToken.equalsIgnoreCase("true") || antiStealToken.equalsIgnoreCase("1");
            }
            ClientGlobal.setG_anti_steal_token(gAntiStealToken);
            if (gAntiStealToken) {
                ClientGlobal.setG_secret_key(secretKey);
            }
        }
        
    }

    /**
     * 上传文件到fastdfs服务器
     * @param inputStream
     * @param localFileName
     * @return
     * @throws Exception
     */
    public String upload(InputStream inputStream, String localFileName) throws IOException, MyException {

        if(inputStream == null){
            throw new NullPointerException("输入流为null");
        }

        // 拼接上传结果
        StringBuilder builder = new StringBuilder();

        int length = inputStream.available();
        byte[] bytes = new byte[length];
        // 读取文件
        int readLen = inputStream.read(bytes);
        if(readLen > -1){
            NameValuePair[] metaList = new NameValuePair[1];
            metaList[0] = new NameValuePair("fileName", localFileName);

            // 获取文件扩展名
            String ext = localFileName.substring(localFileName.lastIndexOf(".") + 1 , localFileName.length());

            TrackerClient tracker = new TrackerClient();
            TrackerServer trackerServer = tracker.getConnection();
            StorageServer storageServer = null;
            StorageClient storageClient = new StorageClient(trackerServer, storageServer);
            // 上传
            log.debug("开始上传文件到fastdfs, 文件名:"+localFileName+", 文件大小:" + bytes.length);

            String[] result = storageClient.upload_file(bytes, ext, metaList);

            for(String str : result){
                builder.append("/").append(str);
            }
        }

        // 关闭流
        inputStream.close();

        return builder.toString();
    }
}
