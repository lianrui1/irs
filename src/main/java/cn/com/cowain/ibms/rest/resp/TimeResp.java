package cn.com.cowain.ibms.rest.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * TimeResp 时间响应
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/8/20
 */
@Data
@ApiModel("时间响应")
public class TimeResp implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     * 时间字符串
     */
    @ApiModelProperty(value = "时间字符串", required = true, example = "2000-12-31")
    private String time;

}
