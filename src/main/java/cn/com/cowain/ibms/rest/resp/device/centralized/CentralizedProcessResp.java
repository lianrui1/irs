package cn.com.cowain.ibms.rest.resp.device.centralized;

import lombok.Data;

@Data
public class CentralizedProcessResp {
    private String traceId;
}
