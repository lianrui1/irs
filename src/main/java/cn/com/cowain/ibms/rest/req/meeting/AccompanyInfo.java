package cn.com.cowain.ibms.rest.req.meeting;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/9 15:12
 */
@Data
public class AccompanyInfo {

    @ApiModelProperty(value = "随行人姓名", required = true, example = "", position = 1)
    private String name;

    @ApiModelProperty(value = "随行人手机", required = true, example = "", position = 2)
    private String phone;
}
