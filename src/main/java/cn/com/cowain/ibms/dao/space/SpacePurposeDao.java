package cn.com.cowain.ibms.dao.space;

import cn.com.cowain.ibms.entity.space.SpacePurpose;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

/**
 * @author Yang.Lee
 * @date 2021/11/17 20:10
 */
public interface SpacePurposeDao extends JpaRepository<SpacePurpose, String>, JpaSpecificationExecutor<SpacePurpose> {

    /**
     * 根据名称查找
     *
     * @param name 用途名称
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/11/17 20:11
     **/
    Optional<SpacePurpose> findByName(String name);
}
