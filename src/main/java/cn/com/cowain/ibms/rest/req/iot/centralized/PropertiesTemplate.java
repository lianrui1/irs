package cn.com.cowain.ibms.rest.req.iot.centralized;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class PropertiesTemplate {
    /**
     * 执行命令的设备id列表
     */
    private List<String> deviceIds;
    /**
     * 设备的服务ID
     */
    private String serviceId;

    @ApiModelProperty(value = "四路开关 on off", position = 8)
    private String light1;

    @ApiModelProperty(value = "四路开关 on off", position = 9)
    private String light2;

    @ApiModelProperty(value = "四路开关 on off", position = 10)
    private String light3;

    @ApiModelProperty(value = "四路开关 on off", position = 11)
    private String light4;

}
