package cn.com.cowain.ibms.rest.advice;

import cn.com.cowain.ibms.aop.FieldErrorWrapper;
import cn.com.cowain.ibms.exceptions.NewValidationFailedException;
import cn.com.cowain.ibms.exceptions.UnAuthorizedException;
import cn.com.cowain.ibms.exceptions.ValidationFailedException;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.ibms.utils.ErrConst;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.time.DateTimeException;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;

/**
 * Iot JSR303 异常处理
 *
 * @author Hu Jingling
 */
@Slf4j
@ControllerAdvice
public class BeanValidationAdvice {

    /**
     * 处理异常
     *
     * @param e 异常参数
     * @return json结果
     */
    @ExceptionHandler(ValidationFailedException.class)
    @ResponseBody
    @ResponseStatus(OK)
    public JsonResult<FieldErrorWrapper> validationFailed(ValidationFailedException e) {
        log.trace("ValidationFailedException Found:" + e);
        return JsonResult.error("Validation Failed", e.getFieldErrorWrapper(), ErrConst.E01);
    }

    /**
     * 新参数校验异常处理，V1.2版本开始使用
     * @param e
     * @return
     */
    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseBody
    @ResponseStatus(OK)
    public JsonResult<String> missingServletRequestParameterException(MissingServletRequestParameterException e) {
        log.trace("MissingServletRequestParameterException Found:" + e);
        return JsonResult.error(e.getMessage(), null, ErrConst.E01);
    }

    /**
     * 新参数校验异常处理，V1.2版本开始使用
     * @param e
     * @return
     */
    @ExceptionHandler(NewValidationFailedException.class)
    @ResponseBody
    @ResponseStatus(OK)
    public JsonResult<FieldErrorWrapper> newValidationFailed(NewValidationFailedException e) {
        log.trace("NewValidationFailedException Found:" + e);
        return JsonResult.error("Validation Failed", e.getFieldErrorWrapper(), ErrConst.E01);
    }


    @ExceptionHandler(UnAuthorizedException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public JsonResult<FieldErrorWrapper> accessFailed(UnAuthorizedException e) {

        return JsonResult.error("Validation Failed", null, ErrConst.E401);
    }

    /**
     * 处理异常
     *
     * @param e 异常参数
     * @return json结果
     */
    @ExceptionHandler({NumberFormatException.class, DateTimeException.class})
    @ResponseBody
    @ResponseStatus(NOT_FOUND)
    public JsonResult<String> numberFormat(RuntimeException e) {
        log.debug("Format Exception Found:" + e);
        return JsonResult.error("NumberFormat Failed", e.getMessage(), ErrConst.E02);
    }

}
