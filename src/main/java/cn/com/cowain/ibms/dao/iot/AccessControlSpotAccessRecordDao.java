package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.AccessControlSpotAccessRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author tql
 * @Description 门禁点通行记录DAO
 * @Date 21-12-3 上午10:41
 * @Version 1.0
 */
@Repository
public interface AccessControlSpotAccessRecordDao extends JpaRepository<AccessControlSpotAccessRecord, String>, JpaSpecificationExecutor<AccessControlSpotAccessRecord> {

    // 根据门禁点ID获取门禁点通行记录
    List<AccessControlSpotAccessRecord> findByAccessControlSpotId(String id);


    @Query(value = "Select id from  iot_device_access_record  where data_time > ?1 and data_time <?2  order by data_time", nativeQuery = true)
    List<String> findByDataTimeSql(String startTime,String  endTime);



    @Query(value = "SELECT  spotrecord.id FROM  iot_access_control_spot_access_record spotrecord  WHERE "+
            " spotrecord.access_control_spot_id IN " +
            "( SELECT iacs.id FROM iot_access_control_spot iacs RIGHT JOIN iot_space ioss ON ioss.id = iacs.space_id WHERE ioss.project_id = ?3 )"+
            " AND spotrecord.device_access_record_id IN (Select id from  iot_device_access_record  where data_time > ?1 and data_time <?2  and access_status=?4  order by data_time)", nativeQuery = true)
    List<String> findByDataTimeProjectNotNullSql(String startTime,String  endTime,String projectId,String type);


}
