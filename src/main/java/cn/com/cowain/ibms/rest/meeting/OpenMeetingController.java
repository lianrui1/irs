package cn.com.cowain.ibms.rest.meeting;

import cn.com.cowain.ibms.annotation.Idempotent;
import cn.com.cowain.ibms.rest.req.ReservationAddReq;
import cn.com.cowain.ibms.rest.req.ReservationUpdateReq;
import cn.com.cowain.ibms.rest.req.meeting.MeetingRoomSearchReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.RoomWithReservationResp;
import cn.com.cowain.ibms.service.ReservationRecordService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.utils.DateUtil;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * 会议室功能开放Controller
 *
 * @author Yang.Lee
 * @date 2022/4/6 15:14
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/open/meeting")
@Api(tags = {IConst.MODULE_OPEN_MEETING})
public class OpenMeetingController {

    @Resource
    private ReservationRecordService reservationRecordService;

    /**
     * 创建会议预约
     *
     * @param req           请求参数
     * @param bindingResult 参数校验对象
     * @return 创建结果，创建成功返回会议对应id
     * @author Yang.Lee
     * @date 2022/4/11 10:15
     **/
    @PostMapping
    @ApiOperation(value = "新增会议室预约记录", tags = {IConst.MODULE_OPEN_MEETING})
    @Idempotent(value = API_BASE + "/open/meeting/post", expireTime = 3L)
    public ResponseEntity<JsonResult<IdResp>> create(@RequestBody @Validated ReservationAddReq req,
                                                     BindingResult bindingResult) {

        log.info("新增会议室预约记录,param: {}", JSON.toJSONString(req));

        LocalDateTime formTime = req.getFrom();
        LocalDateTime nowTime = LocalDateTime.now();
        if (!DateUtil.isTimeAvailable(formTime, nowTime)) {
            return ResponseEntity.ok(JsonResult.error("会议开始时间不能早于当前时间半小时", null, ErrConst.E01));
        }

        //校验预订时间 date
        LocalDate date = req.getDate();
        LocalDate today = LocalDate.now();
        if (date.isBefore(today)) {
            //返回错误信息
            return ResponseEntity.ok(JsonResult.error("会议不能选择过去的日期", null, ErrConst.E01));
        }
        //检查该会议室,在相同时间段是否存在预约记录
        ServiceResult serviceResult = reservationRecordService.checkReservation(req);
        if (!serviceResult.isSuccess()) {
            //返回错误信息
            return ResponseEntity.ok(JsonResult.error("该会议室已存在预约记录", null, ErrConst.E12));
        }

        // 保存
        ServiceResult result = reservationRecordService.createMeeting(req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error((String) result.getObject(), null, result.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.ok((IdResp) result.getObject()));
    }


    /**
     * 更新会议室预约记录
     *
     * @param req
     * @param bindingResult
     * @return
     * @author 张宇鑫
     */
    @PutMapping("/{id}")
    @ApiOperation(value = "更新会议室预约记录", tags = {IConst.MODULE_OPEN_MEETING})
    public ResponseEntity<JsonResult<String>> update(@PathVariable String id, @RequestHeader(name = "token") String token, @RequestBody @Validated ReservationUpdateReq req,
                                                     BindingResult bindingResult) {

        log.info("修改会议室预约记录,param: {}", JSON.toJSONString(req));

        ServiceResult serviceResult = reservationRecordService.updateMeeting(id, req);
        JsonResult<String> jsonResult = JsonResult.okNoData("OK");
        if (!serviceResult.isSuccess()) {
            jsonResult = JsonResult.error(serviceResult.getObject().toString(), null, ErrConst.E17);
        }
        return ResponseEntity.ok(jsonResult);
    }


    /**
     * 取消会议预约记录(发起人操作)
     *
     * @param id 会议ID
     * @return
     */
    @PutMapping("/cancel/{id}")
    @ApiOperation(value = "取消会议预约记录(发起人操作)", tags = IConst.MODULE_OPEN_MEETING)
    public ResponseEntity<JsonResult<String>> cancel(@PathVariable String id) {

        ServiceResult serviceResult = reservationRecordService.cancelMeeting(id);
        JsonResult<String> jsonResult = JsonResult.ok("OK", "取消成功");
        if (!serviceResult.isSuccess()) {
            jsonResult = JsonResult.error(serviceResult.getObject().toString(), null, ErrConst.E16);
        }
        return ResponseEntity.ok(jsonResult);
    }

    @GetMapping("/room/list")
    @ApiOperation(value = "查询会议室列表", tags = IConst.MODULE_OPEN_MEETING)
    public ResponseEntity<JsonResult<List<RoomWithReservationResp>>> getReservationRecordList(MeetingRoomSearchReq req){

        log.info("查询会议室列表,param: {}", JSON.toJSONString(req));

        return ResponseEntity.ok(JsonResult.ok(reservationRecordService.getEmptyRoom(req)));
    }
}
