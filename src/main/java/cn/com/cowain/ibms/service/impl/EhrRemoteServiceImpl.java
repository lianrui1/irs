package cn.com.cowain.ibms.service.impl;

import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.exceptions.NullCheckException;
import cn.com.cowain.ibms.exceptions.RemoteServiceUnavailableException;
import cn.com.cowain.ibms.rest.resp.*;
import cn.com.cowain.ibms.service.EhrRemoteService;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/4 17:04
 */
@Slf4j
@Service
public class EhrRemoteServiceImpl implements EhrRemoteService {

    private static final String PATH_PREFIX = "path:";

    private RestTemplate restTemplate;

    public EhrRemoteServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Value("${cowain.ehr.url.orgTreeLevelPath}")
    private String orgTreeLevelPath ;

    @Value("${cowain.ehr.url.staffCheckPath}")
    private String staffCheckPath ;

    /**
     * EHR 服务
     */
    @Value("${cowain.ehr.server}")
    private String ehrServer;

    private String base;

    /**
     * restTemplate 请求头
     */
    private LinkedMultiValueMap<String, String> headers;

    private static final String EHR_STAFF = "ehrStaff";

    private static final String NAME_ZH = "nameZh";

    private static final String NAME_EN = "nameEn";

    @PostConstruct
    void initBase() {
        //初始化访问路径
        this.base = "http://" + ehrServer;
        log.debug("EHR " + base);
        //初始化header
        headers = new LinkedMultiValueMap<>();
    }

    @Override
    public EhrDeptResp getDepartmentTree() {
        HttpEntity<String> request = new HttpEntity<>(headers);
        ResponseEntity<EhrDeptResp> responseEntity = restTemplate.exchange(this.base + orgTreeLevelPath, HttpMethod.GET, request, EhrDeptResp.class);

        return responseEntity.getBody();
    }

    @Override
    public Map<String, String> getUserDept(String[] jobNumber) {
        String path = "/ehr/external/staff/queryUser/" + StringUtils.join(jobNumber, ",");
        HttpEntity<String> request = new HttpEntity<>(headers);
        ResponseEntity<JSONObject> exchange = restTemplate.exchange(this.base + path, HttpMethod.GET, request, JSONObject.class);
        JSONObject body = exchange.getBody();
        Map<String, String> map = new HashMap<>();
        if (null != body) {
            String[] data = body.get("data").toString().
                    replace("[", "").replace("]", "").split(",");

            for (String datum : data) {
                List<String> list = Arrays.asList(datum.split("/"));
                map.put(list.get(list.size() - 1), datum);
            }
        }
        return map;
    }

    /**
     * 根据员工工号查询用户信息
     *
     * @param hrId 员工工号
     * @return 员工数据
     */
    @Override
    public SysUser getUserByHrId(@NonNull  String hrId) {
        String path = "/ehr/external/staff/getStaffByHrId/" + hrId;

        HttpEntity<String> request = new HttpEntity<>(headers);
        ResponseEntity<JSONObject> responseEntity = restTemplate.exchange(this.base + path, HttpMethod.GET, request, JSONObject.class);
        JSONObject body = responseEntity.getBody();

        if(body == null){
            log.error("通过工号获取ehr用户信息失败，查询不到数据。工号：" + hrId);
            return null;
        }

        JSONObject ehrStaffBody = body.getJSONObject(EHR_STAFF);
        if(ehrStaffBody == null){
            log.error("通过工号获取ehr用户信息失败，查询不到数据。工号：" + hrId);
            return  null;
        }

        SysUser sysUser = new SysUser();

        sysUser.setSysId(String.valueOf(ehrStaffBody.getInteger("stId")));
        sysUser.setEmpNo(hrId);
        sysUser.setWxOpenId(ehrStaffBody.getString("wxId"));
        sysUser.setNameZh(ehrStaffBody.getString(NAME_ZH));
        sysUser.setNameEn(ehrStaffBody.getString(NAME_EN));

        return sysUser;
    }


    @Override
    public EhrStaffRespPage getUserByDeptId(String deptId) {
        String path = "/ehr/external/staff/list?workState=B,A&departmentList=" + deptId + "&stType=CWA&limit=2000";
        HttpEntity<String> request = new HttpEntity<>(headers);
        ResponseEntity<EhrStaffRespRoot> responseEntity = restTemplate.exchange(this.base + path, HttpMethod.GET, request, EhrStaffRespRoot.class);
        EhrStaffRespRoot ehrStaffRespRoot = responseEntity.getBody();
        if (null != ehrStaffRespRoot) {
            List<EhrStaffResp> list = ehrStaffRespRoot.getPage().getList();
            if(!CollectionUtils.isEmpty(list)){
                List<String> collect = list.stream().map(EhrStaffResp::getHrId).collect(Collectors.toList());
                Map<String, String> userDept = getUserDept(collect.toArray(new String[collect.size()]));
                for (EhrStaffResp staffResp : list) {
                    String hrId = userDept.get(staffResp.getHrId());
                    if (StringUtils.isNotEmpty(hrId)) {
                        staffResp.setStDeptName(hrId);
                    }
                }
            }
            return ehrStaffRespRoot.getPage();
        } else {
            throw new RemoteServiceUnavailableException("EHR 根据部门ID查询员工失败");
        }
    }

    @Override
    public EhrStaffRespRoot findSysUserByNameContains(String name) {
        String path = "/ehr/external/staff/list?workState=B,A&key=" + name + "&stType=CWA&limit=2000";
        HttpEntity<String> request = new HttpEntity<>(headers);
        ResponseEntity<EhrStaffRespRoot> responseEntity = restTemplate.exchange(this.base + path, HttpMethod.GET, request, EhrStaffRespRoot.class);
        EhrStaffRespRoot ehrStaffRespRoot = responseEntity.getBody();
        log.debug("ehrStaffRespRoot:" + ehrStaffRespRoot);
        if (null != ehrStaffRespRoot) {
            List<EhrStaffResp> list = ehrStaffRespRoot.getPage().getList();
            if(!CollectionUtils.isEmpty(list)){
                List<String> collect = list.stream().map(EhrStaffResp::getHrId).collect(Collectors.toList());
                Map<String, String> userDept = getUserDept(collect.toArray(new String[collect.size()]));
                for (EhrStaffResp staffResp : list) {
                    String hrId = userDept.get(staffResp.getHrId());
                    if (StringUtils.isNotEmpty(hrId)) {
                        staffResp.setStDeptName(hrId);
                    }
                }
            }
            return ehrStaffRespRoot;
        } else {
            throw new RemoteServiceUnavailableException("EHR 根据部门ID查询员工失败");
        }

    }


    @Override
    public SysUser getInfoById(String sysId) {
        log.debug("getInfoById, sysId:" + sysId);
        String path = "/ehr/external/staff/info/" + sysId;
        log.debug(PATH_PREFIX + path);
        HttpEntity<String> request = new HttpEntity<>(headers);
        ResponseEntity<JSONObject> responseEntity = restTemplate.exchange(this.base + path, HttpMethod.GET, request, JSONObject.class);
        JSONObject body = responseEntity.getBody();
        Assert.notNull(body, "EHR 服务确保在线");
        Map<String, Object> map = (Map<String, Object>) body.get(EHR_STAFF);
        if (map == null) {
            log.error("员工信息查询为空 sysId:" + sysId);
            throw new RemoteServiceUnavailableException("员工信息查询为空!");
        }
        String nameZh = (String) map.get(NAME_ZH);
        String nameEn = (String) map.get(NAME_EN);
        String hrId = (String) map.get("hrId");
        String wxId = (String) map.get("wxId");
        Integer stDept = (Integer) map.get("stDept");
        SysUser sysUser = new SysUser();
        Map<String, Object> ehrStaffJobRecordEntity = (Map) map.get("ehrStaffJobRecordEntity");
        if (null == ehrStaffJobRecordEntity) {
            log.error("员工部门信息为空 deptName:" + sysId);
            throw new NullCheckException(nameZh + "员工部门信息为空!");
        }
        sysUser.setDeptName(String.valueOf(ehrStaffJobRecordEntity.get("deptName")));
        sysUser.setJobRankName(String.valueOf(ehrStaffJobRecordEntity.get("joRankName")));
        sysUser.setSysId(sysId);
        sysUser.setEmpNo(hrId);
        sysUser.setDeptId(stDept + "");
        sysUser.setWxOpenId(wxId);
        sysUser.setNameZh(nameZh);
        sysUser.setNameEn(nameEn);

        return sysUser;
    }

    @Override
    public SysUser getInfoByUserId(String userId) {
        log.debug("getInfoById, userId:" + userId);
        String path = "/ehr/external/staff/infoByUserId/" + userId;
        log.debug(PATH_PREFIX + path);
        HttpEntity<String> request = new HttpEntity<>(headers);
        ResponseEntity<JSONObject> responseEntity = restTemplate.exchange(this.base + path, HttpMethod.GET, request, JSONObject.class);
        JSONObject body = responseEntity.getBody();
        Assert.notNull(body, "EHR 服务确保在线");
        Map<String, Object> map = (Map<String, Object>) body.get(EHR_STAFF);
        if (map == null) {
            log.error("员工信息查询为空 userId:" + userId);
            throw new RemoteServiceUnavailableException("员工信息查询为空!");
        }
        String nameZh = (String) map.get(NAME_ZH);
        String nameEn = (String) map.get(NAME_EN);
        String hrId = (String) map.get("hrId");
        String wxId = (String) map.get("wxId");
        int stIdInt = (int) map.get("stId");
        String stId = String.valueOf(stIdInt);
        Integer stDept = (Integer) map.get("stDept");
        Map<String, Object> ehrStaffJobRecordEntity = (Map) map.get("ehrStaffJobRecordEntity");
        String deptName = (String) ehrStaffJobRecordEntity.get("deptName");
        String joRankName = (String) ehrStaffJobRecordEntity.get("joRankName");
        SysUser sysUser = new SysUser();
        sysUser.setSysId(stId);
        sysUser.setEmpNo(hrId);
        sysUser.setDeptId(stDept + "");
        sysUser.setWxOpenId(wxId);
        sysUser.setNameZh(nameZh);
        sysUser.setNameEn(nameEn);
        sysUser.setDeptName(deptName);
        sysUser.setJobRankName(joRankName);
        return sysUser;
    }

    @Override
    public List<EhrStaffResp> findByEmpNo(List<String> empNoList) {
        log.debug("findByEmpNo, empNoList:" + empNoList);
        String empNoStr = empNoList.toString().replace("[", "").replace("]", "");
        String path = "/ehr/external/staff/queryStaffWxId?hrIds=" + empNoStr;
        log.debug(PATH_PREFIX + path);
        HttpEntity<String> request = new HttpEntity<>(headers);
        ResponseEntity<EhrStaffRespRoot2> responseEntity = restTemplate.exchange(this.base + path, HttpMethod.GET, request, EhrStaffRespRoot2.class);
        EhrStaffRespRoot2 root2 = responseEntity.getBody();
        if (root2 != null) {
            List<EhrStaffResp> list = root2.getList();
            Assert.isTrue(list.size() == empNoList.size(), "结果数量应该与查询参数 数量一致");
            for (EhrStaffResp ehrStaff : list) {
                Assert.isTrue(StringUtils.isNotBlank(ehrStaff.getWxId()), "微信openId不能空!");
            }
            return list;
        } else {
            throw new RemoteServiceUnavailableException("根据工号查询员工失败!");
        }
    }

    @Override
    public boolean serviceCheck() {
        log.debug("serviceCheck");
        HttpEntity<String> request = new HttpEntity<>(headers);
        try {
            ResponseEntity<JSONObject> responseEntity = restTemplate.exchange(this.base + staffCheckPath, HttpMethod.GET, request, JSONObject.class);
            JSONObject body = responseEntity.getBody();
            Assert.notNull(body, "返回数据不为空");
            //todo: 深入判断
        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public JSONObject getAuth2UserInfo(String code) {
        log.debug("getOAuth2UserInfo");
        log.debug("code:" + code);
        String path = "/ehr/wechatpass/getOAuth2UserInfo?code=" + code;
        HttpEntity<String> request = new HttpEntity<>(headers);
        ResponseEntity<JSONObject> responseEntity = restTemplate.exchange(this.base + path, HttpMethod.GET, request, JSONObject.class);
        JSONObject body = responseEntity.getBody();
        Assert.notNull(body, "返回数据不为空");
        log.info("body:" + body.toString());
        return body;
    }

}
