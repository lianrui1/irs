package cn.com.cowain.ibms.rest.req.distinguish;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/27 9:13
 */
@Data
public class UserTimeReq {

    @ApiModelProperty(value = "设备ID")
    private String deviceId;

    @ApiModelProperty(value = "工号")
    private String jobNo;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "通行生效时间")
    private LocalDateTime startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "通行结束时间")
    private LocalDateTime endTime;
}
