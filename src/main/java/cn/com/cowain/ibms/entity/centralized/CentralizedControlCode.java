package cn.com.cowain.ibms.entity.centralized;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.enumeration.integration_control.ControlDeviceRange;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author wei.cheng
 * @date 2022/09/15 16:28
 */
@Data
@Entity
@Table(name = "ibms_centralized_control_code")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "ibms_centralized_control_code", comment = "集控码")
public class CentralizedControlCode extends BaseEntity {

    @Column(name = "name", nullable = false, columnDefinition = "varchar(20) COMMENT '集控码名称'")
    private String name;

    @Column(name = "project_id", nullable = false, columnDefinition = "varchar(36) COMMENT '关联项目id'")
    private String projectId;

    @Column(name = "space_id", nullable = false, columnDefinition = "varchar(36) COMMENT '关联空间id'")
    private String spaceId;

    @Column(name = "control_device_type", nullable = false, columnDefinition = "varchar(300) COMMENT '控制的设备类型，可多个。使用”,“分割'")
    private String controlDeviceType;

    @Enumerated(EnumType.STRING)
    @Column(name = "control_device_range",nullable = false, columnDefinition = "varchar(20) COMMENT '控制设备范围'")
    private ControlDeviceRange controlDeviceRange;

    @Column(name = "admin_hr_id", nullable = false, columnDefinition = "varchar(255) COMMENT '管理员工号'")
    private String adminHrId;

    @Column(name = "admin_name", nullable = false, columnDefinition = "varchar(100) COMMENT '管理员姓名'")
    private String adminName;
}
