package cn.com.cowain.ibms.rest.resp.space;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Yang.Lee
 * @date 2020/12/17 14:35
 */
@Data
@ApiModel("空间平面信息响应对象")
public class SpacePlaneResp {

    @ApiModelProperty(value = "主键ID", required = true)
    private String id;

    @ApiModelProperty(value = "位置", required = true, position = 1)
    private Integer position;

    @ApiModelProperty(value = "名称", required = true, position = 2)
    private String name;

    @ApiModelProperty(value = "图标列表", required = true, position = 3)
    private List<SpaceIconResp> iconResps;


    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "时间", position = 4)
    private LocalDateTime createdTime;
}
