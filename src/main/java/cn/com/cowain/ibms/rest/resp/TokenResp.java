package cn.com.cowain.ibms.rest.resp;

import lombok.Data;

import java.io.Serializable;

/**
 * @author feng
 * @title: TokenResp
 * @projectName bims
 * @Date 2021/12/8 15:55
 */
@Data
public class TokenResp implements Serializable {
    String token;
}
