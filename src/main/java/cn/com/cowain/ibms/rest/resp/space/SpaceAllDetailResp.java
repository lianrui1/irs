package cn.com.cowain.ibms.rest.resp.space;

import cn.com.cowain.ibms.rest.resp.StaffResp;
import cn.com.cowain.ibms.rest.resp.iot.IotDeviceResp;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 空间全信息返回对象
 *
 * @author Yang.Lee
 * @date 2021/11/12 14:59
 */
@Data
@ApiModel("空间详情（包含设备及人员数据）")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SpaceAllDetailResp {

    @ApiModelProperty(value = "空间详情", position = 1)
    private SpaceDetailResp detail;

    @ApiModelProperty(value = "空间内设备数量", position = 2)
    private long totalDevicesNumber;

    @ApiModelProperty(value = "空间内在线设备数量", position = 3)
    private long onlineDevicesNumber;

    @ApiModelProperty(value = "空间内离线设备数量", position = 4)
    private long offlineDevicesNumber;

    @ApiModelProperty(value = "空间内有权限操作的员工总数", position = 5)
    private long staffNumber;

    @ApiModelProperty(value = "设备在线率", position = 6)
    private double onlineRate;

    @ApiModelProperty(value = "设别离线率", position = 7)
    private double offlineRate;

    @ApiModelProperty(value = "设备列表", position = 8)
    private List<IotDeviceResp> deviceList;

    @ApiModelProperty(value = "空间内有权限操作的设备的员工列表", position = 9)
    private List<StaffResp> staffList;
}
