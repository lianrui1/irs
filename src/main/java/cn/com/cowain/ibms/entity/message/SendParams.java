package cn.com.cowain.ibms.entity.message;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/1/19 16:29
 */
@Data
public class SendParams {

    private String key;
    private String val;
    private String color;
}
