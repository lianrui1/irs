package cn.com.cowain.ibms.feign.ks_conn;

import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.resp.meal.DeleteHtAcrossPermissionConfigReq;
import cn.com.cowain.ibms.rest.resp.meal.HtAcrossPermissionConfigReq;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Yang.Lee
 * @date 2022/2/9 20:15
 */
@Service
@RequestMapping({"/ms-ks-conn/api/v1"})
@FeignClient(value = "ms-ks-conn", url = "${cowain.gateway.address}")
public interface KSConnApi {

    @GetMapping("/ht/device/open/door")
    JsonResult<String> open();

    /**
     * 报餐权限配置
     *
     * @param req
     * @return
     */
    @PostMapping("/ht/across/permission/permission/config")
    JsonResult<Object> configPermission(HtAcrossPermissionConfigReq req);

    /**
     * 删除权限组配置
     *
     * @param req
     * @return
     */
    @DeleteMapping("/ht/across/permission/permission/config")
    JsonResult<Object> deletePermission(DeleteHtAcrossPermissionConfigReq req);
}
