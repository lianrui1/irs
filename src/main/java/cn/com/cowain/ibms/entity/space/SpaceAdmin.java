package cn.com.cowain.ibms.entity.space;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.iot.Space;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author Yang.Lee
 * @date 2021/10/18 16:00
 */
@Data
@Entity
@Table(name = "space_admin",indexes = {@Index(columnList = "admin_empNo"),@Index(columnList = "space_id")})
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "space_admin", comment = "空间管理员表")
@Where(clause = "is_del=0")
public class SpaceAdmin extends BaseEntity {

    @Column(name = "admin_empNo", columnDefinition = "varchar(20) COMMENT '管理员工号'")
    private String adminEmpNo;

    @Column(name = "admin_Name", columnDefinition = "varchar(20) COMMENT '管理员姓名'")
    private String adminName;

    @Column(name = "admin_dept", columnDefinition = "varchar(20) COMMENT '管理员部门'")
    private String adminDept;

    @ManyToOne
    @JoinColumn(name = "space_id", nullable = false, columnDefinition = "varchar(36) COMMENT '空间id'")
    private Space space;
}
