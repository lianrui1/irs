package cn.com.cowain.ibms.rest.resp.wps;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wei.cheng
 * @date 2022/02/22 14:40
 */
@Data
@ApiModel("WPS获取文件元数据")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WpsGetFileInfoResp {

    @ApiModelProperty(value = "文件元数据", required = true, position = 1)
    private FileInfoResp file;

    @ApiModelProperty(value = "用户信息", required = true, position = 2)
    private UserResp user;
}
