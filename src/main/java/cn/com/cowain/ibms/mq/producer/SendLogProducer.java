package cn.com.cowain.ibms.mq.producer;

import cn.com.cowain.ibms.mq.bean.InterFaceLogMessage;
import lombok.extern.log4j.Log4j2;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Log4j2
@Component
public class SendLogProducer {
    /**
     * 引入rocketMq模板
     */
    @Autowired
    private RocketMQTemplate mqTemplate;
    @Value("${rocketmq.env}")
    private String env;


    /**
     * 发送Log消息
     */
    public void sendLogMsg(InterFaceLogMessage interFaceLogMessage) {
        if (Objects.isNull(interFaceLogMessage)) {
            log.error("下发参数为空");
            return;
        }
        try {
            String runLogTopic = env + "-iotc-to-tsdb-interface";
            mqTemplate.syncSend(runLogTopic, interFaceLogMessage);
            log.error(runLogTopic+"发送成功"+"消息内容："+interFaceLogMessage);
        } catch (MessagingException e) {
            log.error(e.getMessage());
        }
    }

}

