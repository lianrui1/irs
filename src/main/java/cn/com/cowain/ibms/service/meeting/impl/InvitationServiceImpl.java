package cn.com.cowain.ibms.service.meeting.impl;

import cn.com.cowain.ibms.feign.bean.oa.guest.InvitationReason;
import cn.com.cowain.ibms.feign.bean.oa.guest.InvitationReasonList;
import cn.com.cowain.ibms.service.meeting.InvitationService;
import cn.com.cowain.ibms.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/6/30 17:09
 */
@Slf4j
@Service
public class InvitationServiceImpl implements InvitationService {

    @Value("${ms-oa.url}")
    private String oaDomain;

    @Resource
    private RestTemplate restTemplate;

    @Resource
    private RedisUtil redisUtil;

    /**
     * 获取邀请事由列表
     *
     * @param token ehr短token
     * @param empNo 员工工号
     * @return 事由列表
     * @author Yang.Lee
     * @date 2021/6/30 17:08
     **/
    @Override
    public List<InvitationReason> getInvitationReason(String token, String empNo) {

        HttpHeaders header = new HttpHeaders();
        header.add("token", token);

        String redisKey = redisUtil.createRealKey("invitationReason");
        if(Boolean.TRUE.equals(redisUtil.hasKey(redisKey))){

            return (List<InvitationReason>)redisUtil.get(redisKey);
        }

        ResponseEntity<InvitationReasonList> responseEntity = restTemplate.exchange(oaDomain + "/oa/wechat/visitor/ehrcommon/getReasonList/" + empNo,
                HttpMethod.GET, new HttpEntity(header), InvitationReasonList.class);

        InvitationReasonList reasonList = responseEntity.getBody();
        List<InvitationReason> invitationReasonList = new ArrayList<>();
        if(reasonList != null){
            invitationReasonList = reasonList.getReasonList();
        }

        invitationReasonList.forEach(obj -> obj.setComment(InvitationReason.getDefaultComment(obj.getCode())));
        redisUtil.set(redisKey, invitationReasonList, RedisUtil.ONE_DAY_SECOND);

        return invitationReasonList;
    }
}
