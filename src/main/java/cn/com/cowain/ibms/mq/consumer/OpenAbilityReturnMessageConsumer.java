package cn.com.cowain.ibms.mq.consumer;

import cn.com.cowain.ibms.mq.Topic;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 开放能力返回数据消费者
 *
 * @author Yang.Lee
 * @date 2022/3/3 9:48
 */
@Slf4j
@RocketMQMessageListener(
        topic = "${rocketmq.env}-" + Topic.BIZ_TO_IOTC_RETURN,
        consumerGroup = "${rocketmq.env}-" + Topic.BIZ_TO_IOTC_RETURN
)
@Component
public class OpenAbilityReturnMessageConsumer implements RocketMQListener<String> {

//    @Resource
//    private AbilityService abilityService;

    @Override
    public void onMessage(@RequestBody String data) {

        log.debug("收到iotc反馈下发结果：{}", data);
//
//        try{
//            // 解析数据
//
//            BizReturnMessage message = JSON.parseObject(data, BizReturnMessage.class);
//            // 保存结果
//            abilityService.saveReturnMessage(message);
//
//        } catch (Exception e){
//            log.error(e.getMessage(), e);
//        }

    }
}
