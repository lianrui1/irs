package cn.com.cowain.ibms.rest.resp.device;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/5/17 10:23
 */
@Data
@Entity
@Table(name = "iot_circuit_breaker")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_circuit_breaker", comment = "iot断路器设备")
public class CircuitBreaker extends BaseEntity {

    /**
     * 所属设备
     */
    @ManyToOne
    @JoinColumn(name = "device_id", columnDefinition = "varchar(36) COMMENT 'FK'")
    private IotDevice iotDevice;

    /**
     * 总用电量
     */
    @Column(name = "electricity", columnDefinition = "double(9,2) COMMENT '总用电量'")
    private Double electricity;

    /**
     * 日用电量
     */
    @Column(name = "day_electricity", columnDefinition = "int(11) COMMENT '日用电量'")
    private Integer dayElectricity;

    /**
     * 设备密码
     */
    @Column(name = "date_time", updatable = false, columnDefinition = "datetime COMMENT '用电时间'")
    private LocalDate dateTime;
}
