package cn.com.cowain.ibms.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * JpaConfig
 *
 * @author Hu Jingling
 * @since 2020-08-12
 * @version 1.0
 */
@Configuration
@EnableJpaAuditing
public class JpaConfig {

}
