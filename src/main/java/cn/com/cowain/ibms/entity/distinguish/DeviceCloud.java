package cn.com.cowain.ibms.entity.distinguish;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/6 11:22
 */
@Data
@Entity
@Table(name = "ibms_device_cloud")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_device_cloud", comment = "云端设备表")
@Where(clause = "is_del=0")
public class DeviceCloud extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "iot_device_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 设备ID'")
    private IotDevice iotDevice;

    @Column(name = "cloud_device_id", nullable = false, columnDefinition = "varchar(100)  COMMENT '扫脸设备ID'")
    private String cloudDeviceId;

    @Column(name = "name", nullable = false, columnDefinition = "varchar(100)  COMMENT '设备名称'")
    private String name;

    @Column(name = "address", nullable = false, columnDefinition = "varchar(80)  COMMENT '设备地址'")
    private String address;

    @Column(name = "ks_device_id", nullable = false, columnDefinition = "varchar(36)  COMMENT '旷世云设备id'")
    private String ksDeviceId;
}
