package cn.com.cowain.ibms.feign.bean.uc;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2021/12/20 17:39
 */
@Data
public class UserBaseInfoResDto {
    private String userId;
    private String name;
    private String phone;
    private String mail;
    private String gender;
    @JsonProperty(value = "jobno")
    private String jobNo;

    // 头像
    private String img;

}
