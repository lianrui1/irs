package cn.com.cowain.ibms.utils.amap;

import cn.com.cowain.ibms.service.bean.amap.weather.WeatherPicture;
import cn.com.cowain.ibms.utils.FileUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

/**
 * 天气工具
 *
 * @author Yang.Lee
 * @date 2021/4/26 15:33
 */
@Slf4j
@Component
public class WeatherTools implements Serializable{

    /**
     * 天气图片Map, key值为高德天气api返回的天气现象
     **/
    private static Map<String, WeatherPicture> weatherPictureMap;

    /**
     * 初始化
     *
     * @author Yang.Lee
     * @date 2021/4/26 15:56
     **/
    @PostConstruct
    public void init() {
        if (weatherPictureMap == null) {
            createWeatherPictureMap();
        }
    }


    /**
     * 创建天气图片Map
     *
     * @author Yang.Lee
     * @date 2021/4/26 15:56
     **/
    private void createWeatherPictureMap() {

        try (InputStreamReader inputReader = new InputStreamReader(FileUtil.readFromResources("amap/weather-img.csv"), Charset.forName("UTF-8"));
             BufferedReader bf = new BufferedReader(inputReader)) {

            WeatherTools.weatherPictureMap = new HashMap<>();

            // 跳过文件第一行标题行
            bf.lines().skip(1).forEach(line -> {
                String[] message = line.split(",", -1);

                // 校验文件内容
                if (message.length < 6) {
                    log.error("天气图标文件内容异常，异常行内容：{}", line);
                    return;
                }

                WeatherPicture weatherPicture = new WeatherPicture();

                weatherPicture.setWeather(message[0]);
                weatherPicture.setBackgroundImg(message[2]);
                weatherPicture.setBackgroundImg2X(message[3]);
                weatherPicture.setIconWhite(message[4]);
                weatherPicture.setIconWhite2X(message[5]);

                WeatherTools.weatherPictureMap.put(message[0], weatherPicture);
            });

        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 获取天气现象对应的图片信息
     *
     * @param weather 天气现象
     * @return 图片信息
     * @author Yang.Lee
     * @date 2021/4/26 16:02
     **/
    @Nullable
    public static WeatherPicture getWeatherPicture(String weather) {

        return weatherPictureMap.get(weather);
    }
}
