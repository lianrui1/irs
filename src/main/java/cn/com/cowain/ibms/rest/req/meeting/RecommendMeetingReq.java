package cn.com.cowain.ibms.rest.req.meeting;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 快速预约会议请查询求参数
 *
 * @author: yanzy
 * @date: 2022/4/18 17:03
 */
@Data
@ApiModel("快速预约会议请求参数")
public class RecommendMeetingReq{


    @Length(max = 20, message = "会议主题不能超过20字符")
    @ApiModelProperty(value = "会议主题",required = true,position = 1)
    private String meetingTopic;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "会议预约时间",required = true,position = 2)
    private LocalDate date;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm" )
    @ApiModelProperty(value = "开始时间",required = true,position = 3)
    private LocalDateTime startTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm" )
    @ApiModelProperty(value = "会议结束时间",required = true,position = 4)
    private LocalDateTime endTime;

    @ApiModelProperty(value = "会议时长",example = "分钟 例:30、60、90、120",position = 5)
    private Integer  meetingDuration;

    @ApiModelProperty(value = "参会人数",position = 6)
    private Integer number;

    @ApiModelProperty(value = "项目id",position = 7)
    private String projectId;
}