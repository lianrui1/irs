package cn.com.cowain.ibms.enumeration;

/**
 * 联动规则生效方式
 *
 * @author: yanzy
 * @date: 2022/2/9 15:21
 */
public enum RuleEngineDetailEffectiveType {

    ALWAYS("一直"),

    TIMING("定时");

    RuleEngineDetailEffectiveType(String name){
        this.name = name;
    }

    private String name;

    public String getName(){
        return this.name;
    }
}
