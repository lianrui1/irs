package cn.com.cowain.ibms.service.office.impl;

import cn.com.cowain.ibms.bo.SimpleDevice;
import cn.com.cowain.ibms.bo.SimpleIntelligentOfficeDevice;
import cn.com.cowain.ibms.bo.SimpleIntelligentOfficeInitialization;
import cn.com.cowain.ibms.bo.SimpleIntelligentOfficeUser;
import cn.com.cowain.ibms.component.DeviceDictionaryComponent;
import cn.com.cowain.ibms.component.IntelligentOfficeRedis;
import cn.com.cowain.ibms.component.SpaceAdminRedis;
import cn.com.cowain.ibms.dao.device.DeviceModeStatusDao;
import cn.com.cowain.ibms.dao.device.DeviceWorkingModeDao;
import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.dao.iot.SpaceDao;
import cn.com.cowain.ibms.dao.office.*;
import cn.com.cowain.ibms.dao.space.SpaceAdminDao;
import cn.com.cowain.ibms.dao.space.SpaceDeviceControlDao;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.office.*;
import cn.com.cowain.ibms.entity.space.SpaceAdmin;
import cn.com.cowain.ibms.entity.space.SpaceDeviceControl;
import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.enumeration.iot.IntelligentOfficeStatus;
import cn.com.cowain.ibms.enumeration.iot.UsableStatus;
import cn.com.cowain.ibms.rest.req.office.DeviceCommonReq;
import cn.com.cowain.ibms.rest.req.office.DeviceNameEditReq;
import cn.com.cowain.ibms.rest.req.office.UserOfficeReq;
import cn.com.cowain.ibms.rest.resp.device.OfficeCommonDeviceResp;
import cn.com.cowain.ibms.rest.resp.device.TbDataResp;
import cn.com.cowain.ibms.rest.resp.office.OccupyOfficeResp;
import cn.com.cowain.ibms.rest.resp.office.OfficeDetailDeviceResp;
import cn.com.cowain.ibms.rest.resp.office.ShowDeviceResp;
import cn.com.cowain.ibms.rest.resp.office.TbDataDetailResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.device.HwProductsService;
import cn.com.cowain.ibms.service.office.*;
import cn.com.cowain.ibms.service.office.strategy.FunctionExecutor;
import cn.com.cowain.ibms.service.office.strategy.StrategyFactory;
import cn.com.cowain.ibms.service.space.IotDeviceService;
import cn.com.cowain.ibms.service.space.SpaceService;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.ibms.utils.RedisUtil;
import cn.com.cowain.ibms.utils.TokenUtils;
import cn.com.cowain.sfp.comm.utils.JwtUtil;
import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.google.common.base.Stopwatch;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author feng
 * @title: UserIntelligentOfficeServiceImpl
 * @projectName bims
 * @Date 2021/12/9 14:15
 */
@Slf4j
@Service
public class UserIntelligentOfficeServiceImpl implements UserIntelligentOfficeService {

    @Autowired
    private IntelligentOfficeUserGuideDao intelligentOfficeUserGuideDao;
    @Autowired
    private SpaceAdminDao spaceAdminDao;
    @Autowired
    private IntelligentOfficeInitializationDao intelligentOfficeInitializationDao;

    @Autowired
    private IntelligentOfficeDao intelligentOfficeDao;
    @Autowired
    private IntelligentOfficeDeviceDao intelligentOfficeDeviceDao;

    @Autowired
    private IntelligentOfficeUserDao intelligentOfficeUserDao;

    @Autowired
    private DeviceDao iotDeviceDao;

    @Autowired
    private DeviceWorkingModeDao deviceWorkingModeDao;
    @Autowired
    private DeviceModeStatusDao deviceModeStatusDao;

    @Autowired
    private HwProductsService productsService;
    @Autowired
    private IntelligentOfficeDeviceService intelligentOfficeDeviceService;

    @Autowired
    private SpaceDeviceControlDao spaceDeviceControlDao;

    @Resource
    private SpaceDao spaceDao;
    @Autowired
    RedisUtil redisUtil;

    @Autowired
    private SpaceCacheService spaceCacheService;
    @Autowired
    private SpaceService spaceService;
    @Autowired
    private IotDeviceService iotDeviceService;

    @Override

    public ServiceResult saveGuide() {
        IntelligentOfficeUserGuide guid = new IntelligentOfficeUserGuide();

        // 获取工号
        String token = TokenUtils.getToken();
        String hrId = JwtUtil.getHrId(token);
        Optional<IntelligentOfficeUserGuide> gui = intelligentOfficeUserGuideDao.findByUserHrId(hrId);
        if (gui.isPresent()) {
            return ServiceResult.ok();
        }
        guid.setUserHrId(hrId);
        intelligentOfficeUserGuideDao.save(guid);
        return ServiceResult.ok();
    }

    @Override
    public ServiceResult filterDevice(String officeId) {
        // 获取工号
        String token = TokenUtils.getToken();
        String hrId = JwtUtil.getHrId(token);
        Optional<IntelligentOffice> intelligentOffice = intelligentOfficeDao.findById(officeId);
        if (intelligentOffice.isEmpty()) {
            log.error("未查到该办公室");
            return ServiceResult.error("未查到该办公室");
        }

        //查询该空间下是否存在子空间
        List<Space> spaceList = spaceDao.findAllByIdOrParentId(intelligentOffice.get().getSpace().getId(), intelligentOffice.get().getSpace().getId());

        List<String> spaceIds = new ArrayList<>();
        //所有空间id
        spaceList.forEach(it -> spaceIds.add(it.getId()));

        //更新设备
        List<IotDevice> iotDevices = iotDeviceDao.findAllBySpaceIdIn(spaceIds);
        if (CollectionUtils.isNotEmpty(iotDevices)) {
            List<IntelligentOfficeDevice> devices = intelligentOfficeDeviceDao.findByIntelligentOfficeIdAndUserHrId(intelligentOffice.get().getId(), hrId);
            List<IotDevice> deviceList = devices.stream().map(IntelligentOfficeDevice::getDevice).collect(Collectors.toList());
            List<String> deviceIds = deviceList.stream().map(IotDevice::getId).collect(Collectors.toList());
            for (IotDevice iotDevice : iotDevices) {
                IntelligentOfficeDevice officeDevice = new IntelligentOfficeDevice();
                officeDevice.setDevice(iotDevice);
                officeDevice.setIntelligentOffice(intelligentOffice.get());
                officeDevice.setUsableStatus(UsableStatus.ENABLE);
                officeDevice.setUserHrId(hrId);
                if (!deviceIds.contains(iotDevice.getId())) {
                    intelligentOfficeDeviceDao.save(officeDevice);
                }
            }
        }
        List<ShowDeviceResp> listresp = new ArrayList<>();
        List<IotDevice> controlList = iotDeviceDao.findBySpaceIdInAndSpaceControl(spaceIds, 1);
        if (org.apache.commons.collections4.CollectionUtils.isEmpty(controlList)) {
            return ServiceResult.ok(listresp);
        }

        for (IotDevice deviceUser : controlList) {

            Optional<IntelligentOfficeDevice> officeDeviceOptional = intelligentOfficeDeviceDao.findByIntelligentOfficeIdAndDeviceIdAndUserHrId(intelligentOffice.get().getId(), deviceUser.getId(), hrId);
            if (officeDeviceOptional.isPresent() && UsableStatus.ENABLE.equals(officeDeviceOptional.get().getUsableStatus())) {
                IntelligentOfficeDevice officeDevice = officeDeviceOptional.get();
                ShowDeviceResp deviceResp = new ShowDeviceResp();
                deviceResp.setDeviceName(deviceUser.getDeviceName());
                deviceResp.setDeviceType(deviceUser.getDeviceType());
                deviceResp.setDeviceId(deviceUser.getId());
                deviceResp.setDeviceSN(deviceUser.getSn());
                deviceResp.setDeviceTypeName(deviceUser.getDeviceType().name());
                deviceResp.setHwDeviceId(deviceUser.getHwDeviceId());
                deviceResp.setHwStatus(deviceUser.getHwStatus());
                if (StringUtils.isNotEmpty(officeDevice.getDeviceAlisa())) {
                    deviceResp.setDeviceName(officeDevice.getDeviceAlisa());
                }
                if (StringUtils.isNotEmpty(officeDevice.getDeviceAlisaProperties())) {
                    deviceResp.setAdditionalName(StringUtils.isEmpty(officeDevice.getDeviceAlisaProperties()) ? DeviceNameEditReq.AdditionalName.empty()
                            : JSON.parseObject(officeDevice.getDeviceAlisaProperties(), DeviceNameEditReq.AdditionalName.class));
                }
                deviceResp.setUsableStatusName(officeDevice.getUsableStatus().getName());
                listresp.add(deviceResp);
            }

        }
        return ServiceResult.ok(listresp);
    }

    @Override
    public ServiceResult findNewDevice(String officeId) {
        IntelligentOfficeRedis office = officeRedisService.getIntelligentOfficeRedis(officeId);
        if (Objects.isNull(office)) {
            log.error("未查到该办公室");
            return ServiceResult.error("未查到该办公室");
        }
        return ServiceResult.ok(findNewDevice(officeId, office));
    }

    /**
     * 查询会议室设备列表
     *
     * @param officeId
     * @param office
     * @return
     */
    private List<ShowDeviceResp> findNewDevice(String officeId, IntelligentOfficeRedis office) {
        // 获取工号
        String token = TokenUtils.getToken();
        String hrId = JwtUtil.getHrId(token);
        String name = JwtUtil.getRealName(token);

        //更新设备
        String spaceId = office.getSpace();
        List<String> spaceIds = spaceService.getChildIdListByParentIdFromCache(spaceId);
        spaceIds.add(spaceId);
        List<SimpleDevice> simpleDeviceList = iotDeviceDao.findIdBySpaceIdIn(spaceIds);
        if(CollUtil.isNotEmpty(simpleDeviceList)){
            List<String> allDeviceIds = simpleDeviceList.stream().map(SimpleDevice::getId).collect(Collectors.toList());
            List<String> existedOfficeDeviceIds = intelligentOfficeDeviceDao.findDeviceIdByIntelligentOfficeIdAndDeviceIdsAndUserHrId(officeId, allDeviceIds, hrId);
            List<String> spaceControlDeviceIds = simpleDeviceList.stream().filter(s->1 == s.getSpaceControl()).map(SimpleDevice::getId).collect(Collectors.toList());
            List<String> existedSpaceControlDeviceIds = new ArrayList<>();
            if(CollUtil.isNotEmpty(spaceControlDeviceIds)){
                existedSpaceControlDeviceIds.addAll(spaceDeviceControlDao.findDeviceIdByUserEmpNoAndDeviceIds(hrId, spaceControlDeviceIds));
            }
            for (SimpleDevice simpleDevice : simpleDeviceList) {
                IotDevice iotDevice = null;
                if (!existedOfficeDeviceIds.contains(simpleDevice.getId())) {
                    IntelligentOffice intelligentOffice = new IntelligentOffice();
                    intelligentOffice.setId(officeId);
                    iotDevice = iotDeviceService.getIotDeviceFirstFromRedis(simpleDevice.getId());
                    IntelligentOfficeDevice officeDevice = new IntelligentOfficeDevice();
                    officeDevice.setDevice(iotDevice);
                    officeDevice.setIntelligentOffice(intelligentOffice);
                    officeDevice.setUsableStatus(UsableStatus.ENABLE);
                    officeDevice.setUserHrId(hrId);
                    intelligentOfficeDeviceDao.save(officeDevice);
                }

                // 保存设备控制权限
                if (1 == simpleDevice.getSpaceControl() && !existedSpaceControlDeviceIds.contains(simpleDevice.getId())) {
                    if(Objects.isNull(iotDevice)){
                        iotDevice = iotDeviceService.getIotDeviceFirstFromRedis(simpleDevice.getId());
                    }
                    SpaceDeviceControl spaceDeviceControl = new SpaceDeviceControl();
                    spaceDeviceControl.setDevice(iotDevice);
                    spaceDeviceControl.setUserEmpNo(hrId);
                    spaceDeviceControl.setUserName(name);
                    spaceDeviceControlDao.save(spaceDeviceControl);
                }
            }
        }

        //获取办公室所有的设备
        List<SimpleIntelligentOfficeDevice> officeDevices = getDeviceListOfHrIdAndSpaceId(hrId, spaceIds, officeId);

        List<ShowDeviceResp> list = new ArrayList<>();
        List<ShowDeviceResp> disable = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(officeDevices)) {
            for (SimpleIntelligentOfficeDevice officeDevice : officeDevices) {
                if (null != officeDevice.getDeviceType() && officeDevice.getDeviceType().getIsOffice() == 1) {
                    ShowDeviceResp deviceResp = new ShowDeviceResp();
                    deviceResp.setUsableStatusName(officeDevice.getUsableStatus().name());
                    deviceResp.setDeviceName(StringUtils.isEmpty(officeDevice.getDeviceAlisa()) ? officeDevice.getDeviceName() : officeDevice.getDeviceAlisa());
                    deviceResp.setAdditionalName(StringUtils.isEmpty(officeDevice.getDeviceAlisaProperties()) ? DeviceNameEditReq.AdditionalName.empty() : JSON.parseObject(officeDevice.getDeviceAlisaProperties(), DeviceNameEditReq.AdditionalName.class));
                    deviceResp.setDeviceType(officeDevice.getDeviceType());
                    deviceResp.setDeviceId(officeDevice.getDeviceId());
                    deviceResp.setDeviceSN(officeDevice.getSn());
                    deviceResp.setDeviceTypeName(officeDevice.getDeviceType().name());
                    deviceResp.setHwDeviceId(officeDevice.getHwDeviceId());
                    deviceResp.setHwStatus(officeDevice.getHwStatus());
                    if (DeviceStatus.ONLINE.equals(officeDevice.getHwStatus()) &&
                            UsableStatus.ENABLE.equals(officeDevice.getUsableStatus())) {
                        list.add(deviceResp);
                    } else {
                        disable.add(deviceResp);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(disable)) {
                list.addAll(disable);
            }
        }
        return list;
    }

    /**
     * 查询会议室的设备
     *
     * @param hrId
     * @param spaceIds
     * @param intelligentOfficeId
     * @return
     */
    private List<SimpleIntelligentOfficeDevice> getDeviceListOfHrIdAndSpaceId(String hrId, List<String> spaceIds, String intelligentOfficeId) {
        List<SimpleIntelligentOfficeDevice> officeDeviceList = intelligentOfficeDeviceDao.findDeviceOfOffice(hrId, intelligentOfficeId);
        List<String> deviceIds = officeDeviceList.stream().map(SimpleIntelligentOfficeDevice::getDeviceId).collect(Collectors.toList());
        officeDeviceList.addAll(intelligentOfficeDeviceDao.findDeviceOfOffice(hrId, spaceIds, deviceIds));
        officeDeviceList.sort(Comparator.comparing(SimpleIntelligentOfficeDevice::getCreatedTime).reversed());
        return officeDeviceList;
    }

    @Autowired
    DevicePropertiesService devicePropertiesService;

    @Autowired
    private IOfficeRedisService officeRedisService;

    @Override
    public ServiceResult showDevice(String officeId) {
        // 获取工号
        String token = TokenUtils.getToken();
        String hrId = JwtUtil.getHrId(token);
        IntelligentOfficeRedis office = officeRedisService.getIntelligentOfficeRedis(officeId);
        if(Objects.isNull(office)){
            log.error("未查到办公室信息！");
            return ServiceResult.error("未查到办公室信息");
        }
        OfficeDetailDeviceResp officeResp = new OfficeDetailDeviceResp();
        List<OfficeCommonDeviceResp> commonDeviceRespList = new ArrayList<>();
        Optional<SimpleIntelligentOfficeInitialization> intelligentOfficeInitialization = officeRedisService.getIOIFromRedis(hrId, officeId);

        if (intelligentOfficeInitialization.isPresent()) {
            OccupyOfficeResp nowOffice = new OccupyOfficeResp();
            nowOffice.setHasCreated(Boolean.TRUE);
            nowOffice.setOfficeId(officeId);
            nowOffice.setOfficeName(office.getName());
            nowOffice.setIntellgentOfficeId(intelligentOfficeInitialization.get().getIntelligentOffice());
            nowOffice.setAlias(intelligentOfficeInitialization.get().getAlisa());
            officeResp.setNowOffice(nowOffice);
        } else {
            SimpleIntelligentOfficeUser officeUser = intelligentOfficeUserDao.findSimpleByIntelligentOfficeIdAndUserHrId(officeId, hrId);

            if (Objects.nonNull(officeUser)) {
                Optional<SimpleIntelligentOfficeInitialization> intelligentOfficeInvite = officeRedisService.getIOIFromRedis(officeUser.getInvitedUserHrId(), officeId);

                OccupyOfficeResp nowOffice = new OccupyOfficeResp();
                nowOffice.setOfficeId(officeId);
                if (intelligentOfficeInvite.isPresent()) {
                    nowOffice.setOfficeName(intelligentOfficeInvite.get().getAlisa());
                } else {
                    nowOffice.setOfficeName(office.getName());
                }
                nowOffice.setHasCreated(Boolean.FALSE);
                officeResp.setNowOffice(nowOffice);
            } else {
                log.error("未查到办公室信息！");
                return ServiceResult.error("未查到办公室信息");
            }
        }

        Stopwatch stopwatch = Stopwatch.createStarted();
        //已经创建的办公室，查询对应的所有的设备状态情况，优先查出设备列表
        List<ShowDeviceResp> deviceRespList = this.findNewDevice(officeId, office);
        log.info("查询设备列表耗时：{}ms", stopwatch.elapsed(TimeUnit.MILLISECONDS));
        if (CollectionUtils.isNotEmpty(deviceRespList)) {
            for (ShowDeviceResp resp : deviceRespList) {
                if (resp.getUsableStatusName().equals(UsableStatus.ENABLE.name())) {
                    /*
                     * 使用策略模式处理不同设备类型的逻辑
                     * 传入设备类型的枚举值，FunctionExecutor通过策略工厂拿到相应的设备类型的对象，
                     * 然后去执行对应的功能模块
                     */
                    FunctionExecutor functionExecutor = new FunctionExecutor(StrategyFactory.getDeviceTypeStrategy(resp.getDeviceTypeName()));
                    functionExecutor.addCommonDevice(resp, commonDeviceRespList);

                }
            }
        }
        log.info("查询设备列表2耗时：{}ms", stopwatch.elapsed(TimeUnit.MILLISECONDS));
        officeResp.getOfficeCommonDeviceResp().addAll(commonDeviceRespList);

        return ServiceResult.ok(officeResp);

    }

    @Override
    public ServiceResult hideDevice(DeviceCommonReq req, String officeId) {
        // 获取工号
        String token = TokenUtils.getToken();
        String hrId = JwtUtil.getHrId(token);
        Optional<IntelligentOfficeInitialization> officeList = intelligentOfficeInitializationDao.findByUserHrIdAndIntelligentOfficeId(hrId, officeId);
        if (officeList.isEmpty()) {
            log.error("未找到初始化后的办公室信息");
            return ServiceResult.error("未找到初始化后的办公室信息");
        }
        //获取办公室所有的设备
        List<IntelligentOfficeDevice> officeDevices = intelligentOfficeDeviceService.getDeviceListOfHrIdAndSpaceId(hrId, officeList.get().getIntelligentOffice());
        if (CollectionUtils.isNotEmpty(officeDevices)) {
            Boolean hasDevice = Boolean.FALSE;
            List<IntelligentOfficeUser> users = intelligentOfficeUserDao.findByIntelligentOfficeIdAndInvitedUserHrId(officeId, hrId);
            for (IntelligentOfficeDevice inOffDevice : officeDevices) {
                if (inOffDevice.getDevice().getId().equals(req.getDeviceId()) && !req.getOpen()) {
                    //关闭
                    inOffDevice.setUsableStatus(UsableStatus.DISABLED);
                    intelligentOfficeDeviceDao.save(inOffDevice);
                    hasDevice = Boolean.TRUE;
                    if (CollectionUtils.isNotEmpty(users)) {
                        for (IntelligentOfficeUser officeUser : users) {
                            Optional<IntelligentOfficeDevice> device = intelligentOfficeDeviceDao.findByIntelligentOfficeIdAndDeviceIdAndUserHrId(officeId, inOffDevice.getDevice().getId(), officeUser.getUserHrId());
                            if (device.isPresent()) {
                                device.get().setUsableStatus(UsableStatus.DISABLED);
                                intelligentOfficeDeviceDao.save(device.get());
                            }
                        }
                    }
                } else if (inOffDevice.getDevice().getId().equals(req.getDeviceId()) && req.getOpen()) {
                    //打开
                    inOffDevice.setUsableStatus(UsableStatus.ENABLE);
                    intelligentOfficeDeviceDao.save(inOffDevice);
                    hasDevice = Boolean.TRUE;
                    if (CollectionUtils.isNotEmpty(users)) {
                        for (IntelligentOfficeUser officeUser : users) {
                            Optional<IntelligentOfficeDevice> device = intelligentOfficeDeviceDao.findByIntelligentOfficeIdAndDeviceIdAndUserHrId(officeId, inOffDevice.getDevice().getId(), officeUser.getUserHrId());
                            if (device.isPresent()) {
                                device.get().setUsableStatus(UsableStatus.ENABLE);
                                intelligentOfficeDeviceDao.save(device.get());
                            }
                        }
                    }
                }


            }


            if (!hasDevice) {
                log.error("当前办公室未找到此设备！" + "officeId：" + officeId + "device:" + req.getDeviceId());
                return ServiceResult.error("当前办公室未找到此设备！");
            }
        } else {
            log.error("当前办公室无设备：" + officeId);
            return ServiceResult.error("当前办公室无设备");
        }

        List<DeviceWorkingMode> deviceWorkingMode = deviceWorkingModeDao.findByIntelligentOfficeIdAndUserHrId(officeList.get().getIntelligentOffice().getId(), hrId);
        if (CollectionUtils.isEmpty(deviceWorkingMode)) {
            return ServiceResult.ok();
        }
        for (DeviceWorkingMode mode : deviceWorkingMode) {
            List<DeviceModeStatus> deviceModeStatuses = deviceModeStatusDao.findByModeId(mode.getId());
            if (CollectionUtils.isEmpty(deviceModeStatuses)) {
                return ServiceResult.ok();
            }
            for (DeviceModeStatus status : deviceModeStatuses) {
                if (status.getDevice().getId().equals(req.getDeviceId()) && !req.getOpen()) {
                    status.setIsDelete(1);
                    deviceModeStatusDao.save(status);
                }
            }
        }
        return ServiceResult.ok();
    }

    @Override
    public TbDataDetailResp sensor(String officeId) {
        // 获取工号
        String token = TokenUtils.getToken();
        String hrId = JwtUtil.getHrId(token);
        TbDataDetailResp tbDataDetailResp = new TbDataDetailResp();
        //获取当前办公室的所有设备信息，分批次查询数据
        List<IntelligentOfficeDevice> intelligentOfficeDeviceList = intelligentOfficeDeviceDao.findByIntelligentOfficeIdAndUserHrId(officeId, hrId);
        if (CollectionUtils.isNotEmpty(intelligentOfficeDeviceList)) {
            for (IntelligentOfficeDevice intelligentOfficeDevice : intelligentOfficeDeviceList) {
                if (intelligentOfficeDevice.getUsableStatus().equals(UsableStatus.ENABLE) && (intelligentOfficeDevice.getDevice().getDeviceType().equals(DeviceType.TEMPERATURE_AND_HUMIDITY_AND_ILLUMINATION_SENSOR)
                        || intelligentOfficeDevice.getDevice().getDeviceType().equals(DeviceType.PM_SENSOR)
                        || intelligentOfficeDevice.getDevice().getDeviceType().equals(DeviceType.NOISE_SENSOR))) {

                    String tbId = productsService.findTbId(intelligentOfficeDevice.getDevice().getHwDeviceId());
                    if (tbId == null) {
                        log.error("查询失败 设备id有误" + intelligentOfficeDevice.getDevice().getHwDeviceId());
                        break;
                    }
                    List<TbDataResp> ill = productsService.findNowData(tbId);
                    if (CollectionUtils.isNotEmpty(ill)) {
                        TbDataResp illData = ill.get(0);
                        tbDataDetailResp.putValue(illData);
                    }
                    tbDataDetailResp.setAllDisable(Boolean.TRUE);

                }
            }
            //补充空数据
            tbDataDetailResp.putNullValue();
        } else {
            tbDataDetailResp.setAllDisable(false);
        }
        return tbDataDetailResp;
    }

    @Override
    public Boolean powerScope(String officeId) {
        //查询 spaceAdmin 是否有数据
        Optional<IntelligentOffice> intelligentOffice = intelligentOfficeDao.findById(officeId);
        if (intelligentOffice.isEmpty()) {
            return false;
        }
        // 获取工号
        String token = TokenUtils.getToken();
        String hrId = JwtUtil.getHrId(token);
        Optional<SpaceAdmin> spaceAdmin = spaceAdminDao.findBySpaceIdAndAdminEmpNo(intelligentOffice.get().getSpace().getId(), hrId);
        if (spaceAdmin.isEmpty()) {
            return false;
        }
        return true;
    }

    @Autowired
    DeviceDictionaryComponent deviceDictionaryComponent;

    @Override
    public ServiceResult hasControlOffice() {
        StopWatch allStopWatch = new StopWatch();
        allStopWatch.start("hasControlOffice");
        // 获取工号
        String token = TokenUtils.getToken();
        String hrId = JwtUtil.getHrId(token);
        List<SpaceAdmin> spaceAdminList = getSpaceById(hrId);
        if (CollectionUtils.isEmpty(spaceAdminList)) {
            spaceAdminList = spaceAdminDao.findByAdminEmpNo(hrId);
            if (CollectionUtils.isNotEmpty(spaceAdminList)) {
                spaceAdminList.forEach(spaceAdmin -> {
                    spaceCacheService.setSpaceInAdminRedis(spaceAdmin);
                });
            }
        }
        List<Space> byDeviceIds = spaceAdminList.stream().map(SpaceAdmin::getSpace).collect(Collectors.toList());
        List<OccupyOfficeResp> result = new ArrayList<>();
        List<String> officeSpaceIds = new ArrayList<>();
        List<IntelligentOfficeInitialization> hasOffices = intelligentOfficeInitializationDao.findByUserHrId(hrId);
        if (CollectionUtils.isNotEmpty(hasOffices)) {
            List<IntelligentOffice> offices = hasOffices.stream().map(IntelligentOfficeInitialization::getIntelligentOffice).collect(Collectors.toList());
            List<Space> officeSpace = offices.stream().map(IntelligentOffice::getSpace).collect(Collectors.toList());
            officeSpaceIds = officeSpace.stream().map(Space::getId).collect(Collectors.toList());
        }
        log.info(allStopWatch.prettyPrint());
        if (CollectionUtils.isNotEmpty(spaceAdminList)) {
            for (Space space : byDeviceIds) {
                OccupyOfficeResp resp = new OccupyOfficeResp();
                Optional<IntelligentOffice> intelligentOffice = intelligentOfficeDao.findBySpaceId(space.getId());
                if (intelligentOffice.isPresent() && intelligentOffice.get().getStatus() == IntelligentOfficeStatus.ENABLE) {
                    resp.setOfficeId(intelligentOffice.get().getId());
                    if (CollectionUtils.isNotEmpty(officeSpaceIds) && officeSpaceIds.contains(space.getId())) {
                        resp.setSpaceId(space.getId());
                        resp.setSpaceName(space.getName());
                        //Optional<IntelligentOfficeInitialization> opt = intelligentOfficeInitializationDao.findByUserHrIdAndIntelligentOfficeId(hrId, intelligentOffice.get().getId());
                        Optional<SimpleIntelligentOfficeInitialization> opt=iOfficeRedisService.setIOIFromRedis(hrId,intelligentOffice.get().getId());
                        opt.ifPresent(intelligentOfficeInitialization -> resp.setAlias(intelligentOfficeInitialization.getAlisa()));
                        resp.setAddress(SpaceService.getFullSpaceName(space, space.getName()));
                        resp.setHasCreated(Boolean.TRUE);
                        result.add(resp);
                    } else {
                        resp.setSpaceId(space.getId());
                        resp.setSpaceName(space.getName());
                        resp.setAddress(SpaceService.getFullSpaceName(space, space.getName()));
                        resp.setHasCreated(Boolean.FALSE);
                        result.add(resp);
                    }
                }
            }
        }
        //查看当前用户二级权限里面有哪些办公室，如果有加入并
        List<IntelligentOfficeUser> secondOfficeList = intelligentOfficeUserDao.findByUserHrId(hrId);
        if (CollectionUtils.isNotEmpty(secondOfficeList)) {
            for (IntelligentOfficeUser second : secondOfficeList) {
                Optional<IntelligentOfficeInitialization> secondOffice = intelligentOfficeInitializationDao.findByUserHrIdAndIntelligentOfficeId(second.getInvitedUserHrId(), second.getIntelligentOffice().getId());
                if (secondOffice.isPresent() && secondOffice.get().getIntelligentOffice().getStatus() == IntelligentOfficeStatus.ENABLE) {
                    OccupyOfficeResp resp = new OccupyOfficeResp();
                    resp.setOfficeId(secondOffice.get().getIntelligentOffice().getId());
                    resp.setSpaceId(secondOffice.get().getIntelligentOffice().getSpace().getId());
                    resp.setSpaceName(secondOffice.get().getAlisa());
                    resp.setAddress(SpaceService.getFullSpaceName(secondOffice.get().getIntelligentOffice().getSpace(), secondOffice.get().getIntelligentOffice().getSpace().getName()));
                    resp.setAlias(secondOffice.get().getAlisa());
                    resp.setHasCreated(Boolean.TRUE);
                    resp.setIsSecondePower(Boolean.TRUE);
                    result.add(resp);
                }
            }
        }
        allStopWatch.stop();
        log.info(allStopWatch.prettyPrint());
        return ServiceResult.ok(result);

    }

    public List<SpaceAdmin> getSpaceById(String hrId) {
        String spaceAdminRealKey = redisUtil.createSpaceAdminRealKey(IConst.SPACE_ADMIN_MODULE, hrId, RedisUtil.Separator.COLON);
        List<SpaceAdminRedis> spaceAdminRedisList = redisUtil.lGet(spaceAdminRealKey, 0, -1, SpaceAdminRedis.class);
        List<SpaceAdmin> spaceAdminList = new ArrayList<>();
        spaceAdminRedisList.forEach(spaceAdminRedis -> {
            Space space;
            final String spaceKey = redisUtil.createSpaceAdminRealKey(IConst.SPACE_MODULE, spaceAdminRedis.getSpaceId(), RedisUtil.Separator.COLON);
            if (redisUtil.hasKey(spaceKey)){
                Object o = redisUtil.get(spaceKey);
                space = JSON.toJavaObject((JSON) o, Space.class);
                SpaceAdmin spaceAdmin = new SpaceAdmin();
                BeanUtils.copyProperties(spaceAdminRedis, spaceAdmin);
                spaceAdmin.setSpace(space);
                spaceAdminList.add(spaceAdmin);
            }

        });
        //List<Space> byDeviceIds = spaceAdminList.stream().map(SpaceAdmin::getSpace).collect(Collectors.toList());
        log.info("空间ID为：【{}】", spaceAdminList);
        return spaceAdminList;

    }

    @Autowired
    private IOfficeRedisService iOfficeRedisService;

    @Override
    public ServiceResult saveOffice(UserOfficeReq req) {
        if (StringUtils.isEmpty(req.getOfficeName())) {
            return ServiceResult.error("初始化的办公室名称不能为空，请填写完整！");
        }
        // 获取工号
        String token = TokenUtils.getToken();
        String hrId = JwtUtil.getHrId(token);
        String name = JwtUtil.getRealName(token);
        List<IntelligentOfficeInitialization> hasofficeList = intelligentOfficeInitializationDao.findByUserHrId(hrId);
        if (CollectionUtils.isNotEmpty(hasofficeList)) {
            for (IntelligentOfficeInitialization hasOffice : hasofficeList) {
                if (hasOffice.getIntelligentOffice().getId().equals(req.getOfficeId())) {
                    log.error("已经存在了，不能再次初始化");
                    return ServiceResult.error("已经存在了，不能再次初始化");
                }
            }
        }
        Optional<IntelligentOffice> intelligentOffice = intelligentOfficeDao.findById(req.getOfficeId());
        if (intelligentOffice.isEmpty()) {
            log.error("未查到该办公室");
            return ServiceResult.error("未查到该办公室");
        }
        List<SpaceAdmin> spaceAdminList = spaceAdminDao.findByAdminEmpNo(hrId);
        if (CollectionUtils.isNotEmpty(spaceAdminList)) {
            List<Space> officeSpace = spaceAdminList.stream().map(SpaceAdmin::getSpace).collect(Collectors.toList());
            List<String> officeSpaceIds = officeSpace.stream().map(Space::getId).collect(Collectors.toList());
            if (!officeSpaceIds.contains(intelligentOffice.get().getSpace().getId())) {
                log.error("无权限" + hrId + " officeId:" + req.getOfficeId());
                return ServiceResult.error("您无权限初始化该办公室，请联系管理员！");
            }
        } else {
            log.error("无权限：" + hrId + " officeId:" + req.getOfficeId());
            return ServiceResult.error("您无权限初始化该办公室，请联系管理员！");
        }
        IntelligentOfficeInitialization office = new IntelligentOfficeInitialization();
        office.setUserHrId(hrId);
        office.setUserName(name);
        office.setIntelligentOffice(intelligentOffice.get());
        office.setAlisa(req.getOfficeName());
        intelligentOfficeInitializationDao.save(office);
        iOfficeRedisService.setIOIFromRedis(hrId,office.getIntelligentOffice().getId());
        //查询该空间下所有子空间
        List<Space> spaceList = spaceDao.findAllByIdOrParentId(intelligentOffice.get().getSpace().getId(), intelligentOffice.get().getSpace().getId());

        List<String> spaceIds = new ArrayList<>();
        //所有空间id
        spaceList.forEach(it -> spaceIds.add(it.getId()));

        //该空间和子空间下所有设备
        List<IotDevice> iotDevices = iotDeviceDao.findBySpaceIdInAndStatus(spaceIds, 1);
        if (CollectionUtils.isNotEmpty(iotDevices)) {
            List<IntelligentOfficeDevice> devices = intelligentOfficeDeviceDao.findByIntelligentOfficeIdAndUserHrId(intelligentOffice.get().getId(), hrId);
            Map<String, IntelligentOfficeDevice> deviceIdToIntelligentOfficeDeviceMap = devices.stream().collect(Collectors.toMap(od -> od.getDevice().getId(),
                    d -> d));
            for (IotDevice iotDevice : iotDevices) {
                IntelligentOfficeDevice officeDevice = deviceIdToIntelligentOfficeDeviceMap.get(iotDevice.getId());
                if (Objects.isNull(officeDevice)) {
                    officeDevice = new IntelligentOfficeDevice();
                }
                officeDevice.setDevice(iotDevice);
                officeDevice.setIntelligentOffice(intelligentOffice.get());
                officeDevice.setUsableStatus(UsableStatus.ENABLE);
                officeDevice.setUserHrId(hrId);
                intelligentOfficeDeviceDao.save(officeDevice);

                //判断是否为可控设备
                if (iotDevice.getSpaceControl() == 1 && spaceDeviceControlDao.findByUserEmpNoAndDeviceId(hrId, iotDevice.getId()).isEmpty()) {
                    //将可控设备存入space_device_control 空间内设备控制权限表 中
                    SpaceDeviceControl spaceDeviceControl = new SpaceDeviceControl();
                    spaceDeviceControl.setDevice(iotDevice);
                    spaceDeviceControl.setUserEmpNo(hrId);
                    spaceDeviceControl.setUserName(name);
                    spaceDeviceControlDao.save(spaceDeviceControl);
                }
            }
        }
        return ServiceResult.ok();
    }

    @Override
    public OccupyOfficeResp findOneOffice(UserOfficeReq req) {
        Optional<IntelligentOfficeInitialization> hasFirstOffice = intelligentOfficeInitializationDao.findById(req.getIntellgentOfficeId());
        if (hasFirstOffice.isEmpty()) {
            log.error("未查到该办公室");
            return null;

        }
        Optional<IntelligentOffice> office = intelligentOfficeDao.findById(hasFirstOffice.get().getIntelligentOffice().getId());
        if (office.isEmpty()) {
            log.error("未查到该办公室");
            return null;
        }
        OccupyOfficeResp resp = new OccupyOfficeResp();
        resp.setOfficeId(office.get().getId());
        return resp;

    }

    /**
     * 　　* @title: 修改办公室名称
     * 　　* @description
     * 　　* @author jfsui
     * 　　* @date 2021/12/10 13:52
     */
    @Override
    public void editOffice(UserOfficeReq req) {
        // 获取工号
        String token = TokenUtils.getToken();
        String hrId = JwtUtil.getHrId(token);
        //如果存在就返回异常
        Optional<IntelligentOfficeInitialization> hasFirstOffice = intelligentOfficeInitializationDao.findByUserHrIdAndIntelligentOfficeId(hrId, req.getOfficeId());
        if (hasFirstOffice.isEmpty()) {
            log.error("未查到该办公室");
            return;
        }
        hasFirstOffice.get().setAlisa(req.getOfficeName());
        intelligentOfficeInitializationDao.save(hasFirstOffice.get());
        iOfficeRedisService.setIOIFromRedis(hasFirstOffice.get().getUserHrId(),hasFirstOffice.get().getIntelligentOffice().getId());
    }
}
