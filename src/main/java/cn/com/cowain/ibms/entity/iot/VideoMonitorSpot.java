package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.rest.resp.MonitorSitResp;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 视频监控点
 *
 * @author Yang.Lee
 * @date 2021/11/17 15:55
 */
@Data
@Entity
@Table(name = "iot_video_monitor_spot")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_video_monitor_spot", comment = "视频监控点表")
public class VideoMonitorSpot extends BaseEntity {
    public VideoMonitorSpot(MonitorSitResp sitResp){
        this.name=sitResp.getSiteName();
        this.address=sitResp.getLocation();
    }
    public VideoMonitorSpot(){}
    @Column(name = "name", nullable = false, columnDefinition = "varchar(30) COMMENT '视频监控点名称'")
    private String name;

    @Column(name = "address", nullable = false, columnDefinition = "varchar(100) COMMENT '具体位置'")
    private String address;

    @ManyToOne
    @JoinColumn(name = "space_id", nullable = false, columnDefinition = "varchar(36) COMMENT '视频监控点所属空间ID FK'")
    private Space space;

    @ManyToOne
    @JoinColumn(name = "device_id", nullable = false, columnDefinition = "varchar(36) COMMENT '视频监控点关联设备ID FK'")
    private IotDevice device;

}
