package cn.com.cowain.ibms.rest.req.iot;

import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/3/23 2:24
 */
@Data
@ApiModel("梯控按人员列表分页请求对象")
public class ElevatorSpotUserPageReq extends PageReq {

    @ApiModelProperty(value = "1员工/2访客")
    private Integer isStaff;

    @ApiModelProperty(value = "部门ID")
    private String deptId;

    @ApiModelProperty(value = "空间ID")
    private String spaceId;

    @ApiModelProperty(value = "姓名/工号")
    private String keyword;
}
