package cn.com.cowain.ibms.dao.distinguish;

import cn.com.cowain.ibms.entity.distinguish.TimePlan;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 时间计划DAO对象
 * @author Yang.Lee
 * @date 2021/3/10 10:26
 */
public interface TimePlanDao extends JpaRepository<TimePlan, String> {
}
