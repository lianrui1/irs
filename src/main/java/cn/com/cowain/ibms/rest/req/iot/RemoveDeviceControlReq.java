package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author Yang.Lee
 * @date 2021/11/25 14:01
 */
@Data
@ApiModel("移除设备控制权限")
public class RemoveDeviceControlReq {

    @NotBlank
    @ApiModelProperty(value = "用户工号", position = 1)
    private String userHrId;
}
