package cn.com.cowain.ibms.dao.ability;

import cn.com.cowain.ibms.entity.ability.OpenAbility;
import cn.com.cowain.ibms.enumeration.ability.Ability;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

/**
 * @author Yang.Lee
 * @date 2022/1/14 10:54
 */
public interface OpenAbilityDao extends JpaRepository<OpenAbility, String>, JpaSpecificationExecutor<OpenAbility> {

    /**
     * 查询开放能力
     *
     * @param ability 开放能力
     * @return 结果
     * @author Yang.Lee
     * @date 2022/2/14 10:08
     **/
    Optional<OpenAbility> findByAbility(Ability ability);
}
