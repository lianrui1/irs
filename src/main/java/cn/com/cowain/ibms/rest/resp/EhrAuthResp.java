package cn.com.cowain.ibms.rest.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import me.chanjar.weixin.mp.bean.result.WxMpUser;

import java.io.Serializable;

/**
 * EhrAuthResp
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/18/20
 */
@Data
@ApiModel("EHR授权数据")
public class EhrAuthResp implements Serializable {

    /**
     * token
     */
    private String accessToken;

    @ApiModelProperty("微信用户信息.")
    private WxMpUser wxMpUser;

}
