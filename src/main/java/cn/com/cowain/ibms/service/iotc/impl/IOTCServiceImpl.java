package cn.com.cowain.ibms.service.iotc.impl;

import cn.com.cowain.ibms.feign.iotc.IOTCApi;
import cn.com.cowain.ibms.feign.iotc.bean.CreateHTMemberGroupReq;
import cn.com.cowain.ibms.feign.iotc.bean.RemoveHTMemberFromGroupReq;
import cn.com.cowain.ibms.feign.iotc.enumeration.MethodCode;
import cn.com.cowain.ibms.feign.iotc.enumeration.ModuleCode;
import cn.com.cowain.ibms.mq.bean.AccessPermissions;
import cn.com.cowain.ibms.mq.producer.IOTCUserDeviceRelationProducer;
import cn.com.cowain.ibms.rest.req.iot.GroupBindingReq;
import cn.com.cowain.ibms.rest.req.iot.HtUserGroupAddPersonReq;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.iotc.IOTCService;
import cn.com.cowain.sfp.comm.JsonResult;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Yang.Lee
 * @date 2022/1/27 15:18
 */
@Service
@Slf4j
public class IOTCServiceImpl implements IOTCService {

    @Resource
    private IOTCUserDeviceRelationProducer iotcUserDeviceRelationProducer;

    @Resource
    private IOTCApi iotcApi;

    /**
     * 用户绑定设备
     *
     * @return 处理结果
     * @author Yang.Lee
     * @date 2022/1/27 15:16
     **/
    @Override
    public ServiceResult bindUserWithDevices(AccessPermissions message) {
        if (message.getMethodName() == null) {
            message.setMethodName(AccessPermissions.Method.BIND.getName());
        }
        return iotcUserDeviceRelationProducer.push(message);
    }

    /**
     * 添加用户
     *
     * @return 处理结果
     * @author Yang.Lee
     * @date 2022/1/27 15:17
     **/
    @Override
    public ServiceResult addUser(AccessPermissions message) {
        if (message.getMethodName() == null) {
            message.setMethodName(AccessPermissions.Method.ADD.getName());
        }
        return iotcUserDeviceRelationProducer.push(message);
    }

    /**
     * 添加用户
     *
     * @param message 消息内容
     * @return 处理结果
     * @author Yang.Lee
     * @date 2022/1/27 15:17
     **/
    @Override
    public ServiceResult updateFace(AccessPermissions message) {
        if (message.getMethodName() == null) {
            message.setMethodName(AccessPermissions.Method.UPDATE.getName());
        }
        return iotcUserDeviceRelationProducer.push(message);
    }

    /**
     * 用户解绑设备
     *
     * @return 处理结果
     * @author Yang.Lee
     * @date 2022/1/27 15:17
     **/
    @Override
    public ServiceResult unBindUserWithDevices(AccessPermissions message) {
        if (message.getMethodName() == null) {
            message.setMethodName(AccessPermissions.Method.UNBIND.getName());
        }
        return iotcUserDeviceRelationProducer.push(message);
    }

    /**
     * 删除用户
     *
     * @return 处理结果
     * @author Yang.Lee
     * @date 2022/1/27 15:18
     **/
    @Override
    public ServiceResult delUser(AccessPermissions message) {
        if (message.getMethodName() == null) {
            message.setMethodName(AccessPermissions.Method.DELETE.getName());
        }
        return iotcUserDeviceRelationProducer.push(message);
    }


    /**
     * 创建鸿图员工组
     *
     * @param req 员工组创建参数
     * @return 创建结果
     * @author Yang.Lee
     * @date 2022/2/14 13:31
     **/
    @Override
    public ServiceResult createStaffHTMemberGroup(CreateHTMemberGroupReq req) {

        GroupBindingReq param = GroupBindingReq.builder()
                .chainSite("KS")
                .methodCode(MethodCode.CREATE_GROUP.getName())
                .moduleCode(ModuleCode.KSHT.getName())
                .jsonArg(JSON.toJSONString(req))
                .channelType("2")
                .build();

        log.debug("创建鸿图员工组， {}", JSON.toJSONString(param));

        JsonResult<Object> resp = iotcApi.postToConn(param);

        log.debug("创建鸿图员工组结果：{}", resp);
        if (resp.getStatus() == 0) {
            return ServiceResult.error(resp.getMsg());
        }
        return ServiceResult.ok();
    }

    /**
     * 删除鸿图员工组
     *
     * @param groupId 员工组ID
     * @return 创建结果
     * @author Yang.Lee
     * @date 2022/2/14 13:31
     **/
    @Override
    public ServiceResult removeStaffHTMemberGroup(String groupId) {

        GroupBindingReq param = GroupBindingReq.builder()
                .chainSite("KS")
                .methodCode(MethodCode.REMOVE_GROUP.getName())
                .moduleCode(ModuleCode.KSHT.getName())
                .jsonArg(groupId)
                .channelType("2")
                .build();

        log.debug("删除鸿图员工组 {}", JSON.toJSONString(param));

        JsonResult<Object> resp = iotcApi.postToConn(param);
        log.debug("删除鸿图员工组结果：{}", resp);
        if (resp.getStatus() == 0) {

            return ServiceResult.error(resp.getMsg());
        }
        return ServiceResult.ok();
    }

    /**
     * 从旷视鸿图的人员组中移除用户
     *
     * @param groupId      组ID
     * @param userHrIdList 用户工号集合
     * @return 集合
     * @author Yang.Lee
     * @date 2022/2/17 14:31
     **/
    @Override
    public ServiceResult removeUserFromKSHTGroup(String groupId, List<String> userHrIdList) {

        RemoveHTMemberFromGroupReq req = RemoveHTMemberFromGroupReq.builder()
                .groupUuid(groupId)
                .personUuids(userHrIdList)
                .build();

        GroupBindingReq param = GroupBindingReq.builder()
                .chainSite("KS")
                .methodCode(MethodCode.REMOVE_USER_FROM_GROUP.getName())
                .moduleCode(ModuleCode.KSHT.getName())
                .jsonArg(JSON.toJSONString(req))
                .channelType("2")
                .build();

        log.debug("删除鸿图员工组中用户 {}", JSON.toJSONString(param));

        JsonResult<Object> resp = iotcApi.postToConn(param);
        log.debug("删除鸿图员工组中用户结果：{}", resp);
        if (resp.getStatus() == 0) {

            return ServiceResult.error(resp.getMsg());
        }
        return ServiceResult.ok();
    }

    @Override
    public ServiceResult addUserFromKSHTGroup(String groupId, List<String> userHrIdList) {

        HtUserGroupAddPersonReq req = HtUserGroupAddPersonReq.builder()
                .groupUuid(groupId)
                .personUuids(userHrIdList)
                .build();

        GroupBindingReq param = GroupBindingReq.builder()
                .chainSite("KS")
                .methodCode(MethodCode.BATCH_ADD_PERSON.getName())
                .moduleCode(ModuleCode.KSHT.getName())
                .jsonArg(JSON.toJSONString(req))
                .channelType("2")
                .build();
        log.debug("向iotc下发人员对象{}", JSON.toJSONString(param));

        JsonResult<Object> resp = iotcApi.postToConn(param);

        log.debug("人员向鸿图设备下发结果：{}", resp);
        if (resp.getStatus() == 0) {

            return ServiceResult.error(resp.getMsg());
        }
        return ServiceResult.ok();

    }

    /**
     *
     * @author: yanzy
     * @param: [jobNO]人员id
     * @data: 2022/4/2 11:04:12
     */
    @Override
    public ServiceResult getUserFromPlatform(String jobNO) {

        log.debug("查询设备平台人员");
        JsonResult<Object> result = iotcApi.getUserFromPlatform(jobNO);
        if (result.getStatus() == 0){
            log.error("查询设备平台人员失败结果{}",result);
            return ServiceResult.error(result.getMsg());
        }
        log.info("result{}",result);
        return ServiceResult.ok(result.getData());

    }
}
