package cn.com.cowain.ibms.rest.resp.iot;

import cn.com.cowain.ibms.bo.SimpleUserBO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Objects;

/**
 * @author wei.cheng
 * @date 2022/03/08 14:05
 */
@Data
@ApiModel("门禁点员工数据")
public class AccessControlUserResp {

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "工号")
    private String empNo;

    @ApiModelProperty(value = "部门")
    private String dept;

    @ApiModelProperty(value = "人脸照片，base64格式")
    private String faceImg;

    @ApiModelProperty(value = "人员类型，1代表内部员工 2代表劳务工")
    private Integer personType;

    public static AccessControlUserResp convert(SimpleUserBO user) {
        if (Objects.isNull(user)) {
            return new AccessControlUserResp();
        }
        AccessControlUserResp resp = new AccessControlUserResp();
        resp.setName(user.getName());
        resp.setEmpNo(user.getHrId());
        resp.setDept(user.getDeptName());
        return resp;
    }
}
