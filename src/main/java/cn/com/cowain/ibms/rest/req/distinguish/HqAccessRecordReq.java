package cn.com.cowain.ibms.rest.req.distinguish;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

/**
 * 通行记录分页请求参数
 *
 * @author Yang.Lee
 * @date 2021/3/12 10:57
 */
@Data
@ApiModel("通行记录请求")
public class HqAccessRecordReq {
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "查询开始时间", example = "2022-01-01")
    private LocalDate from;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "查询结束时间", example = "2022-01-01")
    private LocalDate to;
}
