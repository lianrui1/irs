package cn.com.cowain.ibms.feign.mqtt_connector;

import cn.com.cowain.sfp.comm.JsonResult;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * mqtt-connector 门磁服务feign API
 *
 * @author Yang.Lee
 * @date 2021/5/5 15:42
 */
@Service
public interface MagneticApi {

    /**
     * 开门
     *
     * @param macAddress 门磁mac地址
     * @return 开门结果
     * @author Yang.Lee
     * @date 2021/5/5 15:46
     **/
    @PostMapping("/{macAddress}/open")
    JsonResult<String> open(@PathVariable String macAddress);
}
