package cn.com.cowain.ibms.rest.resp.working_mode;

import cn.com.cowain.ibms.enumeration.iot.UsableStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/10/18 15:09
 */
@Data
@ApiModel("工作模式场景详情对象")
public class ShowRoomWorkingModeDetailResp {

    @ApiModelProperty(value = "场景名称id", required = true)
    private String id;

    @ApiModelProperty(value = "场景名称图标", required = true)
    private String imgUrl;

    @ApiModelProperty(value = "场景名称", required = true)
    private String name;

    @ApiModelProperty(value = "用途")
    private String purpose;

    @ApiModelProperty(value = "空间id", required = true)
    private String spaceId;

    @ApiModelProperty(value = "空间名称", required = true)
    private String spaceName;

    @ApiModelProperty(value = "办公室ID")
    private String officeId;

    @ApiModelProperty(value = "办公室名称")
    private String officeName;

    @ApiModelProperty(value = "工作模式场景所包含设备", required = true)
    private List<ShowRoomDeviceModeStatusDetailResp> detailResps;

    @ApiModelProperty(value = "是否默认 0 默认", required = true)
    private int isDefault;

    @ApiModelProperty(value = "是否启用 DISABLED禁用 ENABLE启用")
    private UsableStatus status;
    @ApiModelProperty(value = "是否是总控，0不是总控，1是总控")
    private Integer showroomCentralize;


}
