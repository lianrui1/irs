package cn.com.cowain.ibms.service.disthinguish.impl;

import cn.com.cowain.ibms.dao.distinguish.TimePlanDao;
import cn.com.cowain.ibms.entity.distinguish.TimePlan;
import cn.com.cowain.ibms.rest.resp.distinguish.TimePlanListResp;
import cn.com.cowain.ibms.service.bean.ServiceMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.disthinguish.TimePlanService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * 时间计划Service逻辑实现类
 *
 * @author Yang.Lee
 * @date 2021/3/11 13:36
 */
@Service
public class TimePlanImpl implements TimePlanService {

    @Resource
    private TimePlanDao timePlanDao;

    /**
     * 获取时间计划列表
     *
     * @return 时间计划列表
     * @author Yang.Lee
     * @date 2021/3/11 13:35
     **/
    @Override
    public List<TimePlanListResp> getList() {
        List<TimePlan> timePlanList = timePlanDao.findAll();

        // 对象转换
        return convert(timePlanList);
    }

    /**
     * 判断时间计划是否存在
     *
     * @param planId 时间计划ID,支持多个
     * @return 查询结果。若时间计划存在，则返回的ServiceResult对象中包含该时间计划数据
     * @author Yang.Lee
     * @date 2021/4/7 9:54
     **/
    @Override
    @Transactional
    public ServiceResult isPlanExist(@NotNull String planId) {

        Optional<TimePlan> timePlanOptional = timePlanDao.findById(planId);
        if (!timePlanOptional.isPresent()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_TIME_PLAN, planId));
        }

        return ServiceResult.ok(timePlanOptional.get());
    }

    /**
     * 对象转换
     *
     * @param timePlanList 待转对象
     * @return 转换目标对象
     * @author Yang.Lee
     * @date 2021/3/11 13:49
     **/
    private List<TimePlanListResp> convert(@NotNull List<TimePlan> timePlanList) {
        List<TimePlanListResp> result = new ArrayList<>();
        if (timePlanList.isEmpty()) {
            return result;
        }
        timePlanList.forEach(plan -> result.add(convert(plan)));

        return result;
    }

    /**
     * 对象转换
     *
     * @param timePlan 待转对象
     * @return 目标对象
     * @author Yang.Lee
     * @date 2021/3/11 13:46
     **/
    private TimePlanListResp convert(TimePlan timePlan) {

        TimePlanListResp result = new TimePlanListResp();
        result.setId(timePlan.getId());
        result.setName(timePlan.getName());

        return result;
    }
}
