package cn.com.cowain.ibms.service.disthinguish.impl;

import cn.com.cowain.ibms.bo.AuthTime;
import cn.com.cowain.ibms.dao.distinguish.AuthRecordDao;
import cn.com.cowain.ibms.entity.distinguish.AuthRecord;
import cn.com.cowain.ibms.enumeration.distinguish.AuthTimeType;
import cn.com.cowain.ibms.feign.hk_server_connector.nt.NtAccessRecordService;
import cn.com.cowain.ibms.feign.iotc.IOTCApi;
import cn.com.cowain.ibms.feign.iotc.bean.DeviceExecuteCommandReq;
import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.req.distinguish.AuthTimeDataReq;
import cn.com.cowain.ibms.rest.resp.distinguish.AuthTimeDataFailRep;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.disthinguish.AuthService;
import cn.com.cowain.ibms.utils.DateUtils;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.RedisUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * @author wei.cheng
 * @date 2022/03/23 15:15
 */
@Slf4j
@Service
public class AuthServiceImpl implements AuthService {
    @Resource
    private RedisUtil redisUtil;
    @Resource
    private NtAccessRecordService ntAccessRecordService;
    @Resource
    private AuthRecordDao authRecordDao;
    @Resource
    private IOTCApi iotcApi;
    @Value("${nt-schedule.open-json-args}")
    private String openJsonArgs;
    @Value("${nt-schedule.not-open-json-args}")
    private String notOpenJsonArgs;

    @Override
    public ServiceResult updateAuth(List<AuthTimeDataReq> authTimeDataList) {
        LocalDateTime now = LocalDateTime.now();
        log.info("接受到ehr南通排班或者外出申请请求，时间为：{}", DateUtils.formatLocalDateTime(now, DateUtils.PATTERN_DATETIME));
        JsonResult<List<AuthTimeDataFailRep>> result = ntAccessRecordService.updateAuth(authTimeDataList);
        if (!"0".equals(result.getCode())) {
            return ServiceResult.error("推送数据到海康异常：" + result.getMsg(), result.getCode());
        }
        updateAuthDataInRedis(authTimeDataList, now);
        // 记录本次请求数据
        AuthRecord authRecord = new AuthRecord();
        authRecord.setContent(JSON.toJSONString(authTimeDataList));
        authRecordDao.save(authRecord);
        return ServiceResult.ok(result.getData());
    }

    /**
     * 更新redis中南通排班信息
     *
     * @param authTimeDataList
     * @param now
     */
    private void updateAuthDataInRedis(List<AuthTimeDataReq> authTimeDataList, LocalDateTime now) {
        if (CollectionUtils.isEmpty(authTimeDataList)) {
            return;
        }
        // 工作时间段
        MultiValueMap<String, AuthTime> workDayTimeListMap = new LinkedMultiValueMap<>();
        // 休假时间段
        MultiValueMap<String, AuthTime> holidayTimeListMap = new LinkedMultiValueMap<>();
        // 获取有效的数据
        addValidValue(authTimeDataList, now, workDayTimeListMap, holidayTimeListMap);
        if (MapUtils.isNotEmpty(workDayTimeListMap)) {
            workDayTimeListMap.forEach((hrId, authTimeList) -> {
                String key = getWorkDayTimeKey(hrId);
                Object object = redisUtil.get(key);
                if (Objects.isNull(object)) {
                    redisUtil.set(key, AuthTime.toRedisValue(authTimeList, now));
                } else {
                    List<AuthTime> value = AuthTime.redisValueConvert(object);
                    value.addAll(authTimeList);
                    redisUtil.set(key, AuthTime.toRedisValue(value, now));
                }
            });
        }
        if (MapUtils.isNotEmpty(holidayTimeListMap)) {
            holidayTimeListMap.forEach((hrId, authTimeList) -> {
                String key = getHolidayTimeKey(hrId);
                Object object = redisUtil.get(key);
                if (Objects.isNull(object)) {
                    redisUtil.set(key, AuthTime.toRedisValue(authTimeList, now));
                } else {
                    List<AuthTime> value = AuthTime.redisValueConvert(object);
                    value.addAll(authTimeList);
                    redisUtil.set(key, AuthTime.toRedisValue(value, now));
                }
            });
        }
    }

    /**
     * 人员在缓存中排班计划内容的key值
     *
     * @param hrId
     * @return
     */
    private String getWorkDayTimeKey(String hrId) {
        return redisUtil.createRealKey("work-day-auth-time:" + hrId);
    }

    /**
     * 人员在缓存中外出申请内容的key值
     *
     * @param hrId
     * @return
     */
    private String getHolidayTimeKey(String hrId) {
        return redisUtil.createRealKey("holiday-auth-time:" + hrId);
    }

    /**
     * 添加有效的值
     *
     * @param authTimeDataList
     * @param now
     * @param workDayTimeListMap
     * @param holidayTimeListMap
     */
    private void addValidValue(List<AuthTimeDataReq> authTimeDataList, LocalDateTime now,
                               MultiValueMap<String, AuthTime> workDayTimeListMap,
                               MultiValueMap<String, AuthTime> holidayTimeListMap) {
        if (CollectionUtils.isEmpty(authTimeDataList)) {
            return;
        }
        for (AuthTimeDataReq req : authTimeDataList) {
            if (!AuthTimeType.WORK_DAY.getValue().equals(req.getType()) &&
                    !AuthTimeType.HOLIDAY.getValue().equals(req.getType())) {
                continue;
            }
            if (StringUtils.isEmpty(req.getJobNo())) {
                continue;
            }
            if (StringUtils.isEmpty(req.getStartTime())) {
                continue;
            }
            if (StringUtils.isEmpty(req.getEndTime())) {
                continue;
            }
            LocalDateTime startTime = getEhrDate(req.getStartTime());
            if (Objects.isNull(startTime)) {
                continue;
            }
            LocalDateTime endTime = getEhrDate(req.getEndTime());
            if (Objects.isNull(endTime)) {
                continue;
            }
            if (endTime.isBefore(startTime)) {
                continue;
            }
            if (endTime.isBefore(now)) {
                log.debug("结束时间小于当前时间的授权时间段已无效");
                continue;
            }
            if (AuthTimeType.WORK_DAY.getValue().equals(req.getType())) {
                workDayTimeListMap.add(req.getJobNo(), AuthTime.builder()
                        .startTime(startTime)
                        .endTime(endTime)
                        .build());
            } else {
                holidayTimeListMap.add(req.getJobNo(), AuthTime.builder()
                        .startTime(startTime)
                        .endTime(endTime)
                        .build());
            }
        }
    }

    /**
     * 时间格式转换
     *
     * @param date
     * @return
     */
    private LocalDateTime getEhrDate(String date) {
        try {
            if (StringUtils.isEmpty(date)) {
                return null;
            }
            String[] time = date.split("\\+");
            //格式化
            return DateUtils.parseLocalDateTime(time[0].replace("T", " "), DateUtils.PATTERN_DATETIME);
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        return null;
    }

    @Override
    public ServiceResult openKHSTDoor(String deviceSn, String hrId) {
        log.info("start to open ksht door, deviceSn:{}, hrId:{}", deviceSn, hrId);
        boolean isOpen = isOpen(hrId);
        if (isOpen) {
            DeviceExecuteCommandReq req = DeviceExecuteCommandReq.builder().deviceCode(deviceSn).jsonArg(openJsonArgs).build();
            cn.com.cowain.sfp.comm.JsonResult<Object> serviceResult = iotcApi.deviceExecuteCommand(req);
            log.info("设备执行指令为：{} \n结果为: {}", JSONObject.toJSONString(req), JSONObject.toJSONString(serviceResult));
            if (serviceResult.getStatus() != 1) {
                log.error("开门失败，req:{}, resp:{}", JSONObject.toJSONString(req), JSONObject.toJSONString(serviceResult));
                return ServiceResult.error("开门失败", ErrConst.E01);
            }
            return ServiceResult.ok("已开门");
        } else {
            DeviceExecuteCommandReq req = DeviceExecuteCommandReq.builder().deviceCode(deviceSn).jsonArg(notOpenJsonArgs).build();
            cn.com.cowain.sfp.comm.JsonResult<Object> serviceResult = iotcApi.deviceExecuteCommand(req);
            log.info("设备执行指令为：{} \n结果为: {}", JSONObject.toJSONString(req), JSONObject.toJSONString(serviceResult));
            if (serviceResult.getStatus() != 1) {
                log.error("播报失败，req:{}, resp:{}", JSONObject.toJSONString(req), JSONObject.toJSONString(serviceResult));
            }
            return ServiceResult.ok("排班中无法离开");
        }
    }

    /**
     * 判断用户在当前时间是否可以通过闸门
     *
     * @param hrId
     * @return
     */
    private boolean isOpen(String hrId) {
        LocalDateTime now = LocalDateTime.now();
        String holidayTimeKey = getHolidayTimeKey(hrId);
        if (redisUtil.hasKey(holidayTimeKey)) {
            List<AuthTime> authTimeList = AuthTime.redisValueConvert(redisUtil.get(holidayTimeKey));
            for (AuthTime authTime : authTimeList) {
                if (authTime.getStartTime().isBefore(now) && authTime.getEndTime().isAfter(now)) {
                    return true;
                }
            }
        }
        String workDayTimeKey = getWorkDayTimeKey(hrId);
        if (redisUtil.hasKey(workDayTimeKey)) {
            List<AuthTime> authTimeList = AuthTime.redisValueConvert(redisUtil.get(workDayTimeKey));
            for (AuthTime authTime : authTimeList) {
                if (authTime.getStartTime().isBefore(now) && authTime.getEndTime().isAfter(now)) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public ServiceResult recoveryAuth(String startTime) {
        log.info("start to recover auth, startTime:{}", startTime);
        LocalDateTime time;
        try {
            time = DateUtils.parseLocalDateTime(startTime, DateUtils.PATTERN_DATETIME);
        } catch (Exception e) {
            return ServiceResult.error("时间格式不正确，请确认格式为：`yyyy-MM-dd HH:mm:ss`");
        }
        List<AuthRecord> authRecordList = authRecordDao.findAllByCreatedTimeAfterOrderByCreatedTimeAsc(time);
        LocalDateTime now = LocalDateTime.now();
        authRecordList.forEach(authRecord -> {
            List<AuthTimeDataReq> authTimeDataReqList = JSON.parseArray(authRecord.getContent(), AuthTimeDataReq.class);
            updateAuthDataInRedis(authTimeDataReqList, now);
        });
        return ServiceResult.ok("恢复数据成功");
    }
}
