package cn.com.cowain.ibms.rest.resp.device;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/4 11:40
 */
@Data
public class AirConditionerResp {
    String id;
    String hwDeviceId;
    String hwStatus;
    private String setPower;
    private int temperature;
    private String windDirection;
    private String setFanAuto;
    private String  windSpeed;
    private String mode;
    private String deviceName;
    private String setSwing;
}
