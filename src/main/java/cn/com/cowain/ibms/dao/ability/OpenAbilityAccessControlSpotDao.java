package cn.com.cowain.ibms.dao.ability;

import cn.com.cowain.ibms.entity.ability.OpenAbilityAccessControlSpot;
import cn.com.cowain.ibms.enumeration.ability.Ability;
import cn.com.cowain.ibms.enumeration.iot.AccessControlInOut;
import cn.com.cowain.ibms.enumeration.iot.AccessControlSpotPlace;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

/**
 * IBMS 开放能力关联门禁点表Dao
 *
 * @author: yanzy
 * @date: 2022/01/14/16:53
 * @version:
 */
public interface OpenAbilityAccessControlSpotDao extends JpaRepository<OpenAbilityAccessControlSpot, String>, JpaSpecificationExecutor<OpenAbilityAccessControlSpot> {

    /**
     * 根据能力ID查询
     *
     * @param abilityId 开放能力id
     * @return 对应关系
     * @author Yang.Lee
     * @date 2022/1/14 14:23
     **/
    List<OpenAbilityAccessControlSpot> findByOpenAbilityId(String abilityId);

    /**
     * 根据能力查询门禁点总数
     *
     * @param abilityId 开放能力id
     * @return 数据总数
     * @author Yang.Lee
     * @date 2022/1/14 15:22
     **/
    Long countByOpenAbilityId(String abilityId);

    /**
     * @title
     * @Description 描述 查询这个能力与设备是否已经添加过，如果添加过则不会再处理----需要先删除再添加
     * @author jf.sui
     * @Date
     */

    Long countByOpenAbilityIdAndSpotId(String abilityId, String spotId);


    Optional<OpenAbilityAccessControlSpot> findByOpenAbilityIdAndSpotId(String abilityId, String spotId);

    List<OpenAbilityAccessControlSpot> findBySpotId(String spotId);

    /**
     * 根据开放能力查找门禁点列表
     *
     * @param ability 开放能力
     * @return 门禁点关系列表
     * @author Yang.Lee
     * @date 2022/1/27 17:02
     **/
    List<OpenAbilityAccessControlSpot> findByOpenAbilityAbility(Ability ability);

    /**
     * 根据能力和地点查询
     *
     * @param ability 能力
     * @param place   地点
     * @return 结果
     * @author Yang.Lee
     * @date 2022/2/18 15:54
     **/
    List<OpenAbilityAccessControlSpot> findByOpenAbilityAbilityAndSpotAccessControlSpotPlace(Ability ability, AccessControlSpotPlace place);


    List<OpenAbilityAccessControlSpot> findByOpenAbilityAbilityAndSpotInOut(Ability ability, AccessControlInOut in);
}
