package cn.com.cowain.ibms.rest.resp.distinguish;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/11 11:27
 */
@Data
@ApiModel("人员组响应对象")
public class StaffSysUserResp {

    @ApiModelProperty(value = "员工工号", required = true, position = 0)
    private String hrId;

    @ApiModelProperty(value = "员工中午姓名", required = true, position = 1)
    private String nameZh;
}
