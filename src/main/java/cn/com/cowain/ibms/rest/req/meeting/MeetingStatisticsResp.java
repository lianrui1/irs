package cn.com.cowain.ibms.rest.req.meeting;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2021/9/27 19:45
 */
@Data
@ApiModel("会议数据统计")
public class MeetingStatisticsResp {

    @ApiModelProperty(value = "会议总数", required = true, example = "10", position = 1)
    private int totalNumber;

    @ApiModelProperty(value = "正在进行的会议数量", required = true, example = "3", position = 2)
    private int inProgressNumber;

    @ApiModelProperty(value = "已结束的会议数量", required = true, example = "4", position = 3)
    private int endedNumber;
}
