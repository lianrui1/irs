package cn.com.cowain.ibms.service.meeting;

import cn.com.cowain.ibms.entity.meeting.InvitationCard;
import cn.com.cowain.ibms.rest.req.meeting.AccompanyReq;
import cn.com.cowain.ibms.rest.req.meeting.InvitationAcceptReq;
import cn.com.cowain.ibms.rest.req.meeting.InvitationRefuseReq;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import org.springframework.web.multipart.MultipartFile;

public interface InvitationCardService {

    // 获取邀请卡详情
    ServiceResult getDetail(String code, String recordCode);

    // 接受短信邀约
    ServiceResult accept(String code, InvitationAcceptReq req);

    // 拒绝短信邀约
    ServiceResult refuse(String code, InvitationRefuseReq req);

    // 发送验证码
    ServiceResult verificationCode(String phone);

    // 验证人脸
    ServiceResult uploadAndCheckFace(MultipartFile file);

    // 访客状态查询
    ServiceResult visitorStatus(String visitorRecordNo, String jobNum, String recordCode);

    // 访客随行
    ServiceResult accompany(AccompanyReq req);

    // 删除邀请函及所有信息
    ServiceResult delete(InvitationCard card);

    // 取消邀请函
    ServiceResult cancel(InvitationCard card);
}
