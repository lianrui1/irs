package cn.com.cowain.ibms.enumeration.iot;

/**
 * @Author tql
 * @Description 人员类型
 * @Date 21-11-9 上午10:09
 * @Version 1.0
 */
public enum PersonType {
    /**
     * 员工
     */
    EMPLOYEE("员工"),

    /**
     * 访客
     */
    VISITOR("访客"),

    UNKNOWN("未知");

    PersonType (String name){
        this.name = name;
    }
    String name;

    public String getName(){
        return this.name;
    }
}
