package cn.com.cowain.ibms.rest.req.ms_device;

import lombok.Data;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/3/20 15:09
 */
@Data
public class AccessRuleUpdateReq {

    private List<String> newGroupUuidList;

    private String deviceUuid;
}
