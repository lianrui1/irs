package cn.com.cowain.ibms.dao.office;

import cn.com.cowain.ibms.entity.office.IntelligentOfficeRuleEngine;
import cn.com.cowain.ibms.enumeration.iot.UsableStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * IntelligentOfficeRuleEngineDao
 *
 * @author: yanzy
 * @date: 2022/2/8 17:07
 */
@Repository
public interface IntelligentOfficeRuleEngineDao extends JpaRepository<IntelligentOfficeRuleEngine,String> {

    //根据名称查询
    Optional<IntelligentOfficeRuleEngine> findByName(String name);

    //根据名称和办事id查询
    Optional<IntelligentOfficeRuleEngine> findByNameAndIntelligentOfficeId(String name,String id);

    //根据办公室id查询
    List<IntelligentOfficeRuleEngine> findByIntelligentOfficeIdOrderByCreatedTimeDesc(String id);

    //根据状态查询
    List<IntelligentOfficeRuleEngine> findAllByUsableStatus(UsableStatus usableStatus);

    //根据办公室id查询该办公室下所有联动规则
    List<IntelligentOfficeRuleEngine> findAllByIntelligentOfficeId(String id);

    //根据办公室查询规则
    List<IntelligentOfficeRuleEngine> findAllByIntelligentOfficeIdIn(List<String> officeIds);
}
