package cn.com.cowain.ibms.rest.resp.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author wei.cheng
 * @date 2022/06/29 14:24
 */
@Data
@ApiModel(value = "我对权限的详情")
public class UserAuthDetailResp {

    @ApiModelProperty(value = "是否可申请")
    private Boolean isApplicable;

    @ApiModelProperty(value = "申请人信息")
    private String applicant;

    @ApiModelProperty(value = "权限分配人列表")
    private List<String> approverList;

    @ApiModelProperty(value = "门禁列表")
    private List<AuthSpotResp> spotList;

    @ApiModelProperty(value = "人脸照片")
    private String faceUrl;
}
