package cn.com.cowain.ibms.rest.device;


import cn.com.cowain.ibms.annotation.Idempotent;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.SingleControlCommandIssuedReq;
import cn.com.cowain.ibms.rest.req.device.FailDeviceWechatMsgReq;
import cn.com.cowain.ibms.rest.req.device.centralized.*;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.device.FailDeviceRecordResp;
import cn.com.cowain.ibms.rest.resp.device.centralized.*;
import cn.com.cowain.ibms.service.device.CentralizedControlService;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

@Slf4j
@RestController
@RequestMapping(API_BASE + "/centralized/control")
@Api(tags = IConst.CENTRALIZED_CONTROL)
public class CentralizedControlController {

    @Autowired
    private CentralizedControlService centralizedControlService;

    @PostMapping("/wechat/msg")
    @ApiOperation(value = "失败设备消息推送", tags = IConst.CENTRALIZED_CONTROL)
    public ResponseEntity<JsonResult<String>> wechatMsg(@RequestBody @Validated FailDeviceWechatMsgReq req, BindingResult bindingResult) {

        JsonResult<String> result = centralizedControlService.wechatMsg(req);
        if (result.getStatus() == 0) {
            return ResponseEntity.ok(JsonResult.error(result.getMsg(), null, null));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @GetMapping("/fail/device/record/{traceID}")
    @ApiOperation(value = "失败设备记录列表", tags = IConst.CENTRALIZED_CONTROL)
    public ResponseEntity<JsonResult<PageBean<FailDeviceRecordResp>>> failDeviceRecord(@PathVariable @ApiParam(value = "控制结果ID", required = true) String traceID,
                                                                                       @ApiParam(name = "page", value = "页码", defaultValue = "0") @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                                                                                       @ApiParam(name = "size", value = "每页记录数", defaultValue = "10") @RequestParam(value = "size", required = false, defaultValue = "10") int size) {

        return centralizedControlService.failDeviceRecord(traceID, page, size);
    }

    @GetMapping("/record")
    @ApiOperation(value = "集控记录列表", tags = IConst.CENTRALIZED_CONTROL)
    public ResponseEntity<JsonResult<PageBean<RecordResp>>> recordPage(RecordReq req) {

        return centralizedControlService.recordPage(req);
    }

    @Idempotent(value = "/centralizedControlCode/create", expireTime = 2L)
    @ApiOperation(value = "创建集控码", tags = {IConst.V34})
    @PostMapping("/code")
    public ResponseEntity<JsonResult<IdResp>> createCentralizedControlCode(@RequestBody @Validated CentralizedControlCodeCreateReq req, BindingResult bindingResult) {
        return centralizedControlService.createCentralizedControlCode(req);
    }

    @Idempotent(value = "/centralizedControlCode/update", expireTime = 2L)
    @ApiOperation(value = "修改集控码", tags = {IConst.V34})
    @PutMapping("/code/{id}")
    public ResponseEntity<JsonResult<IdResp>> updateCentralizedControlCode(@PathVariable(value = "id") String id, @RequestBody @Validated CentralizedControlCodeUpdateReq req,
                                                                           BindingResult bindingResult) {
        return centralizedControlService.updateCentralizedControlCode(id, req);
    }

    //@Idempotent(value = "/centralized/command/issued",expireTime = 2L)
    @PostMapping("/centralized/command/issued")
    @ApiOperation(value = "集控命令下发", tags = IConst.CENTRALIZED_CONTROL)
    public ResponseEntity<JsonResult<CentralizedControlCommandIssuedResp>> centralizedCommandIssued(@RequestBody @Validated CentralizedControlCommandIssuedReq req) {
        return centralizedControlService.centralizedCommandIssued(req);
    }

    //@Idempotent(value = "/centralized/command/retry",expireTime = 2L)
    @PostMapping("/centralized/command/retry")
    @ApiOperation(value = "集控命令重试下发", tags = IConst.CENTRALIZED_CONTROL)
    public ResponseEntity<JsonResult<CentralizedControlCommandIssuedResp>> centralizedCommandIssuedRetry(@RequestBody @Validated CentralizedControlCommandIssuedReq req) {
        return centralizedControlService.centralizedCommandIssued(req);
    }

    @PostMapping("/single/command/issued")
    @ApiOperation(value = "集控单个命令下发", tags = IConst.CENTRALIZED_CONTROL)
    public ResponseEntity<JsonResult<CentralizedControlCommandIssuedResp>> singleCommandIssued(@RequestBody @Validated SingleControlCommandIssuedReq req) {
        return centralizedControlService.singleCommandIssued(req);
    }

    @GetMapping("/select/device/{spaceId}")
    @ApiOperation(value = "查询集控码查询选择设备(分页)Web端", tags = {IConst.V34})
    public ResponseEntity<JsonResult<PageBean<CentralizedControlCodeSelectDeviceResp>>> getCentralizedControlCodeSelectDevice(@PathVariable(value = "spaceId") String spaceId, CentralizedControlCodeSelectDeviceReq req) {
        return centralizedControlService.getCentralizedControlCodeSelectDevice(spaceId, req);
    }

    @GetMapping("/control/device")
    @ApiOperation(value = "查询集控码控制设备列表(分页)Web端", tags = {IConst.V34})
    public ResponseEntity<JsonResult<PageBean<CentralizedControlCodeSelectDeviceResp>>> getCentralizedControlCodeControlDevice(@Validated CentralizedControlCodeControlDeviceReq req) {
        return centralizedControlService.getCentralizedControlCodeControlDevice(req);
    }

    @GetMapping("/code/list")
    @ApiOperation(value = "查询集控码列表(分页)", tags = {IConst.V34})
    public ResponseEntity<JsonResult<PageBean<CentralizedControlCodeResp>>> searchCentralizedControlCode(CentralizedControlCodeSearchReq req) {
        return centralizedControlService.searchCentralizedControlCode(req);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除集控码", tags = {IConst.V34})
    public ResponseEntity<JsonResult<String>> deleteCentralizedControlCode(@PathVariable String id) {
        return centralizedControlService.deleteCentralizedControlCode(id);
    }

    @GetMapping("/detail/{id}")
    @ApiOperation(value = "查询集控码详情Web端", tags = {IConst.V34})
    public ResponseEntity<JsonResult<CentralizedControlCodeDetailResp>> getCentralizedControlCodeDetail(@PathVariable String id) {
        return centralizedControlService.getCentralizedControlCodeDetail(id);
    }

    @GetMapping("/h5/code/{id}/detail")
    @ApiOperation(value = "h5端查询集控码详情", tags = {IConst.V34})
    public ResponseEntity<JsonResult<CentralizedControlCodeDetailH5Resp>> getCentralizedControlCodeDetailH5(@PathVariable String id,
                                                                                                            @RequestParam(value = "uid") @ApiParam(value = "用户uc id", required = true) String uid) {
        return centralizedControlService.getCentralizedControlCodeDetailH5(id, uid);
    }

    @GetMapping("/h5/code/{id}/device/list")
    @ApiOperation(value = "h5端查询集控码设备列表", tags = {IConst.V34})
    public ResponseEntity<JsonResult<CentralizedCodeControlDeviceListH5Resp>> getCentralizedControlCodeDeviceListH5(@PathVariable String id,
                                                                                                                    @Validated CentralizedControlCodeDeviceListH5Req req) {
        return centralizedControlService.getCentralizedControlCodeDeviceListH5(id, req);
    }

    @GetMapping("/process/status/polling/{traceId}")
    @ApiOperation(value = "任务状态轮询", tags = IConst.CENTRALIZED_CONTROL)
    public ResponseEntity<JsonResult<CentralizedControlCommandIssuedResp>> processPolling(@PathVariable(value = "traceId") String traceId) {
        return centralizedControlService.processPolling(traceId);
    }

    @PostMapping("/admin")
    @ApiOperation(value = "集控管理员新增", tags = IConst.CENTRALIZED_CONTROL)
    public ResponseEntity<JsonResult<String>> saveAdmin(@RequestBody @Validated CentralizedControlAdminReq req, BindingResult bindingResult) {

        return centralizedControlService.saveAdmin(req);
    }

    @GetMapping("/admin")
    @ApiOperation(value = "查询集控管理员", tags = IConst.CENTRALIZED_CONTROL)
    public ResponseEntity<JsonResult<CentralizedControlAdminReq>> getAdmin() {

        return centralizedControlService.getAdmin();
    }

    @DeleteMapping("/admin/{id}")
    @ApiOperation(value = "删除集控管理员", tags = IConst.CENTRALIZED_CONTROL)
    public ResponseEntity<JsonResult<String>> delAdmin(@PathVariable @ApiParam("ID") String id) {

        return centralizedControlService.delAdmin(id);
    }
}
