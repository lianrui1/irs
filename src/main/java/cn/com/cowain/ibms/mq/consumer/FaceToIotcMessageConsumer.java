package cn.com.cowain.ibms.mq.consumer;

import cn.com.cowain.ibms.mq.Topic;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * iotc处理门禁点更新人脸照片返回topic的消费
 *
 * @author wei.cheng
 * @date 2022/03/15 17:28
 */
@Slf4j
@RocketMQMessageListener(
        topic = "${rocketmq.env}-" + Topic.FACE_TO_IOTC_RETURN,
        consumerGroup = "${rocketmq.group}-" + Topic.FACE_TO_IOTC_RETURN
)
@Component
public class FaceToIotcMessageConsumer implements RocketMQListener<String> {

    @Override
    public void onMessage(@RequestBody String data) {
        log.debug("FaceToIotcMessageConsumer onMessage, data:{}", data);
    }
}
