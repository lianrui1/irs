package cn.com.cowain.ibms.rest.iot;

import cn.com.cowain.ibms.feign.bean.pass_v2.PassV2UserResp;
import cn.com.cowain.ibms.rest.req.iot.AuthApplicationReq;
import cn.com.cowain.ibms.rest.resp.iot.AccessAuthResp;
import cn.com.cowain.ibms.rest.resp.iot.UserAuthDetailResp;
import cn.com.cowain.ibms.service.auth.AuthV1Service;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author wei.cheng
 * @date 2022/06/29 18:20
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/auth/v1")
@Api(tags = IConst.MODULE_AUTH)
public class AuthV1Controller {
    @Autowired
    private AuthV1Service authService;

    @GetMapping(value = "/list")
    @ApiOperation(value = "查询用户的权限列表", tags = {IConst.MODULE_AUTH, IConst.V28})
    public ResponseEntity<JsonResult<AccessAuthResp>> getAuthListOfUser(@RequestParam @ApiParam(name = "empNo", value = "工号") String empNo) {
        ServiceResult result = authService.getAuthListOfUser(empNo);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.ok((AccessAuthResp) result.getObject()));
    }

    @GetMapping(value = "/{id}/queryByUser")
    @ApiOperation(value = "查询用户对权限详情", tags = {IConst.MODULE_AUTH, IConst.V28})
    public ResponseEntity<JsonResult<UserAuthDetailResp>> queryAuthInfoByUser(@PathVariable @ApiParam(name = "id", value = "权限ID") String id,
                                                                              @RequestParam @ApiParam(name = "empNo", value = "工号") String empNo) {
        ServiceResult result = authService.queryAuthInfoByUser(id, empNo);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.ok((UserAuthDetailResp) result.getObject()));
    }

    @PostMapping(value = "/application")
    @ApiOperation(value = "权限申请", tags = {IConst.MODULE_AUTH, IConst.V28})
    public ResponseEntity<JsonResult<String>> authApplication(@RequestBody @Validated AuthApplicationReq req, BindingResult bindingResult) {
        ServiceResult result = authService.authApplication(req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.ok((String) result.getObject()));
    }
    @GetMapping(value = "/person/type/{uuid}")
    @ApiOperation(value = "查询人员类型", tags = {IConst.MODULE_AUTH, IConst.V31})
    public ResponseEntity<JsonResult<PassV2UserResp>> personType(@PathVariable String uuid){
        ServiceResult result = authService.personType(uuid);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.ok((PassV2UserResp) result.getObject()));
    }
}
