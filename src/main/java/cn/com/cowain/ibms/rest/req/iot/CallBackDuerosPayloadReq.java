package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Author tql
 * @Description dueros回调
 * @Date 22-9-26 下午3:21
 * @Version 1.0
 */
@Data
@ApiModel("属性信息")
public class CallBackDuerosPayloadReq {

    @ApiModelProperty("请求token")
    @NotBlank
    private String accessToken;


    @ApiModelProperty("唯一id")
    @NotBlank
    private String cuid;

    @ApiModelProperty("回调具体信息")
    @NotBlank
    private CallBackDuerosApplianceReq appliance;

}
