package cn.com.cowain.ibms.mq.producer;

import cn.com.cowain.ibms.mq.Topic;
import cn.com.cowain.ibms.mq.bean.IotDeviceControlMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/6 11:12
 */
@Slf4j
@Component
public class ErrorIotDeviceMessageProducer {

    @Value("${redis.key.projectName}-${rocketmq.group}-")
    private String topic;

    @Resource
    private RocketMQTemplate rocketMQTemplate;

    public ServiceResult deviceControl(String deviceId, String cmd) {

        String messageStr = JSON.toJSONString(IotDeviceControlMessage.builder().deviceId(deviceId).cmd(cmd).build());
        Message<String> message = new GenericMessage<>(messageStr);
        rocketMQTemplate.asyncSend(topic+Topic.ERROR_DEVICE_CONTROL, message, new SendCallback() {

            public void onSuccess(SendResult sendResult) {
                log.info("send successful");
            }


            public void onException(Throwable throwable) {
                log.info("send fail; {}", throwable.getMessage());
            }
        });

        return ServiceResult.ok();
    }
}
