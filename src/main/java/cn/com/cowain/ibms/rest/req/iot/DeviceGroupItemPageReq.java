package cn.com.cowain.ibms.rest.req.iot;

import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Yang.Lee
 * @date 2021/10/21 11:12
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("设备群组内设备列表查询对象（分页）")
public class DeviceGroupItemPageReq extends PageReq {

    @ApiModelProperty(value = "项目ID", position = 1)
    private String productId;

    @ApiModelProperty(value = "项目ID", position = 2)
    private String projectId;

    @ApiModelProperty(value = "空间ID", position = 3)
    private String spaceId;

    @ApiModelProperty(value = "设备状态", position = 4)
    private DeviceStatus deviceStatus;

    @ApiModelProperty(value = "搜索关键字， SN / 设备名称", position = 5)
    private String keyword;
}
