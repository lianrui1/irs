package cn.com.cowain.ibms.rest;

import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.working_mode.WorkingModeSaveReq;
import cn.com.cowain.ibms.rest.req.working_mode.WorkingModeUpdateReq;
import cn.com.cowain.ibms.rest.resp.working_mode.ManagementPageResp;
import cn.com.cowain.ibms.rest.resp.working_mode.WorkingModeDetailResp;
import cn.com.cowain.ibms.rest.resp.working_mode.WorkingModePageResp;
import cn.com.cowain.ibms.service.workingmode.WorkingModeService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.ibms.utils.TokenUtils;
import cn.com.cowain.sfp.comm.utils.JwtUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/10/18 13:51
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/workingMode")
@Api(tags = IConst.WORKING_MODE)
public class WorkingModeController {


    @Autowired
    private WorkingModeService workingModeService;


    @PostMapping
    @ApiOperation(value = "添加工作模式场景", tags = {IConst.WORKING_MODE, IConst.V18})
    public ResponseEntity<JsonResult<String>> save(@RequestHeader("Authorization") String token, @RequestBody @Validated WorkingModeSaveReq req,  BindingResult bindingResult) {
        log.debug("save...");

        // 从token中解析员工工号
        String hrId = JwtUtil.getHrId(token);
        ServiceResult result = workingModeService.create(hrId, req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @GetMapping("/page")
    @ApiOperation(value = "查询工作模式场景列表(分页)", tags = {IConst.WORKING_MODE, IConst.V19})
    public ResponseEntity<JsonResult<PageBean<WorkingModePageResp>>> getPage(@RequestParam(value = "spaceId")String spaceId, @RequestParam(value = "page", defaultValue = "0") int page,
    @RequestParam(value = "size", defaultValue = "20") int size) {

        Sort sort = Sort.by(Sort.Direction.DESC, "controlTime");
        Pageable pageable = PageRequest.of(page, size, sort);

        PageBean<WorkingModePageResp> pageBean = workingModeService.getPage(spaceId, pageable);

        return ResponseEntity.ok(JsonResult.ok("ok" ,pageBean));
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除工作模式场景", tags = {IConst.WORKING_MODE, IConst.V18})
    public ResponseEntity<JsonResult<String>> delete(@PathVariable @ApiParam("工作模式ID") String id, @RequestHeader("Authorization") String token) {

        // 从token中解析员工工号
        String hrId = JwtUtil.getHrId(token);

        // 删除
        ServiceResult result = workingModeService.delete(id, hrId);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "查询工作模式场景详情", tags = {IConst.WORKING_MODE, IConst.V18})
    public ResponseEntity<JsonResult<WorkingModeDetailResp>> detail(@PathVariable @ApiParam("工作模式ID") String id) {

        ServiceResult result = workingModeService.detail(id);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.ok("ok" ,(WorkingModeDetailResp)result.getObject()));
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "编辑工作模式场景", tags = {IConst.WORKING_MODE, IConst.V18})
    public ResponseEntity<JsonResult<WorkingModeDetailResp>> update(@PathVariable @ApiParam("工作模式ID") String id, @RequestBody @Validated WorkingModeUpdateReq req, BindingResult bindingResult) {

        // 从token中解析员工工号
        String hrId = JwtUtil.getHrId(TokenUtils.getToken());
        ServiceResult result = workingModeService.update(hrId, id, req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK" ));
    }

    @GetMapping("/rule")
    @ApiOperation(value = "人员是否拥有入口权限", tags = {IConst.WORKING_MODE, IConst.V18})
    public ResponseEntity<JsonResult<Integer>> rule(@ApiParam("空间ID") String spaceId){

        // 从token中解析员工工号
        String hrId = JwtUtil.getHrId(TokenUtils.getToken());

        ServiceResult result = workingModeService.rule(spaceId, hrId);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.ok("OK", (Integer) result.getObject()));
    }


    @PostMapping("/management/{id}")
    @ApiOperation(value = "新增权限人员", tags = {IConst.WORKING_MODE, IConst.V18})
    public ResponseEntity<JsonResult<String>> managementCreate(@PathVariable @ApiParam("空间ID") String id, @RequestBody List<String> empNos, @RequestHeader("Authorization") String token) {


        ServiceResult result = workingModeService.managementCreate(id, empNos, token, "0");
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.ok("ok" ,null));
    }

    @GetMapping("/management/page/{id}")
    @ApiOperation(value = "查询权限人员列表", tags = {IConst.WORKING_MODE, IConst.V19})
    public ResponseEntity<JsonResult<PageBean<ManagementPageResp>>> managementPage(@PathVariable @ApiParam("空间ID") String id, @RequestParam(value = "page", defaultValue = "0") int page,
                                                                                   @RequestParam(value = "size", defaultValue = "20") int size) {

        PageBean<ManagementPageResp> managementPage = workingModeService.managementPage(id,page,size);

        return ResponseEntity.ok(JsonResult.ok("ok" ,managementPage));
    }

    @DeleteMapping("/management/{id}")
    @ApiOperation(value = "删除权限人员", tags = {IConst.WORKING_MODE, IConst.V18})
    public ResponseEntity<JsonResult<String>> managementDelete(@PathVariable @ApiParam("空间ID") String id, @RequestParam("empNos") @ApiParam("人员工号") List<String> empNos, @RequestHeader("Authorization") String token) {


        ServiceResult result = workingModeService.managementDelete(id, empNos, token);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK" ));
    }

    @DeleteMapping("/pc/management/{spaceId}")
    @ApiOperation(value = "PC删除权限人员", tags = {IConst.WORKING_MODE, IConst.V19})
    public ResponseEntity<JsonResult<String>> managementDeletePc(@PathVariable @ApiParam(value = "空间ID", required = true) String spaceId, @RequestParam("empNos") @ApiParam(value = "人员工号", required = true) List<String> empNos) {


        ServiceResult result = workingModeService.managementDeletePc(spaceId, empNos);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK" ));
    }
}
