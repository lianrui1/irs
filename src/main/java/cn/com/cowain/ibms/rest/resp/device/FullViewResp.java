package cn.com.cowain.ibms.rest.resp.device;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * @Author tql
 * @Description 楼宇总览 出参类
 * @Date 21-11-4 下午3:13
 * @Version 1.0
 */
@Data
@Builder
@ApiModel("楼宇总览")
public class FullViewResp {

    @ApiModelProperty(value = "工厂设备信息",  example = "网关", position = 1)
    private List<FactoryDeviceMsgResp> factoryDeviceMsgRespList;

    @ApiModelProperty(value = "设备统计圆",  example = "网关", position = 2)
    private List<FullViewRoundResp> fullViewRound;

    @ApiModelProperty(value = "设备统计圆",  example = "网关", position = 2)
    private List<FullViewRadioResp> radio;
}
