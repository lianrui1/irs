package cn.com.cowain.ibms.service.meeting;

import cn.com.cowain.ibms.entity.ReservationRecord;

/**
 * 日程相关Service
 *
 * @author Yang.Lee
 * @date 2021/6/21 13:20
 */
public interface ScheduleService {

    /**
     * 创建日程（异步执行）
     *
     * @param reservationRecord 会议信息
     * @param empNo             日程所属员工
     * @return 操作结果
     * @author Yang.Lee
     * @date 2021/6/21 19:46
     **/
    void create(ReservationRecord reservationRecord, String empNo);

    /**
     * 取消日程（异步执行）
     *
     * @param reservationRecord 会议信息
     * @param empNo             日程所属员工
     * @return 操作结果
     * @author Yang.Lee
     * @date 2021/6/21 20:50
     **/
    void cancel(ReservationRecord reservationRecord, String empNo);

    /**
     * 修改日程（异步执行）
     *
     * @param reservationRecord 会议信息
     * @param empNo             日程所属员工
     * @return 操作结果
     * @author Yang.Lee
     * @date 2021/6/21 21:38
     **/
    void update(ReservationRecord reservationRecord, String empNo);
}
