package cn.com.cowain.ibms.rest.resp.space;

import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.enumeration.space.ProjectPropertyRightType;
import cn.com.cowain.ibms.rest.req.SysUserBean;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 项目信息响应数据实体对象
 * @author Yang.Lee
 * @date 2020/12/17 17:32
 */
@Data
@ApiModel("项目信息响应对象")
public class ProjectResp implements Serializable {

    @ApiModelProperty(value = "主键ID", required = true)
    private String id;

    @ApiModelProperty(value = "项目编号", required = true, position = 1)
    private String no;

    @ApiModelProperty(value = "项目名称", required = true, position = 2)
    private String projectName;

    @ApiModelProperty(value = "省级编号", required = true, position = 3)
    private String provinceId;

    @ApiModelProperty(value = "省级名称", required = true, position = 4)
    private String provinceName;

    @ApiModelProperty(value = "市级编号", required = true, position = 5)
    private String cityId;

    @ApiModelProperty(value = "市级名称", required = true, position = 6)
    private String cityName;

    @ApiModelProperty(value = "区级编号", required = true, position = 7)
    private String districtId;

    @ApiModelProperty(value = "区级名称", required = true, position = 8)
    private String districtName;

    @ApiModelProperty(value = "详细地址", position = 9)
    private String address;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "会议室创建人", required = true, position = 10)
    private SysUser createUser;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "会议室更新人", required = false, position = 11)
    private SysUser updateUser;


    /**
     * 更新时间
     */
    @ApiModelProperty(value = "会议室更新时间", required = false, position = 12)
    private LocalDateTime updateTime;

    /**
     * 创建时间
     */
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "创建时间", required = true, position = 13)
    private LocalDateTime createdTime;

    /**
     * 项目下是否有空间
     */
    @ApiModelProperty(value = "是否包含空间 1为是 0为否", required = true, position = 14)
    private Integer includeSpace;

    /**
     * 项目区域
     */
    @ApiModelProperty(value = "项目区域", required = true, position = 15)
    private String projectArea;

    /**
     * 项目单位
     */
    @ApiModelProperty(value = "项目单位", required = true, position = 16)
    private String visitCompany;

    @ApiModelProperty(value = "产权属性", required = true, position = 17)
    private ProjectPropertyRightType propertyRightType;

    @ApiModelProperty(value = "产权属性名称", required = true, position = 18)
    private String propertyRightTypeName;

    @ApiModelProperty(value = "面积", required = true, position = 19)
    private String areaM2;

    @ApiModelProperty(value = "简介", required = true, position = 20)
    private String introduction;

    @ApiModelProperty(value = "图片", required = true, position = 21)
    private String picture;

    @ApiModelProperty(value = "一级子空间列表", required = true, position = 22)
    private List<SpaceDetailResp> spaceL1;

    @ApiModelProperty(value = "管理员列表", required = true, position = 23)
    private List<SysUserBean> adminList;

    @ApiModelProperty(value = "项目区域ID", required = true, position = 23)
    private String projectAreaId;
}
