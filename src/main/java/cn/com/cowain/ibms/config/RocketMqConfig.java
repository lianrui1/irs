package cn.com.cowain.ibms.config;

import cn.com.cowain.ibms.mq.consumer.ErrorIotDeviceMessageConsumer;
import cn.com.cowain.ibms.rest.distinguish.OpenDoorConsumer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author HJL
 */
@Configuration
public class RocketMqConfig {

//    @Bean
//    public AccessRecordConsumer accessRecordConsumer() {
//        return new AccessRecordConsumer();
//    }

    @Bean
    public OpenDoorConsumer openDoorConsumer() {
        return new OpenDoorConsumer();
    }


    @Bean
    public ErrorIotDeviceMessageConsumer errorIotDeviceMessageConsumer() {
        return new ErrorIotDeviceMessageConsumer();
    }

//    @Bean
//    public HikServerAccessRecordMessageConsumer hikServerAccessRecordMessageConsumer() {
//        return new HikServerAccessRecordMessageConsumer();
//    }
}
