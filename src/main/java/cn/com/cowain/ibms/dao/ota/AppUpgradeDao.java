package cn.com.cowain.ibms.dao.ota;

import cn.com.cowain.ibms.entity.ota.OTAUpgrade;
import cn.com.cowain.ibms.enumeration.ota.Application;
import cn.com.cowain.ibms.enumeration.ota.UpgradeStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface AppUpgradeDao extends JpaRepository<OTAUpgrade, String>, JpaSpecificationExecutor<OTAUpgrade> {

    // 根据应用任务ID和应用查出当前应用所有任务
    List<OTAUpgrade> findByIdNotAndApplication(String id, Application application);

    // 根据升级应用和版本号查询是否有升级任务
    Optional<OTAUpgrade> findByApplicationAndVersionOTANotAndStatusNotAndUpgradeTimeLessThan(Application application, String versionOTA, UpgradeStatus discard, LocalDateTime upgradeTime);
}
