package cn.com.cowain.ibms.dao.device;

import cn.com.cowain.ibms.entity.office.DeviceModeStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DeviceModeStatusDao extends JpaRepository<DeviceModeStatus, String>, JpaSpecificationExecutor<DeviceModeStatus> {

    // 根据模式id获取对应设备
    List<DeviceModeStatus> findByModeId(String id);

    // 根据模式ID和设备ID获取对应状态
    Optional<DeviceModeStatus> findByModeIdAndDeviceId(String id, String id1);

    // 根据空间ID和设备ID获取设备状态
    List<DeviceModeStatus> findByModeSpaceIdAndDeviceId(String id, String id1);

    /**
     * 根据办公室ID和设备id查询
     *
     * @param officeId 办公室ID
     * @param deviceId 设备ID
     * @return 查询结果
     * @author Yang.Lee
     * @date 2022/1/5 9:49
     **/
    List<DeviceModeStatus> findByModeIntelligentOfficeIdAndDeviceId(String officeId, String deviceId);
}
