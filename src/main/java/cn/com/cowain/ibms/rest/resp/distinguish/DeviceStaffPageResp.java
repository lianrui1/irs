package cn.com.cowain.ibms.rest.resp.distinguish;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author Yang.Lee
 * @date 2021/7/8 15:12
 */
@Data
public class DeviceStaffPageResp {

    @ApiModelProperty(value = "员工姓名")
    private String name;

    @ApiModelProperty(value = "员工工号")
    private String empNo;

    @ApiModelProperty(value = "员工部门")
    private String department;

    @ApiModelProperty(value = "图片")
    private String pic;

    @ApiModelProperty(value = "通行开始时间")
    private LocalDateTime startTime;

    @ApiModelProperty(value = "通行结束时间")
    private LocalDateTime endTime;
}
