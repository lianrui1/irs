package cn.com.cowain.ibms.service.auth.impl;

import cn.com.cowain.ibms.bo.AuthV1BO;
import cn.com.cowain.ibms.dao.iot.*;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.iot.*;
import cn.com.cowain.ibms.enumeration.ApprovalStatus;
import cn.com.cowain.ibms.enumeration.iot.ApproverType;
import cn.com.cowain.ibms.enumeration.iot.AuthV1ApplicationStatus;
import cn.com.cowain.ibms.enumeration.iot.UserAuthStatus;
import cn.com.cowain.ibms.feign.bean.pass_v2.PassV2UserResp;
import cn.com.cowain.ibms.rest.req.iot.AuthApplicationReq;
import cn.com.cowain.ibms.rest.resp.iot.AccessAuthResp;
import cn.com.cowain.ibms.rest.resp.iot.AuthSpotResp;
import cn.com.cowain.ibms.rest.resp.iot.UserAuthDetailResp;
import cn.com.cowain.ibms.rest.resp.iot.UserAuthResp;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.auth.AuthV1Service;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.iot.AccessControlSpotApplicationRecordService;
import cn.com.cowain.ibms.service.iot.AccessControlSpotService;
import cn.com.cowain.ibms.service.task.TaskCenterTaskService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.RedisUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author wei.cheng
 * @date 2022/06/29 18:24
 */
@Service
public class AuthV1ServiceImpl implements AuthV1Service {
    @Resource
    private AuthDao authDao;
    @Resource
    private AccessControlSpotUserDao accessControlSpotUserDao;
    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserServiceUC;
    @Resource
    private AccessControlSpotApplicationRecordDao accessControlSpotApplicationRecordDao;
    @Resource
    private AccessControlSpotApproverRelationDao accessControlSpotApproverRelationDao;
    @Resource
    private AccessControlSpotApproverDao accessControlSpotApproverDao;
    @Resource
    private AccessControlSpotService accessControlSpotService;
    @Resource
    private AccessControlSpotDao accessControlSpotDao;
    @Resource
    private TaskCenterTaskService taskCenterTaskService;
    @Resource
    private AccessControlSpotApplicationRecordService accessControlSpotApplicationRecordService;
    @Resource
    private RedisUtil redisUtil;


    @Override
    public ServiceResult getAuthListOfUser(String empNo) {
        SysUser sysUser = sysUserServiceUC.getUserByEmpNo(empNo);
        if (Objects.isNull(sysUser)) {
            return ServiceResult.error("根据工号未找到对于用户");
        }
        AccessAuthResp accessAuthResp = new AccessAuthResp();
        List<UserAuthResp> authList = new ArrayList<>();
        List<UserAuthResp> applicationList = new ArrayList<>();
        // 查询所有权限列表
        List<Auth> allAuthList = authDao.findAll().stream().filter(a -> StringUtils.isNotBlank(a.getSpotIds())).collect(Collectors.toList());
        // 未配置权限数据时，不用处理
        if (CollectionUtils.isNotEmpty(allAuthList)) {
            // 获取用户有权限的门禁点id列表
            List<String> spotIds = getUserAccessSpotIds(empNo);
            for (Auth auth : allAuthList) {
                // 权限关联的门禁点id列表
                List<String> authSpotIds = Arrays.asList(auth.getSpotIds().split(","));
                // 没有权限的门禁点id列表
                List<String> noPermissionSpotIds = ListUtils.removeAll(authSpotIds, spotIds);
                if (CollectionUtils.isEmpty(noPermissionSpotIds)) {
                    authList.add(new UserAuthResp(auth.getId(), auth.getName()));
                } else {
                    // 只有员工可以申请门禁权限
                    if (!SysUserService.isVisitor(empNo)) {
                        UserAuthResp userAuthResp = new UserAuthResp(auth.getId(), auth.getName());
                        List<AccessControlSpotApplicationRecord> accessControlSpotApplicationRecordList = accessControlSpotApplicationRecordDao
                                .findAllByApplicantHrIdAndSpotIdIn(empNo, noPermissionSpotIds);
                        // 根据申请的创建时间降序排序，只计算最近用户对门禁点最近一次申请
                        accessControlSpotApplicationRecordList.sort((a1, a2) -> ObjectUtils.compare(a2.getCreatedTime(), a1.getCreatedTime()));
                        Map<String, AccessControlSpotApplicationRecord> spotIdToApplicationMap = new HashMap<>();
                        // 是否存在待审批的申请
                        boolean isPending = false;
                        // 是否存在已驳回
                        boolean isDenied = false;
                        for (AccessControlSpotApplicationRecord applicationRecord : accessControlSpotApplicationRecordList) {
                            if (spotIdToApplicationMap.containsKey(applicationRecord.getSpotId())) {
                                continue;
                            }
                            spotIdToApplicationMap.put(applicationRecord.getSpotId(), applicationRecord);
                            if (ApprovalStatus.PENDING.equals(applicationRecord.getStatus())) {
                                isPending = true;
                            } else if (ApprovalStatus.DENIED.equals(applicationRecord.getStatus())) {
                                isDenied = true;
                            }
                        }
                        // 是否存在未申请
                        boolean isNotApplied = false;
                        for (String spotId : noPermissionSpotIds) {
                            if (Objects.isNull(spotIdToApplicationMap.get(spotId))) {
                                isNotApplied = true;
                                break;
                            }
                        }
                        //当权限中存在审批中，则此处展示审批中状态；若无审批中，存在审批拒绝/未申请，则展示去申请
                        UserAuthStatus userAuthStatus;
                        if (isPending) {
                            userAuthStatus = UserAuthStatus.UNDER_APPROVAL;
                        } else {
                            if (isDenied || isNotApplied) {
                                userAuthStatus = UserAuthStatus.APPLICABLE;
                            } else {
                                userAuthStatus = UserAuthStatus.PASSED;
                            }
                        }
                        userAuthResp.setStatus(userAuthStatus.getValue());
                        userAuthResp.setStatusDesc(userAuthStatus.getDesc());
                        applicationList.add(userAuthResp);
                    }
                }
            }
        }
        accessAuthResp.setAuthList(authList);
        accessAuthResp.setApplicationList(applicationList);
        return ServiceResult.ok(accessAuthResp);
    }

    /**
     * 获取用户有权限的门禁点ID列表
     *
     * @param empNo
     * @return
     */
    private List<String> getUserAccessSpotIds(String empNo) {
        // 查询用户已有权限的门禁id列表
        List<AccessControlSpotUser> accessControlSpotUserList = accessControlSpotUserDao.findByUserHrId(empNo);
        // 过滤掉开始时间大于当前时间，以及结束时间小于当前时间的记录
        LocalDateTime now = LocalDateTime.now();
        accessControlSpotUserList.removeIf(au -> {
            if (au.getAccessStartTime().isAfter(now)) {
                return true;
            }
            return au.getAccessEndTime().isBefore(now);
        });
        return accessControlSpotUserList.stream().map(a -> a.getAccessControlSpot().getId()).collect(Collectors.toList());
    }

    @Override
    public ServiceResult queryAuthInfoByUser(String id, String empNo) {
        if (SysUserService.isVisitor(empNo)) {
            return ServiceResult.error("只有员工才能查询权限详情");
        }
        Optional<Auth> optionalAuth = authDao.findById(id);
        if (optionalAuth.isEmpty()) {
            return ServiceResult.error("未找到权限");
        }
        Auth auth = optionalAuth.get();
        if (StringUtils.isBlank(auth.getSpotIds())) {
            return ServiceResult.error("权限未配置门禁点");
        }
        SysUser sysUser = sysUserServiceUC.getUserByEmpNo(empNo);
        if (Objects.isNull(sysUser)) {
            return ServiceResult.error("员工库中未找到您的工号");
        }

        List<SysUser> approverList = getAccessControlSpotApproverList(Arrays.asList(auth.getSpotIds().split(",")), ApproverType.STAFF);
        if (CollectionUtils.isEmpty(approverList)) {
            return ServiceResult.error("门a点未设置分配人");
        }
        // 获取用户对权限的部分信息
        AuthV1BO authV1BO = getAuthV1BO(auth, sysUser);

        UserAuthDetailResp userAuthDetailResp = new UserAuthDetailResp();
        userAuthDetailResp.setIsApplicable(authV1BO.getIsApplicable());
        userAuthDetailResp.setApplicant(getPeopleInfo(sysUser));
        userAuthDetailResp.setSpotList(authV1BO.getSpotList());

        // 设置分配人
        userAuthDetailResp.setApproverList(approverList.stream().map(this::getPeopleInfo).collect(Collectors.toList()));

        // 查询人脸照片
        userAuthDetailResp.setFaceUrl(accessControlSpotService.getFaceImg(empNo));
        return ServiceResult.ok(userAuthDetailResp);
    }

    /**
     * 获取用户对权限的部分共用信息
     *
     * @param auth
     * @param sysUser
     * @return
     */
    private AuthV1BO getAuthV1BO(Auth auth, SysUser sysUser) {
        AuthV1BO authV1BO = new AuthV1BO();

        // 门禁列表
        List<AuthSpotResp> spotRespList = new ArrayList<>();
        // 权限关联的门禁点id列表
        List<String> authSpotIds = Arrays.asList(auth.getSpotIds().split(","));
        // 获取用户有权限的门禁点id列表
        List<String> spotIds = getUserAccessSpotIds(sysUser.getEmpNo());
        // 查询用户对门禁点的申请列表
        List<AccessControlSpotApplicationRecord> accessControlSpotApplicationRecordList = accessControlSpotApplicationRecordDao
                .findAllByApplicantHrIdAndSpotIdIn(sysUser.getEmpNo(), authSpotIds);
        // 根据申请的创建时间降序排序，只计算最近用户对门禁点最近一次申请
        accessControlSpotApplicationRecordList.sort((a1, a2) -> ObjectUtils.compare(a2.getCreatedTime(), a1.getCreatedTime()));
        Map<String, AccessControlSpotApplicationRecord> spotIdToApplicationMap = new HashMap<>();
        // 是否存在未申请
        boolean isNotApplied = false;
        // 是否存在已驳回
        boolean isDenied = false;
        // 是否存在审批中
        boolean isPending = false;
        for (AccessControlSpotApplicationRecord applicationRecord : accessControlSpotApplicationRecordList) {
            if (spotIdToApplicationMap.containsKey(applicationRecord.getSpotId())) {
                continue;
            }
            spotIdToApplicationMap.put(applicationRecord.getSpotId(), applicationRecord);
        }
        List<AccessControlSpot> accessControlSpotList = accessControlSpotDao.findAllById(authSpotIds);
        for (AccessControlSpot accessControlSpot : accessControlSpotList) {
            AccessControlSpotApplicationRecord accessControlSpotApplicationRecord = spotIdToApplicationMap.get(accessControlSpot.getId());
            AuthSpotResp authSpotResp = new AuthSpotResp();
            authSpotResp.setId(accessControlSpot.getId());
            authSpotResp.setName(accessControlSpot.getName());
            authSpotResp.setLocation(accessControlSpot.getAddress());
            AuthV1ApplicationStatus authV1ApplicationStatus;
            if (spotIds.contains(accessControlSpot.getId())) {
                // 有权限而无申请时门禁点不展示
                if (Objects.isNull(accessControlSpotApplicationRecord)) {
                    continue;
                } else {
                    authV1ApplicationStatus = AuthV1ApplicationStatus.PASS;
                }
            } else {
                if (Objects.isNull(accessControlSpotApplicationRecord)) {
                    authV1ApplicationStatus = AuthV1ApplicationStatus.NOT_APPLIED;
                    isNotApplied = true;
                } else {
                    if (ApprovalStatus.PENDING.equals(accessControlSpotApplicationRecord.getStatus())) {
                        authV1ApplicationStatus = AuthV1ApplicationStatus.IN_APPLICATION;
                        isPending = true;
                    } else if (ApprovalStatus.APPROVED.equals(accessControlSpotApplicationRecord.getStatus())) {
                        authV1ApplicationStatus = AuthV1ApplicationStatus.PASS;
                    } else {
                        authV1ApplicationStatus = AuthV1ApplicationStatus.DECLINE;
                        isDenied = true;
                    }
                }
            }
            authSpotResp.setApplicationStatus(authV1ApplicationStatus.getValue());
            authSpotResp.setApplicationStatusDesc(authV1ApplicationStatus.getDesc());
            spotRespList.add(authSpotResp);

        }
        authV1BO.setSpotList(spotRespList);
        //若无审批中，存在审批拒绝/未申请，则可以申请
        authV1BO.setIsApplicable(!isPending && (isDenied || isNotApplied));
        authV1BO.setAccessControlSpotList(accessControlSpotList);
        return authV1BO;
    }

    private String getPeopleInfo(SysUser sysUser) {
        return sysUser.getNameZh() + "/" + sysUser.getEmpNo() + "/" + sysUser.getDeptName();
    }

    /**
     * 批量获取门禁点的分配人的总和分配人
     *
     * @param spotIdList
     * @param approverType
     * @return
     */
    public List<SysUser> getAccessControlSpotApproverList(List<String> spotIdList, ApproverType approverType) {
        if (CollectionUtils.isEmpty(spotIdList)) {
            return new ArrayList<>();
        }
        List<SysUser> sysUserList = new ArrayList<>();
        List<AccessControlSpotApproverRelation> accessControlSpotApproverRelationList = accessControlSpotApproverRelationDao.findAllBySpotIdIn(spotIdList);
        if (!CollectionUtils.isEmpty(accessControlSpotApproverRelationList)) {
            List<String> userHrIdList = accessControlSpotApproverRelationList.stream().map(AccessControlSpotApproverRelation::getUserHrId)
                    .collect(Collectors.toList());
            List<AccessControlSpotApprover> accessControlSpotApproverList = accessControlSpotApproverDao.findAllByUserHrIdIn(userHrIdList);
            List<String> accessControlSpotApproverHrIdList = accessControlSpotApproverList.stream().filter(a -> a.getApproveType()
                    .contains(approverType.name())).map(AccessControlSpotApprover::getUserHrId)
                    .collect(Collectors.toList());
            sysUserList = accessControlSpotApproverHrIdList.stream().map(id -> sysUserServiceUC.getUserByEmpNo(id))
                    .filter(Objects::nonNull).collect(Collectors.toList());
        }
        return sysUserList;
    }

    /**
     * 批量获取门禁点的分配人，一一对应
     *
     * @param spotIdList
     * @param approverType
     * @return
     */
    public MultiValueMap<String, SysUser> getAccessControlSpotApproverMap(List<String> spotIdList, ApproverType approverType) {
        if (CollectionUtils.isEmpty(spotIdList)) {
            return new LinkedMultiValueMap<>();
        }
        List<SysUser> sysUserList;
        MultiValueMap<String, SysUser> spotIdToApproverMap = new LinkedMultiValueMap<>();
        List<AccessControlSpotApproverRelation> accessControlSpotApproverRelationList = accessControlSpotApproverRelationDao.findAllBySpotIdIn(spotIdList);
        if (!CollectionUtils.isEmpty(accessControlSpotApproverRelationList)) {
            MultiValueMap<String, String> spotIdToHrIdMap = new LinkedMultiValueMap<>();
            accessControlSpotApproverRelationList.forEach(accessControlSpotApproverRelation -> spotIdToHrIdMap.add(
                    accessControlSpotApproverRelation.getSpot().getId(), accessControlSpotApproverRelation.getUserHrId()));
            List<String> userHrIdList = accessControlSpotApproverRelationList.stream().map(AccessControlSpotApproverRelation::getUserHrId)
                    .collect(Collectors.toList());
            List<AccessControlSpotApprover> accessControlSpotApproverList = accessControlSpotApproverDao.findAllByUserHrIdIn(userHrIdList);
            List<String> accessControlSpotApproverHrIdList = accessControlSpotApproverList.stream().filter(a -> a.getApproveType()
                    .contains(approverType.name())).map(AccessControlSpotApprover::getUserHrId)
                    .collect(Collectors.toList());
            sysUserList = accessControlSpotApproverHrIdList.stream().map(id -> sysUserServiceUC.getUserByEmpNo(id))
                    .filter(Objects::nonNull).collect(Collectors.toList());
            Map<String, SysUser> hrIdToSysUserMap = sysUserList.stream().collect(Collectors.toMap(SysUser::getEmpNo, s -> s));
            spotIdToHrIdMap.forEach((spotId, hrIdList) -> {
                hrIdList.forEach(hrId -> {
                    SysUser sysUser = hrIdToSysUserMap.get(hrId);
                    if (Objects.nonNull(sysUser)) {
                        spotIdToApproverMap.add(spotId, sysUser);
                    }
                });
            });
        }
        return spotIdToApproverMap;
    }

    @Override
    @Transactional
    public ServiceResult authApplication(AuthApplicationReq req) {
        if (SysUserService.isVisitor(req.getEmpNo())) {
            return ServiceResult.error("只有员工才能查询权限详情");
        }
        Optional<Auth> optionalAuth = authDao.findById(req.getAuthId());
        if (optionalAuth.isEmpty()) {
            return ServiceResult.error("未找到权限");
        }
        Auth auth = optionalAuth.get();
        if (StringUtils.isBlank(auth.getSpotIds())) {
            return ServiceResult.error("权限未配置门禁点");
        }
        SysUser sysUser = sysUserServiceUC.getUserByEmpNo(req.getEmpNo());
        if (Objects.isNull(sysUser)) {
            return ServiceResult.error("员工库中未找到您的工号");
        }

        // 获取用户对权限的部分信息
        AuthV1BO authV1BO = getAuthV1BO(auth, sysUser);

        if (!Boolean.TRUE.equals(authV1BO.getIsApplicable())) {
            return ServiceResult.error("用户不可申请");
        }

        // 权限的所有门禁列表
        List<AccessControlSpot> accessControlSpotList = authV1BO.getAccessControlSpotList();
        Map<String, AccessControlSpot> idToSpotMap = accessControlSpotList.stream().collect(Collectors.toMap(AccessControlSpot::getId, a -> a));

        // 用户可见的权限列表
        List<AuthSpotResp> spotRespList = authV1BO.getSpotList();

        // 用户可申请的门禁列表
        List<AccessControlSpot> applicableSpotList = new ArrayList<>();

        for (AuthSpotResp authSpotResp : spotRespList) {
            // 未申请以及审批拒绝的可以进行申请
            if (AuthV1ApplicationStatus.NOT_APPLIED.getValue().equals(authSpotResp.getApplicationStatus()) ||
                    AuthV1ApplicationStatus.DECLINE.getValue().equals(authSpotResp.getApplicationStatus())) {
                applicableSpotList.add(idToSpotMap.get(authSpotResp.getId()));
            }
        }

        if (CollectionUtils.isEmpty(applicableSpotList)) {
            return ServiceResult.error("无可申请的门禁点");
        }

        // 获取门禁点的分配人
        MultiValueMap<String, SysUser> spotIdToApproverMap = getAccessControlSpotApproverMap(applicableSpotList.stream().map(AccessControlSpot::getId)
                .collect(Collectors.toList()), ApproverType.STAFF);

        // 校验需要申请的门禁点是否都已设置了分配人
        for (AccessControlSpot accessControlSpot : applicableSpotList) {
            if (CollectionUtils.isEmpty(spotIdToApproverMap.get(accessControlSpot.getId()))) {
                return ServiceResult.error("门禁点" + accessControlSpot.getName() + "未设置分配人");
            }
        }

        for (AccessControlSpot accessControlSpot : applicableSpotList) {
            // 创建申请
            AccessControlSpotApplicationRecord applicationRecord = AccessControlSpotApplicationRecord.buildAccessControlSpotApplicationRecord(req.getFaceUrl(),
                    sysUser, accessControlSpot);
            accessControlSpotApplicationRecordDao.save(applicationRecord);
            for (SysUser approver : spotIdToApproverMap.get(accessControlSpot.getId())) {
                // 创建代办任务
                taskCenterTaskService.createAccessControlSpotApplicationTask(approver, applicationRecord);
                // 发送微信消息提示
                accessControlSpotApplicationRecordService.sendMessageToApprover(sysUser, applicationRecord, approver);
            }
        }

        //有效人脸照片保存，以需要重复上传人脸
        String faceKey = redisUtil.createRealKey("face-image-data:" + sysUser.getEmpNo());
        redisUtil.set(faceKey, req.getFaceUrl());
        return ServiceResult.ok("权限申请成功");
    }

    @Override
    public ServiceResult personType(String uuid) {
        // 获取用户信息
        Optional<PassV2UserResp> userRespOptional = sysUserServiceUC.getUserByUid(uuid);
        if (userRespOptional.isEmpty()) {
            return ServiceResult.error("未找到用户信息", ErrConst.E01);
        }

        PassV2UserResp user = userRespOptional.get();
        return ServiceResult.ok(user);
    }
}
