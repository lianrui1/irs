package cn.com.cowain.ibms.rest.resp.meeting;

import cn.com.cowain.ibms.enumeration.ReservationRecordStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * @author Yang.Lee
 * @date 2021/7/8 13:44
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel("会议记录分页数据响应对象")
public class ReservationPageResp {

    @ApiModelProperty(value = "预约记录id")
    private String reservationId;

    @ApiModelProperty(value = "员工姓名")
    private String name;

    @ApiModelProperty(value = "员工工号")
    private String empNo;

    @ApiModelProperty(value = "部门")
    private String department;

    @ApiModelProperty(value = "会议室名称")
    private String roomName;

    @ApiModelProperty(value = "项目名称")
    private String projectName;

    @ApiModelProperty(value = "空间名称")
    private String spaceName;

    @ApiModelProperty(value = "会议日期")
    private LocalDate meetingDate;

    @JsonFormat(pattern = "HH:mm")
    @ApiModelProperty(value = "会议开始时间")
    private LocalTime startTime;

    @JsonFormat(pattern = "HH:mm")
    @ApiModelProperty(value = "会议结束时间")
    private LocalTime endTime;

    @ApiModelProperty(value = "会议主题")
    private String topic;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "预约时间")
    private LocalDateTime reservationDatetime;

    @ApiModelProperty(value = "会议状态")
    private ReservationRecordStatus status;

    @ApiModelProperty(value = "会议状态名称")
    private String statusName;
}
