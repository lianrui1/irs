package cn.com.cowain.ibms.enumeration.iot;

/**
 * 设备可用状态
 *
 * @author Yang.Lee
 * @date 2021/12/7 13:25
 */
public enum UsableStatus {

    DISABLED("禁用"),

    ENABLE("启用");

    UsableStatus(String name) {
        this.name = name;
    }

    private String name;

    public String getName() {
        return this.name;
    }
}
