package cn.com.cowain.ibms.enumeration.iot;

/**
 * 设备状态
 *
 * @author Yang.Lee
 * @date 2021/2/18 15:58
 */
public enum DeviceStatus {

    /**
     * 设备在线
     */
    ONLINE("在线"),
    /**
     * 设备离线
     */
    OFFLINE("离线"),
    /**
     * 设备异常
     */
    ABNORMAL("异常"),
    /**
     * 设备未激活
     */
    INACTIVE("未激活"),
    /**
     * 设备冻结
     */
    FROZEN("冻结"),

    UNKNOW ("未知"),

    ACTIVE("已激活"),

    OTHER("其他"),

    ACTIVE_FAIL("激活失败")
    ;

    DeviceStatus(String name) {
        this.name = name;
    }

    private String name;

    public String getName() {
        return this.name;
    }
}
