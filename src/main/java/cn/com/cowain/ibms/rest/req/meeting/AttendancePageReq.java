package cn.com.cowain.ibms.rest.req.meeting;

import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2021/7/8 11:21
 */
@Data
@ApiModel("会议签到分页数据请求对象")
public class AttendancePageReq extends PageReq {

    @ApiModelProperty(value = "地区（项目）ID", position = 1)
    private String projectId;

    @ApiModelProperty(value = "会议室ID", position = 2)
    private String roomId;

    @ApiModelProperty(value = "签到状态", position = 3)
    private String signState;

    @ApiModelProperty(value = "搜索关键字（姓名，工号，部门，会议主题）", position = 4)
    private String keyWord = "";
}
