package cn.com.cowain.ibms.dao.iot.lift_column;

import cn.com.cowain.ibms.entity.iot.LiftColumnGroupRelation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 升降柱组DAO
 **/
@Repository
public interface LiftColumnGroupRelationDao extends JpaRepository<LiftColumnGroupRelation, String>, JpaSpecificationExecutor<LiftColumnGroupRelation> {

    /**
     * 根据升降柱组来查询升降柱数据
     *
     * @param groupId 升降柱组ID
     * @return 关系列表
     * @author Yang.Lee
     * @date 2022/6/24 10:48
     **/
    List<LiftColumnGroupRelation> findByColumnGroupId(String groupId);
}
