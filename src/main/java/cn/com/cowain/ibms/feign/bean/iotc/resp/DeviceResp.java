package cn.com.cowain.ibms.feign.bean.iotc.resp;

import cn.com.cowain.ibms.enumeration.IotNodeType;
import cn.com.cowain.ibms.enumeration.iot.DataSource;
import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.utils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author Yang.Lee
 * @date 2021/10/27 15:03
 */
@Data
public class DeviceResp {

    private String hwDeviceId;

    private String code;

    private String deviceName;

    private DeviceStatus workStatus;

    private String joinStatus;

    private String useStatus;

    private String productName;

    private DeviceProperties deviceProperties;

    private DeviceChannelVO deviceChannelVo;

    private String chName;

    private String enName;

    private IotNodeType iotNodeType;

    @JsonFormat(pattern = DateUtils.PATTERN_DATETIME, shape = JsonFormat.Shape.STRING)
    private LocalDateTime changeTime;

    private DeviceType deviceType;

    @Data
    public static class DeviceProperties {

        private int status;

        private String password;

        private String sn;

        private String parentId;

        private String hwDeviceId;

        private String hwDeviceParentId;

        private String hwDeviceName;

        private String hwNodeType;

        private String hwProductId;

        private String hwProductName;

        private String hwGatewayId;

        private String hwStatus;

        private DataSource dataSource;

        private String address;

        private String lastHwStatusChangedDatetime;

        private String ip;

        private String port;

        private String account;

        private String snCode;

        private String uuid;

        private String checkinSite;

        private String deviceSerial;

        private String deviceName;

        private String deviceType;

        private int defence;

        private String deviceVersion;

        private String parentCategory;

        private int riskLevel;

        private String netAddress;

        private String model;

        private String netType;

        private int isEncrypt;

        private int alarmSoundMode;

        private int offlineNotify;

        private String signal;

        /**
         * 视频流地址
         */
        private String rtspUrl;
    }

    @Data
    public static class DeviceChannelVO {
        private String name;

        private String channelType;

        private String channelOrigin;
    }
}
