package cn.com.cowain.ibms.rest.req.space;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author yuchunlei
 * @version 1.0.0
 * @ClassName IotAttributesDictReq.java
 * @Description
 * @createTime 2020年12月21日 13:49:00
 */
@Data
@ApiModel("数据点信息")
public class IotAttributesDictReq implements Serializable {

    @NotBlank(message = "数据点名称不能为空")
    @ApiModelProperty(value = "数据点名称", required = true, position = 0)
    private String name;


}
