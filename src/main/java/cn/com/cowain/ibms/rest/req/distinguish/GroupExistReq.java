package cn.com.cowain.ibms.rest.req.distinguish;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/5/27 9:43
 */
@Data
public class GroupExistReq {
    private String groupName;
}
