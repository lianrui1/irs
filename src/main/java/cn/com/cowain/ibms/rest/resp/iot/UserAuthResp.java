package cn.com.cowain.ibms.rest.resp.iot;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author wei.cheng
 * @date 2022/06/29 09:00
 */
@Data
@ApiModel("权限信息")
public class UserAuthResp {

    @ApiModelProperty("权限id")
    private String authId;

    @ApiModelProperty("权限名")
    private String authName;

    @ApiModelProperty(value = "状态，可选值：`0`可申请，`1`审批中")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String status;

    @ApiModelProperty(value = "状态，可选值：可申请，审批中")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String statusDesc;

    public UserAuthResp(String authId, String authName) {
        this.authId = authId;
        this.authName = authName;
    }
}
