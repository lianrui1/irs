package cn.com.cowain.ibms.enumeration.iot;

/**
 * @author Yang.Lee
 * @date 2022/2/17 15:10
 */
public enum LockType {

    HW_DEVICE_ID("根据设备ID加锁"),

    OTHER("其他");

    LockType(String name){
        this.name = name;
    }
    String name;

    public String getName(){
        return this.name;
    }

}
