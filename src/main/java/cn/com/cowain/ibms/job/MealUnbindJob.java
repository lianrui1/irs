package cn.com.cowain.ibms.job;

import cn.com.cowain.ibms.dao.meal.MealGroupDao;
import cn.com.cowain.ibms.entity.meal.MealGroup;
import cn.com.cowain.ibms.feign.iotc.IOTCApi;
import cn.com.cowain.ibms.feign.iotc.enumeration.MethodCode;
import cn.com.cowain.ibms.feign.iotc.enumeration.ModuleCode;
import cn.com.cowain.ibms.rest.req.iot.GroupBindingReq;
import cn.com.cowain.ibms.rest.req.iot.HtUserGroupAddPersonReq;
import cn.com.cowain.sfp.comm.JsonResult;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author tql
 * @Description 报餐解绑定时任务
 * @Date 22-5-10 下午5:08
 * @Version 1.0
 */
@Slf4j
@Component
public class MealUnbindJob {

    @Resource
    private MealGroupDao mealGroupDao;


    @Resource
    private IOTCApi iotcApi;

    // 凌晨一点执行
   // @Scheduled(cron = "0 0 1 * * ?")
    public void run() {
        log.info("报餐解绑执行中");
        List<MealGroup> memberGroups = mealGroupDao.findAll();
        if (!CollectionUtils.isEmpty(memberGroups)) {
            memberGroups.forEach(value -> {
                // 旷视参数定义
                HtUserGroupAddPersonReq req = HtUserGroupAddPersonReq.builder()
                        .groupUuid(value.getId())
                        .personUuids(new ArrayList<>())
                        .build();

                GroupBindingReq param = GroupBindingReq.builder()
                        .chainSite("KS")
                        .methodCode(MethodCode.BATCH_ADD_PERSON.getName())
                        .moduleCode(ModuleCode.KSHT.getName())
                        .jsonArg(JSON.toJSONString(req))
                        .channelType("2")
                        .build();
                log.debug("向iotc下发人员对象{}", JSON.toJSONString(param));

                // 调用透传接口(旷视)
                JsonResult<Object> resp = iotcApi.postToConn(param);
                if (resp.getStatus() == 0) {
                    // 更新成功
                    log.info("人员解绑失败:{}", resp.getMsg());
                }
            });
        }
    }
}
