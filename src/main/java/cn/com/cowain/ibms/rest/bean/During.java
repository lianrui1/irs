package cn.com.cowain.ibms.rest.bean;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @author wei.cheng
 * @date 2022/09/28 15:41
 */
@Data
public class During implements Serializable {
    /**
     * 开始时间，格式为`HH:mm`
     */
    @NotBlank
    @Pattern(regexp = "^([0-1][0-9]|2[0-3]):([0-5][0-9])$")
    private String from;
    /**
     * 结束时间，格式为`HH:mm`
     */
    @NotBlank
    @Pattern(regexp = "^([0-1][0-9]|2[0-3]):([0-5][0-9])$")
    private String to;
}
