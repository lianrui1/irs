package cn.com.cowain.ibms.rest.resp;

import cn.com.cowain.ibms.rest.bean.EhrDeptBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/4 20:23
 */
@Data
@ApiModel("EHR 部门数据")
public class EhrDeptResp implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("消息")
    private String msg;

    @ApiModelProperty("代号")
    private Integer code;

    @ApiModelProperty("部门列表")
    private List<EhrDeptBean> treeList;

}
