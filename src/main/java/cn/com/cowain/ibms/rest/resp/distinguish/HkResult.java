package cn.com.cowain.ibms.rest.resp.distinguish;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/8/3 18:38
 */
@Data
public class HkResult {

    private int over;

    private int percent;

}
