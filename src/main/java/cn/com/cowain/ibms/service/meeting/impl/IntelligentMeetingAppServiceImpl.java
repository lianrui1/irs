package cn.com.cowain.ibms.service.meeting.impl;
/**
 * Created with IntelliJ IDEA.
 *
 * @author: yanzy
 * @date: 2021/12/29/18:12
 * @version:
 */

import cn.com.cowain.ibms.dao.ReservationRecordDao;
import cn.com.cowain.ibms.dao.ReservationRecordItemDao;
import cn.com.cowain.ibms.dao.RoomDao;
import cn.com.cowain.ibms.dao.meeting.AttendanceDao;
import cn.com.cowain.ibms.dao.meeting.IntelligentMeetingAppDao;
import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.entity.ReservationRecordItem;
import cn.com.cowain.ibms.entity.Room;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.meeting.Attendance;
import cn.com.cowain.ibms.entity.meeting.IntelligentMeetingApp;
import cn.com.cowain.ibms.enumeration.ReservationRecordStatus;
import cn.com.cowain.ibms.enumeration.iot.DoorPlateStatus;
import cn.com.cowain.ibms.rest.req.working_mode.BandingReq;
import cn.com.cowain.ibms.rest.bean.ParticipantBean;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.StaffResp;
import cn.com.cowain.ibms.rest.resp.iot.QrCodeResp;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.ServiceMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.guest.GuestService;
import cn.com.cowain.ibms.service.meeting.IntelligentMeetingAppService;
import cn.com.cowain.ibms.service.space.IotDeviceService;
import cn.com.cowain.ibms.utils.Base64Util;
import cn.com.cowain.ibms.utils.DateUtils;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.RedisUtil;
import cn.com.cowain.ibms.utils.enums.DateUnit;
import cn.com.cowain.ibms.utils.qrcode.QRCodeUtil;
import cn.com.cowain.ibms.websocket.bean.MessageResult;
import cn.com.cowain.ibms.websocket.bean.resp.MeetingAppResp;
import cn.com.cowain.ibms.websocket.enumeration.MessageType;
import cn.com.cowain.ibms.websocket.server.WsServer;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author yanzy
 * @date 2021/12/29 18:12
 */
@Slf4j
@Service
public class IntelligentMeetingAppServiceImpl implements IntelligentMeetingAppService {

    @Resource
    private IntelligentMeetingAppDao intelligentMeetingAppDao;

    @Resource
    private RoomDao roomDao;

    @Resource
    private RedisUtil redisUtil;

    @Value("${cowain.wechat.room_reservation.url-h5}")
    private String qrcodeUrl;
    @Value("${reservation-record.about-to-begin-interval:10}")
    private Long ABOUT_TO_BEGIN_INTERVAL;
    @Resource
    private ReservationRecordDao reservationRecordDao;

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserServiceUC;

    @Resource
    private ReservationRecordItemDao reservationRecordItemDao;

    @Resource
    private AttendanceDao attendanceDao;

    @Resource
    private GuestService guestService;

    @Resource
    private IotDeviceService iotDeviceService;

    /**
     * 会议室设备绑定
     *
     * @author yanzy
     * @date 2021/12/29
     */
    @Override
    @Transactional
    public ServiceResult banding(BandingReq req) {
        Optional<Room> room = roomDao.findById(req.getRoomId());
        if (room.isEmpty()) {
            return ServiceResult.error("绑定失败，会议室不存在");
        }

        Optional<IntelligentMeetingApp> meetingApp = intelligentMeetingAppDao.findBySerialNumber(req.getDeviceId());
        if (meetingApp.isPresent()) {
            IntelligentMeetingApp intelligentMeetingApp = meetingApp.get();
            intelligentMeetingApp.setSerialNumber(req.getDeviceId());
            intelligentMeetingApp.setRoom(room.get());
            intelligentMeetingAppDao.save(intelligentMeetingApp);
            return ServiceResult.ok(new IdResp(intelligentMeetingApp.getId()));
        }
        IntelligentMeetingApp intelligentMeetingApp = new IntelligentMeetingApp();
        intelligentMeetingApp.setSerialNumber(req.getDeviceId());
        intelligentMeetingApp.setRoom(room.get());
        intelligentMeetingAppDao.save(intelligentMeetingApp);
        return ServiceResult.ok(new IdResp(intelligentMeetingApp.getId()));
    }

    /**
     * 会议室设备绑定修改
     *
     * @author yanzy
     * @date 2021/12/30
     */
    @Override
    @Transactional
    public ServiceResult updateBanding(BandingReq req) {

        Optional<Room> roomOp = roomDao.findById(req.getRoomId());
        if (roomOp.isEmpty()) {
            return ServiceResult.error("修改绑定失败，会议室不存在");
        }

        Optional<IntelligentMeetingApp> meetingApp = intelligentMeetingAppDao.findBySerialNumber(req.getDeviceId());
        if (meetingApp.isEmpty()) {
            return ServiceResult.error("修改绑定未绑定无法修改，请重新创建绑定");
        }

        IntelligentMeetingApp intelligentMeetingApp = meetingApp.get();
        intelligentMeetingApp.setSerialNumber(req.getDeviceId());
        intelligentMeetingApp.setRoom(roomOp.get());
        intelligentMeetingAppDao.save(intelligentMeetingApp);
        return ServiceResult.ok(new IdResp(intelligentMeetingApp.getId()));
    }

    /**
     * 二维码生成
     *
     * @author yanzy
     * @date 2021/1/4
     */
    @Override
    @Transactional
    public ServiceResult createQrCode(String roomId) {
        Optional<Room> roomOp = roomDao.findById(roomId);
        if (roomOp.isEmpty()) {
            return ServiceResult.error("二维码生成失败，办公室不存在");
        }

        QrCodeResp resp;
        //创建redisKey
        String redisKey = redisUtil.createRealKey("MEETING_APP_QRCODE" + roomId);
        //判断redis中死否有数据
        if (Boolean.TRUE.equals(redisUtil.hasKey(redisKey))) {
            //reids中有数据,获取数据
            String redisContent = String.valueOf(redisUtil.get(redisKey));
            resp = new QrCodeResp();
            resp.setBase64Content(redisContent);
            return ServiceResult.ok(resp);
        }

        String url = qrcodeUrl + "reservation/temporary?roomId=" + roomId;
        try {
            BufferedImage bufferedImage = QRCodeUtil.encode(url);
            String base64Content = Base64Util.encode(bufferedImage, QRCodeUtil.FORMAT_NAME.toLowerCase());
            resp = new QrCodeResp();
            //添加base64数据
            base64Content = addBase64Head(base64Content, QRCodeUtil.FORMAT_NAME.toLowerCase());
            resp.setBase64Content(base64Content);

            //创建redisValue
            String redisValue = base64Content;

            //将数据存入到redis中
            redisUtil.set(redisKey, redisValue, 60 * 60 * 24L);
            log.info("redis缓存成功,key={}",redisKey);
        } catch (Exception e) {
            log.error("create qrcode error!", e);
            return ServiceResult.error("二维码生成失败");
        }
        return ServiceResult.ok(resp);
    }

    /**
     * 根据会议室ID查询会议详情
     *
     * @param roomId 会议室ID
     * @return 会议详情， 当查询是成功时返回MeetingAppResp对象
     * @author Yang.Lee
     * @date 2022/1/5 15:34
     **/
    @Override
    @Transactional
    public ServiceResult getMeetingAppDetail(String roomId) {

        // 查询room
        Optional<Room> roomOptional = roomDao.findById(roomId);

        if (roomOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_MEETING_ROOM, roomId), ErrConst.E01);
        }

        MeetingAppResp resp = new MeetingAppResp();
        MeetingAppResp.MeetingDetail current;
        MeetingAppResp.MeetingDetail next = null;

        // 根据会议时间和会议室获取会议信息
        LocalDateTime now = LocalDateTime.now();
        List<ReservationRecord> reservationRecordOpList = reservationRecordDao.findByToGreaterThanEqualAndFromLessThanEqualAndRoomIdAndStatusOrderByFromAsc(now, now, roomId, ReservationRecordStatus.DEFAULT);

        Optional<ReservationRecord> reservationRecordOptional = reservationRecordOpList.stream().findFirst();
        current = getMeetingDetail(reservationRecordOptional);

        // 查找下一场会议
        List<ReservationRecord> reservationRecordList = reservationRecordDao.findByFromGreaterThanAndRoomIdAndStatusNotOrderByFromAsc(now, roomId, ReservationRecordStatus.CANCEL);
        Optional<ReservationRecord> nextRecordOp = reservationRecordList.stream().findFirst();
        if (nextRecordOp.isPresent()) {
            ReservationRecord nextRecord = nextRecordOp.get();
            if (DateUtils.betweenTime(now, nextRecord.getFrom(), DateUnit.MINUTE) <= ABOUT_TO_BEGIN_INTERVAL) {
                next = getMeetingDetail(nextRecordOp);
            }
        }

        if (!DoorPlateStatus.FREE.equals(current.getMeetingStatus())) {
            current.setMeetingStatus(DoorPlateStatus.IN_PROCESS);

        }
        current.setMeetingStatusName(current.getMeetingStatus().getDescribe());
        if (next != null && !DoorPlateStatus.FREE.equals(next.getMeetingStatus())) {
            next.setMeetingStatus(DoorPlateStatus.ABOUT_TO_BEGIN);
            next.setMeetingStatusName(next.getMeetingStatus().getDescribe());
        }

        resp.setCurrentMeeting(current);
        resp.setNextMeeting(next);

        log.info("获取会议app信息，{}", resp);

        return ServiceResult.ok(resp);
    }

    private MeetingAppResp.MeetingDetail getMeetingDetail(Optional<ReservationRecord> reservationRecordOptional) {
        MeetingAppResp.MeetingDetail detail = new MeetingAppResp.MeetingDetail();

        if (reservationRecordOptional.isPresent()) {
            ReservationRecord reservationRecord = reservationRecordOptional.get();
            detail.setMeetingId(reservationRecord.getId());
            detail.setTopic(reservationRecord.getTopic());
            detail.setFrom(reservationRecord.getFrom());
            detail.setTo(reservationRecord.getTo());
            detail.setHostHrId(reservationRecord.getInitiatorEmpNo());

            SysUser sysUser = sysUserServiceUC.getUserByEmpNo(reservationRecord.getInitiatorEmpNo());
            detail.setHostName(sysUser.getNameZh());
            detail.setHostImg(sysUser.getHeadImgUrl());
            detail.setHostDepartmentName(sysUser.getDeptName());
            detail.setDuration(getMeetingDuration(reservationRecord.getFrom(), reservationRecord.getTo()));
            detail.setWelcomeWords(Optional.ofNullable(reservationRecord.getWelcomeWords()).orElse(""));

            // 获取参会人信息及签到数据
            Set<ReservationRecordItem> reservationRecordItemSet = reservationRecordItemDao.findByReservationRecordId(reservationRecord.getId());

            Set<String> hrIdSet = reservationRecordItemSet.stream().map(ReservationRecordItem::getOwnerEmpNo).collect(Collectors.toSet());
            List<MeetingAppResp.MeetingMember> noSignList = new ArrayList<>();
            List<MeetingAppResp.MeetingMember> hasSignList = new ArrayList<>();
            List<MeetingAppResp.MeetingMember> hostList = new ArrayList<>();
            hrIdSet.stream().map(obj -> {
                SysUser member = sysUserServiceUC.getUserByEmpNo(obj);
                MeetingAppResp.MeetingMember meetingMember = new MeetingAppResp.MeetingMember();
                meetingMember.setStaff(StaffResp.convert(member));
                // 查询该用户是否已签到
                Optional<Attendance> attendanceOptional = attendanceDao.findByUserEmpNoAndReservationRecordId(member.getEmpNo(), reservationRecord.getId());
                if (attendanceOptional.isPresent()) {
                    meetingMember.setHasSignIn(true);
                    if (member.getEmpNo().equals(detail.getHostHrId())) {
                        hostList.add(meetingMember);
                    } else {
                        hasSignList.add(meetingMember);
                    }
                } else {
                    if (member.getEmpNo().equals(detail.getHostHrId())) {
                        hostList.add(meetingMember);
                    } else {
                        noSignList.add(meetingMember);
                    }
                }

                return meetingMember;
            }).collect(Collectors.toList());
            // 获取访客信息
            List<ParticipantBean> guestList = guestService.getGuest(reservationRecord);
            guestList.stream().map(obj -> {
                MeetingAppResp.MeetingMember meetingMember = new MeetingAppResp.MeetingMember();
                meetingMember.setGuest(true);
                StaffResp staffResp = new StaffResp();
                staffResp.setHrId(obj.getHrId());
                staffResp.setNameZh(obj.getOwnerName());
                meetingMember.setStaff(staffResp);
                // 查询该用户是否已签到
                Optional<Attendance> attendanceOptional = attendanceDao.findByUserEmpNoAndReservationRecordId(obj.getHrId(), reservationRecord.getId());
                if (attendanceOptional.isPresent()) {
                    meetingMember.setHasSignIn(true);
                    if (obj.getHrId().equals(detail.getHostHrId())) {
                        hostList.add(meetingMember);
                    } else {
                        hasSignList.add(meetingMember);
                    }
                } else {
                    if (obj.getHrId().equals(detail.getHostHrId())) {
                        hostList.add(meetingMember);
                    } else {
                        noSignList.add(meetingMember);
                    }
                }

                return meetingMember;

            }).collect(Collectors.toList());
            detail.setMemberList(hostList);
            detail.getMemberList().addAll(hasSignList);
            detail.getMemberList().addAll(noSignList);
        } else {
            detail.setMeetingStatus(DoorPlateStatus.FREE);
        }


        return detail;
    }

    /**
     * 处理app请求消息
     *
     * @param messageType end/extend   结束/延长会议
     * @param cmd         指令
     * @return
     * @author Yang.Lee
     * @date 2022/1/6 10:42
     **/
    @Override
    @Transactional
    public ServiceResult meetingMessage(MessageType messageType, String[] cmd) {

        ServiceResult result;

//        meeting|end|a1a62546-a6bc-4a45-b33c-c659da043f16|closeDevice
        String recordId = cmd[2];

        switch (messageType) {
            case END:

                boolean close =false;
                if(cmd.length > 3 && "closeDevice".equals(cmd[3])){
                    close = true;
                }

                result = endMeeting(recordId, close);
                break;
            case EXTEND:
                // 默认延长10分钟
                result = extendMeeting(recordId, 10);
                break;
            default:
                result = ServiceResult.ok();
                break;

        }

        return result;
    }

    /**
     * 推送会议室信息
     *
     * @param roomId 会议室ID,将会向所有连接到该会议室的设备推送消息
     * @return
     * @author Yang.Lee
     * @date 2022/1/19 9:39
     **/
    @Override
    @Transactional
    public void pushMeetingRoomDetail(String roomId) {
        List<WsServer> wsServerList = WsServer.getServerList(roomId);

        if (CollectionUtils.isEmpty(wsServerList)) {
            log.info("会议室 {} 未连接到任何设备，数据推送结束。", roomId);
            return;
        }

        wsServerList.forEach(obj -> {
            ServiceResult result = getMeetingAppDetail(roomId);
            if (!result.isSuccess()) {
                obj.sendMessage(JSON.toJSONString(MessageResult.error((String) result.getObject(), null, MessageType.MEETING_DETAIL)), obj.getSession());
                obj.close(obj.getSessionId());

            } else {

                obj.sendMessage(JSON.toJSONString(MessageResult.ok((MeetingAppResp) result.getObject(), MessageType.MEETING_DETAIL)), obj.getSession());
            }
        });

    }

    /**
     * 结束会议，若结束后有剩余时间，则释放会议时间
     *
     * @param recordId 会议记录
     * @param closeDevice 是否关闭设备
     * @return 结果
     * @author Yang.Lee
     * @date 2022/1/6 13:30
     **/
    @Transactional
    public ServiceResult endMeeting(String recordId, boolean closeDevice) {

        Optional<ReservationRecord> reservationRecordOptional = reservationRecordDao.findById(recordId);

        if (reservationRecordOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_MEETING_RESERVATION, recordId));
        }

        ReservationRecord reservationRecord = reservationRecordOptional.get();
        reservationRecord.setStatus(ReservationRecordStatus.FINISH);
        // 会议结束后将结束时间改为当前时间
        reservationRecord.setTo(LocalDateTime.now());

        reservationRecordDao.save(reservationRecord);

        if(closeDevice){
            // 关闭空间下所有设备
            iotDeviceService.closeSpaceDevices(reservationRecord.getRoom().getSpace());
        }

        return ServiceResult.ok();
    }

    /**
     * 延长会议
     *
     * @param recordId 会议记录
     * @param min      延长的时间，单位：分钟
     * @return
     * @author Yang.Lee
     * @date 2022/1/6 13:31
     **/
    @Transactional
    public ServiceResult extendMeeting(String recordId, long min) {

        Optional<ReservationRecord> reservationRecordOptional = reservationRecordDao.findById(recordId);

        if (reservationRecordOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_MEETING_RESERVATION, recordId));
        }

        ReservationRecord reservationRecord = reservationRecordOptional.get();
        reservationRecord.setTo(reservationRecord.getTo().plusMinutes(min));

        reservationRecordDao.save(reservationRecord);

        return getMeetingAppDetail(reservationRecord.getRoom().getId());
    }

    /**
     * 为图片文件添加头信息
     *
     * @param imgStr
     * @param format
     * @return
     * @author yanzy
     */
    private String addBase64Head(String imgStr, String format) {
        StringBuilder header = new StringBuilder("data:image/" + format + ";base64,");
        return header.append(imgStr).toString();
    }

    /**
     * 获取会议持续时间
     *
     * @param from 会议开始时间
     * @param to   会议结束时间
     * @return 会议持续时间，e.m.: 1.5h
     * @author Yang.Lee
     * @date 2022/1/6 15:48
     **/
    private String getMeetingDuration(LocalDateTime from, LocalDateTime to) {

        Long durationMin = DateUtils.betweenTime(from, to, DateUnit.MINUTE);

        double hourDouble = new BigDecimal(durationMin).divide(new BigDecimal(60), 1, RoundingMode.HALF_UP).doubleValue();

        return hourDouble + "h";
    }

}
