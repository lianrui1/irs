package cn.com.cowain.ibms.rest.resp.meeting;

import lombok.Data;

import java.io.Serializable;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/12 19:32
 */
@Data
public class CustomUserGroupResp2 implements Serializable {

    private String id;

    private String name;
}
