package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.IotDeviceNetworkChange;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/5/6 14:01
 */
@Repository
public interface IotDeviceNetworkChangeDao extends JpaRepository<IotDeviceNetworkChange, String>, JpaSpecificationExecutor<IotDeviceNetworkChange> {

    Page<IotDeviceNetworkChange> findAllByDeviceId(String id, Pageable pageable);

    /**
     * 查询deviceId = {@code deviceId}的IotDeviceNetworkChange
     *
     * @param deviceId
     * @return
     */
    List<IotDeviceNetworkChange> findAllByDeviceId(String deviceId);
}
