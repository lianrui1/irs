package cn.com.cowain.ibms.rest.req.iot;

import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/26 13:27
 */
@Data
@ApiModel("门禁点人员列表请求对象")
public class SpotUserPageReq extends PageReq {

    @ApiModelProperty(value = "人员姓名/工号")
    private String keyword;

    @ApiModelProperty(value = "门禁点ID")
    private String spotId;
}
