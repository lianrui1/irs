package cn.com.cowain.ibms.entity.office;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author Yang.Lee
 * @date 2021/8/27 16:11
 */
@Data
@Entity
@Table(name = "iot_devcie_default_mode_status")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_devcie_default_mode_status", comment = "设备工作模式对应设备表")
public class DeviceModeDefaultStatus extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "default_mode", nullable = false, columnDefinition = "varchar(36) COMMENT '工作模式'")
    private DeviceWorkingDefaultMode mode;

    @Enumerated(EnumType.STRING)
    @Column(name = "device_type", columnDefinition = "varchar(50) COMMENT '设备类型'")
    private DeviceType deviceType;

    @Column(name = "properties", nullable = false, columnDefinition = "varchar(500) COMMENT '设备属性'")
    private String properties;

}
