package cn.com.cowain.ibms.rest.resp.space;

import cn.com.cowain.ibms.entity.iot.Properties;
import cn.com.cowain.ibms.enumeration.iot.SpaceType;
import cn.com.cowain.ibms.rest.resp.StaffResp;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Yang.Lee
 * @date 2020/12/17 14:35
 */
@Data
@ApiModel("空间信息响应对象")
public class SpaceTreeResp {

    @ApiModelProperty(value = "主键ID", required = true)
    private String id;

    @ApiModelProperty(value = "空间名称", required = true, position = 1)
    private String name;

    @ApiModelProperty(value = "空间层级", required = true, position = 2)
    private int level;

    @ApiModelProperty(value = "所属项目ID", required = true, position = 3)
    private String projectId;

    @ApiModelProperty(value = "所属项目名称", required = true, position = 4)
    private String projectName;

    @ApiModelProperty(value = "备注", position = 5)
    private String remark;

    @ApiModelProperty(value = "空间编号", position = 6)
    private String number;

    @ApiModelProperty(value = "父节点ID", position = 7)
    private String parentId;

    @ApiModelProperty(value = "子节点列表", dataType="SpaceTreeResp", position = 8)
    private List<SpaceTreeResp> childrenList;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "创建时间",  position = 9)
    private LocalDateTime createdTime;

    @ApiModelProperty(value = "空间设备数", position = 10)
    private int deviceNum;

    @ApiModelProperty(value = "空间类型 DEFAULT(默认)|ROOM(房间)|BUILDING(建筑)|GROUND(场地)|FLOOR(楼层)|BUILDINGS(楼宇)|GREEN_SPACE(绿化)|OTHERS(其他)")
    private SpaceType spaceType;

    @ApiModelProperty(value = "空间类型名称")
    private String spaceTypeName;

    @ApiModelProperty(value = "空间属性")
    private Properties properties;

    @ApiModelProperty(value = "简介")
    private String introduction;

    @ApiModelProperty(value = "图片")
    private String img;

    @ApiModelProperty(value = "管理员列表", required = true, position = 23)
    private List<StaffResp> adminList;

    @ApiModelProperty(value = "父节点名称", position = 24)
    private String parentName;

    @ApiModelProperty(value = "空间楼层")
    private String floor;
}
