package cn.com.cowain.ibms.rest.resp.meeting;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/8 19:25
 */
@Data
public class AcceptResp {

    @ApiModelProperty(value = "邀请函单号", required = true, example = "", position = 1)
    private String visitorRecordNo;

    @ApiModelProperty(value = "访客工号", required = true, example = "", position = 1)
    private String jobNum;
}
