package cn.com.cowain.ibms.feign.iotc.bean;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author Yang.Lee
 * @date 2022/2/17 14:34
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RemoveHTMemberFromGroupReq {

    @NotBlank
    @ApiModelProperty(value = "用户组业务id", required = true)
    private String groupUuid;

    @NotEmpty
    @ApiModelProperty(value = "工号列表", required = true)
    private List<String> personUuids;
}
