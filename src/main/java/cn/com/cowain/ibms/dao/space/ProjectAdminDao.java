package cn.com.cowain.ibms.dao.space;

import cn.com.cowain.ibms.entity.space.ProjectAdmin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/11/11 13:19
 */
public interface ProjectAdminDao extends JpaRepository<ProjectAdmin, String>, JpaSpecificationExecutor<ProjectAdmin> {

    /**
     * 根据项目查询管理员信息
     *
     * @param projectId 项目ID
     * @return 项目- 管理员列表
     * @author Yang.Lee
     * @date 2021/11/11 17:15
     **/
    List<ProjectAdmin> findByProjectId(String projectId);
}
