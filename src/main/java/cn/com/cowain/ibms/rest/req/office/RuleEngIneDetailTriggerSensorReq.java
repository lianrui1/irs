package cn.com.cowain.ibms.rest.req.office;

import cn.com.cowain.ibms.enumeration.RuleEngIneDetailTriggerDeviceCondition;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 传感器设备
 *
 * @author: yanzy
 * @date: 2022/2/14 19:17
 */
@Data
@ApiModel("传感器设备")
public class RuleEngIneDetailTriggerSensorReq {

    @ApiModelProperty(value = "属性")
    private String key;

    @ApiModelProperty(value = "值  或 (差:4 中:3 良:2 优:1) 或 true or false")
    private String value;

    @ApiModelProperty(value = "条件")
    private RuleEngIneDetailTriggerDeviceCondition deviceCondition;

}
