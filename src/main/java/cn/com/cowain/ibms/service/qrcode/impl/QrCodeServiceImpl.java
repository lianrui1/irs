package cn.com.cowain.ibms.service.qrcode.impl;

import cn.com.cowain.ibms.dao.ReservationRecordDao;
import cn.com.cowain.ibms.dao.ReservationRecordItemDao;
import cn.com.cowain.ibms.dao.RoomDao;
import cn.com.cowain.ibms.dao.iot.*;
import cn.com.cowain.ibms.dao.meeting.GuestDetailDao;
import cn.com.cowain.ibms.dao.meeting.InvitationCardDao;
import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.entity.ReservationRecordItem;
import cn.com.cowain.ibms.entity.Room;
import cn.com.cowain.ibms.entity.iot.*;
import cn.com.cowain.ibms.entity.meeting.GuestDetail;
import cn.com.cowain.ibms.entity.meeting.InvitationCard;
import cn.com.cowain.ibms.enumeration.ReservationRecordStatus;
import cn.com.cowain.ibms.enumeration.iot.DoorMagneticSource;
import cn.com.cowain.ibms.enumeration.iot.DoorPlateStatus;
import cn.com.cowain.ibms.enumeration.meeting.DoorMagneticAccessResult;
import cn.com.cowain.ibms.enumeration.meeting.UserType;
import cn.com.cowain.ibms.rest.req.iot.QrCodeReq;
import cn.com.cowain.ibms.rest.resp.iot.QrCodeResp;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.Guest;
import cn.com.cowain.ibms.service.bean.ServiceMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.guest.GuestService;
import cn.com.cowain.ibms.service.iot.DoorMagneticService;
import cn.com.cowain.ibms.service.iot.DoorPlateService;
import cn.com.cowain.ibms.service.meeting.AttendanceService;
import cn.com.cowain.ibms.service.qrcode.QrCodeService;
import cn.com.cowain.ibms.service.qrcode.QrcodeAccessRuleService;
import cn.com.cowain.ibms.utils.Base64Util;
import cn.com.cowain.ibms.utils.DateUtils;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.RedisUtil;
import cn.com.cowain.ibms.utils.enums.DateUnit;
import cn.com.cowain.ibms.utils.qrcode.QRCodeUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.awt.image.BufferedImage;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Yang.Lee
 * @date 2020/12/2 13:50
 */
@Slf4j
@Service
public class QrCodeServiceImpl implements QrCodeService {

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private DoorPlateDao doorPlateDao;

    @Autowired
    private ReservationRecordDao reservationRecordDao;

    @Autowired
    private ReservationRecordItemDao reservationRecordItemDao;

    @Autowired
    private DoorPlateService doorPlateService;

    @Autowired
    private DoorMagneticDao doorMagneticDao;

    @Resource
    private DoorMagneticService doorMagneticService;

    @Autowired
    private DeviceToSpaceDao deviceToSpaceDao;

    @Autowired
    private RoomDao roomDao;

    @Resource
    private QrcodeAccessRuleService qrcodeAccessRuleService;

    @Resource
    private AttendanceService attendanceService;

    @Resource
    private GuestDetailDao guestDetailDao;

    @Resource
    private InvitationCardDao invitationCardDao;

    @Resource
    private AccessControlSpotDao accessControlSpotDao;

    @Resource
    private AccessControlSpotUserDao accessControlSpotUserDao;

    @Resource
    private GuestService guestService;

    /**
     * 二维码有效期
     */
    @Value("${cowain.doorplate.qrcode.expireTime}")
    private int expiredTime;

    private static final String NO_PERMISSION = "无开门权限";

    private static final String NO_DOORPLATE = "门牌ID不存在";

    @Value("${cowain.doorplate.qrcode.url}")
    private String qrcodeUrl;

    @Value("${wechat.appId}")
    private String appId;

    /**
     * 创建普通二维码
     *
     * @param doorPlateId
     * @return
     */
    @Override
    public ServiceResult create(String doorPlateId) {

        log.debug("create qrcode, doorplateId: " + doorPlateId);
        Optional<DoorPlate> doorPlateOp = doorPlateDao.findById(doorPlateId);
        if (!doorPlateOp.isPresent()) {
            return ServiceResult.error(NO_DOORPLATE);
        }

        DoorPlate doorPlate = doorPlateOp.get();
        // 查询门磁信息，判断静态二维码还是动态二维码
        Optional<DoorMagnetic> doorMagneticOptional = doorMagneticDao.findByNumber(doorPlate.getMagneticNum());
        if (doorMagneticOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.NOT_FOUNT_DOOR_MAGNETISM);
        }

        QrCodeResp resp;
        StringBuilder content = new StringBuilder();
        DoorMagnetic doorMagnetic = doorMagneticOptional.get();
        if (DoorMagneticSource.EHR.equals(doorMagnetic.getDoorMagneticSource())) {
            content = new StringBuilder(qrcodeUrl);
            content.append("?uuid=1")
                    .append("&doorplateid=")
                    .append(doorPlateId)
                    .append("&dm=" + doorMagnetic.getNumber());
            log.debug("生成静态二维码，二维码地址：{}", content);
        } else if (DoorMagneticSource.HW_CLOUD.equals(doorMagnetic.getDoorMagneticSource())) {
            // 从redis中查看是否有数据
            String redisKey = redisUtil.createRealKey(doorPlateId);
            log.debug("create redis key :" + redisKey);
            String uuid = UUID.randomUUID().toString();

            long currentTime = System.currentTimeMillis();
            long redisTime = currentTime;

            // 判断redis中是否已有数据
            if (Boolean.TRUE.equals(redisUtil.hasKey(redisKey))) {
                log.debug("redis 里存在二维码信息");
                // redis中有数据，获取redis数据
                String redisContent = String.valueOf(redisUtil.get(redisKey));
                String[] contentParam = redisContent.split("/");

                // redis中二维码时间
                long time = Long.parseLong(contentParam[1]);
                // redis中数据未过期，不需要新生成二维码
                if (currentTime - time <= expiredTime) {
                    log.debug("不用生成新的二维码，二维码剩余有效时间 ：" + (expiredTime - (currentTime - time)) / 1000 + " 秒");
                    // 获取redis中的uuid
                    uuid = contentParam[0];
                    redisTime = time;
                } else {
                    // redis数据已过期，则删除旧数据
                    redisUtil.del(doorPlateId);
                    uuid = UUID.randomUUID().toString();
                    log.debug("uuid过期，生成新的uuid： " + uuid);
                }
            }

            content = new StringBuilder(qrcodeUrl);
            content.append("?uuid=")
                    .append(uuid)
                    .append("&doorplateid=")
                    .append(doorPlateId)
                    .append("&dm=" + doorMagnetic.getNumber());

            // 将数据存放到redis中
            String redisValue = uuid + "/" + redisTime;
            redisUtil.set(redisKey, redisValue);
            log.debug("二维码生成成功");
            log.debug("uuid : " + uuid);
        }

        String url = createWeChatRedirectUrl(content.toString());
        try {
            BufferedImage bufferedImage = QRCodeUtil.encode(url);
            String base64Content = Base64Util.encode(bufferedImage, QRCodeUtil.FORMAT_NAME);
            // 数据输出对象
            resp = new QrCodeResp();
            // 将base64数据添加头信息
            base64Content = addBase64Head(base64Content, QRCodeUtil.FORMAT_NAME.toLowerCase());
            resp.setBase64Content(base64Content);
        } catch (Exception e) {
            // 图片生成失败，打印异常
            log.error("create qrcode error!", e);
            return ServiceResult.error("二维码生成失败");
        }

        return ServiceResult.ok(resp);
    }

    /**
     * 扫码开门
     *
     * @param qrCodeReq
     * @return
     * @author Yang.Lee
     */
    @Override
    @Transactional
    public ServiceResult scan(QrCodeReq qrCodeReq) {

        log.debug("scan qrcode , doorplateId :" + qrCodeReq.getDoorPlateId());
        // 参数验证
        ServiceResult checkResult = checkScanParams(qrCodeReq);
        if (!checkResult.isSuccess()) {
            log.debug("开门数据校验失败：" + checkResult.getObject());
            return checkResult;
        }
        Optional<DoorPlate> doorPlateOp = doorPlateDao.findById(qrCodeReq.getDoorPlateId());
        if (!doorPlateOp.isPresent()) {
            return ServiceResult.error(NO_DOORPLATE);
        }
        DoorPlate doorPlate = doorPlateOp.get();
        Optional<DoorMagnetic> doorMagneticOp = doorMagneticDao.findByNumber(doorPlate.getMagneticNum());
        if (!doorMagneticOp.isPresent()) {
            return ServiceResult.error("找不到门磁信息");
        }

        // 通过doorPlateId获取当天会议信息
        Date now = new Date();
        Instant instant = now.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        LocalDateTime nowTime = LocalDateTime.ofInstant(instant, zone);

        // 查询会议室room信息
        String roomId = null;
        Optional<DeviceToSpace> d2s = deviceToSpaceDao.findByDeviceId(doorPlate.getDevice().getId());
        if (d2s.isPresent()) {
            Optional<Room> roomOp = roomDao.findBySpaceId(d2s.get().getSpace().getId());
            if (roomOp.isPresent()) {
                roomId = roomOp.get().getId();
            }
        }

        List<ReservationRecord> resultList = reservationRecordDao.findByDateAndRoomIdOrderByFromAsc(nowTime.toLocalDate(), roomId);
        // 根据会议状态进行判断， 如果状体为空闲/进行中/即将开始且无马上开始的下一场会议

        DoorPlateStatus status = DoorPlateStatus.FREE;
        ReservationRecord rr = null;
        ReservationRecord currentRecord = null;
        ReservationRecord nextRecord = null;

        resultList = resultList.stream()
                .filter(obj -> !obj.getStatus().equals(ReservationRecordStatus.CANCEL))
                .collect(Collectors.toList());

        for (int i = 0; i < resultList.size(); i++) {

            // 当前会议数据
            rr = resultList.get(i);

            ReservationRecord next = null;
            if (i < resultList.size() - 1) {
                next = resultList.get(i + 1);
            }

            // 获取会议状态
            status = doorPlateService.checkCurrentRoomStatus(rr, next);

            if (status.equals(DoorPlateStatus.END)) {
                continue;
            }

            if (!status.equals(DoorPlateStatus.FREE)) {
                // 非空闲状态，获取当前会议信息，并且判断是否有紧跟的下场会议
                currentRecord = rr;      // 当前会议数据
                boolean hasNext = (resultList.size() - i) > 0;
                if (Boolean.TRUE.equals(hasNext) && next != null) {
                    // 判断下一场有没有被取消
                    boolean isNotCancel = !next.getStatus().equals(ReservationRecordStatus.CANCEL);
//                    boolean isFollow = next.getFrom().isEqual(rr.getTo());
//                    boolean isGetNextRecord = isNotCancel && isFollow;
                    if (Boolean.TRUE.equals(isNotCancel)) {
                        nextRecord = new ReservationRecord();    // 下场会议数据
                        BeanUtils.copyProperties(next, nextRecord);
                    }
                }
                break;
            }
        }

        boolean isValid = checkScanner(qrCodeReq.getEmpNo(), status, currentRecord, nextRecord);
        boolean inQrcodeGroup = false;

        DoorMagnetic doorMagnetic = doorMagneticOp.get();
        // 静态二维码判断权限组
        if (DoorMagneticSource.EHR.equals(doorMagnetic.getDoorMagneticSource())) {
            ServiceResult authCheck = qrcodeAccessRuleService.checkUserAuth(doorMagnetic.getNumber(), qrCodeReq.getEmpNo());
            inQrcodeGroup = authCheck.isSuccess();
        }

        UserType userType = SysUserService.isVisitor(qrCodeReq.getEmpNo()) ? UserType.GUEST : UserType.STAFF;
        if (!isValid && !inQrcodeGroup) {
            doorMagneticService.saveAccessRecord(qrCodeReq.getEmpNo(), doorMagnetic.getId(), userType, DoorMagneticAccessResult.NO_ACCESS);
            // 数据验证失败发送开门信息
            return ServiceResult.error(NO_PERMISSION);
        }
        // 记录通行结果
        doorMagneticService.saveAccessRecord(qrCodeReq.getEmpNo(), doorMagnetic.getId(), userType, DoorMagneticAccessResult.ACCESS);

        //签到
        meetingSign(roomId, qrCodeReq.getEmpNo());

        // 发送开门信号
        return doorMagneticService.open(doorPlate.getMagneticNum(), qrCodeReq.getEmpNo());
    }

    /**
     * 会议签到
     *
     * @param roomId   会议室ID
     * @param userHrId 用户工号
     * @return 签到结果
     * @author Yang.Lee
     * @date 2022/1/19 10:54
     **/
    @Override
    @Transactional
    public ServiceResult meetingSign(String roomId, String userHrId) {

        Optional<Room> roomOptional = roomDao.findById(roomId);
        if (roomOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_MEETING_ROOM, roomId), ErrConst.E01);
        }

        // 找到会议室当前的会议
        Set<ReservationRecord> reservationRecordSet = reservationRecordDao.findByDateGreaterThanEqualAndRoomId(LocalDate.now(), roomId);

        LocalDateTime now = LocalDateTime.now();

        List<ReservationRecord> reservationRecordList = reservationRecordSet.stream().filter(obj -> {

            boolean status = ReservationRecordStatus.CANCEL.equals(obj.getStatus()) || ReservationRecordStatus.FINISH.equals(obj.getStatus());
            long t1 = DateUtils.betweenTime(now, obj.getTo(), DateUnit.MINUTE);
            long t2 = DateUtils.betweenTime(now, obj.getFrom(), DateUnit.MINUTE);
            boolean time = t1 > 0 && t2 < 0;
            boolean time1 = t2 > -10;

            return !status && time || time1;
        }).collect(Collectors.toList());
        reservationRecordList.forEach(obj -> attendanceService.signIn(obj, userHrId));

        return ServiceResult.ok();
    }


    /**
     * 验证扫码人身份
     *
     * @param empNo         工号
     * @param status
     * @param currentRecord
     * @param nextRecord
     * @return
     */
    @Override
    public boolean checkScanner(String empNo, DoorPlateStatus status, ReservationRecord currentRecord, ReservationRecord nextRecord) {

        // 2022-08-18  门牌新需求。空闲状态不能开门
        if (status.equals(DoorPlateStatus.FREE)) {
            return false;
        }
        boolean result = false;
        // 判断当前场
        boolean currentValid = auth(currentRecord, empNo);
        if (status.equals(DoorPlateStatus.NEXT_ABOUT_TO_BEGIN)) {
            // 判断下一场
            boolean nextValid = auth(nextRecord, empNo);
            result = currentValid || nextValid;
        } else {
            result = currentValid;
        }

        return result;
    }

    /**
     * 扫码人身份检测
     *
     * @param reservationRecord
     * @param empNo
     * @return
     */
    private boolean auth(ReservationRecord reservationRecord, String empNo) {

        if (reservationRecord == null) {
            // 2022-08-18 修改逻辑。 当前无会议不允许开门
            return false;
        }
        // 判断免打扰
        Boolean isDisturb = reservationRecord.getNoDisturb();
        log.debug("record : id = " + reservationRecord.getId() + ",  from " + reservationRecord.getFrom() + " to " + reservationRecord.getTo() + " , noDisturb :" + reservationRecord.getNoDisturb());
        // 判断是否参会人
        if (Boolean.TRUE.equals(isDisturb)) {
            log.debug("empNo : " + empNo);
            return isStaffJoin(reservationRecord.getId(), empNo);
        }

        return true;
    }

    /**
     * 判断扫码开门参数合法性
     *
     * @param qrCodeReq
     * @return
     */
    @Transactional
    public ServiceResult checkScanParams(QrCodeReq qrCodeReq) {

        // 检查doorPlateId是否存在
        String doorPlateId = qrCodeReq.getDoorPlateId();
        Optional<DoorPlate> doorPlateOp = doorPlateDao.findById(doorPlateId);
        if (!doorPlateOp.isPresent()) {
            return ServiceResult.error(NO_DOORPLATE);
        }

        Optional<DoorMagnetic> doorMagneticOp = doorMagneticDao.findByNumber(doorPlateOp.get().getMagneticNum());
        if (!doorMagneticOp.isPresent()) {
            return ServiceResult.error("找不到门磁信息");
        }
        // EHR的静态二维码不做失效判断
        if (DoorMagneticSource.EHR.equals(doorMagneticOp.get().getDoorMagneticSource())) {
            return ServiceResult.ok();
        }

        String redisKey = redisUtil.createRealKey(doorPlateId);
        String content = String.valueOf(redisUtil.get(redisKey));
        String[] contentParam = content.split("/");
        if (contentParam.length < 2) {
            log.debug("redis中二维码数据：" + content);
            return ServiceResult.error("开门失败，未找到门牌二维码信息");
        }
        // 验证uuid
        String redisUuid = contentParam[0];
        if (!qrCodeReq.getUuid().equals(redisUuid)) {
            log.debug("请求uuid:" + qrCodeReq.getUuid());
            log.debug("redis中uuid:" + redisUuid);
            return ServiceResult.error("二维码已更新，请重新扫码");
        }
        // 验证二维码有效期
        long redisTime = Long.parseLong(contentParam[1]);
        long currentTime = System.currentTimeMillis();
        if (currentTime - redisTime > expiredTime) {
            return ServiceResult.error("二维码已过期");
        }

        return ServiceResult.ok();
    }

    /**
     * 判断员工是否是参会人员
     *
     * @param recordId 会议id
     * @param empNo    员工工号
     * @return true: 该员工是参会人员; false: 该员工不是参会人员
     * @author Yang.Lee
     */
    @Override
    @Transactional
    public boolean isStaffJoin(String recordId, String empNo) {

        boolean result = false;
        // 根据会议id查询出所有的参会人员
        Set<ReservationRecordItem> reservationRecordItemSet = reservationRecordItemDao.findByReservationRecordId(recordId);
        // 遍历参与人列表，判断员工是否存在列表中
        for (ReservationRecordItem reservationRecordItem : reservationRecordItemSet) {
            if (empNo.equals(reservationRecordItem.getOwnerEmpNo())) {
                result = true;
                break;
            }
        }
        return result;
    }

    /**
     * 扫描静态二维码开门
     *
     * @param doorMagnetismNo 门磁编号
     * @param empNo           用户工号
     * @return 开门结果
     * @author Yang.Lee
     * @date 2021/3/22 15:24
     **/
    @Override
    @Transactional
    public ServiceResult scanStaticQRCode(String doorMagnetismNo, String empNo) {

        // 验证用户权限
        ServiceResult authCheck = qrcodeAccessRuleService.checkUserAuth(doorMagnetismNo, empNo);

        Optional<DoorMagnetic> doorMagneticOptional = doorMagneticDao.findByNumber(doorMagnetismNo);
        if (!doorMagneticOptional.isPresent()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUNT_DOOR_MAGNETISM, doorMagnetismNo));
        }

        UserType userType = SysUserService.isVisitor(empNo) ? UserType.GUEST : UserType.STAFF;
        if (!authCheck.isSuccess()) {

            doorMagneticService.saveAccessRecord(empNo, doorMagneticOptional.get().getId(), userType, DoorMagneticAccessResult.NO_ACCESS);
            return authCheck;
        }

        doorMagneticService.saveAccessRecord(empNo, doorMagneticOptional.get().getId(), userType, DoorMagneticAccessResult.ACCESS);
        // 开门
        return doorMagneticService.open(doorMagnetismNo, empNo);
    }

    /**
     * 访客扫码开门
     *
     * @param qrCodeReq 请求参数
     * @return 开门结果
     * @author Yang.Lee
     * @date 2021/7/1 15:31
     **/
    @Override
    @Transactional
    public ServiceResult guestScan(QrCodeReq qrCodeReq) {

        Optional<DoorPlate> doorPlateOptional = doorPlateDao.findById(qrCodeReq.getDoorPlateId());
        if (doorPlateOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_DOORPLATE, qrCodeReq.getDoorPlateId()));
        }

        DoorPlate doorPlate = doorPlateOptional.get();
        Optional<DoorMagnetic> doorMagneticOptional = doorMagneticDao.findByNumber(doorPlate.getMagneticNum());

        if (doorMagneticOptional.isEmpty()) {
            return ServiceResult.error("无法找到门磁信息");
        }

        DoorMagnetic doorMagnetic = doorMagneticOptional.get();

        Optional<DeviceToSpace> d2sOptional = deviceToSpaceDao.findByDeviceId(doorPlate.getDevice().getId());
        if (d2sOptional.isEmpty()) {
            return ServiceResult.error("门牌未关联会议室，不允许访客开门");
        }

        String roomId = d2sOptional.get().getSpace().getId();
        Optional<Room> roomOptional = roomDao.findBySpaceId(roomId);
        if (roomOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_MEETING_ROOM, roomId));
        }

        Room room = roomOptional.get();

        // 1. 查询对应会议室当天所有会议
        List<ReservationRecord> reservationRecordList = reservationRecordDao.findByDateAndRoomIdOrderByFromAsc(LocalDate.now(), room.getId());

        ReservationRecord current = null;
        List<ReservationRecord> unStartList = new ArrayList<>();
        LocalDateTime now = LocalDateTime.now();

        for (ReservationRecord reservationRecord : reservationRecordList) {
            // 过滤已取消会议， 已完成会议， 内部会议
            if (ReservationRecordStatus.CANCEL.equals(reservationRecord.getStatus())
                    || reservationRecord.getTo().isBefore(now)
            ) {
                continue;
            }

            if (now.isAfter(reservationRecord.getFrom()) && now.isBefore(reservationRecord.getTo())) {
                current = reservationRecord;
                break;
            }

            if (now.isBefore(reservationRecord.getFrom())) {
                unStartList.add(reservationRecord);
            }
        }

        if (current != null) {
            // 当前有会议，判断是否被会议邀请
            if (!isGuestInReservation(current, qrCodeReq.getGuestMobile())) {

                doorMagneticService.saveGuestAccessRecord(qrCodeReq.getGuestMobile(), doorMagnetic.getId(), DoorMagneticAccessResult.NO_ACCESS);
                return ServiceResult.error(ServiceMessage.PERMISSION_DENIED);
            }

        } else if (!unStartList.isEmpty()) {
            // 当前无会议，但是将来有会议，判断下一场会议是否在5分钟内开始。  5分钟内开始的下场会议判断访客权限
            ReservationRecord next = unStartList.get(0);

            // 开会前5分钟可扫码
            if (DateUtils.betweenTime(now, next.getFrom(), DateUnit.MINUTE) > 5) {
                return ServiceResult.error("会议开始前5分钟可扫码进入");
            }
            // 判断是否会议被邀请
            if (!isGuestInReservation(next, qrCodeReq.getGuestMobile())) {
                doorMagneticService.saveGuestAccessRecord(qrCodeReq.getGuestMobile(), doorMagnetic.getId(), DoorMagneticAccessResult.NO_ACCESS);
                return ServiceResult.error(ServiceMessage.PERMISSION_DENIED);
            }
        }

        if(current == null && unStartList.isEmpty()){
            doorMagneticService.saveGuestAccessRecord(qrCodeReq.getGuestMobile(), doorMagnetic.getId(), DoorMagneticAccessResult.NO_ACCESS);
            return ServiceResult.error(ServiceMessage.PERMISSION_DENIED);
        }

        // 访客签到
        // 1. 通过会议找到邀请函
        // 2. 通过邀请函和手机号码找到访客工号
        String mobile = qrCodeReq.getGuestMobile();
        String hrId = "";
        Optional<InvitationCard> invitationCardOptional = invitationCardDao.findByReservationRecord(current);
        if (invitationCardOptional.isPresent()) {

            InvitationCard invitationCard = invitationCardOptional.get();
            Optional<GuestDetail> guestDetailOptional = guestDetailDao.findByInvitationAndMobile(invitationCard, mobile);

            if (guestDetailOptional.isPresent() && current != null) {
                hrId = guestDetailOptional.get().getJobNum();
                meetingSign(current.getRoom().getId(), hrId);
            }
        }

        doorMagneticService.saveGuestAccessRecord(qrCodeReq.getGuestMobile(), doorMagnetic.getId(), DoorMagneticAccessResult.ACCESS);
        // 验证通过，开门
        return doorMagneticService.open(doorPlate.getMagneticNum(), hrId);
    }

    /**
     * 访客扫码开门（静态码）
     *
     * @param qrCodeReq 二维码信息
     * @return 开门结果
     * @author Yang.Lee
     * @date 2021/12/14 9:29
     **/
    @Override
    @Transactional
    public ServiceResult gustScanStaticQrcode(QrCodeReq qrCodeReq) {
        log.debug("进入访客开静态二维码方法");

        // 静态二维码,根据dm查询门磁信息
        Optional<DoorMagnetic> doorMagneticOptional = doorMagneticDao.findByNumber(qrCodeReq.getDm());
        if (doorMagneticOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUNT_DOOR_MAGNETISM, qrCodeReq.getDm()));
        }

        DoorMagnetic doorMagnetic = doorMagneticOptional.get();

        // 根据设备查询门禁点信息，
        Optional<AccessControlSpot> accessControlSpotOptional = accessControlSpotDao.findByDeviceId(doorMagnetic.getDevice().getId());
        if (accessControlSpotOptional.isEmpty()) {
            return ServiceResult.error("该门磁未绑定门禁点，无法开门");
        }

        Optional<Guest> guestOptional = guestService.getByMobile(qrCodeReq.getGuestMobile());
        if (guestOptional.isEmpty()) {
            doorMagneticService.saveGuestAccessRecord(qrCodeReq.getGuestMobile(), doorMagnetic.getId(), DoorMagneticAccessResult.NO_ACCESS);
            return ServiceResult.error("未找到访客手机号码{}，开门失败。", qrCodeReq.getGuestMobile());
        }

        Guest guest = guestOptional.get();
        List<AccessControlSpotUser> spotUserList = accessControlSpotUserDao.findAccessControlSpotUserByUserHrIdAndAccessControlSpot(guest.getHrId(),accessControlSpotOptional.get());
        if (spotUserList.isEmpty()) {
            doorMagneticService.saveGuestAccessRecord(guest.getHrId(), guest.getName(), doorMagnetic.getId(), DoorMagneticAccessResult.NO_ACCESS);
            return ServiceResult.error(ServiceMessage.PERMISSION_DENIED);
        }

        boolean access = false;
        LocalDateTime now = LocalDateTime.now();
        for (AccessControlSpotUser accessControlSpotUser : spotUserList) {

            if (now.isBefore(accessControlSpotUser.getAccessStartTime()) || now.isAfter(accessControlSpotUser.getAccessEndTime())) {
                continue;
            }

            access = true;
        }

        if (!access) {
            doorMagneticService.saveGuestAccessRecord(guest.getHrId(), guest.getName(), doorMagnetic.getId(), DoorMagneticAccessResult.NO_ACCESS);
            return ServiceResult.error("开门失败，当前非用户通行时间");
        }

        // 权限鉴定成功，发送开门指令
        doorMagneticService.open(doorMagnetic.getNumber(), guest.getHrId());
        // 写入通行记录
        doorMagneticService.saveGuestAccessRecord(guest.getHrId(), guest.getName(), doorMagnetic.getId(), DoorMagneticAccessResult.ACCESS);

        return ServiceResult.ok();
    }

    /**
     * 判断访客是否属于该会议
     *
     * @param reservationRecord 会议
     * @param mobile            访客手机号码
     * @return ture: 属于， false ； 不属于
     * @author Yang.Lee
     * @date 2021/7/5 11:25
     **/
    @Transactional(readOnly = true)
    public boolean isGuestInReservation(ReservationRecord reservationRecord, String mobile) {

        List<GuestDetail> detailList = guestDetailDao.findByInvitationReservationRecord(reservationRecord);
        List<GuestDetail> mobileList = detailList.stream()
                .filter(obj -> obj.getMobile().equals(mobile))                          // 筛选手机号码
                .collect(Collectors.toList());

        return !mobileList.isEmpty();
    }

    /**
     * 为图片文件添加头信息
     *
     * @param imgStr
     * @param format
     * @return
     * @author Yang.Lee
     */
    private String addBase64Head(String imgStr, String format) {
        StringBuilder header = new StringBuilder("data:image/" + format + ";base64,");
        return header.append(imgStr).toString();
    }

    /**
     * 创建微信url，将回调URL包裹在微信url中
     *
     * @param url 回调url
     * @return
     * @author Yang.Lee
     */
    private String createWeChatRedirectUrl(String url) {
        try {
            url = URLEncoder.encode(url, "UTF-8");
            log.debug(url);
        } catch (UnsupportedEncodingException e) {
            log.error("encode qrcode url error！", e);
            return null;
        }

        String weChatUrl = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect";
        return String.format(weChatUrl, appId, url);
    }
}
