package cn.com.cowain.ibms.rest.req.meeting;

import cn.com.cowain.ibms.utils.DateUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * @author Yang.Lee
 * @date 2022/4/12 10:25
 */
@Data
@ApiModel("会议室查询参数对象")
public class MeetingRoomSearchReq {

    @ApiModelProperty(value = "项目区域。昆山；南通；花桥", example = "昆山", position = 1)
    private String projectArea;

    @DateTimeFormat(pattern = DateUtils.PATTERN_DATETIME1)
    @ApiModelProperty(value = "查询起始时间； yyyy-MM-dd HH:mm", example = "2022-01-01 01:00", position = 2)
    private LocalDateTime startTime;

    @DateTimeFormat(pattern = DateUtils.PATTERN_DATETIME1)
    @ApiModelProperty(value = "查询结束时间； yyyy-MM-dd HH:mm", example = "2022-01-01 01:30", position = 3)
    private LocalDateTime endTime;

    @ApiModelProperty(value = "仅显示空闲时间标识。emptyOnly=1 表示只查询有空闲的会议室数据", example = "1", position = 4)
    private int emptyOnly;

    @ApiModelProperty(value = "会议预约记录id。 该字段尽在emptyOnly=1时生效，该字段有数据时将会查询出该会议所在的数据", example = "1123", position = 5)
    private String reservationId;

    @ApiModelProperty(value = "参会人数",example = "1",position = 6)
    private Integer number;
}
