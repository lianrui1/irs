package cn.com.cowain.ibms.service.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * Description
 *
 * @author 张宇鑫
 * @version 1.0
 * @since 2020 8/14/20
 */
@Data
@Builder
@AllArgsConstructor
public class MeetingReminderBean {
    /**
     * 微信推送标题
     */
    private String first;
    /**
     * 微信推送服务类型
     */
    private String keyword1;
    /**
     * 微信推送  服务状态
     */
    private String keyword2;
    /**
     * 微信推送 服务时间
     */
    private String keyword3;
    /**
     * 微信推送  备注
     */
    private String remark;
    /**
     * 微信推送 详情链接
     */
    private String url;
    /**
     * 用户openId
     */
    private String wxOpenId;

}
