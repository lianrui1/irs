package cn.com.cowain.ibms.service.meeting.impl;

import cn.com.cowain.ibms.bo.CreateMeetingFileBO;
import cn.com.cowain.ibms.dao.ReservationRecordDao;
import cn.com.cowain.ibms.dao.ReservationRecordItemDao;
import cn.com.cowain.ibms.dao.SysUserDao;
import cn.com.cowain.ibms.dao.meetingfile.MeetingFileDao;
import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.meeting.MeetingFile;
import cn.com.cowain.ibms.enumeration.ReservationRecordStatus;
import cn.com.cowain.ibms.enumeration.meeting.FileType;
import cn.com.cowain.ibms.enumeration.wps.WpsScene;
import cn.com.cowain.ibms.exceptions.BizLogicNotMatchException;
import cn.com.cowain.ibms.rest.req.ReservationUploadUrlReq;
import cn.com.cowain.ibms.rest.req.meeting.MeetingFileCreateReq;
import cn.com.cowain.ibms.rest.req.meeting.MeetingFileReq;
import cn.com.cowain.ibms.rest.req.meeting.UpdateMeetingFileReq;
import cn.com.cowain.ibms.rest.resp.StaffResp;
import cn.com.cowain.ibms.rest.resp.meeting.MeetingFileResp;
import cn.com.cowain.ibms.rest.resp.meeting.MeetingFileUrlResp;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.ServiceMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.meeting.MeetingFileService;
import cn.com.cowain.ibms.service.wps.WpsService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.FileUtil;
import cn.com.cowain.ibms.utils.RegexUtil;
import cn.com.cowain.ibms.utils.TokenUtils;
import cn.com.cowain.sfp.comm.utils.JwtUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author wei.cheng
 * @date 2022/02/23 10:44
 */
@Slf4j
@Service
public class MeetingFileServiceImpl implements MeetingFileService {
    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserService;
    @Resource
    private SysUserDao sysUserDao;
    @Resource
    private ReservationRecordDao reservationRecordDao;
    @Resource
    private ReservationRecordItemDao reservationRecordItemDao;
    @Resource
    private MeetingFileDao meetingFileDao;
    @Resource
    private WpsService wpsService;
    @Value("${fastdfs.fileUrl}")
    private String fastDfsHttpUrl;

    @Override
    @Transactional
    public ServiceResult createMeetingFile(MeetingFileCreateReq req, String token) {
        log.debug("start to create meetingFile, reservationRecordId:{}, token:{}", req.getReservationRecordId(), token);
        CreateMeetingFileBO bo = requestCheckOfCreateMeetingFile(req, token);
        ServiceResult serviceResult = bo.getServiceResult();
        if (!serviceResult.isSuccess()) {
            return serviceResult;
        }
        ReservationRecord reservationRecord = bo.getReservationRecord();
        SysUser sysUser = bo.getSysUser();
        MeetingFile meetingFile = buildMeetingFile(req, reservationRecord, sysUser.getEmpNo());
        //同一个人上传相同文档会覆盖之前的，相同的判断逻辑为：类型，名称，同一人
        List<MeetingFile> sameFileList = meetingFileDao.findAllByReservationRecordIdAndCreatorHrIdAndFileNameAndFileExt(
                reservationRecord.getId(), sysUser.getEmpNo(), meetingFile.getFileName(), meetingFile.getFileExt());
        if (CollectionUtils.isNotEmpty(sameFileList)) {
            sameFileList.forEach(file -> file.setIsDelete(1));
            meetingFileDao.saveAll(sameFileList);
        }
        meetingFileDao.save(meetingFile);
        MeetingFileResp resp = convert(meetingFile, sysUser);
        serviceResult.setObject(resp);
        return serviceResult;
    }

    /**
     * 保存meetingFile
     *
     * @param req
     * @param reservationRecord
     * @param userHrId
     * @return
     */
    private MeetingFile buildMeetingFile(MeetingFileReq req, ReservationRecord reservationRecord, String userHrId) {
        MeetingFile meetingFile = new MeetingFile();
        meetingFile.setReservationRecord(reservationRecord);
        meetingFile.setCreatorHrId(userHrId);
        meetingFile.setFilePath(StringUtils.removeStart(req.getUrl(), fastDfsHttpUrl));
        meetingFile.setSize(req.getSize());
        String fileExt = FileUtil.getExt(req.getOriginalFileName());
        meetingFile.setFileName(StringUtils.removeEnd(req.getOriginalFileName(), "." + fileExt));
        meetingFile.setFileExt(fileExt);
        meetingFile.setFileType(FileType.FILE);
        return meetingFile;
    }

    /**
     * 将meetingFile转换为meetingFileResp
     *
     * @param meetingFile 会议文档，非空
     * @param sysUser     创建者，可能为null
     * @return
     */
    private MeetingFileResp convert(MeetingFile meetingFile, SysUser sysUser) {
        MeetingFileResp meetingFileResp = convertNotFullMeetingFileResp(meetingFile);
        meetingFileResp.setWpsPreviewUrl(wpsService.buildWpsPreviewUrl(meetingFile.getId(), meetingFile.getFileExt(), TokenUtils.getToken(),
                WpsScene.MEETING_FILE));
        StaffResp creator;
        if (Objects.nonNull(sysUser)) {
            creator = StaffResp.convert(sysUser);
        } else {
            creator = new StaffResp();
            creator.setHrId(meetingFile.getCreatorHrId());
            creator.setNameZh("未知");
            creator.setHeadImgUrl(StringUtils.EMPTY);
        }
        meetingFileResp.setCreator(creator);
        return meetingFileResp;
    }

    /**
     * 将meetingFile转换为MeetingFileResp对象，其中MeetingFileResp对象的值未设全
     *
     * @param meetingFile
     * @return
     */
    private MeetingFileResp convertNotFullMeetingFileResp(MeetingFile meetingFile) {
        MeetingFileResp meetingFileResp = new MeetingFileResp();
        meetingFileResp.setId(meetingFile.getId());
        meetingFileResp.setReservationRecordId(meetingFile.getReservationRecord().getId());
        meetingFileResp.setUrl(fastDfsHttpUrl + meetingFile.getFilePath());
        meetingFileResp.setOriginalFileName(meetingFile.getFileName() + "." + meetingFile.getFileExt());
        meetingFileResp.setSize(meetingFile.getSize());
        meetingFileResp.setFileName(meetingFile.getFileName());
        meetingFileResp.setFileExt(meetingFile.getFileExt());
        meetingFileResp.setCreateTime(meetingFile.getCreatedTime());
        return meetingFileResp;
    }

    /**
     * 校验请求参数是否有效
     *
     * @param req
     * @param token
     * @return
     */
    private CreateMeetingFileBO requestCheckOfCreateMeetingFile(MeetingFileCreateReq req, String token) {
        CreateMeetingFileBO bo = new CreateMeetingFileBO();
        //验证当前登陆用户是否有效
        String hrId = JwtUtil.getHrId(token);
        SysUser sysUser = sysUserService.getUserByEmpNo(hrId);
        bo.setSysUser(sysUser);
        if (Objects.isNull(sysUser)) {
            bo.setServiceResult(ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_STAFF, hrId)));
            return bo;
        }
        //验证会议记录是否存在
        Optional<ReservationRecord> reservationRecordOptional = reservationRecordDao.findById(req.getReservationRecordId());
        if (reservationRecordOptional.isEmpty()) {
            bo.setServiceResult(ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_MEETING_RESERVATION,
                    req.getReservationRecordId())));
            return bo;
        }
        ReservationRecord reservationRecord = reservationRecordOptional.get();
        bo.setReservationRecord(reservationRecord);
        //验证用户是否有上传会议文档的权限
        if (hrId.equals(reservationRecord.getInitiatorEmpNo())) {
            log.debug("会议的发起人可以上传会议文档");
        } else {
            if (reservationRecordItemDao.existsByReservationRecordIdAndOwnerEmpNo(reservationRecord.getId(), hrId)) {
                log.debug("会议的参与人可以上传会议文档");
            } else {
                bo.setServiceResult(ServiceResult.error("你非参会用户，暂无使用权限"));
                return bo;
            }
        }

        //校验文件数据是否有效
        if (!isFileValid(req)) {
            bo.setServiceResult(ServiceResult.error("文件数据异常"));
            return bo;
        }
        bo.setServiceResult(ServiceResult.ok());
        return bo;
    }

    /**
     * 判断文档信息是否无效
     *
     * @param req
     * @return
     */
    private Boolean isFileValid(MeetingFileReq req) {
        Assert.notNull(req, "req must not be null");
        // 最好能校验文件在fastDfs是否真实存在
        return req.getUrl().startsWith(fastDfsHttpUrl) && req.getUrl().length() > fastDfsHttpUrl.length() &&
                RegexUtil.match(req.getOriginalFileName(), RegexUtil.FILE_NAME);
    }

    @Override
    @Transactional
    public ServiceResult deleteMeetingFile(String id, String token) {
        log.info("start to delete meetingFile, id:{}, token:{}", id, token);
        Optional<MeetingFile> meetingFileOptional = meetingFileDao.findById(id);
        if (meetingFileOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_MEETING_FILE, id));
        }
        MeetingFile meetingFile = meetingFileOptional.get();
        String hrId = JwtUtil.getHrId(token);
        if (!StringUtils.equals(hrId, meetingFile.getCreatorHrId())) {
            return ServiceResult.error("仅支持上传人自己删除自己上传的文档");
        }
        ReservationRecord reservationRecord = meetingFile.getReservationRecord();
        if (ReservationRecordStatus.CANCEL.equals(reservationRecord.getStatus()) ||
                ReservationRecordStatus.FINISH.equals(reservationRecord.getStatus())) {
            return ServiceResult.error("已取消、已结束的会议不支持删除功能");
        }
        meetingFile.setIsDelete(1);
        meetingFileDao.save(meetingFile);
        return ServiceResult.ok(convertNotFullMeetingFileResp(meetingFile));
    }

    @Override
    public ServiceResult searchMeetingFileOfReservationRecord(String reservationRecordId) {
        log.info("start to search meetingFile of reservationRecord, reservationRecordId:{}", reservationRecordId);
        Optional<ReservationRecord> reservationRecordOptional = reservationRecordDao.findById(reservationRecordId);
        if (reservationRecordOptional.isEmpty()) {
            return ServiceResult.error("会议预约ID不存在", ErrConst.E01);
        }
        return ServiceResult.ok(getMeetingFileByRecordId(reservationRecordId));
    }

    @Override
    public List<MeetingFileResp> getMeetingFileByRecordId(String reservationRecordId) {
        List<MeetingFile> meetingFileList = meetingFileDao.findAllByReservationRecordIdAndFileTypeOrderByCreatedTimeAsc(reservationRecordId, FileType.FILE);
        List<MeetingFileResp> meetingFileRespList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(meetingFileList)) {
            List<String> userHrIdList = meetingFileList.stream().map(MeetingFile::getCreatorHrId).distinct().collect(Collectors.toList());
            List<SysUser> sysUserList = sysUserDao.findAllByEmpNoIn(userHrIdList);
            Map<String, SysUser> hrIdToSysUserMap = sysUserList.stream().collect(Collectors.toMap(SysUser::getEmpNo, s -> s));
            List<String> notFoundHrIdList = ListUtils.removeAll(userHrIdList, hrIdToSysUserMap.keySet());
            if (CollectionUtils.isNotEmpty(notFoundHrIdList)) {
                notFoundHrIdList.forEach(hrId -> {
                    SysUser sysUser = sysUserService.getUserByEmpNo(hrId);
                    if (Objects.nonNull(sysUser)) {
                        hrIdToSysUserMap.put(sysUser.getEmpNo(), sysUser);
                    }
                });
            }
            meetingFileList.forEach(meetingFile -> meetingFileRespList.add(convert(meetingFile, hrIdToSysUserMap.get(meetingFile.getCreatorHrId()))));
        }
        return meetingFileRespList;
    }

    @Override
    public List<MeetingFileUrlResp> getMeetingFileUrlByRecordId(String reservationRecordId) {
        List<MeetingFile> meetingFileList = meetingFileDao.findAllByReservationRecordIdAndFileTypeOrderByCreatedTimeAsc(reservationRecordId, FileType.URL);
        List<MeetingFileUrlResp> list = new ArrayList<>();

        meetingFileList.forEach(meetingFile -> {
            String[] split = meetingFile.getUrl().split(",");
            List<String> urls = new ArrayList<>();
            for(int i = 0;i < split.length;i++){
                urls.add(split[i]);
            }
            MeetingFileUrlResp urlResp = new MeetingFileUrlResp();
            urlResp.setId(meetingFile.getId());
            urlResp.setCreateTime(urlResp.getCreateTime());
            urlResp.setReservationRecordId(reservationRecordId);
            StaffResp creator = new StaffResp();
            SysUser sysUser = sysUserService.getUserByEmpNo(meetingFile.getCreatorHrId());
            BeanUtils.copyProperties(sysUser, creator);
            urlResp.setCreator(creator);
            urlResp.setUrl(urls);
            urlResp.setUrlRemark(meetingFile.getUrlRemark());
            list.add(urlResp);
        });
        return list;
    }

    @Override
    @Transactional
    public void createMeetingFile(ReservationRecord reservationRecord, List<MeetingFileReq> meetingFileReqList, String token) {
        Assert.notNull(reservationRecord, "reservationRecord must not be null");
        Assert.notNull(token, "token must not be null");
        if (CollectionUtils.isEmpty(meetingFileReqList)) {
            return;
        }
        // 获取用户工号
        String userHrId = JwtUtil.getHrId(token);

        //校验文档信息是否有效
        meetingFileReqList.forEach(req -> {
            if (!isFileValid(req)) {
                throw new BizLogicNotMatchException("会议文档信息异常");
            }
        });
        // 批量构建meetingFile对象
        List<MeetingFile> meetingFileList = meetingFileReqList.stream().map(r -> buildMeetingFile(r, reservationRecord, userHrId)).collect(Collectors.toList());

        //保存
        meetingFileDao.saveAll(meetingFileList);
    }

    @Override
    @Transactional
    public void updateMeetingFile(ReservationRecord reservationRecord, List<UpdateMeetingFileReq> meetingFileReqList, String token) {
        Assert.notNull(reservationRecord, "reservationRecord must not be null");
        Assert.notNull(token, "token must not be null");
        // 获取用户工号
        String userHrId = JwtUtil.getHrId(token);
        //校验文档信息是否有效
        meetingFileReqList.forEach(req -> {
            if (!isFileValid(req)) {
                throw new BizLogicNotMatchException("会议文档信息异常");
            }
        });
        // 获取会议已有的所有文档
        List<MeetingFile> meetingFileList = meetingFileDao.findAllByReservationRecordIdAndFileTypeOrderByCreatedTimeAsc(reservationRecord.getId(), FileType.FILE);
        Map<String, MeetingFile> idToMeetingFileMap = meetingFileList.stream().collect(Collectors.toMap(MeetingFile::getId, m -> m));
        List<String> idsInRequest = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(meetingFileReqList)) {
            meetingFileReqList.forEach(updateMeetingFileReq -> {
                if (StringUtils.isNotEmpty(updateMeetingFileReq.getId())) {
                    idsInRequest.add(updateMeetingFileReq.getId());
                }
            });
        }
        meetingFileList.forEach(meetingFile -> {
            // 判断请求中是否存在相同id的会议文档，如果没有需要删除
            if (!idsInRequest.contains(meetingFile.getId())) {
                // 只有文档的上传人才能删除文档
                if (!userHrId.equals(meetingFile.getCreatorHrId())) {
                    throw new BizLogicNotMatchException("只有文档的上传人才能删除文档");
                }
                if (ReservationRecordStatus.CANCEL.equals(reservationRecord.getStatus()) ||
                        ReservationRecordStatus.FINISH.equals(reservationRecord.getStatus())) {
                    throw new BizLogicNotMatchException("已取消、已结束的会议不支持删除文档功能");
                }
                meetingFile.setIsDelete(1);
            }
        });
        meetingFileReqList.forEach(updateMeetingFileReq -> {
            if (StringUtils.isEmpty(updateMeetingFileReq.getId())) {
                meetingFileList.add(buildMeetingFile(updateMeetingFileReq, reservationRecord, userHrId));
            } else {
                MeetingFile meetingFile = idToMeetingFileMap.get(updateMeetingFileReq.getId());
                if (Objects.nonNull(meetingFile)) {
                    log.debug("会议文档本身一旦创建暂不支持修改");
                } else {
                    meetingFileList.add(buildMeetingFile(updateMeetingFileReq, reservationRecord, userHrId));
                }
            }
        });
        if (CollectionUtils.isNotEmpty(meetingFileList)) {
            meetingFileDao.saveAll(meetingFileList);
        }
    }

    @Override
    public void createMeetingUrl(ReservationRecord reservationRecord, ReservationUploadUrlReq req, String token) {

        // 获取用户工号
        String userHrId = JwtUtil.getHrId(token);


        MeetingFile file = new MeetingFile();
        file.setFileType(FileType.URL);
        file.setReservationRecord(reservationRecord);
        file.setCreatorHrId(userHrId);
        String url1 = "";
        for(String url : req.getUrlList()){
            url1 += url + ",";
        }
        file.setUrl(url1.substring(0, url1.length() - 1));
        file.setUrlRemark(req.getUrlRemark());
        meetingFileDao.save(file);
    }
}
