package cn.com.cowain.ibms.dao.meeting;

import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.entity.meeting.GuestDetail;
import cn.com.cowain.ibms.entity.meeting.InvitationCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GuestDetailDao extends JpaRepository<GuestDetail, String> {

    // 根据邀请函和手机号获取详细信息
    Optional<GuestDetail> findByInvitationAndMobile(InvitationCard invitationCard, String mobile);

    // 根据会议记录获取访客列表
    List<GuestDetail> findByInvitationReservationRecord(ReservationRecord reservationRecord);

    // 根据访客工号和邀请卡获取访客信息
    Optional<GuestDetail> findByJobNumAndInvitation(String jobNum, InvitationCard card);
}
