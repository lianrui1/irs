package cn.com.cowain.ibms.enumeration.iot;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/3/11 14:18
 */
public enum  ApproverType {

    STAFF("员工"),

    LABOR("劳务");

    ApproverType (String name){
        this.name = name;
    }
    String name;

    public String getName(){
        return this.name;
    }
}
