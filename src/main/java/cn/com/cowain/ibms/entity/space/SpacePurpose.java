package cn.com.cowain.ibms.entity.space;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 空间用途
 *
 * @author Yang.Lee
 * @date 2021/11/8 14:39
 */
@Data
@Entity
@Table(name = "ibms_space_purpose")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_space_purpose", comment = "空间用途表")
@Where(clause = "is_del=0")
public class SpacePurpose extends BaseEntity {

    @Column(name = "name", nullable = false, columnDefinition = "varchar(10)  COMMENT '用途名称'")
    private String name;
}
