package cn.com.cowain.ibms.dao.office;

import cn.com.cowain.ibms.bo.SimpleIntelligentOfficeDevice;
import cn.com.cowain.ibms.entity.office.IntelligentOfficeDevice;
import cn.com.cowain.ibms.enumeration.iot.UsableStatus;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IntelligentOfficeDeviceDao extends JpaRepository<IntelligentOfficeDevice, String>, JpaSpecificationExecutor<IntelligentOfficeDevice> {

    List<IntelligentOfficeDevice> findByIntelligentOfficeIdAndUserHrId(String intelligentOfficeId, String userHrId);

    // 根据会议室ID 工号和设备状态获取办公室设备信息
    List<IntelligentOfficeDevice> findByIntelligentOfficeIdAndUserHrIdAndUsableStatus(String id, String hrId, UsableStatus enable);

    Optional<IntelligentOfficeDevice> findByDeviceIdAndUserHrId(String deviceId, String userHrId);

    List<IntelligentOfficeDevice> findByIntelligentOfficeId(String intelligentOfficeId);

    Optional<IntelligentOfficeDevice> findByIntelligentOfficeIdAndDeviceIdAndUserHrId(String intelligentOfficeId, String deviceId, String userHrId);

    /**
     * 简化查询
     *
     * @param intelligentOfficeId
     * @param deviceIds
     * @param userHrId
     * @return
     */
    @Query(
            value = "select device.id from IntelligentOfficeDevice where intelligentOffice.id = :intelligentOfficeId " +
                    "and device.id in (:deviceIds) and userHrId = :userHrId and isDelete = 0"
    )
    List<String> findDeviceIdByIntelligentOfficeIdAndDeviceIdsAndUserHrId(@Param("intelligentOfficeId")String intelligentOfficeId,
                                                                          @Param("deviceIds")List<String> deviceIds, @Param("userHrId")String userHrId);

    /**
     * 根据设备id查询
     *
     * @param deviceId 设备ID
     * @return 查询结果（因每个admin用户可自定义设备，因此查询出的结果为list）
     * @author Yang.Lee
     * @date 2022/1/5 9:41
     **/
    List<IntelligentOfficeDevice> findByDeviceId(String deviceId);

    /**
     * 根据设备id、工号和办事id获取设备信息
     *
     * @param deviceId
     * @param hrId
     * @param officeId
     * @return
     */
    IntelligentOfficeDevice findByDeviceIdAndUserHrIdAndIntelligentOfficeId(String deviceId, String hrId, String officeId);

    /**
     * 根据工号和会议室id查询
     *
     * @param userHrId
     * @param intelligentOfficeId
     * @return
     */
    List<IntelligentOfficeDevice> findAllByUserHrIdAndIntelligentOfficeId(String userHrId,String intelligentOfficeId);

    // 根据空间Id
    List<IntelligentOfficeDevice> findByUserHrIdAndDeviceSpaceIdInOrderByCreatedTimeDesc(String hrId, List<String> spaceIds);

    List<IntelligentOfficeDevice> findByUserHrIdAndDeviceSpaceIdInAndDeviceIdNotInOrderByCreatedTimeDesc(String hrId, List<String> collect, List<String> spaceIds);

    List<IntelligentOfficeDevice> findAllByUserHrIdAndIntelligentOfficeIdOrderByCreatedTimeDesc(String hrId, String id);

    @Query(
            value = "select new cn.com.cowain.ibms.bo.SimpleIntelligentOfficeDevice(i.usableStatus, i.deviceAlisa, i.deviceAlisaProperties, i.device.id, i.device.hwDeviceId, i.device.deviceType, i.device.sn, i.device.deviceName, i.device.hwStatus, i.createdTime) from IntelligentOfficeDevice i " +
                    "where i.userHrId = :hrId and i.intelligentOffice.id = :intelligentOfficeId and i.isDelete = 0 "
    )
    List<SimpleIntelligentOfficeDevice> findDeviceOfOffice(@Param("hrId") String hrId, @Param("intelligentOfficeId") String intelligentOfficeId);

    @Query(
            value = "select new cn.com.cowain.ibms.bo.SimpleIntelligentOfficeDevice(i.usableStatus, i.deviceAlisa, i.deviceAlisaProperties, i.device.id, i.device.hwDeviceId, i.device.deviceType, i.device.sn, i.device.deviceName, i.device.hwStatus, i.createdTime) from IntelligentOfficeDevice i " +
                    "where i.userHrId = :hrId and i.device.space.id in (:spaceIds) and i.device.id not in (:deviceIdsNotIn) and i.isDelete = 0"
    )
    List<SimpleIntelligentOfficeDevice> findDeviceOfOffice(@Param("hrId") String hrId, @Param("spaceIds") List<String> spaceIds, @Param("deviceIdsNotIn") List<String> deviceIdsNotIn);
}

