package cn.com.cowain.ibms.service.impl;

import cn.com.cowain.ibms.exceptions.RemoteServiceUnavailableException;
import cn.com.cowain.ibms.feign.iotc.CheckFaceImageApi;
import cn.com.cowain.ibms.rest.resp.UploadResp;
import cn.com.cowain.ibms.service.UploadService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.utils.FastDfsPool;
import cn.com.cowain.ibms.utils.FileUtil;
import cn.com.cowain.ibms.utils.RedisUtil;
import cn.com.cowain.sfp.comm.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.util.Optional;
import java.util.UUID;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/4/22 11:04
 */
@Slf4j
@Service
public class UploadServiceImpl implements UploadService {
    public static final String VALID_FACE_URL_PREFIX = "VALID_FACE_URL_PREFIX:";
    @Value("${fastdfs.fileUrl}")
    private String fastDfsHttpUrl;

    @Resource
    private CheckFaceImageApi checkFaceImageApi;

    @Resource
    private RedisUtil redisUtil;


    @Override
    @Transactional
    public ServiceResult upload(MultipartFile multipartFile) {
        UploadResp uploadResp = new UploadResp();
        String url = null;
        try {
            // 获取文件后缀名
            String originalFileName = Optional.ofNullable(multipartFile.getOriginalFilename()).orElse("");
            String suffix = originalFileName.substring(originalFileName.lastIndexOf("."));
            // 临时file文件
            File file = new File(UUID.randomUUID() + suffix);
            // 将MultipartFile对象转file对象
            file = FileUtil.multipartFileToFile(multipartFile, file);
            // 将文件上传至fastdfs
            url = FastDfsPool.upload(file);
            long fileSize = file.length();
            // 会在本地产生临时文件，用完后需要删除
            FileUtil.deleteFiles(file);
            uploadResp.setUrl(fastDfsHttpUrl + url);
            uploadResp.setOriginalFileName(originalFileName);
            uploadResp.setSize(fileSize);

        } catch (Exception e) {
            log.error("文件上传失败: " + e);
            return ServiceResult.error("文件上传失败");
        }
        return ServiceResult.ok(uploadResp);
    }

    @Override
    @Transactional
    public ServiceResult uploadAndCheckFace(MultipartFile file) {
        log.info("start to upload and check face, originalFileName:{}, size:{}", file.getOriginalFilename(), file.getSize());
        ServiceResult serviceResult = upload(file);
        if (!serviceResult.isSuccess()) {
            return ServiceResult.error("文件上传失败");
        }
        UploadResp uploadResp = (UploadResp) serviceResult.getObject();
        try {
            JsonResult<Object> resp = checkFaceImageApi.checkFaceImage(uploadResp.getUrl());
            if (resp.getStatus() == 0) {
                log.error("IOTC 校验人脸照片失败，imageUrl: {}，结果：{}", uploadResp.getUrl(), resp);
                return ServiceResult.error("校验人脸照片失败：" + resp.getMsg(), resp.getCode());
            }
        } catch (Exception e) {
            throw new RemoteServiceUnavailableException("IOTC 校验人脸照片失败");
        }
        //将有效的人脸照片存入缓存，以免短时间内校验相同的照片
        String redisKey = redisUtil.createRealKey(VALID_FACE_URL_PREFIX + uploadResp.getUrl());
        redisUtil.set(redisKey, "", RedisUtil.FIFTEEN_MIN_SECOND);
        return serviceResult;
    }

    @Override
    public ServiceResult upload(File file) {
        UploadResp uploadResp = new UploadResp();
        String url = null;
        try {
//            // 获取文件后缀名
//            String originalFileName = Optional.ofNullable(multipartFile.getOriginalFilename()).orElse("");
//            String suffix = originalFileName.substring(originalFileName.lastIndexOf("."));
//            // 临时file文件
//            File file = new File(UUID.randomUUID() + suffix);
            // 将MultipartFile对象转file对象
//            file = FileUtil.multipartFileToFile(multipartFile, file);
            // 将文件上传至fastdfs
            url = FastDfsPool.upload(file);
            long fileSize = file.length();
            // 会在本地产生临时文件，用完后需要删除
            FileUtil.deleteFiles(file);
            uploadResp.setUrl(fastDfsHttpUrl + url);
            uploadResp.setOriginalFileName(file.getName());
            uploadResp.setSize(fileSize);

        } catch (Exception e) {
            log.error("文件上传失败: " + e);
            return ServiceResult.error("文件上传失败");
        }
        return ServiceResult.ok(uploadResp);
    }
}
