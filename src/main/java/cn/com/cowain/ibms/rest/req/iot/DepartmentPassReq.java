package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author tql
 * @Description PersonPassPageReq
 * @Date 21-11-9 下午9:05
 * @Version 1.0
 */
@Data
@ApiModel("部门通行权限通行参数对象")
public class DepartmentPassReq{

    @ApiModelProperty(value = "部门列表")
    private List<String> departmentList;

    @ApiModelProperty(value = "门禁点id")
    private String controlPointId;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm" )
    @ApiModelProperty(value = "通行时间(开始时间)")
    private LocalDateTime startTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm" )
    @ApiModelProperty(value = "通行时间(结束时间)")
    private LocalDateTime endTime;
}
