package cn.com.cowain.ibms.service.device;

import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.device.HardwarePageReq;
import cn.com.cowain.ibms.rest.req.device.HardwareReq;
import cn.com.cowain.ibms.rest.resp.device.HardwarePageResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;

public interface HardwareService {

    /*
     * 新建硬件
     */
    ServiceResult save(HardwareReq req);

    /*
     * 根据SN码 设备名称 产品 查询硬件 按创建时间倒叙
     *
     */
    PageBean<HardwarePageResp> getPage(HardwarePageReq req);

    /*
     * 删除硬件
     *
     */
    ServiceResult delete(String id);

    /*
     * 查询硬件详情
     *
     */
    ServiceResult get(String id);

    /*
     * 更新硬件
     *
     */
    ServiceResult update(String id, HardwareReq req);

}
