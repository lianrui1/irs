package cn.com.cowain.ibms.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author wei.cheng
 * @date 2022/10/14 16:51
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class SimpleDevice implements Serializable {
    /**
     * id
     */
    private String id;
    /**
     * 设备是否纳入空间控制
     **/
    private Integer spaceControl;
}
