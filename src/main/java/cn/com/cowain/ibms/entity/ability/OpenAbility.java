package cn.com.cowain.ibms.entity.ability;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.enumeration.ability.Ability;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 开放能力
 *
 * @author Yang.Lee
 * @date 2022/1/10 15:40
 */
@Data
@Entity
@Table(name = "ibms_open_ability")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_open_ability", comment = "IBMS 开放能力表")
@Where(clause = "is_del=0")
public class OpenAbility extends BaseEntity {

    @Enumerated(EnumType.STRING)
    @Column(name = "ability", nullable = false, columnDefinition = "varchar(50) COMMENT '开放能力'")
    private Ability ability;

    @Column(name = "`describe`", columnDefinition = "varchar(500) COMMENT '能力描述'")
    private String describe;
}
