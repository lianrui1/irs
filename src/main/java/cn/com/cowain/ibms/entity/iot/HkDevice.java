package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.enumeration.HkAddress;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/15 10:23
 */
@Data
@Entity
@Table(name = "iot_hk_device")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_hk_device", comment = "hk海康设备")
public class HkDevice extends BaseEntity {

    /**
     * iot 设备
     */
    @ManyToOne
    @JoinColumn(name = "device_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 设备ID'")
    private IotDevice device;

    /**
     * code
     */
    @Column(name = "resource_index_code", columnDefinition = "varchar(45) COMMENT 'hk面板机code'")
    private String  resourceIndexCode;

    /**
     *设备ip
     */
    @Column(name = "ip", columnDefinition = "varchar(45) COMMENT '设备ip'")
    private String ip;

    @Enumerated(EnumType.STRING)
    @Column(name = "address", columnDefinition = "varchar(45) COMMENT '设备所属地'")
    private HkAddress address;
}
