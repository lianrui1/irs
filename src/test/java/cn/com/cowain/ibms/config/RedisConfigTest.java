package cn.com.cowain.ibms.config;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/10/20
 */
@SpringBootTest
public class RedisConfigTest {

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void test1() {
        Assertions.assertNotNull(redisTemplate);
    }

}