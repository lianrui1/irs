package cn.com.cowain.ibms.rest.req.space;

import cn.com.cowain.ibms.enumeration.IotNodeType;
import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 设备列表分页查询参数对象
 *
 * @author Yang.Lee
 * @date 2021/3/29 16:50
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("设备列表分页请求参数")
public class DevicePageReq extends PageReq {

    @ApiModelProperty(value = "项目ID", example = "001122aabbcc", position = 1)
    private String projectId;

    @ApiModelProperty(value = "空间ID", example = "001122aabbcc", position = 2)
    private String spaceId;

    @ApiModelProperty(value = "产品ID", example = "001122aabbcc", position = 3)
    private String productId;

    @ApiModelProperty(value = "所属应用", example = "001122aabbcc", position = 4)
    private String application;

    @ApiModelProperty(value = "sn或设备名称", example = "打脸机", position = 5)
    private String snOrName;

    @ApiModelProperty(value = "网络状态", example = "正常", position = 6)
    private DeviceStatus deviceStatus;

    @ApiModelProperty(value = "数据来源", example = "HW_CLOUD", position = 7)
    private String dataSource;

    @ApiModelProperty(value = "设备类型", example = "DOOR_PLATE", position = 8)
    private DeviceType deviceType;

    @ApiModelProperty(value = "网络节点类型", example = "GATEWAY_SUB_DEVICE", position = 9)
    private IotNodeType iotNodeType;

    @ApiModelProperty(value = "不查询的数据",  position = 10)
    private List<String> excludedIdList;

    private int size = 30;
}
