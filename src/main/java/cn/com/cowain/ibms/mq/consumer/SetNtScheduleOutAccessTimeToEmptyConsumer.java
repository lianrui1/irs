package cn.com.cowain.ibms.mq.consumer;

import cn.com.cowain.ibms.mq.Topic;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * @author wei.cheng
 * @date 2022/03/23 15:12
 */
@Slf4j
@RocketMQMessageListener(
        topic = "${rocketmq.env}-" + Topic.SET_NT_SCHEDULE_OUT_ACCESS_TIME_TO_EMPTY_MESSAGE_BACK,
        consumerGroup = "${rocketmq.group}-" + Topic.SET_NT_SCHEDULE_OUT_ACCESS_TIME_TO_EMPTY_MESSAGE_BACK
)
@Component
public class SetNtScheduleOutAccessTimeToEmptyConsumer implements RocketMQListener<String> {
    @Override
    public void onMessage(String message) {
        log.debug("SetNtScheduleOutAccessTimeToEmptyConsumer onMessage, data:{}", message);
    }
}
