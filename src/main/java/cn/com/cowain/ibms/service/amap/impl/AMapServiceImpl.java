package cn.com.cowain.ibms.service.amap.impl;

import cn.com.cowain.ibms.exceptions.AMapApiFailedException;
import cn.com.cowain.ibms.service.amap.AMapService;
import cn.com.cowain.ibms.service.bean.amap.enumeration.CoordinateSystem;
import cn.com.cowain.ibms.service.bean.amap.geo.ReGeo;
import cn.com.cowain.ibms.service.bean.amap.geo.ReGeoCode;
import cn.com.cowain.ibms.service.bean.amap.poi.CoordinateConvert;
import cn.com.cowain.ibms.service.bean.amap.poi.LonLat;
import cn.com.cowain.ibms.service.bean.amap.weather.ForceCastsTempResult;
import cn.com.cowain.ibms.service.bean.amap.weather.ForecastsWeather;
import cn.com.cowain.ibms.service.bean.amap.weather.Lives;
import cn.com.cowain.ibms.service.bean.amap.weather.Weather;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 高德WebApi服务Service
 *
 * @author Yang.Lee
 * @date 2021/4/19 14:44
 */
@Slf4j
@Service
public class AMapServiceImpl implements AMapService {

    @Resource
    private RestTemplate restTemplate;

    @Value("${amap.application.key}")
    private String key;

    private String keyAppender;


    /**
     * 初始化
     *
     * @author Yang.Lee
     * @date 2021/4/19 15:18
     **/
    @PostConstruct
    protected void init() {
        this.keyAppender = "&key=" + this.key;
    }

    /**
     * 获取实时天气信息
     *
     * @param cityCode 城市编号
     * @return 天气信息
     * @author Yang.Lee
     * @date 2021/4/19 15:01
     **/
    @Override
    public Lives getLiveWeather(String cityCode) {

        StringBuilder url;
        url = new StringBuilder(API_WEATHER)
                .append("?city=")
                .append(cityCode)
                .append("&extensions=base");

        ResponseEntity<String> responseEntity = restTemplate.getForEntity(appendKeyToUrl(url.toString()), String.class);
        Weather weather = JSON.parseObject(responseEntity.getBody(), Weather.class);

        assert weather != null;

        if (!weather.success()) {

            log.error("获取高德实时天气数据失败，失败信息：{}", weather.getInfoMessage());
            throw new AMapApiFailedException();
        }

        // 高德返回的天气数据是一个集合，只返回第一条数据为实时天气
        return weather.getLives().get(0);
    }
    @Override
    public ForceCastsTempResult getWeatherForecasts(String cityCode) {

        StringBuilder url;
        url = new StringBuilder(API_WEATHER)
                .append("?city=")
                .append(cityCode)
                .append("&extensions=all");

        ResponseEntity<String> responseEntity = restTemplate.getForEntity(appendKeyToUrl(url.toString()), String.class);

        ForecastsWeather weather = JSON.parseObject(responseEntity.getBody(), ForecastsWeather.class);

        assert weather != null;

        if (!weather.success()) {

            log.error("获取高德实时天气数据失败，失败信息：{}", weather.getInfoMessage());
            throw new AMapApiFailedException();
        }
        // 高德返回的天气数据是一个集合，只返回第一条数据为实时天气
       return weather.getForecasts().get(0);

    }

    /**
     * 坐标转换
     *
     * @param lonLatList 待转坐标列表，单次转换最多40对坐标。单对坐标小数点后最多6位数字
     * @param cs         待转坐标所属坐标系
     * @return 转后的高德坐标
     * @author Yang.Lee
     * @date 2021/4/19 19:38
     **/
    @Override
    public List<LonLat> coordinateConvert(CoordinateSystem cs, LonLat... lonLatList ) {

        if (lonLatList == null) {
            return new ArrayList<>();
        }

        // 构造请求url
        StringBuilder url = new StringBuilder(API_COORDINATE_CONVERT)
                .append("?coordsys=")
                .append(cs)
                .append("&locations=")
                .append(createLocationParam(lonLatList));

        String finalUrl = appendKeyToUrl(url.toString());

        // 发送get请求
        ResponseEntity<CoordinateConvert> responseEntity = restTemplate.getForEntity(finalUrl, CoordinateConvert.class);
        CoordinateConvert coordinateConvert = responseEntity.getBody();

        assert coordinateConvert != null;

        if (!coordinateConvert.success()) {
            log.error("高德坐标转换失败，失败信息：{}", coordinateConvert.getInfoMessage());
            throw new AMapApiFailedException();
        }

        // 分割转换完成的坐标对
        String[] locationArray = coordinateConvert.getLocations().split(";");

        List<LonLat> result = new ArrayList<>();
        // 将String类型的坐标对转为坐标集合
        for (String location : locationArray) {
            String[] lonLat = location.split(",");
            result.add(new LonLat(Double.parseDouble(lonLat[0]), Double.parseDouble(lonLat[1])));
        }
        return result;
    }

    /**
     * 逆地理编码
     *
     * @param params   可选参数，具体参数参照高德api（无需包含location字段）。https://lbs.amap.com/api/webservice/guide/api/georegeo
     * @param location 经纬度坐标,经纬度小数点后不要超过 6 位
     * @return 逆地理解析结果
     * @author Yang.Lee
     * @date 2021/4/20 14:10
     **/
    @Override
    public List<ReGeoCode> reGeo(Map<String, Object> params, LonLat... location) {

        if(params == null){
            params = new HashMap<>();
        }

        /*
          高德地图中batch为false或不填时，返回的regeo为对象；
          batch为true时返回的regeo为集合
          因此发送请求时统一将batch为true，将返回数据当作集合处理
         */
        params.put("batch", true);

        // 删除map参数中的location,使用方法中的location对象构建经纬度参数
        params.remove("location");

        String finalUrl = appendParams(API_REGEO + "?location=" + createLocationParam(location), params);

        ResponseEntity<String> responseEntity = restTemplate.getForEntity(appendKeyToUrl(finalUrl), String.class);
        ReGeo reGeo = JSON.parseObject(responseEntity.getBody(), ReGeo.class);

        assert reGeo != null;
        if (!reGeo.success()) {
            log.error("高德逆地理解析失败，失败信息：{}", reGeo.getInfoMessage());
            throw new AMapApiFailedException();
        }

        return reGeo.getRegeoCodeList();
    }

    /**
     * 统一向get请求url后添加key参数
     *
     * @param url 请求url
     * @return 新url
     * @author Yang.Lee
     * @date 2021/4/19 15:16
     **/
    private String appendKeyToUrl(String url) {

        return url + keyAppender;
    }


    /**
     * 创建经纬度参数字符串，多个经纬度对以字符“|”分割.
     *
     * @param lonLats 坐标点列表
     * @return 经纬度字符串。例：121.123456,31.123456|121.654321,31.654321
     * @author Yang.Lee
     * @date 2021/4/20 15:21
     **/
    public String createLocationParam(@NotNull LonLat... lonLats) {

        // 获取待转化的坐标字符串
        StringBuilder locations = new StringBuilder();

        if (lonLats.length == 0) {
            return locations.toString();
        }

        // 经度和纬度用","分割，经度在前，纬度在后，经纬度小数点后不得超过6位。多个坐标对之间用”|”进行分隔
        for(LonLat lonLat : lonLats){
            locations.append(lonLat.getLon()).append(",").append(lonLat.getLat()).append("|");
        }

        return locations.deleteCharAt(locations.length() - 1).toString();
    }

    /**
     * 向url后拼接参数
     *
     * @param sourceUrl 原始URL
     * @param params    参数Map,Map中的key，value对应参数中的key，value
     * @return 拼接参数后的url
     * @author Yang.Lee
     * @date 2021/4/20 15:45
     **/
    public String appendParams(String sourceUrl, Map<String, Object> params) {

        StringBuffer url = new StringBuffer(sourceUrl);
        // 如果sourceUrl不包含“?”, 表示url中未添加任何参数，则默认添加"?"
        if (!sourceUrl.contains("?")) {
            url.append("?1=1");
        }

        // 拼接参数
        params.keySet().forEach(key -> url.append("&").append(key).append("=").append(params.get(key)));

        return url.toString();
    }
}
