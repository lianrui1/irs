package cn.com.cowain.ibms.entity.wps;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.enumeration.wps.WpsScene;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author wei.cheng
 * @date 2022/02/23 18:40
 */
@Data
@Entity
@Table(name = "ibms_wps_notification")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "ibms_wps_notification", comment = "WPS事件通知数据表")
public class WpsNotification extends BaseEntity {

    @Column(name = "operator_hr_id", columnDefinition = "varchar(36) COMMENT '操作人员工号'")
    private String operatorHrId;

    @Column(name = "file_id", nullable = false, columnDefinition = "varchar(36) COMMENT '文件唯一标识'")
    private String fileId;

    @Enumerated(EnumType.STRING)
    @Column(name = "scene", columnDefinition = "varchar(50) COMMENT '文件应用场景'")
    private WpsScene scene;

    @Column(name = "cmd", nullable = false, columnDefinition = "varchar(36) COMMENT '命令参数'")
    private String cmd;

    @Column(name = "body", nullable = false, columnDefinition = "json COMMENT '通知内容'")
    private String body;
}
