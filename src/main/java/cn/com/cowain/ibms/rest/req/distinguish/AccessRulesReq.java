package cn.com.cowain.ibms.rest.req.distinguish;

import cn.com.cowain.ibms.entity.distinguish.DeviceSource;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/6 14:06
 */
@Data
public class AccessRulesReq {

    @NotBlank(message = "设备不能为空")
    @ApiModelProperty(value = "云端设备信息", required = true, example = "eb2f8fa8-77cd-3f9e-acd5-2017a9b713f1")
    private String distinguishDeviceId;

    @NotBlank(message = "项目不能为空")
    @ApiModelProperty(value = "所属项目", required = true, example = "fb39d8e5-fe2c-3d2d-87f1-37023b11d02b", position = 1)
    private String projectId;

    @ApiModelProperty(value = "所属空间", example = "d7eb0758-ff15-3277-9909-48d76ce7d0f7", position = 2)
    private String spaceId;

    @NotEmpty(message="人员组不能为空")
    @ApiModelProperty(value = "包含人员组ID", required = true, example = "[1,2]", position = 3)
    private List<String> staffGroups;

    @ApiModelProperty(value = "设备来源。KUANGSHI:旷世;HAIKANG:海康;MODIAN:魔点", example = "KUANGSHI", position = 4)
    private DeviceSource deviceSource;

    @NotEmpty(message = "时间计划不能为空")
    @ApiModelProperty(value = "时间计划", required = true, position = 5)
    private String timePlanId;
}
