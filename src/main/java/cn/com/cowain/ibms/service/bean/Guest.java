package cn.com.cowain.ibms.service.bean;

import lombok.Data;

/**
 * 访客信息
 *
 * @author Yang.Lee
 * @date 2021/12/14 9:50
 */
@Data
public class Guest {

    private String name;

    private String mobile;

    private String hrId;
}
