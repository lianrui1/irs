package cn.com.cowain.ibms.service.ability.impl;
/**
 * OpenAbilityAccessControlSpotServiceImpl
 *
 * @author: yanzy
 * @date: 2022/01/14/17:01
 */

import cn.com.cowain.ibms.dao.ability.OpenAbilityAccessControlSpotDao;
import cn.com.cowain.ibms.dao.iot.AccessControlSpotDao;
import cn.com.cowain.ibms.dao.space.ProjectDao;
import cn.com.cowain.ibms.entity.ability.OpenAbilityAccessControlSpot;
import cn.com.cowain.ibms.entity.iot.AccessControlSpot;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.space.Project;
import cn.com.cowain.ibms.rest.resp.space.ProjectSpaceTreeResp;
import cn.com.cowain.ibms.rest.resp.space.SpaceTreeResp;
import cn.com.cowain.ibms.service.ability.OpenAbilityAccessControlSpotService;
import cn.com.cowain.ibms.service.space.SpaceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author yanzy
 * @date 2022/1/14 17:01
 */
@Slf4j
@Service
public class OpenAbilityAccessControlSpotServiceImpl implements OpenAbilityAccessControlSpotService {

    @Resource
    private OpenAbilityAccessControlSpotDao openAbilityAccessControlSpotDao;

    @Resource
    private SpaceService spaceService;

    @Resource
    private ProjectDao projectDao;

    @Resource
    private AccessControlSpotDao accessControlSpotDao;

    /**
     * 获取能力下绑定的门禁点树
     *
     * @param openAbilityId 开发能力id
     * @return 门禁点树
     * @author yanzy
     * @date 2022-01-14
     */
    @Override
    @Transactional
    public List<ProjectSpaceTreeResp> getHasDeviceTree(String openAbilityId) {

        List<OpenAbilityAccessControlSpot> openAbilityIdList = openAbilityAccessControlSpotDao.findByOpenAbilityId(openAbilityId);

        List<Space> spaceList = new ArrayList<>();

        openAbilityIdList.forEach(obj -> {

            Space space = obj.getSpot().getSpace();
            spaceList.add(space);

            while ((space = space.getParent()) != null) {
                spaceList.add(space);
            }
        });

        List<SpaceTreeResp> spaceTreeRespList = spaceService.getSpaceTreeResp(spaceList);

        Map<String, List<SpaceTreeResp>> spaceTreeRespMap = spaceTreeRespList.stream()
                .collect(Collectors.toMap(SpaceTreeResp::getProjectId,
                        e -> new ArrayList<>(Arrays.asList(e)), (oldList, newList) -> {
                            oldList.addAll(newList);
                            return oldList;
                        }));


        Set<String> projectIdSet = spaceTreeRespList.stream().map(SpaceTreeResp::getProjectId).collect(Collectors.toSet());
        List<Project> projectList = projectDao.findAllById(projectIdSet);
        List<ProjectSpaceTreeResp> result = projectList.stream().map(obj -> {

            ProjectSpaceTreeResp resp = new ProjectSpaceTreeResp();
            resp.setId(obj.getId());
            resp.setName(obj.getProjectName());
            resp.setNodeType("PROJECT");

            resp.setChildrenList(spaceTreeRespMap.get(obj.getId()).stream()
                    .map(space -> ProjectSpaceTreeResp.convert(space))
                    .collect(Collectors.toList()));

            return resp;
        }).collect(Collectors.toList());

        return result;
    }

    /**
     * 获取能力下未绑定的门禁点树
     *
     * @param openAbilityId 开发能力id
     * @return 门禁点树
     * @author yanzy
     * @date 2022-01-18
     */
    @Override
    public List<ProjectSpaceTreeResp> getNoBind(String openAbilityId) {

        List<OpenAbilityAccessControlSpot> byOpenAbilityId = openAbilityAccessControlSpotDao.findByOpenAbilityId(openAbilityId);

        List<String> abilityId = byOpenAbilityId.stream().map(openAbilityAccessControlSpot -> openAbilityAccessControlSpot.getSpot().getId()).collect(Collectors.toList());
        List<AccessControlSpot> allByIdNotIn;
        if (byOpenAbilityId.isEmpty()) {
            allByIdNotIn = accessControlSpotDao.findAll();
        } else {
            allByIdNotIn = accessControlSpotDao.findByIdNotIn(abilityId);
        }

        List<Space> spaceList = new ArrayList<>();

        allByIdNotIn.forEach(obj -> {

            Space space = obj.getSpace();
            spaceList.add(space);

            while ((space = space.getParent()) != null) {
                spaceList.add(space);
            }
        });

        List<SpaceTreeResp> spaceTreeRespList = spaceService.getSpaceTreeResp(spaceList);

        Map<String, List<SpaceTreeResp>> spaceTreeRespMap = spaceTreeRespList.stream()
                .collect(Collectors.toMap(SpaceTreeResp::getProjectId,
                        e -> new ArrayList<>(Arrays.asList(e)), (oldList, newList) -> {
                            oldList.addAll(newList);
                            return oldList;
                        }));


        Set<String> projectIdSet = spaceTreeRespList.stream().map(SpaceTreeResp::getProjectId).collect(Collectors.toSet());
        List<Project> projectList = projectDao.findAllById(projectIdSet);
        List<ProjectSpaceTreeResp> result = projectList.stream().map(obj -> {

            ProjectSpaceTreeResp resp = new ProjectSpaceTreeResp();
            resp.setId(obj.getId());
            resp.setName(obj.getProjectName());
            resp.setNodeType("PROJECT");

            resp.setChildrenList(spaceTreeRespMap.get(obj.getId()).stream()
                    .map(space -> ProjectSpaceTreeResp.convert(space))
                    .collect(Collectors.toList()));

            return resp;
        }).collect(Collectors.toList());

        return result;
    }
}
