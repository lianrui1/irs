package cn.com.cowain.ibms.service.meeting.impl;

import cn.com.cowain.ibms.dao.SysUserDao;
import cn.com.cowain.ibms.dao.meeting.CustomUserGroupDao;
import cn.com.cowain.ibms.dao.meeting.CustomUserGroupMemberDao;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.meeting.CustomUserGroup;
import cn.com.cowain.ibms.entity.meeting.CustomUserGroupMember;
import cn.com.cowain.ibms.rest.req.meeting.CustomUserGroupEditReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.StaffResp;
import cn.com.cowain.ibms.rest.resp.meeting.CustomUserGroupResp;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.meeting.CustomUserGroupService;
import cn.com.cowain.ibms.utils.RedisUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/6/9 11:07
 */
@Slf4j
@Service
public class CustomUserGroupServiceImpl implements CustomUserGroupService {

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private SysUserDao sysUserDao;

    @Autowired
    private CustomUserGroupDao customUserGroupDao;

    @Autowired
    private CustomUserGroupMemberDao customUserGroupMemberDao;

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserServiceUC;

    @Override
    public ServiceResult save(CustomUserGroupEditReq req, String hrId) {
        Optional<CustomUserGroup> userGroup = customUserGroupDao.findByNameAndOwnerEmpNo(req.getName() ,hrId);
        if(userGroup.isPresent()){
            return ServiceResult.error("该名称已存在");
        }
        // 保存用户组
        CustomUserGroup customUserGroup = new CustomUserGroup();
        customUserGroup.setName(req.getName());
        customUserGroup.setOwnerEmpNo(hrId);
        // 保存用户组
        customUserGroupDao.save(customUserGroup);
        // 保存用户组成员
        List<CustomUserGroupMember> customUserGroupMembers = new ArrayList<>();
        for(String user : req.getUserList()){
            CustomUserGroupMember customUserGroupMember = new CustomUserGroupMember();
            customUserGroupMember.setCustomUserGroup(customUserGroup);
            customUserGroupMember.setMemberEmpNo(user);
            customUserGroupMembers.add(customUserGroupMember);
        }
        customUserGroupMemberDao.saveAll(customUserGroupMembers);
        // 将新数据写入redis
        String realKey = redisUtil.createRealKey("CustomUserGroup" + hrId);
        // 存入redis的用户组
        List<CustomUserGroupResp> list = new ArrayList<>();
        // 根据创建人工号获取用户组
        List<CustomUserGroup> customUserGroups = customUserGroupDao.findByOwnerEmpNoOrderByCreatedTimeDesc(hrId);
        redis(realKey ,list ,customUserGroups);
        // 新建成功，返回新增的id
        IdResp idResp = new IdResp();
        idResp.setId(customUserGroup.getId());
        return ServiceResult.ok(idResp);
    }

    @Override
    public ServiceResult get(String id, String hrId) {
        Optional<CustomUserGroup> byId = customUserGroupDao.findById(id);
        if(!byId.isPresent()){
            return ServiceResult.error("没有此人员组");
        }
        String redisKey = redisUtil.createRealKey("CustomUserGroup" + hrId);
        // redis中是否有用户组
        if (Boolean.TRUE.equals(redisUtil.hasKey(redisKey))) {
            List<CustomUserGroupResp> result = JSON.parseArray(String.valueOf(redisUtil.get(redisKey)), CustomUserGroupResp.class);
            CustomUserGroupResp customUserGroupResp = result.stream().filter(user -> user.getId().equals(id)).findAny().orElse(null);
            return ServiceResult.ok(customUserGroupResp);
        }
        CustomUserGroup userGroup = byId.get();
        // 根据用户组查询成员
        List<CustomUserGroupMember> byCustomUserGroup = customUserGroupMemberDao.findByCustomUserGroup(userGroup);
        List<StaffResp> userList = new ArrayList<>();
        for(CustomUserGroupMember member : byCustomUserGroup){
            Optional<SysUser> byEmpNo = sysUserDao.findByEmpNo(member.getMemberEmpNo());
            StaffResp staffResp = new StaffResp();
            staffResp.setHrId(member.getMemberEmpNo());
            if(byEmpNo.isPresent()){
                staffResp.setNameZh(byEmpNo.get().getNameZh());
                staffResp.setHeadImgUrl(byEmpNo.get().getHeadImgUrl());
            }
            userList.add(staffResp);
        }
        CustomUserGroupResp customUserGroupResp = new CustomUserGroupResp();
        customUserGroupResp.setUserList(userList);
        BeanUtils.copyProperties(userGroup , customUserGroupResp);
        return ServiceResult.ok(customUserGroupResp);
    }

    @Override
    public ServiceResult update(CustomUserGroupEditReq req , String id) {

        // 判断用户组是否存在
        Optional<CustomUserGroup> byId = customUserGroupDao.findById(id);
        if(!byId.isPresent()){
            return ServiceResult.error("用户组不存在");
        }
        CustomUserGroup userGroup = byId.get();

        Optional<CustomUserGroup> customUserGroup = customUserGroupDao.findByNameAndOwnerEmpNo(req.getName(), userGroup.getOwnerEmpNo());
        if(customUserGroup.isPresent()  && !customUserGroup.get().getId().equals(id)){
            return ServiceResult.error("该名称已存在");
        }
        BeanUtils.copyProperties(req , userGroup);
        // 保存用户组变更
        customUserGroupDao.save(userGroup);
        List<CustomUserGroupMember> customUserGroupMembers = new ArrayList<>();
        List<CustomUserGroupMember> byCustomUserGroup = customUserGroupMemberDao.findByCustomUserGroup(userGroup);
        // 获取被删除的用户组成员
        List<String> list = new ArrayList<>();
        for(CustomUserGroupMember user : byCustomUserGroup){
            if(!req.getUserList().contains(user.getMemberEmpNo())){
                user.setIsDelete(1);
                customUserGroupMembers.add(user);
            }
            list.add(user.getMemberEmpNo());
        }
        // 获取新增的用户组成员
        for(String user : req.getUserList()){
            if(!list.contains(user)){
                CustomUserGroupMember customUserGroupMember = new CustomUserGroupMember();
                customUserGroupMember.setCustomUserGroup(userGroup);
                customUserGroupMember.setMemberEmpNo(user);
                customUserGroupMembers.add(customUserGroupMember);
            }
        }
        customUserGroupMemberDao.saveAll(customUserGroupMembers);

        // 将新数据写入redis
        String realKey = redisUtil.createRealKey("CustomUserGroup" + userGroup.getOwnerEmpNo());
        // 存入redis的用户组
        List<CustomUserGroupResp> list1 = new ArrayList<>();
        // 根据创建人工号获取用户组
        List<CustomUserGroup> customUserGroups = customUserGroupDao.findByOwnerEmpNoOrderByCreatedTimeDesc(userGroup.getOwnerEmpNo());
        redis(realKey ,list1 ,customUserGroups);
        return ServiceResult.ok();
    }

    @Override
    public ServiceResult delete(String id) {
        // 判断用户组是否存在
        Optional<CustomUserGroup> byId = customUserGroupDao.findById(id);
        if(!byId.isPresent()){
            return ServiceResult.error("用户组不存在");
        }
        CustomUserGroup userGroup = byId.get();
        List<CustomUserGroupMember> byCustomUserGroup = customUserGroupMemberDao.findByCustomUserGroup(userGroup);
        // 删除用户组成员
        for(CustomUserGroupMember user : byCustomUserGroup){
            user.setIsDelete(1);
        }
        customUserGroupMemberDao.saveAll(byCustomUserGroup);
        // 删除用户组
        userGroup.setIsDelete(1);
        customUserGroupDao.save(userGroup);

        // 将新数据写入redis
        String realKey = redisUtil.createRealKey("CustomUserGroup" + userGroup.getOwnerEmpNo());
        // 存入redis的用户组
        List<CustomUserGroupResp> list = new ArrayList<>();
        // 根据创建人工号获取用户组
        List<CustomUserGroup> customUserGroups = customUserGroupDao.findByOwnerEmpNoOrderByCreatedTimeDesc(userGroup.getOwnerEmpNo());
        redis(realKey ,list ,customUserGroups);
        return ServiceResult.ok();
    }

    @Override
    public ServiceResult list(String hrId) {
        String redisKey = redisUtil.createRealKey("CustomUserGroup" + hrId);
        // redis中是否有用户组
        if (Boolean.TRUE.equals(redisUtil.hasKey(redisKey))) {
            // 返回redis中的用户组
            List<CustomUserGroupResp> result = JSON.parseArray(String.valueOf(redisUtil.get(redisKey)), CustomUserGroupResp.class);
            return ServiceResult.ok(result);
        }
        // 从数据库获取用户组
        List<CustomUserGroupResp> list = new ArrayList<>();
        // 获取员工创建的所有用户组
        List<CustomUserGroup> byOwnerEmpNo = customUserGroupDao.findByOwnerEmpNoOrderByCreatedTimeDesc(hrId);
        for(CustomUserGroup group : byOwnerEmpNo){
            CustomUserGroupResp customUserGroupResp = new CustomUserGroupResp();
            customUserGroupResp.setId(group.getId());
            customUserGroupResp.setName(group.getName());
            // 根据会议用户组获取成员明细
            List<StaffResp> userList = new ArrayList<>();
            List<CustomUserGroupMember> byCustomUserGroup = customUserGroupMemberDao.findByCustomUserGroup(group);
            for(CustomUserGroupMember user : byCustomUserGroup){
                StaffResp staffResp = new StaffResp();
                staffResp.setHrId(user.getMemberEmpNo());
                SysUser sysUser = sysUserServiceUC.getUserByEmpNo(user.getMemberEmpNo());
                staffResp.setNameZh(sysUser.getNameZh());
                staffResp.setHeadImgUrl(sysUser.getHeadImgUrl());
                staffResp.setDepartName(sysUser.getDeptName());
                staffResp.setStDeptName(sysUser.getFullDepartmentName());
                userList.add(staffResp);
            }
            customUserGroupResp.setUserList(userList);
            list.add(customUserGroupResp);
        }
        return ServiceResult.ok(list);
    }

    @Override
    public void delLeave(List<SysUser> sysUsers1) {
        // 循环所以离职人员
        for(SysUser user :sysUsers1){
            List<CustomUserGroup> customUserGroups = new ArrayList<>();
            List<String> strings = new ArrayList<>();
            // 根据工号获取其所在成员组
            List<CustomUserGroupMember> members = customUserGroupMemberDao.findByMemberEmpNo(user.getEmpNo());
            for(CustomUserGroupMember member :members){
                member.setIsDelete(1);
                customUserGroups.add(member.getCustomUserGroup());
            }
            // 从人员组删除此成员
            customUserGroupMemberDao.saveAll(members);
            for(CustomUserGroup group : customUserGroups){
                List<CustomUserGroupMember> memberList = customUserGroupMemberDao.findByCustomUserGroup(group);
                for(CustomUserGroupMember member : memberList){
                    strings.add(member.getMemberEmpNo());
                }
                CustomUserGroupEditReq req = new CustomUserGroupEditReq();
                req.setName(group.getName());
                // 根据用户组获取
                req.setUserList(strings);
                this.update(req ,group.getId());
            }
        }
    }

    private void redis(String realKey, List<CustomUserGroupResp> list, List<CustomUserGroup> customUserGroups) {
        for (CustomUserGroup customUserGroup : customUserGroups){
            List<StaffResp> list1 = new ArrayList<>();
            // 根据人员组获取成员
            List<CustomUserGroupMember> members = customUserGroupMemberDao.findByCustomUserGroup(customUserGroup);
            for(CustomUserGroupMember member : members){
                StaffResp staffResp = new StaffResp();
                staffResp.setHrId(member.getMemberEmpNo());
                Optional<SysUser> sysUserOp = sysUserDao.findByEmpNo(member.getMemberEmpNo());
                if(sysUserOp.isPresent()){
                    SysUser sysUser = sysUserOp.get();
                    staffResp.setNameZh(sysUser.getNameZh());
                    staffResp.setHeadImgUrl(sysUser.getHeadImgUrl());
                }
                list1.add(staffResp);
            }
            CustomUserGroupResp customUserGroupResp = new CustomUserGroupResp();
            customUserGroupResp.setId(customUserGroup.getId());
            customUserGroupResp.setName(customUserGroup.getName());
            customUserGroupResp.setUserList(list1);
            list.add(customUserGroupResp);
        }
        log.info("存入redis的用户组 : " + list);
        // 默认过期时间一个月
        redisUtil.set(realKey , list , 60 * 60 * 24L * 30);
    }

}
