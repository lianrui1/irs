package cn.com.cowain.ibms.rest.req.wps;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

/**
 * @author wei.cheng
 * @date 2022/02/22 16:18
 */
@Data
@ApiModel("WPS事件通知")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WpsOnnotifyReq {

    @ApiModelProperty(value = "命令参数", required = true, position = 1)
    @NotEmpty
    private String cmd;

    @ApiModelProperty(value = "信息内容", required = false, position = 2)
    private Object body;
}
