package cn.com.cowain.ibms.exceptions;

import cn.com.cowain.ibms.dao.ReservationRecordDao;
import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.enumeration.ReservationRecordStatus;
import cn.com.cowain.ibms.service.ReservationRecordService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * BizLogicNotMatchExceptionTest
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/21/20
 */
@SpringBootTest
class BizLogicNotMatchExceptionTest {

    @Autowired
    private ReservationRecordService reservationRecordService;

    @MockBean
    private ReservationRecordDao reservationRecordDao;

    @Test
    public void test1() {
        String id = "mockRecordId";
        ReservationRecord mockRecord = new ReservationRecord();
        mockRecord.setStatus(ReservationRecordStatus.DEFAULT);
        Optional<ReservationRecord> optional = Optional.of(mockRecord);
        Mockito.when(reservationRecordDao.findById(id)).thenReturn(optional);
//        assertThrows(BizLogicNotMatchException.class, () -> reservationRecordService.deleteByEmpNoAndSpaceId(id));
    }

}
