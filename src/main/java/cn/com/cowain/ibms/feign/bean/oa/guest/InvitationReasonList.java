package cn.com.cowain.ibms.feign.bean.oa.guest;

import lombok.Data;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/6/30 16:36
 */
@Data
public class InvitationReasonList {

    private String msg;

    private int code;

    private List<InvitationReason> reasonList;
}
