package cn.com.cowain.ibms.rest.resp.wps;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wei.cheng
 * @date 2022/02/22 14:49
 */
@Data
@ApiModel("文件详情")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FileInfoResp {

    @ApiModelProperty(value = "文件id，字符串长度小于40，和 URL 中的fileid必须一致", required = true, position = 1)
    private String id;

    @ApiModelProperty(value = "文件名(必须带文件后缀)", required = true, position = 2)
    private String name;

    @ApiModelProperty(value = "当前版本号，必须大于 0，同时位数小于 11", required = true, position = 3)
    private Long version;

    @ApiModelProperty(value = "文件大小，单位为B(文件真实大小，否则会出现异常)", required = true, position = 4)
    private Long size;

    @ApiModelProperty(value = "创建者id，字符串长度小于40", required = true, position = 5)
    private String creator;

    @ApiModelProperty(value = "创建时间，时间戳，单位为秒", required = true, position = 6)
    @JsonProperty(value = "create_time")
    private Long createTime;

    @ApiModelProperty(value = "修改者id，字符串长度小于40", required = true, position = 7)
    private String modifier;

    @ApiModelProperty(value = "修改时间，时间戳，单位为秒", required = true, position = 8)
    @JsonProperty(value = "modify_time")
    private Long modifyTime;

    @ApiModelProperty(value = "文档下载地址", required = true, position = 9)
    @JsonProperty(value = "download_url")
    private String downloadUrl;

    @ApiModelProperty(value = "限制预览页数", position = 10)
    @JsonProperty(value = "preview_pages")
    private String previewPages;

    @ApiModelProperty(value = "控制用户权限", position = 11)
    @JsonProperty(value = "user_acl")
    private UserAclResp userAcl;

    @ApiModelProperty(value = "水印", position = 12)
    private WatermarkResp watermark;
}
