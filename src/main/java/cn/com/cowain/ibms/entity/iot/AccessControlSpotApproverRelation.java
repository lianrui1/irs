package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author Yang.Lee
 * @date 2022/3/8 10:54
 */
@Data
@Entity
@Table(name = "iot_access_control_spot_approver_relation")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_access_control_spot_approver_relation", comment = "门禁点-审批人关系")
public class AccessControlSpotApproverRelation extends BaseEntity {

    @Column(name = "hr_id", nullable = false, columnDefinition = "varchar(30) COMMENT '审批人工号'")
    private String userHrId;

    @ManyToOne
    @JoinColumn(name = "access_control_spot_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK。门禁点id'")
    private AccessControlSpot spot;
}
