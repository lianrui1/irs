package cn.com.cowain.ibms.service.iot.impl;

import cn.com.cowain.ibms.dao.iot.*;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.iot.*;
import cn.com.cowain.ibms.entity.space.Project;
import cn.com.cowain.ibms.enumeration.iot.SpaceType;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.iot.*;
import cn.com.cowain.ibms.rest.resp.iot.ElevatorSpotFloorPageResp;
import cn.com.cowain.ibms.rest.resp.iot.ElevatorSpotUserPageResp;
import cn.com.cowain.ibms.rest.resp.iot.IotDeviceResp;
import cn.com.cowain.ibms.rest.resp.iot.ProjectResp;
import cn.com.cowain.ibms.rest.resp.space.SpaceFloorResp;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.iot.ElevatorSpotService;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.usercenter.api.service.UserCenterStaffApi;
import cn.com.cowain.usercenter.common.resp.PassportRespForFeign;
import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/3/24 15:23
 */
@Slf4j
@Service
public class ElevatorSpotServiceImpl implements ElevatorSpotService {

    @Autowired
    private DeviceDao deviceDao;

    @Autowired
    private SpaceDao spaceDao;

    @Autowired
    private ElevatorSpotDao elevatorSpotDao;

    @Autowired
    private ElevatorSpotUserDao elevatorSpotUserDao;

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserServiceUC;

    @Autowired
    private ElevatorSpotDeviceRelationDao relationDao;

    @Autowired
    private UserCenterStaffApi userCenterStaffApi;

    @Override
    @Transactional
    public ServiceResult create(ElevatorSpotAuthSaveReq req) {

        // todo 和IOTC对接 下发权限

        for (String spaceId : req.getSpaceIds()) {
            Optional<Space> spaceOp = spaceDao.findById(spaceId);
            if (spaceOp.isEmpty()) {
                // 业务处理失败，手动回滚事务,返回异常
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error("空间ID有误,分配权限失败");
            }
            Space space = spaceOp.get();

            Optional<ElevatorSpot> elevatorSpotOp = elevatorSpotDao.findBySpaceId(spaceId);
            ElevatorSpot elevatorSpot = new ElevatorSpot();
            if (elevatorSpotOp.isEmpty()) {
                // 创建梯控点
                elevatorSpot.setName(space.getName() + "梯控点");
                elevatorSpot.setSpace(space);
                elevatorSpotDao.save(elevatorSpot);
            } else {
                elevatorSpot = elevatorSpotOp.get();
            }

            // 报存梯控人员权限
            for (ElevatorSpotUserSaveReq user : req.getUserReqList()) {
                Optional<ElevatorSpotUser> spotUserOp = elevatorSpotUserDao.findByUserHrIdAndSpaceId(user.getEmpNo(), space.getId());
                if (spotUserOp.isEmpty()) {
                    ElevatorSpotUser elevatorSpotUser = new ElevatorSpotUser();
                    elevatorSpotUser.setAccessEndTime(req.getAccessEndTime());
                    elevatorSpotUser.setAccessStartTime(req.getAccessStartTime());
                    elevatorSpotUser.setUserHrId(user.getEmpNo());
                    elevatorSpotUser.setUserName(user.getName());
                    elevatorSpotUser.setDeptId(user.getDeptId());
                    elevatorSpotUser.setIsStaff(user.getIsStaff());
                    elevatorSpotUser.setElevatorSpot(elevatorSpot);
                    elevatorSpotUser.setSpace(space);
                    elevatorSpotUserDao.save(elevatorSpotUser);
                }
            }
        }


        return ServiceResult.ok();
    }

    @Override
    public PageBean<ElevatorSpotUserPageResp> getUserPage(ElevatorSpotUserPageReq req) {

        // 获取部门下人员
        List<String> empNos = new ArrayList<>();
        if (StringUtils.isNotEmpty(req.getDeptId())) {
            JsonResult<List<PassportRespForFeign>> result = userCenterStaffApi.passportList(Long.parseLong(req.getDeptId()));

            if (result.getStatus() != 1) {
                log.error("从UC获取部门内员工数据失败！！！UC返回信息：" + result.toString());
            } else {
                List<PassportRespForFeign> ucStaffList = result.getData();
                ucStaffList.forEach(person ->
                    empNos.add(person.getJobNo())
                );
            }
        }

        // 根据权限时间排序
        Sort sort = Sort.by(Sort.Direction.DESC, "accessStartTime");

        List<ElevatorSpotUser> elevatorSpotUserList = elevatorSpotUserDao.findAll((root, criteriaQuery, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();

            // 按人员类型
            if (null != req.getIsStaff()) {
                list.add(criteriaBuilder.equal(root.get("isStaff"), req.getIsStaff()));
            }

            // 按部门ID
            if (!CollectionUtils.isEmpty(empNos)) {
                list.add(criteriaBuilder.in(root.get("userHrId")).value(empNos));
            }

            // 按空间ID
            if (StringUtils.isNotEmpty(req.getSpaceId())) {
                list.add(criteriaBuilder.equal(root.get("space").get("id"), req.getSpaceId()));
            }

            // 按姓名或者工号
            if (StringUtils.isNotEmpty(req.getKeyword())) {
                String like = "%" + req.getKeyword() + "%";
                list.add(criteriaBuilder.or(
                        criteriaBuilder.like(root.get("userHrId"), like),
                        criteriaBuilder.like(root.get("userName"), like)));
            }

            return criteriaQuery.where(list.toArray(new Predicate[0])).getRestriction();
        }, sort);

        // 根据工号去重
        elevatorSpotUserList = elevatorSpotUserList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(()
                -> new TreeSet<>(Comparator.comparing(ElevatorSpotUser::getUserHrId))), ArrayList::new));

        // 返回数据
        List<ElevatorSpotUserPageResp> newList = new ArrayList<>();
        for (ElevatorSpotUser spotUser : elevatorSpotUserList) {
            ElevatorSpotUserPageResp resp = new ElevatorSpotUserPageResp();
            resp.setEmpNo(spotUser.getUserHrId());
            resp.setName(spotUser.getUserName());
            if (1 == spotUser.getIsStaff()) {
                SysUser sysUser = sysUserServiceUC.getUserByEmpNo(spotUser.getUserHrId());
                resp.setDeptName(sysUser.getFullDepartmentName());
                resp.setDeptId(spotUser.getDeptId());
            }
            resp.setIsStaff(spotUser.getIsStaff());
            resp.setFaceUrl("");
            resp.setAccessEndTime(spotUser.getAccessEndTime());
            resp.setAccessStartTime(spotUser.getAccessStartTime());
            if (StringUtils.isNotEmpty(spotUser.getFaceImg())) {
                resp.setFaceUrl(spotUser.getFaceImg());
            }

            // 人员拥有楼层
            List<ElevatorSpotUser> byUserHrId = elevatorSpotUserDao.findByUserHrId(spotUser.getUserHrId());
            // 按照项目ID去重
            List<ElevatorSpotUser> collect = byUserHrId.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() ->
                    new TreeSet<>(Comparator.comparing(t -> t.getSpace().getProject().getId()))), ArrayList::new));
            // 按项目创建时间排序
            Collections.sort(collect, Comparator.comparing(o -> o.getSpace().getProject().getCreatedTime()));
            List<ProjectResp> floorList = new ArrayList<>();
            collect.forEach(it -> {
                ProjectResp projectResp = new ProjectResp();
                projectResp.setProjectId(it.getSpace().getProject().getId());
                projectResp.setProjectName(it.getSpace().getProject().getProjectName());
                List<SpaceFloorResp> spaceList = new ArrayList<>();
                // 获取楼栋
                spaceDao.findByProjectIdAndIsShowAndLevel(it.getSpace().getProject().getId(), 1, 1).forEach(space -> {

                    List<ElevatorSpotUser> list = elevatorSpotUserDao.findByUserHrIdAndSpaceParentIdAndSpaceSpaceType(it.getUserHrId(), space.getId(), SpaceType.FLOOR);
                    if(!CollectionUtils.isEmpty(list)){
                        SpaceFloorResp spaceFloorResp = new SpaceFloorResp();
                        spaceFloorResp.setSpaceId(space.getId());
                        spaceFloorResp.setSpaceName(space.getName());
                        spaceFloorResp.setSpaceType(space.getSpaceType());
                        // 获取楼层
                        List<SpaceFloorResp> spaceFloorList = new ArrayList<>();

                        for(ElevatorSpotUser floor : list){
                            SpaceFloorResp floorResp = new SpaceFloorResp();
                            Space space1 = floor.getSpace();
                            floorResp.setSpaceId(space1.getId());
                            floorResp.setSpaceName(space1.getName());
                            floorResp.setSpaceType(space1.getSpaceType());
                            if (null != space1.getFloor()) {
                                floorResp.setFloor(space1.getFloor());

                            }
                            spaceFloorList.add(floorResp);
                        }
                        spaceFloorResp.setSpaceList(spaceFloorList);
                        spaceList.add(spaceFloorResp);
                    }
                });
                projectResp.setSpaceList(spaceList);
                floorList.add(projectResp);
            });
            resp.setFloorList(floorList);


            newList.add(resp);
        }

        return PageBean.createPageBeanByAllData(newList, req.getPage(), req.getSize());
    }

    @Override
    public ServiceResult deleteAuth(String empNo) {

        List<ElevatorSpotUser> elevatorSpotUserList = elevatorSpotUserDao.findByUserHrId(empNo);
        if (CollUtil.isEmpty(elevatorSpotUserList)) {
            return ServiceResult.error("删除失败,该人员无权限");
        }

        // 删除人员权限
        List<ElevatorSpotUser> list = new ArrayList<>();
        elevatorSpotUserList.forEach(it -> {
            it.setIsDelete(1);
            list.add(it);
        });
        elevatorSpotUserDao.saveAll(list);
        return ServiceResult.ok();
    }

    @Override
    public ServiceResult deleteFloorUserAuth(String empNo, String spaceId) {

        Optional<ElevatorSpotUser> elevatorSpotUserOp = elevatorSpotUserDao.findByUserHrIdAndSpaceId(empNo, spaceId);
        if (elevatorSpotUserOp.isEmpty()) {
            return ServiceResult.error("删除失败,该梯控无此人权限");
        }
        ElevatorSpotUser spotUser = elevatorSpotUserOp.get();

        // 删除 todo 向IOTC删除人员梯控权限
        spotUser.setIsDelete(1);
        elevatorSpotUserDao.save(spotUser);
        return ServiceResult.ok();
    }

    @Override
    @Transactional
    public ServiceResult update(ElevatorSpotAuthSaveReq req) {

        if (CollUtil.isEmpty(req.getUserReqList())) {
            return ServiceResult.error("请传入人员信息");
        }

        // todo 向IOTC更新梯控权限
        // 删除原有权限
        ElevatorSpotUserSaveReq userSaveReq = req.getUserReqList().get(0);
        elevatorSpotUserDao.findByUserHrId(userSaveReq.getEmpNo()).forEach(spotUser -> {
            spotUser.setIsDelete(1);
            elevatorSpotUserDao.save(spotUser);
        });

        for (String spaceId : req.getSpaceIds()) {
            Optional<Space> spaceOp = spaceDao.findById(spaceId);
            if (spaceOp.isEmpty()) {
                // 业务处理失败，手动回滚事务,返回异常
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error("空间ID有误,分配权限失败");
            }
            Space space = spaceOp.get();

            Optional<ElevatorSpot> elevatorSpotOp = elevatorSpotDao.findBySpaceId(spaceId);
            ElevatorSpot elevatorSpot = new ElevatorSpot();
            if (elevatorSpotOp.isEmpty()) {
                // 创建梯控点
                elevatorSpot.setName(space.getName() + "梯控点");
                elevatorSpot.setSpace(space);
                elevatorSpotDao.save(elevatorSpot);
            } else {
                elevatorSpot = elevatorSpotOp.get();
            }

            // 报存梯控人员权限
            ElevatorSpotUser elevatorSpotUser = new ElevatorSpotUser();
            elevatorSpotUser.setAccessEndTime(req.getAccessEndTime());
            elevatorSpotUser.setAccessStartTime(req.getAccessStartTime());
            elevatorSpotUser.setUserHrId(userSaveReq.getEmpNo());
            elevatorSpotUser.setUserName(userSaveReq.getName());
            elevatorSpotUser.setDeptId(userSaveReq.getDeptId());
            elevatorSpotUser.setIsStaff(userSaveReq.getIsStaff());
            elevatorSpotUser.setElevatorSpot(elevatorSpot);
            elevatorSpotUser.setSpace(space);
            elevatorSpotUserDao.save(elevatorSpotUser);
        }
        return ServiceResult.ok();
    }

    @Override
    public ServiceResult getFloorPage(ElevatorSpotPageReq req) {
        log.info("start to get floorPage, req:{}", JSONObject.toJSONString(req));
        Pageable pageable = PageRequest.of(req.getPage(), req.getSize(), Sort.by(req.getOrders()));
        Page<ElevatorSpot> elevatorSpotPage = elevatorSpotDao.findAll((root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> list = new ArrayList<>();
            // 按项目ID筛选
            if (StringUtils.isNotEmpty(req.getProjectId())) {
                list.add(criteriaBuilder.equal(root.get("space").get("project").get("id"), req.getProjectId()));
            }
            // 按姓名或者工号
            if (StringUtils.isNotEmpty(req.getSpaceId())) {
                Optional<Space> spaceOptional = spaceDao.findById(req.getSpaceId());
                if (spaceOptional.isEmpty()) {
                    log.warn("空间ID无效，spaceId:{}", req.getSpaceId());
                } else {
                    Project project = spaceOptional.get().getProject();
                    if (Objects.isNull(project)) {
                        log.warn("空间没有所属项目，spaceId:{}", req.getSpaceId());
                        list.add(criteriaBuilder.equal(root.get("space").get("id"), req.getSpaceId()));
                    } else {
                        List<Space> projectSpaceList = spaceDao.findAllByProjectIdAndIsShow(project.getId(), 1);
                        projectSpaceList.sort((s1, s2) -> ObjectUtils.compare(s1.getLevel(), s2.getLevel()));
                        List<String> spaceIdList = new ArrayList<>();
                        projectSpaceList.forEach(space -> {
                            if (space.getId().equals(req.getSpaceId())) {
                                spaceIdList.add(space.getId());
                            } else if (Objects.nonNull(space.getParent())) {
                                Space parent = space.getParent();
                                if (parent.getId().equals(req.getSpaceId()) || spaceIdList.contains(parent.getId())) {
                                    spaceIdList.add(space.getId());
                                }
                            }
                        });
                        if (CollectionUtils.isEmpty(spaceIdList)) {
                            log.warn("空间ID以及子节点数据获取异常，spaceId:{}", req.getSpaceId());
                        } else {
                            list.add(criteriaBuilder.in(root.get("space").get("id")).value(spaceIdList));
                        }
                    }
                }
            }
            return criteriaQuery.where(list.toArray(new Predicate[0])).getRestriction();
        }, pageable);

        List<ElevatorSpotFloorPageResp> list = new ArrayList<>();
        if (!elevatorSpotPage.isEmpty()) {
            elevatorSpotPage.get().forEach(elevatorSpot -> list.add(ElevatorSpotFloorPageResp.convert(elevatorSpot)));
            List<String> elevatorSpotIdList = list.stream().map(ElevatorSpotFloorPageResp::getId).collect(Collectors.toList());
            //批量获取梯控点的人员数量
            List<Object[]> objectsList = elevatorSpotUserDao.findElevatorSpotIdToPeopleNum(elevatorSpotIdList);
            Map<String, Integer> spotIdToNumMap = new HashMap<>();
            objectsList.forEach(objects -> spotIdToNumMap.put((String) objects[0], ((BigInteger) objects[1]).intValue()));
            //批量获取梯控点的绑定设备
            List<ElevatorSpotDeviceRelation> elevatorSpotDeviceRelationList = relationDao.findAllByElevatorSpotIdIn(elevatorSpotIdList);
            MultiValueMap<String, ElevatorSpotDeviceRelation> spotIdToRelationMap = new LinkedMultiValueMap<>();
            elevatorSpotDeviceRelationList.forEach(relation -> spotIdToRelationMap.add(relation.getElevatorSpot().getId(), relation));
            list.forEach(spot -> {
                spot.setPeopleNum(spotIdToNumMap.get(spot.getId()));
                if (Objects.isNull(spot.getPeopleNum())) {
                    spot.setPeopleNum(0);
                }
                List<ElevatorSpotDeviceRelation> relationList = spotIdToRelationMap.get(spot.getId());
                if (CollectionUtils.isEmpty(relationList)) {
                    spot.setDeviceList(new ArrayList<>());
                } else {
                    //梯控绑定设备按照绑定时间升序返回
                    relationList.sort((r1, r2) -> ObjectUtils.compare(r1.getCreatedTime(), r2.getCreatedTime()));
                    spot.setDeviceList(relationList.stream().map(ElevatorSpotDeviceRelation::getDevice).map(IotDeviceResp::convert)
                            .collect(Collectors.toList()));
                }
            });
        }
        PageBean<ElevatorSpotFloorPageResp> result = new PageBean<>();
        result.setList(list);
        result.setTotalPages(elevatorSpotPage.getTotalPages());
        result.setTotalElements(elevatorSpotPage.getTotalElements());
        result.setPage(pageable.getPageNumber());
        result.setSize(pageable.getPageSize());
        return ServiceResult.ok(result);
    }

    @Override
    @Transactional
    public ServiceResult bindDevice(ElevatorSpotBindDeviceReq req) {

        Optional<Space> spaceOptional = spaceDao.findById(req.getSpaceId());
        if (spaceOptional.isEmpty()) {
            // 业务处理失败，手动回滚事务,返回异常
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return ServiceResult.error("空间ID有误,空间不存在");
        }

        ElevatorSpot elevatorSpot = new ElevatorSpot();
        Optional<ElevatorSpot> elevatorSpotOptional = elevatorSpotDao.findBySpaceId(req.getSpaceId());
        if (elevatorSpotOptional.isEmpty()) {
            // 创建梯控点
            elevatorSpot.setName(spaceOptional.get().getName() + "梯控点");
            elevatorSpot.setSpace(spaceOptional.get());
            elevatorSpotDao.save(elevatorSpot);
        } else {
            elevatorSpot = elevatorSpotOptional.get();
        }

        for (String deviceId : req.getDeviceIds()) {
            Optional<IotDevice> deviceOptional = deviceDao.findById(deviceId);
            if (deviceOptional.isEmpty()) {
                // 业务处理失败，手动回滚事务,返回异常
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error("设备ID有误,绑定失败");
            }
            IotDevice device = deviceOptional.get();
            // 报存梯控点和设备关系
            ElevatorSpotDeviceRelation relation = new ElevatorSpotDeviceRelation();
            relation.setDevice(device);
            relation.setElevatorSpot(elevatorSpot);
            relationDao.save(relation);
        }

        return ServiceResult.ok();
    }

    @Override
    public ServiceResult unbindDevice(ElevatorSpotBindDeviceReq req) {

        Optional<Space> spaceOptional = spaceDao.findById(req.getSpaceId());
        if (spaceOptional.isEmpty()) {
            // 业务处理失败，手动回滚事务,返回异常
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return ServiceResult.error("空间ID有误,空间不存在");
        }
        Optional<ElevatorSpot> elevatorSpotOptional = elevatorSpotDao.findBySpaceId(req.getSpaceId());
        if (elevatorSpotOptional.isEmpty()) {
            // 业务处理失败，手动回滚事务,返回异常
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return ServiceResult.error("解绑失败,该楼层未创建梯控点");
        }

        for (String deviceId : req.getDeviceIds()) {
            Optional<ElevatorSpotDeviceRelation> relationOptional = relationDao.findByElevatorSpotIdAndDeviceId(elevatorSpotOptional.get().getId(), deviceId);
            if (relationOptional.isEmpty()) {
                // 业务处理失败，手动回滚事务,返回异常
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error("解绑失败,未绑定此设备");
            }
            ElevatorSpotDeviceRelation relation = relationOptional.get();
            relation.setIsDelete(1);
            relationDao.save(relation);
        }

        return ServiceResult.ok();
    }
}
