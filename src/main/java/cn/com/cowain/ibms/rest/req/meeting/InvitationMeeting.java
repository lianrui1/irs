package cn.com.cowain.ibms.rest.req.meeting;

import lombok.Data;

/**
 *
 * @author: yanzy
 * @date: 2022/5/13 15:36
 */
@Data
public class InvitationMeeting {

    //会议id
    private String meetingId;

    //roomId
    private String roomId;

    // 开始时间
    private String meetingStartTime;

    // 结束时间
    private String meetingEndTime;


}
