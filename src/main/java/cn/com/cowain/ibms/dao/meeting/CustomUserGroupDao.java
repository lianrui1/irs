package cn.com.cowain.ibms.dao.meeting;

import cn.com.cowain.ibms.entity.meeting.CustomUserGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CustomUserGroupDao extends JpaRepository<CustomUserGroup, String> {

    // 根据创建人工号获取用户组
    List<CustomUserGroup> findByOwnerEmpNoOrderByCreatedTimeDesc(String hrId);

    // 根据人员组名称查询人员组
    Optional<CustomUserGroup> findByName(String name);

    // 根据人员组名称和发起人查询人员组
    Optional<CustomUserGroup> findByNameAndOwnerEmpNo(String name , String hrId);
}
