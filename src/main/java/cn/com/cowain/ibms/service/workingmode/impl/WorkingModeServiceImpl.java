package cn.com.cowain.ibms.service.workingmode.impl;

import cn.com.cowain.ibms.dao.device.DeviceModeStatusDao;
import cn.com.cowain.ibms.dao.device.DeviceWorkingModeDao;
import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.dao.iot.SpaceDao;
import cn.com.cowain.ibms.dao.space.SpaceAdminDao;
import cn.com.cowain.ibms.dao.space.SpaceDeviceControlDao;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.office.DeviceModeStatus;
import cn.com.cowain.ibms.entity.office.DeviceWorkingMode;
import cn.com.cowain.ibms.entity.space.SpaceAdmin;
import cn.com.cowain.ibms.entity.space.SpaceDeviceControl;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.working_mode.DeviceModeStatusSaveReq;
import cn.com.cowain.ibms.rest.req.working_mode.WorkingModeSaveReq;
import cn.com.cowain.ibms.rest.req.working_mode.WorkingModeUpdateReq;
import cn.com.cowain.ibms.rest.resp.iot.DeviceWechatResp;
import cn.com.cowain.ibms.rest.resp.working_mode.DeviceModeStatusDetailResp;
import cn.com.cowain.ibms.rest.resp.working_mode.ManagementPageResp;
import cn.com.cowain.ibms.rest.resp.working_mode.WorkingModeDetailResp;
import cn.com.cowain.ibms.rest.resp.working_mode.WorkingModePageResp;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.office.IntelligentOfficeDeviceService;
import cn.com.cowain.ibms.service.space.SpaceService;
import cn.com.cowain.ibms.service.workingmode.WorkingModeService;
import cn.com.cowain.sfp.comm.utils.JwtUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/10/19 10:42
 */
@Slf4j
@Service
public class WorkingModeServiceImpl implements WorkingModeService {

    @Autowired
    private DeviceWorkingModeDao workingModeDao;

    @Autowired
    private SpaceDao spaceDao;

    @Autowired
    private SpaceAdminDao spaceAdminDao;

    @Autowired
    private DeviceModeStatusDao modeStatusDao;

    @Autowired
    private DeviceDao deviceDao;

    @Autowired
    private SpaceService spaceService;

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserServiceUC;

    @Autowired
    private SpaceDeviceControlDao spaceDeviceControlDao;

    @Autowired
    private IntelligentOfficeDeviceService intelligentOfficeDeviceService;


    @Override
    @Transactional
    public ServiceResult create(String hrId, WorkingModeSaveReq req) {

        Optional<Space> spaceOp = spaceDao.findById(req.getSpaceId());
        if(spaceOp.isEmpty()){
            return ServiceResult.error("添加失败,空间不存在");
        }

        // 判断创建人是否是该空间管理员
        Optional<SpaceAdmin> spaceAdminOp = spaceAdminDao.findBySpaceIdAndAdminEmpNo(req.getSpaceId(), hrId);
        if(spaceAdminOp.isEmpty()){
            return ServiceResult.error("添加失败,无权限");
        }

        // 判断该空间下该模式是否已经存在
        Optional<DeviceWorkingMode> workingModeOp = workingModeDao.findBySpaceIdAndName(req.getSpaceId(), req.getName());
        if(workingModeOp.isPresent()){
            return ServiceResult.error("此空间下该模式已存在,不可重复创建");
        }

        // 工作模式
        DeviceWorkingMode workingMode = new DeviceWorkingMode();
        workingMode.setIsDefault(1);
        workingMode.setName(req.getName());
        workingMode.setImg(req.getImgUrl());
        workingMode.setSpace(spaceOp.get());
        // 保存
        workingModeDao.save(workingMode);

        // 保存模式下对应设备
        for(DeviceModeStatusSaveReq modeStatusSaveReq : req.getSaveReqs()){

            Optional<IotDevice> deviceOp = deviceDao.findByHwDeviceId(modeStatusSaveReq.getDeviceId());
            if(deviceOp.isEmpty()){
                // 业务处理失败，手动回滚事务,返回异常
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error("设备不存在，绑定失败");
            }

            // 设备状态
            IotDevice device = deviceOp.get();
            DeviceModeStatus modeStatus = new DeviceModeStatus();
            if(modeStatusSaveReq.isStatus()){
                modeStatus.setStatus("on");
                if(device.getDeviceType().equals(DeviceType.CURTAINS) || device.getDeviceType().equals(DeviceType.DOOR_MAGNETIC)){
                    modeStatus.setStatus("open");
                }
            }else {
                modeStatus.setStatus("off");
                if(device.getDeviceType().equals(DeviceType.CURTAINS) || device.getDeviceType().equals(DeviceType.DOOR_MAGNETIC)){
                    modeStatus.setStatus("close");
                }
            }
            modeStatus.setDevice(device);
            modeStatus.setMode(workingMode);
            modeStatusDao.save(modeStatus);
        }
        return ServiceResult.ok();
    }

    @Override
    public PageBean<WorkingModePageResp> getPage(String spaceId, Pageable pageable) {

        Page<DeviceWorkingMode> workingModePage = workingModeDao.findBySpaceId(spaceId, pageable);

        PageBean<WorkingModePageResp> pageBean = new PageBean<>();

        List<WorkingModePageResp> modePageResps = new ArrayList<>();
        workingModePage.getContent().forEach(it ->{
            WorkingModePageResp modePageResp = new WorkingModePageResp();
            modePageResp.setId(it.getId());
            modePageResp.setImgUrl(it.getImg());
            modePageResp.setName(it.getName());
            modePageResp.setIsDefault(it.getIsDefault());
            modePageResps.add(modePageResp);
        });

        pageBean.setTotalElements(workingModePage.getTotalElements());
        pageBean.setPage(pageable.getPageNumber());
        pageBean.setSize(pageable.getPageSize());
        pageBean.setTotalPages(workingModePage.getTotalPages());
        pageBean.setList(modePageResps);
        return pageBean;
    }

    @Override
    public ServiceResult delete(String id, String hrId) {

        // 判断模式是否存在
        Optional<DeviceWorkingMode> workingModeOp = workingModeDao.findById(id);
        if(workingModeOp.isEmpty()){
            return ServiceResult.error("id有误,删除失败");
        }
        DeviceWorkingMode workingMode = workingModeOp.get();

        if(workingMode.getIsDefault() == 0){
            return ServiceResult.error("默认模式不可删除");
        }

        // 判断创建人是否是该空间管理员
        Optional<SpaceAdmin> spaceAdminOp = spaceAdminDao.findBySpaceIdAndAdminEmpNo(workingMode.getSpace().getId(), hrId);
        if(spaceAdminOp.isEmpty()){
            return ServiceResult.error("删除失败,无权限");
        }

        // 删除模式下设备
        List<DeviceModeStatus> modeStatusList = modeStatusDao.findByModeId(id);
        modeStatusList.forEach(it -> it.setIsDelete(1));
        modeStatusDao.saveAll(modeStatusList);

        // 删除模式
        workingMode.setIsDelete(1);
        workingModeDao.save(workingMode);

        return ServiceResult.ok();
    }

    @Override
    public ServiceResult detail(String id) {

        Optional<DeviceWorkingMode> workingModeOp = workingModeDao.findById(id);
        if(workingModeOp.isEmpty()){
            return ServiceResult.error("id有误,模式不存在");
        }
        DeviceWorkingMode workingMode = workingModeOp.get();

        // 详情返回对象
        WorkingModeDetailResp workingModeDetailResp = new WorkingModeDetailResp();
        workingModeDetailResp.setId(workingMode.getId());
        workingModeDetailResp.setImgUrl(workingMode.getImg());
        workingModeDetailResp.setName(workingMode.getName());
        workingModeDetailResp.setSpaceId(workingMode.getSpace().getId());
        workingModeDetailResp.setSpaceName(workingMode.getSpace().getName());
        workingModeDetailResp.setIsDefault(workingMode.getIsDefault());

        // 模式下设备
        List<DeviceModeStatusDetailResp> detailResps = new ArrayList<>();
        List<DeviceModeStatus> modeStatusList = modeStatusDao.findByModeId(workingMode.getId());
        modeStatusList.forEach(it ->{

            DeviceModeStatusDetailResp statusDetailResp = new DeviceModeStatusDetailResp();
            IotDevice device = it.getDevice();
            statusDetailResp.setId(device.getId());
            statusDetailResp.setDeviceName(device.getDeviceName());
            if(it.getStatus().equals("on") || it.getStatus().equals("open")){
                statusDetailResp.setStatus(true);
            }else {
                statusDetailResp.setStatus(false);
            }
            statusDetailResp.setDeviceType(device.getDeviceType());
            statusDetailResp.setHwDeviceId(device.getHwDeviceId());
            detailResps.add(statusDetailResp);
        });

        workingModeDetailResp.setDetailResps(detailResps);

        return ServiceResult.ok(workingModeDetailResp);
    }

    @Override
    @Transactional
    public ServiceResult managementCreate(String id, List<String> empNos, String token, String isApplet) {

        // 从token中解析员工工号
        String hrId = JwtUtil.getHrId(token);

        Optional<Space> spaceOp = spaceDao.findById(id);
        if(spaceOp.isEmpty()){
            return ServiceResult.error("空间不存在,添加失败");
        }

        Optional<SpaceAdmin> spaceAdminOp = spaceAdminDao.findBySpaceIdAndAdminEmpNo(id, hrId);
        if(spaceAdminOp.isEmpty()){
            return ServiceResult.error("您不是管理员，无权限添加");
        }

        // 获取空间下设备
        List<String> spaceIds = new ArrayList<>();
        List<DeviceWechatResp> deviceWechatRespList = new ArrayList<>();

        spaceIds.add(spaceOp.get().getId());
        // 获取空间下设备
        if(StringUtils.isNotEmpty(isApplet) && "1".equals(isApplet)){
            intelligentOfficeDeviceService.getListBySpace(spaceIds, spaceOp.get());
        }

        spaceIds.forEach(spaceId ->{
            ServiceResult result = spaceService.getDeviceListControl(spaceId);
            if(result.isSuccess()){
                deviceWechatRespList.addAll((List<DeviceWechatResp>)result.getObject());
            }
        });
        /*ServiceResult result = spaceService.getDeviceListControl(id);
        if(!result.isSuccess()){
            return ServiceResult.error(String.valueOf(result.getObject()));
        }
        List<DeviceWechatResp> deviceWechatRespList = (List<DeviceWechatResp>)result.getObject();*/

        // 循环保存使用者设备权限
        for(String it : empNos){
            SysUser userByEmpNo;
            try {
                userByEmpNo = sysUserServiceUC.getUserByEmpNo(it);
            }catch (Exception e){
                return ServiceResult.error("添加失败,人员不存在 " + it);
            }

            if(userByEmpNo != null){
                deviceWechatRespList.forEach(device -> {
                    Optional<IotDevice> iotDeviceOp = deviceDao.findByHwDeviceId(device.getDeviceId());
                    SpaceDeviceControl spaceDeviceControl = new SpaceDeviceControl();

                    SysUser sysUser = sysUserServiceUC.getUserByEmpNo(it);
                    if(sysUser == null){
                        log.error("添加人员权限失败，未找到用户{}", it);
                        return ;
                    }

                    if(iotDeviceOp.isPresent()){
                        spaceDeviceControl.setDevice(iotDeviceOp.get());
                        spaceDeviceControl.setUserEmpNo(it);
                        spaceDeviceControl.setUserName(sysUser.getNameZh());

                        Optional<SpaceDeviceControl> spaceDeviceControlOp = spaceDeviceControlDao.findByUserEmpNoAndDeviceId(it, iotDeviceOp.get().getId());
                        // 如果使用者没有当前设备权限则保存
                        if(spaceDeviceControlOp.isEmpty()){
                            spaceDeviceControlDao.save(spaceDeviceControl);
                        }
                    }
                });
            }else {
                // 业务处理失败，手动回滚事务,返回异常
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error("工号不存在，添加失败");
            }
        }
//
//        // 向uc添加入库权限
//        PassportRoleEditReq passportRoleEditReq = new PassportRoleEditReq();
//        passportRoleEditReq.setAddUserHrIdList(empNos);
//        passportRoleEditReq.setRmUserHrIdList(new ArrayList<>());
//        JsonResult<String> stringJsonResult = userCenterRoleApi.addMenuForIntelligentOffice(token, passportRoleEditReq);
//        if (stringJsonResult.getStatus() == 0) {
//            // 业务处理失败，手动回滚事务,返回异常
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            return ServiceResult.error("向uc添加权限失败:" + stringJsonResult.getMsg());
//        }
//        log.info("向uc添加权限成功:" + JSON.toJSONString(passportRoleEditReq));


        return ServiceResult.ok();
    }

    @Override
    public PageBean<ManagementPageResp> managementPage(String id, int page, int size) {

        List<SpaceDeviceControl> controlList = new ArrayList<>();

        List<SpaceDeviceControl> deviceControlList = spaceDeviceControlDao.findByDeviceSpaceIdOrderByCreatedTime(id);
        //deviceControlList = deviceControlList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(SpaceDeviceControl :: getUserEmpNo))), ArrayList :: new));
        for(SpaceDeviceControl control : deviceControlList){
            if(!controlList.toString().contains(control.getUserEmpNo())){
                controlList.add(control);
            }
        }

        List<ManagementPageResp> managementList = new ArrayList<>();


        //  查询空间管理员，将管理员放在前列
        List<SpaceAdmin> spaceAdminList = spaceAdminDao.findBySpaceId(id);
        List<String> spaceAdminHrIdList = spaceAdminList.stream().map(SpaceAdmin::getAdminEmpNo).collect(Collectors.toList());
        spaceAdminList = spaceAdminList.stream().sorted(Comparator.comparing(SpaceAdmin::getCreatedTime).reversed()).collect(Collectors.toList());

        // 查询所有空间管理员列表
        List<ManagementPageResp> adminResp = spaceAdminList.stream()
                .sorted(Comparator.comparing(SpaceAdmin::getCreatedTime).reversed())
                .map(obj -> {
                    SysUser sysUser = sysUserServiceUC.getUserByEmpNo(obj.getAdminEmpNo());
                    return ManagementPageResp.builder()
                            .empNo(sysUser.getEmpNo())
                            .name(sysUser.getNameZh())
                            .dept(sysUser.getFullDepartmentName())
                            .img(sysUser.getHeadImgUrl())
                            .isAdmin(0)
                            .build();
                })
                .collect(Collectors.toList());

        // 将空间管理员数据输出到列表的最上方
        managementList.addAll(adminResp);

        controlList.forEach(it ->{

            if(spaceAdminHrIdList.contains(it.getUserEmpNo())){
                return ;
            }

            ManagementPageResp management = new ManagementPageResp();
            // 根据工号获取人员信息
            SysUser userByEmpNo = sysUserServiceUC.getUserByEmpNo(it.getUserEmpNo());
            management.setIsAdmin(1);
            Optional<SpaceAdmin> spaceAdminOp = spaceAdminDao.findBySpaceIdAndAdminEmpNo(id, it.getUserEmpNo());
            if(spaceAdminOp.isPresent()){
                management.setIsAdmin(0);
            }
            management.setDept(userByEmpNo.getDeptName());
            management.setEmpNo(userByEmpNo.getEmpNo());
            management.setImg(userByEmpNo.getHeadImgUrl());
            management.setName(userByEmpNo.getNameZh());
            managementList.add(management);
        });
        return PageBean.createPageBeanByAllData(managementList, page , size);
    }

    @Override
    @Transactional
    public ServiceResult managementDelete(String id, List<String> empNos, String token) {

        // 从token中解析员工工号
        String hrId = JwtUtil.getHrId(token);


        Optional<Space> spaceOp = spaceDao.findById(id);
        if(spaceOp.isEmpty()){
            return ServiceResult.error("空间不存在,删除失败");
        }

        Optional<SpaceAdmin> spaceAdminOp = spaceAdminDao.findBySpaceIdAndAdminEmpNo(id, hrId);
        if(spaceAdminOp.isEmpty()){
            return ServiceResult.error("您不是管理员，无权限删除");
        }

        for(String it : empNos){
            Optional<SpaceAdmin> adminOp = spaceAdminDao.findBySpaceIdAndAdminEmpNo(id, it);
            if(adminOp.isPresent()){
                // 业务处理失败，手动回滚事务,返回异常
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error(it + "该成员是管理员,无法删除");
            }
            List<SpaceDeviceControl> controlList = spaceDeviceControlDao.findByDeviceSpaceIdAndUserEmpNo(id, it);
            controlList.forEach(control -> control.setIsDelete(1));
            spaceDeviceControlDao.saveAll(controlList);
        }

//        // 向uc删除入口权限
//        PassportRoleEditReq passportRoleEditReq = new PassportRoleEditReq();
//        passportRoleEditReq.setAddUserHrIdList(new ArrayList<>());
//        passportRoleEditReq.setRmUserHrIdList(empNos);
//        JsonResult<String> stringJsonResult = userCenterRoleApi.addMenuForIntelligentOffice(token, passportRoleEditReq);
//        if (stringJsonResult.getStatus() == 0) {
//            // 业务处理失败，手动回滚事务,返回异常
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            return ServiceResult.error("向uc删除权限失败:" + stringJsonResult.getMsg());
//        }
//        log.info("向uc删除权限成功:" + JSON.toJSONString(passportRoleEditReq));

        return ServiceResult.ok();
    }

    @Override
    public ServiceResult rule(String spaceId, String hrId) {

        Optional<Space> spaceOp = spaceDao.findById(spaceId);
        if(spaceOp.isEmpty()){
            return ServiceResult.error("空间不存在");
        }

        Optional<SpaceAdmin> spaceAdminOp = spaceAdminDao.findBySpaceIdAndAdminEmpNo(spaceId, hrId);
        if(spaceAdminOp.isEmpty()){
            return ServiceResult.ok(1);
        }
        return ServiceResult.ok(0);
    }

    @Override
    @Transactional
    public ServiceResult update(String hrId, String id, WorkingModeUpdateReq req) {

        Optional<DeviceWorkingMode> workingModeOp = workingModeDao.findById(id);
        if(workingModeOp.isEmpty()){
            return ServiceResult.error("模式不存在,编辑失败");
        }
        DeviceWorkingMode workingMode = workingModeOp.get();

        // 判断创建人是否是该空间管理员
        Optional<SpaceAdmin> spaceAdminOp = spaceAdminDao.findBySpaceIdAndAdminEmpNo(workingMode.getSpace().getId(), hrId);
        if(spaceAdminOp.isEmpty()){
            return ServiceResult.error("编辑失败,无权限");

        }

        Optional<DeviceWorkingMode> bySpaceIdAndNameOp = workingModeDao.findBySpaceIdAndName(workingMode.getSpace().getId(), req.getName());
        if(bySpaceIdAndNameOp.isPresent() && !bySpaceIdAndNameOp.get().getId().equals(workingMode.getId())){
            return ServiceResult.error("编辑失败,名称已存在");
        }

        workingMode.setImg(req.getImgUrl());
        workingMode.setName(req.getName());
        workingModeDao.save(workingMode);

        List<DeviceModeStatus> deviceModeStatusList = modeStatusDao.findByModeId(id);
        deviceModeStatusList.forEach(it -> it.setIsDelete(1));

        for(DeviceModeStatusSaveReq statusSaveReq : req.getSaveReqs()){
            Optional<IotDevice> deviceOp = deviceDao.findByHwDeviceId(statusSaveReq.getDeviceId());
            if(deviceOp.isEmpty()){
                // 业务处理失败，手动回滚事务,返回异常
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error("设备不存在，绑定失败");
            }

            // 设备状态
            IotDevice device = deviceOp.get();
            DeviceModeStatus modeStatus = new DeviceModeStatus();
            Optional<DeviceModeStatus> modeStatusOp = modeStatusDao.findByModeIdAndDeviceId(workingMode.getId(), device.getId());
            if(modeStatusOp.isPresent()){
                modeStatus = modeStatusOp.get();
            }
            if(statusSaveReq.isStatus()){
                modeStatus.setStatus("on");
                if(device.getDeviceType().equals(DeviceType.CURTAINS) || device.getDeviceType().equals(DeviceType.DOOR_MAGNETIC)){
                    modeStatus.setStatus("open");
                }
            }else {
                modeStatus.setStatus("off");
                if(device.getDeviceType().equals(DeviceType.CURTAINS) || device.getDeviceType().equals(DeviceType.DOOR_MAGNETIC)){
                    modeStatus.setStatus("close");
                }
            }
            modeStatus.setDevice(device);
            modeStatus.setMode(workingMode);
            deviceModeStatusList.add(modeStatus);
        }
        modeStatusDao.saveAll(deviceModeStatusList);
        return ServiceResult.ok();
    }

    @Override
    public ServiceResult managementDeletePc(String spaceId, List<String> empNos) {

        List<SpaceDeviceControl> controls = new ArrayList<>();

        // 判断人员是否是管理员
        for(String empNo : empNos){
            Optional<SpaceAdmin> spaceAdminOp = spaceAdminDao.findBySpaceIdAndAdminEmpNo(spaceId, empNo);
            if(spaceAdminOp.isPresent()){
                return ServiceResult.error("管理员不可删除:" + spaceAdminOp.get().getAdminName());
            }
            List<SpaceDeviceControl> controlList = spaceDeviceControlDao.findByDeviceSpaceIdAndUserEmpNo(spaceId, empNo);
            controlList.forEach(it -> it.setIsDelete(1));
            controls.addAll(controlList);
        }
        spaceDeviceControlDao.saveAll(controls);

        return ServiceResult.ok();
    }
}
