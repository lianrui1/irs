package cn.com.cowain.ibms.rest.ota;

import cn.com.cowain.ibms.enumeration.ota.Application;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.Sorts;
import cn.com.cowain.ibms.rest.req.oat.UpgradePageReq;
import cn.com.cowain.ibms.rest.req.oat.UpgradeTaskCreateReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.ota.FileResp;
import cn.com.cowain.ibms.rest.resp.ota.UpgradeTaskDetailResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.ota.AppUpgradeService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author Yang.Lee
 * @date 2021/4/14 17:09
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/otaUpgrade")
@Api(tags = IConst.MODULE_OTA_UPGRADE)
public class AppUpgradeController {

    @Autowired
    private AppUpgradeService appUpgradeService;

    @PostMapping
    @ApiOperation(value = "创建升级任务", tags = IConst.MODULE_OTA_UPGRADE)
    public ResponseEntity<JsonResult<IdResp>> create(@RequestBody @Validated UpgradeTaskCreateReq upgradeTaskCreateReq,
                                                     BindingResult bindingResult) {
        log.debug(upgradeTaskCreateReq.toString());
        ServiceResult result = appUpgradeService.save(upgradeTaskCreateReq);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @PostMapping("/upload")
    @ApiOperation(value = "文件上传", tags = IConst.MODULE_OTA_UPGRADE)
    public ResponseEntity<JsonResult<FileResp>> upload(MultipartFile multipartFile) {
        log.debug("文件上传: " + multipartFile);
        ServiceResult result = appUpgradeService.upload(multipartFile);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.ok("ok", (FileResp)result.getObject()));
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "编辑升级任务", tags = IConst.MODULE_OTA_UPGRADE)
    public ResponseEntity<JsonResult<String>> update(@PathVariable  String id,
                                                     @RequestBody @Validated UpgradeTaskCreateReq upgradeTaskCreateReq,
                                                     BindingResult bindingResult){
        // 变更
        ServiceResult service = appUpgradeService.update(id,upgradeTaskCreateReq);
        JsonResult<String> jsonResult = JsonResult.okNoData("OK");
        if (!service.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(service.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(jsonResult);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除升级任务", tags = IConst.MODULE_OTA_UPGRADE)
    public ResponseEntity<JsonResult<String>> delete(@PathVariable  String id){
        // 删除
        ServiceResult service = appUpgradeService.delete(id);
        JsonResult<String> jsonResult = JsonResult.okNoData("OK");
        if (!service.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(service.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(jsonResult);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "获取升级任务详情", tags = IConst.MODULE_OTA_UPGRADE)
    public ResponseEntity<JsonResult<UpgradeTaskDetailResp>> get(@PathVariable String id){
        log.debug("查询升级任务详情: " + id);

        ServiceResult result = appUpgradeService.get(id);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.ok("ok", (UpgradeTaskDetailResp)result.getObject()));
    }

    @GetMapping("/search/page")
    @ApiOperation(value = "获取升级任务分页数据", tags = IConst.MODULE_OTA_UPGRADE)
    public ResponseEntity<JsonResult<PageBean<UpgradeTaskDetailResp>>> get(UpgradePageReq req){
        // 默认根据创建时间倒叙查询数据
        req.addOrderDesc(Sorts.CREATED_TIME);

        PageBean<UpgradeTaskDetailResp> pageBean = appUpgradeService.search(req);

        return ResponseEntity.ok(JsonResult.ok("ok" ,pageBean));
    }

//    @GetMapping("/task")
//    @ApiOperation(value = "App查询是否有升级任务", tags = IConst.MODULE_OTA_UPGRADE)
//    public ResponseEntity<JsonResult<UpgradeTaskDetailResp>> getTask(@RequestParam @ApiParam(value = "当前版本", required = true) String versionOTA,
//                                                                     @RequestParam @ApiParam(value = "升级应用", required = true, example = "MEETING_AUDIO_APP: 智能语音助手； DOORPLATE_PAD：智能会议") Application application){
//
//        ServiceResult result = appUpgradeService.getTask(application, versionOTA);
//
//        return ResponseEntity.ok(JsonResult.ok("ok", (UpgradeTaskDetailResp)result.getObject()));
//    }

    @GetMapping("/download/{id}")
    @ApiOperation(value = "App下载升级包", tags = IConst.MODULE_OTA_UPGRADE)
    public ResponseEntity<JsonResult<String>> download(@PathVariable String id){
        log.debug("App下载升级包: " + id);

        ServiceResult result = appUpgradeService.download(id);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.okNoData("ok"));
    }
}
