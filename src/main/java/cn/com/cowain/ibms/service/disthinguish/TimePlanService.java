package cn.com.cowain.ibms.service.disthinguish;

import cn.com.cowain.ibms.rest.resp.distinguish.TimePlanListResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 时间计划Service
 *
 * @author Yang.Lee
 * @date 2021/3/11 13:34
 */
public interface TimePlanService {

    /**
     * 获取时间计划列表
     *
     * @return 时间计划列表
     * @author Yang.Lee
     * @date 2021/3/11 13:35
     **/
    List<TimePlanListResp> getList();

    /**
     * 判断时间计划是否存在
     *
     * @param planIds 时间计划ID
     * @return 查询结果。若时间计划存在，则返回的ServiceResult对象中包含该时间计划数据
     * @author Yang.Lee
     * @date 2021/4/7 9:54
     **/
    ServiceResult isPlanExist(@NotNull String planIds);
}
