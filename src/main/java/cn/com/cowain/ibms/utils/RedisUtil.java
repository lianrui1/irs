package cn.com.cowain.ibms.utils;

import cn.com.cowain.ibms.exceptions.RedisFailedException;
import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static cn.com.cowain.ibms.utils.IConst.EXCEPTION;

/**
 * Redis Util
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/10/20
 */
@Log4j2
@Component
public class RedisUtil {

    public static final long ONE_DAY_SECOND = 86400L;

    public static final long TWO_DAY_SECOND = 172800L;

    public static final long FIFTEEN_MIN_SECOND = 900L;

    public static final long HALF_AN_HOUR_SECOND = 1800L;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    public RedisUtil(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    // 环境
    @Value("${spring.profiles.active}")
    private String env;

    @Value("${redis.key.projectName}")
    private String projectName;

    /**
     * 指定缓存失效时间
     *
     * @param key  键
     * @param time 时间(秒)
     * @return
     */
    public boolean expire(String key, long time) {
        try {
            if (time > 0) {
                redisTemplate.expire(key, time, TimeUnit.SECONDS);
            }
            return true;
        } catch (Exception e) {
            log.error(EXCEPTION, e);
            return false;
        }
    }

    /**
     * 根据key 获取过期时间
     *
     * @param key 键 不能为null
     * @return 时间(秒) 返回0代表为永久有效
     */
    public Long getExpire(String key) {
        return redisTemplate.getExpire(key, TimeUnit.SECONDS);
    }

    /**
     * 判断key是否存在
     *
     * @param key 键
     * @return true 存在 false不存在
     */
    public Boolean hasKey(String key) {
        try {
            return redisTemplate.hasKey(key);
        } catch (Exception e) {
            log.error(EXCEPTION, e);
            return false;
        }
    }

    /**
     * 删除缓存
     *
     * @param key 可以传一个值 或多个
     */
    @SuppressWarnings("unchecked")
    public void del(String... key) {
        if (key != null && key.length > 0) {
            if (key.length == 1) {
                redisTemplate.delete(key[0]);
            } else {
                redisTemplate.delete(CollectionUtils.arrayToList(key));
            }
        }
    }

    //============================String=============================

    /**
     * 普通缓存获取
     *
     * @param key 键
     * @return 值
     */
    public Object get(String key) {
        if (key == null) {
            return null;
        } else {
            return redisTemplate.opsForValue().get(key);
        }
    }

    /**
     * 普通缓存放入
     *
     * @param key   键
     * @param value 值
     * @return true成功 false失败
     */
    public boolean set(String key, Object value) {
        try {
            redisTemplate.opsForValue().set(key, value);
            return true;
        } catch (Exception e) {
            log.error(EXCEPTION, e);
            return false;
        }
    }

    /**
     * 普通缓存放入并设置时间
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒) time要大于0 如果time小于等于0 将设置无限期
     * @return true成功 false 失败
     */
    public boolean set(String key, Object value, long time) {
        try {
            if (time > 0) {
                redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
            } else {
                set(key, value);
            }
            return true;
        } catch (Exception e) {
            log.error(EXCEPTION, e);
            return false;
        }
    }

    /**
     * 递增
     *
     * @param key   键
     * @param delta 要增加几(大于0)
     * @return
     */
    public Long incr(String key, long delta) {
        if (delta < 0) {
            throw new RedisFailedException("递增因子必须大于0");
        }
        return redisTemplate.opsForValue().increment(key, delta);
    }

    /**
     * 递减
     *
     * @param key   键
     * @param delta 要减少几(小于0)
     * @return
     */
    public Long decr(String key, long delta) {
        if (delta < 0) {
            throw new RedisFailedException("递减因子必须大于0");
        }
        return redisTemplate.opsForValue().increment(key, -delta);
    }

    /**
     * 普通缓存获取
     *
     * @param keyList 键
     * @return 值
     */
    public List<Object> multiGet(List<String> keyList) {
        if (CollUtil.isEmpty(keyList)) {
            return null;
        } else {
            return redisTemplate.opsForValue().multiGet(keyList);
        }
    }

    //================================Map=================================

    /**
     * HashGet
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     * @return 值
     */
    public Object hget(String key, String item) {
        return redisTemplate.opsForHash().get(key, item);
    }

    /**
     * 获取hashKey对应的所有键值
     *
     * @param key 键
     * @return 对应的多个键值
     */
    public Map<Object, Object> hmget(String key) {
        return redisTemplate.opsForHash().entries(key);
    }

    /**
     * HashSet
     *
     * @param key 键
     * @param map 对应多个键值
     * @return true 成功 false 失败
     */
    public boolean hmset(String key, Map<String, Object> map,boolean isExpire) {
        try {
            redisTemplate.opsForHash().putAll(key, map);
            if (isExpire){
                redisTemplate.expire(key,24,TimeUnit.HOURS);
            }
            return true;
        } catch (Exception e) {
            log.error(EXCEPTION, e);
            return false;
        }
    }

    /**
     * HashSet 并设置时间
     *
     * @param key  键
     * @param map  对应多个键值
     * @param time 时间(秒)
     * @return true成功 false失败
     */
    public boolean hmset(String key, Map<String, Object> map, long time) {
        try {
            redisTemplate.opsForHash().putAll(key, map);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            log.error(EXCEPTION, e);
            return false;
        }
    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @return true 成功 false失败
     */
    public boolean hset(String key, String item, Object value) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            return true;
        } catch (Exception e) {
            log.error(EXCEPTION, e);
            return false;
        }
    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @param time  时间(秒)  注意:如果已存在的hash表有时间,这里将会替换原有的时间
     * @return true 成功 false失败
     */
    public boolean hset(String key, String item, Object value, long time) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            log.error(EXCEPTION, e);
            return false;
        }
    }

    /**
     * 删除hash表中的值
     *
     * @param key  键 不能为null
     * @param item 项 可以使多个 不能为null
     */
    public void hdel(String key, Object... item) {
        redisTemplate.opsForHash().delete(key, item);
    }

    /**
     * 判断hash表中是否有该项的值
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     * @return true 存在 false不存在
     */
    public boolean hHasKey(String key, String item) {
        return redisTemplate.opsForHash().hasKey(key, item);
    }

    /**
     * hash递增 如果不存在,就会创建一个 并把新增后的值返回
     *
     * @param key  键
     * @param item 项
     * @param by   要增加几(大于0)
     * @return
     */
    public double hincr(String key, String item, double by) {
        return redisTemplate.opsForHash().increment(key, item, by);
    }

    /**
     * hash递减
     *
     * @param key  键
     * @param item 项
     * @param by   要减少记(小于0)
     * @return
     */
    public double hdecr(String key, String item, double by) {
        return redisTemplate.opsForHash().increment(key, item, -by);
    }

    //============================set=============================

    /**
     * 根据key获取Set中的所有值
     *
     * @param key 键
     * @return
     */
    public Set<Object> sGet(String key) {
        try {
            return redisTemplate.opsForSet().members(key);
        } catch (Exception e) {
            log.error(EXCEPTION, e);
            return Collections.emptyNavigableSet();
        }
    }

    /**
     * 根据value从一个set中查询,是否存在
     *
     * @param key   键
     * @param value 值
     * @return true 存在 false不存在
     */
    public Boolean sHasKey(String key, Object value) {
        try {
            return redisTemplate.opsForSet().isMember(key, value);
        } catch (Exception e) {
            log.error(EXCEPTION, e);
            return false;
        }
    }

    /**
     * 将数据放入set缓存
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 成功个数
     */
    public Long sSet(String key, Object... values) {
        try {
            return redisTemplate.opsForSet().add(key, values);
        } catch (Exception e) {
            log.error(EXCEPTION, e);
            return 0L;
        }
    }

    /**
     * 将set数据放入缓存
     *
     * @param key    键
     * @param time   时间(秒)
     * @param values 值 可以是多个
     * @return 成功个数
     */
    public Long sSetAndTime(String key, long time, Object... values) {
        try {
            Long count = redisTemplate.opsForSet().add(key, values);
            if (time > 0) {
                expire(key, time);
            }
            return count;
        } catch (Exception e) {
            log.error(EXCEPTION, e);
            return 0L;
        }
    }

    /**
     * 获取set缓存的长度
     *
     * @param key 键
     * @return
     */
    public Long sGetSetSize(String key) {
        try {
            return redisTemplate.opsForSet().size(key);
        } catch (Exception e) {
            log.error(EXCEPTION, e);
            return 0L;
        }
    }

    /**
     * 移除值为value的
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 移除的个数
     */
    public Long setRemove(String key, Object... values) {
        try {
            return redisTemplate.opsForSet().remove(key, values);
        } catch (Exception e) {
            log.error(EXCEPTION, e);
            return 0L;
        }
    }
    //===============================list=================================

    /**
     * 获取list缓存的内容
     *
     * @param key   键
     * @param start 开始
     * @param end   结束  0 到 -1代表所有值
     * @return
     */
    public List<? extends Object> lGet(String key, long start, long end) {
        try {
            return redisTemplate.opsForList().range(key, start, end);
        } catch (Exception e) {
            log.error(EXCEPTION, e);
            return Collections.emptyList();
        }
    }

    /**
     * 获取list缓存的内容
     *
     * @param key   键
     * @param start 开始
     * @param end   结束  0 到 -1代表所有值
     * @return
     */
    public  <T> List<T> lGet(String key, long start, long end, Class<T> tClass) {
        try {
            List<Object> array=redisTemplate.opsForList().range(key, start, end);
            List<T> tList=new LinkedList<>();
            if(!CollectionUtils.isEmpty(array)){
                Objects.requireNonNull(array).forEach(arrObject->{
                    JSONObject jsonObject= (JSONObject) arrObject;
                    try {
                        T t= tClass.getDeclaredConstructor().newInstance();
                        Field[] fields=tClass.getDeclaredFields();
                        for (Field field:fields){
                            field.setAccessible(true);
                            field.set(t,jsonObject.get(field.getName()));
                        }
                        tList.add(t);
                    } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                        log.info("类转换异常！");
                        e.printStackTrace();
                    }
                });
            }

            return tList;
        } catch (Exception e) {
            log.error(EXCEPTION, e);
            return Collections.emptyList();
        }
    }
    /**
     * 获取list缓存的长度
     *
     * @param key 键
     * @return
     */
    public Long lGetListSize(String key) {
        try {
            return redisTemplate.opsForList().size(key);
        } catch (Exception e) {
            log.error(EXCEPTION, e);
            return 0L;
        }
    }

    /**
     * 通过索引 获取list中的值
     *
     * @param key   键
     * @param index 索引  index>=0时， 0 表头，1 第二个元素，依次类推；index<0时，-1，表尾，-2倒数第二个元素，依次类推
     * @return
     */
    public Object lGetIndex(String key, long index) {
        try {
            return redisTemplate.opsForList().index(key, index);
        } catch (Exception e) {
            log.error(EXCEPTION, e);
            return null;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @return
     */
    public boolean lSet(String key, Object value,boolean isExpire) {
        try {
            redisTemplate.opsForList().rightPush(key, value);
            if(isExpire){
                redisTemplate.expire(key,36,TimeUnit.HOURS);
            }
            return true;
        } catch (Exception e) {
            log.error(EXCEPTION, e);
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒)
     * @return
     */
    public boolean lSet(String key, Object value, long time) {
        try {
            redisTemplate.opsForList().rightPush(key, value);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            log.error(EXCEPTION, e);
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @return
     */
    public boolean lSet(String key, List<Object> value) {
        try {
            redisTemplate.opsForList().rightPushAll(key, value);
            return true;
        } catch (Exception e) {
            log.error(EXCEPTION, e);
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒)
     * @return
     */
    public boolean lSet(String key, List<Object> value, long time) {
        try {
            redisTemplate.opsForList().rightPushAll(key, value);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            log.error(EXCEPTION, e);
            return false;
        }
    }

    /**
     * 根据索引修改list中的某条数据
     *
     * @param key   键
     * @param index 索引
     * @param value 值
     * @return
     */
    public boolean lUpdateIndex(String key, long index, Object value) {
        try {
            redisTemplate.opsForList().set(key, index, value);
            return true;
        } catch (Exception e) {
            log.error(EXCEPTION, e);
            return false;
        }
    }

    /**
     * 移除N个值为value
     *
     * @param key   键
     * @param count 移除多少个
     * @param value 值
     * @return 移除的个数
     */
    public Long lRemove(String key, long count, Object value) {
        try {
            return redisTemplate.opsForList().remove(key, count, value);
        } catch (Exception e) {
            log.error(EXCEPTION, e);
            return 0L;
        }
    }

    // ========================= sorted set ===========================
    /**
     * zSet 插入值
     *
     * @param key
     * @param value
     * @param score
     * @return
     */
    public boolean zSSet(String key, Object value, double score){
        try {
            redisTemplate.opsForZSet().add(key, value, score);
            return true;
        }catch (Exception e){
            log.error(EXCEPTION, e);
            return false;
        }
    }


    /**
     * 获取zSet score从大到小的缓存的内容
     *
     * @param key   键
     * @param start 开始
     * @param end   结束  0 到 -1代表所有值
     * @return
     */
    public  <T> List<T> zSReverseRange(String key, long start, long end, Class<T> tClass) {
        try {
            Set<ZSetOperations.TypedTuple<Object>> set = redisTemplate.opsForZSet().reverseRangeWithScores(key, start, end);
            List<T> tList=new LinkedList<>();
            if(!CollectionUtils.isEmpty(set)){
                List<Object> array = set.stream().sorted((s1,s2)-> ObjectUtils.compare(s2.getScore(), s1.getScore())).map(ZSetOperations.TypedTuple::getValue).collect(Collectors.toList());
                Objects.requireNonNull(array).forEach(arrObject->{
                    try {
                        if(tClass == String.class){
                            tList.add((T) arrObject.toString());
                        }else {
                            T t = tClass.getDeclaredConstructor().newInstance();
                            Field[] fields = tClass.getDeclaredFields();
                            JSONObject jsonObject = (JSONObject) arrObject;
                            for (Field field : fields) {
                                field.setAccessible(true);
                                field.set(t, jsonObject.get(field.getName()));
                            }
                            tList.add(t);
                        }
                    } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                        log.info("类转换异常！");
                        e.printStackTrace();
                    }
                });
            }

            return tList;
        } catch (Exception e) {
            log.error(EXCEPTION, e);
            return Collections.emptyList();
        }
    }

    /**
     * 生成key
     * <p>因不同项目或不同环境可能共用一台redis服务器，因此根据规则：项目名-环境-键名 来生成key值</p>
     * @param key
     * @return
     * @author
     */
    public String createRealKey(final String key){

        return new StringBuilder(projectName)
                .append("-")
                .append(env)
                .append("-")
                .append(key).toString();
    }
    /**
     * 创建key
     * eg:
     * env = "test"
     * module = "IOT-DEVICE";
     * deviceType = "air"
     * key = "nabkjsdaoisn"
     * separator = COLON
     * <p>
     * resultKey = test:executor1:auth:CWA1234
     *
     * @param module       模块
     * @param deviceType    设备类型
     * @param key           deviceId
     * @param separator         分隔符
     * @return resultKey
     */
    public String createDeviceRealKey(String module, String deviceType, String key, Separator separator) {
       // log.info("创建设备的redis的key:{}",env + separator.getSymbol() + module + separator.getSymbol() + deviceType + separator.getSymbol() + key);
        return projectName+separator.getSymbol()+env + separator.getSymbol() + module + separator.getSymbol() + deviceType + separator.getSymbol() + key;
    }
    //办公室初始化列表
    public String createOfficeRealKey(String module, String office, String key, Separator separator) {
        if(StringUtils.isEmpty(key)){
            return projectName+separator.getSymbol()+env + separator.getSymbol() + module + separator.getSymbol()+ office;
        }
        return projectName+separator.getSymbol()+env + separator.getSymbol() + module + separator.getSymbol() + key  + separator.getSymbol() + office;
    }

    /**
     * 创建key
     * eg:
     * env = "test"
     * module = "IOT-DEVICE";
     * key = ""
     * separator = COLON
     * <p>
     * resultKey = test:executor1:auth:CWA1234
     *
     * @param module       模块
     * @param separator         分隔符
     * @return resultKey
     */
    public String createDeviceTypeListKey(String module, Separator separator) {
        //log.info("创建设备的redis的key:{}",projectName+separator.getSymbol()+env + separator.getSymbol() + module );
        return projectName+separator.getSymbol()+env + separator.getSymbol() + module;
    }

    /**
     * 创建key
     * eg:
     * env = "test"
     * module = "IOT-DEVICE";
     * key = ""
     * separator = COLON
     * <p>
     * resultKey = test:executor1:auth:CWA1234
     *
     * @param module       模块
     * @param deviceId     设备id
     * @param separator         分隔符
     * @return resultKey
     */
    public String createDeviceIdKey(String module,String deviceId, Separator separator){
//        log.info("创建设备的redis的key：{}",projectName+separator.getSymbol()+env + separator.getSymbol() + module + deviceId);
        return projectName+separator.getSymbol()+env + separator.getSymbol() + module +separator.getSymbol() + deviceId;
    }

    /**
     * 创建key
     * eg:
     * env = "test"
     * module = "IOT-DEVICE"
     * deviceType="ari"
     * key = "CWA1234"
     * separator = COLON
     * <p>
     * resultKey = test:executor1:auth:CWA1234
     *
     * @param module       模块
     * @param deviceType         设备类型
     * @param separator         分隔符
     * @return resultKey
     */
    public String createDeviceIdListKey(String module, String deviceType, Separator separator) {
       // log.info("创建设备的redis的key:{}",env + separator.getSymbol() + module + separator.getSymbol() + deviceType );
        return projectName+separator.getSymbol()+env + separator.getSymbol() + module + separator.getSymbol() + deviceType;
    }

    /**
     * 创建集控码key
     *
     * @param module
     * @param key
     * @param separator
     * @return
     */
    public String createCentralizedControlKey(String module, String key, Separator separator) {
       // log.info("创建设备的redis的key:{}",env + separator.getSymbol() + module + separator.getSymbol() + deviceType );
        return projectName+separator.getSymbol()+env + separator.getSymbol() + module + separator.getSymbol() + key;
    }

    /**
     * 创建集控码设备列表key
     *
     * @param module
     * @param deviceType
     * @param key
     * @param separator
     * @return
     */
    public String createCentralizedCodeDeviceListKey(String module, String deviceType, String key, Separator separator){
        return projectName+separator.getSymbol()+env + separator.getSymbol() + module + separator.getSymbol() + deviceType + separator.getSymbol() + key;
    }

    public enum Separator {
        COLON(":"),
        DASH("-");

        String symbol;

        Separator(String s) {
            this.symbol = s;
        }

        public String getSymbol() {
            return symbol;
        }

    }
    public String createLockKey( String prefix,String key) {
        return projectName+Separator.COLON.getSymbol() +env + Separator.COLON.getSymbol() +  "Lock" +Separator.COLON.getSymbol()+prefix+ Separator.COLON.getSymbol()+ key;
    }

    public String createSpaceAdminRealKey(String module, String key, Separator separator) {
        // log.info("创建设备的redis的key:{}",env + separator.getSymbol() + module + separator.getSymbol() + deviceType + separator.getSymbol() + key);
        return projectName+separator.getSymbol()+env + separator.getSymbol() + module +   separator.getSymbol() + key;
    }

    public String createValidUidKey(String module, String key, Separator separator) {
        // log.info("创建设备的redis的key:{}",env + separator.getSymbol() + module + separator.getSymbol() + deviceType + separator.getSymbol() + key);
        return projectName+separator.getSymbol()+env + separator.getSymbol() + module +   separator.getSymbol() + key;
    }

    public String createRoomKey(String module,String key, Separator separator){
        return projectName+separator.getSymbol()+env + separator.getSymbol() + module +separator.getSymbol() + key;
    }

    public String createRoomUsedTimeKey(String module,String key, Separator separator){
        return projectName+separator.getSymbol()+env + separator.getSymbol() + module +separator.getSymbol() + key;
    }

     /**
     * 创建用户绑定门禁点时间计划key
     *
     * @param module
     * @param spotId
     * @param userHrId
     * @param separator
     * @return
     */
    public String createAccessControlTimePlanKey(String module, String spotId, String userHrId, Separator separator){
        return projectName+separator.getSymbol()+env + separator.getSymbol() + module + separator.getSymbol() + spotId + separator.getSymbol() + userHrId;
    }

    /**
     * 分布式锁缓存放入并设置时间
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒) time要大于0 如果time小于等于0 将设置默认时间
     * @return true成功 false 失败
     */
    public boolean setNx(String key, Object value, long time) {
        try {
            if (time > 0) {
                redisTemplate.opsForValue().setIfAbsent(key, value, time, TimeUnit.SECONDS);
            } else {
                redisTemplate.opsForValue().setIfAbsent(key, value, 30L, TimeUnit.SECONDS);
            }
            return true;
        } catch (Exception e) {
            log.error(EXCEPTION, e);
            return false;
        }
    }

    /**
     * 模糊查询符合要求的key
     *
     * @param pattern 正则
     * @return
     */
    public Set<String> keys(String pattern){
        return redisTemplate.keys(pattern);
    }
}
