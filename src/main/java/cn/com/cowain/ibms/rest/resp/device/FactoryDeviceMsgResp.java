package cn.com.cowain.ibms.rest.resp.device;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

/**
 * @Author tql
 * @Description 楼宇总览 出参类
 * @Date 21-11-4 下午3:13
 * @Version 1.0
 */
@Data
@Builder
@ApiModel("工厂设备信息")
public class FactoryDeviceMsgResp {

    @ApiModelProperty(value = "区域名",  example = "昆山工厂", position = 1)
    private String name;

    @ApiModelProperty(value = "数值",  example = "123456", position = 2)
    private Integer value;

    @ApiModelProperty(value = "类型",  example = "123456", position = 3)
    private String type;
}
