package cn.com.cowain.ibms.service.bean.amap.enumeration;

/**
 * 坐标系
 *
 * @author Yang.Lee
 * @date 2021/4/19 19:34
 */
public enum CoordinateSystem {

    /**
     * GPS
     **/
    GPS,

    /**
     * 图吧
     **/
    MAPBAR,

    /**
     * 百度
     **/
    BAIDU
}
