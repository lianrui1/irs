package cn.com.cowain.ibms.feign.bean.iotc.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 应用信息
 *
 * @author Yang.Lee
 * @date 2021/10/20 9:43
 */
@Data
@ApiModel("IOTC 应用详情")
public class ApplicationResp implements Serializable {

    @ApiModelProperty(value = "id", position = 1)
    private String id;

    @ApiModelProperty(value = "应用描述", position = 2)
    private String describe;

    @ApiModelProperty(value = "应用名称", position = 3)
    private String name;

    @ApiModelProperty(value = "是否删除", position = 4)
    private String idDel;
}
