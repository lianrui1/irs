package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.DeviceLinkRelation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface DeviceLinkRelationDao extends
        JpaRepository<DeviceLinkRelation, String>, JpaSpecificationExecutor<DeviceLinkRelation> {

    /**
     * 根据联动id查找
     *
     * @param deviceLinkId
     * @return
     */
    List<DeviceLinkRelation> findByDeviceLinkId(String deviceLinkId);

    /**
     * 查询iotDeviceId = {@code iotDeviceId}的DeviceLinkRelation列表
     *
     * @param iotDeviceId
     * @return
     */
    List<DeviceLinkRelation> findAllByIotDeviceId(String iotDeviceId);
}
