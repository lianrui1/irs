package cn.com.cowain.ibms.service.disthinguish;

import cn.com.cowain.ibms.entity.distinguish.AccessRecord;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.distinguish.AccessRecordPageReq;
import cn.com.cowain.ibms.rest.resp.EquipmentDataResp;
import cn.com.cowain.ibms.rest.resp.SearchStaffDataResp;
import cn.com.cowain.ibms.rest.resp.distinguish.AccessRecordPageResp;
import cn.com.cowain.ibms.rest.resp.distinguish.WorkTimeCheckResp;
import com.alibaba.fastjson.JSONObject;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 通行记录Service
 *
 * @author Yang.Lee
 * @date 2021/3/15 12:56
 */
public interface AccessRecordHqService {

    /**
     * 查询通行记录分页数据
     *
     * @param req 查询参数
     * @return 分页数据
     * @author Yang.Lee
     * @date 2021/3/15 12:57
     **/
    PageBean<AccessRecordPageResp> getPage(AccessRecordPageReq req);

    /**
     * 对象转换
     *
     * @param recordList 待转对象
     * @return 目标对象
     * @author Yang.Lee
     * @date 2021/3/15 13:12
     **/
    List<AccessRecordPageResp> convertList(List<AccessRecord> recordList);

    /**
     * 对象转换
     *
     * @param record 待转对象
     * @return 目标对象
     * @author Yang.Lee
     * @date 2021/3/15 13:11
     **/
    AccessRecordPageResp convert(AccessRecord record);

    /**
     * 保存通行记录
     **/
    void save(JSONObject accessRecordJson);

    /**
     * 获取考勤数据
     *
     * @param startTime 查询开始时间
     * @param endTime   查询结束时间
     * @return 考勤列表
     */
    List<WorkTimeCheckResp> getWorkTimeCheckList(LocalDateTime startTime, LocalDateTime endTime);

    /**
     * 获取总部考勤数据
     *
     * @param startTime 查询开始时间
     * @param endTime   查询结束时间
     * @return 考勤列表
     */
    List<SearchStaffDataResp> getHqWorkTimeCheckList(LocalDate startTime, LocalDate endTime);

    /**
     * 获取设备列表
     *
     * @return
     */
    List<EquipmentDataResp> getHqEquipmentList();
}
