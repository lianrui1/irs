package cn.com.cowain.ibms.service.iot;

import cn.com.cowain.ibms.entity.iot.AccessControlSpotKSHTMemberGroup;

import java.util.List;

/**
 * 门禁点Service
 *
 * @author Yang.Lee
 * @date 2021/11/19 13:59
 */
public interface AccessControlSpotUserService {

    /**
     * @param hrId
     * @return
     * @description 根据工号查询人员所在旷视鸿图人员组
     * @author tql
     * @date 22-5-20
     */
    List<AccessControlSpotKSHTMemberGroup> findKsHtUserGroupByHrId(String hrId);
}
