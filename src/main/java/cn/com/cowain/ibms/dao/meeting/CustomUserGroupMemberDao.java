package cn.com.cowain.ibms.dao.meeting;

import cn.com.cowain.ibms.entity.meeting.CustomUserGroup;
import cn.com.cowain.ibms.entity.meeting.CustomUserGroupMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomUserGroupMemberDao extends JpaRepository<CustomUserGroupMember, String> {

    // 跟据用户组获取用户组成员
    List<CustomUserGroupMember> findByCustomUserGroup(CustomUserGroup userGroup);

    // 根据工号获取其所在成员组
    List<CustomUserGroupMember> findByMemberEmpNo(String empNo);
}
