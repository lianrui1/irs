package cn.com.cowain.ibms.dao.distinguish;

import cn.com.cowain.ibms.entity.distinguish.AccessRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * 通行记录Dao
 *
 * @author Yang.Lee
 * @date 2021/3/15 13:13
 */
public interface AccessRecordDao extends JpaRepository<AccessRecord, String>, JpaSpecificationExecutor<AccessRecord> {
}
