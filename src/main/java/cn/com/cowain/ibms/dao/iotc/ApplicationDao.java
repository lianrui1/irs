package cn.com.cowain.ibms.dao.iotc;

import cn.com.cowain.ibms.entity.iotc.Application;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

/**
 * @author Yang.Lee
 * @date 2021/10/20 13:59
 */
public interface ApplicationDao extends JpaRepository<Application, String>, JpaSpecificationExecutor<Application> {

    /**
     * 根据名称查询
     *
     * @param name 名称
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/10/20 17:38
     **/
    Optional<Application> findByName(String name);
}
