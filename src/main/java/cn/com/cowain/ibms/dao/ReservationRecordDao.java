package cn.com.cowain.ibms.dao;

import cn.com.cowain.ibms.bo.SimpleReservationRecord;
import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.entity.Room;
import cn.com.cowain.ibms.enumeration.ReservationRecordStatus;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

/**
 * ReservationRecord Dao
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/4 18:40
 */
@Repository
public interface ReservationRecordDao extends JpaRepository<ReservationRecord, String>, JpaSpecificationExecutor<ReservationRecord> {

    /**
     * 根据 room_id 精确查找
     *
     * @param id
     * @return
     */
    Set<ReservationRecord> findByRoomId(String id);

    /**
     * 根据日期 room_id 精确查找
     *
     * @param date
     * @param id
     * @return
     */
    Set<ReservationRecord> findByDateGreaterThanEqualAndRoomId(LocalDate date, String id);

    /**
     * 根据日期 和 room_id 精确查找自己发起的会议, 按 from 正序排序
     *
     * @param date   日期
     * @param roomId Room ID
     * @return
     */
    List<ReservationRecord> findByDateAndRoomIdAndInitiatorOrderByFromAsc(LocalDate date, String roomId, String sysId);

    /**
     * 根据日期 和 room_id 精确查找自己发起的会议, 按 from 正序排序
     *
     * @param date   日期
     * @param roomId Room ID
     * @param empNo  员工工号
     * @return
     */
    List<ReservationRecord> findByDateAndRoomIdAndInitiatorEmpNoOrderByFromAsc(LocalDate date, String roomId, String empNo);

    /**
     * 根据时间间隔 查找 会议记录
     *
     * @param timeStart 开始时间
     * @param timeEnd   结束时间
     * @return Set<ReservationRecord>
     */
    Set<ReservationRecord> findByFromBetween(LocalDateTime timeStart, LocalDateTime timeEnd);

    /**
     * 根据开始时间查找会议记录
     *
     * @param timeFrom
     * @return
     */
    Set<ReservationRecord> findByFromAndStatus(LocalDateTime timeFrom, ReservationRecordStatus aDefault);

    /**
     * 根据发起人进行查询 (分页)
     *
     * @param initiator 发起人 sysId
     * @param pageable  分页参数
     * @return
     */
    List<ReservationRecord> findByInitiator(String initiator, Pageable pageable);

    /**
     * 根据指定日期查询范围内未结束的会议数据
     *
     * @param date
     * @param timeStart
     * @param timeEnd
     * @return
     * @author Yang.Lee
     */
    List<ReservationRecord> findByDateAndToBetweenOrderByToAsc(LocalDate date, LocalDateTime timeStart, LocalDateTime timeEnd);

    /**
     * 根据日期 和 room_id 精确查找, 按 from 正序排序
     *
     * @param date   日期
     * @param roomId Room ID
     * @return
     */
    List<ReservationRecord> findByDateAndRoomIdOrderByFromAsc(LocalDate date, String roomId);


    List<ReservationRecord> findByDateAndRoomIdOrderByFromAscToAsc(LocalDate date, String roomId);

    List<ReservationRecord> findByDateBetweenAndRoomIdOrderByFromAscToAsc(LocalDate dateStart, LocalDate dateEnd,String roomId);

    /**
     * 根据会议室及会议时间查询
     *
     * @param roomId    会议室
     * @param timeStart 会议开始时间
     * @param timeEn    会议结束时间
     * @return 会议列表
     * @author Yang.Lee
     * @date 2022/4/21 14:33
     **/
    List<ReservationRecord> findByRoomIdAndFromBetweenOrderByFromAsc(String roomId, LocalDateTime timeStart, LocalDateTime timeEn);

    /**
     * 获取会议发起人工号
     *
     * @return
     */
    @Query("SELECT rr.initiatorEmpNo FROM ReservationRecord AS rr GROUP BY rr.initiatorEmpNo")
    Set<String> findGroupByInitiatorEmpNo();

    /**
     * 根据员工工号获取会议记录
     *
     * @return
     */
    List<ReservationRecord> findByInitiatorEmpNo(String empNo);

    /*
     * 根据员工工号和会议室获取会议记录  已取消的会议不计算
     *
     */
    List<ReservationRecord> findByInitiatorEmpNoAndStatusNotAndRoom(String empNo, ReservationRecordStatus cancel, Room room);

    /**
     * 根据结束时间查找会议记录
     *
     * @return
     */
    Set<ReservationRecord> findByToAndStatus(LocalDateTime ldt, ReservationRecordStatus aDefault);

    // 根据会议室获取当前正在进行中的会议
    List<ReservationRecord> findByToGreaterThanEqualAndFromLessThanEqualAndRoomIdAndStatusOrderByFromAsc(LocalDateTime now, LocalDateTime now1, String roomId, ReservationRecordStatus aDefault);

    /**
     * 根据开始时间和状态查询
     *
     * @param now     开始时间
     * @param roomId  会议室
     * @param aDefaul 状态
     * @return
     * @author Yang.Lee
     * @date 2022/1/5 17:58
     **/
    List<ReservationRecord> findByFromGreaterThanAndRoomIdAndStatusNotOrderByFromAsc(LocalDateTime now, String roomId, ReservationRecordStatus aDefaul);

    /**
     * 批量查询
     * @param date
     * @param roomIds
     * @return
     */
    @Query(
            value = "select new cn.com.cowain.ibms.bo.SimpleReservationRecord(r.id, r.from, r.to, r.date, r.topic, r.initiatorEmpNo, r.status, r.room.id) from ReservationRecord r where r.date = :date and r.room.id in (:roomIds) and r.isDelete = 0 order by r.from asc"
    )
    List<SimpleReservationRecord> findByDateAndRoomIdIdsOrderByFromAsc(@Param("date") LocalDate date, @Param("roomIds") List<String> roomIds);
}
