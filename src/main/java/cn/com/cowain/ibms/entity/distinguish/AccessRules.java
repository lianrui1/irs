package cn.com.cowain.ibms.entity.distinguish;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.space.Project;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/6 10:39
 * 通行权限
 */
@Data
@Entity
@Table(name = "ibms_access_rules")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_access_rules", comment = "通行权限表")
@Where(clause = "is_del=0")
public class AccessRules extends BaseEntity {

    /**
     * 设备来源
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "iot_device_source", columnDefinition = "varchar(50) COMMENT '设备来源'")
    private DeviceSource deviceSource;

    /**
     * 所属项目
     */
    @ManyToOne
    @JoinColumn(name = "project_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK'")
    private Project project;

    /**
     * 所属空间
     */
    @ManyToOne
    @JoinColumn(name = "space_id", columnDefinition = "varchar(36) COMMENT 'FK'")
    private Space space;

    @ManyToOne
    @JoinColumn(name = "device_cloud_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 云端设备ID'")
    private DeviceCloud deviceCloud;

    @ManyToOne
    @JoinColumn(name = "time_plan_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 时间计划ID'")
    private TimePlan timePlan;
}
