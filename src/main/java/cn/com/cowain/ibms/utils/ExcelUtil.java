package cn.com.cowain.ibms.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * excel操作工具类
 *
 * @author Yang.Lee
 * @date 2021/7/13 11:05
 */
@Slf4j
public class ExcelUtil {


    /**
     * 读取excel文件，读取成功后关闭输入流
     *
     * @param input 数据流
     * @return excel工作簿对象
     * @author Yang.Lee
     * @date 2021/7/13 11:09
     **/
    public Workbook readFile(InputStream input) {
        try {
            return WorkbookFactory.create(input);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            return null;
        } finally {
            try {
                input.close();
            } catch (IOException e) {
                log.error("关闭input失败！！！");
                log.error(e.getMessage(), e);
            }

        }
    }

    /**
     * 获取工作簿下的所有sheet
     *
     * @param workbook 工作簿
     * @return sheet列表
     * @author Yang.Lee
     * @date 2021/7/13 11:31
     **/
    public List<Sheet> readWorkbook(Workbook workbook) {

        List<Sheet> sheetList = new ArrayList<>();

        int sheetCount = workbook.getActiveSheetIndex();
        for (int i = 0; i < sheetCount; i++) {
            sheetList.add(workbook.getSheetAt(i));
        }

        return sheetList;
    }

    public void setCellStyle(Cell cell){

    }
}
