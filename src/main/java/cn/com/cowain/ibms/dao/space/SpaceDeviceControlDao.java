package cn.com.cowain.ibms.dao.space;

import cn.com.cowain.ibms.entity.space.SpaceDeviceControl;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

import java.util.Optional;

@Repository
public interface SpaceDeviceControlDao extends JpaRepository<SpaceDeviceControl, String>, JpaSpecificationExecutor<SpaceDeviceControl> {


    // 获取多设备下人员
    List<SpaceDeviceControl> findByUserEmpNoAndDeviceIdIn(String userEmpNo, List<String> deviceIds);

    // 获取设备下人员
    List<SpaceDeviceControl> findByDeviceId(String deviceId);
    // 根据工号和设备id获取信息
    Optional<SpaceDeviceControl> findByUserEmpNoAndDeviceId(String it, String deviceId);

    /**
     * 简化查询
     * @param empNo
     * @param deviceIds
     * @return
     */
    @Query(
            value = "select device.id from SpaceDeviceControl where userEmpNo = :empNo and device.id in (:deviceIds) and isDelete = 0"
    )
    List<String> findDeviceIdByUserEmpNoAndDeviceIds(@Param("empNo")String empNo, @Param("deviceIds")List<String> deviceIds);



    // 根据空间ID获取拥有权限人员
    List<SpaceDeviceControl> findByDeviceSpaceIdOrderByCreatedTimeDesc(String id);

    // 根据空间ID和工号获取人员权限
    List<SpaceDeviceControl> findByDeviceSpaceIdAndUserEmpNo(String id, String it);

    List<SpaceDeviceControl> findByDeviceSpaceIdOrderByCreatedTime(String id);

    // 根据人员工号获取设备控制权信息
    List<SpaceDeviceControl> findByUserEmpNo(String hrId);

    List<SpaceDeviceControl> findByDeviceSpaceIdIn(Collection<String> ids);
}
