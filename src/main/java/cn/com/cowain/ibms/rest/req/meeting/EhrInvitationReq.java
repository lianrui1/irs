package cn.com.cowain.ibms.rest.req.meeting;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/5 10:08
 */
@Data
public class EhrInvitationReq {

    // 访问开始时间
    private String startTime;
    // 访问结束时间
    private String endTime;
    // 事由编码
    private String reasonCode;
    // 地址
    private String address;
    // 发送邀请方式 1:短信2:微信
    private String inviteMethod;
    // 备注
    private String remarks;
    // 是否vip '1'vip  '0'非vip
    private String isVip;
    // 地区
    private String companyId;

    //访客类型代码code
    private String roleCode;

    //会议室信息
    private String meeting;

}
