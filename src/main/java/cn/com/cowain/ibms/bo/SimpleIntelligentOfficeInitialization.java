package cn.com.cowain.ibms.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author wei.cheng
 * @date 2022/10/17 11:01
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class SimpleIntelligentOfficeInitialization implements Serializable {

    private String intelligentOffice;

    private String userHrId;

    private String userName;

    private String alisa;
}
