package cn.com.cowain.ibms.feign.iotc;

import cn.com.cowain.ibms.entity.space.IotProduct;
import cn.com.cowain.ibms.feign.bean.iotc.resp.ApplicationResp;
import cn.com.cowain.ibms.feign.bean.iotc.resp.DeviceResp;
import cn.com.cowain.ibms.feign.bean.iotc.resp.DeviceStatusChangeListResp;
import cn.com.cowain.ibms.feign.iotc.bean.DeviceExecuteCommandReq;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.iot.GroupBindingReq;
import cn.com.cowain.ibms.rest.resp.TokenResp;
import cn.com.cowain.sfp.comm.JsonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

/**
 * @author Yang.Lee
 * @date 2021/10/20 9:32
 */
@Service
@RequestMapping({"/ms-iotCenter/api/v1"})
@FeignClient(value = "ms-iotCenter-backend", url = "${ms-iotc.url}")
public interface IOTCApi {

    /**
     * 获取应用列表
     *
     * @return 获取结果
     * @author Yang.Lee
     * @date 2021/10/20 9:48
     **/
    @GetMapping("/ApplicationManage/application/page")
    JsonResult<PageBean<ApplicationResp>> getApplicationList(@RequestParam int page, @RequestParam int size);

    /**
     * 获取产品列表
     *
     * @return 获取结果
     * @author Yang.Lee
     * @date 2021/10/25 11:19
     **/
    @GetMapping("/productManage/page")
    JsonResult<PageBean<IotProduct>> getProductList(@RequestParam int page, @RequestParam int size);

    /**
     * 获取设备状态数据列表
     *
     * @param page         页长
     * @param size         页码
     * @param sn           sn
     * @param deviceStatus
     * @return 结果
     * @author Yang.Lee
     * @date 2021/10/26 15:51
     **/
    @GetMapping("/StatusManage/deviceStatus/page")
    JsonResult<PageBean<DeviceStatusChangeListResp>> getStatusList(@RequestParam(required = false) int page,
                                                                   @RequestParam(required = false) int size,
                                                                   @RequestParam(required = false) String sn,
                                                                   @RequestParam(required = false) String deviceStatus);

    /**
     * 获取设备列表
     *
     * @param page 页面
     * @param size 页长
     * @return 结果
     * @author Yang.Lee
     * @date 2021/10/27 16:02
     **/
    @GetMapping("/deviceManage/page")
    JsonResult<PageBean<DeviceResp>> getDeviceList(@RequestParam(required = false) int page,
                                                   @RequestParam(required = false) int size);

    /**
     * 　　* @title: 获取萤石的token
     * 　　* @description
     * 　　* @author jfsui
     * 　　* @date 2021/12/8 13:13
     */
    @GetMapping("/yingshi/token")
    JsonResult<TokenResp> getToken();


    /**
     * 　　* @title: iotc转发绑定，解绑，组与设备信息
     * 　　* @description 主要用在web端的功能绑定与解绑
     * 　　* @author jfsui
     * 　　* @date 2021/12/8 13:13
     */
    @PostMapping("/transmit/post/to/conn")
    JsonResult<Object> postToConn(@RequestBody GroupBindingReq req);


    /**
     * 设备执行指令
     *
     * @param req
     * @return
     * @author wei.cheng
     */
    @PostMapping("/command/execute")
    JsonResult<Object> deviceExecuteCommand(@RequestBody DeviceExecuteCommandReq req);

    /**
     * 查询人员在设备平台中是否存在
     *
     * @author: yanzy
     * @data: 2022/4/2 14:04:32
     */
    @GetMapping("/faceData/check/employee/info")
    JsonResult<Object> getUserFromPlatform(@RequestParam String jobNo);
}
