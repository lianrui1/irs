package cn.com.cowain.ibms.enumeration.task;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wei.cheng
 * @date 2022/03/10 16:20
 */
public enum TaskStatus {
    /**
     * 待办
     */
    PENDING(1),
    /**
     * 已办
     */
    DONE(2);

    private static final Map<Integer, TaskStatus> TASK_STATUS_MAP = new HashMap<>();

    static {
        for (TaskStatus status : TaskStatus.values()) {
            TASK_STATUS_MAP.put(status.value, status);
        }
    }

    private final Integer value;

    TaskStatus(Integer value) {
        this.value = value;
    }

    public static TaskStatus resolve(Integer value) {
        return TASK_STATUS_MAP.get(value);
    }

    public Integer getValue() {
        return this.value;
    }
}
