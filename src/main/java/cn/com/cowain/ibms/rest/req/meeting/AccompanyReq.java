package cn.com.cowain.ibms.rest.req.meeting;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/8 19:01
 */
@Data
public class AccompanyReq {

    @ApiModelProperty(value = "邀请函code", required = true, example = "", position = 1)
    private String code;

    @ApiModelProperty(value = "邀请人code", required = true, example = "", position = 2)
    private String recordCode;

    @ApiModelProperty(value = "随行人员信息", required = true, example = "", position = 2)
    private List<AccompanyInfo> accompanyInfoList;
}
