package cn.com.cowain.ibms.job;

import cn.com.cowain.ibms.dao.RoomDao;
import cn.com.cowain.ibms.entity.Room;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.ibms.utils.RedisUtil;
import cn.com.cowain.ibms.utils.ThreadPoolUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * @author wei.cheng
 * @date 2022/10/08 16:24
 */
@Slf4j
@Component
public class RoomJob {
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private RoomDao roomDao;

    /**
     * 同步会议室数据到缓存，每天直行一次
     */
    @Scheduled(cron = "0 0 1 * * ?")
    @PostConstruct
    public void initRoom() {
        ThreadPoolUtils.EXECUTOR_SERVICE_REDIS.submit(() -> {
            List<Room> roomList = roomDao.findAll();
            roomList.forEach(room -> {
                String key = redisUtil.createRoomKey(IConst.ROOM_MODULE, room.getId(), RedisUtil.Separator.COLON);
                redisUtil.set(key, room, RedisUtil.TWO_DAY_SECOND);
            });
        });
    }
}
