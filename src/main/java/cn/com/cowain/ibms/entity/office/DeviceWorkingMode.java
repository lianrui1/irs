package cn.com.cowain.ibms.entity.office;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.enumeration.iot.UsableStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author Yang.Lee
 * @date 2021/8/27 16:09
 */
@Data
@Entity
@Table(name = "iot_devcie_working_mode")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_devcie_working_mode", comment = "设备工作模式表")
public class DeviceWorkingMode extends BaseEntity {

    @Column(name = "name", nullable = false, columnDefinition = "varchar(50) COMMENT '工作模式名称'")
    private String name;

    @Deprecated(since = "智能办公室1.2", forRemoval = true)
    @Column(name = "is_default", nullable = false, columnDefinition = "tinyint(1) default 0 COMMENT '是否是默认模式'")
    private int isDefault;

    @Column(name = "img", nullable = false, columnDefinition = "varchar(100) COMMENT '工作模式图标'")
    private String img;

    @Column(name = "purpose", nullable = false, columnDefinition = "varchar(50) COMMENT '工作模式用途'")
    private String purpose;

    @ManyToOne
    @JoinColumn(name = "space_id", nullable = false, columnDefinition = "varchar(36) COMMENT '绑定空间'")
    @Deprecated(since = "智能办公室1.2", forRemoval = true)
    private Space space;

    @ManyToOne
    @JoinColumn(name = "office_id", nullable = false, columnDefinition = "varchar(36) COMMENT '智能办公室'")
    private IntelligentOffice intelligentOffice;

    @Column(name = "usable_status", nullable = false, columnDefinition = "varchar(50) COMMENT '可用状态'")
    @Enumerated(EnumType.STRING)
    private UsableStatus usableStatus;

    /**
     * 控制时间
     */
    // @Deprecated(since="智能办公室1.2", forRemoval = true)
    @Column(name = "control_time", columnDefinition = "datetime COMMENT '控制时间'")
    private LocalDateTime controlTime;

    @Column(name = "user_hr_id", nullable = false, columnDefinition = "varchar(20) COMMENT '用户工号'")
    private String userHrId;

    @ManyToOne
    @JoinColumn(name = "default_mode_id", columnDefinition = "varchar(36) COMMENT '默认模式ID'")
    private DeviceWorkingDefaultMode defaultMode;

    @Column(name = "is_showroom_centralize", nullable = false, columnDefinition = "tinyint(1) default 0 COMMENT '是否是总控，0不是总控，1是总控'")
    private int showroomCentralize;
}
