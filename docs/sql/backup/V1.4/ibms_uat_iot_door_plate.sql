-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 10.0.1.232    Database: ibms_uat
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `iot_door_plate`
--

DROP TABLE IF EXISTS `iot_door_plate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `iot_door_plate` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `created_by` varchar(36) DEFAULT NULL COMMENT '创建人',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(36) DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int(11) DEFAULT NULL COMMENT '版本号',
  `magnetic_num` varchar(50) DEFAULT NULL COMMENT '门磁设备编号',
  `name` varchar(20) NOT NULL COMMENT '门牌名称',
  `seq` int(11) DEFAULT NULL COMMENT '排序',
  `device_id` varchar(36) NOT NULL COMMENT 'FK',
  `password` varchar(20) DEFAULT NULL,
  `open_password` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FKlfhe6ixbhlh6mcoyfe106pk9b` (`device_id`) USING BTREE,
  CONSTRAINT `FKlfhe6ixbhlh6mcoyfe106pk9b` FOREIGN KEY (`device_id`) REFERENCES `iot_device` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='门牌';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `iot_door_plate`
--

LOCK TABLES `iot_door_plate` WRITE;
/*!40000 ALTER TABLE `iot_door_plate` DISABLE KEYS */;
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('06350354-155f-45a6-97ea-328e70e3a505',NULL,'2021-02-18 15:49:48',0,NULL,'2021-02-18 16:34:15',1,'010001','天璇会议室',16,'ca4cb03e-4bc8-4acd-b62c-42ae062de1c7','123456',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('0dae1019-86a3-4bb7-891c-1b614f191c1c',NULL,'2021-02-24 13:49:21',0,NULL,'2021-02-24 13:49:21',0,'010009','开阳会议室',20,'58010f49-976b-4e4c-a7a0-3865c218a40b','',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('127c413c-4fe7-4fd8-8432-5235646d2a16',NULL,'2021-02-09 10:26:51',0,NULL,'2021-02-18 16:34:15',1,'010004','天璇会议室',9,'359720ce-a731-4850-b4d1-741509029354','123456',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('17568421-0480-4d2d-94cf-5cd3ce94e69b',NULL,'2021-02-09 10:21:19',0,NULL,'2021-02-18 16:34:15',1,'010001','天璇会议室',8,'41d5ad2e-f3ad-4e5a-8a8d-0b9d0d20591d','123456',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('1d9d0107-6821-474b-a925-a94e634aaacf',NULL,'2021-02-09 10:45:08',0,NULL,'2021-02-18 16:34:15',1,'010008','玉衡会议室',13,'5d9e7eca-b8d4-41b0-b768-c41f9b59eb92','123456',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('1e753ed7-d6c2-4dae-b33d-151db09a8535',NULL,'2020-12-31 11:22:46',1,NULL,'2021-01-28 08:30:38',15,'123','花桥会议室JJC',0,'5646bc13-31db-4f7a-85bb-f46dd148873a','111111',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('26deacd6-55b4-40e4-9dbc-6af678500f67',NULL,'2021-01-28 14:00:07',0,NULL,'2021-02-18 16:34:15',8,'123','天天向上',4,'1bd562c3-809e-45a6-a9fd-600f7bfaf6fa','123456',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('2c31623a-f27f-4190-a7be-159a39fac9a8',NULL,'2021-02-19 09:18:44',0,NULL,'2021-02-19 09:18:44',0,'010005','天玑会议室',19,'15bb2a6e-036b-4211-b387-6d0d04595ae6','',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('3b9e1a65-59c0-446c-a4e3-bfe6eb07f3ad',NULL,'2021-01-21 16:23:18',1,NULL,'2021-01-28 08:30:33',6,'fm21zu8v','Room',3,'e28739ed-358c-4770-aadd-13095490e5eb','111111',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('467535b5-8940-4477-bdd8-352cf0dc4847',NULL,'2021-01-26 13:53:23',1,NULL,'2021-01-28 08:31:21',2,'123','333333',6,'2e2a2508-b149-4450-8895-c8d6d2d30658','111111',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('4d64abd3-079d-4803-a3c0-8e244315cafa',NULL,'2021-01-06 18:41:36',0,NULL,'2021-02-18 16:34:15',7,'fm21zu8v','产品研发中心',2,'2f646c72-111f-4610-9b32-61fe8182de3b','123456',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('594f1070-a46d-4248-bf30-ec4d87427e16',NULL,'2021-02-18 12:23:56',0,NULL,'2021-02-18 16:34:15',1,'我让我朋友','小肉手',14,'53cf1c0f-8234-498a-97c9-c7cd107ba63b','123456',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('5c169bf6-0624-451b-93be-ecdd447f376c',NULL,'2021-01-14 15:59:13',1,NULL,'2021-01-28 08:30:59',5,NULL,'天玑会议室',11,'3047883a-43de-4364-880b-320e8f5d50b9','111111',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('63a64535-61f2-409b-94ea-2cb1c20e4df9',NULL,'2021-02-09 10:39:26',0,NULL,'2021-02-18 16:34:15',1,'010009','开阳会议室',12,'5b21adec-46ea-4af6-a009-5c9d8d86fedb','123456',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('76431e31-ca5e-4f04-8aef-62eea872cdda',NULL,'2021-02-18 16:53:10',0,NULL,'2021-02-18 17:50:37',1,'010004','天璇会议室',17,'f80696f3-7b7c-40b3-b7f6-33e34427a9a8','123456',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('80a7788b-4cee-42d7-9e90-87d069a870ca',NULL,'2021-02-18 14:00:09',0,NULL,'2021-02-18 16:34:15',1,'是是是','三生三世',15,'76072557-8675-4156-9095-e862c431a471','123456',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('8613aaf9-6fc5-44dd-a5cb-819e7ed01e28',NULL,'2021-02-09 10:34:16',0,NULL,'2021-02-18 16:34:15',3,'010006','天权会议室',11,'16e24d3c-d277-49e8-bb80-beadb2ffd043','123456',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('894988ed-d486-4922-8c6c-223c0851b375',NULL,'2021-01-12 15:42:51',1,NULL,'2021-01-28 08:30:10',5,NULL,'发发发',6,'6288bd48-cd0f-4da0-9a96-b53527289363','111111',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('89ec9521-4652-49fc-b9f5-f85ff49c0ee8',NULL,'2020-12-30 20:16:22',1,NULL,'2020-12-30 20:16:22',1,'55667777','test',0,'e1068166-8389-4d87-b67b-4fde4f4f467a','111111',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('9303e225-6977-4610-8786-393f6af68425',NULL,'2021-02-18 17:19:15',0,NULL,'2021-02-18 17:50:37',1,'010006','天权会议室',18,'1f8d9030-7664-44bf-840a-46f7d3574c5b','123456',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('97ae1fb8-e75a-46eb-a9c8-1944c49977eb',NULL,'2021-01-12 15:45:37',1,NULL,'2021-01-28 08:30:18',5,NULL,'华侨',8,'1fc7e9ac-611f-493d-946d-a7fbbc636266','111111',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('a0b193a9-14ee-4394-8226-51880afb4999',NULL,'2020-12-30 20:29:00',0,NULL,'2021-02-18 16:34:15',7,'123','启明',0,'08ce2a99-c807-4ada-8976-9dec5ace9e88','123456',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('a31c05a4-4fcb-494b-a75c-c121d7a83d7c',NULL,'2021-01-06 17:45:31',0,NULL,'2021-02-18 16:34:15',6,'fm21zu8v','产品研发中心',1,'8ac1b16d-c38e-4317-8780-702234f8c0d6','123456',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('abf091ed-3c6d-49ed-a0b1-1cb4672329a2',NULL,'2021-01-21 16:23:16',1,NULL,'2021-01-28 08:30:31',6,'fm21zu8v','Room',2,'e36cbbbe-665b-4616-a221-8b94a41a855a','111111',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('afadc89f-aacb-4d60-b0cb-b5f4e3e048f9',NULL,'2021-02-09 10:30:19',0,NULL,'2021-02-18 16:34:15',1,'010005','天玑会议室',10,'7375cb1b-08ad-40f5-833a-7fd76173830d','123456',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('b061218e-b995-45b2-8f7f-1950e66c9ed8',NULL,'2021-01-05 15:23:51',1,NULL,'2021-01-05 15:23:51',2,'fm21zu8v','产品研发中心',3,'511a8015-3993-4fa1-8151-f6543bb48d23','111111',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('b43bb4e2-9cd8-4a53-8c8e-6195cbc915ff',NULL,'2021-01-12 15:45:05',1,NULL,'2021-01-28 08:30:14',6,NULL,'fff',7,'6a1cd0d1-55f9-4986-92e8-d7458d9847da','111111',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('b4897769-aec2-4352-8fee-11fd58814638',NULL,'2021-01-12 15:45:47',1,NULL,'2021-01-28 08:30:26',5,NULL,'华侨',9,'72aa01a1-7b55-48df-9f7c-c7fc4ccfd3fe','111111',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('b8a52db3-b1fc-4500-88a7-32525eb0e113',NULL,'2021-01-08 09:23:18',1,NULL,'2021-01-28 08:30:51',5,'啦啦啦啦','恩久软件',5,'45741b54-c080-40b2-a804-13a6024a0aa2','111111',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('c9270edc-b13b-44b0-a957-5ddfd9fdb9e2',NULL,'2021-02-08 15:27:07',0,NULL,'2021-02-18 16:34:15',1,'010002','天枢会议室',6,'e88f1ad5-426f-4ffb-b92c-b0d30295bb8d','123456',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('dabc499f-f4c4-42d7-a727-de8e85ea1c8f',NULL,'2021-01-13 09:37:50',1,NULL,'2021-01-13 09:37:50',0,'55667777','test',10,'e7065df1-100a-4864-934b-b6e026d3c223','111111',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('e08ac359-f499-49b7-915a-e09ed8b03559',NULL,'2021-02-01 13:25:32',0,NULL,'2021-02-18 16:34:15',1,'123','就是不知道',5,'c628a454-c08e-4776-a52a-e582a294f5cb','123456',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('e0ed4869-5543-4d83-95c3-edf54f4201bb',NULL,'2021-01-25 19:31:01',1,NULL,'2021-01-28 08:30:48',4,'123','123213213',13,'31c91e14-b761-4a48-a260-dd4e89edcfc2','111111',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('e49789a6-f16f-4aab-90f7-50052bc3af91',NULL,'2021-01-06 18:08:46',0,NULL,'2021-02-18 16:34:15',5,'fm21zu8v','产品研发中心',3,'4150c508-fc0d-407e-8fe4-4a34a8947cd1','123456',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('e840c468-e16a-4583-a509-95242ad43010',NULL,'2021-01-13 15:14:10',1,NULL,'2021-01-28 10:50:30',9,'123','天玑会议室',4,'716c30e4-4d09-48ed-b986-c1a922a323e4','111111',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('e9f631e5-3093-4116-976a-58fba4f91353',NULL,'2021-02-09 10:14:43',0,NULL,'2021-02-18 16:34:15',2,'010003','天枢会议室',7,'fa109b78-026c-44ea-88a8-1ecf61ec022f','123456',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('f2998b1b-61c5-4632-b32f-0b63ea58c10f',NULL,'2021-01-25 20:40:35',1,NULL,'2021-01-28 08:30:43',7,'123','产品研发中心测试专用',14,'45b0e113-a7d0-443f-970a-96b7c7bb1bce','111111',NULL);
INSERT INTO `iot_door_plate` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `magnetic_num`, `name`, `seq`, `device_id`, `password`, `open_password`) VALUES ('f85afcf3-2cb9-4f9a-a591-075016ae3d23',NULL,'2021-01-25 19:28:18',1,NULL,'2021-01-28 08:31:25',6,'123','很high',5,'a552caa4-b4e0-4df1-9af9-53f08b113a56','111111',NULL);
/*!40000 ALTER TABLE `iot_door_plate` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-26  9:34:18
