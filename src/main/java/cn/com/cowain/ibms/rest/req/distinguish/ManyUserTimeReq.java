package cn.com.cowain.ibms.rest.req.distinguish;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/3 17:19
 */
@Data
@ApiModel("批量下发人员请求对象")
public class ManyUserTimeReq {

    @NotNull
    @ApiModelProperty(value = "人员ID", required = true)
    private List<String> ids;

    @ApiModelProperty(value = "设备ID", required = true)
    private List<String> deviceIds;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "通行生效时间")
    private LocalDateTime startTime;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "通行结束时间")
    private LocalDateTime endTime;

    @ApiModelProperty(value = "是否全部 0 是")
    private Integer all;

}
