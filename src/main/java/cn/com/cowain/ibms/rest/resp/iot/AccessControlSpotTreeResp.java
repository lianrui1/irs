package cn.com.cowain.ibms.rest.resp.iot;

import cn.com.cowain.ibms.rest.resp.space.ProjectSpaceTreeResp;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/12/3 16:25
 */
@Data
@ApiModel("门禁点树")
public class AccessControlSpotTreeResp extends ProjectSpaceTreeResp {

    @ApiModelProperty(value = "节点类型.PROJECT:项目;SPACE:空间;ACCESS_CONTROL_SPOT:门禁点", required = true, position = 2)
    private String nodeType;

    @ApiModelProperty(value = "子节点列表", required = true, position = 5)
    private List<ProjectSpaceTreeResp> childrenList;
}
