package cn.com.cowain.ibms.rest.resp.iot.doorplate;

import cn.com.cowain.ibms.enumeration.iot.DoorPlateStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2020/11/30 16:25
 */
@Data
@ApiModel("门牌状态信息")
public class DoorPlateStatusResp {

    @ApiModelProperty(value = "门牌ID", required = true)
    private String doorPlateId;

    @ApiModelProperty(name = "门牌状态", example = "FREE: 空闲; IN_PROCESS: 进行中; ABOUT_TO_BEGIN: 即将开始")
    private DoorPlateStatus doorPlateStatus;

    @ApiModelProperty("当前会议信息")
    private DoorPlateReservationRecordResp currentReservationRecord;

    @ApiModelProperty("下一场会议信息")
    private DoorPlateReservationRecordResp nextReservationRecord;

    @ApiModelProperty("会议状态文字描述")
    private String statusDescribe;

    @ApiModelProperty("门牌名称")
    private String doorPlateName;

    @ApiModelProperty("绑定会议室ID")
    private String roomId;

    @ApiModelProperty("绑定会议室名称")
    private String roomName;

    @ApiModelProperty("门磁编号")
    private String magneticNum;

    @ApiModelProperty("设备密码")
    private String password;

    @ApiModelProperty("会议室图片")
    private String roomImg;

    @ApiModelProperty("设备列表")
    private List<String> deviceList;

    @ApiModelProperty("注意事项")
    private String attentions;
}
