package cn.com.cowain.ibms.enumeration.ability;

/**
 * @author Yang.Lee
 * @date 2022/1/27 16:30
 */
public enum MethodType {

    ADD,

    UPDATE,

    DELETE,

    ADDBATCH;
}
