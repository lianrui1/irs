package cn.com.cowain.ibms.utils;

import cn.com.cowain.ibms.dao.SysUserDao;
import cn.com.cowain.ibms.entity.SysUser;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/10/20
 */
@Slf4j
@SpringBootTest
class RedisUtilTest2 {

    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private SysUserDao sysUserDao;

    private String userId = "5f8c27fd-ea13-4255-9b3d-022957c8b20a";

    @Test
    public void test1() {
        Optional<SysUser> optional = sysUserDao.findById(userId);
        SysUser sysUser = optional.get();
        System.out.println(sysUser.getId());
        redisUtil.set(sysUser.getId(), sysUser);
    }

    @Test
    public void test2() {
        JSONObject jsonObject = (JSONObject) redisUtil.get(userId);
        jsonObject.keySet().remove("createdTime");
        jsonObject.keySet().remove("updatedTime");
        SysUser sysUser = JSONObject.toJavaObject(jsonObject, SysUser.class);
        System.out.println(sysUser);
    }

}
