package cn.com.cowain.ibms.websocket.bean;

import cn.com.cowain.ibms.websocket.enumeration.MessageType;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2022/3/4 9:38
 */
@Data
public class MeetingMessage {

    private MessageType cmd;

}
