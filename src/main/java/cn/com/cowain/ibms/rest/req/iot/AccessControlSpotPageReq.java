package cn.com.cowain.ibms.rest.req.iot;

import cn.com.cowain.ibms.enumeration.iot.AccessControlSpotType;
import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/11/7 15:51
 */
@Data
@ApiModel("门禁点分页查询请求")
public class AccessControlSpotPageReq extends PageReq {

    @ApiModelProperty(value = "门禁点名称",  position = 1)
    private String name;

    @ApiModelProperty(value = "门禁点类型",  position = 2)
    private AccessControlSpotType type;

    @ApiModelProperty(value = "空间ID",  position = 3)
    private String spaceId;

    @ApiModelProperty(value = "项目ID", position = 3)
    private String projectId;

    @ApiModelProperty(value = "门禁点ID 根据ID过滤")
    private List<String> ids;
}
