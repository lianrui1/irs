package cn.com.cowain.ibms.rest.resp.device;

import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/17 9:55
 */
@Data
@ApiModel("设备响应对象")
public class SensorDeviceResp {


    @ApiModelProperty(value = "华为云设备ID")
    private String hwDeviceId;

    @ApiModelProperty(value = "设备类型")
    private DeviceType deviceType;
}

