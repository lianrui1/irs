package cn.com.cowain.ibms.rest.resp.distinguish;

import cn.com.cowain.ibms.rest.resp.BaseListResp;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * 人脸识别设备列表数据返回对象
 *
 * @author Yang.Lee
 * @date 2021/3/11 14:04
 */
@Data
@ApiModel("人脸识别设备列表数据对象")
public class DistinguishDeviceListResp extends BaseListResp {
}
