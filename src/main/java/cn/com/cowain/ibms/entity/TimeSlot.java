package cn.com.cowain.ibms.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/6/20
 */

/**
 * 时间槽
 *
 * @author 胡荆陵
 * @since 2020-7-30
 */
@Data
@Entity
@Table(name = "ibms_room_time_slot")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_room_time_slot", comment = "会议室时间槽点")
@Where(clause = "is_del=0")
public class TimeSlot extends BaseEntity {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 从时间
     */
    @Column(name = "ts_from_time", nullable = false, columnDefinition = "varchar(20) COMMENT '从时间'")
    private String from;

    /**
     * 到时间
     */
    @Column(name = "ts_to_time", nullable = false, columnDefinition = "varchar(20) COMMENT '到时间'")
    private String to;

    /**
     * 排序
     */
    @Column(name = "ts_seq", columnDefinition = "integer COMMENT '排序'")
    private int seq;

    /**
     * 关联Room
     */
    @ManyToOne
    @JoinColumn(name = "room_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK'")
    private Room room;

}
