package cn.com.cowain.ibms.enumeration.iot;

/**
 * 数据来源枚举
 *
 * @author Yang.Lee
 * @date 2021/3/30 10:00
 */
public enum DataSource {

    OTHIER("其他"),
    /**
     * 华为云
     **/
    HW_CLOUD("华为云"),

    /**
     * 旷世云
     **/
    MEGVII_CLOUD("旷世云"),

    /**
     * Pad门牌
     **/
    DOORPLATE("门牌PAD"),

    /**
     * IBMS
     **/
    IBMS("IBMS"),

    /**
     * EHR
     **/
    EHR("EHR"),

    /**
     * HK_CONNECTOR
     **/
    HK_CONNECTOR("HK_CONNECTOR");

    DataSource(String name) {
        this.name = name;
    }

    private String name;

    public String getName() {
        return this.name;
    }
}
