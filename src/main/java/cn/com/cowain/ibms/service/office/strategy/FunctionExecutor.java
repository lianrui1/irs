package cn.com.cowain.ibms.service.office.strategy;

import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.rest.resp.device.OfficeCommonDeviceResp;
import cn.com.cowain.ibms.rest.resp.device.centralized.CentralizedControlDeviceFilter;
import cn.com.cowain.ibms.rest.resp.office.ShowDeviceResp;

import java.util.List;

public class FunctionExecutor {

    private final DeviceTypeStrategy deviceTypeStrategy;
    public FunctionExecutor(DeviceTypeStrategy deviceTypeStrategy) {
        this.deviceTypeStrategy=deviceTypeStrategy;
    }
    public void addCommonDevice(ShowDeviceResp showDeviceResp, List<OfficeCommonDeviceResp> commonDeviceRespList) {
         deviceTypeStrategy.addCommonDeviceResp(showDeviceResp, commonDeviceRespList);
    }
    public void filterCommonDevice(IotDevice iotDevice, CentralizedControlDeviceFilter centralizedControlDeviceFilter, String targetAction) {
        deviceTypeStrategy.filterDevicePropertiesStatus(iotDevice, centralizedControlDeviceFilter,targetAction);
    }
    public void sendCommand(IotDevice iotDevice, CentralizedControlDeviceFilter centralizedControlDeviceFilter) {
        // deviceTypeStrategy.sendCommand(showDeviceResp, commonDeviceRespList);
    }

    public boolean isOpen(IotDevice device){
      return deviceTypeStrategy.isOpen(device);
    }

    public DeviceStatus getNewestHwStatus(IotDevice device){
        return deviceTypeStrategy.getNewestStatus(device);
    }
}
