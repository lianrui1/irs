package cn.com.cowain.ibms.service.task;

import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.iot.AccessControlSpotApplicationRecord;
import cn.com.cowain.ibms.entity.task.TaskCenterTask;
import cn.com.cowain.ibms.feign.task_center.bean.UpdateTaskReq;
import cn.com.cowain.ibms.service.bean.ServiceResult;

/**
 * @author wei.cheng
 * @date 2022/03/10 15:52
 */
public interface TaskCenterTaskService {
    /**
     * 创建门禁点授权申请的task
     *
     * @param approver          审批人
     * @param applicationRecord 申请
     * @return
     */
    TaskCenterTask createAccessControlSpotApplicationTask(SysUser approver, AccessControlSpotApplicationRecord applicationRecord);

    /**
     * 更新task
     *
     * @param taskCenterTask
     * @param updateTaskReq
     */
    ServiceResult updateTask(TaskCenterTask taskCenterTask, UpdateTaskReq updateTaskReq);

    /**
     * 向待办中心创建任务（权限申请）
     *
     * @param approverHrId      审批人工号
     * @param approverName      审批人名称
     * @param applicationRecord 申请记录
     * @return
     * @author Yang.Lee
     * @date 2022/7/18 17:58
     **/
    TaskCenterTask createAccessControlSpotApplicationTask(String approverHrId, String approverName, AccessControlSpotApplicationRecord applicationRecord);
}
