package cn.com.cowain.ibms.rest.req.iot;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/18 18:44
 */
@Data
@ApiModel("修改人员通行权限请求对象")
public class AccessControlUpdateTimeReq {

    @ApiModelProperty(value = "门禁点ID")
    private String id;

    @ApiModelProperty(value = "工号")
    private String empNo;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "通行开始时间", required = true)
    private LocalDateTime startTime;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "通行结束时间", required = true)
    private LocalDateTime endTime;
}
