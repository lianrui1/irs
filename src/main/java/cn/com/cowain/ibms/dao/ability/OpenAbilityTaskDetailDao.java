package cn.com.cowain.ibms.dao.ability;

import cn.com.cowain.ibms.entity.ability.OpenAbilityTaskDetail;
import cn.com.cowain.ibms.enumeration.UserImportStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2022/1/27 17:38
 */
public interface OpenAbilityTaskDetailDao extends JpaRepository<OpenAbilityTaskDetail, String>, JpaSpecificationExecutor<OpenAbilityTaskDetail> {

    /**
     * 根据任务ID查询任务详情
     *
     * @param taskId 任务ID
     * @return 结果
     * @author Yang.Lee
     * @date 2022/3/3 13:29
     **/
    List<OpenAbilityTaskDetail> findByTaskId(String taskId);

    /**
     * 根据任务id和人员id查询详情
     *
     * @author: yanzy
     * @param: [taskId, userHrId]
     * @data: 2022/4/8 13:04:24
     */
    List<OpenAbilityTaskDetail> findByTaskIdAndUserHrId(String taskId,String userHrId);

    /**
     * 根据任务id和人员导入状态查询
     *
     * @author: yanzy
     * @data: 2022/4/8 18:04:27
     */
    List<OpenAbilityTaskDetail> findByTaskIdAndUserImportStatus(String taskId, UserImportStatus userImportStatus);

    // 根据任务ID获取
    List<OpenAbilityTaskDetail> findByTaskIdIn(List<String> taskIds);

    List<OpenAbilityTaskDetail> findByTaskIdInAndUserImportStatus(List<String> taskIds, UserImportStatus doing);

    List<OpenAbilityTaskDetail> findByTaskIdInAndUserHrId(List<String> taskIds, String hrID);
}
