package cn.com.cowain.ibms.entity.qrcode;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.distinguish.StaffGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 二维码通行权限 - 用户组 关系对象
 *
 * @author Yang.Lee
 * @date 2021/3/10 9:08
 */
@Data
@Entity
@Table(name = "ibms_qrcode_access_rules_staff_group")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_qrcode_access_rules_staff_group", comment = "二维码通行权限-用户组 关系表")
@Where(clause = "is_del=0")
public class QrcodeAccessRulesToStaffGroup extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "rule_id", nullable = false, columnDefinition = "varchar(36)  COMMENT 'FK 通行权限ID'")
    private QrcodeAccessRules rule;

    @ManyToOne
    @JoinColumn(name = "staff_group_id", nullable = false, columnDefinition = "varchar(36)  COMMENT 'FK 人员组ID'")
    private StaffGroup group;
}
