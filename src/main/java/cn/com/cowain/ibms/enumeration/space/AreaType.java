package cn.com.cowain.ibms.enumeration.space;

/**
 * @author Yang.Lee
 * @date 2020/12/17 16:49
 */
public enum AreaType {

    PROVINCE("省"),

    CITY("市"),

    DISTRICT("区");

    private String name;

    AreaType(String name){
        this.name = name;
    }
}
