package cn.com.cowain.ibms.enumeration.iot;

public enum ControlName {

    AIR_OFF("空调全关"),

    AIR_ON("空调全开"),

    LIGHT_OFF("照明灯全关"),

    LIGHT_ON("照明灯全开"),

    ALL_OFF("一键全关"),

    SINGLE_OFF("单关"),
    SINGLE_ON("单开"),

    ALL_ON("一键全开");

    ControlName(String name) {
        this.name = name;
    }

    private final String name;

    public String getName() {
        return this.name;
    }
}
