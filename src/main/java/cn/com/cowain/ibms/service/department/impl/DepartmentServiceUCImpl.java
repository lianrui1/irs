package cn.com.cowain.ibms.service.department.impl;

import cn.com.cowain.ibms.rest.resp.StaffResp;
import cn.com.cowain.ibms.rest.resp.department.DepartmentResp;
import cn.com.cowain.ibms.rest.resp.department.DepartmentStaffPageResp;
import cn.com.cowain.ibms.rest.resp.department.DepartmentStaffPageRootResp;
import cn.com.cowain.ibms.rest.resp.department.DepartmentTreeResp;
import cn.com.cowain.ibms.service.department.DepartmentService;
import cn.com.cowain.ibms.utils.RedisUtil;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.usercenter.api.service.UserCenterStaffApi;
import cn.com.cowain.usercenter.common.resp.OrganizationToTreeResp;
import cn.com.cowain.usercenter.common.resp.PassportRespForFeign;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 组织部门Service UC实现类
 *
 * @author Yang.Lee
 * @date 2021/2/5 11:29
 */
@Slf4j
@Service("departmentServiceUCImpl")
public class DepartmentServiceUCImpl implements DepartmentService {

    @Resource
    private UserCenterStaffApi userCenterStaffApi;


    @Resource
    private RedisUtil redisUtil;

    /**
     * 部门树 redis key
     */
    private static final String REDIS_KEY_PRE_TREE = "department-tree";

    /**
     * 部门员工redis key, 使用时后面需要拼接部门ID
     */
    private static final String REDIS_KEY_PRE_DEPARTMENT_STAFF = "department-staff-";

    /**
     * redis过期时间，默认24h
     */
    private static final long EXPIRED_TIME = 60 * 60 * 24L;

    /**
     * 获取组织部门树
     *
     * @return 组织部门树
     */
    @Override
    public DepartmentTreeResp getDepartmentTree() {

        DepartmentTreeResp result;

        // 先从redis中获取数据，如果redis中没有则从UC同步数据
        String key = redisUtil.createRealKey(REDIS_KEY_PRE_TREE);

        if (Boolean.TRUE.equals(redisUtil.hasKey(key))) {
            result = JSON.parseObject(String.valueOf(redisUtil.get(key)), DepartmentTreeResp.class);
        } else {
            result = syncTree();
            if (result != null) {
                // 将同步结果存入redis
                redisUtil.set(key, result, EXPIRED_TIME);
            }
        }

        return result;
    }

    /**
     * 获取部门下员工数据
     *
     * @param departmentId 部门ID
     * @return 部门下员工列表（分页）
     */
    @Override
    public DepartmentStaffPageResp getDepartmentStaffPage(long departmentId) {

        DepartmentStaffPageResp result;

        // 生成redis中的key
        String key = redisUtil.createRealKey(REDIS_KEY_PRE_DEPARTMENT_STAFF + departmentId);

        // 判断redis中是否存在数据
        if (Boolean.TRUE.equals(redisUtil.hasKey(key))) {
            result = JSON.parseObject(String.valueOf(redisUtil.get(key)), DepartmentStaffPageResp.class);
        } else {
            // redis不存在数据则从uc同步数据
            result = syncDepartmentStaff(departmentId);
            if (result != null) {
                // 将同步后的数据存入redis
                redisUtil.set(key, result, EXPIRED_TIME);
            }
        }

        return result;
    }

    /**
     * 模糊查询员工信息
     *
     * @param name 姓名关键字
     * @return 员工数据
     */
    @Override
    public DepartmentStaffPageRootResp searchStaff(String name) {

        JsonResult<List<PassportRespForFeign>> result = userCenterStaffApi.passportList(name);
        if (result.getStatus() != 1) {
            log.error("从UC模糊查询用户数据失败！！！姓名关键字： 【" + name + "】。UC返回信息：" + result.toString());
            return null;
        }

        // 数据同步成功后进行对象转换
        DepartmentStaffPageRootResp departmentStaffPageRootResp = new DepartmentStaffPageRootResp();
        departmentStaffPageRootResp.setPage(convert(result.getData()));
        departmentStaffPageRootResp.setMsg("success");
        departmentStaffPageRootResp.setCode(0);

        return departmentStaffPageRootResp;
    }

    /**
     * 从uc同步组织结构树
     *
     * @return 组织部门树
     */
    private DepartmentTreeResp syncTree() {

        JsonResult<List<OrganizationToTreeResp>> result = userCenterStaffApi.list();
        if (result.getStatus() != 1) {
            log.error("从UC同步组织部门树失败！！！UC返回信息：" + result);
            return null;
        } else {

            List<OrganizationToTreeResp> ucTree = result.getData();
            // 同步完成后进行对象转换
            List<DepartmentResp> departmentRespList = new ArrayList<>();
            ucTree.forEach(uc -> departmentRespList.add(convert(uc)));

            DepartmentTreeResp departmentTreeResp = new DepartmentTreeResp();
            departmentTreeResp.setMsg("success");
            departmentTreeResp.setCode(0);
            departmentTreeResp.setTreeList(departmentRespList);

            return departmentTreeResp;
        }
    }

    /**
     * 从UC同步部门内员工数据(分页)
     *
     * @param departmentId 部门ID
     * @return 员工数据
     */
    private DepartmentStaffPageResp syncDepartmentStaff(long departmentId) {

        JsonResult<List<PassportRespForFeign>> result = userCenterStaffApi.passportList(departmentId);

        if (result.getStatus() != 1) {
            log.error("从UC同步部门内员工数据失败！！！UC返回信息：" + result.toString());
            return null;
        } else {

            List<PassportRespForFeign> ucStaffList = result.getData();

            return convert(ucStaffList);
        }
    }

    /**
     * 对象转换
     *
     * @param ucStaffList UC返回的用户列表
     * @return IBMS用户分页对象
     */
    private DepartmentStaffPageResp convert(@NonNull List<PassportRespForFeign> ucStaffList) {

        List<StaffResp> staffList = new ArrayList<>();

        // 进行对象关注按
        if (!ucStaffList.isEmpty()) {
            ucStaffList.forEach(obj -> staffList.add(convert(obj)));
        }

        // 生成分页对象，为了匹配旧版EHR的数据结构
        DepartmentStaffPageResp departmentStaffPageResp = new DepartmentStaffPageResp();
        departmentStaffPageResp.setTotalCount(ucStaffList.size());
        departmentStaffPageResp.setPageSize(9999);
        departmentStaffPageResp.setCurrPage(1);

        int totalPage = departmentStaffPageResp.getTotalCount() == 0
                ? 0 : departmentStaffPageResp.getTotalCount() / departmentStaffPageResp.getPageSize() + 1;
        departmentStaffPageResp.setTotalPage(totalPage);
        departmentStaffPageResp.setList(staffList);

        return departmentStaffPageResp;
    }

    /**
     * 组织树对象转换
     *
     * @param data UC返回的组织树对象
     * @return IBMS的组织树对象
     */
    private DepartmentResp convert(OrganizationToTreeResp data) {

        if (data == null) {
            return null;
        }

        DepartmentResp departmentResp = new DepartmentResp();

        departmentResp.setOrId(data.getId());
        departmentResp.setRoName(data.getOrgName());

        if (data.getChildren() != null && !data.getChildren().isEmpty()) {
            List<DepartmentResp> childrenList = new ArrayList<>();
            for (OrganizationToTreeResp obj : data.getChildren()) {
                childrenList.add(convert(obj));
            }
            departmentResp.setList(childrenList);
        }

        return departmentResp;
    }

    /**
     * 对象转换
     *
     * @param data UC返回的员工对象
     * @return IBMS的员工对象
     */
    private StaffResp convert(@NonNull PassportRespForFeign data) {

        StaffResp staffResp = new StaffResp();

        staffResp.setHrId(data.getJobNo());
        staffResp.setNameZh(data.getRealName());
        staffResp.setNameEn(data.getEnName());
        staffResp.setStDeptName(data.getDepartName());
        staffResp.setSysId(data.getUserId());
        staffResp.setWxId(data.getOpenId());
        staffResp.setHeadImgUrl(data.getHeadImgUrl());
        staffResp.setStId(staffResp.getSysId());

        return staffResp;
    }

}
