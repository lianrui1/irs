package cn.com.cowain.ibms.dao.device;

import cn.com.cowain.ibms.entity.iot.DeviceControlLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeviceControlLogDao extends JpaRepository<DeviceControlLog, String>, JpaSpecificationExecutor<DeviceControlLog> {
    /**
     * 查询deviceId = {@code deviceId}的DeviceControlLog
     *
     * @param deviceId
     * @return
     */
    List<DeviceControlLog> findAllByDeviceId(String deviceId);
}
