package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author tql
 * @Description dueros回调
 * @Date 22-9-26 下午3:21
 * @Version 1.0
 */
@Data
@ApiModel("dueros回调")
public class CallBackDuerosReq {

    @ApiModelProperty("返回头")
    private CallBackDuerosHeaderReq header;


    @ApiModelProperty("属性信息")
    private CallBackDuerosPayloadReq payload;
}
