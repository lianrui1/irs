package cn.com.cowain.ibms.service.space.impl;

import cn.com.cowain.ibms.component.DeviceDictionaryComponent;
import cn.com.cowain.ibms.dao.iot.AirConditionerDao;
import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.dao.iot.SpaceDao;
import cn.com.cowain.ibms.dao.office.IntelligentOfficeDao;
import cn.com.cowain.ibms.dao.office.IntelligentOfficeDeviceDao;
import cn.com.cowain.ibms.dao.office.IntelligentOfficeUserDao;
import cn.com.cowain.ibms.dao.space.ProjectDao;
import cn.com.cowain.ibms.dao.space.SpaceAdminDao;
import cn.com.cowain.ibms.dao.space.SpaceDeviceControlDao;
import cn.com.cowain.ibms.dao.space.SpacePurposeDao;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.iot.AirConditioner;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.iot.Properties;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.office.IntelligentOffice;
import cn.com.cowain.ibms.entity.office.IntelligentOfficeDevice;
import cn.com.cowain.ibms.entity.office.IntelligentOfficeUser;
import cn.com.cowain.ibms.entity.space.Project;
import cn.com.cowain.ibms.entity.space.SpaceAdmin;
import cn.com.cowain.ibms.entity.space.SpaceDeviceControl;
import cn.com.cowain.ibms.entity.space.SpacePurpose;
import cn.com.cowain.ibms.enumeration.IotNodeType;
import cn.com.cowain.ibms.enumeration.iot.DataSource;
import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.enumeration.iot.SpaceType;
import cn.com.cowain.ibms.feign.bean.common.CommonReq;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.Sorts;
import cn.com.cowain.ibms.rest.req.space.SpacePageReq;
import cn.com.cowain.ibms.rest.req.space.SpacePurposeEditReq;
import cn.com.cowain.ibms.rest.req.space.SpaceTreeReq;
import cn.com.cowain.ibms.rest.resp.BaseListResp;
import cn.com.cowain.ibms.rest.resp.DeviceResp;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.StaffResp;
import cn.com.cowain.ibms.rest.resp.device.AirConditionerResp;
import cn.com.cowain.ibms.rest.resp.device.TbDataResp;
import cn.com.cowain.ibms.rest.resp.iot.DeviceWechatResp;
import cn.com.cowain.ibms.rest.resp.iot.IotDeviceResp;
import cn.com.cowain.ibms.rest.resp.meeting.OngoingMeetingResp;
import cn.com.cowain.ibms.rest.resp.space.*;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.DoorMagneticHWReq;
import cn.com.cowain.ibms.service.bean.ServiceMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.device.HwProductsService;
import cn.com.cowain.ibms.service.office.SpaceCacheService;
import cn.com.cowain.ibms.service.space.IotDeviceService;
import cn.com.cowain.ibms.service.space.SpaceService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.ibms.utils.RedisUtil;
import cn.com.cowain.usercenter.common.req.PassportRoleEditReq;
import com.alibaba.fastjson.JSON;
import com.google.common.base.Stopwatch;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;

import javax.annotation.Nullable;
import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 空间管理逻辑处理实现类
 *
 * @author Yang.Lee
 * @date 2020/12/21 9:13
 */
@Slf4j
@Service
public class SpaceServiceImpl implements SpaceService {

    @Autowired
    private HwProductsService productsService;

    @Autowired
    private SpaceDao spaceDao;

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private DeviceDao deviceDao;

    @Autowired
    private SpaceAdminDao spaceAdminDao;

    @Autowired
    private AirConditionerDao airConditionerDao;

    @Resource
    private SpaceDeviceControlDao spaceDeviceControlDao;

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserService;

    @Resource
    private SpacePurposeDao spacePurposeDao;

    @Resource
    private IotDeviceService iotDeviceService;
    @Resource
    private IntelligentOfficeDao intelligentOfficeDao;
    @Resource
    private IntelligentOfficeUserDao intelligentOfficeUserDao;
    @Resource
    private IntelligentOfficeDeviceDao intelligentOfficeDeviceDao;
    @Autowired
    DeviceDictionaryComponent deviceDictionaryComponent;
    @Autowired
    SpaceCacheService spaceCacheService;
    @Autowired
    private RedisUtil redisUtil;
    /**
     * 新增空间信息
     *
     * @param spaceTreeReq
     * @return
     * @author Yang.Lee
     */
    @Override
    @Transactional
    public ServiceResult save(SpaceTreeReq spaceTreeReq) {

        // 校验参数合法性
        ServiceResult result = checkSpaceTreeReq(spaceTreeReq);
        if (!result.isSuccess()) {
            return result;
        }

        if (spaceTreeReq.getParentId() != null && spaceTreeReq.getParentId().length() == 0) {
            // 不存在父ID的时候，将父ID设为null，防止数据库中null与‘’共存
            spaceTreeReq.setParentId(null);
        }

        List<SysUser> adminList = new ArrayList<>();

        // 检查管理员数据
        if (!CollectionUtils.isEmpty(spaceTreeReq.getEmpNos())) {
            for (String empNo : spaceTreeReq.getEmpNos()) {
                SysUser sysUser = sysUserService.getUserByEmpNo(empNo);
                if (sysUser == null) {
                    return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_STAFF, empNo));
                }

                adminList.add(sysUser);
            }
        }

        SpacePurpose spacePurpose = null;
        if (spaceTreeReq.getProperties() != null && StringUtils.isNotBlank(spaceTreeReq.getProperties().getPurposeId())) {
            Optional<SpacePurpose> spacePurposeOptional = spacePurposeDao.findById(spaceTreeReq.getProperties().getPurposeId());
            if (spacePurposeOptional.isEmpty()) {
                return ServiceResult.error(ServiceMessage.getMessageWithColon("未找到空间用途", spaceTreeReq.getProperties().getPurposeId()));
            }
            spacePurpose = spacePurposeOptional.get();
        }

        if (spaceTreeReq.getProperties() != null && StringUtils.isNotBlank(spaceTreeReq.getProperties().getSpaceNumber())) {
            // 判断房间号是否已存在
            List<Space> spaceList = spaceDao.findByProjectIdAndPropertiesLikeAndIsShow(spaceTreeReq.getProjectId(), "%\"spaceNumber\":\"" + spaceTreeReq.getProperties().getSpaceNumber() + "\"%", 1);

            if (!spaceList.isEmpty()) {
                return ServiceResult.error(ServiceMessage.getMessageWithColon("房间编号已存在", spaceTreeReq.getProperties().getSpaceNumber()));
            }
        }

        Space space = new Space();
        // 数据复制
        BeanUtils.copyProperties(spaceTreeReq, space);
        space.setSpaceType(spaceTreeReq.getSpaceType());
        space.setSpacePurpose(spacePurpose);
        space.setPosition(Optional.ofNullable(spaceTreeReq.getProperties()).map(Properties::getSpaceNumber).orElse(null));
        space.setPicture(spaceTreeReq.getImg());
        space.setIsShow(1);
        if (spaceTreeReq.getProperties() != null) {
            space.setProperties(JSON.toJSONString(spaceTreeReq.getProperties()));
        }

        // 查询项目信息
        Optional<Project> projectOp = projectDao.findById(spaceTreeReq.getProjectId());
        projectOp.ifPresent(space::setProject);

        // 空间编号，规则：每个层级增加两位数递增
        String parentNumber = "";
        // 查询父节点信息
        int level = 1;
        if (spaceTreeReq.getParentId() != null) {
            Optional<Space> parentSpaceOp = spaceDao.findById(spaceTreeReq.getParentId());
            if (parentSpaceOp.isPresent()) {
                Space parent = parentSpaceOp.get();
                parentNumber = parent.getNumber();

                // 获取空间层级
                level = parent.getLevel() + 1;
                // 赋值上级空间信息
                space.setParent(parent);
            }
        }

        space.setLevel(level);
        // 父空间下的新空间最大编号
        int max = spaceDao.countByProjectIdAndLevel(spaceTreeReq.getProjectId(), level) + 1;
        // 根据上级编号 + 2位数当前编号，生成新编号
        space.setNumber(parentNumber + String.format("%02d", max));
        spaceDao.save(space);
        spaceCacheService.setSpaceInRedis(space);
        // 保存管理员
        spaceAdminDao.saveAll(adminList.stream().map(obj -> {
            SpaceAdmin spaceAdmin = new SpaceAdmin();
            spaceAdmin.setSpace(space);
            spaceAdmin.setAdminEmpNo(obj.getEmpNo());
            spaceAdmin.setAdminName(obj.getNameZh());
            spaceAdmin.setAdminDept(obj.getFullDepartmentName());
            return spaceAdmin;
        }).collect(Collectors.toList()));

        // 给用户添加UC的权限
        // 判断添加权限的空间是否是智能会议室，如果是，则开通uc权限
        PassportRoleEditReq passportRoleEditReq = new PassportRoleEditReq();
        passportRoleEditReq.setAddUserHrIdList(adminList.stream().map(SysUser::getEmpNo).collect(Collectors.toList()));
        passportRoleEditReq.setRmUserHrIdList(new ArrayList<>());

        ServiceResult serviceResult = editUCIntelligentOfficeRole(space.getId(), passportRoleEditReq);
        if (!serviceResult.isSuccess()) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return serviceResult;
        }

        // 返回主键ID
        IdResp idResp = new IdResp();
        idResp.setId(space.getId());

        return ServiceResult.ok(idResp);
    }

    /**
     * 编辑空间信息
     *
     * @param id
     * @param target
     * @return
     * @author Yang.Lee
     */
    @Override
    @Transactional
    public ServiceResult update(String id, SpaceTreeReq target) {

        // 查询数据库中的空间信息
        Optional<Space> sourceOp = spaceDao.findById(id);
        if (!sourceOp.isPresent()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_SPACE, id));
        }

        // 参数校验
        ServiceResult result = checkSpaceTreeReq(target);
        if (!result.isSuccess()) {
            return result;
        }

        Space source = sourceOp.get();

        // 修改父空间
        if (source.getLevel() > 1) {
            Optional<Space> targetParentSpaceOp = spaceDao.findById(target.getParentId());
            if (!targetParentSpaceOp.isPresent()) {
                return ServiceResult.error("编辑失败，未找到父空间信息");
            }
            source.setParent(targetParentSpaceOp.get());
            source.setLevel(source.getParent().getLevel() + 1);
        }

        SpacePurpose spacePurpose = null;
        if (target.getProperties() != null && StringUtils.isNotBlank(target.getProperties().getPurposeId())) {
            Optional<SpacePurpose> spacePurposeOptional = spacePurposeDao.findById(target.getProperties().getPurposeId());
            if (spacePurposeOptional.isEmpty()) {
                return ServiceResult.error(ServiceMessage.getMessageWithColon("未找到空间用途", target.getProperties().getPurposeId()));
            }
            spacePurpose = spacePurposeOptional.get();
        }

        if (target.getProperties() != null && StringUtils.isNotBlank(target.getProperties().getSpaceNumber())) {
            // 判断房间号是否已存在
            List<Space> spaceList = spaceDao.findByProjectIdAndPropertiesLikeAndIsShow(target.getProjectId(), "%\"spaceNumber\":\"" + target.getProperties().getSpaceNumber() + "\"%", 1);

            for (Space space : spaceList) {
                if (!space.getId().equals(id)) {
                    return ServiceResult.error(ServiceMessage.getMessageWithColon("房间编号已存在", target.getProperties().getSpaceNumber()));

                }
            }
        }

        // 数据赋值
        source.setName(target.getName());
        source.setRemark(target.getRemark());
        source.setSpaceType(target.getSpaceType());
        source.setSpacePurpose(spacePurpose);
        source.setPosition(Optional.ofNullable(target.getProperties()).map(Properties::getSpaceNumber).orElse(null));
        source.setPicture(target.getImg());
        source.setIntroduction(target.getIntroduction());
        source.setFloor(target.getFloor());

        if (target.getProperties() != null) {
            source.setProperties(JSON.toJSONString(target.getProperties()));
        } else {
            source.setProperties(null);
        }

        // 保存修改
        spaceDao.save(source);
        spaceCacheService.setSpaceInRedis(source);
        // 修改管理员信息
        List<SpaceAdmin> spaceAdminList = spaceAdminDao.findBySpaceId(source.getId());

        Set<String> dbAdminHrIdSet = spaceAdminList.stream().map(SpaceAdmin::getAdminEmpNo).collect(Collectors.toSet());
        Set<String> reqAdminHrIdSet = new HashSet<>(target.getEmpNos());

        // 找出所有需要删除的数据
        Set<String> delSet = new HashSet<>(dbAdminHrIdSet);
        delSet.removeAll(reqAdminHrIdSet);

        List<SpaceAdmin> delList = spaceAdminList.stream()
                .filter(obj -> delSet.contains(obj.getAdminEmpNo()))
                .map(obj -> {
                    obj.setIsDelete(1);
                    return obj;
                })
                .collect(Collectors.toList());
        spaceAdminDao.saveAll(delList);
        spaceCacheService.delSpaceAdmin(delList);
        //删除已分配关联的数据
        if(!CollectionUtils.isEmpty(delList)) {
            Optional<IntelligentOffice> intelligentOffice= intelligentOfficeDao.findBySpaceId(id);
            if(intelligentOffice.isPresent()){
                for(SpaceAdmin spaceAdmin:delList) {
                    List<IntelligentOfficeUser>  userList=  intelligentOfficeUserDao.findByIntelligentOfficeIdAndInvitedUserHrId(intelligentOffice.get().getId(),spaceAdmin.getAdminEmpNo());
                        if(!CollectionUtils.isEmpty(userList)){
                            for(IntelligentOfficeUser officeUser:userList){
                                officeUser.setIsDelete(1);
                                intelligentOfficeUserDao.save(officeUser);
                                List<IntelligentOfficeDevice>  waitDeleteList= intelligentOfficeDeviceDao.findByIntelligentOfficeIdAndUserHrId( intelligentOffice.get().getId(),officeUser.getUserHrId());
                                if(org.apache.commons.collections.CollectionUtils.isNotEmpty(waitDeleteList)){
                                    for(IntelligentOfficeDevice device:waitDeleteList){
                                        device.setIsDelete(1);
                                    }
                                    intelligentOfficeDeviceDao.saveAll(waitDeleteList);
                                }
                            }
                        }
                }
            }
        }

        // 找到所有需要添加的数据
        Set<String> addSet = new HashSet<>(reqAdminHrIdSet);
        addSet.removeAll(dbAdminHrIdSet);

        List<SpaceAdmin> addList = addSet.stream().map(obj -> {

            SysUser sysuser = sysUserService.getUserByEmpNo(obj);

            SpaceAdmin spaceAdmin = new SpaceAdmin();
            spaceAdmin.setSpace(source);
            spaceAdmin.setAdminEmpNo(obj);
            spaceAdmin.setAdminName(sysuser.getNameZh());
            spaceAdmin.setAdminDept(sysuser.getFullDepartmentName());
            return spaceAdmin;
        }).collect(Collectors.toList());

        spaceAdminDao.saveAll(addList);
        return ServiceResult.ok();
    }

    /**
     * 根据空间ID删除空间
     *
     * @param spaceId
     * @return
     * @author Yang.Lee
     */
    @Override
    @Transactional
    public ServiceResult delete(String spaceId) {

        // 验证空间是否存在
        Optional<Space> spaceOp = spaceDao.findById(spaceId);
        if (!spaceOp.isPresent()) {
            return ServiceResult.error("删除失败，未查询到空间信息");
        }

        Space space = spaceOp.get();
        // 查询所有子空间
        List<SpaceTreeResp> spaceTreeRespList = getListByParentId(space.getId());
        // 对象转换
        SpaceTreeResp spaceTreeResp = convertSpace2SpaceTreeResp(space);
        spaceTreeResp.setChildrenList(spaceTreeRespList);
        // 获取该空间下的所有子空间，并判断是否绑定设备，若有绑定设备则该空间不可删除
        boolean couldDelete = isSpaceCouldDelete(spaceTreeResp);

        if (!couldDelete) {
            return ServiceResult.error("该空间或子空间下已绑定设备，不允许删除");
        }
        // 获取空间及所有子空间id
        List<String> ids = getIds(spaceTreeResp);
        spaceDao.deleteByIds(ids);
        spaceCacheService.delSpaces(ids);
        return ServiceResult.ok();
    }

    /**
     * 根据空间ID查询空间信息
     *
     * @param spaceId
     * @return
     * @author Yang.Lee
     */
    @Override
    @Transactional
    public ServiceResult get(String spaceId) {

        Optional<Space> spaceOp = spaceDao.findById(spaceId);
        if (!spaceOp.isPresent()) {
            return ServiceResult.error("空间ID错误，未查询到结果");
        }

        Space space = spaceOp.get();
        SpaceTreeResp spaceTreeResp = convertSpace2SpaceTreeResp(space);
        // 查询该节点下所有子节点
        spaceTreeResp.setChildrenList(getListByParentId(space.getId()));

        // 查询空间管理员
        List<SpaceAdmin> spaceAdminList = spaceAdminDao.findBySpaceId(space.getId());
        spaceTreeResp.setAdminList(spaceAdminList.stream().map(obj -> {
            SysUser sysUser = sysUserService.getUserByEmpNo(obj.getAdminEmpNo());
            return StaffResp.convert(sysUser);
        }).collect(Collectors.toList()));

        return ServiceResult.ok(spaceTreeResp);
    }

    @Override
    public ServiceResult spacePlaneDetail(String spaceId) {
        return null;
    }

    /**
     * 分页查询空间列表
     *
     * @param page
     * @param size
     * @return
     */
    @Override
    @Transactional
    public ServiceResult getPage(int page, int size) {
        return null;
    }

    /**
     * 根据项目ID获取所有空间列表
     *
     * @param projectID
     * @return
     * @author Yang.Lee
     */
    @Override
    @Transactional
    public ServiceResult getListByProjectId(String projectID) {

//        List<Space> list = spaceDao.findAllByProjectIdAndLevelGreaterThanOrderByCreatedTimeDesc(projectID, 0);
        List<Space> spaceList = spaceDao.findAll((root, query, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();

            list.add(criteriaBuilder.equal(root.get("project").get("id"), projectID));
            list.add(criteriaBuilder.equal(root.get("isShow"), 1));
            list.add(criteriaBuilder.greaterThan(root.get("level"), 0));

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, Sort.by(Sort.Direction.DESC, Sorts.CREATED_TIME));

        List<SpaceTreeResp> spaceTreeRespList = getSpaceTreeResp(spaceList);

        return ServiceResult.ok(spaceTreeRespList);
    }

    /**
     * 查询所有空间列表（空间树）
     *
     * @return
     * @author Yang.Lee
     */
    @Override
    @Transactional
    public ServiceResult getList() {

        // 查询所有大于0的数据
        List<Space> list = spaceDao.findByLevelGreaterThan(0);
        List<SpaceTreeResp> spaceTreeRespList = getSpaceTreeResp(list);

        return ServiceResult.ok(spaceTreeRespList);
    }

    /**
     * 获取上级空间列表
     *
     * @param projectId
     * @param level
     * @return
     */
    @Override
    public ServiceResult getPreLevelList(String projectId, int level) {

        List<SpaceParentResp> result = new ArrayList<>();

        // 查询该项目层级内的所有数据
        List<Space> spaceList = spaceDao.findAllByProjectIdAndLevelLessThanEqual(projectId, level);
        if (spaceList == null || spaceList.isEmpty()) {
            return ServiceResult.ok(result);
        }

        Map<String, Space> spaceMap = new HashMap<>();

        // 目标层级列表
        List<Space> levelList = new ArrayList<>();
        // List转Map
        for (Space space : spaceList) {
            spaceMap.put(space.getId(), space);
            if (space.getLevel() == level) {
                levelList.add(space);
            }
        }

        List<SpaceParentResp> resultList = new ArrayList<>();
        SpaceParentResp spaceParentResp = null;
        for (Space obj : levelList) {
            spaceParentResp = new SpaceParentResp();
            spaceParentResp.setId(obj.getId());
            String name = getTreeFullName(obj, spaceMap, "");
            spaceParentResp.setName(name);
            resultList.add(spaceParentResp);
        }

        // 按照名称排序
        resultList = resultList.stream().sorted(Comparator.comparing(SpaceParentResp::getName)).collect(Collectors.toList());

        return ServiceResult.ok(resultList);
    }

    /**
     * 获取空间全程
     * <p>example : A楼/1层/101</p>
     *
     * @param spaceTreeResp
     * @param spaceTreeRespMap
     * @param content
     * @return
     */
    private String getTreeFullName(Space spaceTreeResp, Map<String, Space> spaceTreeRespMap, String content) {
        StringBuilder stringBuilder = new StringBuilder();
        String parentName = "";
        String currentName = "";
        if (!content.equals("")) {
            currentName = spaceTreeResp.getName() + "/" + content;
        } else {
            currentName = content + spaceTreeResp.getName();
        }
        if (spaceTreeResp.getLevel() != 1) {
            Space parent = spaceTreeRespMap.get(spaceTreeResp.getParent().getId());
            parentName = getTreeFullName(parent, spaceTreeRespMap, currentName);
            stringBuilder.append(parentName);

        } else {
            stringBuilder.append(currentName);
        }

        return stringBuilder.toString();
    }

    /**
     * 查询SpaceTreeReq参数合法性
     *
     * @param spaceTreeReq
     * @return
     */
    @Transactional
    public ServiceResult checkSpaceTreeReq(SpaceTreeReq spaceTreeReq) {
        // 1.查看项目是否存在
        Optional<Project> projectOp = projectDao.findById(spaceTreeReq.getProjectId());
        if (!projectOp.isPresent()) {
            return ServiceResult.error("项目不存在");
        }

//        if (spaceTreeReq.getLevel() != 1 && (spaceTreeReq.getParentId() == null || spaceTreeReq.getParentId().length() == 0)) {
//            return ServiceResult.error("校验错误，空间层级不为1，但父id为空");
//        }
//
//        if (spaceTreeReq.getLevel() == 1 && spaceTreeReq.getParentId() != null && spaceTreeReq.getParentId().length() > 0) {
//            return ServiceResult.error("校验错误，空间层级为1，但父id不为空");
//        }

        if (spaceTreeReq.getParentId() != null && spaceTreeReq.getParentId().length() > 0) {

            // 2.查询父节点是否存在
            Optional<Space> parentSpaceOp = spaceDao.findById(spaceTreeReq.getParentId());

            if (!parentSpaceOp.isPresent()) {
                return ServiceResult.error("父节点不存在");
            }
            // 3.层级检查
//            Space parentSpace = parentSpaceOp.get();
//            int parentLevel = parentSpace.getLevel();
//            if (spaceTreeReq.getLevel() - 1 != parentLevel) {
//                return ServiceResult.error("节点层级不匹配，父节点层级为：" + parentLevel + ", 子节点层级为：" + spaceTreeReq.getLevel());
//            }
//
//            // 判断与父空间项目是否一致
//            Optional<Project> parentProjectOp = projectDao.findById(parentSpace.getProject().getId());
//            if (!parentProjectOp.isPresent()) {
//                log.error("父空间项目信息不存在！！！！ 父空间ID：" + parentSpace.getId() + " ， 项目id:" + parentSpace.getProject().getId());
//                return ServiceResult.error("校验错误，未找到父空间项目信息");
//            }
//            Project parentProject = parentProjectOp.get();
//            if (!parentProject.getId().equals(spaceTreeReq.getProjectId())) {
//                return ServiceResult.error("校验错误，项目ID与父空间不匹配");
//            }
        }

        return ServiceResult.ok();
    }

    /**
     * 将空间列表转换成空间树对象
     *
     * @param spaceList
     * @return
     * @Yang.Lee
     */
    @Override
    public List<SpaceTreeResp> getSpaceTreeResp(List<Space> spaceList) {

        List<SpaceTreeResp> spaceTreeRespList = new ArrayList<>();

        if (spaceList == null || spaceList.isEmpty()) {
            return spaceTreeRespList;
        }

        // 将list对象转成Map
        Map<String, SpaceTreeResp> spaceMap = new HashMap<>();
        SpaceTreeResp spaceTreeResp = null;
        Project project = null;
        for (Space space : spaceList) {
            spaceTreeResp = new SpaceTreeResp();
            // 数据赋值
            BeanUtils.copyProperties(space, spaceTreeResp);
            project = space.getProject();
            spaceTreeResp.setProjectId(project.getId());
            spaceTreeResp.setProjectName(project.getProjectName());
            spaceTreeResp.setParentId(space.getParent() == null ? null : space.getParent().getId());
            spaceTreeResp.setNumber(space.getNumber());

            spaceMap.put(space.getId(), spaceTreeResp);
        }

        // 循环查询父节点
        for (Map.Entry<String, SpaceTreeResp> entry : spaceMap.entrySet()) {
            SpaceTreeResp node = entry.getValue();
            if (node.getParentId() == null) {
                // 如果是顶层节点，直接添加到结果集合中
                spaceTreeRespList.add(node);
            } else {
                // 如果不是顶层节点，找其父节点，并且添加到父节点的子节点集合中
                if (spaceMap.get(node.getParentId()) != null) {
                    if (spaceMap.get(node.getParentId()).getChildrenList() == null) {
                        spaceMap.get(node.getParentId()).setChildrenList(new ArrayList<>());
                    }
                    spaceMap.get(node.getParentId()).getChildrenList().add(node);
                }
            }
        }

        return spaceTreeRespList;
    }

    @Override
    public ServiceResult searchDeviceProperties(String spaceId) {

        Optional<Space> spaceOp = spaceDao.findById(spaceId);
        if(spaceOp.isEmpty()){
            return ServiceResult.error("ID有误,空间不存在");
        }

        OngoingMeetingResp ongoingMeetingResp = new OngoingMeetingResp();
        List<DeviceWechatResp> deviceWechatList = new ArrayList<>();
        List<DeviceWechatResp> deviceControlList = new ArrayList<>();
        DeviceResp deviceResp = new DeviceResp();

        for(IotDevice device : deviceDao.findBySpaceId(spaceId)){
                DeviceWechatResp deviceWechatResp = new DeviceWechatResp();
                deviceWechatResp.setDeviceId(device.getId());
                deviceWechatResp.setDeviceName(device.getDeviceName());
                deviceWechatResp.setType(device.getDeviceType());
                deviceWechatResp.setTypeName(device.getDeviceType().getName());
                deviceWechatResp.setStatus(device.getHwStatus());

                // 空调温度
                if(DeviceType.AIR_CONDITIONER.equals(device.getDeviceType())) {
                    //获取设备所属产品Id
                    ServiceResult result = productsService.findProductProperties(device);
                    if(!result.isSuccess()){
                        deviceWechatResp.setStatus(DeviceStatus.OFFLINE);
                    }else {
                        DoorMagneticHWReq hwReq = (DoorMagneticHWReq) result.getObject();
                        ServiceResult airResult = productsService.findAirProperties(device.getHwDeviceId(), hwReq.getService_id());
                        if(airResult.isSuccess()){
                            AirConditionerResp airConditionerResp = (AirConditionerResp)airResult.getObject();
                            //获取空调温度
                            deviceWechatResp.setAirTemp(airConditionerResp.getTemperature());
                        }else {
                            deviceWechatResp.setStatus(DeviceStatus.OFFLINE);
                        }
                    }

//                    Optional<AirConditioner> airConditionerOp = airConditionerDao.findByDevice(device);
//                    deviceWechatResp.setAirTemp(airConditionerOp.isPresent() ? airConditionerOp.get().getTemperature() : null);
            }
            deviceWechatList.add(deviceWechatResp);

            if(null != device.getSpaceControl() && 1 == device.getSpaceControl()){
                deviceControlList.add(deviceWechatResp);
            }

            if(DeviceType.PM_SENSOR.equals(device.getDeviceType()) && DeviceStatus.ONLINE.equals(device.getHwStatus())){
                String tbId = productsService.findTbId(device.getHwDeviceId());
                if (StringUtils.isNotBlank(tbId)) {
                    List<TbDataResp> data = productsService.findNowData(tbId);
                    deviceResp.setPm10(!data.isEmpty() ? data.get(0).getPm10() : "");
                    deviceResp.setPm2_5(!data.isEmpty() ? data.get(0).getPm2_5() : "");
                }
            }else if(DeviceType.NOISE_SENSOR.equals(device.getDeviceType()) && DeviceStatus.ONLINE.equals(device.getHwStatus())){
                String tbId = productsService.findTbId(device.getHwDeviceId());
                if (StringUtils.isNotBlank(tbId)) {
                    List<TbDataResp> data = productsService.findNowData(tbId);
                    deviceResp.setNoise(!data.isEmpty() ? data.get(0).getNoise() : "");
                }
            }else if(DeviceType.TEMPERATURE_AND_HUMIDITY_AND_ILLUMINATION_SENSOR.equals(device.getDeviceType()) && DeviceStatus.ONLINE.equals(device.getHwStatus())){
                String tbId = productsService.findTbId(device.getHwDeviceId());
                if (StringUtils.isNotBlank(tbId)) {
                    List<TbDataResp> data = productsService.findNowData(tbId);
                    deviceResp.setTemperature(!data.isEmpty() ? data.get(0).getTemperature() : "");
                    deviceResp.setHumidity(!data.isEmpty() ? data.get(0).getHumidity() : "");
                    deviceResp.setLuminance(!data.isEmpty() ? data.get(0).getLuminance() : "");
                }
            }
        }

        ongoingMeetingResp.setValue(deviceResp);
        ongoingMeetingResp.setDeviceControlList(deviceControlList);
        ongoingMeetingResp.setDeviceWechatList(deviceWechatList);



        return ServiceResult.ok(ongoingMeetingResp);
    }

    @Override
    public ServiceResult searchFloorTree(String projectId, String spaceId) {


        List<Space> spaceList = spaceDao.findAll((root, criteriaQuery, criteriaBuilder) -> {

            List<Predicate> predicateList = new ArrayList<>();

            if(StringUtils.isNotEmpty(projectId)){
                predicateList.add(criteriaBuilder.equal(root.get("project").get("id"), projectId));
            }
            if(StringUtils.isNotEmpty(spaceId)){
                predicateList.add(criteriaBuilder.equal(root.get("parent").get("id"), spaceId));
            }
            predicateList.add(criteriaBuilder.equal(root.get("isShow"), 1));
            predicateList.add(criteriaBuilder.equal(root.get("spaceType"), SpaceType.FLOOR));
            return criteriaBuilder.and(predicateList.toArray(new Predicate[0]));

            },Sort.by(Sort.Direction.DESC, Sorts.CREATED_TIME));

        List<String> spaceIds = new ArrayList<>();
        for(Space space : spaceList){
            if(null != space.getParent() && !spaceIds.contains(space.getParent().getId())){
                spaceIds.add(space.getParent().getId());
            }
        }

        SpaceResp spaceResps = new SpaceResp();
        List<SpaceFloorResp> spaceFloorResps = new ArrayList<>();
        spaceIds.forEach(it ->{
            Optional<Space> spaceOptional = spaceDao.findById(it);
            if(spaceOptional.isPresent()){
                SpaceFloorResp spaceFloorResp = new SpaceFloorResp();
                Space space = spaceOptional.get();
                spaceFloorResp.setSpaceId(space.getId());
                spaceFloorResp.setSpaceName(space.getName());
                spaceFloorResp.setSpaceType(space.getSpaceType());

                // 获取子空间楼层信息
                List<SpaceFloorResp> spaceFloorRespList = new ArrayList<>();
                spaceDao.findByParentIdAndSpaceType(space.getId(), SpaceType.FLOOR).forEach(floor ->{
                    SpaceFloorResp floorResp = new SpaceFloorResp();
                    floorResp.setSpaceId(floor.getId());
                    floorResp.setSpaceType(floor.getSpaceType());
                    floorResp.setSpaceName(floor.getName());
                    floorResp.setFloor(floor.getFloor());
                    spaceFloorRespList.add(floorResp);
                });
                spaceFloorResp.setSpaceList(spaceFloorRespList);
                spaceFloorResps.add(spaceFloorResp);
            }
        });

        spaceResps.setSpaceList(spaceFloorResps);
        return ServiceResult.ok(spaceResps);
    }

    @Override
    public List<ProjectSpaceTreeResp> getSpaceTree() {

        List<ProjectSpaceTreeResp> treeRespList = new ArrayList<>();

        List<Project> projectList = projectDao.findAll(Sort.by(Sort.Direction.DESC, "createdTime"));
        if (projectList.isEmpty()) {
            return new ArrayList<>();
        }
        projectList.forEach(project -> {
            ProjectSpaceTreeResp projectResp = new ProjectSpaceTreeResp();
            projectResp.setId(project.getId());
            projectResp.setName(project.getProjectName());
            projectResp.setNodeType("PROJECT");
            List<ProjectSpaceTreeResp> childrenList = new ArrayList<>();
            spaceDao.findByProjectIdAndIsShowAndLevel(project.getId(), 1, 1).forEach(space -> {
                ProjectSpaceTreeResp spaceResp = new ProjectSpaceTreeResp();
                spaceResp.setId(space.getId());
                spaceResp.setName(space.getName());
                spaceResp.setNodeType("SPACE");
                childrenList.add(spaceResp);
            });
            projectResp.setChildrenList(childrenList);
            treeRespList.add(projectResp);
        });

        return treeRespList;
    }

    @Override
    public DeviceInfoShowRoomResp getDeviceInfo(String deviceId) {
      Optional<IotDevice> optionalIotDevice= deviceDao.findById(deviceId);
      if(optionalIotDevice.isPresent()){
         return  DeviceInfoShowRoomResp.convert(optionalIotDevice.get());
      }
        return null;
    }
    /**
     *  @title  电视创建设备
     *  @Description 描述
     *  @author jf.sui
     *  @Date
     */
    @Override
    public DeviceInfoShowRoomResp getCreateTVDevice(CommonReq req) {

        Optional<IotDevice> deviceOptional= deviceDao.findBySn(req.getSn());
        //如果已经有了sn 则直接返回已经有的设备。
        if(deviceOptional.isPresent()){
            return  DeviceInfoShowRoomResp.convert(deviceOptional.get());
        }

        IotDevice device =new IotDevice();
        device.setDataSource(DataSource.OTHIER);
        device.setDeviceName(req.getName());
        device.setDeviceNo(0);
        device.setDeviceType(DeviceType.SCREEN);
        device.setHwStatus(DeviceStatus.ONLINE);
        device.setIotNodeType(IotNodeType.DIRECT_DEVICE);
        device.setSn(req.getSn());
        device.setStatus(1);
        deviceDao.save(device);
        return  DeviceInfoShowRoomResp.convert(device);
    }
    /**
     * 根据父空间ID获取所有子列表
     *
     * @param parentId
     * @return
     */
    @Transactional
    public List<SpaceTreeResp> getListByParentId(String parentId) {

        List<SpaceTreeResp> result = new ArrayList<>();

        List<Space> spaceList = spaceDao.findAllByParentId(parentId);

        SpaceTreeResp spaceTreeResp = null;
        if (spaceList != null && !spaceList.isEmpty()) {
            for (Space space : spaceList) {

                spaceTreeResp = convertSpace2SpaceTreeResp(space);
                // 递归获取子节点的子节点列表
                spaceTreeResp.setChildrenList(getListByParentId(space.getId()));

                result.add(spaceTreeResp);
            }
        }
        return result;
    }

    /**
     * 将Space对象转换成SpaceTreeResp对象
     *
     * @param space
     * @return
     */
    private SpaceTreeResp convertSpace2SpaceTreeResp(Space space) {
        SpaceTreeResp spaceTreeResp = new SpaceTreeResp();

        BeanUtils.copyProperties(space, spaceTreeResp);
        Project project = space.getProject();
        spaceTreeResp.setProjectId(project.getId());
        spaceTreeResp.setProjectName(project.getProjectName());
        spaceTreeResp.setParentId(space.getParent() == null ? "" : space.getParent().getId());
        spaceTreeResp.setParentName(space.getParent() == null ? "" : SpaceService.getFullSpaceName(space.getParent(), space.getParent().getName()));
        spaceTreeResp.setNumber(space.getNumber());
        spaceTreeResp.setSpaceTypeName(space.getSpaceType().getName());
        spaceTreeResp.setImg(space.getPicture());
        spaceTreeResp.setFloor(space.getFloor());
        if (StringUtils.isNotBlank(space.getProperties())) {
            Properties properties = JSON.parseObject(space.getProperties(), Properties.class);
            spaceTreeResp.setProperties(properties);
        }

        return spaceTreeResp;
    }

    /**
     * 判断是否可以删除空间
     *
     * @param source 需要被删除的空间对象
     * @return
     */
    @Transactional
    public boolean isSpaceCouldDelete(SpaceTreeResp source) {
        boolean result = true;
        List<IotDevice> deviceList = deviceDao.findAllBySpaceId(source.getId());

        if (deviceList != null && !deviceList.isEmpty()) {
            // 如果空间下绑定设备，则不可删除
            result = false;
        } else {
            // 如果空间下不绑定设备，则判断子空间
            for (SpaceTreeResp spaceTreeResp : source.getChildrenList()) {
                result = isSpaceCouldDelete(spaceTreeResp);
            }
        }

        return result;
    }

    /**
     * 获取空间及其子空间id列表
     *
     * @param source
     * @return
     */
    List<String> getIds(SpaceTreeResp source) {
        List<String> ids = new ArrayList<>();
        ids.add(source.getId());

        if (source.getChildrenList() != null && !source.getChildrenList().isEmpty()) {
            for (SpaceTreeResp spaceTreeResp : source.getChildrenList()) {
                ids.addAll(getIds(spaceTreeResp));
            }
        }

        return ids;
    }

    /**
     * 按照创建时间排序
     *
     * @param source
     * @return
     */
    @Override
    public List<SpaceTreeResp> sortByCreateTime(List<SpaceTreeResp> source) {

        if (source != null && !source.isEmpty()) {
            // 排序本身
            source = source.stream().sorted(Comparator.comparing(SpaceTreeResp::getCreatedTime)).collect(Collectors.toList());
            // 排序子列表
            source.forEach(obj -> {
                if (obj.getChildrenList() != null && !obj.getChildrenList().isEmpty()) {
                    obj.setChildrenList(sortByCreateTime(obj.getChildrenList()));
                }
            });
        }

        return source;
    }

    /**
     * 查询项目内最大空间层级
     *
     * @param projectId
     * @return
     */
    @Override
    @Transactional
    public ServiceResult getMaxLevel(String projectId) {
        // 查询最大层级
        Integer maxLevel = spaceDao.findMaxLevelByProjectId(projectId);

        SpaceLevelResp spaceLevelResp = new SpaceLevelResp();
        spaceLevelResp.setMax(maxLevel);

        return ServiceResult.ok(spaceLevelResp);
    }

    /**
     * 根据空间查询空间下的设备列表
     *
     * @param spaceId 空间ID
     * @return 业务处理结果，当ServiceResult.isSuccess() = true是，返回设备列表。否则返回错误信息（String）
     */
    @Override
    @Transactional
    public ServiceResult getDeviceList(String spaceId) {

        List<IotDevice> deviceList = deviceDao.findBySpaceId(spaceId);
        List<DeviceWechatResp> list = new ArrayList<>();
        for (IotDevice dev : deviceList) {
            DeviceWechatResp device = new DeviceWechatResp();

            device.setStatus(dev.getHwStatus());
            device.setType(dev.getDeviceType());
            // 如果设备有华为云id合名称则展示华为云数据
            if (dev.getHwDeviceId() == null) {
                device.setDeviceId(dev.getId());
                device.setDeviceName(dev.getDeviceName());
            } else {
                device.setDeviceId(dev.getHwDeviceId());
                device.setDeviceName(dev.getHwDeviceName());
            }
            // 如果设备没有状态统一返回在线
            if (dev.getHwStatus() == null) {
                device.setStatus(DeviceStatus.ONLINE);
            }
            list.add(device);
        }

        return ServiceResult.ok(list);
    }


    @Override
    public ServiceResult getDeviceListAll(String spaceId, OngoingMeetingResp ongoingMeetingResp) {

        List<IotDevice> deviceList = deviceDao.findBySpaceId(spaceId);
        List<DeviceWechatResp> list = new ArrayList<>();
        DeviceResp deviceResp = new DeviceResp();
        for (IotDevice dev : deviceList) {
            DeviceWechatResp device = new DeviceWechatResp();

            device.setStatus(dev.getHwStatus());
            device.setType(dev.getDeviceType());
            // 如果设备有华为云id合名称则展示华为云数据
            if (dev.getHwDeviceId() == null) {
                device.setDeviceId(dev.getId());
                device.setDeviceName(dev.getDeviceName());
            } else {
                device.setDeviceId(dev.getHwDeviceId());
                device.setDeviceName(dev.getHwDeviceName());
                // 传感器设备
                if (DeviceType.TEMPERATURE_AND_HUMIDITY_AND_ILLUMINATION_SENSOR.equals(dev.getDeviceType()) || DeviceType.NOISE_SENSOR.equals(dev.getDeviceType()) || DeviceType.PM_SENSOR.equals(dev.getDeviceType())) {
                    String tbId = productsService.findTbId(dev.getHwDeviceId());
                    if (tbId != null) {
                        List<TbDataResp> data = productsService.findNowData(tbId);
                        if (!data.get(0).getPm2_5().isEmpty()) {
                            deviceResp.setPm2_5(data.get(0).getPm2_5());
                            deviceResp.setPm10(data.get(0).getPm10());
                        }
                        if (!data.get(0).getNoise().isEmpty()) {
                            deviceResp.setNoise(data.get(0).getNoise());
                        }
                        if (!data.get(0).getLuminance().isEmpty()) {
                            deviceResp.setLuminance(data.get(0).getLuminance());
                            deviceResp.setTemperature(data.get(0).getTemperature());
                            deviceResp.setHumidity(data.get(0).getHumidity());
                        }
                    }
                }
                // 空调
                if (DeviceType.AIR_CONDITIONER.equals(dev.getDeviceType())) {
                    Optional<AirConditioner> byDevice = airConditionerDao.findByDevice(dev);
                    // 判断该空调是否是首次操作 是则返回空 不是则返回上次操作命令参数
                    if (byDevice.isPresent()) {
                        AirConditioner airConditioner = byDevice.get();
                        BeanUtils.copyProperties(airConditioner, ongoingMeetingResp);
                        deviceResp.setSetTemp(airConditioner.getTemperature());
                    } else {
                        deviceResp.setSetTemp(23);
                    }
                }
            }
            // 如果设备没有状态统一返回在线
            if (dev.getHwStatus() == null) {
                device.setStatus(DeviceStatus.ONLINE);
            }
            list.add(device);
        }
        ongoingMeetingResp.setValue(deviceResp);
        return ServiceResult.ok(list);
    }

    /**
     * 获取空间列表（无子空间）
     *
     * @param projectId 项目ID, null表示查询所有空间
     * @param parentId  父空间ID, null表示查询level = 1 的空间列表
     * @return 空间列表
     * @author Yang.Lee
     * @date 2021/6/28 10:49
     **/
    @Override
    @Transactional(readOnly = true)
    public List<SpaceTreeResp> getListWithoutChildren(@Nullable String projectId, @Nullable String parentId) {

        List<Space> spaceList = spaceDao.findAll((root, criteriaQuery, criteriaBuilder) -> {
            //集合 用于封装查询条件
            List<Predicate> list = new ArrayList<>();
            // 项目ID
            if (StringUtils.isNotEmpty(projectId)) {
                list.add(criteriaBuilder.equal(root.get("project").get("id"), projectId));
            }
            // 设备名称
            if (StringUtils.isEmpty(parentId)) {
                list.add(criteriaBuilder.equal(root.get("level").as(Integer.class), 1));
            } else {
                list.add(criteriaBuilder.equal(root.get("parent").get("id").as(String.class), parentId));
            }

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, Sort.by(Sort.Direction.DESC, "createdTime"));

        return spaceList.stream().map(this::convertSpace2SpaceTreeResp).collect(Collectors.toList());
    }

    @Override
    public ServiceResult getDeviceListControl(String id) {
        // 查找该空间下所有可被控制的设备列表
        List<IotDevice> controlList = deviceDao.findBySpaceIdAndSpaceControl(id, 1);
        if (controlList.isEmpty()) {
            return ServiceResult.error("该空间下无可被控制的设备");
        }
        List<DeviceWechatResp> list = new ArrayList<>();
        for (IotDevice dev : controlList) {
            DeviceWechatResp device = new DeviceWechatResp();
            device.setStatus(dev.getHwStatus());
            device.setType(dev.getDeviceType());
            device.setTypeName(dev.getDeviceType().getName());
            // 如果设备有华为云id合名称则展示华为云数据
            if (dev.getHwDeviceId() == null) {
                device.setDeviceId(dev.getId());
                device.setDeviceName(dev.getDeviceName());
            } else {
                device.setDeviceId(dev.getHwDeviceId());
                device.setDeviceName(dev.getHwDeviceName());
            }
            // 如果设备没有状态统一返回在线
            if (dev.getHwStatus() == null) {
                device.setStatus(DeviceStatus.ONLINE);
            }
            list.add(device);
        }

        return ServiceResult.ok(list);
    }


    /**
     * 获取项目及空间树
     *
     * @return 结果
     * @author Yang.Lee
     * @date 2021/10/18 11:15
     **/
    @Override
    @Transactional
    public List<ProjectSpaceTreeResp> getProjectSpaceTree() {

        List<Project> projectList = projectDao.findAll();
        if (projectList.isEmpty()) {
            return new ArrayList<>();
        }

        return projectList.stream().map(obj -> {

            ProjectSpaceTreeResp resp = new ProjectSpaceTreeResp();
            resp.setId(obj.getId());
            resp.setName(obj.getProjectName());
            resp.setNodeType("PROJECT");

            ServiceResult spaceListServiceResult = getListByProjectId(obj.getId());
            if (spaceListServiceResult.isSuccess()) {
                List<SpaceTreeResp> spaceTreeRespList = (List<SpaceTreeResp>) spaceListServiceResult.getObject();
                resp.setChildrenList(spaceTreeRespList.stream().map(space -> convert(space)).collect(Collectors.toList()));
            }
            return resp;
        }).collect(Collectors.toList());
    }

    private List<Space> treeMenuList(List<Space> menuList, String pid, List<Space> childMenu) {
        for (Space mu : menuList) {
            // 遍历出父id等于参数的id，add进子节点集合
            if (mu.getParent() != null && pid.equals(mu.getParent().getId())) {
                // 递归遍历下一级
                childMenu.add(mu);
                treeMenuList(menuList, mu.getId(), childMenu);
            }
        }
        return childMenu;
    }
/**
 * 查询空间下的详情（包含设备，人员）
 *
 * @param spaceId 空间ID
 * @return 空间详情
 * @author Yang.Lee
 * @date 2021/11/15 15:03
 **/
@Override
@Transactional
public List<SpaceAllDetailShowRoomResp> getAllDetailForShowRoom(String spaceId) {

    Optional<Space> spaceOptional = spaceDao.findById(spaceId);
    if (spaceOptional.isEmpty()) {
        return new ArrayList<SpaceAllDetailShowRoomResp>();
    }
    List<Space> allList = new ArrayList<>();
    List<Space> spaces = spaceDao.findAll();
    spaces = spaces.stream().filter(obj -> obj.getIsShow() == 1 && obj.getLevel() > 0).collect(Collectors.toList());
    treeMenuList(new ArrayList<>(spaces), spaceId, allList);
    Set<String> spaceIdSet = allList.stream().map(Space::getId).collect(Collectors.toSet());
    List<SpaceAllDetailShowRoomResp> resps = new ArrayList<>();
    if (!CollectionUtils.isEmpty(spaceIdSet)) {
        for (Space sonSpace : allList) {
            List<IotDevice> deviceLists = deviceDao.findAllBySpaceId(sonSpace.getId());
            SpaceAllDetailShowRoomResp result = new SpaceAllDetailShowRoomResp();
            List<IotDeviceResp> deviceListResp = new ArrayList<>();
            if (!CollectionUtils.isEmpty(deviceLists)) {
                for (IotDevice iotDevice : deviceLists) {
                    deviceListResp.add(IotDeviceResp.convert(iotDevice));
                }
            }
            result.setDetail(ShowRoomSpaceDetailResp.convert(sonSpace));
            result.setDeviceList(deviceListResp);
            resps.add(result);
        }
    }
    return resps;
}

    /**
     * 查询空间下的详情（包含设备，人员）
     *
     * @param spaceId 空间ID
     * @return 空间详情
     * @author Yang.Lee
     * @date 2021/11/15 15:03
     **/
    @Override
    @Transactional
    public SpaceAllDetailResp getAllDetail(String spaceId) {

        Optional<Space> spaceOptional = spaceDao.findById(spaceId);
        if (spaceOptional.isEmpty()) {
            return SpaceAllDetailResp.builder().build();
        }
        SpaceAllDetailResp result = new SpaceAllDetailResp();

        // 获取选择的空间及其所有子空间
        List<Space> allList = new ArrayList<>();
        List<Space> spaces = spaceDao.findAll();
        spaces = spaces.stream().filter(obj -> obj.getIsShow() == 1 && obj.getLevel() > 0).collect(Collectors.toList());
        treeMenuList(new ArrayList<>(spaces), spaceId, allList);
        allList.add(spaceOptional.get());

        Set<String> spaceIdSet = allList.stream().map(Space::getId).collect(Collectors.toSet());

        // 查询设备信息
        List<IotDevice> deviceList = deviceDao.findBySpaceIdIn(spaceIdSet);
        List<SpaceDeviceControl> spaceDeviceControlList = spaceDeviceControlDao.findByDeviceSpaceIdIn(spaceIdSet);

        // 计算设备数据
        long allDevice = deviceList.size();
        long onlineDevice = deviceList.stream().filter(obj -> DeviceStatus.ONLINE.equals(obj.getHwStatus())).count();
        long offlineDevice = deviceList.stream().filter(obj -> DeviceStatus.OFFLINE.equals(obj.getHwStatus())).count();

        // 计算在线/离线率， 保留两位小数，四舍五入
        // 离线率
        double offlineRate = allDevice == 0 ? 0 : new BigDecimal(offlineDevice).divide(new BigDecimal(allDevice), 2, RoundingMode.HALF_UP).doubleValue();
        // 在线率
        double onlineRate = allDevice == 0 ? 0 : new BigDecimal(onlineDevice).divide(new BigDecimal(allDevice), 2, RoundingMode.HALF_UP).doubleValue();

        // 计算人员数据
        List<StaffResp> staffRespList = spaceDeviceControlList.stream()
                .map(SpaceDeviceControl::getUserEmpNo)
                .map(obj -> StaffResp.convert(sysUserService.getUserByEmpNo(obj)))
                .collect(Collectors.collectingAndThen(
                        Collectors.toCollection(() ->
                                new TreeSet<>(Comparator.comparing(StaffResp::getHrId))), ArrayList::new));

        // 获取空间管理员
        List<SpaceAdmin> spaceAdminList = spaceAdminDao.findBySpaceId(spaceId);

        List<String> staffRespHrIdList = staffRespList.stream().map(StaffResp::getHrId).collect(Collectors.toList());
        staffRespList.addAll(0,
                spaceAdminList.stream()
                        .map(SpaceAdmin::getAdminEmpNo)
                        .filter(obj -> !staffRespHrIdList.contains(obj))
                        .map(obj -> StaffResp.convert(sysUserService.getUserByEmpNo(obj)))
                        .collect(Collectors.toList()));

        result.setTotalDevicesNumber(allDevice);
        result.setOfflineRate(offlineRate);
        result.setOnlineDevicesNumber(onlineDevice);
        result.setOfflineDevicesNumber(offlineDevice);
        result.setOnlineRate(onlineRate);
        result.setStaffNumber(staffRespList.size());
        result.setStaffList(staffRespList);
        result.setDeviceList(deviceList.stream().map(obj -> IotDeviceResp.convert(obj)).collect(Collectors.toList()));

        result.setDetail(SpaceDetailResp.convert(spaceOptional.get()));

        return result;
    }

    /**
     * 从空间中移除设备
     *
     * @param spaceId      空间id
     * @param deviceIdList 设备列表
     * @return 操作结果
     * @author Yang.Lee
     * @date 2021/11/15 17:12
     **/
    @Override
    @Transactional
    public ServiceResult removeDevices(String spaceId, List<String> deviceIdList) {

        Optional<Space> spaceOptional = spaceDao.findById(spaceId);
        if (spaceOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_SPACE, spaceId), ErrConst.E01);
        }

        List<IotDevice> iotDeviceList = deviceDao.findAllBySpaceId(spaceId);
        List<IotDevice> removeList = iotDeviceList.stream().filter(obj -> deviceIdList.contains(obj.getId())).map(obj -> {
            obj.setSpace(null);
            return obj;
        }).collect(Collectors.toList());

        deviceDao.saveAll(removeList);
        return ServiceResult.ok();
    }

    /**
     * 向空间中添加设备
     *
     * @param spaceId      空间ID
     * @param deviceIdList 设备id列表
     * @return 操作结果
     * @author Yang.Lee
     * @date 2021/11/15 18:42
     **/
    @Override
    @Transactional
    public ServiceResult addDevices(String spaceId, List<String> deviceIdList) {

        Optional<Space> spaceOptional = spaceDao.findById(spaceId);
        if (spaceOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_SPACE, spaceId), ErrConst.E01);
        }

        Space space = spaceOptional.get();

        for (String deviceId : deviceIdList) {
            Optional<IotDevice> deviceOptional = deviceDao.findById(deviceId);
            if (deviceOptional.isEmpty()) {
                return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_DEVICE, deviceId), ErrConst.E01);
            }

            IotDevice device = deviceOptional.get();

            // 设备所属空间变化后，删除旧的设备控制权限
            if (!spaceId.equals(device.getId())) {
                iotDeviceService.removeDeviceAllControlUsers(device.getId());
            }

            device.setSpace(space);
            device.setProject(space.getProject());
            deviceDao.save(device);
        }

        return ServiceResult.ok();
    }

    @Override
    public List<SpaceAdminResp> findSpaceAdminList(String spaceId) {

        // 根据空间ID获取管理员列表
        List<SpaceAdmin> spaceAdminList = spaceAdminDao.findBySpaceId(spaceId);
        List<SpaceAdminResp> adminResps = new ArrayList<>();
        spaceAdminList.forEach(it -> {
            SpaceAdminResp spaceAdminResp = new SpaceAdminResp();
            if (StringUtils.isNotBlank(it.getAdminName())) {
                spaceAdminResp.setName(it.getAdminName());
            } else {
                spaceAdminResp.setName("");
            }

            if (StringUtils.isNotBlank(it.getAdminEmpNo())) {
                spaceAdminResp.setEmpNo(it.getAdminEmpNo());
            } else {
                spaceAdminResp.setEmpNo("");
            }

            if (StringUtils.isNotBlank(it.getAdminDept())) {
                spaceAdminResp.setDept(it.getAdminDept());
            } else {
                spaceAdminResp.setDept("");
            }

            adminResps.add(spaceAdminResp);
        });
        return adminResps;
    }

    @Override
    public PageBean<IotDeviceResp> getDevicePageBySpace(String id, int page, int size) {

        //查询该空间下是否存在子空间
        List<Space> spaceList = spaceDao.findAllByIdOrParentId(id,id);

        List<String> spaceIds = new ArrayList<>();
        //所有空间id
        spaceList.forEach(it -> spaceIds.add(it.getId()));

        List<IotDevice> deviceList = deviceDao.findAllBySpaceIdIn(spaceIds);
        List<IotDeviceResp> iotDeviceResps = new ArrayList<>();
        deviceList.forEach(it -> {
            IotDeviceResp deviceResp = new IotDeviceResp();
            deviceResp.setId(it.getId());
            deviceResp.setDeviceStatus(it.getHwStatus());
            deviceResp.setSn(it.getSn());
            deviceResp.setDeviceName(it.getDeviceName());
            deviceResp.setSpaceControl(2);
            if (null != it.getSpaceControl()) {
                deviceResp.setSpaceControl(it.getSpaceControl());
            }
            iotDeviceResps.add(deviceResp);
        });
        return PageBean.createPageBeanByAllData(iotDeviceResps, page, size);
    }

    /**
     * 添加空间用途
     *
     * @param req 请求参数
     * @return 添加结果
     * @author Yang.Lee
     * @date 2021/11/17 20:08
     **/
    @Override
    @Transactional
    public ServiceResult addSpacePurpose(SpacePurposeEditReq req) {

        Optional<SpacePurpose> spacePurposeOptional = spacePurposeDao.findByName(req.getName());
        if (spacePurposeOptional.isPresent()) {
            return ServiceResult.error("该名称已存在", ErrConst.E01);
        }

        SpacePurpose spacePurpose = new SpacePurpose();
        spacePurpose.setName(req.getName());

        spacePurposeDao.save(spacePurpose);
        return ServiceResult.ok(IdResp.builder().id(spacePurpose.getId()).build());
    }

    /**
     * 获取空间用途列表
     *
     * @return 列表
     * @author Yang.Lee
     * @date 2021/11/17 20:29
     **/
    @Override
    @Transactional
    public List<BaseListResp> getSpacePurposeList() {

        List<SpacePurpose> spacePurposesList = spacePurposeDao.findAll(Sort.by(Sort.Direction.DESC, Sorts.CREATED_TIME));
        return spacePurposesList.stream().map(obj -> new BaseListResp(obj.getId(), obj.getName())).collect(Collectors.toList());
    }

    /**
     * 获取空间列表（分页）
     *
     * @param req 请求参数
     * @return 分页列表
     * @author Yang.Lee
     * @date 2021/11/18 9:59
     **/
    @Override
    @Transactional
    public PageBean<SpaceDetailResp> getListWithChildrenAllByProject(SpacePageReq req) {


        Pageable pageable = PageRequest.of(req.getPage(), req.getSize(), Sort.by(req.getOrders()));
        Page<Space> spacePage = spaceDao.findAll((root, query, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();
            list.add(criteriaBuilder.equal(root.get("project").get("id"), req.getProjectId()));
            list.add(criteriaBuilder.equal(root.get("isShow"), 1));
            if (StringUtils.isNotBlank(req.getPurposeId())) {
                list.add(criteriaBuilder.equal(root.get("spacePurpose").get("id"), req.getPurposeId()));
            }

            if (req.getSpaceType() != null) {
                list.add(criteriaBuilder.equal(root.get("spaceType"), req.getSpaceType()));
            }

            if (StringUtils.isNotBlank(req.getKeyword())) {
                String like = "%" + req.getKeyword() + "%";
                list.add(criteriaBuilder.or(criteriaBuilder.like(root.get("position"), like),
                        criteriaBuilder.like(root.get("name"), like)));
            }


            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, pageable);

        PageBean pageBean = new PageBean();
        pageBean.setPage(spacePage.getPageable().getPageNumber());
        pageBean.setSize(spacePage.getPageable().getPageSize());
        pageBean.setTotalPages(spacePage.getTotalPages());
        pageBean.setTotalElements(spacePage.getTotalElements());
        pageBean.setList(spacePage.getContent().stream().map(SpaceDetailResp::convert).collect(Collectors.toList()));

        return pageBean;
    }

    /**
     * 获取空间下所有空间列表（分页）
     *
     * @param req 请求参数
     * @return 分页列表
     * @author Yang.Lee
     * @date 2021/11/18 9:59
     **/
    @Override
    @Transactional
    public PageBean<SpaceDetailResp> getListWithChildrenAllBySpace(SpacePageReq req) {

        Optional<Space> spaceOp = spaceDao.findById(req.getSpaceId());
        if (!spaceOp.isPresent()) {
            return new PageBean<>();
        }

        Space space = spaceOp.get();

        // 查询该节点下所有子节点,非树形
        List<Space> resultSpace = getChildren(space.getId(), req);
        // 将数据排序
        resultSpace.sort(Comparator.comparing(Space::getCreatedTime).reversed());

        PageBean<Space> spacePageBean = PageBean.createPageBeanByAllData(resultSpace, req.getPage(), req.getSize());

        PageBean<SpaceDetailResp> result = new PageBean<>();
        result.setPage(spacePageBean.getPage());
        result.setSize(spacePageBean.getSize());
        result.setTotalElements(spacePageBean.getTotalElements());
        result.setTotalPages(spacePageBean.getTotalPages());
        result.setList(spacePageBean.getList().stream().map(SpaceDetailResp::convert).collect(Collectors.toList()));

        return result;
    }

    /**
     * 获取BIM中的设备-空间数据
     *
     * @param parentSpaceId 父空间ID
     * @param keyword       查询关键字
     * @return 结果
     * @author Yang.Lee
     * @date 2021/11/18 22:17
     **/
    @Override
    @Transactional
    public List<SpaceDeviceBIMResp> getBIMDeviceList(String parentSpaceId, String keyword) {

        List<IotDevice> iotDeviceList = deviceDao.findAll((root, query, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();

            list.add(criteriaBuilder.equal(root.get("space").get("parent").get("id"), parentSpaceId));
            if (StringUtils.isNotBlank(keyword)) {
                String like = "%" + keyword + "%";
                list.add(criteriaBuilder.or(criteriaBuilder.like(root.get("deviceName"), like),
                        criteriaBuilder.like(root.get("sn"), like)));
            }

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        });


        return iotDeviceList.stream().map(obj -> {
            SpaceDeviceBIMResp resp = new SpaceDeviceBIMResp();
            resp.setIotDeviceResp(IotDeviceResp.convert(obj));
            resp.setSpaceDetailResp(SpaceDetailResp.convert(obj.getSpace()));
            return resp;

        }).collect(Collectors.toList());
    }

    /**
     * 获取空间有权限操作设备的用户列表
     *
     * @param parentSpaceId 父空间ID
     * @param keyword       查询关键字
     * @return 结果
     * @author Yang.Lee
     * @date 2021/11/18 22:33
     **/
    @Override
    @Transactional
    public List<SpaceDeviceBIMResp> getBIMUserList(String parentSpaceId, String keyword) {

        List<SpaceDeviceControl> spaceDeviceControlList = spaceDeviceControlDao.findAll((root, query, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();

            list.add(criteriaBuilder.equal(root.get("device").get("space").get("parent").get("id"), parentSpaceId));
            if (StringUtils.isNotBlank(keyword)) {
                String like = "%" + keyword + "%";
                list.add(criteriaBuilder.or(criteriaBuilder.like(root.get("userEmpNo"), like),
                        criteriaBuilder.like(root.get("userName"), like)));
            }

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        });


        return spaceDeviceControlList.stream().map(SpaceDeviceControl::getDevice).map(obj -> {
            SpaceDeviceBIMResp resp = new SpaceDeviceBIMResp();
            resp.setIotDeviceResp(IotDeviceResp.convert(obj));
            resp.setSpaceDetailResp(SpaceDetailResp.convert(obj.getSpace()));
            return resp;
        }).collect(Collectors.toList());
    }

    /**
     * 查询用户空间内设备管理信息
     *
     * @param spaceId   空间ID
     * @param userEmpNo 用户工号
     * @return 详情
     * @author Yang.Lee
     * @date 2021/11/19 15:46
     **/
    @Override
    @Transactional
    public SpaceAdminResp getUserSpaceAdmin(String spaceId, String userEmpNo) {

        SysUser sysUser = sysUserService.getUserByEmpNo(userEmpNo);
        if (sysUser == null) {
            return new SpaceAdminResp();
        }

        List<IotDeviceResp> deviceRespList;
        Optional<SpaceAdmin> spaceAdminOptional = spaceAdminDao.findBySpaceIdAndAdminEmpNo(spaceId, userEmpNo);

        if (spaceAdminOptional.isPresent()) {
            // 空间管理员显示所有空间下的设备
//            spaceAdminList = spaceDeviceControlDao.findAll((root, query, criteriaBuilder) ->
//                    criteriaBuilder.and(criteriaBuilder.equal(root.get("device").get("space").get("id"), spaceId)));
            List<IotDevice> deviceList = deviceDao.findAllBySpaceId(spaceId);
            deviceRespList = deviceList.stream().map(IotDeviceResp::convert).collect(Collectors.toList());
        } else {
            List<SpaceDeviceControl> spaceAdminList = spaceDeviceControlDao.findAll((root, query, criteriaBuilder) -> {

                List<Predicate> list = new ArrayList<>();

                list.add(criteriaBuilder.equal(root.get("userEmpNo"), userEmpNo));

                list.add(criteriaBuilder.equal(root.get("device").get("space").get("id"), spaceId));

                return criteriaBuilder.and(list.toArray(new Predicate[0]));
            });

            deviceRespList = spaceAdminList.stream()
                    .map(SpaceDeviceControl::getDevice)
                    .map(IotDeviceResp::convert)
                    .collect(Collectors.toList());
        }

        SpaceAdminResp spaceAdminResp = new SpaceAdminResp();
        spaceAdminResp.setName(sysUser.getNameZh());
        spaceAdminResp.setDept(sysUser.getFullDepartmentName());
        spaceAdminResp.setEmpNo(sysUser.getEmpNo());
        spaceAdminResp.setWorkPhoto(Optional.ofNullable(sysUser.getWorkPhoto()).orElse(""));
        spaceAdminResp.setDeviceList(deviceRespList);

        return spaceAdminResp;
    }

    /**
     * 判断用户是否是空间管理员
     *
     * @param hrId    用户工号
     * @param spaceId 空间id
     * @return true: 是；false：否
     * @author Yang.Lee
     * @date 2021/11/25 9:49
     **/
    @Override
    public boolean isUserSpaceAdmin(String hrId, String spaceId) {

        List<SpaceAdmin> spaceAdminList = spaceAdminDao.findBySpaceId(spaceId);

        List<String> hrIdList = spaceAdminList.stream()
                .map(SpaceAdmin::getAdminEmpNo)
                .filter(obj -> obj.equals(hrId))
                .collect(Collectors.toList());

        return !hrIdList.isEmpty();
    }

    /**
     * 向UC的智能办公室权限发送请求,若所属空间不是智能办公室空间，则返回成功
     *
     * @param spaceId 空间ID
     * @param req     编辑参数
     * @return 操作结果
     * @author Yang.Lee
     * @date 2021/11/29 11:01
     **/
    @Override
    public ServiceResult editUCIntelligentOfficeRole(String spaceId, PassportRoleEditReq req) {

        // 给用户添加UC的权限
        // 判断添加权限的空间是否是智能会议室，如果是，则开通uc权限
//        Optional<IntelligentOffice> intelligentOfficeOptional = intelligentOfficeDao.findBySpaceId(spaceId);
//        if (intelligentOfficeOptional.isPresent()) {
//            // 向uc添加入库权限
//            PassportRoleEditReq passportRoleEditReq = new PassportRoleEditReq();
//            passportRoleEditReq.setAddUserHrIdList(req.getAddUserHrIdList());
//            passportRoleEditReq.setRmUserHrIdList(req.getRmUserHrIdList());
//            JsonResult<String> stringJsonResult = userCenterRoleApi.addMenuForIntelligentOffice(TokenUtils.getToken(), passportRoleEditReq);
//            if (stringJsonResult.getStatus() == 0) {
//                return ServiceResult.error("向uc添加权限失败:" + stringJsonResult.getMsg());
//            }
//        } else {
//            log.info("空间{}不是智慧办公室空间，未发送权限请求到uc", spaceId);
//        }

        return ServiceResult.ok();
    }

    private List<Space> getChildren(String parentId, SpacePageReq req) {
        List<Space> childrenList = new ArrayList<>();
        List<Space> children = spaceDao.findAll((root, query, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();

            list.add(criteriaBuilder.equal(root.get("parent").get("id"), parentId));
            list.add(criteriaBuilder.equal(root.get("isShow"), 1));
            if (StringUtils.isNotBlank(req.getPurposeId())) {
                list.add(criteriaBuilder.equal(root.get("spacePurpose").get("id"), req.getPurposeId()));
            }

            if (req.getSpaceType() != null) {
                list.add(criteriaBuilder.equal(root.get("spaceType"), req.getSpaceType()));
            }

            if (StringUtils.isNotBlank(req.getKeyword())) {
                String like = "%" + req.getKeyword() + "%";
                list.add(criteriaBuilder.or(criteriaBuilder.like(root.get("position"), like),
                        criteriaBuilder.like(root.get("name"), like)));
            }

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        });

        childrenList.addAll(children);

        if (!CollectionUtils.isEmpty(children)) {
            children.forEach(obj -> childrenList.addAll(getChildren(obj.getId(), req)));

        }
        return childrenList;
    }

//
//    public List<SpaceTreeResp> treeToList(List<SpaceTreeResp> treeList){
//        List<SpaceTreeResp> list = new ArrayList<>();
//        List<SpaceTreeResp> list1 = new ArrayList<>();
//        String temp;
//        for(SpaceTreeResp tree : treeList){
//            if(!CollectionUtils.isEmpty(tree.getChildrenList())){
//                list = treeToList(tree.getChildrenList());
//                temp = JSON.toJSONString(tree).substring(JSON.toJSONString(tree).lastIndexOf("],") + 2);
//                temp = "{".concat(temp);
//
//                SpaceTreeResp node = JSON.parseObject(temp, SpaceTreeResp.class);
//                list.add(node);
//            } else{
//                list.add(tree);
//            }
//
//            for(SpaceTreeResp tr: list){
//                int count = 0;
//                for(SpaceTreeResp sub : list1){
//                    if(sub.getId() != tr.getId()){
//                        count++;
//                    }
//                    if(count == list1.size()){
//                        list1.add(tr);
//                    }
//                }
//            }
//            return list1;
//        }
//    }

    public ProjectSpaceTreeResp convert(SpaceTreeResp source) {
        ProjectSpaceTreeResp target = new ProjectSpaceTreeResp();

        target.setId(source.getId());
        target.setName(source.getName());
        target.setNodeType("SPACE");

        if (!CollectionUtils.isEmpty(source.getChildrenList())) {
            target.setChildrenList(source.getChildrenList().stream().map(obj -> convert(obj)).collect(Collectors.toList()));
        }

        return target;
    }

    @Override
    public Space getSpaceFromCacheFirst(String spaceId) {
        Space space;
        String spaceKey = redisUtil.createSpaceAdminRealKey(IConst.SPACE_MODULE, spaceId, RedisUtil.Separator.COLON);
        if (redisUtil.hasKey(spaceKey)){
            Object o = redisUtil.get(spaceKey);
            space = JSON.toJavaObject((JSON) o, Space.class);
        }else {
            space = spaceDao.findById(spaceId).orElse(null);
            if(Objects.nonNull(space)){
                spaceCacheService.setSpaceInRedis(space);
            }
        }
        return space;
    }

    @Override
    public List<String> getChildIdListByParentIdFromCache(String parentId){
        Stopwatch stopwatch = Stopwatch.createStarted();
        Set<String> result = new HashSet<>();
        addChildToList(parentId, result);
        log.info("从缓存中获取空间的子节点耗时：{} ms", stopwatch.elapsed(TimeUnit.MILLISECONDS));
        return new ArrayList<>(result);
    }

    private void addChildToList(String parentId, Set<String> list) {
        String key = redisUtil.createSpaceAdminRealKey(IConst.SPACE_PARENT_MODULE, parentId, RedisUtil.Separator.COLON);
        if(!redisUtil.hasKey(key)){
            return;
        }
        Set<Object> children =  redisUtil.sGet(key);
        for (Object child : children) {
            String childS = (String) child;
            list.add(childS);
            addChildToList(childS, list);
        }
    }
}
