package cn.com.cowain.ibms.rest.meal;

import cn.com.cowain.ibms.dao.meal.KshtTimePlanDao;
import cn.com.cowain.ibms.dao.meal.MealGroupDao;
import cn.com.cowain.ibms.dao.meal.MealPersonDao;
import cn.com.cowain.ibms.entity.meal.KsHtTimePlan;
import cn.com.cowain.ibms.entity.meal.MealGroup;
import cn.com.cowain.ibms.entity.meal.MealPerson;
import cn.com.cowain.ibms.enumeration.ability.MethodType;
import cn.com.cowain.ibms.enumeration.iot.AccessControlSpotPlace;
import cn.com.cowain.ibms.enumeration.meal.MealStatus;
import cn.com.cowain.ibms.feign.ks_conn.KSConnApi;
import cn.com.cowain.ibms.job.MealUnbindJob;
import cn.com.cowain.ibms.mq.bean.OpenAbilityMessage;
import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.resp.meal.DeleteHtAcrossPermissionConfigReq;
import cn.com.cowain.ibms.service.ability.AbilityService;
import cn.com.cowain.ibms.service.meal.MealService;
import cn.com.cowain.ibms.utils.DateUtils;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author wei.cheng
 * @date 2022/06/02 13:21
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/meal")
@Api(tags = IConst.MODULE_MEAL)
public class MealController {
    @Autowired
    private MealUnbindJob mealUnbindJob;
    @Autowired
    private AbilityService abilityService;
    @Autowired
    private MealService mealService;
    @Resource
    private KSConnApi ksConnApi;
    @Resource
    private MealPersonDao mealPersonDao;
    @Resource
    private MealGroupDao mealGroupDao;
    @Resource
    private KshtTimePlanDao kshtTimePlanDao;


    @PostMapping("/unbindJobStart")
    @ApiOperation(value = "主动执行解绑任务", tags = IConst.MODULE_MEAL)
    public JsonResult unbindJobStart() {
        try {
            mealUnbindJob.run();
            return JsonResult.ok("OK", "解绑任务主动执行成功");
        } catch (Exception e) {
            return JsonResult.error("解绑任务主动执行失败", e.getMessage(), ErrConst.E20);
        }
    }

    /**
     * 模拟报餐，用于测试
     *
     * @param object
     * @return
     */
    @PostMapping("/post")
    @ApiOperation(value = "主动下发报餐", tags = IConst.MODULE_MEAL)
    public JsonResult mealPost(@RequestBody Object object) {
        OpenAbilityMessage abilityMessage = JSON.parseObject(JSON.toJSONString(object), OpenAbilityMessage.class);
        abilityService.process(abilityMessage);
        return JsonResult.ok("OK", "");
    }

    /**
     * 报餐数据刷新
     *
     * @param minutes 重新下发多少分钟内的数据
     * @return
     */
    @PostMapping("/refresh")
    @ApiOperation(value = "报餐数据刷新", tags = IConst.MODULE_MEAL)
    public JsonResult refresh(@RequestParam @ApiParam(value = "重新下发多少分钟内的数据") Integer minutes) {
        // 查询需要下发的有效人员数据
        List<MealPerson> mealPeopleList = mealPersonDao.findAllByCreatedTimeAfter(LocalDateTime.now().minusMinutes(minutes));
        log.info("需要重新下发的人员数:{}", mealPeopleList.size());
        if (CollectionUtils.isEmpty(mealPeopleList)) {
            return JsonResult.ok("OK", "不存在需要下发的人员");
        }

        // 查询所有报餐人员组
        List<MealGroup> mealGroups = mealGroupDao.findAll();

        // 查询所有旷世时间计划
        List<KsHtTimePlan> ksHtTimePlans = kshtTimePlanDao.findAll();

        // 获取所有place
        List<String> places = getAllPlace(mealPeopleList, ksHtTimePlans);

        // 先清空旷世所有有关报餐的权限数据，以防由于脏数据导致下发失败
        for (String place : places) {
            DeleteHtAcrossPermissionConfigReq req = new DeleteHtAcrossPermissionConfigReq();
            req.setName(place + '_');
            JsonResult result = ksConnApi.deletePermission(req);
            if (1 != result.getStatus()) {
                return JsonResult.error(String.format("删除包含%s的旷世权限失败", place), result.getData(), ErrConst.E20);
            }
        }

        // 删除所有旷世报餐人员组
        mealGroups.forEach(mealGroup -> mealGroup.setIsDelete(1));
        mealGroupDao.saveAll(mealGroups);

        // 删除所有旷世报餐时间计划
        ksHtTimePlans.forEach(ksHtTimePlan -> ksHtTimePlan.setIsDelete(1));
        kshtTimePlanDao.saveAll(ksHtTimePlans);

        // 获取需要配置的权限
        List<OpenAbilityMessage.Meal> mealList = getMeals(mealPeopleList);

        // 报餐权限配置
        mealService.permissionConfig(mealList);

        // 报餐人员数据下发
        abilityService.mealUserDownload(MethodType.UPDATE, mealPeopleList);

        // 报餐数据状态更新
        mealPeopleList.forEach(val -> {
            val.setStatus(MealStatus.SUCCESS);
            mealPersonDao.save(val);
        });

        return JsonResult.ok("OK", "已刷新报餐人员权限数据");
    }

    /**
     * 获取所有place
     *
     * @param mealPeopleList
     * @param ksHtTimePlans
     * @return
     */
    private List<String> getAllPlace(List<MealPerson> mealPeopleList, List<KsHtTimePlan> ksHtTimePlans) {
        List<String> places = new ArrayList<>();
        mealPeopleList.forEach(mealPerson -> {
            if (!places.contains(mealPerson.getPlace().name())) {
                places.add(mealPerson.getPlace().name());
            }
        });

        ksHtTimePlans.forEach(ksHtTimePlan -> {
            if (!places.contains(ksHtTimePlan.getPlace().name())) {
                places.add(ksHtTimePlan.getPlace().name());
            }
        });

        if (!places.contains("KS")) {
            places.add("KS");
        }
        if (!places.contains("NT")) {
            places.add("NT");
        }
        return places;
    }

    /**
     * 获取需要配置的权限
     *
     * @param mealPeopleList
     * @return
     */
    private List<OpenAbilityMessage.Meal> getMeals(List<MealPerson> mealPeopleList) {
        Map<AccessControlSpotPlace, Map<String, Set<String>>> mealListMap = new HashMap<>();
        mealPeopleList.forEach(mealPerson -> {
            Map<String, Set<String>> timeMap = mealListMap.get(mealPerson.getPlace());
            if (Objects.isNull(timeMap)) {
                timeMap = new HashMap<>();
            }
            String startTime = DateUtils.formatLocalDateTime(mealPerson.getStartTime(), DateUtils.PATTERN_TIME1);

            Set<String> endTimes = timeMap.get(startTime);
            if (Objects.isNull(endTimes)) {
                endTimes = new HashSet<>();
            }
            endTimes.add(DateUtils.formatLocalDateTime(mealPerson.getEndTime(), DateUtils.PATTERN_TIME1));
            timeMap.put(startTime, endTimes);
            mealListMap.put(mealPerson.getPlace(), timeMap);
        });

        List<OpenAbilityMessage.Meal> mealList = new ArrayList<>();
        mealListMap.forEach((place, timeMap) -> {
            timeMap.forEach((startTime, endTimeSet) -> {
                for (String endTime : endTimeSet) {
                    OpenAbilityMessage.Meal meal = new OpenAbilityMessage.Meal();
                    meal.setPlace(place);
                    meal.setStartTime(startTime);
                    meal.setEndTime(endTime);
                    mealList.add(meal);
                }
            });
        });
        return mealList;
    }
}
