package cn.com.cowain.ibms.rest.resp.device;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 硬件详情数据对象
 *
 * @author Yang.Lee
 * @date 2021/3/17 18:25
 */
@Data
@ApiModel("硬件详情返回数据")
public class HardwareDetailResp {

    @ApiModelProperty(value = "硬件SN",  example = "123456", position = 1)
    private String hardWareSn;

    @ApiModelProperty(value = "硬件名称，中文",  example = "设备A", position = 2)
    private String nameZh;

    @ApiModelProperty(value = "硬件名称，英文",  example = "ABC", position = 3)
    private String nameEn;

    @ApiModelProperty(value = "所属产品Id",  example = "123333s", position = 4)
    private String productId;

    @ApiModelProperty(value = "所属产品名称",  example = "产品A", position = 5)
    private String productName;

    @ApiModelProperty(value = "设备类型",  example = "GATEWAY", position = 6)
    private String deviceType;

    @ApiModelProperty(value = "设备类型名称",  example = "网关", position = 7)
    private String deviceTypeName;

    @ApiModelProperty(value = "品牌",  example = "海康", position = 8)
    private String brand;

    @ApiModelProperty(value = "型号",  example = "MM-001", position = 9)
    private String model;
}
