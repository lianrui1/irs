package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2022/2/9 19:58
 */
@Data
public class UserAccessCheckReq {

    @ApiModelProperty(value = "用户工号")
    private String userHrId;

    @ApiModelProperty(value = "设备SN")
    private String deviceSn;
}
