package cn.com.cowain.ibms.rest.req.distinguish;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/22 14:29
 */
@Data
public class HkFaceRecordReq {

    private String resourceCode;

    private String name;

    private String startDate;

    private String endDate;

    private int page;

    private int size;
}
