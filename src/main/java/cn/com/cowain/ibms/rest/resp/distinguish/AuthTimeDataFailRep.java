package cn.com.cowain.ibms.rest.resp.distinguish;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author wei.cheng
 * @date 2022/03/22 17:32
 */
@Data
@ApiModel("排班时间段异常信息")
public class AuthTimeDataFailRep {

    @ApiModelProperty(value = "人员工号", required = true)
    private String jobNo;

    @ApiModelProperty(value = "异常消息", required = true)
    private String msg;
}
