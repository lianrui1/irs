package cn.com.cowain.ibms.entity;

import cn.com.cowain.ibms.enumeration.ReservationRecordStatus;
import cn.com.cowain.ibms.enumeration.ReservationRecordType;
import cn.com.cowain.ibms.enumeration.ReservationType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 房间预定记录
 *
 * @author 胡荆陵
 * @since 2020-7-30
 */
@Data
@Entity
@Table(name = "ibms_reservation_record")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_reservation_record", comment = "会议室预定记录")
@Where(clause = "is_del=0")
public class ReservationRecord extends BaseEntity {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 会议主题
     */
    @Column(name = "r_topic", nullable = false, columnDefinition = "varchar(255)  COMMENT '会议主题'")
    private String topic;

    /**
     * 预定日期
     */
    @Column(name = "r_date", nullable = false, columnDefinition = "date  COMMENT '预定日期'")
    private LocalDate date;

    /**
     * 从时间
     */
    @Column(name = "r_from", nullable = false, columnDefinition = "datetime COMMENT '从时间'")
    private LocalDateTime from;

    /**
     * 到时间
     */
    @Column(name = "r_to", nullable = false, columnDefinition = "datetime COMMENT '到时间'")
    private LocalDateTime to;

    /**
     * 到时间
     */
    @Column(name = "r_original_to", columnDefinition = "datetime COMMENT '到时间(不会变更的时间)'")
    private LocalDateTime originalTo;

    /**
     * 备注说明
     */
    @Column(name = "r_remark", columnDefinition = "varchar(200) COMMENT '备注说明'")
    private String remark;

    /**
     * 需要的服务
     */
    @Column(name = "r_service", columnDefinition = "varchar(255) COMMENT '需要的服务'")
    private String service;

    /**
     * 发起人
     */
    @Column(name = "r_initiator", columnDefinition = "varchar(36) COMMENT '发起人'")
    private String initiator;

    /**
     * 发起人工号
     */
    @Column(name = "r_initiator_emp_no", nullable = false, columnDefinition = "varchar(36) COMMENT '发起人工号'")
    private String initiatorEmpNo;

    /**
     * 参与人，冗余字段
     */
    @Column(name = "r_user_ids", columnDefinition = "varchar(255) COMMENT '参与人，冗余字段'")
    private String participantIds;

    /**
     * 预定类型,预留
     */
    @Column(name = "r_type", columnDefinition = "varchar(50) COMMENT '预定类型,预留'")
    @Enumerated(EnumType.STRING)
    private ReservationType type;

    /**
     * 预定会议记录状态
     */
    @Column(name = "r_status", nullable = false, columnDefinition = "varchar(50) COMMENT '预定记录状态'")
    @Enumerated(EnumType.STRING)
    private ReservationRecordStatus status;

    @ManyToOne
    @JoinColumn(name = "room_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK'")
    private Room room;

    /**
     * 免打扰
     */
    @Column(name = "no_disturb", columnDefinition = "tinyint(1) COMMENT '免打扰 0:允许打扰;1:不要打扰'")
    private Boolean noDisturb = true;

    @Column(name = "record_type", nullable = false, columnDefinition = "varchar(50) COMMENT '是否是内部会议  VISITOR 访客'")
    private ReservationRecordType recordType;

    @Column(name = "welcome_words", columnDefinition = "varchar(200) COMMENT '欢迎语'")
    private String welcomeWords;

    /**
     * 会议大屏状态
     */
    @Column(name = "screen_status", columnDefinition = "tinyint(1) default 2 COMMENT '会议大屏状态 1:是； 2：否'")
    private Integer screenStatus;
}
