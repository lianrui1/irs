package cn.com.cowain.ibms.feign.bean.ehr;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

/**
 * @author wei.cheng
 * @date 2022/03/25 16:37
 */
@Data
@Builder
public class CheckLabourResp {

    @ApiModelProperty(value = "工号")
    private String jobNum;

    @ApiModelProperty(value = "是否是劳务")
    private Boolean labour;
}
