package cn.com.cowain.ibms.event.process;

import cn.com.cowain.ibms.entity.centralized.CentralizedControlCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * 刷新集控码的缓存数据事件
 *
 * @author wei.cheng
 * @date 2022/09/16 19:14
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RefreshCentralizedCodeCacheDataEvent {
    /**
     * 集控码
     */
    @NotNull
    private CentralizedControlCode centralizedControlCode;
}
