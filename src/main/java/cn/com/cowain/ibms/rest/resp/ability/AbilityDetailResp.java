package cn.com.cowain.ibms.rest.resp.ability;

import cn.com.cowain.ibms.entity.ability.OpenAbility;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * @author Yang.Lee
 * @date 2022/1/11 16:06
 */
@Data
@ApiModel
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AbilityDetailResp {

    @ApiModelProperty(value = "能力ID", example = "11", position = 1)
    private String id;

    @ApiModelProperty(value = "能力名称", example = "入职", position = 2)
    private String name;

    @ApiModelProperty(value = "能力功能", example = "xxxxxx", position = 3)
    private String describe;

    @ApiModelProperty(value = "业务应用", example = "EHR", position = 4)
    private String application;

    @ApiModelProperty(value = "门禁点数量", example = "123", position = 5)
    private Long spotCount;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "创建时间", example = "2022-01-01 01:01", position = 5)
    private LocalDateTime createTime;

    /**
     * 对象转换
     * @param source
     * @return
     * @author Yang.Lee
     * @date 2022/1/14 13:09
     **/
    public static AbilityDetailResp convert(OpenAbility source){

        return AbilityDetailResp.builder()
                .id(source.getId())
                .name(source.getAbility().getName())
                .describe(Optional.ofNullable(source.getDescribe()).orElse(""))
                .createTime(source.getCreatedTime())
                .build();
    }
}
