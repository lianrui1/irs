package cn.com.cowain.ibms.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
@ComponentScan("cn.com.cowain.sfp")
public class BaseConfig {

    public BaseConfig() {
        log.debug("BaseConfig empty constructor...");
    }

}
