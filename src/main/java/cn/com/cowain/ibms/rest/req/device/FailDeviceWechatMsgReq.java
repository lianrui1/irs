package cn.com.cowain.ibms.rest.req.device;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@ApiModel("失败设备消息推送请求对象")
public class FailDeviceWechatMsgReq {


    @NotEmpty
    @ApiModelProperty(value = "设备IDlist", required = true)
    private List<String> deviceIds;

    @NotEmpty
    @ApiModelProperty(value = "集控码ID", required = true)
    private String centralizedControlCodeId;

    @NotEmpty
    @ApiModelProperty(value = "用户ucId", example = "123abcABC", required = true, position = 1)
    private String uid;
}
