package cn.com.cowain.ibms.feign.iotc.bean;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 * @author: yanzy
 * @date: 2022/2/17 18:59
 */
@Data
public class BodySensorReq {

    @ApiModelProperty(value = "参数名", example = "peopleSensor1")
    private String key;

    @ApiModelProperty(value = "条件判断值", example = "true/false")
    private String value;

    @ApiModelProperty(value = "参数名", example = "peopleTime1")
    private String timeKey;

    @ApiModelProperty(value = "人体传感器分钟")
    private Integer timeValue;
}
