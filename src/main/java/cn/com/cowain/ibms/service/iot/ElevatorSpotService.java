package cn.com.cowain.ibms.service.iot;

import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.iot.ElevatorSpotAuthSaveReq;
import cn.com.cowain.ibms.rest.req.iot.ElevatorSpotBindDeviceReq;
import cn.com.cowain.ibms.rest.req.iot.ElevatorSpotPageReq;
import cn.com.cowain.ibms.rest.req.iot.ElevatorSpotUserPageReq;
import cn.com.cowain.ibms.rest.resp.iot.ElevatorSpotUserPageResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;

public interface ElevatorSpotService {


    // 分配权限
    ServiceResult create(ElevatorSpotAuthSaveReq req);

    // 梯控按人员列表分页
    PageBean<ElevatorSpotUserPageResp> getUserPage(ElevatorSpotUserPageReq req);

    // 删除人员所有梯控权限
    ServiceResult deleteAuth(String empNo);

    // 删除人员指定梯控权限
    ServiceResult deleteFloorUserAuth(String empNo, String spaceId);

    // 修改梯控权限
    ServiceResult update(ElevatorSpotAuthSaveReq req);

    /**
     * 梯控按楼层列表分页
     *
     * @param req
     * @author wei.cheng
     * @date 2022/3/30 2:33 下午
     **/
    ServiceResult getFloorPage(ElevatorSpotPageReq req);

    //绑定设备
    ServiceResult bindDevice(ElevatorSpotBindDeviceReq req);

    // 解绑设备
    ServiceResult unbindDevice(ElevatorSpotBindDeviceReq req);
}
