package cn.com.cowain.ibms.config;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/21/20
 */
@SpringBootTest
class CorsConfigTest {

    @Autowired
    private CorsConfig corsConfig;

    @Test
    public void test1(){
        Assertions.assertNotNull(corsConfig);
    }

}
