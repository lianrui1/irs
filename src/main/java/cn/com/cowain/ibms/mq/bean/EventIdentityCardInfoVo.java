package cn.com.cowain.ibms.mq.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author tql
 * @Description EventIdentityCardInfoVo
 * @Date 21-5-24 下午5:37
 * @Version 1.0
 */
@Data
public class EventIdentityCardInfoVo {

    /**
     * 住址
     */
    @JsonProperty("Address")
    @ApiModelProperty(value = "住址")
    private String address;

    /**
     * 出生日期
     */
    @JsonProperty("Birth")
    @ApiModelProperty(value = "出生日期")
    private String birth;

    /**
     * 有效日期结束时间
     */
    @JsonProperty("EndDate")
    @ApiModelProperty(value = "有效日期结束时间")
    private String endDate;

    /**
     * 身份证id
     */
    @JsonProperty("IdNum")
    @ApiModelProperty(value = "身份证id")
    private String idNum;

    /**
     * 签发机关
     */
    @JsonProperty("IssuingAuthority")
    @ApiModelProperty(value = "签发机关")
    private String issuingAuthority;

    /**
     * String
     */
    @JsonProperty("Name")
    @ApiModelProperty(value = "String")
    private String name;

    /**
     * 住址
     */
    @JsonProperty("Nation")
    @ApiModelProperty(value = "住址")
    private Integer nation;

    /**
     * 性别
     */
    @JsonProperty("Sex")
    @ApiModelProperty(value = "性别")
    private Integer sex;

    /**
     * 有效日期开始时间
     */
    @JsonProperty("StartDate")
    @ApiModelProperty(value = "有效日期开始时间")
    private String startDate;

    /**
     * 是否长期有效
     */
    @JsonProperty("TermOfValidity")
    @ApiModelProperty(value = "是否长期有效")
    private Integer termOfValidity;

}
