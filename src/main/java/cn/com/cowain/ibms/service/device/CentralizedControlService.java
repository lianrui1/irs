package cn.com.cowain.ibms.service.device;

import cn.com.cowain.ibms.entity.centralized.CentralizedControlCode;
import cn.com.cowain.ibms.event.dto.CentralizedCodeCacheData;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.SingleControlCommandIssuedReq;
import cn.com.cowain.ibms.rest.req.device.FailDeviceWechatMsgReq;
import cn.com.cowain.ibms.rest.req.device.centralized.*;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.device.FailDeviceRecordResp;
import cn.com.cowain.ibms.rest.resp.device.centralized.*;
import cn.com.cowain.sfp.comm.JsonResult;
import org.springframework.http.ResponseEntity;

/**
 * @author wei.cheng
 * @date 2022/09/16 13:53
 */
public interface CentralizedControlService {
    /**
     * 创建集控码
     *
     * @param req
     * @return
     */
    ResponseEntity<JsonResult<IdResp>> createCentralizedControlCode(CentralizedControlCodeCreateReq req);

    /**
     * 修改集控码
     *
     * @param id
     * @param req
     * @return
     */
    ResponseEntity<JsonResult<IdResp>> updateCentralizedControlCode(String id, CentralizedControlCodeUpdateReq req);

    /**
     * 查询集控码列表
     *
     * @param req
     * @return
     */
    ResponseEntity<JsonResult<PageBean<CentralizedControlCodeResp>>> searchCentralizedControlCode(CentralizedControlCodeSearchReq req);

    /**
     * 刷新集控码的缓存数据
     *
     * @param code
     * @return
     */
    CentralizedCodeCacheData refreshCentralizedCodeCacheData(CentralizedControlCode code);

    ResponseEntity<JsonResult<CentralizedControlCommandIssuedResp>> centralizedCommandIssued(CentralizedControlCommandIssuedReq req);

    ResponseEntity<JsonResult<CentralizedControlCommandIssuedResp>> processPolling(String traceId);

    ResponseEntity<JsonResult<CentralizedControlCommandIssuedResp>> singleCommandIssued(SingleControlCommandIssuedReq req);

    ResponseEntity<JsonResult<PageBean<CentralizedControlCodeSelectDeviceResp>>> getCentralizedControlCodeSelectDevice(String spaceId, CentralizedControlCodeSelectDeviceReq req);

    JsonResult<String> wechatMsg(FailDeviceWechatMsgReq req);

    ResponseEntity<JsonResult<String>> saveAdmin(CentralizedControlAdminReq req);

    ResponseEntity<JsonResult<CentralizedControlAdminReq>> getAdmin();

    ResponseEntity<JsonResult<String>> delAdmin(String id);

    ResponseEntity<JsonResult<PageBean<RecordResp>>> recordPage(RecordReq req);

    ResponseEntity<JsonResult<PageBean<FailDeviceRecordResp>>> failDeviceRecord(String traceID, int page, int size);

    ResponseEntity<JsonResult<PageBean<CentralizedControlCodeSelectDeviceResp>>> getCentralizedControlCodeControlDevice(CentralizedControlCodeControlDeviceReq req);

    ResponseEntity<JsonResult<CentralizedControlCodeDetailResp>> getCentralizedControlCodeDetail(String id);

    /**
     * h5端查询集控码详情
     *
     * @param id
     * @param uid
     * @return
     */
    ResponseEntity<JsonResult<CentralizedControlCodeDetailH5Resp>> getCentralizedControlCodeDetailH5(String id, String uid);

    /**
     * h5端查询集控码下设备列表
     *
     * @param id
     * @param req
     * @return
     */
    ResponseEntity<JsonResult<CentralizedCodeControlDeviceListH5Resp>> getCentralizedControlCodeDeviceListH5(String id, CentralizedControlCodeDeviceListH5Req req);

    /**
     * web端删除集控码
     *
     * @author: yanzy
     * @data: 2022/9/21 14:09:39
     */
    ResponseEntity<JsonResult<String>> deleteCentralizedControlCode(String id);
}
