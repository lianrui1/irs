package cn.com.cowain.ibms.rest.req;

import cn.com.cowain.ibms.enumeration.ReservationRecordItemStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Description
 *
 * @author 张宇鑫
 * @version 1.0
 * @date 2020/8/10 13:07
 */
@Data
public class InviteeStatusReq implements Serializable{
    
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 用户中心的全局唯一id
     */
    @ApiModelProperty(value = "用户全局唯一id", required = true, position = 0)
    private String sysId;

    @NotNull(message = "用户工号不能为空")
    @ApiModelProperty(value = "用户工号", required = true, position = 1)
    private String empNo;

    /**
     * 状态
     */
    @NotNull(message = "状态不能为空")
    @ApiModelProperty(value = "状态", required = true)
    private ReservationRecordItemStatus status;

}
