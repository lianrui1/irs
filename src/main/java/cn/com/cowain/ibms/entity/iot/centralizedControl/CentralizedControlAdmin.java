package cn.com.cowain.ibms.entity.iot.centralizedControl;


import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "centralized_control_admin")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "centralized_control_admin", comment = "集控管理员")
public class CentralizedControlAdmin extends BaseEntity {

    @Column(name = "hrId", nullable = false, columnDefinition = "varchar(30) COMMENT '人员工号'")
    private String hrId;

    @Column(name = "name", nullable = false, columnDefinition = "varchar(30) COMMENT '人员名称'")
    private String name;
}
