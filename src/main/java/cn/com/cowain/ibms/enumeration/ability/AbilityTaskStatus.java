package cn.com.cowain.ibms.enumeration.ability;

/**
 * @author Yang.Lee
 * @date 2022/1/10 16:56
 */
public enum AbilityTaskStatus {

    DOING("执行中"),

    SUCCESS("成功"),

    FAILED("失败");

    AbilityTaskStatus(String name){
        this.name = name;
    }

    private  String name;

    public String getName(){
        return this.name;
    }
}
