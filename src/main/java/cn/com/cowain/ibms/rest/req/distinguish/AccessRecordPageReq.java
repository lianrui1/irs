package cn.com.cowain.ibms.rest.req.distinguish;

import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 通行记录分页请求参数
 *
 * @author Yang.Lee
 * @date 2021/3/12 10:57
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("通行记录分页请求")
public class AccessRecordPageReq extends PageReq {

    @ApiModelProperty(value = "员工姓名", example = "张三", position = 4)
    private String staffName;

    @ApiModelProperty(value = "设备名称", example = "设备A", position = 5)
    private String deviceName;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    @ApiModelProperty(value = "查询开始时间", example = "2022-01-01 01:00", position = 6)
    private LocalDateTime from;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    @ApiModelProperty(value = "查询结束时间", example = "2022-01-01 02:00", position = 7)
    private LocalDateTime to;

    @ApiModelProperty(value = "查询关键字（姓名，工号）", example = "设备A", position = 8)
    private String keyWord;
}
