package cn.com.cowain.ibms.rest.office;

import cn.com.cowain.ibms.enumeration.iot.IntelligentOfficeStatus;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.office.DeviceNameEditReq;
import cn.com.cowain.ibms.rest.req.office.DeviceNameResp;
import cn.com.cowain.ibms.rest.req.office.IntelligentOfficeCreateReq;
import cn.com.cowain.ibms.rest.req.office.IntelligentOfficePageReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.office.IntelligentOfficeH5Resp;
import cn.com.cowain.ibms.rest.resp.office.IntelligentOfficePageResp;
import cn.com.cowain.ibms.rest.resp.office.UserOfficeResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.office.IntelligentOfficeService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.sfp.comm.utils.JwtUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/8 15:25
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/office")
@Api(tags = IConst.INTELLIGENT_OFFICE)
public class IntelligentOfficeController {

    @Autowired
    private IntelligentOfficeService officeService;

    @PostMapping
    @ApiOperation(value = "智能办公室新建", tags = {IConst.INTELLIGENT_OFFICE, IConst.V19})
    public ResponseEntity<JsonResult<IdResp>> create(@RequestHeader("Authorization") String token,@RequestBody @Validated @ApiParam("办公室信息") IntelligentOfficeCreateReq req, BindingResult bindingResult) {
        ServiceResult result = officeService.create(req);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.ok("OK", (IdResp)result.getObject()));
    }

    @GetMapping("/page")
    @ApiOperation(value = "智能办公室列表(分页)", tags = {IConst.INTELLIGENT_OFFICE, IConst.V19})
    public ResponseEntity<JsonResult<PageBean<IntelligentOfficePageResp>>> page(IntelligentOfficePageReq req) {

        PageBean<IntelligentOfficePageResp> officePage = officeService.page(req);

        return ResponseEntity.ok(JsonResult.ok(officePage));
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "智能办公室删除", tags = {IConst.INTELLIGENT_OFFICE, IConst.V19})
    public ResponseEntity<JsonResult<String>> delete(@PathVariable @ApiParam("智能办公室ID")String id) {


        ServiceResult result = officeService.delete(id);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "智能办公室详情", tags = {IConst.INTELLIGENT_OFFICE, IConst.V19})
    public ResponseEntity<JsonResult<IntelligentOfficePageResp>> detail(@PathVariable @ApiParam("智能办公室ID")String id) {

        ServiceResult result = officeService.detail(id);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.ok((IntelligentOfficePageResp)result.getObject()));
    }

    @GetMapping("/h5/{id}")
    @ApiOperation(value = "智能办公室扫码预览", tags = {IConst.INTELLIGENT_OFFICE, IConst.V19})
    public ResponseEntity<JsonResult<IntelligentOfficeH5Resp>> detailH5(@PathVariable @ApiParam("空间ID")String id) {

        ServiceResult result = officeService.detailH5(id);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.ok((IntelligentOfficeH5Resp)result.getObject()));
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "智能办公室状态切换", tags = {IConst.INTELLIGENT_OFFICE, IConst.V19})
    public ResponseEntity<JsonResult<String>> status(@PathVariable @ApiParam("智能办公室ID")String id, @ApiParam(value = "变更状态 ENABLE启用 DISABLE禁用", required = true) IntelligentOfficeStatus status) {

        ServiceResult result = officeService.updateStatus(id, status);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }


    @GetMapping("/user/page")
    @ApiOperation(value = "查询人员办公室列表", tags = {IConst.INTELLIGENT_OFFICE, IConst.V19})
    public ResponseEntity<JsonResult<PageBean<UserOfficeResp>>> officePage(@RequestParam(value = "page", defaultValue = "0") int page,
                                                                           @RequestParam(value = "size", defaultValue = "20") int size) {

        PageBean<UserOfficeResp> userOfficePage = officeService.userOfficePage(page, size);

        return ResponseEntity.ok(JsonResult.ok(userOfficePage));
    }

    @PutMapping("/{deviceId}/update/alias")
    @ApiOperation(value = "修改设备别名", tags = {IConst.INTELLIGENT_OFFICE, IConst.V24})
    public ResponseEntity<JsonResult<String>> updateDeviceAlias(@RequestHeader("Authorization") String token,
                                                                @PathVariable String deviceId,
                                                                @RequestBody DeviceNameEditReq req){

        String hrId = JwtUtil.getHrId(token);

        ServiceResult result = officeService.updateDeviceAlias(hrId, deviceId, req);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, result.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @GetMapping("/{officeId}/deviceName/list")
    @ApiOperation(value = "查询设备名称列表", tags = {IConst.INTELLIGENT_OFFICE, IConst.V24})
    public ResponseEntity<JsonResult<List<DeviceNameResp>>> getDeviceNameList(@RequestHeader("Authorization") String token,
                                                                              @PathVariable String officeId){

        String hrId = JwtUtil.getHrId(token);
        return ResponseEntity.ok(JsonResult.ok(officeService.getDeviceNameList(officeId, hrId)));
    }
}
