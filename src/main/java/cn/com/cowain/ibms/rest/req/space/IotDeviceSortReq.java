package cn.com.cowain.ibms.rest.req.space;

import cn.com.cowain.ibms.rest.req.IdAndSeqBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 设备排序请求类
 * @author Yang.Lee
 * @date 2021/1/11 10:52
 */
@Data
public class IotDeviceSortReq {
    
    @ApiModelProperty(value = "排序数据列表", required = true)
    List<IdAndSeqBean> data;
}
