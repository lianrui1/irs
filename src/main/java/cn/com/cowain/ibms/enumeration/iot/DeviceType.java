package cn.com.cowain.ibms.enumeration.iot;

/**
 * DeviceType
 *
 * @author cuiEnming
 * @date 2020/11/19  18:51
 * @since s2
 */
public enum DeviceType {

    /**
     * 默认
     */
    DEFAULT("未知", 0),
    /**
     * 远程设备
     */
    REMOTE_DEVICE("远程设备",0),
    /**
     * 门牌
     */
    DOOR_PLATE("门牌", 0),

    /**
     * 门磁
     */
    DOOR_MAGNETIC("门磁", 1),

    /**
     * 各种灯
     */
    LIGHT("灯", 1),

    /**
     * 屏幕
     */
    SCREEN("屏幕", 0),

    /**
     * 空调
     */
    AIR_CONDITIONER("空调", 1),

    /**
     * 光照度传感器
     */
    ILLUMINATION_SENSING("光照度传感器", 1),

    /**
     * 窗帘
     **/
    CURTAINS("智能窗帘", 1),

    /**
     * 四路开关
     **/
    FOUR_WAY_SWITCH("四路开关", 1),

    /**
     * 温湿度传感器
     **/
    TEMPERATURE_AND_HUMIDITY_SENSOR("温湿度传感器", 1),

    /**
     * 温湿度光三合一传感器
     **/
    TEMPERATURE_AND_HUMIDITY_AND_ILLUMINATION_SENSOR("温湿度光三合一传感器", 1),

    /**
     * 噪音传感器
     **/
    NOISE_SENSOR("噪音传感器", 1),

    /**
     * 空气质量传感器
     **/
    PM_SENSOR("空气质量传感器", 1),

    /**
     * 断路器设备
     **/
    CIRCUIT_BREAKER("断路器", 0),

    /**
     * 断路器设备
     **/
    SENSOR_BODY_SENSING("人体传感器", 1),

    /**
     * 旷世人脸识别设备
     **/
    FACE_RECOGNITION("旷世人脸识别设备", 0),

    /**
     * 海康人脸识别设备
     **/
    HIK_FACE_MACHINE("海康人脸识别设备", 0),

    CAMERA("萤石云摄像机", 0),
    /**
     * 旷世鸿图智能摄像头
     */
    KS_HT_SMART_CAMERA("旷世鸿图智能摄像头", 0),
    /**
     * 旷世鸿图智能服务器
     */
    KS_HT_INTELLIGENT_SERVER("旷世鸿图智能服务器", 0),
    /**
     * 旷世鸿图网络摄像机
     */
    KS_HT_WEB_CAMERA("旷世鸿图网络摄像机", 0),
    /**
     * 旷世鸿图存储服务器
     */
    KS_HT_STORAGE_SERVER("旷世鸿图存储服务器", 0),

    /**
     * 升降柱
     **/
    LEFT_COLUMN("升降柱", 0);


    String name;
    //是否是办公室会录入的 默认为0 不是  1是
    Integer isOffice = 0;
    DeviceType(String name, Integer isOffice) {
        this.name = name;
        this.isOffice = isOffice;
    }

    public String getName() {
        return this.name;
    }

    public Integer getIsOffice() {
        return this.isOffice;
    }

}
