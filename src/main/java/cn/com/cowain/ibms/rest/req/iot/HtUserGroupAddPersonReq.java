package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * ht添加用户组人员参数
 *
 * @author: yanzy
 * @date: 2022/3/23 14:55
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HtUserGroupAddPersonReq {

    /**
     *    
     * @author: yanzy
     * @param: 员工组id
     * @data: 2022/3/23 14:03:50
     * @return
     */
    @NotBlank
    @ApiModelProperty(value = "用户组业务id", required = true)
    private String groupUuid;

    /***
     *    
     * @author: yanzy
     * @param: 工号列表
     * @data: 2022/3/23 15:03:19
     * @return  
     */
    @NotEmpty
    @ApiModelProperty(value = "工号列表", required = true)
    private List<String> personUuids;
    
}
