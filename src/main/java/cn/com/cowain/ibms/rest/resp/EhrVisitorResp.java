package cn.com.cowain.ibms.rest.resp;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/2 15:06
 */
@Data
public class EhrVisitorResp {

    private String jobNum;

    private String name;

    private String phone;
}
