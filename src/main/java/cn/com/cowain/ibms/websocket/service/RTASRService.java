package cn.com.cowain.ibms.websocket.service;

import cn.com.cowain.ibms.service.bean.ServiceResult;

import javax.websocket.Session;
import java.io.IOException;
import java.net.URISyntaxException;

/**
 * @author Yang.Lee
 * @date 2021/1/18 15:04
 */
public interface RTASRService {

    /**
     * 建立链接
     * @param session
     * @return
     */
    ServiceResult open(Session session) throws URISyntaxException, InterruptedException;

    /**
     *获取音频文件，执行转写操作
     * @param session
     * @param source
     * @return
     */
    ServiceResult translate(Session session, byte[] source) throws URISyntaxException, InterruptedException;

    /**
     * 音频上传完成，断开转写客户端
     * @param session
     * @return
     */
    ServiceResult close(Session session) throws IOException;
}
