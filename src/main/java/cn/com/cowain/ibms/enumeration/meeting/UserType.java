package cn.com.cowain.ibms.enumeration.meeting;

/**
 * 用户类型
 *
 * @author Yang.Lee
 * @date 2021/8/17 10:29
 */
public enum UserType {

    /**
     * 员工
     **/
    STAFF,

    /**
     * 访客
     **/
    GUEST;
}
