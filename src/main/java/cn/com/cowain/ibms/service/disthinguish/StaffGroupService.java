package cn.com.cowain.ibms.service.disthinguish;

import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.distinguish.StaffGroup;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.PageReq;
import cn.com.cowain.ibms.rest.req.distinguish.StaffGroupPageReq;
import cn.com.cowain.ibms.rest.req.distinguish.StaffGroupReq;
import cn.com.cowain.ibms.rest.resp.BaseListResp;
import cn.com.cowain.ibms.rest.resp.distinguish.AccessControlSpotResp;
import cn.com.cowain.ibms.rest.resp.distinguish.StaffGroupResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface StaffGroupService {

    /*
     * 新增人员租
     *
     */
    ServiceResult save(StaffGroupReq staffGroupResp);

    /*
     * 查询所有人员组列表
     *
     */
    PageBean<StaffGroupResp> search(StaffGroupPageReq req);

    /*
     * 根据人员组删除人员组成员
     *
     */
    ServiceResult delete(String staffGroup);

    /*
     * 根据人员组ID获取人员组信息
     *
     */
    ServiceResult get(String id);

    /*
     * 更新人员组
     *
     */
    ServiceResult update(String id, StaffGroupReq staffGroupReq);

    /**
     * 查询所有人员组列表
     *
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/3/12 17:02
     **/
    List<BaseListResp> getList();

    /**
     * 对象转换
     *
     * @param staffGroupList 待转对象
     * @return 目标对象
     * @author Yang.Lee
     * @date 2021/3/12 17:10
     **/
    List<BaseListResp> convert(List<StaffGroup> staffGroupList);

    /**
     * 判断人员组是否存在
     *
     * @param groupIds 人员组ID,支持多个id
     * @return true:存在 ； false:不存在
     * @author Yang.Lee
     * @date 2021/4/7 9:09
     **/
    ServiceResult isGroupExist(@NotNull String... groupIds);

    /**
     * 判断员工是否在人员组中
     *
     * @param empNo  员工工号
     * @param groups 人员组
     * @return true: 员工属于所查询人员组。 false: 员工不属于所查询人员组
     * @author Yang.Lee
     * @date 2021/4/9 13:28
     **/
    boolean isStaffInGroups(@NotNull String empNo, @NotNull StaffGroup... groups);

    // 向旷世增加人员组
    ServiceResult saveStaffGroup(StaffGroupReq staffGroupReq);

    // 查询所有人员组列表分页
    PageBean<StaffGroupResp> searchPage(PageReq pageReq, String accessControlSpotId);

    // 从该员工所在人员组删除此人
    void delLeave(List<SysUser> sysUsers);

    /**
     * 根据excel文件批量添加员工组
     *
     * @param file 文件
     * @return 操作结果
     * @author Yang.Lee
     * @date 2021/7/13 11:15
     **/
    ServiceResult addStaffGroupBatch(MultipartFile file);

    /**
     * 获取导入失败数据的文件二进制数组
     *
     * @param errorDataId 失败数据的ID
     * @return excel文件的二进制数据
     * @author Yang.Lee
     * @date 2021/7/14 15:21
     **/
    byte[] getImportErrorDataFileByte(String errorDataId);

    /**
     * 根据人员组ID查询关联门禁点表
     *
     * @param staffGroupId 失败数据的ID
     * @author yanzy
     * @date 2022/01/12
     **/
    PageBean<AccessControlSpotResp> getAccessControlSpotRespList(String staffGroupId,PageReq req);
}
