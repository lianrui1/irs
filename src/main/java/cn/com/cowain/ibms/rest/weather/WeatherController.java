package cn.com.cowain.ibms.rest.weather;

import cn.com.cowain.ibms.rest.resp.weather.BaseWeatherResp;
import cn.com.cowain.ibms.service.bean.amap.weather.ForceCastsTempResult;
import cn.com.cowain.ibms.service.weather.WeatherService;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author Yang.Lee
 * @date 2021/4/15 9:38
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/weather")
@Api(tags = IConst.MODULE_WEATHER)
public class WeatherController {

    @Resource
    private WeatherService weatherService;

    /**
     * 获取实时天气数据
     *
     * @param lon 坐标经度，gps坐标
     * @param lat 坐标纬度，gps坐标
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/4/21 13:54
     **/
    @GetMapping("/live")
    @ApiOperation(value = "获取实时天气数据", tags = IConst.MODULE_WEATHER)
    public ResponseEntity<JsonResult<BaseWeatherResp>> update(@RequestParam @ApiParam(value = "经度,gps坐标", required = true) double lon,
                                                              @RequestParam @ApiParam(value = "经度,gps坐标", required = true) double lat) {

        return ResponseEntity.ok(JsonResult.ok("OK", weatherService.getWeatherByLonLat(lon, lat)));
    }
    /**
     * 获取天气预报
     *
     * @param lon 坐标经度，gps坐标
     * @param lat 坐标纬度，gps坐标
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/4/21 13:54
     **/
    @GetMapping("/forecasts")
    @ApiOperation(value = "获取天气预报数据", tags = IConst.MODULE_WEATHER)
    public ResponseEntity<JsonResult<ForceCastsTempResult>>  forecasts(@RequestParam @ApiParam(value = "经度,gps坐标", required = true) double lon,
                                                                       @RequestParam @ApiParam(value = "经度,gps坐标", required = true) double lat) {
        return ResponseEntity.ok(JsonResult.ok("OK", weatherService.getForecastsWeatherByLonLat(lon, lat)));
    }
}
