package cn.com.cowain.ibms.feign.iotc.bean;

import cn.com.cowain.ibms.utils.DateUtils;
import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 设备通行记录查询请求对象
 *
 * @author Yang.Lee
 * @date 2022/2/24 20:31
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DeviceRecordSearchReq {

    @ApiModelProperty(value = "设备SN集合，多个设备用',' 分割")
    private String doorIds;

    @JSONField(format = DateUtils.PATTERN_DATETIME)
    private LocalDateTime startTime;

    @JSONField(format = DateUtils.PATTERN_DATETIME)
    private LocalDateTime endTime;

    private int page;

    private int size;

    private DataType dataType;

    public enum DataType{

        /**
         * 实时
         */
        REALTIME("实时"),
        /**
         * 候补
         */
        WAITING("候补");

        String name;

        DataType(String name){
            this.name = name;
        }
    }
}
