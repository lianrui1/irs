package cn.com.cowain.ibms.service.office.strategy.deviceType;

import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.rest.resp.device.OfficeCommonDeviceResp;
import cn.com.cowain.ibms.rest.resp.device.centralized.CentralizedControlDeviceFilter;
import cn.com.cowain.ibms.rest.resp.office.DoorlResp;
import cn.com.cowain.ibms.rest.resp.office.ShowDeviceResp;
import cn.com.cowain.ibms.service.office.strategy.DeviceTypeStrategy;

import java.util.List;

public class DoorMagneticStrategy implements DeviceTypeStrategy {

    @Override
    public void addCommonDeviceResp(ShowDeviceResp resp, List<OfficeCommonDeviceResp> commonDeviceRespList) {
        DoorlResp doorlResp = new DoorlResp();
        doorlResp.setId(resp.getDeviceId());
        doorlResp.setHwDeviceId(resp.getHwDeviceId());
        doorlResp.setHwStatus(resp.getHwStatus().name());
        doorlResp.setDeviceName(resp.getDeviceName());
        commonDeviceRespList.add(new OfficeCommonDeviceResp(doorlResp, resp.getDeviceTypeName()));
    }

    @Override
    public void filterDevicePropertiesStatus(IotDevice iotDevice, CentralizedControlDeviceFilter controlDeviceFilter, String action) {

    }


}
