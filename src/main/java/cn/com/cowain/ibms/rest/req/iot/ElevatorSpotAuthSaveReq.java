package cn.com.cowain.ibms.rest.req.iot;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/3/23 2:45
 */
@Data
@ApiModel("分配权限请求对象")
public class ElevatorSpotAuthSaveReq {

    @NotEmpty
    @ApiModelProperty(value = "人员信息")
    private List<ElevatorSpotUserSaveReq> userReqList;

    @NotEmpty
    @ApiModelProperty(value = "空间Id")
    private List<String> spaceIds;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "通行开始时间")
    private LocalDateTime accessStartTime;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "通行结束时间")
    private LocalDateTime accessEndTime;
}
