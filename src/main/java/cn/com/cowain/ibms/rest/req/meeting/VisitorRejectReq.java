package cn.com.cowain.ibms.rest.req.meeting;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/9 13:49
 */
@Data
public class VisitorRejectReq {

    // vrurId
    private String id;

    // 拒绝的code是30
    private int visitorStatus;
}
