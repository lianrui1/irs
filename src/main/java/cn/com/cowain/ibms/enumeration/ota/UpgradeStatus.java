package cn.com.cowain.ibms.enumeration.ota;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/4/16 9:26
 */
public enum UpgradeStatus{

    /**
     * 废弃
     */
    DISCARD("废弃"),
    /**
     * 应用
     */
    APPLY("应用");

    UpgradeStatus(String name) {this.name = name;}

    private String name;

    public String getName() {
        return this.name;
    }


}
