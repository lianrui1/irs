package cn.com.cowain.ibms.rest.resp.bim;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/10/18 17:07
 */
@Data
@ApiModel("bim项目数据及能耗")
public class BimResp {

    @ApiModelProperty(value = "总设备数", required = true)
    private Integer all;

    @ApiModelProperty(value = "在线设备数", required = true)
    private Integer online;

    @ApiModelProperty(value = "在线率", required = true)
    private Double onlineRate;

    @ApiModelProperty(value = "离线设备数", required = true)
    private Integer offline;

    @ApiModelProperty(value = "离线率", required = true)
    private Double offlineRate;

    @ApiModelProperty(value = "项目设备数", required = true)
    private List<BimAllResp> bimAllResps;
}
