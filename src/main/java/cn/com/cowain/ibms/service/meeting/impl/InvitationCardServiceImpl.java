package cn.com.cowain.ibms.service.meeting.impl;

import cn.com.cowain.ibms.dao.ReservationRecordItemDao;
import cn.com.cowain.ibms.dao.SysUserDao;
import cn.com.cowain.ibms.dao.meeting.GuestDetailDao;
import cn.com.cowain.ibms.dao.meeting.GuestInvitationRecordDao;
import cn.com.cowain.ibms.dao.meeting.InvitationCardDao;
import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.entity.ReservationRecordItem;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.meeting.GuestDetail;
import cn.com.cowain.ibms.entity.meeting.GuestInvitationRecord;
import cn.com.cowain.ibms.entity.meeting.InvitationCard;
import cn.com.cowain.ibms.entity.space.Project;
import cn.com.cowain.ibms.enumeration.ReservationRecordStatus;
import cn.com.cowain.ibms.enumeration.meeting.GuestInvitationStatus;
import cn.com.cowain.ibms.enumeration.meeting.InvitationCardStatus;
import cn.com.cowain.ibms.rest.req.meeting.AccompanyReq;
import cn.com.cowain.ibms.rest.req.meeting.EhrAcceptReq;
import cn.com.cowain.ibms.rest.req.meeting.InvitationAcceptReq;
import cn.com.cowain.ibms.rest.req.meeting.InvitationRefuseReq;
import cn.com.cowain.ibms.rest.resp.meeting.AcceptResp;
import cn.com.cowain.ibms.rest.resp.meeting.FaceResp;
import cn.com.cowain.ibms.rest.resp.meeting.InvitationCardResp;
import cn.com.cowain.ibms.service.ReservationRecordService;
import cn.com.cowain.ibms.service.SMSService;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.WeChatService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.meeting.InvitationCardService;
import cn.com.cowain.ibms.service.space.SpaceService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.zip.CRC32;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/6/29 13:42
 */
@Slf4j
@Service
public class InvitationCardServiceImpl implements InvitationCardService {

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserServiceUC;

    @Autowired
    private WeChatService weChatService;
    @Autowired
    private SMSService smsService;

    @Autowired
    private ReservationRecordService reservationRecordService;

    @Autowired
    private InvitationCardDao cardDao;

    @Autowired
    private SysUserDao sysUserDao;

    @Autowired
    private GuestDetailDao detailDao;

    @Autowired
    private GuestInvitationRecordDao recordDao;

    @Autowired
    private ReservationRecordItemDao reservationRecordItemDao;

    @Autowired
    private RestTemplate restTemplate;

    @Value("${cowain.ehr.accept.url}")
    private String acceptUrl;

    @Value("${cowain.ehr.verificationCode.url}")
    private String verificationCodeUrl;

    @Value("${cowain.ehr.face.url}")
    private String faceUrl;

    @Value("${cowain.ehr.visitor.status.url}")
    private String statusUrl;

    @Override
    @Transactional
    public ServiceResult getDetail(String code, String recordCode) {
        Optional<InvitationCard> op = cardDao.findByInvitationCode(code);
        if(!op.isPresent()){
            return ServiceResult.error("邀请函有误");
        }
        InvitationCardResp resp = new InvitationCardResp();
        InvitationCard card = op.get();
        BeanUtils.copyProperties(card , resp);
        ReservationRecord reservationRecord = card.getReservationRecord();
        // 邀请人
        Optional<SysUser> byEmpNo = sysUserDao.findByEmpNo(reservationRecord.getInitiatorEmpNo());
        if(byEmpNo.isPresent()){
            resp.setHost(byEmpNo.get().getNameZh());
        }
        visitor(resp ,card ,reservationRecord);
        // 根据recordCode获取访客信息
        if (!StringUtils.isBlank(recordCode)){
            Optional<GuestInvitationRecord> recordOp = recordDao.findByCode(recordCode);
            if(recordOp.isPresent()){
                GuestInvitationRecord record = recordOp.get();
                Optional<GuestDetail> detailOp = detailDao.findByInvitationAndMobile(record.getInvitationCard(), record.getMobile());
                resp.setVisitorStatus(recordOp.get().getGuestInvitationStatus().toString());
                resp.setMobile(record.getMobile());
                resp.setName(record.getName());
                if(detailOp.isPresent()){
                    resp.setJobNum(detailOp.get().getJobNum());
                }
            }else {
                resp.setVisitorStatus(GuestInvitationStatus.DELETE.toString());
            }
        }
        if(ReservationRecordStatus.CANCEL.equals(card.getReservationRecord().getStatus())){
            resp.setVisitorStatus(GuestInvitationStatus.CANCEL.toString());
        }
        return ServiceResult.ok(resp);
    }

    private void visitor(InvitationCardResp resp ,InvitationCard card ,ReservationRecord reservationRecord) {
        // 邀请函单号
        resp.setVisitorRecordNo(card.getVisitorRecordNo());
        // 访客类型代码code
        resp.setRoleCode(card.getRoleCode());
        // 来访地址
        Project project = reservationRecord.getRoom().getSpace().getProject();
        resp.setAddress(project.getProvinceName() + project.getCityName() + project.getDistrictName() + project.getAddress());
        // 来访单位
        resp.setVisitCompany(project.getVisitCompany());
        // 审批备注
        resp.setInvitationRemark(card.getRemark());
        // 会议预约记录信息
        resp.setTimeFrom(reservationRecord.getFrom());
        resp.setTimeTo(reservationRecord.getTo());
        resp.setTopic(reservationRecord.getTopic());
        String spaceName = SpaceService.getFullSpaceName(reservationRecord.getRoom().getSpace() , reservationRecord.getRoom().getSpace().getName());
        resp.setRoom(spaceName);
    }

    @Override
    @Transactional
    public ServiceResult accept(String code, InvitationAcceptReq req) {
        /*String url = null;
        try {
            // 解密
            byte[] b = Base64.decodeBase64(req.getPhoto());
            // 图片校验
            *//*ServiceResult result = PicUtils.validateFace4HC(b);
            if(!result.isSuccess()){
                return ServiceResult.error(result.getObject());
            }*//*

            // 解密
            url = PicUtils.readImg2FastDfs(b);
            将文件上传至fastdfs
            url = FastDfsPool.upload(s);

            // 文件地址
        } catch (Exception e) {
            return ServiceResult.error("图片有误");
        }*/

        // 根据code获取邀请函
        Optional<InvitationCard> byInvitationCode = cardDao.findByInvitationCode(code);
        if(!byInvitationCode.isPresent()){
            return ServiceResult.error("当前邀请函不存在");
        }
        // 向ehr同步访客数据
        EhrAcceptReq acceptReq = new EhrAcceptReq();
        acceptReq.setV(byInvitationCode.get().getVisitorRecordNo());
        acceptReq.setCompany(req.getCompany());
        acceptReq.setName(req.getName());
        acceptReq.setPhotoUrl(req.getPhotoUrl());
        acceptReq.setPhone(req.getMobile());
        acceptReq.setVerificationCode(req.getVerificationCode());
        String requestParam = JSON.toJSONString(acceptReq);
        log.debug("json参数 ： " + requestParam);
        // 发送Http请求 下发控制命令
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity<>(requestParam, headers);
        ResponseEntity<String> responseEntity;

        try {
            responseEntity = restTemplate.postForEntity(acceptUrl, requestEntity, String.class);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ServiceResult.error("ehr同步访客失败");
        }
        // 解析请求结果
        String responseBody = responseEntity.getBody();
        JSONObject jsonResult = JSON.parseObject(responseBody);
        String codeEhr = jsonResult.getString("code");
        if(!codeEhr.equals("0")){
            return ServiceResult.error(jsonResult.getString("msg"));
        }
        log.debug("向ehr同步访客 : " + req.getName() + req.getMobile());
        // 访客工号
        String jobNum = jsonResult.getString("jobNum");

        // 根据邀请函和手机号获取会议访客详情
        Optional<GuestDetail> op = detailDao.findByInvitationAndMobile(byInvitationCode.get() ,req.getMobile());
        if(op.isPresent()){
            GuestDetail guestDetail = op.get();
            BeanUtils.copyProperties(req ,guestDetail);
            guestDetail.setPhotoUrl(req.getPhotoUrl());
            guestDetail.setJobNum(jobNum);
            detailDao.save(guestDetail);
        }else {
            GuestDetail guestDetail = new GuestDetail();
            BeanUtils.copyProperties(req ,guestDetail);
            guestDetail.setPhotoUrl(req.getPhotoUrl());
            guestDetail.setInvitation(byInvitationCode.get());
            guestDetail.setJobNum(jobNum);
            detailDao.save(guestDetail);
        }

        // 根据邀请函及手机号获取邀请记录
        Optional<GuestInvitationRecord> recordOp = recordDao.findByInvitationCardAndCode(byInvitationCode.get() ,req.getCode());
        // 如果记录存在则改为已接受
        if(recordOp.isPresent()){
            GuestInvitationRecord record = recordOp.get();
            record.setGuestInvitationStatus(GuestInvitationStatus.ACCEPTED);
            recordDao.save(record);
            log.debug("接受会议，向发起人【" + record.getInvitationCard().getReservationRecord().getInitiatorEmpNo() + "】发送微信模板消息");
            ReservationRecord reservationRecord = record.getInvitationCard().getReservationRecord();
            weChatService.accept(record, reservationRecord);
            log.debug("接受会议，发送微信模板消息完成");
        }
        AcceptResp resp = new AcceptResp();
        resp.setJobNum(jobNum);
        resp.setVisitorRecordNo(byInvitationCode.get().getVisitorRecordNo());
        return ServiceResult.ok(resp);
    }

    @Override
    @Transactional
    public ServiceResult refuse(String code, InvitationRefuseReq req) {
        // 根据code获取邀请函
        Optional<InvitationCard> byInvitationCode = cardDao.findByInvitationCode(code);
        if(!byInvitationCode.isPresent()){
            return ServiceResult.error("当前邀请函不存在");
        }
        InvitationCard card = byInvitationCode.get();
        // 根据邀请函及访客邀请记录码获取邀请记录
        Optional<GuestInvitationRecord> recordOp = recordDao.findByInvitationCardAndCode(card ,req.getRecordCode());
        if(recordOp.isPresent()){
            GuestInvitationRecord record = recordOp.get();
            record.setGuestInvitationStatus(GuestInvitationStatus.REJECTED);
            recordDao.save(record);
            log.debug("拒绝会议，向发起人【" + record.getInvitationCard().getReservationRecord().getInitiatorEmpNo() + "】发送微信模板消息");
            ReservationRecord reservationRecord = record.getInvitationCard().getReservationRecord();
            weChatService.refuse(record, reservationRecord);
            log.debug("拒绝会议，发送微信模板消息完成");
        }
        // 根据邀请卡获取访客邀请记录
        List<GuestInvitationRecord> recordList = recordDao.findByInvitationCardReservationRecord(card.getReservationRecord());
        // 判断参会人仅为访客  所有邀请记录状态 如果全拒绝则释放会议资源
        List<ReservationRecordItem> recordItems = reservationRecordItemDao.findByReservationRecord(card.getReservationRecord());
        if (getGuestInvitationStatus(recordList) && recordItems.size() <= 1){
            reservationRecordService.cancel(card.getReservationRecord().getId() ,"token");
            log.debug("拒绝会议，向发起人【" + card.getReservationRecord().getInitiatorEmpNo() + "】发送微信模板消息");
            ReservationRecord reservationRecord = card.getReservationRecord();
            weChatService.refuseAll(reservationRecord);
            log.debug("拒绝会议，发送微信模板消息完成");
        }
        return ServiceResult.ok();
    }

    @Override
    @Transactional
    public ServiceResult verificationCode(String phone) {
        // url
        StringBuilder urlTemplate = new StringBuilder();
        urlTemplate.append(verificationCodeUrl).append(phone);
        // 发送Http请求 下发控制命令
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity<>( headers);
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = restTemplate.exchange(urlTemplate.toString(), HttpMethod.GET, requestEntity, String.class);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ServiceResult.error("发送短信验证码失败");
        }
        // 解析请求结果
        String responseBody = responseEntity.getBody();
        JSONObject jsonResult = JSON.parseObject(responseBody);
        String codeEhr = jsonResult.getString("code");
        if(!codeEhr.equals("0")){
            return ServiceResult.error(jsonResult.getString("msg"));
        }
        return ServiceResult.ok();
    }

    @Override
    public ServiceResult uploadAndCheckFace(MultipartFile multipartFile) {
        // 调ehr接口上传人脸照片
        ResponseEntity<String> responseEntity;
        MultiValueMap<String, Object> multiValueMap = new LinkedMultiValueMap<>();
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpHeaders fileHeader = new HttpHeaders();
        String contentType = multipartFile.getContentType();
        if(StringUtils.isNotEmpty(contentType)){
            fileHeader.setContentType(MediaType.parseMediaType(contentType));
        }
        fileHeader.setContentDispositionFormData("file", multipartFile.getOriginalFilename());
        try {

            HttpEntity<ByteArrayResource> fileEntity = new HttpEntity<>(new ByteArrayResource(multipartFile.getBytes()),
                    fileHeader);
            multiValueMap.add("file", fileEntity);
            HttpEntity<MultiValueMap<String, Object>> param = new HttpEntity<>(multiValueMap, header);
            responseEntity = restTemplate.postForEntity(faceUrl, param, String.class);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ServiceResult.error("人脸验证失败");
        }
        // 解析请求结果
        String responseBody = responseEntity.getBody();
        JSONObject jsonResult = JSON.parseObject(responseBody);
        String codeEhr = jsonResult.getString("code");
        if (!codeEhr.equals("0")) {
            log.error("图片上传失败 : " + jsonResult.getString("msg"));
            return ServiceResult.error(jsonResult.getString("msg"));
        }
        // 判断照片是否合格
        String filePath = jsonResult.getString("filePath");
        FaceResp faceResp = new FaceResp();
        JSONObject face = jsonResult.getJSONObject("validateFace");
        if(face != null){
            String picIsOk = face.getString("picIsOk");
            String message = face.getString("message");
            log.debug(message + " : " +filePath);
            if("1".equals(picIsOk)){
                faceResp.setFilePath(filePath);
                faceResp.setMessage(message);
                faceResp.setPicIsOk(picIsOk);
            }else {
                log.debug(message + " : " +filePath);
                return ServiceResult.error(message);
            }
        }

        return ServiceResult.ok(faceResp);
    }

    @Override
    @Transactional
    public ServiceResult visitorStatus(String visitorRecordNo, String jobNum, String recordCode) {
        InvitationCardResp cardResp = new InvitationCardResp();
        // todo 如果是拒绝直接返回
        // 查询本地访客状态如果拒绝直接返回
        /*Optional<GuestInvitationRecord> recordOp = recordDao.findByCode(recordCode);
        if(recordOp.isPresent()){
            GuestInvitationRecord record = recordOp.get();
            if(GuestInvitationStatus.REJECTED.equals(record.getGuestInvitationStatus())){
                cardResp.setVisitorStatus("1000");
                return ServiceResult.ok(cardResp);
            }
        }*/
        Optional<InvitationCard> cardOp = cardDao.findByVisitorRecordNo(visitorRecordNo);
        if(!cardOp.isPresent()){
            return ServiceResult.error("邀请函单号有误");
        }
        InvitationCard card = cardOp.get();
        // 向ehr查询访客状态
        log.debug("邀请函单号: " + visitorRecordNo);
        log.debug("访客工号: " + jobNum);
        // url
        StringBuilder urlTemplate = new StringBuilder();
        urlTemplate.append(statusUrl).append("?visitorRecordNo=").append(visitorRecordNo)
                .append("&jobNum=").append(jobNum);
        // 发送Http请求 下发控制命令
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity<>( headers);
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = restTemplate.exchange(urlTemplate.toString(), HttpMethod.GET, requestEntity, String.class);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ServiceResult.error("查询访客信息失败");
        }
        // 解析请求结果
        String responseBody = responseEntity.getBody();
        JSONObject jsonResult = JSON.parseObject(responseBody);
        log.debug("查询访客状态从ehr查询访客邀请函结果{}",jsonResult);
        String codeEhr = jsonResult.getString("code");
        if(!codeEhr.equals("0")){
            return ServiceResult.error(jsonResult.getString("msg"));
        }
        JSONObject self = jsonResult.getJSONObject("self");
        if(self!=null){
            cardResp.setVisitorStatus(self.getString("visitorStatus"));
            cardResp.setIsSignIn(self.getString("isSignIn"));
        }
        // 响应信息
        BeanUtils.copyProperties(card , cardResp);
        ReservationRecord reservationRecord = card.getReservationRecord();
        visitor(cardResp ,card ,reservationRecord);
        SysUser user = sysUserServiceUC.getUserByEmpNo(reservationRecord.getInitiatorEmpNo());
        // 发起人
        cardResp.setHost(user.getNameZh());
        Optional<GuestDetail> detailOp = detailDao.findByJobNumAndInvitation(jobNum ,card);
        if(detailOp.isPresent()){
            GuestDetail detail = detailOp.get();
            BeanUtils.copyProperties(detail ,cardResp);
        }
        return ServiceResult.ok(cardResp);
    }

    @Override
    public ServiceResult accompany(AccompanyReq req) {
        Optional<GuestInvitationRecord> recordOp = recordDao.findByCode(req.getRecordCode());
        if(!recordOp.isPresent()){
            return ServiceResult.error("你不是访客,无法转发");
        }
        GuestInvitationRecord record = recordOp.get();
        InvitationCard card = record.getInvitationCard();
        req.getAccompanyInfoList().forEach(it ->{
            GuestInvitationRecord invitationRecord = new GuestInvitationRecord();
            invitationRecord.setName(it.getName());
            invitationRecord.setMobile(it.getPhone());
            invitationRecord.setGuestInvitationStatus(GuestInvitationStatus.DEFAULT);
            invitationRecord.setInvitationCard(card);
            // 生成访客邀请记录码
            CRC32 crc32 = new CRC32();
            crc32.update(UUID.randomUUID().toString().getBytes(StandardCharsets.UTF_8));
            String value = String.valueOf(crc32.getValue());
            invitationRecord.setCode(value);
            invitationRecord.setInviterCode(record.getCode());
            recordDao.save(invitationRecord);
            // 获取发起人信息
            SysUser user = sysUserServiceUC.getUserByEmpNo(card.getReservationRecord().getInitiatorEmpNo());
            smsService.asyncSendMeetingInvitationSMS(user.getNameZh() ,user.getMobile() ,it.getPhone() ,record.getInvitationCard().getInvitationCode() ,value);
        });
        return ServiceResult.ok();
    }

    @Override
    public ServiceResult delete(InvitationCard card) {
        // 删除该邀请含下所有访客
        List<GuestDetail> details = detailDao.findByInvitationReservationRecord(card.getReservationRecord());
        details.forEach( it -> it.setIsDelete(1));
        detailDao.saveAll(details);
        List<GuestInvitationRecord> records = recordDao.findByInvitationCardReservationRecord(card.getReservationRecord());
        records.forEach(it -> it.setIsDelete(1));
        recordDao.saveAll(records);
        card.setIsDelete(1);
        cardDao.save(card);
        return ServiceResult.ok();
    }

    @Override
    public ServiceResult cancel(InvitationCard card) {
        card.setStatus(InvitationCardStatus.CANCEL);
        cardDao.save(card);
        /*List<GuestInvitationRecord> recordList = recordDao.findByInvitationCardReservationRecord(card.getReservationRecord());
        for (GuestInvitationRecord record : recordList){
            record.setGuestInvitationStatus(GuestInvitationStatus.REJECTED);
            records.add(record);
        }
        recordDao.saveAll(records);*/
        return ServiceResult.ok();
    }

    private boolean getGuestInvitationStatus(List<GuestInvitationRecord> recordList) {
        for(GuestInvitationRecord record : recordList){
            // 如果邀请卡状态为接受或待确认则false
            if(GuestInvitationStatus.DEFAULT.equals(record.getGuestInvitationStatus()) ||
                    GuestInvitationStatus.ACCEPTED.equals(record.getGuestInvitationStatus())){
                return false;
            }
        }
        return true;
    }
}
