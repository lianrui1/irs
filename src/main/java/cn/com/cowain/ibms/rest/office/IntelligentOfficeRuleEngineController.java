package cn.com.cowain.ibms.rest.office;

import cn.com.cowain.ibms.annotation.Idempotent;
import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.req.office.RuleEngineControlReq;
import cn.com.cowain.ibms.rest.req.office.RuleEngineDetailReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.office.ControlledDeviceResp;
import cn.com.cowain.ibms.rest.resp.office.RuleEngineResp;
import cn.com.cowain.ibms.rest.resp.office.SensorDeviceResp;
import cn.com.cowain.ibms.rest.resp.office.RuleEngIneDetailResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.office.IntelligentOfficeRuleEngineService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.utils.JwtUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * 联动规则
 *
 * @author: yanzy
 * @date: 2022/1/27 13:14
 */

@Slf4j
@RestController
@RequestMapping(API_BASE+"/linkage")
@Api(tags = IConst.INTELLIGENT_OFFICE_APPLET)
public class IntelligentOfficeRuleEngineController {

    @Resource
    private IntelligentOfficeRuleEngineService intelligentOfficeRuleEngineService;


    /**
     * 根据办公室id查询规则引擎列表
     *
     * @author: yanzy
     * @data: 2022/1/27 15:01:26
     * @return
     */
    @GetMapping("/engine/{id}")
    @ApiOperation(value = "查询规则引擎列表",tags = {IConst.INTELLIGENT_OFFICE_APPLET,IConst.V24})
    public ResponseEntity<JsonResult<List<RuleEngineResp>>> findRuleEngine(@PathVariable @ApiParam(value = "办公室ID")String id){

        List<RuleEngineResp> ruleEngine = intelligentOfficeRuleEngineService.findRuleEngine(id);
        return ResponseEntity.ok(JsonResult.ok("OK",ruleEngine));
    }

    /**
     * 添加规则引擎
     *
     * @author: yanzy
     * @param:
     * @data: 2022/1/27 16:01:07
     * @return
     */
    @PostMapping("/engine")
    @ApiOperation(value = "添加规则引擎",tags = {IConst.INTELLIGENT_OFFICE_APPLET,IConst.V24})
    public ResponseEntity<JsonResult<IdResp>> creatRuleEngine(@RequestHeader("Authorization") String token,@RequestBody @Validated RuleEngineDetailReq req){
        log.debug("save...");

        ServiceResult serviceResult = intelligentOfficeRuleEngineService.creatRuleEngine(req);
        if (!serviceResult.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(serviceResult.getObject()),null,null));
        }
        return ResponseEntity.ok(JsonResult.ok("OK", (IdResp)serviceResult.getObject()));
    }

    /**
     * 根据id查询规则引擎详情
     *
     * @author: yanzy
     * @data: 2022/1/27 16:01:54
     * @return
     */
    @GetMapping("/engine/detail/{ruleEngineId}")
    @ApiOperation(value = "查询规则引擎详情",tags = {IConst.INTELLIGENT_OFFICE_APPLET,IConst.V24})
    public ResponseEntity<JsonResult<RuleEngIneDetailResp>> findRuleEngineDetail(@RequestHeader("Authorization") String token,@PathVariable @ApiParam(value = "规则引擎ID") String ruleEngineId){
        //从token中获取员工工号
        String hrId = JwtUtil.getHrId(token);
        ServiceResult ruleEngineDetail = intelligentOfficeRuleEngineService.findRuleEngineDetail(ruleEngineId,hrId);
        if (!ruleEngineDetail.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(ruleEngineDetail.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.ok("ok" ,(RuleEngIneDetailResp)ruleEngineDetail.getObject()));
    }

    /**
     * 根据规则引擎id删除
     *
     * @author: yanzy
     * @data: 2022/1/27 15:01:02
     * @return
     */

    @DeleteMapping("/engine/{ruleEngineId}")
    @ApiOperation(value = "删除规则引擎",tags = {IConst.INTELLIGENT_OFFICE_APPLET,IConst.V24})
    public ResponseEntity<JsonResult<String>> deleteRuleEngine(@PathVariable @ApiParam(value = "规则引擎ID")String ruleEngineId,@RequestHeader("Authorization") String token){
        //从token中获取员工工号
        String hrId = JwtUtil.getHrId(token);
        ServiceResult serviceResult = intelligentOfficeRuleEngineService.deleteRuleEngine(ruleEngineId, hrId);
        if (!serviceResult.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(serviceResult.getObject()),null,ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    /***
     * 根据规则引擎id修改
     *
     * @author: yanzy
     * @data: 2022/1/27 18:01:00
     * @return
     */
    @PutMapping("/engine/{ruleEngineId}")
    @Idempotent(value = API_BASE + "/engine/put", expireTime = 2L)
    @ApiOperation(value = "修改规则引擎",tags = {IConst.INTELLIGENT_OFFICE_APPLET,IConst.V24})
    public ResponseEntity<JsonResult<String>> updateRileEngine(@RequestHeader("Authorization") String token,@PathVariable @ApiParam(value = "规则引擎ID")String ruleEngineId,@RequestBody @Validated RuleEngineDetailReq req){
        ServiceResult serviceResult = intelligentOfficeRuleEngineService.updateRileEngine(ruleEngineId, req);
        if (!serviceResult.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(serviceResult.getObject()),null,ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    /**
     * 根据办公室id查询传感器智能设备列表
     *
     * @author: yanzy
     * @data: 2022/1/27 13:01:14
     * @return：
     */
    @GetMapping("/sensor/{id}")
    @ApiOperation(value = "查询传感器设备列表",tags = {IConst.INTELLIGENT_OFFICE_APPLET,IConst.V24})
    public ResponseEntity<JsonResult<List<SensorDeviceResp>>> findSensorDevice(@RequestHeader("Authorization") String token,@PathVariable @ApiParam(value = "办公室ID") String id){
        //从token中获取员工工号
        String hrId = JwtUtil.getHrId(token);
        List<SensorDeviceResp> sensorDevice = intelligentOfficeRuleEngineService.findSensorDevice(id,hrId);
        return ResponseEntity.ok(JsonResult.ok("OK",sensorDevice));
    }

    /**
     * 根据办公室id查询可控智能设备列表
     *
     * @author: yanzy
     * @data: 2022/1/27 14:01:05
     * @return
     */
    @GetMapping("/controlled/{id}")
    @ApiOperation(value = "查询可控设备列表",tags = {IConst.INTELLIGENT_OFFICE_APPLET,IConst.V24})
    public ResponseEntity<JsonResult<List<ControlledDeviceResp>>> findControlledDeviceAll(@RequestHeader("Authorization") String token,@PathVariable @ApiParam(value = "办公室ID") String id){
        //从token中获取员工工号
        String hrId = JwtUtil.getHrId(token);
        List<ControlledDeviceResp> controlledDeviceAll = intelligentOfficeRuleEngineService.findControlledDeviceAll(id,hrId);
        return ResponseEntity.ok(JsonResult.ok("OK",controlledDeviceAll));
    }

    /**
     * 根据可控设备id查询当前办公室下联动规则是否存在
     *
     * @author: yanzy
     * @data: 2022/1/28 18:01:15
     * @return
     */
    @GetMapping("/controlled/device/{id}")
    @ApiOperation(value = "判断当前办公室下联动规则中可控设备是否已存在")
    public ResponseEntity<JsonResult<ServiceResult>> findControlledDevice(@RequestHeader("Authorization") String token,@PathVariable @ApiParam(value = "办公室ID") String id, @RequestParam @ApiParam(value = "设备id") List<String> deviceId){
        //从token中获取员工工号
        String hrId = JwtUtil.getHrId(token);
        ServiceResult controlledDevice = intelligentOfficeRuleEngineService.findControlledDevice(id, deviceId,hrId);
        return ResponseEntity.ok(JsonResult.ok("OK",controlledDevice));
    }

    @PutMapping("/ruleChain/control")
    @ApiOperation(value = "规则引擎控制",tags = {IConst.INTELLIGENT_OFFICE_APPLET,IConst.V24})
    public ResponseEntity<JsonResult<String>> ruleEngineControl(@RequestBody @Validated RuleEngineControlReq req){

        ServiceResult serviceResult = intelligentOfficeRuleEngineService.ruleEngineControl(req);
        if (!serviceResult.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(serviceResult.getObject()),null,ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }
}
