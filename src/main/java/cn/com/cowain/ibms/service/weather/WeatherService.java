package cn.com.cowain.ibms.service.weather;

import cn.com.cowain.ibms.rest.resp.weather.BaseWeatherResp;
import cn.com.cowain.ibms.service.bean.amap.weather.ForceCastsTempResult;

/**
 * @author Yang.Lee
 * @date 2021/4/19 14:28
 */
public interface WeatherService {

    /**
     * 获取天气情况
     *
     * @param cityCode 城市编码
     * @return 天气情况
     * @author Yang.Lee
     * @date 2021/4/19 14:29
     **/
    BaseWeatherResp getWeatherByCityCode(String cityCode);

    /**
     * 根据经纬度获取天气状况
     *
     * @param lon 精度，gps坐标
     * @param lat 维度，gps坐标
     * @return 天气状况
     * @author Yang.Lee
     * @date 2021/4/21 13:19
     **/
    BaseWeatherResp getWeatherByLonLat(double lon, double lat);
    /**
     * 获取天气预报
     *
     * @param cityCode 城市编码
     * @return 天气情况
     * @author Yang.Lee
     * @date 2021/4/19 14:29
     **/
    ForceCastsTempResult getForecastsWeatherByCityCode(String cityCode);
    /**
     * 根据经纬度获取天气预报
     *
     * @param lon 精度，gps坐标
     * @param lat 维度，gps坐标
     * @return 天气状况
     * @author Yang.Lee
     * @date 2021/4/21 13:19
     **/
    ForceCastsTempResult getForecastsWeatherByLonLat(double lon, double lat) ;
}
