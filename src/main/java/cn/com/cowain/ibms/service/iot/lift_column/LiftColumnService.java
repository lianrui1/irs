package cn.com.cowain.ibms.service.iot.lift_column;

import cn.com.cowain.ibms.rest.resp.BaseListResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;

import java.util.List;

/**
 * 升降柱业务Service
 *
 * @author Yang.Lee
 * @date 2022/6/24 10:50
 */
public interface LiftColumnService {

    /**
     * 控制设备
     *
     * @param userHrId 用户工号
     * @param parkId   停车场ID
     * @param cmd      控制指令。up: 升柱， down: 降柱
     * @return 控制结果
     * @author Yang.Lee
     * @date 2022/6/24 10:58
     **/
    ServiceResult control(String userHrId, String parkId, String cmd);

    /**
     * 获取停车场列表
     *
     * @return 停车场列表
     * @author Yang.Lee
     * @date 2022/6/24 13:11
     **/
    List<BaseListResp> getParkList();
}
