package cn.com.cowain.ibms.rest.resp.office;

import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.rest.req.office.DeviceNameEditReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * @author Yang.Lee
 * @date 2021/12/7 13:23
 */
@Data
public class ShowDeviceResp {



    /**
     * 设备类型类型
     */
    @ApiModelProperty(value = "设备类型名称", required = true)
    private String deviceTypeName;
    /**
     * 设备类型类型
     */
    @ApiModelProperty(value = "设备类型", required = true)
    private DeviceType deviceType;


    /**
     * 设备类型类型
     */
    @ApiModelProperty(value = "id", required = true)
    private String deviceId;

    /**
     * 设备类型类型
     */
    @ApiModelProperty(value = "id", required = true)
    private String  deviceName;
    /**
     * 设备类型类型
     */
    @ApiModelProperty(value = "sn", required = true)
    private String deviceSN;

    @ApiModelProperty(value = "设备可用状态", required = true)
    private String usableStatusName;


    /**
     * 华为云id
     */
    @ApiModelProperty(value = "华为云id", required = true)
    private String  hwDeviceId;

    /**
     * 华为云id
     */
    @ApiModelProperty(value = "是否在线", required = true)
    private DeviceStatus hwStatus;


    @ApiModelProperty(value = "设备附加名称", required = true)
    private DeviceNameEditReq.AdditionalName additionalName;
}
