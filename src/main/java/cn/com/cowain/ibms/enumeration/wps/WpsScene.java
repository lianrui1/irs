package cn.com.cowain.ibms.enumeration.wps;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author wei.cheng
 * @date 2022/02/23 18:48
 */
public enum WpsScene {
    /**
     * 会议文档
     */
    MEETING_FILE;

    private static final Map<String, WpsScene> WPS_SCENE_MAP = new HashMap<>();

    static {
        for (WpsScene wpsScene : WpsScene.values()) {
            WPS_SCENE_MAP.put(wpsScene.name(), wpsScene);
        }
    }

    public static WpsScene resolves(String name) {
        return Objects.isNull(name) ? null : WPS_SCENE_MAP.get(name);
    }

    public static Boolean contains(String name) {
        return Objects.nonNull(resolves(name));
    }
}
