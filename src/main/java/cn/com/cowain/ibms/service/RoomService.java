package cn.com.cowain.ibms.service;

import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.RoomReq;
import cn.com.cowain.ibms.rest.resp.RoomAllResp;
import cn.com.cowain.ibms.rest.resp.RoomSearchResp;
import cn.com.cowain.ibms.rest.resp.room.RoomResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;

import java.util.List;

/**
 * Description
 *
 * @author 张宇鑫
 * @version 1.0
 * @date 2020/8/5 13:30
 */
public interface RoomService {

    /**
     * 根据时间排序
     *
     * @param roomReq
     * @return
     * @author 张宇鑫
     */
    ServiceResult save(RoomReq roomReq);

    /**
     * 根据会议室id 更新
     *
     * @param id
     * @param roomReq
     * @return
     * @author 张宇鑫
     */
    ServiceResult update(String id, RoomReq roomReq);

    /**
     * 根据会议室名称 page size 模糊查询
     *
     * @param roomName
     * @param page
     * @param size
     * @return
     */
    PageBean<RoomSearchResp> search(String roomName, int page, int size);

    /**
     * 根据会议室id 更新 timeSlot roomDevice状态
     *
     * @param id
     * @param integer
     * @return
     * @author 张宇鑫
     */
    Boolean updateRoomTimeSlotAndDeviceStatus(String id, Integer integer);

    /**
     * 判断添加/更新 参数有效性
     *
     * @param roomReq
     * @return true:有效; false 无效
     */
    boolean valid(RoomReq roomReq);

    /**
     * 查询可用会议室列表
     *
     * @return
     * @author Yang.Lee
     */
    List<RoomSearchResp> searchAvailableList();

    /**
     * 查询可用会议室列表
     *
     * @return
     */
    List<RoomAllResp> searchAllRoomList();

    /**
     * 删除会议室
     *
     * @param roomId: 会议室ID
     * @return 逻辑结果
     * @author Yang.Lee
     * @date 2021/3/6 14:30
     **/
    ServiceResult delete(String roomId, String token);

    /**
     * 获取会议室详情
     *
     * @param roomId 会议室ID
     * @return 会议室详情
     * @author Yang.Lee
     * @date 2021/4/22 19:33
     **/
    RoomSearchResp get(String roomId);

    /**
     * 根据区域id查询会议室列表
     *
     * @param projectId 区域id
     * @return 会议室列表
     * @author Yang.Lee
     * @date 2021/7/16 10:23
     **/
    List<RoomSearchResp> getRoomByProjectId(String projectId);

    // 查询会议室是否可以邀请访客
    ServiceResult getMeeting(String roomId);

    /***
     *@Param: 根据项目ID查询并且Space中isShow=1
     *@author：yanzy
     *@date:2021/12/28
     */
    List<RoomResp> findBySpaceProjectIdAndSpaceIsShow(String projectid);
}
