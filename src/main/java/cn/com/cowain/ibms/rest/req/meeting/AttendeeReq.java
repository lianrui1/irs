package cn.com.cowain.ibms.rest.req.meeting;

import cn.com.cowain.ibms.rest.req.SysUserBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 添加参会人请求参数
 *
 * @author: yanzy
 * @date: 2022/4/19 15:33
 */
@Data
public class AttendeeReq {

    @ApiModelProperty(name = "会议id",required = true,position = 1)
    private String reservationId;

    @ApiModelProperty(name = "参与人列表", required = true,position = 2)
    private List<SysUserBean> participantList;

    /**
     * 会议组列表
     */
    @ApiModelProperty(value = "会议组列表", position = 9)
    private List<String> userGroupList;

}
