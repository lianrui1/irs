package cn.com.cowain.ibms.entity.meeting;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/17 15:36
 */
@Data
@ApiModel("EHR邀请函详情")
public class EhrVisitor {

    // id
    private String id;

    // 邀请函单号
    private String visitorRecordNo;

    // 事由code
    private String reasonCode;

    // 事由内容
    private String reasonCodeName;

    private String assginee;

    private String examineCode;

    private String examineValue;

    private String receiverJobNum;

    private String companyId;

    private String receiverName;

    private String receiverPhone;

    // 地址
    private String address;

    private String visitorUserStatus;

    private String visitorUserStatusName;

    // 邀请函生效时间
    private String startTime;

    // 邀请函结束时间
    private String endTime;

    // 邀请函状态
    private String status;

    // 备注
    private String remarks;

    private String visitorType;

    private String examineRemarks;

    // 是否vip
    private String isVip;

    //访客类型代码code
    private String roleCode;
}
