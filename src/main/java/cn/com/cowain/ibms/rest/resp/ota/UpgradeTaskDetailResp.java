package cn.com.cowain.ibms.rest.resp.ota;

import cn.com.cowain.ibms.enumeration.ota.Application;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.time.LocalDateTime;

/**
 * @author Yang.Lee
 * @date 2021/4/14 18:17
 */
@Data
@Builder
@ApiModel("app升级任务详情")
public class UpgradeTaskDetailResp {

    @ApiModelProperty(value = "升级任务ID", required = true, example = "id", position = 1)
    private String id;

    @ApiModelProperty(value = "升级任务名称", required = true, example = "task name", position = 2)
    private String taskName;

    @ApiModelProperty(value = "应用安装包", required = true, example = "x.apk", position = 3)
    private String appPackageUrl;

    @ApiModelProperty(value = "应用安装包MD5", required = true, example = "123456", position = 4)
    private String appPackageMD5;

    @ApiModelProperty(value = "升级任务大小", required = true, example = "20M", position = 5)
    private String appSize;

    @ApiModelProperty(value = "下载次数", required = true, example = "0", position = 6)
    private int download;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "创建时间", required = true, example = "3000-12-31 09:30", position = 7)
    private LocalDateTime createdTime;

    @ApiModelProperty(value = "升级应用", required = true, example = "DOORPLATE_PAD", position = 8)
    private Application application;

    @ApiModelProperty(value = "升级应用名称", required = true, example = "智能语音助手", position = 9)
    private String applicationName;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "升级时间", required = true, example = "3000-12-31 09:30", position = 10)
    private LocalDateTime upgradeTime;

    @ApiModelProperty(value = "更新内容", example = "1. 哈哈哈", position = 11)
    private String content;

    @ApiModelProperty(value = "强制更新，默认否", example = "false", position = 12)
    private boolean forceUpdate;

    @ApiModelProperty(value = "文件名", required = true, example = "x.apk", position = 13)
    private String originalFileName;

    @Tolerate
    public UpgradeTaskDetailResp() {
        throw new UnsupportedOperationException();
    }
}
