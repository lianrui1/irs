package cn.com.cowain.ibms.dao.device;

import cn.com.cowain.ibms.entity.centralized.CentralizedCodeDeviceRelation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author wei.cheng
 * @date 2022/09/16 09:19
 */
public interface CentralizedCodeDeviceRelationDao extends JpaRepository<CentralizedCodeDeviceRelation, String>, JpaSpecificationExecutor<CentralizedCodeDeviceRelation> {

    /**
     * 根据集控码id查询
     *
     * @author: yanzy
     * @data: 2022/9/20 13:09:44
     */
    List<CentralizedCodeDeviceRelation> findByCentralizedControlCodeId(String codeId);


    /**
     * 软删除集控码的关联设备
     *
     * @param centralizedControlCodeId
     */
    @Modifying
    @Query(
            value = "update CentralizedCodeDeviceRelation set isDelete = 1 where centralizedControlCodeId = ?1"
    )
    void deleteAllByCentralizedControlCodeId(String centralizedControlCodeId);

    /**
     * 查询集控码的关联设备
     *
     * @param centralizedControlCodeId
     * @return
     */
    List<CentralizedCodeDeviceRelation> findAllByCentralizedControlCodeId(String centralizedControlCodeId);
}
