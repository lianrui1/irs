package cn.com.cowain.ibms.enumeration.meeting;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/4/6 17:16
 */
public enum  FileType {

    URL("链接"),

    FILE("文件");

    private String name;

    FileType(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
}
