package cn.com.cowain.ibms.rest.req.office;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @author Yang.Lee
 * @date 2022/1/28 9:33
 */
@Data
@ApiModel("设备名称编辑")
public class DeviceNameEditReq implements Serializable {

    @NotEmpty
    @ApiModelProperty(value = "办公室ID", required = true)
    private String officeId;

    @ApiModelProperty(value = "别名", required = true)
    private String alias;

    @ApiModelProperty(value = "附加名称", required = true)
    private AdditionalName additionalName;

    @Data
    public static class AdditionalName implements Serializable {

        public static AdditionalName empty() {
            return new AdditionalName();
        }

        @ApiModelProperty(value = "开关1名称")
        private String switch1Name = "";

        @ApiModelProperty(value = "开关2名称")
        private String switch2Name = "";

        @ApiModelProperty(value = "开关3名称")
        private String switch3Name = "";

        @ApiModelProperty(value = "开关4名称")
        private String switch4Name = "";
    }
}
