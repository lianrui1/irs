package cn.com.cowain.ibms.rest.iot;

import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.PageReq;
import cn.com.cowain.ibms.rest.req.Sorts;
import cn.com.cowain.ibms.rest.req.iot.DeviceGroupEditReq;
import cn.com.cowain.ibms.rest.req.space.DevicePageReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.iot.DeviceGroupDetailResp;
import cn.com.cowain.ibms.rest.resp.iot.DeviceGroupTreeResp;
import cn.com.cowain.ibms.rest.resp.iot.IotDeviceResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.iot.DeviceGroupService;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.*;

/**
 * @author Yang.Lee
 * @date 2021/10/21 9:26
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/deviceGroup")
@Api(tags = MODULE_DEVICE_GROUP)
public class DeviceGroupController {

    @Resource
    private DeviceGroupService deviceGroupService;

    /**
     * 创建群组
     *
     * @param req           请求参数
     * @param bindingResult 参数校验对象
     * @return 创建结果，创建成功返回新数据主键ID
     * @author Yang.Lee
     * @date 2021/10/21 10:45
     **/
    @PostMapping
    @ApiOperation(value = "创建群组", tags = {MODULE_DEVICE_GROUP, V17})
    public ResponseEntity<JsonResult<IdResp>> create(@RequestBody DeviceGroupEditReq req, BindingResult bindingResult) {

        ServiceResult serviceResult = deviceGroupService.create(req);
        if (!serviceResult.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(serviceResult.getObject()), null, serviceResult.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.ok((IdResp) serviceResult.getObject()));
    }

    /**
     * 获取群组树
     *
     * @return 群组树列表
     * @author Yang.Lee
     * @date 2021/10/21 10:54
     **/
    @GetMapping("/tree")
    @ApiOperation(value = "获取群组树", tags = {MODULE_DEVICE_GROUP, V17})
    public ResponseEntity<JsonResult<List<DeviceGroupTreeResp>>> getTree() {

        return ResponseEntity.ok(JsonResult.ok(deviceGroupService.getTree()));
    }

    /**
     * 向群组中添加设备
     *
     * @param id           设备群组ID
     * @param deviceIdList 需要向群组中添加的设备ID集合
     * @return 操作结果
     * @author Yang.Lee
     * @date 2021/10/21 11:00
     **/
    @PostMapping("/{id}/addDevice")
    @ApiOperation(value = "向群组中添加设备", tags = {MODULE_DEVICE_GROUP, V17})
    public ResponseEntity<JsonResult<String>> addDevice(@PathVariable @ApiParam("设备群组ID") String id,
                                                        @RequestBody @ApiParam(value = "需要向群组中添加的设备ID集合", required = true) List<String> deviceIdList) {

        ServiceResult serviceResult = deviceGroupService.addDevice(id, deviceIdList);
        if (!serviceResult.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(serviceResult.getObject()), null, serviceResult.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.ok(null));
    }

    /**
     * 查询群组详情
     *
     * @param id 群组ID
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/10/21 11:09
     **/
    @GetMapping("/{id}")
    @ApiOperation(value = "获取设备群组详情", tags = {MODULE_DEVICE_GROUP, V17})
    public ResponseEntity<JsonResult<DeviceGroupDetailResp>> get(@PathVariable String id, PageReq pageReq) {

        return ResponseEntity.ok(JsonResult.ok(deviceGroupService.get(id, pageReq)));
    }

    /**
     * 获取所有群组里的设备列表
     *
     * @return 设备列表
     * @author Yang.Lee
     * @date 2021/10/21 11:11
     **/
    @GetMapping("/device/list")
    @ApiOperation(value = "获取所有群组里的设备列表", tags = {MODULE_DEVICE_GROUP, V17})
    public ResponseEntity<JsonResult<PageBean<IotDeviceResp>>> getAllDevice(DevicePageReq req) {

        req.addOrderDesc(Sorts.CREATED_TIME);
        return ResponseEntity.ok(JsonResult.ok("OK", deviceGroupService.getAllDevice(req)));
    }

    /**
     * 删除设备群组
     *
     * @param id 群组 ID
     * @return 删除结果
     * @author Yang.Lee
     * @date 2021/10/21 16:43
     **/
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除设备群组", tags = {MODULE_DEVICE_GROUP, V17})
    public ResponseEntity<JsonResult<String>> delete(@PathVariable String id) {

        ServiceResult serviceResult = deviceGroupService.delete(id);
        if (!serviceResult.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(serviceResult.getObject()), null, serviceResult.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.ok(null));
    }

    /**
     * 从群组中移除设备
     *
     * @param id 群组 ID
     * @return 删除结果
     * @author Yang.Lee
     * @date 2021/10/21 16:43
     **/
    @PutMapping("/{id}/removeDevice/{deviceId}")
    @ApiOperation(value = "从群组中移除设备", tags = {MODULE_DEVICE_GROUP, V17})
    public ResponseEntity<JsonResult<String>> removeDevice(@PathVariable @ApiParam("群组ID") String id, @PathVariable @ApiParam("设备ID") String deviceId) {

        ServiceResult serviceResult = deviceGroupService.removeDevice(id, deviceId);
        if (!serviceResult.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(serviceResult.getObject()), null, serviceResult.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.ok(null));
    }

    /**
     * 编辑设备群组信息
     *
     * @param id 群组 ID
     * @param req 编辑参数
     * @return 编辑结果
     * @author Yang.Lee
     * @date 2021/10/21 16:43
     **/
    @PutMapping("/{id}")
    @ApiOperation(value = "编辑设备群组信息", tags = {MODULE_DEVICE_GROUP, V17})
    public ResponseEntity<JsonResult<String>> update(@PathVariable String id, @RequestBody @Valid DeviceGroupEditReq req, BindingResult bindingResult) {

        ServiceResult serviceResult = deviceGroupService.edit(id, req);
        if (!serviceResult.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(serviceResult.getObject()), null, serviceResult.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.ok("OK", null));
    }
}
