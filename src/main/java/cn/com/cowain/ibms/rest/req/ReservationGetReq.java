package cn.com.cowain.ibms.rest.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

/**
 * Description
 *
 * @author 张宇鑫
 * @version 1.0
 * @date 2020/8/4 17:58
 */
@Data
@ApiModel("会议预约查询请求")
public class ReservationGetReq {

    /**
     * 当前页数
     */
    @Min(0)
    @ApiModelProperty(value = "当前页码，默认0", position = 1, example = "1")
    private Integer page = 0;

    /**
     * 每页大小
     */
    @Min(0)
    @ApiModelProperty(value = "每页数据长度，默认10", position = 2, example = "10")
    private Integer size = 10;

    /**
     * 从多少天前开始算
     */
    @Min(0)
    @ApiModelProperty(value = "从多少天前开始算，默认7", position = 3, example = "7")
    private Integer days = 7;

    /**
     * 项目ID
     */
    @ApiModelProperty(value = "项目ID", required = true, position = 4, example = "1234556778900")
    private String projectId;

    /*
     * 员工工号
     **/
    @NotBlank(message = "员工工号不能为空")
    @ApiModelProperty(value = "员工工号", required = true, position = 5, example = "CWA0001")
    private String empNo;

    @ApiModelProperty(value = "开始日期", example = "2022-03-30")
    private String start;

    @ApiModelProperty(value = "结束日期", example = "2022-03-30")
    private String end;

    @ApiModelProperty(value = "模糊查关键字")
    private String keyword;
}
