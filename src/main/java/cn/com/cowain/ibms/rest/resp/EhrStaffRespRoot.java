package cn.com.cowain.ibms.rest.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/4 20:23
 */
@Data
@ApiModel("EHR 员工响应")
public class EhrStaffRespRoot implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("消息")
    private String msg;

    @ApiModelProperty("代码")
    private Integer code;

    @ApiModelProperty("分页数据")
    private EhrStaffRespPage page;

}
