package cn.com.cowain.ibms.service.bean;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2021/1/28 11:21
 */
@Data
public class DoorMagneticHWParasReq {

    @ApiModelProperty(value = "1不进行权限判断 空则进行权限判断")
    private Integer isAuth;

    @ApiModelProperty(value = "会议记录ID", position = 1)
    private String recordId;

    @ApiModelProperty(value = "门磁开门 open close", position = 1)
    private String action;

    @ApiModelProperty(value = "空调的打开与关闭 on off", position = 2)
    private String setPower;

    @ApiModelProperty(value = "空调温度", position = 3)
    private Integer setTemp;

    @ApiModelProperty(value = "空调制冷制热模式 cool heat", position = 4)
    private String setMode;

    @ApiModelProperty(value = "空调风速 小 中 大 自动 min med max auto", position = 5)
    private String setFanSpeed;

    @ApiModelProperty(value = "空调风向 上 中 下 up med down", position = 6)
    private String setFanDirection;

    @ApiModelProperty(value = "空调风向 手动 自动 true false", position = 7)
    private String setFanAuto;

    @ApiModelProperty(value = "设置扫风模式 ，无扫风，上下扫风，左右扫风，上下左右扫风 noSwing,updownSwing,leftrightSwing,aroundSwing")
    private String setSwing;

    @ApiModelProperty(value = "四路开关 on off", position = 8)
    private String light1;

    @ApiModelProperty(value = "四路开关 on off", position = 9)
    private String light2;

    @ApiModelProperty(value = "四路开关 on off", position = 10)
    private String light3;

    @ApiModelProperty(value = "四路开关 on off", position = 11)
    private String light4;

    @ApiModelProperty(value = "断路器开关 on off", position = 11)
    @JSONField(name = "switch")
    private String isSwitch;

    @ApiModelProperty(value = "用电量", position = 12)
    private Double electricity;

    @ApiModelProperty(value = "亮度", position = 11)
    private String luminance;

    @ApiModelProperty(value = "温度", position = 11)
    private String temperature;

    @ApiModelProperty(value = "湿度", position = 11)
    private String humidity;

    @ApiModelProperty(value = "噪音", position = 11)
    private String noise;

    @ApiModelProperty(value = "空气质量", position = 11)
    private String pm10;

    @ApiModelProperty(value = "空气质量", position = 11)
    private String pm2_5;

}
