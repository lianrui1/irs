package cn.com.cowain.ibms.dao.space;

import cn.com.cowain.ibms.entity.space.IotProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Description
 *
 * @author yuchunlei
 * @version 1.0.0
 * @createTime 2020年12月21日 09:03:00
 */
@Repository
public interface IotProductDao extends JpaRepository<IotProduct, String>, JpaSpecificationExecutor<IotProduct> {

    @Query("SELECT MAX(iotProduct.productNo) FROM IotProduct AS iotProduct where iotProduct.productNo > 0")
    Integer findMaxProductNo();

    /**
     * 根据产品编号查询产品
     *
     * @param productNo
     * @param productName
     * @return
     * @author Yang.Lee
     */
    Optional<IotProduct> findByProductNoAndName(int productNo, String productName);

    /**
     * 根据产品名称查询产品详情
     *
     * @param name
     * @return
     * @author Yang.Lee
     */
    Optional<IotProduct> findByName(String name);

    /**
     * 根据IOTC id查询
     *
     * @param iotcId iotc id
     * @return 产品信息
     * @author Yang.Lee
     * @date 2021/10/25 15:31
     **/
    Optional<IotProduct> findByIotcId(String iotcId);

    /**
     * 根据华为id查询
     *
     * @param hwId 华为id
     * @return 产品信息
     * @author Yang.Lee
     * @date 2021/10/25 15:40
     **/
    Optional<IotProduct> findByhwProductId(String hwId);
}
