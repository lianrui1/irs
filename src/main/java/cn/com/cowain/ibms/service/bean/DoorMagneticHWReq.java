package cn.com.cowain.ibms.service.bean;

import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2021/1/28 11:11
 */
@Data
public class DoorMagneticHWReq {

    /**
     * 场景模式
     */
    private String mode;

    private String deviceId;
    /**
     * 华为设备ID
     */
    private String device_id;
    /**
     * 服务ID,默认DoorManage
     */
    private String service_id;

    /**
     * 指令，默认doorAction
     */
    private String command_name;
    /**
     * 参数
     */
    private DoorMagneticHWParasReq paras;

    /**
     * 操作失败返回error
     */
    private String read = "read";

    // 是否单个命令下发
    private String isOne = "0";
}
