package cn.com.cowain.ibms.rest.resp.device;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 硬件清单分页数据返回对象
 * @author Yang.Lee
 * @date 2021/3/17 18:10
 */
@Data
@Builder
@ApiModel("硬件清单分页返回数据")
public class HardwarePageResp {
    @ApiModelProperty(value = "硬件ID",  example = "id", position = 0)
    private String id;

    @ApiModelProperty(value = "硬件SN",  example = "123456", position = 1)
    private String hardwareSN;

    @ApiModelProperty(value = "硬件名称，中文",  example = "设备A", position = 2)
    private String nameZH;

    @ApiModelProperty(value = "硬件名称，英文",  example = "ABC", position = 3)
    private String nameEN;

    @ApiModelProperty(value = "所属产品名称",  example = "产品A", position = 4)
    private String productName;

    @ApiModelProperty(value = "设备类型",  example = "网关", position = 5)
    private String deviceType;

    @ApiModelProperty(value = "品牌",  example = "海康", position = 6)
    private String brand;

    @ApiModelProperty(value = "型号",  example = "MM-001", position = 7)
    private String model;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "创建时间",  example = "2222-02-22 22:22", position = 8)
    private LocalDateTime createTime;
}
