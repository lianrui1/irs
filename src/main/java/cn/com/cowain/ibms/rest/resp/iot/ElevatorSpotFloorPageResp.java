package cn.com.cowain.ibms.rest.resp.iot;

import cn.com.cowain.ibms.entity.iot.ElevatorSpot;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.space.Project;
import cn.com.cowain.ibms.rest.resp.space.SpaceFloorResp;
import cn.com.cowain.ibms.service.space.SpaceService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Objects;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/3/23 2:54
 */
@Slf4j
@Data
@Builder
@ApiModel("梯控按人员列表响应对象")
public class ElevatorSpotFloorPageResp {

    @ApiModelProperty(value = "梯控点ID")
    private String id;

    @ApiModelProperty(value = "所属楼层空间")
    private SpaceFloorResp space;

    @ApiModelProperty(value = "所属楼宇")
    private String address;

    @ApiModelProperty(value = "权限人员数")
    private Integer peopleNum;

    @ApiModelProperty(value = "绑定设备")
    private List<IotDeviceResp> deviceList;

    public static ElevatorSpotFloorPageResp convert(ElevatorSpot elevatorSpot) {
        return ElevatorSpotFloorPageResp.builder()
                .id(elevatorSpot.getId())
                .space(SpaceFloorResp.convert(elevatorSpot.getSpace()))
                .address(getAddress(elevatorSpot))
                .build();
    }

    public static String getAddress(ElevatorSpot elevatorSpot) {
        if (Objects.isNull(elevatorSpot.getSpace())) {
            return "";
        }
        Space space = elevatorSpot.getSpace();
        Project project = space.getProject();
        StringBuilder result = new StringBuilder();
        if (Objects.nonNull(space.getParent())) {
            result.append(SpaceService.getFullSpaceName(space.getParent(), space.getParent().getName()));
            if (Objects.nonNull(project)) {
                result.insert(0, " / ").insert(0, project.getProjectName());
            }
        } else {
            if (Objects.nonNull(project)) {
                result.insert(0, project.getProjectName());
            }
        }
        return result.toString();
    }
}
