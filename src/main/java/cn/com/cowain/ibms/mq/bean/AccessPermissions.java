package cn.com.cowain.ibms.mq.bean;

import cn.com.cowain.ibms.utils.DateUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZoneId;
import java.util.List;

/**
 * iotc 设备人员处理对象
 *
 * @author Yang.Lee
 * @date 2022/1/27 16:14
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccessPermissions {

    @ApiModelProperty(value = "返回的topic", example = "return-person")
    private String returnTopic;

    @ApiModelProperty(value = "系统名称", example = "IBMS")
    private String sourceName = "IBMS";

    @ApiModelProperty(value = "返回时的资源id")
    private String resourceId;

    @ApiModelProperty(value = "方法名", example = "add:添加人员信息  bind:绑定人员到组  increment_bind:旷视设备人员添加  unbind:解绑人员 del:从平台删除人员组")
    private String methodName;

    @ApiModelProperty(value = "模块名", example = "person:添加人员信息")
    private String moduleName;

    @ApiModelProperty(value = "设备集合")
    private List<String> deviceCodeList;

    @ApiModelProperty(value = "用户列表", example = "[{'jobNo':'CWA3264','groupIds':['d40e04c8-3db7-4005-bc78-ce7b43b0936c'],'photo':'https://file.cowainglobal.com/img/M00/07/25/rBIQr2HKe9uAHLdnAAJcCEuEUYo424.jpg','name':'唐其亮'}]}")
    private List<User> userList;

    @ApiModelProperty(value = "数据发送结束标识位", required = true, example = "", position = 8)
    private String over;

    /**
     * 员工信息对象
     *
     * @author Yang.Lee
     * @date 2022/1/27 16:13
     **/
    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class User {

        @ApiModelProperty(value = "用户工号", required = true, example = "CWA8888", position = 1)
        private String jobNo;

        @ApiModelProperty(value = "姓名", required = true, example = "张三", position = 2)
        private String name;

        @ApiModelProperty(value = "图片url", example = "https://a.d/b/c.jpg", position = 3)
        private String photo;

        @ApiModelProperty(value = "人员组id", position = 4)
        private List<String> groupIds;

        @ApiModelProperty(value = "访客邀请人工号", position = 4)
        private String visitedWorkNo;

        @ApiModelProperty(value = "创建时间(旷视)", position = 5)
        private String createTime;

        @ApiModelProperty(value = "创建时间(海康)", position = 5)
        private String startTime;

        @ApiModelProperty(value = "结束时间", position = 6)
        private String endTime;

        @ApiModelProperty(value = "人员类型", position = 7)
        private PersonType personType;

        /**
         * 对象转换
         *
         * @return
         * @author Yang.Lee
         * @date 2022/1/27 17:59
         **/
        public static User convert(OpenAbilityMessage.User user, List<String> groupIdList) {

            return User.builder()
                    .jobNo(user.getHrId())
                    .name(user.getName())
                    .photo(user.getPhoto())
                    .visitedWorkNo(user.getInvitedUserHrId())
                    .createTime(user.getStartTime() != null ? DateUtils.formatLocalDateTime(user.getStartTime(), DateUtils.PATTERN_DATETIME) : null)
                    .startTime(user.getStartTime() != null ? DateUtils.formatZoneDateTime(user.getStartTime().atZone(ZoneId.of(DateUtils.GMT8)), DateUtils.PATTERN_DATETIME_WITH_ZONE) : null)
                    .endTime(user.getStartTime() != null ? DateUtils.formatZoneDateTime(user.getEndTime().atZone(ZoneId.of(DateUtils.GMT8)), DateUtils.PATTERN_DATETIME_WITH_ZONE) : null)
                    .groupIds(groupIdList)
                    .build();
        }
    }

    /**
     * 模块
     *
     * @author Yang.Lee
     * @date 2022/1/27 16:13
     **/
    public enum Model {

        /**
         * 访客
         **/
        VISITOR("visitor"),

        /**
         * 员工
         **/
        PERSON("person");

        String name;

        Model(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }
    }

    /**
     * 消息 TAG
     *
     * @author Yang.Lee
     * @date 2022/1/27 16:18
     **/
    public enum Method {

        ADD("add"),

        DELETE("delete"),

        BIND("bind"),

        INCREMENT_BIND("increment_bind"),

        UNBIND("unbind"),

        ADD_VISITOR("add_visitor"),

        UPDATE("update");

        String name;

        Method(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }
    }

    public enum PersonType {
        /**
         * 员工
         */
        EMPLOYEE("1"),


        /**
         * 访客
         */
        VISITOR("2"),


        /**
         * 重点人员
         */
        KEY_PERSON("3");

        PersonType(String name) {
            this.name = name;
        }

        private String name;

        public String getName() {
            return this.name;
        }
    }
}
