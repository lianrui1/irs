package cn.com.cowain.ibms.dao.meal;

import cn.com.cowain.ibms.entity.meal.MealPerson;
import cn.com.cowain.ibms.enumeration.meal.MealStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * @author Yang.Lee
 * @date 2022/1/14 10:54
 */
public interface MealPersonDao extends JpaRepository<MealPerson, String>, JpaSpecificationExecutor<MealPerson> {

    /**
     * @param mealStatus
     * @return
     * @description 获取报餐进行中数据
     * @author tql
     * @date 22-5-18
     */
    List<MealPerson> findByStatus(MealStatus mealStatus);


    /**
     * @param workNo
     * @return
     * @description 根据工号获取最新的一条数据
     * @author tql
     * @date 22-5-26
     */
    Optional<MealPerson> findFirstByWorkNoOrderByCreatedTimeDesc(String workNo);

    /**
     * 查询{@code time}之后的报餐数据
     *
     * @param time
     * @return
     */
    List<MealPerson> findAllByCreatedTimeAfter(LocalDateTime time);

}
