package cn.com.cowain.ibms.rest.resp.iot;

import cn.com.cowain.ibms.rest.resp.StaffResp;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author wei.cheng
 * @date 2022/03/08 10:28
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("查询我对门禁点拥有的权限信息")
public class GetMyPermissionOfAccessControlSpotResp {

    @ApiModelProperty(value = "是否具有通过门禁的权限")
    private Boolean isAccess;

    @ApiModelProperty("员工对门禁点最近一次门禁点审批记录")
    private AccessControlSpotApplicationRecordResp latestAccessControlSpotApplicationRecord;

    @ApiModelProperty(value = "门禁点信息")
    private AccessControlSpotPageResp accessControlSpot;

    @ApiModelProperty("门禁点的分配人列表")
    @JsonInclude()
    private List<StaffResp> accessControlSpotApproverList;

    @ApiModelProperty("当前人员信息")
    private AccessControlUserResp currentUser;

    @ApiModelProperty("用户对门禁点拥有的通行时间段")
    private List<AccessTimeResp> accessTimeList;
}
