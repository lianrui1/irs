package cn.com.cowain.ibms.rest.req.meeting;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author wei.cheng
 * @date 2022/03/02 17:39
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("会议室文档")
@AllArgsConstructor
@NoArgsConstructor
public class UpdateMeetingFileReq extends MeetingFileReq {

    @ApiModelProperty(value = "ID，只有当已存在的会议文档时有效", position = 1)
    private String id;
}
