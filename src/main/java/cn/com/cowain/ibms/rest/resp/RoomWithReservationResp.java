package cn.com.cowain.ibms.rest.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

/**
 * 房间,和对应的预约记录
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/5/20
 */
@Data
@ApiModel("房间和对应的预约记录")
public class RoomWithReservationResp {

    /**
     * room ID
     */
    @ApiModelProperty(value = "room ID", required = true, position = 0)
    private String roomId;

    /**
     * room 名称
     */
    @ApiModelProperty(value = "room 名称", required = true, position = 1)
    private String name;

    /**
     * 楼栋 + 楼层
     */
    @ApiModelProperty(value = "空间地址", required = true, position = 2)
    private String location;

    /**
     * 楼栋
     */
    @ApiModelProperty(value = "楼栋", required = true, position = 3)
    private String building;

    /**
     * 楼层
     */
    @ApiModelProperty(value = "楼层", required = true, position = 4)
    private String floor;

    /**
     * 人数容量
     */
    @ApiModelProperty(value = "人数容量", required = true, position = 5)
    private int capacity;

    /**
     * 设备列表
     */
    @ApiModelProperty(value = "设备列表", required = true, position = 6)
    private List<String> deviceList;

    /**
     * 时间槽 列表
     */
    @ApiModelProperty(value = "时间槽列表", required = true, position = 7)
    private List<TimeSlotResultResp> timeSlotResultRespList;

    /**
     * 会议室 预约日期
     */
    @ApiModelProperty(value = "会议室 预约日期", required = true, position = 8)
    private LocalDate date;
    /**
     * 是否开放 0:否 1:是
     */
    @ApiModelProperty(value = "integer COMMENT '是否开放 0:否;1:是'")
    private int isOpen;

    private Integer usedTime;
}
