package cn.com.cowain.ibms.rest;

import cn.com.cowain.ibms.rest.resp.AreaResp;
import cn.com.cowain.ibms.service.AreaService;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * 省市区字典数据controller
 * @author Yang.Lee
 * @date 2020/12/17 17:49
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/area")
@Api(tags = IConst.AREA)
public class AreaController {

    @Autowired
    private AreaService areaService;

    /**
     * 获取省列表
     * @return
     */
    @GetMapping("/province")
    @ApiOperation(value = "获取省级列表", tags = IConst.AREA)
    public ResponseEntity<JsonResult<List<AreaResp>>> getProvinceList() {

        // 查询所有省级列表
        List<AreaResp> result = areaService.searchAreaProvinceList();
        return ResponseEntity.ok(JsonResult.ok("OK", result));
    }

    /**
     * 获取市列表
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}/city")
    @ApiOperation(value = "获取市级列表", tags = IConst.AREA)
    public ResponseEntity<JsonResult<List<AreaResp>>> getCityList(@PathVariable @ApiParam("省编号") Long id) {
        // 查询对应市级列表
        List<AreaResp> result = areaService.searchAreaCityList(id);
        return ResponseEntity.ok(JsonResult.ok("OK", result));
    }

    /**
     * 获取区列表
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}/district")
    @ApiOperation(value = "获取区级列表", tags = IConst.AREA)
    public ResponseEntity<JsonResult<List<AreaResp>>> getDistrictList(@PathVariable @ApiParam("市编号") Long id) {
        // 查询对应区级列表
        List<AreaResp> result = areaService.searchAreaDistrictList(id);
        return ResponseEntity.ok(JsonResult.ok("OK", result));
    }

}
