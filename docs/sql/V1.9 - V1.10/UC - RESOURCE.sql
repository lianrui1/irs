INSERT INTO sys_resource (
	res_code,
	res_name,
	symbol,
	symbol_name,
	res_type,
	icon_path,
	forward,
	sort_no,
	p_id,
	depth,
	is_leaf,
	is_public,
	`status`,
	is_display,
	version,
	create_at,
	create_by,
	create_by_name,
	update_at,
	update_by,
	update_by_name,
	ts,
	name_key,
	abbreviation 
)
VALUES
	(
		'IBMS-MEGNETIC-ACCESS_RULE',
		'门磁通行权限',
		'6',
		'ibms',
		'11',
		'',
		'/sucurity/door_magnet',
		2,
		1340935650907525174,
		2,
		1,
		0,
		1,
		1,
		0,
		now(),
		1,
		'测试',
		now(),
		1,
		'测试',
		NULL,
		'MEGNETIC',
	'门磁通行权限' 
	);

	INSERT INTO sys_resource (
	res_code,
	res_name,
	symbol,
	symbol_name,
	res_type,
	icon_path,
	forward,
	sort_no,
	p_id,
	depth,
	is_leaf,
	is_public,
	`status`,
	is_display,
	version,
	create_at,
	create_by,
	create_by_name,
	update_at,
	update_by,
	update_by_name,
	ts,
	name_key,
	abbreviation 
)
VALUES
	(
		'IBMS-MEERING-ROOM-APP-UPGRADE',
		'APP升级管理',
		'6',
		'ibms',
		'11',
		'',
		'/meeting/app_upgrade',
		2,
		1340935650907525128,
		2,
		1,
		0,
		1,
		1,
		0,
		now(),
		1,
		'测试',
		now(),
		1,
		'测试',
		NULL,
		'APP-UPGRADE',
	'APP升级' 
	);

INSERT INTO sys_resource (res_code, res_name, symbol, symbol_name, res_type , icon_path, forward, sort_no, p_id, depth , is_leaf, is_public, `status`, is_display, version , create_at, create_by, create_by_name, update_at, update_by , update_by_name, ts, name_key, abbreviation) VALUES ('ibms_doorMagnet_add', '新建', '6', 'ibms', '12' , NULL, '', 2, 1340935650907525203, 2 , 1, 0, 1, 1, 0 , now(), 1, '测试', now(), 1 , '测试', NULL, '', NULL);
INSERT INTO sys_resource (res_code, res_name, symbol, symbol_name, res_type , icon_path, forward, sort_no, p_id, depth , is_leaf, is_public, `status`, is_display, version , create_at, create_by, create_by_name, update_at, update_by , update_by_name, ts, name_key, abbreviation) VALUES ('ibms_doorMagnet_delete', '删除', '6', 'ibms', '12' , NULL, '', 2, 1340935650907525203, 2 , 1, 0, 1, 1, 0 , now(), 1, '测试', now(), 1 , '测试', NULL, '', NULL);  
INSERT INTO sys_resource (res_code, res_name, symbol, symbol_name, res_type , icon_path, forward, sort_no, p_id, depth , is_leaf, is_public, `status`, is_display, version , create_at, create_by, create_by_name, update_at, update_by , update_by_name, ts, name_key, abbreviation) VALUES ('ibms_doorMagnet_update', '编辑', '6', 'ibms', '12' , NULL, '', 2, 1340935650907525203, 2 , 1, 0, 1, 1, 0 , now(), 1, '测试', now(), 1 , '测试', NULL, '', NULL);  
INSERT INTO sys_resource (res_code, res_name, symbol, symbol_name, res_type , icon_path, forward, sort_no, p_id, depth , is_leaf, is_public, `status`, is_display, version , create_at, create_by, create_by_name, update_at, update_by , update_by_name, ts, name_key, abbreviation) VALUES ('ibms_doorMagnet_query', '查询', '6', 'ibms', '12' , NULL, '', 2, 1340935650907525203, 2 , 1, 0, 1, 1, 0 , now(), 1, '测试', now(), 1 , '测试', NULL, '', NULL);  

INSERT INTO sys_resource (res_code, res_name, symbol, symbol_name, res_type , icon_path, forward, sort_no, p_id, depth , is_leaf, is_public, `status`, is_display, version , create_at, create_by, create_by_name, update_at, update_by , update_by_name, ts, name_key, abbreviation) VALUES ('ibms_appUpgrade_add', '新建', '6', 'ibms', '12' , NULL, '', 2, 1340935650907525204, 2 , 1, 0, 1, 1, 0 , now(), 1, '测试', now(), 1 , '测试', NULL, '', NULL);
INSERT INTO sys_resource (res_code, res_name, symbol, symbol_name, res_type , icon_path, forward, sort_no, p_id, depth , is_leaf, is_public, `status`, is_display, version , create_at, create_by, create_by_name, update_at, update_by , update_by_name, ts, name_key, abbreviation) VALUES ('ibms_appUpgrade_delete', '删除', '6', 'ibms', '12' , NULL, '', 2, 1340935650907525204, 2 , 1, 0, 1, 1, 0 , now(), 1, '测试', now(), 1 , '测试', NULL, '', NULL);  
INSERT INTO sys_resource (res_code, res_name, symbol, symbol_name, res_type , icon_path, forward, sort_no, p_id, depth , is_leaf, is_public, `status`, is_display, version , create_at, create_by, create_by_name, update_at, update_by , update_by_name, ts, name_key, abbreviation) VALUES ('ibms_appUpgrade_update', '编辑', '6', 'ibms', '12' , NULL, '', 2, 1340935650907525204, 2 , 1, 0, 1, 1, 0 , now(), 1, '测试', now(), 1 , '测试', NULL, '', NULL);  
INSERT INTO sys_resource (res_code, res_name, symbol, symbol_name, res_type , icon_path, forward, sort_no, p_id, depth , is_leaf, is_public, `status`, is_display, version , create_at, create_by, create_by_name, update_at, update_by , update_by_name, ts, name_key, abbreviation) VALUES ('ibms_appUpgrade_query', '查询', '6', 'ibms', '12' , NULL, '', 2, 1340935650907525204, 2 , 1, 0, 1, 1, 0 , now(), 1, '测试', now(), 1 , '测试', NULL, '', NULL);  

INSERT INTO `sys_role_res` (role_id, res_id, `STATUS`, version, create_at , create_by, create_by_name, update_at, update_by, update_by_name , ts) VALUES ('1363755151856242689', '1340935650907525203', 1, 0, NOW() , 1, '测试', now(), 1, '测试' , NULL);
INSERT INTO `sys_role_res` (role_id, res_id, `STATUS`, version, create_at , create_by, create_by_name, update_at, update_by, update_by_name , ts) VALUES ('1363755151856242689', '1340935650907525204', 1, 0, NOW() , 1, '测试', now(), 1, '测试' , NULL);
