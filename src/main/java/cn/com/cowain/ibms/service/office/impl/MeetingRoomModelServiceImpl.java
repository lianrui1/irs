package cn.com.cowain.ibms.service.office.impl;

import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.dao.meeting.IntelligentMeetingAppDao;
import cn.com.cowain.ibms.dao.meeting.IntelligentMeetingAppModeDao;
import cn.com.cowain.ibms.dao.meeting.IntelligentMeetingModeStatusDao;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.meeting.IntelligentMeetingApp;
import cn.com.cowain.ibms.entity.meeting.IntelligentMeetingAppMode;
import cn.com.cowain.ibms.entity.meeting.IntelligentMeetingAppModeStatus;
import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.rest.req.working_mode.ModeChangeReq;
import cn.com.cowain.ibms.rest.req.working_mode.WebDeviceModeStatusSaveReq;
import cn.com.cowain.ibms.rest.req.working_mode.WebWorkingModeSaveReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.device.TbDataResp;
import cn.com.cowain.ibms.rest.resp.office.TbDataDetailResp;
import cn.com.cowain.ibms.rest.resp.working_mode.ScreenDeviceModeStatusDetailResp;
import cn.com.cowain.ibms.rest.resp.working_mode.ScreenWorkingModeDetailResp;
import cn.com.cowain.ibms.rest.resp.working_mode.WebWorkingModePageResp;
import cn.com.cowain.ibms.service.bean.DoorMagneticHWParasReq;
import cn.com.cowain.ibms.service.bean.DoorMagneticHWReq;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.device.HwProductsService;
import cn.com.cowain.ibms.service.office.MeetingRoomModelService;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author feng
 * @title: MeetingRoomServiceImpl
 * @projectName ibms
 * @Date 2021/12/29 9:34
 */
@Slf4j
@Service
@Transactional
public class MeetingRoomModelServiceImpl implements MeetingRoomModelService {

    @Resource
    private IntelligentMeetingModeStatusDao intelligentMeetingModeStatusDao;
    @Resource
    private IntelligentMeetingAppDao intelligentMeetingAppDao;

    @Resource
    private IntelligentMeetingAppModeDao intelligentMeetingAppModeDao;

    @Resource
    private DeviceDao deviceDao;

    @Resource
    private HwProductsService productsService;

    @Override

    public ServiceResult save(WebWorkingModeSaveReq req, BindingResult bindingResult) {
        //查看有没有同名的
        Optional<IntelligentMeetingAppMode> mode = intelligentMeetingAppModeDao.findByName(req.getName());
        long x = intelligentMeetingAppModeDao.count();

        if (mode.isPresent() && x >= 4) {
            return ServiceResult.error("当前模式已经有 " + x + " 条，总条数不能超过4条");
        }

        if (mode.isPresent()) {
            return ServiceResult.error("已经存在名字为:" + req.getName() + " 的模式，请变更名字");
        }

        IntelligentMeetingAppMode workingMode = new IntelligentMeetingAppMode();
        workingMode.setName(req.getName());
        workingMode.setImg(req.getImgUrl());
        workingMode.setPurpose(req.getPurpose());
        // 保存
        intelligentMeetingAppModeDao.save(workingMode);
        for (WebDeviceModeStatusSaveReq modeStatusSaveReq : req.getSaveReqs()) {
            IntelligentMeetingAppModeStatus modeStatus = new IntelligentMeetingAppModeStatus();
            modeStatus.setDeviceType(modeStatusSaveReq.getDeviceType());
            modeStatus.setProperties(JSON.toJSONString(modeStatusSaveReq));
            modeStatus.setMode(workingMode);
            intelligentMeetingModeStatusDao.save(modeStatus);
        }
        // 返回主键ID
        IdResp idResp = new IdResp();
        idResp.setId(workingMode.getId());
        return ServiceResult.ok(idResp);
    }

    @Override
    public ServiceResult findDetail(String modelId) {
        Optional<IntelligentMeetingAppMode> workingModeOp = intelligentMeetingAppModeDao.findById(modelId);
        if (workingModeOp.isEmpty()) {
            return ServiceResult.error("id有误");
        }
        IntelligentMeetingAppMode workingMode = workingModeOp.get();
        // 获取模式下设备状态
        List<ScreenDeviceModeStatusDetailResp> detailResps = new ArrayList<>();
        intelligentMeetingModeStatusDao.findByModeId(modelId).forEach(it -> {
            ScreenDeviceModeStatusDetailResp resp = new ScreenDeviceModeStatusDetailResp();
            if (StringUtils.isNotBlank(it.getProperties())) {
                 resp = JSON.parseObject(it.getProperties(), ScreenDeviceModeStatusDetailResp.class);
                resp.setDeviceName(resp.getDeviceType().getName());
            }
            detailResps.add(resp);
        });
        ScreenWorkingModeDetailResp resp = new ScreenWorkingModeDetailResp();
        resp.setId(workingMode.getId());
        resp.setName(workingMode.getName());
        resp.setPurpose(workingMode.getPurpose());
        resp.setImgUrl(workingMode.getImg());
        resp.setDetailResps(detailResps);
        return ServiceResult.ok(resp);
    }


    @Override
    public ServiceResult update(String id, WebWorkingModeSaveReq req) {
        Optional<IntelligentMeetingAppMode> mode = intelligentMeetingAppModeDao.findById(id);
        if (mode.isEmpty()) {
            return ServiceResult.error("未查到该模式，请重新选择");
        }
        Optional<IntelligentMeetingAppMode> temp = intelligentMeetingAppModeDao.findByName(req.getName());
        if (temp.isPresent() && !temp.get().getId().equals(id)) {
            return ServiceResult.error("有重复的名字，请修正模式名称");
        }
        IntelligentMeetingAppMode deviceWorkingMode = mode.get();
        deviceWorkingMode.setId(id);
        deviceWorkingMode.setImg(req.getImgUrl());
        deviceWorkingMode.setName(req.getName());
        deviceWorkingMode.setPurpose(req.getPurpose());
        // 更新模式下设备
        List<IntelligentMeetingAppModeStatus> list = new ArrayList<>();
        intelligentMeetingModeStatusDao.findByModeId(id).forEach(it -> {
            it.setIsDelete(1);
            list.add(it);
        });
        // 保存模式下对应设备
        for (WebDeviceModeStatusSaveReq modeStatusSaveReq : req.getSaveReqs()) {
            // 设备状态
            IntelligentMeetingAppModeStatus modeStatus = new IntelligentMeetingAppModeStatus();
            modeStatus.setProperties(JSON.toJSONString(modeStatusSaveReq));
            modeStatus.setDeviceType(modeStatusSaveReq.getDeviceType());
            modeStatus.setMode(deviceWorkingMode);
            list.add(modeStatus);
        }
        intelligentMeetingModeStatusDao.saveAll(list);
        intelligentMeetingAppModeDao.save(deviceWorkingMode);
        return ServiceResult.ok();
    }


    @Override
    public List<WebWorkingModePageResp> getModelList() {
        List<IntelligentMeetingAppMode> all = intelligentMeetingAppModeDao.findAll();
        List<WebWorkingModePageResp> list = new ArrayList<>();
        all.forEach(it -> {
            WebWorkingModePageResp resp = new WebWorkingModePageResp();
            resp.setId(it.getId());
            resp.setImgUrl(it.getImg());
            resp.setName(it.getName());
            resp.setPurpose(it.getPurpose());
            resp.setCreateTime(it.getCreatedTime());
            list.add(resp);
        });
        return list;
    }

    @Override
    public ServiceResult delete(String id) {
        Optional<IntelligentMeetingAppMode> workingModeOp = intelligentMeetingAppModeDao.findById(id);
        if (workingModeOp.isEmpty()) {
            return ServiceResult.error("id有误");
        }
        IntelligentMeetingAppMode deviceWorkingMode = workingModeOp.get();
        // 删除模式下设备状态
        intelligentMeetingModeStatusDao.findByModeId(id).forEach(it -> {
            it.setIsDelete(1);
            intelligentMeetingModeStatusDao.save(it);
        });
        deviceWorkingMode.setIsDelete(1);
        intelligentMeetingAppModeDao.save(deviceWorkingMode);

        return ServiceResult.ok();
    }

    //控制场景
    @Override
    public ServiceResult changeMode(ModeChangeReq req) {
        Optional<IntelligentMeetingAppMode> workingModeOp = intelligentMeetingAppModeDao.findById(req.getModeId());
        if (workingModeOp.isEmpty()) {
            return ServiceResult.error("控制失败,模式不存在");
        }
        Optional<IntelligentMeetingApp> app = intelligentMeetingAppDao.findById(req.getAppId());
        if (app.isEmpty()) {
            return ServiceResult.error("未找到该应用！");
        }
        List<IotDevice> devices = deviceDao.findAllBySpaceId(app.get().getRoom().getSpace().getId());
        // 循环控制设备
        String errorMsg = "";
        List<IntelligentMeetingAppModeStatus> list = intelligentMeetingModeStatusDao.findByModeId(req.getModeId());
        if (CollectionUtils.isEmpty(list)) {
            return ServiceResult.error("控制失败,模式对应的设置不存在");
        }
        for (IotDevice it : devices) {
            if(DeviceType.DOOR_PLATE.equals(it.getDeviceType())||!it.getSpaceControl().equals(1)){
                continue;
            }

            // 获取设备所属产品的服务ID和命令名称
            ServiceResult serviceResult = productsService.findProductProperties(it);
            DoorMagneticHWReq hwReq = (DoorMagneticHWReq) serviceResult.getObject();

            DoorMagneticHWParasReq paras = null;
            for (IntelligentMeetingAppModeStatus appModeStatus : list) {
                if (appModeStatus.getDeviceType().equals(it.getDeviceType())) {
                    paras = JSON.parseObject(appModeStatus.getProperties(), DoorMagneticHWParasReq.class);
                }
            }

            hwReq.setDeviceId(it.getId());
            hwReq.setDevice_id(it.getHwDeviceId());
            if(paras==null){
                continue;
            }
            if (!DeviceStatus.ONLINE.equals(it.getHwStatus())) {
                errorMsg += it.getHwDeviceName() + ",";
                continue;
            }
            hwReq.setParas(paras);
            ServiceResult result;
            if(it.getDeviceType().equals(DeviceType.FOUR_WAY_SWITCH)||it.getDeviceType().equals(DeviceType.CIRCUIT_BREAKER)){
                // 控制四路开关
                result=  productsService.updateSwitch( hwReq,  "001",  "会议大屏");
            }else {
                result = productsService.command(hwReq, app.get().getRoom().getName(), "会议大屏");
            }
            if (!result.isSuccess()) {
                errorMsg += it.getHwDeviceName() + ",";
                continue;
            }

        }
        if (StringUtils.isNotBlank(errorMsg)) {
            return ServiceResult.error("控制失败设备" + errorMsg.substring(0, errorMsg.length() - 1));
        }
        return ServiceResult.ok();
    }

    @Override
    public ServiceResult getSensorData(String appId) {
        Optional<IntelligentMeetingApp> meetingAppOpt = intelligentMeetingAppDao.findById(appId);
        if (meetingAppOpt.isEmpty()) {
            return ServiceResult.error("未查到会议室");
        }
        List<IotDevice> iotDevices = deviceDao.findAllBySpaceId(meetingAppOpt.get().getRoom().getSpace().getId());
        if (iotDevices.isEmpty()) {
            return ServiceResult.error("未查到传感器信息");
        }
        TbDataDetailResp resp = new TbDataDetailResp();
        for (IotDevice tdDevice : iotDevices) {
            if (tdDevice.getDeviceType().equals(DeviceType.NOISE_SENSOR)
                    || tdDevice.getDeviceType().equals(DeviceType.PM_SENSOR)
                    || tdDevice.getDeviceType().equals(DeviceType.TEMPERATURE_AND_HUMIDITY_SENSOR)
                    || tdDevice.getDeviceType().equals(DeviceType.TEMPERATURE_AND_HUMIDITY_AND_ILLUMINATION_SENSOR)
                    || tdDevice.getDeviceType().equals(DeviceType.ILLUMINATION_SENSING)) {
                String tbId = productsService.findTbId(tdDevice.getHwDeviceId());
                if (tbId == null) {
                    log.error("查询失败 设备id有误" + tdDevice.getHwDeviceId());
                    return ServiceResult.error("传感器数据异常，请查传感器：" + tdDevice.getDeviceType().getName());
                }
                List<TbDataResp> ill = productsService.findNowData(tbId);
                if (com.baomidou.mybatisplus.core.toolkit.CollectionUtils.isNotEmpty(ill)) {
                    TbDataResp illData = ill.get(0);
                    resp.putValue(illData);
                }
                resp.setAllDisable(Boolean.TRUE);
            }
        }
        return ServiceResult.ok(resp);
    }

}
