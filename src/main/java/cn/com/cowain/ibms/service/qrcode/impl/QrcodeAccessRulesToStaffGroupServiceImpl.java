package cn.com.cowain.ibms.service.qrcode.impl;

import cn.com.cowain.ibms.dao.qrcode.QrcodeAccessRuleToStaffGroupDao;
import cn.com.cowain.ibms.entity.distinguish.StaffGroup;
import cn.com.cowain.ibms.entity.qrcode.QrcodeAccessRules;
import cn.com.cowain.ibms.entity.qrcode.QrcodeAccessRulesToStaffGroup;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.qrcode.QrcodeAccessRulesToStaffGroupService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/4/7 10:34
 */
@Service
public class QrcodeAccessRulesToStaffGroupServiceImpl implements QrcodeAccessRulesToStaffGroupService {

    @Resource
    private QrcodeAccessRuleToStaffGroupDao qrcodeAccessRuleToStaffGroupDao;

    /**
     * 保存权限与用户组关系
     *
     * @param qrCodeAccessRules     二维码权限
     * @param staffGroupCollections 用户组
     * @return 操作结果
     * @author Yang.Lee
     * @date 2021/4/7 10:32
     **/
    @Override
    @Transactional
    public void saveGroupIdsByQrcodeRule(@NotNull QrcodeAccessRules qrCodeAccessRules, @NotNull Collection<StaffGroup> staffGroupCollections) {

        List<QrcodeAccessRulesToStaffGroup> access2GroupList = new ArrayList<>();
        staffGroupCollections.forEach(group -> {

            QrcodeAccessRulesToStaffGroup a2s = new QrcodeAccessRulesToStaffGroup();
            a2s.setRule(qrCodeAccessRules);
            a2s.setGroup(group);
            access2GroupList.add(a2s);

        });
        qrcodeAccessRuleToStaffGroupDao.saveAll(access2GroupList);
    }

    /**
     * 根据权限查询对应关系列表
     *
     * @param ruleId 权限ID
     * @return 权限关系list
     * @author Yang.Lee
     * @date 2021/4/8 10:04
     **/
    @Override
    @Transactional
    public List<QrcodeAccessRulesToStaffGroup> getListByRules(String ruleId) {

        return qrcodeAccessRuleToStaffGroupDao.findByRuleId(ruleId);
    }

    /**
     * 根据门磁id查询关系列表
     *
     * @param doorMagneticId 门磁ID
     * @return 权限关系list
     * @author Yang.Lee
     * @date 2021/4/9 9:19
     **/
    @Override
    public List<QrcodeAccessRulesToStaffGroup> getListByDoorMagneticId(String doorMagneticId) {

        return qrcodeAccessRuleToStaffGroupDao.findByRuleDoorMagneticId(doorMagneticId);
    }

    /**
     * 根据权限写入关系
     *
     * @param rule     通行权限
     * @param groups 人员组id列表
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/3/10 15:26
     **/
    @Override
    @Transactional
    public ServiceResult saveGroupIdsByRule(@NotNull QrcodeAccessRules rule, @NotNull List<StaffGroup> groups) {
        List<QrcodeAccessRulesToStaffGroup> access2GroupList = new ArrayList<>();
        groups.forEach(group -> {

            QrcodeAccessRulesToStaffGroup a2s = new QrcodeAccessRulesToStaffGroup();
            a2s.setRule(rule);
            a2s.setGroup(group);
            access2GroupList.add(a2s);

        });
        qrcodeAccessRuleToStaffGroupDao.saveAll(access2GroupList);
        return ServiceResult.ok();
    }

    /**
     * 删除权限关系
     *
     * @param qrcodeAccessRulesToStaffGroups 权限关系
     * @return 操作结果
     * @author Yang.Lee
     * @date 2021/5/6 10:27
     **/
    @Override
    @Transactional
    public ServiceResult deleteQrcodeAccessRulesToStaffGroup(@NotEmpty QrcodeAccessRulesToStaffGroup... qrcodeAccessRulesToStaffGroups) {

        for(QrcodeAccessRulesToStaffGroup relation : qrcodeAccessRulesToStaffGroups){
            relation.setIsDelete(1);
            qrcodeAccessRuleToStaffGroupDao.save(relation);

        }

        return ServiceResult.ok();
    }

    /**
     * 根据人员组Id查询关系列表
     *
     * @param staffGroupId 人员组id
     * @return 关系列表
     */
    @Override
    @Transactional
    public List<QrcodeAccessRulesToStaffGroup> getListByStaffGroupId(String staffGroupId) {

        return qrcodeAccessRuleToStaffGroupDao.findByGroupId(staffGroupId);
    }
}
