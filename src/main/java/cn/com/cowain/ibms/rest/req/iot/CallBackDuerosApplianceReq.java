package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Author tql
 * @Description dueros回调
 * @Date 22-9-26 下午3:21
 * @Version 1.0
 */
@Data
@ApiModel("回调具体信息")
public class CallBackDuerosApplianceReq {


    @ApiModelProperty("回调具体信息")
    @NotBlank
    private additionalApplianceDetails additionalApplianceDetails;

    @ApiModelProperty("回调具体信息")
    @NotBlank
    private String applianceId;

    @Data
    @ApiModel("控制房间信息")
    private static class additionalApplianceDetails {
        @ApiModelProperty("商品id")
        @NotBlank
        private String shopId;


        @ApiModelProperty("房间id")
        @NotBlank
        private String roomId;

        @ApiModelProperty("组id")
        @NotBlank
        private String groupId;

        @ApiModelProperty("设备id")
        @NotBlank
        private String deviceId;
    }
}
