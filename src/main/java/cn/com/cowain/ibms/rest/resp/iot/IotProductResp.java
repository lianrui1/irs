package cn.com.cowain.ibms.rest.resp.iot;

import cn.com.cowain.ibms.enumeration.IotNodeType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author yuchunlei
 * @version 1.0.0
 * @createTime 2020年12月17日 17:06:00
 */
@Data
public class IotProductResp implements Serializable {
    private static final long serialVersionUID = -217797871158296671L;


    @ApiModelProperty(value = "iot产品id", required = true, position = 0)
    private String id;

    @ApiModelProperty(value = "iot产品名称", required = true, position = 1)
    private String name;

    @ApiModelProperty(value = "IOT网络节点 类型", required = true, position = 2)
    private IotNodeType iotNodeType;

    @ApiModelProperty(value = "IOT网络节点 类型名称", required = true, position = 3)
    private String iotNodeTypeName;

    @ApiModelProperty(value = "数据点", required = true, position = 4)
    private List<String> iotAttributes;

    @ApiModelProperty(value = "创建时间", required = true, position = 5)
    private String createTime;

    @ApiModelProperty(value = "产品编号", required = true, position = 6)
    private String productNo;

    @ApiModelProperty(value = "iot产品名称备注", required = true, position = 7)
    private String remark;

    @ApiModelProperty(value = "iot产品名称En", required = true, position = 8)
    private String nameEn;
}
