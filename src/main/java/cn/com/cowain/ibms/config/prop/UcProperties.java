package cn.com.cowain.ibms.config.prop;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author wei.cheng
 * @date 2022/10/08 13:09
 */
@ConfigurationProperties(prefix = "service.uc")
@Configuration
@Data
@Slf4j
public class UcProperties {

    /**
     * 授权码换token的地址
     */
    private String accessTokenUrl;

    /**
     * 员工信息接口
     */
    private String hrInfoUrl;
}
