package cn.com.cowain.ibms.rest.space;

import cn.com.cowain.ibms.annotation.Idempotent;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.space.ProjectAreaEditReq;
import cn.com.cowain.ibms.rest.req.space.ProjectPageReq;
import cn.com.cowain.ibms.rest.req.space.ProjectReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.space.ProjectAreaResp;
import cn.com.cowain.ibms.rest.resp.space.ProjectResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.space.ProjectService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * 项目controller
 *
 * @author Yang.Lee
 * @date 2020/12/17 17:08
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/project")
@Api(tags = IConst.PROJECT)
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    /**
     * 新增项目
     *
     * @param projectReq    项目信息
     * @param bindingResult
     * @return
     */
    @PostMapping
    @Idempotent(value = API_BASE + "/post/project", expireTime = 2L)
    @ApiOperation(value = "PC新增项目", tags = {IConst.PROJECT, IConst.V19})
    public ResponseEntity<JsonResult<IdResp>> save(@RequestBody @Validated @ApiParam("项目信息") ProjectReq projectReq,
                                                   BindingResult bindingResult) {
        // 保存
        ServiceResult service = projectService.save(projectReq);
        if (!service.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error((String) service.getObject(), null, service.getErrorCode()));
        }
        return ResponseEntity.ok(JsonResult.ok("OK", (IdResp) service.getObject()));
    }

    /**
     * 更新项目信息
     *
     * @param id            项目ID
     * @param projectReq    项目信息
     * @param bindingResult
     * @return
     */
    @PutMapping("/{id}")
    @ApiOperation(value = "PC更新项目信息", tags = {IConst.PROJECT, IConst.V19})
    public ResponseEntity<JsonResult<String>> update(@PathVariable @ApiParam("项目ID") String id,
                                                     @RequestBody @Validated @ApiParam("项目信息") ProjectReq projectReq,
                                                     BindingResult bindingResult) {
        log.debug("update projectReq:" + projectReq);
        ServiceResult serviceResult = projectService.update(id, projectReq);
        JsonResult<String> jsonResult = JsonResult.okNoData("OK");
        if (!serviceResult.isSuccess()) {
            jsonResult = JsonResult.error(serviceResult.getObject().toString(), null, serviceResult.getErrorCode());
        }
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 删除项目
     *
     * @param id 项目ID
     * @return
     * @author Yang.Lee
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "PC删除项目", tags = {IConst.PROJECT, IConst.V19})
    public ResponseEntity<JsonResult<String>> delete(@PathVariable @ApiParam("项目ID") String id) {
        JsonResult<String> jsonResult = JsonResult.okNoData("OK");
        ServiceResult result = projectService.delete(id);

        if (!result.isSuccess()) {
            jsonResult = JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01);
        }

        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 根据ID查询项目信息
     *
     * @param id 项目ID
     * @return
     * @author Yang.Lee
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "PC查询项目信息", tags = {IConst.PROJECT, IConst.V19})
    public ResponseEntity<JsonResult<ProjectResp>> search(@PathVariable @ApiParam("项目ID") String id) {

        ServiceResult result = projectService.get(id);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.ok("ok", (ProjectResp) result.getObject()));
    }

    /**
     * 查询项目列表分页数据
     *
     * @param req 查询参数
     * @return
     * @author Yang.Lee
     */
    @GetMapping("/search/page")
    @ApiOperation(value = "PC查询项目数据(分页)", tags = {IConst.PROJECT, IConst.V19})
    public ResponseEntity<JsonResult<PageBean<ProjectResp>>> searchPage(ProjectPageReq req) {

        // 参数：搜索条件, 分页页码,
        // 数据：数据，页码，总页数，
        PageBean<ProjectResp> pageBean = projectService.search(req);
        JsonResult<PageBean<ProjectResp>> jr = JsonResult.ok("OK", pageBean);
        return ResponseEntity.ok(jr);
    }

    /**
     * 查询所有项目列表
     *
     * @return
     * @author Yang.Lee
     */
    @GetMapping
    @ApiOperation(value = "PC查询所有项目列表", tags = IConst.PROJECT)
    public ResponseEntity<JsonResult<List<ProjectResp>>> searchList() {
        // 查询所有项目列表
        List<ProjectResp> result = projectService.searchProjectList();
        return ResponseEntity.ok(JsonResult.ok("OK", result));
    }

    /**
     * 根据项目名称或者编号模糊查询
     *
     * @param projectName
     * @return
     */
    @GetMapping("/search")
    @ApiOperation(value = "PC查询项目数据", tags = IConst.PROJECT)
    @ApiImplicitParam(name = "projectName", value = "模糊查询关键字，项目名称或项目编号")
    public ResponseEntity<JsonResult<List<ProjectResp>>> searchProjectList(
            @RequestParam(value = "projectName") String projectName) {
        //判断字符串中是否含有特殊字符
        // 目标字符串
        String regex = "^[0-9A-Za-z\u4e00-\u9fa5]+$";
        if (projectName.matches(regex) || projectName.isEmpty()) {
            // 查询所有项目列表
            List<ProjectResp> result = projectService.findProjectList(projectName);
            JsonResult<List<ProjectResp>> jsonResult = JsonResult.ok("OK", result);
            return ResponseEntity.ok(jsonResult);
        } else {
            JsonResult<List<ProjectResp>> jsonResult = JsonResult.ok("你输入的内容有误", null);
            return ResponseEntity.ok(jsonResult);
        }
    }

    /**
     * 移动端查询项目区域列表
     *
     * @return
     */
    @GetMapping("/area/list")
    @ApiOperation(value = "APP查询项目区域列表", tags = IConst.PROJECT)
    public ResponseEntity<JsonResult<List<ProjectAreaResp>>> getProjectArea() {
        // 查询所有项目列表
        List<ProjectAreaResp> result = projectService.searchProjectAreaList();
        return ResponseEntity.ok(JsonResult.ok("OK", result));
    }

    /**
     * PC端查询项目区域列表
     *
     * @return
     */
    @GetMapping("/pc/area/list")
    @ApiOperation(value = "PC查询项目区域列表", tags = IConst.PROJECT)
    public ResponseEntity<JsonResult<List<ProjectAreaResp>>> getPcProjectArea() {
        // 查询所有项目列表
        List<ProjectAreaResp> result = projectService.searchPcProjectAreaList();
        return ResponseEntity.ok(JsonResult.ok("OK", result));
    }

    /**
     * 添加区域接口
     *
     * @param req           请求参数
     * @param bindingResult 参数校验对象
     * @return 创建结果，创建成功返回数据主键
     * @author Yang.Lee
     * @date 2021/11/9 9:26
     **/
    @PostMapping("/projectArea")
    @ApiOperation(value = "添加项目区域接口", tags = {IConst.PROJECT, IConst.V19})
    public ResponseEntity<JsonResult<IdResp>> createArea(@RequestBody @Validated ProjectAreaEditReq req, BindingResult bindingResult) {

        //去除首尾的空格
        req.setName(req.getName().trim());

        ServiceResult result = projectService.createProjectArea(req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error((String) result.getObject(), null, result.getErrorCode()));
        }
        return ResponseEntity.ok(JsonResult.ok("OK", (IdResp) result.getObject()));
    }

    /**
     * 查询区域接口
     *
     * @return 区域列表
     * @author Yang.Lee
     * @date 2021/11/9 9:26
     **/
    @GetMapping("/projectArea/list")
    @ApiOperation(value = "获取所有项目区域", tags = {IConst.PROJECT, IConst.V19})
    public ResponseEntity<JsonResult<List<ProjectAreaResp>>> getArea() {


        return ResponseEntity.ok(JsonResult.ok("OK", projectService.getProjectArea()));
    }
}
