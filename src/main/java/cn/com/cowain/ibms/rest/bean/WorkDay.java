package cn.com.cowain.ibms.rest.bean;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @author wei.cheng
 * @date 2022/09/28 15:41
 */
@Data
public class WorkDay implements Serializable {
    /**
     * 星期几，可选值：1-7
     */
    @NotNull
    @Min(value = 1)
    @Max(value = 7)
    private Integer week;
    /**
     * 时间段
     */
    @NotEmpty
    @Valid
    private List<During> durings;
}
