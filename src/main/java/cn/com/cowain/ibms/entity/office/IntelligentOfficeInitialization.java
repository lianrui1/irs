package cn.com.cowain.ibms.entity.office;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author Yang.Lee
 * @date 2021/12/7 13:34
 */
@Data
@Entity
@Table(name = "ibms_intelligent_office_initialization",indexes = {@Index(columnList = "user_hr_id,office_id")})
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "ibms_intelligent_office_initialization", comment = "智能办公室初始化记录表")
public class IntelligentOfficeInitialization extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "office_id", nullable = false, columnDefinition = "varchar(36) COMMENT '智能办公室'")
    private IntelligentOffice intelligentOffice;

    @Column(name = "user_hr_id", nullable = false, columnDefinition = "varchar(20) COMMENT '用户工号'")
    private String userHrId;

    @Column(name = "user_name", nullable = false, columnDefinition = "varchar(20) COMMENT '用户姓名'")
    private String userName;

    @Column(name = "alisa", columnDefinition = "varchar(50) COMMENT '办公室别名'")
    private String alisa;
}
