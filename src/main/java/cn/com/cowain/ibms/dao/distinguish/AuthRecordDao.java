package cn.com.cowain.ibms.dao.distinguish;

import cn.com.cowain.ibms.entity.distinguish.AuthRecord;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author wei.cheng
 * @date 2022/03/28 10:23
 */
public interface AuthRecordDao extends JpaRepository<AuthRecord, String> {
    /**
     * 查询createdTime > {@code createdTime}的AuthRecord列表
     *
     * @param createdTime
     * @return
     */
    List<AuthRecord> findAllByCreatedTimeAfterOrderByCreatedTimeAsc(LocalDateTime createdTime);
}
