package cn.com.cowain.ibms.rest.iot.lift_column;

import cn.com.cowain.ibms.rest.req.iot.LiftColumnControlReq;
import cn.com.cowain.ibms.rest.resp.BaseListResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.iot.lift_column.LiftColumnService;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.ibms.utils.TokenUtils;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.sfp.comm.utils.JwtUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * 升降柱相关controller
 *
 * @author Yang.Lee
 * @date 2022/6/24 10:21
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/liftColumn")
@Api(tags = IConst.MODULE_LIFT_COLUMN)
public class LiftColumnController {

    @Resource
    private LiftColumnService liftColumnService;

    /**
     * 获取停车场列表
     *
     * @return 停车场列表，返回停车场ID及名称
     * @author Yang.Lee
     * @date 2022/6/24 10:29
     **/
    @GetMapping("/park/list")
    @ApiOperation(value = "获取停车场列表", tags = {IConst.MODULE_LIFT_COLUMN})
    public ResponseEntity<JsonResult<List<BaseListResp>>> getLiftColumnGroupList() {

        return ResponseEntity.ok(JsonResult.ok(liftColumnService.getParkList()));
    }

    /**
     * 控制升降柱
     *
     * @param token  token
     * @param parkId 停车场ID
     * @param req    控制参数
     * @return 控制结果
     * @author Yang.Lee
     * @date 2022/6/24 10:36
     **/
        @PutMapping("/control/{parkId}")
    @ApiOperation(value = "控制升降柱", tags = {IConst.MODULE_LIFT_COLUMN})
    public ResponseEntity<JsonResult<String>> control(@RequestHeader("Authorization") String token,
                                                      @PathVariable @ApiParam(value = "需要控制升降柱的停车场ID") String parkId,
                                                      @RequestBody LiftColumnControlReq req) {

        log.debug("开始控制升降柱 token{}, parkID {}, req {}" , token, parkId, req);

        String userHrId = JwtUtil.getHrId(TokenUtils.getToken());

        ServiceResult result = liftColumnService.control(userHrId, parkId, req.getCmd());
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, result.getErrorCode()));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }
}
