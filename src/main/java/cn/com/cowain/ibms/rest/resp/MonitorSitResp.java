package cn.com.cowain.ibms.rest.resp;

import cn.com.cowain.ibms.entity.iot.VideoMonitorSpot;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author feng
 * @title: MonitorSit
 * @projectName bims
 * @Date 2021/11/18 10:55
 */
@Data
public class MonitorSitResp  implements Serializable {

    public MonitorSitResp (){}
    public MonitorSitResp(VideoMonitorSpot videoMonitorSpot){
        this.id=videoMonitorSpot.getId();
        this.monitorCode=videoMonitorSpot.getDevice().getSn();
        this.monitorId=videoMonitorSpot.getDevice().getId();
        this.siteName=videoMonitorSpot.getName();
        this.location=videoMonitorSpot.getSpace().getName();
        this.preciseLocation=videoMonitorSpot.getAddress();
        this.monitorName=videoMonitorSpot.getDevice().getDeviceName();
        this.spaceId=videoMonitorSpot.getSpace().getId();
    }

    @ApiModelProperty("id")
    String id;
    @ApiModelProperty("摄像机Id")
    String monitorId;
    @ApiModelProperty("摄像机code")
    String monitorCode;
    /**
    　　* @title: 监控点名称
    　　* @description
    　　* @author jfsui
    　　* @date 2021/11/18 10:59
    　　*/
    @ApiModelProperty("监控点名称")
    String siteName;
    /**
    　　* @title: 所在空间位置
    　　* @description
    　　* @author jfsui
    　　* @date 2021/11/18 10:59
    　　*/
    @ApiModelProperty("所在空间位置")
    String location;
    /**
    　　* @title: 具体位置
    　　* @description
    　　* @author jfsui
    　　* @date 2021/11/18 10:58
    　　*/
    @ApiModelProperty("具体位置")
    String preciseLocation;
    @ApiModelProperty("设备名称")
    String monitorName;
    @ApiModelProperty("空间位置Id")
    String spaceId;

    String token;
}
