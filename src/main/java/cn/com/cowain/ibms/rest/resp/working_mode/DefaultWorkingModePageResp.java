package cn.com.cowain.ibms.rest.resp.working_mode;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/12/21 16:01
 */
@Data
@ApiModel("推荐场景列表响应对象")
public class DefaultWorkingModePageResp {

    @ApiModelProperty(value = "场景名称id", required = true)
    private String id;

    @ApiModelProperty(value = "场景名称图标", required = true)
    private String imgUrl;

    @ApiModelProperty(value = "场景名称", required = true)
    private String name;

    @ApiModelProperty(value = "用途")
    private String purpose;

    @ApiModelProperty(value = "是否添加")
    private boolean add;
}
