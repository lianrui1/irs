package cn.com.cowain.ibms.enumeration.iot;

/**
 * SpaceType
 *
 * @author cuiEnming
 * @date 2020/11/19  18:52
 * @since s2
 */
public enum SpaceType {

    /**
     * 默认
     */
    DEFAULT("默认"),
    
    /**
     * 房间
     */
    ROOM("房间"),

    /**
     * 建筑
     */
    BUILDING("建筑"),

    /**
     * 场地
     */
    GROUND("场地"),

    FLOOR("楼层"),

    GREEN_SPACE("绿化"),

   OTHERS("其他");


    SpaceType (String name){
        this.name = name;
    }
    String name;

    public String getName(){
        return this.name;
    }

}
