package cn.com.cowain.ibms.rest.req.device;

import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 硬件清单分页请求对象
 *
 * @author Yang.Lee
 * @date 2021/3/17 18:04
 */
@Data
@ApiModel("硬件清单分页请求")
public class HardwarePageReq extends PageReq {

    @ApiModelProperty(value = "硬件SN",  example = "123456", position = 3)
    private String hardwareSN;

    @ApiModelProperty(value = "设备名称，支持中英文名称查询",  example = "设备 / Sensor", position = 4)
    private String deviceName;

    @ApiModelProperty(value = "产品ID",  example = "ABC", position = 4)
    private String productId;
}
