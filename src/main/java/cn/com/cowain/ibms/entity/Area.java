package cn.com.cowain.ibms.entity;

import cn.com.cowain.ibms.enumeration.space.AreaType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 地区字典数据模型实体
 * @author Yang.Lee
 * @date 2020/12/17 16:26
 */
@Data
@Entity
@Table(name = "ibms_area")
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "ibms_area", comment = "地区数据")
public class Area{

    /**
     * 主键ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, columnDefinition = "int(10) COMMENT '主键'")
    private Long id;



    /**
     * 创建时间
     */
    @CreatedDate
    @Column(name = "created_time", updatable = false, columnDefinition = "datetime COMMENT '创建时间'")
    private LocalDateTime createdTime;

    /**
     * 创建人
     */
    @Column(name = "created_by", updatable = false, columnDefinition = "varchar(36) COMMENT '创建人'")
    private String createdBy;

    @CreatedDate
    @Column(name = "updated_time", columnDefinition = "datetime COMMENT '更新时间'")
    private LocalDateTime updatedTime;

    @Column(name = "updated_by", columnDefinition = "varchar(36) COMMENT '更新人'")
    private String updatedBy;

    /**
     * 版本号
     */
    @Version
    @JsonIgnore
    @Column(name = "version", columnDefinition = "integer  COMMENT '版本号'")
    private int version;

    /**
     * 是否已删除 0:否 1:是
     */
    @TableLogic
    @Column(name = "is_del", nullable = false, columnDefinition = "tinyint(1)  COMMENT '是否已删除,0:否;1:是'")
    @TableField("is_del")
    @JsonIgnore
    private Integer isDelete = 0;

    /**
     * 地址名称;省、市、区域、小区、楼号、单元、楼层
     */
    @Column(name = "name", nullable = false, columnDefinition = "varchar(20) COMMENT '名称'")
    private String name;

    /**
     * 行政编码
     */
    @Column(name = "code", nullable = false, columnDefinition = "varchar(20) COMMENT '行政编码'")
    private String code;

    /**
     * 区域类型
     */
    @Column(name = "type", nullable = false, columnDefinition = "varchar(10) COMMENT '区域类型'")
    @Enumerated(EnumType.STRING)
    private AreaType type;

    /**
     * 层级标记
     */
    @Column(name = "level", nullable = false, columnDefinition = "int(2) COMMENT '区域层级'")
    private Integer level;

    /**
     * 上一层级Id(for JPA)
     */
    @ManyToOne
    @JoinColumn(name = "parent_id", columnDefinition = "int(10) COMMENT '上级id'")
    private Area parent;
}
