package cn.com.cowain.ibms.exceptions;

/**
 * 高德地图api调用失败异常
 *
 * @author Yang.Lee
 * @date 2021/4/19 15:49
 */
public class AMapApiFailedException extends RuntimeException{

}
