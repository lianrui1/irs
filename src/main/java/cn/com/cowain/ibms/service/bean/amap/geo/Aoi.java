package cn.com.cowain.ibms.service.bean.amap.geo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * aoi信息
 * @author Yang.Lee
 * @date 2021/4/20 13:44
 */
@Data
public class Aoi {

    /**
     * 所属 aoi的id
     **/
    private String id;

    /**
     * 所属 aoi 名称
     **/
    private String name;

    /**
     * 所属 aoi 所在区域编码
     **/
    @JSONField(name = "adcode")
    private String adCode;

    /**
     * 所属 aoi 中心点坐标
     **/
    private String location;

    /**
     * 所属aoi点面积, 单位：平方米
     **/
    private String area;

    /**
     * 输入经纬度是否在aoi面之中.0，代表在aoi内,其余整数代表距离AOI的距离
     **/
    private String distance;
}
