package cn.com.cowain.ibms.service.auth;

import cn.com.cowain.ibms.rest.req.iot.AuthApplicationReq;
import cn.com.cowain.ibms.service.bean.ServiceResult;

/**
 * @author wei.cheng
 * @date 2022/06/29 18:24
 */
public interface AuthV1Service {
    /**
     * 查询用户的权限列表
     *
     * @param empNo
     * @return
     */
    ServiceResult getAuthListOfUser(String empNo);

    /**
     * 用户查询权限详情
     *
     * @param id
     * @param empNo
     * @return
     */
    ServiceResult queryAuthInfoByUser(String id, String empNo);

    /**
     * 用户申请权限
     *
     * @param req
     * @return
     */
    ServiceResult authApplication(AuthApplicationReq req);

    /**
     * 用户类型
     *
     * @param uuid
     * @return
     */
    ServiceResult personType (String uuid);
}
