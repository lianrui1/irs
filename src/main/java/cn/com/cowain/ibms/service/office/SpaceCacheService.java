package cn.com.cowain.ibms.service.office;

import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.space.SpaceAdmin;

import java.util.List;

public interface SpaceCacheService {
    void setSpaceInAdminRedis(SpaceAdmin spaceAdmin);

    void initSpace();

    void initSpace(List<SpaceAdmin> spaceAdminList);

    void delSpaceAdmin(List<SpaceAdmin> spaceAdminList);

    void setSpaceInRedis(Space space);

    void delSpace(Space space);

    void delSpaces(List<String> ids);

    void setSpaceParent(Space space);
}
