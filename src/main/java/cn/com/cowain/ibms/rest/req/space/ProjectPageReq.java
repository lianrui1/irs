package cn.com.cowain.ibms.rest.req.space;

import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2021/11/11 15:02
 */
@Data
public class ProjectPageReq extends PageReq {

    @ApiModelProperty(value = "项目名称")
    private String projectName;

    @ApiModelProperty(value = "项目区域id")
    private String projectAreaId;
}
