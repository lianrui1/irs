package cn.com.cowain.ibms.utils;

import cn.com.cowain.ibms.exceptions.HttpRequestException;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Nullable;
import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * restTemplate封装工具类
 */
@Slf4j
@Component
public class RestTemplateUtil {

    @Resource
    private RestTemplate restTemplate;

    /**
     * 发送get请求(响应为文本信息)
     *
     * @param url       请求url
     * @param headerMap 请求头
     * @return 请求结果
     * @throws HttpRequestException 请求异常，响应返回的http status不是2XX
     */
    public String get(String url, Map<String, String> headerMap) throws HttpRequestException {

        return get(url, headerMap, String.class);
    }

    /**
     * 发送get请求(响应为指定对象)
     *
     * @param url       请求url
     * @param headerMap 请求头
     * @param response  响应实体对象
     * @return 请求结果
     * @throws HttpRequestException 请求异常，响应返回的http status不是2XX
     */
    public <T> T get(String url, Map<String, String> headerMap, Class<T> response) throws HttpRequestException {

        return exchange(url, headerMap, null, HttpMethod.GET, response).getBody();
    }

    /**
     * 发送get请求(响应为指定对象)
     *
     * @param url       请求url
     * @param headerMap 请求头
     * @param responseType  响应类型
     * @return 请求结果
     * @throws HttpRequestException 请求异常，响应返回的http status不是2XX
     */
    public <T> T get(String url, Map<String, String> headerMap, ParameterizedTypeReference<T> responseType) throws HttpRequestException {

        return exchange(url, headerMap, null, HttpMethod.GET, responseType).getBody();
    }

    /**
     * 发送get请求(响应为指定对象)
     *
     * @param url       请求url
     * @param responseType  响应类型
     * @return 请求结果
     * @throws HttpRequestException 请求异常，响应返回的http status不是2XX
     */
    public <T> T get(String url, ParameterizedTypeReference<T> responseType) throws HttpRequestException {

        return exchange(url, null, null, HttpMethod.GET, responseType).getBody();
    }

    /**
     * 发送get请求(响应为指定对象)
     *
     * @param url       请求url
     * @param headerMap 请求头
     * @param response  响应实体对象
     * @return 请求结果
     * @throws HttpRequestException 请求异常，响应返回的http status不是2XX
     */
    public <T> ResponseEntity<T> getResponse(String url, Map<String, String> headerMap, Class<T> response) throws HttpRequestException {

        return exchange(url, headerMap, null, HttpMethod.GET, response);
    }

    /**
     * 发送get请求(响应为指定对象)
     *
     * @param url       请求url
     * @param headerMap 请求头
     * @param responseType  响应类型
     * @return 请求结果
     * @throws HttpRequestException 请求异常，响应返回的http status不是2XX
     */
    public <T> ResponseEntity<T> getResponse(String url, Map<String, String> headerMap, ParameterizedTypeReference responseType) throws HttpRequestException {

        return exchange(url, headerMap, null, HttpMethod.GET, responseType);
    }


    /**
     * 发送get请求(响应为文本信息)
     *
     * @param url 请求url
     * @return 请求结果
     * @throws HttpRequestException 请求异常，响应返回的http status不是2XX
     */
    public String get(String url) throws HttpRequestException {

        return get(url, null, String.class);
    }

    /**
     * 发送post请求
     *
     * @param url         请求地址
     * @param headerMap   请求头
     * @param requestBody 请求体
     * @param response    响应类型
     * @return
     * @author Yang.Lee
     * @date 2021/6/21 11:02
     **/
    public <T> T post(String url, @Nullable Map<String, String> headerMap, @Nullable String requestBody, Class<T> response) throws HttpRequestException {

        return exchange(url, headerMap, requestBody, HttpMethod.POST, response).getBody();
    }

    /**
     * 发送post请求
     *
     * @param url       请求地址
     * @param jsonParam 请求参数
     * @param response  响应类型
     * @return
     * @author Yang.Lee
     * @date 2021/6/21 11:15
     **/
    public <T> T jsonPost(String url, String jsonParam, Class<T> response) throws HttpRequestException {

        return post(url, createGeneralJsonHeader(), jsonParam, response);
    }

    public <T> T postForObject(String url, Object requestArg, Class<T> responseType) throws Exception {

        HttpEntity<String> requsetEntity = new HttpEntity<String>(JSON.toJSONString(requestArg), buildBasicJSONHeaders());
        return restTemplate.postForObject(url, requsetEntity, responseType);
    }
    /**
     * 获取一个application/json头
     *
     * @return
     */
    public HttpHeaders buildBasicJSONHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }
    /**
     * 发送http请求
     *
     * @param url         请求url
     * @param headerMap   请求头
     * @param requestBody 请求体
     * @param method      请求方法
     * @param response    响应类型
     * @return 请求响应
     * @author Yang.Lee
     * @date 2021/6/21 10:59
     **/
    public <T, E> ResponseEntity<T> exchange(String url, @Nullable Map<String, String> headerMap, @Nullable E requestBody, HttpMethod method, Class<T> response) throws HttpRequestException {

        HttpHeaders headers = createHttpHeader(headerMap);

        ResponseEntity<T> responseEntity = restTemplate.exchange(url, method, new HttpEntity<>(requestBody, headers), response);

        if (!responseEntity.getStatusCode().is2xxSuccessful()) {
            log.error("发送get请求失败，请求地址：{}，请求结果：{}", url, responseEntity.getBody());
            throw new HttpRequestException(JSON.toJSONString(responseEntity.getBody()));
        }

        return responseEntity;
    }

    /**
     * 发送http请求
     *
     * @param url          请求url
     * @param headerMap    请求头
     * @param requestBody  请求体
     * @param method       请求方法
     * @param responseType 响应类型
     * @return 请求响应
     * @author Yang.Lee
     * @date 2021/6/21 10:59
     **/
    public <T, E> ResponseEntity<T> exchange(String url, @Nullable Map<String, String> headerMap, @Nullable E requestBody, HttpMethod method, ParameterizedTypeReference<T> responseType) throws HttpRequestException {

        HttpHeaders headers = createHttpHeader(headerMap);

        log.debug("开始发送http请求，请求url {}, 请求方法 {}, 请求头 {}, 请求参数 {}", url, method, headers, requestBody);

        ResponseEntity<T> responseEntity = restTemplate.exchange(url, method, new HttpEntity<>(requestBody, headers), responseType);

        log.debug("请求结果： {}", responseEntity);

        if (!responseEntity.getStatusCode().is2xxSuccessful()) {
            log.error("发送get请求失败，请求地址：{}，请求结果：{}", url, responseEntity.getBody());
            throw new HttpRequestException(JSON.toJSONString(responseEntity.getBody()));
        }

        return responseEntity;
    }


    /**
     * 下载文件（get方法）
     *
     * @param url 文件url地址
     * @return 临时文件，使用过后需要删除
     * @author Yang.Lee
     * @date 2022/7/11 18:12
     **/
    public File getFile(String url) throws IOException {

        ResponseEntity<byte[]> responseEntity = exchange(url, null, null, HttpMethod.GET, byte[].class);

        byte[] body = responseEntity.getBody();

        if (body == null || body.length == 0) {
            throw new NullPointerException();
        }

        HttpHeaders headers = responseEntity.getHeaders();

        File file = createFile(body, headers.getContentType());

        return file;
    }

    /**
     * 生成文件，文件类型根据mediaType自动生成
     *
     * @param data      二进制数据
     * @param mediaType 文件类型
     * @return 文件
     * @author Yang.Lee
     * @date 2022/7/11 18:53
     **/
    private File createFile(byte[] data, MediaType mediaType) throws IOException {

        // 生成的是临时文件，如果无扩展名则默认生成.tmp文件
        String fileExt;

        String mediaTypeStr = mediaType == null ? "" : mediaType.toString();
        // 获取文件扩展名
        switch (mediaTypeStr) {
            case MediaType.IMAGE_JPEG_VALUE:
                fileExt = "jpg";
                break;
            case MediaType.IMAGE_PNG_VALUE:
                fileExt = "png";
                break;
            case MediaType.IMAGE_GIF_VALUE:
                fileExt = "gif";
                break;
            default:
                // TODO: 2022/7/11 yang.lee 此处根据需求进行扩展
                fileExt = "";
        }
        log.debug("通过MediaType获取文件扩展名，mediaType : {}, 文件扩展名：{}", mediaTypeStr, fileExt);

        File file = File.createTempFile(UUID.randomUUID().toString(), "." + fileExt);

        // 将数据写入文件对象
        FileUtil.byteToFile(data, file);

        return file;
    }

    /**
     * 生成通用请求json头
     *
     * @return
     * @author Yang.Lee
     * @date 2021/6/21 11:07
     **/
    public static Map<String, String> createGeneralJsonHeader() {

        Map<String, String> header = new HashMap<>();
        header.put("Content-Type", "application/json");

        return header;
    }

    /**
     * 创建请求头
     *
     * @param headerMap
     * @return
     * @author Yang.Lee
     * @date 2022/7/13 17:15
     **/
    private HttpHeaders createHttpHeader(@Nullable Map<String, String> headerMap) {

        HttpHeaders headers = new HttpHeaders();
        if (headerMap != null) {
            headerMap.keySet().forEach(key -> headers.set(key, headerMap.get(key)));
        }

        return headers;
    }
}
