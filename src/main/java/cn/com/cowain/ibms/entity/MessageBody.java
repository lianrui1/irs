package cn.com.cowain.ibms.entity;

import lombok.Data;

/**
 * 推送消息内容
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/3/25 15:21
 */
@Data
public class MessageBody {

    // 标题
    private String title;

    // key1服务类型
    private String type;

    // key2服务状态
    private String depart;

    // key3服务时间
    private String time;

    // 备注
    private String remark;
}
