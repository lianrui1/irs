package cn.com.cowain.ibms.rest.qrcode;

import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.Sorts;
import cn.com.cowain.ibms.rest.req.distinguish.HkFaceRecordPageReq;
import cn.com.cowain.ibms.rest.req.qrcode.QrcodeAccessRulePageReq;
import cn.com.cowain.ibms.rest.req.qrcode.QrcodeAccessRuleReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.qrcode.FaceRecordPageResp;
import cn.com.cowain.ibms.rest.resp.qrcode.QrcodeAccessRuleDetailResp;
import cn.com.cowain.ibms.rest.resp.qrcode.QrcodeAccessRulePageResp;
import cn.com.cowain.ibms.rest.resp.qrcode.UserPageResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.qrcode.QrcodeAccessRuleService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * 二维码通行权限Controller
 *
 * @author Yang.Lee
 * @date 2021/4/6 15:01
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/qrcodeRule")
@Api(tags = IConst.MODULE_QRCODE_ACCESS_RULE)
public class QrcodeAccessRuleController {

    @Resource
    private QrcodeAccessRuleService qrcodeAccessRuleService;

    /**
     * 创建二维码通行权限
     *
     * @param qrcodeAccessRuleReq 请求参数
     * @param bindingResult       参数校验对象
     * @return 请求结果
     * @author Yang.Lee
     * @date 2021/4/6 15:11
     **/
    @PostMapping
    @ApiOperation(value = "创建二维码通行权限", tags = IConst.MODULE_QRCODE_ACCESS_RULE)
    public ResponseEntity<JsonResult<IdResp>> create(@RequestBody @Validated QrcodeAccessRuleReq qrcodeAccessRuleReq, BindingResult bindingResult) {

        JsonResult<IdResp> jsonResult;

        ServiceResult result = qrcodeAccessRuleService.createAccessRule(qrcodeAccessRuleReq);
        if (!result.isSuccess()) {
            jsonResult = JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01);
        } else {
            jsonResult = JsonResult.ok("ok", (IdResp) result.getObject());
        }

        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 更新通行权限
     *
     * @param id                  权限ID
     * @param qrcodeAccessRuleReq 更新参数
     * @param bindingResult       参数校验对象
     * @return 更新结果
     * @author Yang.Lee
     * @date 2021/4/6 15:54
     **/
    @PutMapping("/{id}")
    @ApiOperation(value = "编辑二维码通行权限", tags = IConst.MODULE_QRCODE_ACCESS_RULE)
    public ResponseEntity<JsonResult<String>> update(@PathVariable @ApiParam("权限ID") String id,
                                                     @RequestBody @Validated QrcodeAccessRuleReq qrcodeAccessRuleReq,
                                                     BindingResult bindingResult) {

        JsonResult<String> jsonResult;
        ServiceResult result = qrcodeAccessRuleService.updateAccessRule(id, qrcodeAccessRuleReq);
        if (result.isSuccess()) {
            jsonResult = JsonResult.okNoData("OK");
        } else {
            jsonResult = JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01);
        }

        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 查询通行权限详情
     *
     * @param id 权限ID
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/4/6 16:00
     **/
    @GetMapping("/{id}")
    @ApiOperation(value = "获取权限详情", tags = IConst.MODULE_QRCODE_ACCESS_RULE)
    public ResponseEntity<JsonResult<QrcodeAccessRuleDetailResp>> get(@PathVariable @ApiParam("权限ID") String id) {

        QrcodeAccessRuleDetailResp result = qrcodeAccessRuleService.getAccessRuleDetailByAccessId(id);

        return ResponseEntity.ok(JsonResult.ok("OK", result));
    }

    /**
     * 删除权限
     *
     * @param id 权限ID
     * @return 删除结果
     * @author Yang.Lee
     * @date 2021/4/6 16:03
     **/
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除二维码通行权限", tags = IConst.MODULE_QRCODE_ACCESS_RULE)
    public ResponseEntity<JsonResult<String>> delete(@PathVariable @ApiParam("权限ID") String id) {

        ServiceResult result = qrcodeAccessRuleService.deleteAccessRule(id);

        JsonResult<String> jsonResult;
        if (result.isSuccess()) {
            jsonResult = JsonResult.okNoData("OK");
        } else {
            jsonResult = JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01);
        }

        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 查询通行权限分页数据
     *
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/4/6 16:00
     **/
    @GetMapping("/search/page")
    @ApiOperation(value = "获取权限分页数据", tags = IConst.MODULE_QRCODE_ACCESS_RULE)
    public ResponseEntity<JsonResult<PageBean<QrcodeAccessRulePageResp>>> getPage(QrcodeAccessRulePageReq req) {

        // 添加默认的排序方式：按照创建时间倒叙查询
        req.addOrder(Sorts.getDefaultDesc());

        PageBean<QrcodeAccessRulePageResp> resultPage = qrcodeAccessRuleService.getAccessRulePage(req);
        return ResponseEntity.ok(JsonResult.ok("OK", resultPage));
    }

    /**
     * PC查询门磁人员列表
     *
     * @param name
     * @param ruleId
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/user/page")
    @ApiOperation(value = "PC查询门磁人员列表(分页)", tags = IConst.MODULE_QRCODE_ACCESS_RULE)
    public ResponseEntity<JsonResult<PageBean<UserPageResp>>> hkUserList(
            @RequestParam(required = false) @ApiParam(value = "人员姓名/工号") String name,
            @RequestParam @ApiParam(value = "权限ID", required = true) String ruleId,
            @RequestParam(value = "page", defaultValue = "0") @ApiParam(value = "页码，从0开始，默认0") int page,
            @RequestParam(value = "size", defaultValue = "10") @ApiParam(value = "页长，默认10") int size) {


        PageBean<UserPageResp> pageBean = qrcodeAccessRuleService.userList(name, ruleId, page, size);

        return ResponseEntity.ok(JsonResult.ok("OK", pageBean));
    }

    /**
     * PC查询门磁设备通行记录
     *
     * @param req
     * @return
     */
    @GetMapping("/face/record")
    @ApiOperation(value = "PC查询门磁设备通行记录(分页)", tags = IConst.MODULE_QRCODE_ACCESS_RULE)
    public ResponseEntity<JsonResult<PageBean<FaceRecordPageResp>>> hkFaceRecord(HkFaceRecordPageReq req) {


        PageBean<FaceRecordPageResp> pageBean = qrcodeAccessRuleService.accessRecord(req);
        if (pageBean == null || pageBean.getList() == null) {
            return ResponseEntity.ok(JsonResult.error("设备不存在", null, null));
        }

        return ResponseEntity.ok(JsonResult.ok("OK", pageBean));
    }
}
