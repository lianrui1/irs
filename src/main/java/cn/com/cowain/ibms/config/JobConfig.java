package cn.com.cowain.ibms.config;

import cn.com.cowain.ibms.job.HealthCheckJob;
import cn.com.cowain.ibms.job.ReservationReminderJob;
import cn.com.cowain.ibms.service.MeetingPollService;
import cn.com.cowain.ibms.service.hk.HealthCheckService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/17/20
 */
@Slf4j
@Configuration
@EnableScheduling
@ComponentScan("cn.com.cowain.ibms.job")
public class JobConfig {

    public JobConfig() {
        log.debug("JobConfig()..");
    }

    @Bean
    public ReservationReminderJob reservationReminderJob(MeetingPollService meetingPollService) {
        return new ReservationReminderJob(meetingPollService);
    }

    @Bean
    public HealthCheckJob checkService(HealthCheckService healthCheckService) {
        return new HealthCheckJob(healthCheckService);
    }
}
