package cn.com.cowain.ibms.utils.media;

import lombok.extern.slf4j.Slf4j;

import java.io.*;

/**
 * 媒体文件通用工具类
 * @author Yang.Lee
 * @date 2021/1/6 9:51
 */
@Slf4j
public class MediaUtil {

    private MediaUtil(){}

    /**
     * 将pcm格式文件转成mp3格式文件
     * @param source
     * @param target
     * @throws IOException
     */
    public static void pcmToMp3(File source, File target) {
        FileInputStream fis = null;
        FileOutputStream fos = null;

        try{
            fis = new FileInputStream(source);
            fos = new FileOutputStream(target);

            // 计算长度
            byte[] buf = new byte[1024 * 4];
            int size = fis.read(buf);
            int PCMSize = 0;
            while (size != -1) {
                PCMSize += size;
                size = fis.read(buf);
            }
            fis.close();

            // 填入参数，比特率等等。这里用的是16位单声道 8000 hz
            WaveHeader header = new WaveHeader();
            // 长度字段 = 内容的大小（PCMSize) + 头部字段的大小(不包括前面4字节的标识符RIFF以及fileLength本身的4字节)
            header.fileLength = PCMSize + (44 - 8);
            header.FmtHdrLeth = 16;
            header.BitsPerSample = 16;
            header.Channels = 1;
            header.FormatTag = 0x0001;
            header.SamplesPerSec = 16000;// 正常速度是8000，这里写成了16000，速度加快一倍
            header.BlockAlign = (short) (header.Channels * header.BitsPerSample / 8);
            header.AvgBytesPerSec = header.BlockAlign * header.SamplesPerSec;
            header.DataHdrLeth = PCMSize;

            byte[] h = header.getHeader();

            assert h.length == 44; // WAV标准，头部应该是44字节
            //write header
            fos.write(h, 0, h.length);

            //write data stream
            fis = new FileInputStream(source);
            size = fis.read(buf);

            while (size != -1) {
                fos.write(buf, 0, size);
                size = fis.read(buf);
            }

        } catch (IOException e){
            log.error(e.getMessage(), e);
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                } finally {
                    if (fis != null) {
                        try {
                            fis.close();
                        } catch (IOException e) {
                            log.error(e.getMessage(), e);
                        }
                    }
                }
            }
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }
}
