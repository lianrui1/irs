package cn.com.cowain.ibms.rest.resp.device.centralized;

import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 集控码选择设备查询参数
 *
 * @author: yanzy
 * @date: 2022/9/19 10:14
 */
@Data
@ApiModel("集控码选择设备查询参数")
public class CentralizedControlCodeSelectDeviceReq extends PageReq {

    @ApiModelProperty(value = "设备类型")
    private DeviceType deviceType;

    @ApiModelProperty(value = "设备名称")
    private String deviceName;
}
