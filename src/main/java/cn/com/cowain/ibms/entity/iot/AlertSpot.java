package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author Yang.Lee
 * @date 2022/3/17 11:07
 */
@Data
@Entity
@Table(name = "iot_alert_spot")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_alert_spot", comment = "警戒点")
public class AlertSpot extends BaseEntity {

    @Column(name = "name", nullable = false, columnDefinition = "varchar(30) COMMENT '名称'")
    private String name;

    @Column(name = "address", columnDefinition = "varchar(100) COMMENT '具体位置'")
    private String address;

    @ManyToOne
    @JoinColumn(name = "space_id", nullable = false, columnDefinition = "varchar(36) COMMENT '所属空间ID FK'")
    private Space space;

    @ManyToOne
    @JoinColumn(name = "device_id", columnDefinition = "varchar(36) COMMENT '关联设备ID FK'")
    private IotDevice device;

    @Column(name = "ms_url", columnDefinition = "varchar(30) COMMENT '流媒体地址'")
    private String msUrl;
}
