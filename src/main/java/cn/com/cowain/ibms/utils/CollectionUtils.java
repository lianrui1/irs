package cn.com.cowain.ibms.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/10/9 14:54
 */
@Slf4j
public class CollectionUtils {

    /**
     * List深拷贝
     *
     * @param src 基础对象
     * @return 拷贝后对象
     * @author Yang.Lee
     * @date 2021/10/9 14:56
     **/
    public static <T> List<T> deepCopy(List<T> src) {
        try(ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(byteOut)){
            out.writeObject(src);

            ByteArrayInputStream byteIn = new ByteArrayInputStream(byteOut.toByteArray());
            ObjectInputStream in = new ObjectInputStream(byteIn);
            return  (List<T>) in.readObject();
        } catch (Exception e){
            log.error(e.getMessage(), e);
            return new ArrayList<>();
        }
    }
}
