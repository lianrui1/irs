package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 门牌通信信息数据实体
 * @author Yang.Lee
 * @date 2021/1/14 16:15
 */
@Data
@Entity
@Table(name = "iot_door_plate_connection")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_door_plate_connection", comment = "门牌通信")
public class DoorPlateConn extends BaseEntity {

    /**
     * 门牌
     */
    @ManyToOne
    @JoinColumn(name = "door_plate_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 门牌ID'")
    private DoorPlate doorPlate;

    /**
     * 通信时间
     */
    @Column(name = "connect_time", updatable = false, columnDefinition = "datetime COMMENT '通信时间'")
    private LocalDateTime connectTime;
}
