package cn.com.cowain.ibms.rest.req.monitor;

import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author feng
 * @title: screenShotPage
 * @projectName bims
 * @Date 2021/11/18 16:18
 */
@Data
public class ScreenShotPage extends PageReq {

    @ApiModelProperty(value = "id", required = true, example = "", position = 3)
    private String id;
}
