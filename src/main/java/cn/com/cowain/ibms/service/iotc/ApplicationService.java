package cn.com.cowain.ibms.service.iotc;

import cn.com.cowain.ibms.feign.bean.iotc.resp.ApplicationResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/10/20 13:53
 */
public interface ApplicationService {

    /**
     * 保存应用数据数据
     *
     * @param applicationList 应用列表
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/10/20 13:54
     **/
    ServiceResult save(List<ApplicationResp> applicationList);

    /**
     * 向IOTC获取应用列表
     *
     * @return 应用列表
     * @author Yang.Lee
     * @date 2021/10/20 14:02
     **/
    List<ApplicationResp> getApplicationListFromIOTC();

    /**
     * 获取本地数据库中的应用列表
     *
     * @param name 应用名称
     * @return 应用列表
     * @author Yang.Lee
     * @date 2021/10/20 14:05
     **/
    List<ApplicationResp> getList(String name);

    /**
     * 查询设备的应用列表
     *
     * @param deviceId 设备ID
     * @return 应用列表
     * @author Yang.Lee
     * @date 2021/10/26 11:06
     **/
    List<ApplicationResp> getListByDevice(String deviceId);
}
