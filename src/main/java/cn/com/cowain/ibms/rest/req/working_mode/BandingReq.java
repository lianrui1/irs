package cn.com.cowain.ibms.rest.req.working_mode;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author feng
 * @title: ChoiceModel 更换模式请求参数
 * @projectName ibms
 * @Date 2021/12/28 13:48
 */
@Data
public class BandingReq {
    @ApiModelProperty(value = "大屏设备Id")
    private String deviceId;
    @ApiModelProperty(value = "会议室Id")
    private String roomId;
    @ApiModelProperty(value = "项目所在地区Id")
    private String projectId;
}
