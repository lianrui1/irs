package cn.com.cowain.ibms.websocket.enumeration;

/**
 * @author Yang.Lee
 * @date 2022/1/6 9:42
 */
public enum MessageType {

    /**
     * 异常消息
     **/
    ERROR,

    /**
     * 通用消息
     **/
    GENERAL,

    /**
     * 会议详情
     **/
    MEETING_DETAIL,

    /**
     * 会议延长
     **/
    EXTEND,

    /**
     * 会议结束
     **/
    END,

    /**
     * 心跳
     **/
    HEART,

    /**
     * 录音
     **/
    RECORDING,

    /**
     * 结束录音
     **/
    STOP_RECORDING,

    /**
     * 结束录音并保存
     **/
    STOP_RECORDING_AND_SAVE

}
