package cn.com.cowain.ibms.enumeration;

import java.util.Objects;

/**
 * 警戒抓拍子类型
 *
 * @author wei.cheng
 * @date 2022/04/13 18:44
 */
public enum AlertSubType {

    SINGLE_TRIP_WIRE_DETECTION("单绊线检测"),

    ENTET_AREA("进入区域"),

    LEAVE_AREA("离开区域"),

    ACCESS_AREA("进出区域"),

    STAY_AREA("区域停留"),

    SURMOUNT("翻墙检测");

    private final String name;

    AlertSubType(String name) {
        this.name = name;
    }

    public static AlertSubType getAlertSubTypeByRequest(Integer type) {
        if (Objects.isNull(type)) {
            return null;
        }
        AlertSubType result;
        switch (type) {
            case 1:
                result = SINGLE_TRIP_WIRE_DETECTION;
                break;
            case 2:
                result = ENTET_AREA;
                break;
            case 3:
                result = LEAVE_AREA;
                break;
            case 4:
                result = ACCESS_AREA;
                break;
            case 5:
                result = STAY_AREA;
                break;
            case 6:
                result = SURMOUNT;
                break;
            default:
                result = null;
        }
        return result;
    }

    public String getName() {
        return this.name;
    }
}
