package cn.com.cowain.ibms.entity.office;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author Yang.Lee
 * @date 2021/12/7 13:40
 */
@Data
@Entity
@Table(name = "ibms_intelligent_office_user" ,indexes = {@Index(columnList = "user_hr_id"),@Index(columnList = "office_id,invited_user_hr_id")})
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "ibms_intelligent_office_user", comment = "智能办公室用户权限表")
public class IntelligentOfficeUser extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "office_id", nullable = false, columnDefinition = "varchar(36) COMMENT '智能办公室'")
    private IntelligentOffice intelligentOffice;

    @Column(name = "user_hr_id", nullable = false, columnDefinition = "varchar(20) COMMENT '用户工号'")
    private String userHrId;

    @Column(name = "user_name", nullable = false, columnDefinition = "varchar(20) COMMENT '用户姓名'")
    private String userName;

    @Column(name = "invited_user_hr_id", nullable = false, columnDefinition = "varchar(20) COMMENT '邀请人工号'")
    private String invitedUserHrId;
}
