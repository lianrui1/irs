package cn.com.cowain.ibms.rest.resp.space;

import cn.com.cowain.ibms.enumeration.space.ProjectPropertyRightType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/10 14:56
 */
@Data
@ApiModel(value = "项目详情响应对象")
public class ProjectDetailResp {

    @ApiModelProperty(value = "项目ID")
    private String id;

    @ApiModelProperty(value = "项目名称")
    private String name;

    @ApiModelProperty(value = "项目编号")
    private String no;

    @ApiModelProperty(value = "项目地址")
    private String address;

    @ApiModelProperty(value = "项目区域")
    private String area;

    @ApiModelProperty(value = "项目产权属性")
    private ProjectPropertyRightType propertyRightType;

    @ApiModelProperty(value = "项目单位")
    private String visitCompany;

    @ApiModelProperty(value = "项目单位")
    private String areaM2;

    @ApiModelProperty(value = "简介")
    private String introduction;

}
