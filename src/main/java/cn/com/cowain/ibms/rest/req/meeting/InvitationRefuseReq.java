package cn.com.cowain.ibms.rest.req.meeting;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2021/6/25 9:30
 */
@Data
@ApiModel("访客拒绝邀请数据请求对象")
public class InvitationRefuseReq {

    @ApiModelProperty(value = "访客邀请记录码", required = true, example = "", position = 1)
    private String recordCode;
}
