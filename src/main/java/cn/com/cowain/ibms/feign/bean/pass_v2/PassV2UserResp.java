package cn.com.cowain.ibms.feign.bean.pass_v2;

import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.enumeration.iot.PersonType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author wei.cheng
 * @date 2022/06/15 10:54
 */
@Data
@ApiModel(value = "用户详情")
public class PassV2UserResp implements Serializable {

    @ApiModelProperty(value = "人员名称", required = true, example = "张三")
    private String name;

    @ApiModelProperty(value = "人员类型", required = true, example = "EMPLOYEE")
    private PersonType personType;

    @ApiModelProperty(value = "人员类型描述", required = true, example = "员工")
    private String personTypeDesc;

    @ApiModelProperty(value = "是否完成人脸认证", required = true, example = "false")
    private Boolean faceAuthentication;

    @ApiModelProperty(value = "是否完成实名认证", required = true, example = "false")
    private Boolean realNameAuthentication;

    @ApiModelProperty(value = "工号", example = "CWA1111")
    private String hrId;

    @ApiModelProperty(value = "uc id", example = "111")
    private String uid;

    @ApiModelProperty(value = "头像", example = "11")
    private String img;

    @ApiModelProperty(value = "手机号码", example = "152221212")
    private String phone;

    public SysUser toSysUser(){

        SysUser sysUser = new SysUser();
        sysUser.setEmpNo(this.hrId);
        sysUser.setNameZh(this.name);
        sysUser.setHeadImgUrl(this.img);
        sysUser.setMobile(this.phone);
        return sysUser;
    }
}
