package cn.com.cowain.ibms.dao;

import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.entity.ReservationRecordItem;
import cn.com.cowain.ibms.entity.Room;
import cn.com.cowain.ibms.enumeration.ReservationRecordItemStatus;
import cn.com.cowain.ibms.enumeration.ReservationRecordStatus;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.bean.ReservationRecordBean;
import cn.com.cowain.ibms.rest.req.InviteeStatusReq;
import cn.com.cowain.ibms.rest.req.ReservationUpdateReq;
import cn.com.cowain.ibms.rest.req.SysUserBean;
import cn.com.cowain.ibms.rest.req.UserStatusBean;
import cn.com.cowain.ibms.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Slf4j
@SpringBootTest
public class ReservationRecordDaoTest {

    @Autowired
    private ReservationRecordItemDao reservationRecordItemDao;
    @Autowired
    private ReservationRecordDao reservationRecordDao;
    @Autowired
    private RoomDao roomDao;

    @Test
    public void search() {
        // Sort sort = Sort.by(Sort.Direction.DESC, "from");
        Pageable pageable = PageRequest.of(0, 7);

        // Page<ReservationRecordItem> reservationRecordItemSet =
        // reservationRecordItemDao.findByOwner("testId111",pageable);
        // Page<ReservationRecordItem> reservationRecordItemSet =
        // reservationRecordItemDao.findByOwnerOrderByReservationRecordFromDesc("testId111",pageable);
        LocalDateTime localDateTime = LocalDateTime.now().minusDays(7);
        // Page<ReservationRecordItem> reservationRecordItemSet =
        // reservationRecordItemDao.findByReservationRecordFromGreaterThan(localDateTime,pageable);
        // Page<ReservationRecordItem> reservationRecordItemSet =
        // reservationRecordItemDao.findByOwnerAndReservationRecordFromGreaterThan("testId111",localDateTime,pageable);
        // Page<ReservationRecordItem> reservationRecordItemSet =
        // reservationRecordItemDao.findByOwnerOrInitiator("testId111",
        // "testId111",pageable);
        String Owner = "testId111";
        Page<ReservationRecordItem> reservationRecordItemPage = reservationRecordItemDao.
                findByOwnerEmpNoAndReservationRecordFromGreaterThanAndReservationRecordRoomSpaceProjectIdContainsOrderByReservationRecordFromDesc(Owner, localDateTime, "123",pageable);
        PageBean pageBean = new PageBean();
        List<ReservationRecordBean> reservationRecordItemList = new ArrayList<>();
        if (reservationRecordItemPage.getContent().size() != 0) {

            reservationRecordItemPage.getContent().forEach(it -> {
                ReservationRecordBean reservationRecordBean = new ReservationRecordBean();
                reservationRecordBean.setTopic(it.getReservationRecord().getTopic());
                reservationRecordBean.setTimeFrom(it.getReservationRecord().getFrom());
                reservationRecordBean.setTimeTo(it.getReservationRecord().getTo());
                reservationRecordBean.setRoomName(it.getReservationRecord().getRoom().getName());
                reservationRecordBean.setRoomBuilding(it.getReservationRecord().getRoom().getBuilding());
                reservationRecordBean.setFloor(it.getReservationRecord().getRoom().getFloor());
                reservationRecordBean.setInitiator(it.getInitiator());
//                reservationRecordBean.setReservationRecordStatus(it.getStatus().name());
                if (it.getInitiator().equals(Owner)) {
                    reservationRecordBean.setIsOwn(1);
                }
                reservationRecordItemList.add(reservationRecordBean);

            });
            pageBean.setPage(pageable.getPageNumber()); // 当前页码
            pageBean.setSize(pageable.getPageSize()); // 每页记录数
            pageBean.setTotalElements(reservationRecordItemPage.getTotalElements()); // 总记录数
            pageBean.setTotalPages(reservationRecordItemPage.getTotalPages()); // 总页数
            pageBean.setList(reservationRecordItemList);

        }
        log.info(String.valueOf(reservationRecordItemPage));
    }

    @Test
    public void test1() {
        LocalDate date = LocalDate.of(2020, 8, 7);
        List<ReservationRecord> reservationRecord = reservationRecordDao.findByDateAndRoomIdOrderByFromAsc(date,
                "a7624aa4-98bc-4532-936c-01b47734e068");
        System.out.println(reservationRecord);
    }

    @Test
    public void updateReservation() {
        ReservationUpdateReq reservationUpdateReq = new ReservationUpdateReq();
        reservationUpdateReq.setTopic("更新会议主题");
        String roomId = "a7624aa4-98bc-4532-936c-01b47734e068";
        String reservationId = "b9aeb22f-6a06-494f-9071-8d8ed55e5fa6";
        LocalDate localDate = LocalDate.of(2020, 8, 9);
        LocalTime localTimeFrom = LocalTime.of(15, 15);
        LocalTime localTimeTo = LocalTime.of(16, 15);
        LocalDateTime localDateTimeFrom = LocalDateTime.of(localDate, localTimeFrom);
        LocalDateTime localDateTimeTo = LocalDateTime.of(localDate, localTimeTo);
        reservationUpdateReq.setDate(localDate);
        reservationUpdateReq.setFrom(localDateTimeFrom);
        reservationUpdateReq.setTo(localDateTimeTo);
        // reservationUpdateReq.setReservationId(reservationId);
        reservationUpdateReq.setRoomId(roomId);
        List list = new ArrayList();
        SysUserBean sysUserBean = new SysUserBean();
        sysUserBean.setSysId("testId22");
        UserStatusBean sysUserBean1 = new UserStatusBean();
        UserStatusBean sysUserBean2 = new UserStatusBean();
        UserStatusBean sysUserBean3 = new UserStatusBean();
        UserStatusBean sysUserBean4 = new UserStatusBean();

        sysUserBean1.setSysId("testId111");
        sysUserBean1.setStatus(ReservationRecordItemStatus.CANCEL);
        list.add(sysUserBean1);
        sysUserBean2.setSysId("testId22");
        sysUserBean2.setStatus(ReservationRecordItemStatus.UNDETERMINED);
        list.add(sysUserBean2);
        sysUserBean3.setSysId("testId333");
        sysUserBean3.setStatus(ReservationRecordItemStatus.FINISH);
        list.add(sysUserBean4);
        sysUserBean4.setSysId("testId44");
        sysUserBean4.setStatus(ReservationRecordItemStatus.CONFIRM);
        list.add(sysUserBean4);
        reservationUpdateReq.setParticipantList(list);

        reservationUpdateReq.setInitiator(sysUserBean);

        Optional<ReservationRecord> reservationRecordOptional = reservationRecordDao.findById(reservationId);

        boolean present = reservationRecordOptional.isPresent();
        if (present) {
            log.info(String.valueOf(reservationRecordOptional));
            if (reservationRecordOptional.get().getFrom() != localDateTimeFrom
                    || reservationRecordOptional.get().getTo() != localDateTimeTo) {
                log.info("开始时间 或 结束时间 不相同");
                log.info("更新。。。。。");
                reservationRecordOptional.get().setDate(reservationUpdateReq.getDate());
                reservationRecordOptional.get().setFrom(reservationUpdateReq.getFrom());
                reservationRecordOptional.get().setTo(reservationUpdateReq.getTo());
            } else {
                log.info("时间相同");
            }
            if (reservationRecordOptional.get().getRoom().getId() != roomId) {
                Optional<Room> room = roomDao.findById(roomId);
                Boolean roomPresent = room.isPresent();
                if (roomPresent) {
                    reservationRecordOptional.get().setRoom(room.get());
                }
            }
            reservationRecordOptional.get().setTopic(reservationUpdateReq.getTopic());

            reservationRecordDao.save(reservationRecordOptional.get());

            List<ReservationRecordItem> reservationRecordItemList = new ArrayList<>();

            reservationUpdateReq.getParticipantList().forEach(it -> {
                Optional<ReservationRecordItem> reservationRecordItemOptional = reservationRecordItemDao
                        .findByReservationRecordIdAndOwner(reservationId, it.getSysId());
                boolean reservationRecordItemOptionalPresent = reservationRecordItemOptional.isPresent();
                if (reservationRecordItemOptionalPresent) {
                    if (reservationRecordItemOptional.get().getStatus() != it.getStatus()) {
                        reservationRecordItemOptional.get().setStatus(it.getStatus()); // 修改参会人状态
                        reservationRecordItemList.add(reservationRecordItemOptional.get());
                    } else {
                        reservationRecordItemList.add(reservationRecordItemOptional.get());
                    }
                } else {
                    ReservationRecordItem reservationRecordItem = new ReservationRecordItem();
                    reservationRecordItem.setStatus(ReservationRecordItemStatus.UNDETERMINED);
                    reservationRecordItem.setOwner(it.getSysId());
                    reservationRecordItem.setInitiator(reservationUpdateReq.getInitiator().getSysId());
                    reservationRecordItem.setReservationRecord(reservationRecordOptional.get());
                    reservationRecordItemList.add(reservationRecordItem);
                }

            });
            reservationRecordItemDao.saveAll(reservationRecordItemList);
        }
        log.info(String.valueOf(reservationUpdateReq));
    }

    @Test
    public void cancel() {
        String id = "b9aeb22f-6a06-494f-9071-8d8ed55e5fa6";
        Optional<ReservationRecord> reservationRecord = reservationRecordDao.findById(id);
        Boolean present = reservationRecord.isPresent();
        if (present) {
            ReservationRecord reservationRecord1 = reservationRecord.get();
            reservationRecord1.setStatus(ReservationRecordStatus.CANCEL);
            Set<ReservationRecordItem> reservationRecordItemSet = reservationRecordItemDao
                    .findByReservationRecordId(id);
            List<ReservationRecordItem> reservationRecordItemList = new ArrayList<>();
            reservationRecordItemSet.forEach(it -> {
                log.debug("修改参会人状态:" + it.getOwner());
                it.setStatus(ReservationRecordItemStatus.CANCEL);
                reservationRecordItemList.add(it);
                log.debug("此处推送 取消会议消息给用户: " + it.getOwner());
            });
            reservationRecordItemDao.saveAll(reservationRecordItemList);
            reservationRecordDao.save(reservationRecord1);
            log.info("数据库更新完成！！");
        }
        log.info(String.valueOf(reservationRecord));
    }

    @Test
    public void inn() {
        String id = "b9aeb22f-6a06-494f-9071-8d8ed55e5fa6";
        InviteeStatusReq inviteeStatusReq = new InviteeStatusReq();
        inviteeStatusReq.setSysId("test3");
        inviteeStatusReq.setStatus(ReservationRecordItemStatus.FINISH);
        Optional<ReservationRecordItem> byReservationRecordIdAndOwner = reservationRecordItemDao
                .findByReservationRecordIdAndOwner(id, inviteeStatusReq.getSysId());
        Boolean present = byReservationRecordIdAndOwner.isPresent();
        if (present) {
            byReservationRecordIdAndOwner.get().setStatus(inviteeStatusReq.getStatus());
            reservationRecordItemDao.save(byReservationRecordIdAndOwner.get());
            log.info("此处可能要写一些推送的消息");
        }
    }

    @Test
    public void findByInitiator() {
        Assertions.assertNotNull(reservationRecordDao);
        Sort sort = Sort.by(Sort.Direction.DESC, "date");
        int page = 0;
        int size = 10;
        Pageable pageable = PageRequest.of(page, size, sort);
        String initiator = "asdf";
        List<ReservationRecord> list = reservationRecordDao.findByInitiator(initiator, pageable);
        System.out.println(list);
    }

    @Test
    public void findById() {
        Optional<ReservationRecord> byId = reservationRecordDao.findById("002ffe93-0cec-4a67-95ea-e4e520fb42c1");
        ReservationRecord reservationRecord = byId.get();
        System.out.println(reservationRecord);

        LocalDateTime from = reservationRecord.getFrom();
        System.out.println(from);

        String str = "2020-12-01 19:00:00";
        LocalDateTime localDateTime = DateUtils.parseLocalDateTime(str, "yyyy-MM-dd HH:mm:ss");
        System.out.println(localDateTime);

        ReservationRecordStatus aDefault = ReservationRecordStatus.DEFAULT;
        Set<ReservationRecord> byFrom = reservationRecordDao.findByFromAndStatus(localDateTime,aDefault);
        //...

    }

}