package cn.com.cowain.ibms.rest.req.device.centralized;

import cn.com.cowain.ibms.enumeration.integration_control.ControlDeviceRange;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import com.sun.istack.NotNull;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author wei.cheng
 * @date 2022/09/15 17:20
 */
@Data
@ApiModel(value = "集控码修改请求")
public class CentralizedControlCodeUpdateReq {
    @ApiModelProperty(value = "集控码名称，1～20字符大小", required = true)
    @NotBlank
    @Length(max = 20, message = "不能超过20字符")
    private String name;

    @ApiModelProperty(value = "关联项目id", required = true)
    @NotBlank
    private String projectId;

    @ApiModelProperty(value = "关联空间id", required = true)
    @NotBlank
    private String spaceId;

    @ApiModelProperty(value = "控制的设备类型列表", required = true)
    @NotEmpty
    private List<DeviceType> controlDeviceTypeList;

    @ApiModelProperty(value = "控制设备范围, ALL：全部，PART：部分")
    @NotNull
    private ControlDeviceRange controlDeviceRange;

    @ApiModelProperty(value = "集控码绑定的设备，只有当控制设备范围为部分时有效")
    private List<String> deviceIds;

    @ApiModelProperty(value = "管理员工号")
    @NotBlank
    private String adminHrId;

    @ApiModelProperty(value = "管理员姓名")
    @NotBlank
    private String adminName;
}
