package cn.com.cowain.ibms.service.iot.impl;

import cn.com.cowain.ibms.dao.iot.AccessControlSpotAccessRecordDao;
import cn.com.cowain.ibms.dao.iot.AccessControlSpotDao;
import cn.com.cowain.ibms.dao.iot.DeviceAccessRecordDao;
import cn.com.cowain.ibms.entity.iot.AccessControlSpot;
import cn.com.cowain.ibms.entity.iot.AccessControlSpotAccessRecord;
import cn.com.cowain.ibms.entity.iot.DeviceAccessRecord;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.iot.AccessControlSpotAccessRecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Optional;

/**
 * @author Yang.Lee
 * @date 2021/12/8 11:25
 */
@Service
@Slf4j
public class AccessControlSpotAccessRecordServiceImpl implements AccessControlSpotAccessRecordService {

    @Resource
    private AccessControlSpotAccessRecordDao accessControlSpotAccessRecordDao;

    @Resource
    private DeviceAccessRecordDao deviceAccessRecordDao;

    @Resource
    private AccessControlSpotDao accessControlSpotDao;

    /**
     * 创建门禁点通行记录
     *
     * @param deviceAccessRecord 设备通行记录
     * @return 创建结果
     * @author Yang.Lee
     * @date 2021/12/8 11:24
     **/
    @Override
    @Transactional
    public ServiceResult create(DeviceAccessRecord deviceAccessRecord) {

        // 保存记录到设备通行记录表
        deviceAccessRecordDao.save(deviceAccessRecord);

        // 判断设备是否关联到门禁点
        Optional<AccessControlSpot> accessControlSpotOptional = accessControlSpotDao.findByDeviceId(deviceAccessRecord.getIotDevice().getId());
        if(accessControlSpotOptional.isEmpty()){
            log.error("收到设备通行记录，但设备并未关联门禁点。 通行信息：{}", deviceAccessRecord);
            return ServiceResult.ok();
        }

        // 将记录写到门禁点通行中
        AccessControlSpotAccessRecord record = new AccessControlSpotAccessRecord();
        record.setAccessControlSpot(accessControlSpotOptional.get());
        record.setDeviceAccessRecord(deviceAccessRecord);

        accessControlSpotAccessRecordDao.save(record);

        return ServiceResult.ok();
    }
}
