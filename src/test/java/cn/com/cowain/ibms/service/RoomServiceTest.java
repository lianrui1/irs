package cn.com.cowain.ibms.service;

import cn.com.cowain.ibms.dao.RoomDao;
import cn.com.cowain.ibms.entity.Room;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.OpeningHoursReq;
import cn.com.cowain.ibms.rest.req.RoomReq;
import cn.com.cowain.ibms.rest.req.SysUserBean;
import cn.com.cowain.ibms.rest.resp.RoomSearchResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@SpringBootTest
class RoomServiceTest {

    @Autowired
    private RoomService roomService;

    @Autowired
    private DataFactory dataFactory;

    @Autowired
    private RoomDao roomDao;

    @Test
    void save() {
        for (Integer i = 0; i < 5; i++) {
            RoomReq roomReq = new RoomReq();
            roomReq.setName("room-add1" + dataFactory.getName());
//            roomReq.setBuilding("A" + dataFactory.getName());
//            roomReq.setFloor("82" + dataFactory.getNumberText(1));
            roomReq.setCapacity(50);
            roomReq.setIsOpen(1);
            List<String> deviceList = new ArrayList<>();
            deviceList.add(dataFactory.getName());
            deviceList.add(dataFactory.getName());
            roomReq.setDevices(deviceList);
            List<OpeningHoursReq> openingHours = new ArrayList<>();
            OpeningHoursReq openingHoursReq = new OpeningHoursReq();
            openingHoursReq.setFrom("08:00");
            openingHoursReq.setTo("22:00");
            openingHours.add(openingHoursReq);
            roomReq.setOpeningHours(openingHours);
            SysUserBean sysUserBean = new SysUserBean();
            sysUserBean.setSysId("1");
            sysUserBean.setNameZh("zyx");
//            roomReq.setSysUserBean(sysUserBean);
            ServiceResult serviceResult = roomService.save(roomReq);
            System.out.println(serviceResult);
        }
    }

    @Test
//    @Ignore
    void update() {
        Set<Room> room = roomDao.findByNameLike("%room%");
        if (room != null && room.size() > 0) {
            Room roomNew = room.iterator().next();
            String id = roomNew.getId();
            RoomReq roomReq = new RoomReq();
            roomReq.setName("roomNew-update-" + dataFactory.getName());
//            roomReq.setBuilding("A-new-" + dataFactory.getName());
//            roomReq.setFloor("80" + dataFactory.getNumberBetween(0, 8));
            roomReq.setCapacity(50);
            roomReq.setIsOpen(1);
            List<String> deviceList = new ArrayList<>();
            deviceList.add(dataFactory.getName());
            deviceList.add(dataFactory.getName());
            roomReq.setDevices(deviceList);
            List<OpeningHoursReq> openingHours = new ArrayList<>();
            OpeningHoursReq openingHoursReq = new OpeningHoursReq();
            openingHoursReq.setFrom("08:00");
            openingHoursReq.setTo("22:00");
            openingHours.add(openingHoursReq);
            roomReq.setOpeningHours(openingHours);
            SysUserBean sysUserBean = new SysUserBean();
            sysUserBean.setSysId("1");
            sysUserBean.setNameZh("zyx");
//            roomReq.setSysUserBean(sysUserBean);
            ServiceResult serviceResult = roomService.update(id, roomReq);
            System.out.println(serviceResult);
        }
    }

    @Test
    void search() {
        String roomName = "room";
        int page = 0;
        int size = 20;
        PageBean<RoomSearchResp> roomSearchRespPageBean = roomService.search(roomName, page, size);
        System.out.println(roomSearchRespPageBean);
    }

    @Test
    void updateRoomTimeSlotAndDeviceStatus() {
        Set<Room> roomSet = roomDao.findByNameLike("%room%");
        if(roomSet!=null && roomSet.size()>0) {
            String id = roomSet.iterator().next().getId();
            Integer integer = 1;
            Boolean aBoolean = roomService.updateRoomTimeSlotAndDeviceStatus(id, integer);
            Assertions.assertTrue(aBoolean);
        }
    }

    @Test
    void valid() {
        RoomReq roomReq = new RoomReq();
        roomReq.setName("add room new");
//        roomReq.setBuilding("A new ");
//        roomReq.setFloor("82");
        roomReq.setCapacity(52);
        roomReq.setIsOpen(1);
        List<String> deviceList = new ArrayList<>();
        deviceList.add("投影仪");
        deviceList.add("电视机");
        deviceList.add("音响");
        roomReq.setDevices(deviceList);
        List<OpeningHoursReq> openingHours = new ArrayList<>();
        OpeningHoursReq openingHoursReq = new OpeningHoursReq();
        openingHoursReq.setFrom("09:00");
        openingHoursReq.setTo("09:00");
        openingHours.add(openingHoursReq);
        roomReq.setOpeningHours(openingHours);
        SysUserBean sysUserBean = new SysUserBean();
        sysUserBean.setSysId("1");
        sysUserBean.setNameZh("zyx");
//        roomReq.setSysUserBean(sysUserBean);
        Boolean booLean = roomService.valid(roomReq);
        System.out.println(booLean);
    }
}