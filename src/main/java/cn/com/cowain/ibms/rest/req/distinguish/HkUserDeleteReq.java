package cn.com.cowain.ibms.rest.req.distinguish;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/22 19:09
 */
@Data
public class HkUserDeleteReq {

    private String deviceId;

    private String jobNum;
}
