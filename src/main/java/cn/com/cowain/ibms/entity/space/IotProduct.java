package cn.com.cowain.ibms.entity.space;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.enumeration.IotNodeType;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;


/**
 * IOT 产品
 *
 * @author yu chun lei
 * @date 2020/12/17  18:51
 * @since s3
 */
@Data
@Entity
@Table(name = "iot_product")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_product", comment = "产品")
public class IotProduct extends BaseEntity {

    private static final long serialVersionUID = 2076473191094061922L;
    /**
     * 产品名称
     */
    @Column(name = "prod_name", nullable = false, columnDefinition = "varchar(256) COMMENT '产品名称'")
    private String name;


    /**
     * 产品类目类型
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "device_type", columnDefinition = "varchar(50) COMMENT '设备类型'")
    private DeviceType deviceType;

    /**
     * IOT网络节点 类型
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "iot_node_type", columnDefinition = "varchar(50) COMMENT 'IOT网络节点 类型'")
    private IotNodeType iotNodeType;


    /**
     * 排序
     */
    @Column(name = "prod_seq", columnDefinition = "int(2) COMMENT '排序'")
    private int seq;

    /**
     * 产品编号
     */
    @Column(name = "product_no", columnDefinition = "int(3) COMMENT '产品编号'")
    private int productNo;

    /**
     * 产品名称备注
     */
    @Column(name = "remark", columnDefinition = "varchar(2000) COMMENT '产品名称备注'")
    private String remark;

    /**
     * 华为云中的设备关联的产品ID
     */
    @Column(name = "hw_product_id", columnDefinition = "varchar(256) COMMENT '华为云中的设备关联的产品ID'")
    private String hwProductId;

    /**
     * 华为云中的产品名称
     */
    @Column(name = "hw_name", columnDefinition = "varchar(256) COMMENT '华为云中的产品名称'")
    private String hwName;

    /**
     * 华为云中的设备类型
     */
    @Column(name = "hw_device_type", columnDefinition = "varchar(256) COMMENT '华为云中的设备类型'")
    private String hwDeviceType;

    /**
     * 华为云中的设备使用的协议类型。取值范围：MQTT，CoAP，HTTP，HTTPS，Modbus，ONVIF。
     */
    @Column(name = "hw_protocol_type", columnDefinition = "varchar(20) COMMENT '华为云中的设备使用的协议类型。取值范围：MQTT，CoAP，HTTP，HTTPS，Modbus，ONVIF。'")
    private String hwProtocolType;

    /**
     * 华为云中的设备上报数据的格式，取值范围：json，binary。
     */
    @Column(name = "hw_data_format", columnDefinition = "varchar(20) COMMENT '华为云中的设备上报数据的格式，取值范围：json，binary。'")
    private String hwDataFormat;

    /**
     * 华为云中的厂商名称。
     */
    @Column(name = "hw_manufacturer_name", columnDefinition = "varchar(256) COMMENT '华为云中的厂商名称'")
    private String hwManufacturerName;

    /**
     * 华为云中的设备所属行业。
     */
    @Column(name = "hw_industry", columnDefinition = "varchar(256) COMMENT '华为云中的设备所属行业'")
    private String hwIndustry;

    /**
     * 华为云中的设备描述
     */
    @Column(name = "hw_description", columnDefinition = "varchar(2048) COMMENT '华为云中的设备描述'")
    private String hwDescription;

    /**
     * 华为云中的在物联网平台创建产品的时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", shape = JsonFormat.Shape.STRING)
    @Column(name = "hw_create_time", columnDefinition = "datetime COMMENT '华为云中的在物联网平台创建产品的时间'")
    private LocalDateTime hwCreateTime;

    /**
     * 华为云硬件属性
     */
    @Column(name = "hw_attribute", columnDefinition = "text COMMENT '华为云硬件属性(json字符串)'")
    private String hwAttribute;

    /**
     * 华为云硬件提供的服务能力
     */
    @Column(name = "hw_service", columnDefinition = "text COMMENT '华为云硬件提供的服务能力(json字符串)'")
    private String hwService;

    @Column(name = "iotc_id", columnDefinition = "varchar(50) COMMENT 'iotc中id'")
    private String iotcId;
}
