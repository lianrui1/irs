package cn.com.cowain.ibms.service.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/14/20
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WeChatMsg {

    private String first;
    private String keyword1;
    private String keyword2;
    private String keyword3;
    private String ketword4;
    private String remark;

}
