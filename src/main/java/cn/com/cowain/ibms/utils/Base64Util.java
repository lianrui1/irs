package cn.com.cowain.ibms.utils;

import org.apache.axis.encoding.Base64;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Base64操作类
 * @author Yang.Lee
 * @date 2020/12/2 14:33
 */
public class Base64Util {

    private Base64Util(){}

    /**
     * encode
     * @param bufferedImage  图片对象
     * @param ext  图片格式
     * @return
     * @author Yang.Lee
     */
    public static String encode(BufferedImage bufferedImage, String ext) throws IOException {

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, ext, stream);

        return Base64.encode(stream.toByteArray());
    }
}
