package cn.com.cowain.ibms.enumeration;

public enum LogStatus {

    /**
     * 已解决
     */
    RESOLVED("已解决"),


    /**
     * 已修复
     */
    REPAIR("已修复"),

    /**
     * 异常
     */
    EXCEPTION("异常"),

    /**
     * 忽略
     */
    IGNORE("忽略"),

    /**
     * 正常
     */
    NORMAL("正常");


    LogStatus(String name) {
        this.name = name;
    }

    private String name;

    public String getName() {
        return this.name;
    }
}

