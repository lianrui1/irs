package cn.com.cowain.ibms.rest.resp.distinguish;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/8/3 15:33
 */
@Data
public class HkResultResp {

    private int totalCount;

    private int successCount;

    private int errorCount;

    private int over;


    private int percent;

    private String errorDataId;
}
