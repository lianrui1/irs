package cn.com.cowain.ibms.bo;

import cn.com.cowain.ibms.rest.resp.EhrStaffResp;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author wei.cheng
 * @date 2022/10/10 14:27
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SimpleSysUser implements Serializable {

    /**
     * 用户中心的全局唯一id
     */
    private String sysId;

    /**
     * 员工姓名 中文
     */
    private String nameZh;

    /**
     * 员工姓名 英文
     */
    private String nameEn;

    /**
     * 用户部门全称
     */
    private String fullDepartmentName;

    /**
     * 微信头像 headImgUrl
     */
    private String headImgUrl;
    /**
     * 工号
     */
    private String empNo;

    public SimpleSysUser(String empNo, String nameZh) {
        this.empNo = empNo;
        this.nameZh = nameZh;
    }

    public static EhrStaffResp convert(SimpleSysUser sysUser) {
        EhrStaffResp ehrStaffResp = new EhrStaffResp();
        ehrStaffResp.setSysId(sysUser.getSysId());
        ehrStaffResp.setNameZh(sysUser.getNameZh());
        ehrStaffResp.setNameEn(sysUser.getNameEn());
        ehrStaffResp.setHeadImgUrl(sysUser.getHeadImgUrl());
        ehrStaffResp.setHrId(sysUser.getEmpNo());
        return ehrStaffResp;
    }
}
