-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 10.0.1.232    Database: ibms_uat
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ibms_room`
--

DROP TABLE IF EXISTS `ibms_room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ibms_room` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `created_by` varchar(36) DEFAULT NULL COMMENT '创建人',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(36) DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int(11) DEFAULT NULL COMMENT '版本号',
  `building` varchar(200) NOT NULL COMMENT '楼栋',
  `room_cap` int(11) NOT NULL COMMENT '人数容量',
  `devices` varchar(200) DEFAULT NULL COMMENT '包含设备',
  `floor` varchar(200) NOT NULL COMMENT '楼层',
  `is_open` int(11) DEFAULT NULL COMMENT '是否开放 0:否;1:是',
  `room_name` varchar(255) NOT NULL COMMENT '名称',
  `seq` int(11) DEFAULT NULL COMMENT '排序',
  `room_type` varchar(50) DEFAULT NULL COMMENT '房间类型',
  `space_id` varchar(36) DEFAULT NULL COMMENT 'space_id FK',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK64qgoeoomviu28r3dcdu13u4l` (`space_id`) USING BTREE,
  CONSTRAINT `FK64qgoeoomviu28r3dcdu13u4l` FOREIGN KEY (`space_id`) REFERENCES `iot_space` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='会议室';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ibms_room`
--

LOCK TABLES `ibms_room` WRITE;
/*!40000 ALTER TABLE `ibms_room` DISABLE KEYS */;
INSERT INTO `ibms_room` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `building`, `room_cap`, `devices`, `floor`, `is_open`, `room_name`, `seq`, `room_type`, `space_id`) VALUES ('11de355e-dd8e-4b3a-b6c7-483874d2cac2','5337','2020-12-30 20:23:57',0,NULL,'2021-02-08 16:36:47',5,'汇金大厦',20,NULL,'16',1,'长庚',7,NULL,'57452c93-af90-4ba7-bd02-4267eb20c417');
INSERT INTO `ibms_room` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `building`, `room_cap`, `devices`, `floor`, `is_open`, `room_name`, `seq`, `room_type`, `space_id`) VALUES ('4af6f5da-98f0-4fcc-9120-2b7f12b74426','5337','2020-12-31 08:10:19',0,NULL,'2021-02-08 16:33:52',3,'A',20,NULL,'3',1,'天玑',2,NULL,'17007a67-0272-4f83-bc76-fac755a106b5');
INSERT INTO `ibms_room` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `building`, `room_cap`, `devices`, `floor`, `is_open`, `room_name`, `seq`, `room_type`, `space_id`) VALUES ('5061fd31-e67c-4547-aedb-9b7abcb319b0','5337','2021-02-08 16:33:11',0,NULL,'2021-02-08 16:37:01',4,'A',100,NULL,'3',1,'天枢',2,NULL,'91e5b167-bcf5-4697-8067-aa87cf659602');
INSERT INTO `ibms_room` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `building`, `room_cap`, `devices`, `floor`, `is_open`, `room_name`, `seq`, `room_type`, `space_id`) VALUES ('525df556-1e78-4c65-b2fa-cf6365791218','5337','2021-02-09 16:19:37',1,NULL,'2021-02-09 16:19:43',1,'1',1,NULL,'1',0,'测试',9,NULL,'19ceb3d8-6c3b-47d8-8717-4827a5f530ea');
INSERT INTO `ibms_room` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `building`, `room_cap`, `devices`, `floor`, `is_open`, `room_name`, `seq`, `room_type`, `space_id`) VALUES ('58fe7d83-e1c6-484c-ab4b-cf7584e1e63e','5337','2021-02-08 16:35:21',0,NULL,'2021-02-08 16:37:01',1,'A',20,NULL,'3',1,'开阳',4,NULL,'8eefce85-669c-4f19-b032-8aeccc8339a8');
INSERT INTO `ibms_room` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `building`, `room_cap`, `devices`, `floor`, `is_open`, `room_name`, `seq`, `room_type`, `space_id`) VALUES ('6d9faaf8-9215-4402-aaaf-b2282a63bb87','5337','2020-12-31 09:47:19',1,NULL,'2020-12-31 09:47:19',1,'C',5,NULL,'3',1,'临时会议室',4,NULL,'15c5ad65-ab30-43b8-a5dc-38446e31a847');
INSERT INTO `ibms_room` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `building`, `room_cap`, `devices`, `floor`, `is_open`, `room_name`, `seq`, `room_type`, `space_id`) VALUES ('764ad672-ec0b-4004-9b0a-75be9d0215b5','5337','2021-01-25 19:46:30',1,NULL,'2021-01-25 19:47:18',1,'334',22,NULL,'22',1,'33321',4,NULL,'7dde8e1d-352a-4b55-b352-a14861726a3a');
INSERT INTO `ibms_room` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `building`, `room_cap`, `devices`, `floor`, `is_open`, `room_name`, `seq`, `room_type`, `space_id`) VALUES ('7fdab81b-4607-485b-a0e9-49029a3534f7','5337','2021-02-08 16:35:46',0,NULL,'2021-02-08 16:36:47',1,'A',10,NULL,'3',1,'玉衡',5,NULL,'df3a2435-e32d-4ddf-9f94-332ab7ff1fdb');
INSERT INTO `ibms_room` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `building`, `room_cap`, `devices`, `floor`, `is_open`, `room_name`, `seq`, `room_type`, `space_id`) VALUES ('a59af0e2-f314-4d2b-8f2a-a3d6e7aebe5c','5337','2021-02-08 16:33:43',0,NULL,'2021-02-08 16:33:58',2,'A',50,NULL,'3',1,'天璇',2,NULL,'ee850242-e317-4005-9e4f-c73238c15bd3');
INSERT INTO `ibms_room` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `building`, `room_cap`, `devices`, `floor`, `is_open`, `room_name`, `seq`, `room_type`, `space_id`) VALUES ('e45e15aa-06d9-423e-be61-efd795f0b712','5337','2021-02-08 16:34:41',0,NULL,'2021-02-08 16:37:01',1,'A',20,NULL,'3',1,'天权',3,NULL,'dd16ff08-efa9-408b-9cae-7cf54628d737');
INSERT INTO `ibms_room` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `building`, `room_cap`, `devices`, `floor`, `is_open`, `room_name`, `seq`, `room_type`, `space_id`) VALUES ('e8e1e58b-6db8-4ca4-8133-7146d70a2177','5337','2020-12-30 20:24:26',0,NULL,'2021-02-08 16:36:14',4,'汇金大厦',20,NULL,'16',1,'启明',8,NULL,'839b114d-87bc-4e3a-a8be-f274916d0416');
INSERT INTO `ibms_room` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `building`, `room_cap`, `devices`, `floor`, `is_open`, `room_name`, `seq`, `room_type`, `space_id`) VALUES ('ed2dc26f-cd02-4142-86e1-eee8e0753d63','5337','2020-12-30 20:23:18',0,NULL,'2021-02-08 16:36:47',5,'C',30,NULL,'3',1,'产品研发中心',6,NULL,'1f53db55-dc98-444b-8cef-74762900930b');
/*!40000 ALTER TABLE `ibms_room` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-26  9:34:19
