package cn.com.cowain.ibms.rest.resp.department;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 组织部门详情resp
 *
 * @author Yang.Lee
 * @date 2021/2/5 10:58
 */
@Data
@ApiModel("组织部门详情")
public class DepartmentResp implements Serializable {

    private static final long serialVersionUID = -4077524274706903313L;

    @ApiModelProperty(value = "组织ID", required = true)
    private String orId;

    @ApiModelProperty(value = "组织名", required = true, position = 1)
    private String roName;

    @ApiModelProperty(value = "子数据列表", required = true, position = 2)
    private List<DepartmentResp> list;
}
