package cn.com.cowain.ibms.feign.iotc.bean;

import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.rest.resp.device.TbDataResp;
import lombok.Data;


/**
 *  联动规则所需遥测数据
 *
 * @author: yanzy
 * @date: 2022/2/21 21:56
 */
@Data
public class RuleEngineTbDataResp {

    //联动规则id
    private String ruleChainId;

    //设备类型
    private DeviceType deviceType;

    //设备id
    private String deviceId;

    //遥测数据
    private TbDataResp tbDataResp;
}
