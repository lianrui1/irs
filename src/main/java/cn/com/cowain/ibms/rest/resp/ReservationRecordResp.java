package cn.com.cowain.ibms.rest.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/11/20
 */
@Data
@ApiModel("预定记录")
public class ReservationRecordResp implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 会议主题
     */
    @ApiModelProperty(value = "会议主题", required = true, position = 0)
    private String topic;

    /**
     * 从时间
     */
    @ApiModelProperty(value = "会议开始时间", required = true, position = 1)
    private LocalDateTime from;

    /**
     * 到时间
     */
    @ApiModelProperty(value = "会议结束时间", required = true, position = 2)
    private LocalDateTime to;

    /**
     * 发起人
     */
    @ApiModelProperty(value = "会议发起人", required = true, position = 3)
    private String initiator;

    /**
     * 会议记录ID
     */
    @ApiModelProperty(value = "会议ID", required = true, position = 4)
    private String id;

    /**
     * 从时间
     */
    @ApiModelProperty(value = "会议开始时间时分秒", required = true, position = 5)
    private String startMinute;

    /**
     * 到时间
     */
    @ApiModelProperty(value = "会议结束时间时分秒", required = true, position = 6)
    private String endMinute;
}
