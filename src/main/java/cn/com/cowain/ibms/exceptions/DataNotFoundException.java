package cn.com.cowain.ibms.exceptions;


import lombok.Data;

/**
 * DataNotFound Exception
 *
 * @author Hu Jingling
 * @since 2020-08-04
 */
@Data
public class DataNotFoundException extends RuntimeException {

    public DataNotFoundException(String message) {
        super(message);
    }

}
