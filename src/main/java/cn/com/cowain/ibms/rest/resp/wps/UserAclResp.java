package cn.com.cowain.ibms.rest.resp.wps;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wei.cheng
 * @date 2022/02/22 15:08
 */
@Data
@ApiModel(value = "控制用户权限")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserAclResp {

    @ApiModelProperty(value = "重命名权限，1为打开该权限，0为关闭该权限，默认为0", position = 1)
    private Integer rename;

    @ApiModelProperty(value = "历史版本权限，1为打开该权限，0为关闭该权限,默认为1", position = 2)
    private Integer history;

    @ApiModelProperty(value = "复制", position = 3)
    private Integer copy;

    @ApiModelProperty(value = "导出PDF", position = 4)
    private Integer export;

    @ApiModelProperty(value = "打印", position = 5)
    private Integer print;
}
