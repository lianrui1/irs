package cn.com.cowain.ibms.rest.distinguish;

import cn.com.cowain.ibms.service.disthinguish.AccessRulesService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/24 17:35
 */
@Slf4j
@RocketMQMessageListener(
        topic = "${cowain.ms-device.topic}-ms-device-ks-across-record-door",
        consumerGroup = "door",
        selectorExpression = "*"
)
public class OpenDoorConsumer implements RocketMQListener<String> {

    @Resource
    private AccessRulesService accessRulesService;

    @Override
    public void onMessage(@RequestBody String data) {
        log.info("旷视云回调数据" + data);
        // 解析数据
        JSONObject jsonObject = JSON.parseObject(data);
        JSONArray jsonIndex = jsonObject.getJSONArray("data");
        JSONObject partDaily = jsonIndex.getJSONObject(0);
        accessRulesService.open(partDaily);

    }
}
