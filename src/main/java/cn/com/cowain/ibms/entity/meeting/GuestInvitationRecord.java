package cn.com.cowain.ibms.entity.meeting;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.enumeration.meeting.GuestInvitationStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author Yang.Lee
 * @date 2021/6/24 18:45
 */
@Data
@Entity
@Table(name = "ibms_meeting_guest_invitation_record")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_meeting_guest_invitation_record", comment = "访客邀请记录表")
@Where(clause = "is_del=0")
public class GuestInvitationRecord extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "invitation_card_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 邀请卡ID'")
    private InvitationCard invitationCard;

    @Enumerated(EnumType.STRING)
    @Column(name = "invitation_status", nullable = false, columnDefinition = "varchar(20) COMMENT '邀请状态。DEFAULT 待反馈； ACCEPTED 已接受； REJECTED 已拒绝'")
    private GuestInvitationStatus guestInvitationStatus;

    @Column(name = "name", nullable = false, columnDefinition = "varchar(20) COMMENT '访客姓名'")
    private String name;

    @Column(name = "mobile", nullable = false, columnDefinition = "varchar(20) COMMENT '访客手机号码'")
    private String mobile;

    @Column(name = "code", nullable = false, columnDefinition = "varchar(40) COMMENT '访客邀请记录码'")
    private String code;

    @Column(name = "inviter_code", columnDefinition = "varchar(40) COMMENT '访客邀请人code'")
    private String inviterCode;
}
