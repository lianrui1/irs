package cn.com.cowain.ibms.rest.resp.office;

import cn.com.cowain.ibms.rest.resp.device.SensorDeviceResp;
import cn.com.cowain.ibms.rest.resp.device.TbDataResp;
import cn.com.cowain.ibms.rest.resp.iot.DeviceWechatResp;
import cn.com.cowain.ibms.rest.resp.working_mode.WorkingModePageResp;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/16 15:15
 */
@Data
public class IntelligentOfficeH5Resp {

    @ApiModelProperty(value = "传感器设备列表")
    private List<SensorDeviceResp> sensorList;

    @ApiModelProperty(value = "传感器属性值")
    private TbDataResp tbDataResp;

    @ApiModelProperty(value = "项目名称")
    private String projectName;

    @ApiModelProperty(value = "空间名称")
    private String spaceName;

    @ApiModelProperty(value = "工作模式列表")
    private List<WorkingModePageResp> workingModeList;

    @ApiModelProperty(value = "设备列表")
    private List<DeviceWechatResp> deviceList;

}
