package cn.com.cowain.ibms.service.office;

import cn.com.cowain.ibms.rest.req.office.DeviceCommonReq;
import cn.com.cowain.ibms.rest.req.office.UserOfficeReq;
import cn.com.cowain.ibms.rest.resp.office.OccupyOfficeResp;
import cn.com.cowain.ibms.rest.resp.office.TbDataDetailResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;

/**
 * @author feng
 * @title: UserIntelligentOfficeService
 * @projectName bims
 * @Date 2021/12/9 14:14
 */
public interface UserIntelligentOfficeService {

     ServiceResult saveGuide();

     ServiceResult  hasControlOffice();
     ServiceResult saveOffice(UserOfficeReq req);
     void editOffice(UserOfficeReq req);
     OccupyOfficeResp findOneOffice(UserOfficeReq req);
     ServiceResult  findNewDevice(String officeId) ;
     ServiceResult showDevice(String officeId);
     ServiceResult hideDevice(DeviceCommonReq req, String officeId) ;
     TbDataDetailResp sensor(String officeId);
     Boolean powerScope(String officeId);
     ServiceResult filterDevice(String officeId) ;

}
