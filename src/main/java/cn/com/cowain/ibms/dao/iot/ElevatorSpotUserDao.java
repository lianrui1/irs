package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.ElevatorSpotUser;
import cn.com.cowain.ibms.enumeration.iot.SpaceType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ElevatorSpotUserDao extends JpaRepository<ElevatorSpotUser, String>, JpaSpecificationExecutor<ElevatorSpotUser> {

    // 根据工号获取人员所有梯控权限
    List<ElevatorSpotUser> findByUserHrId(String empNo);

    // 根据空间和工号获取人员
    Optional<ElevatorSpotUser> findByUserHrIdAndSpaceId(String empNo, String id);


    List<ElevatorSpotUser> findByUserHrIdAndSpaceParentIdAndSpaceSpaceType(String userHrId, String id, SpaceType floor);

    /**
     * 统计楼梯点人员数量
     *
     * @param elevatorSpotIdList
     * @return
     */
    @Query(
            value = "select elevator_spot_id, count(distinct user_hr_id) from iot_elevator_spot_user where elevator_spot_id in (:elevatorSpotIdList) " +
                    "and is_del = 0 group by elevator_spot_id",
            nativeQuery = true
    )
    List<Object[]> findElevatorSpotIdToPeopleNum(@Param("elevatorSpotIdList") List<String> elevatorSpotIdList);

}
