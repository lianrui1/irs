package cn.com.cowain.ibms.service.space;

import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.space.ProjectAreaEditReq;
import cn.com.cowain.ibms.rest.req.space.ProjectPageReq;
import cn.com.cowain.ibms.rest.req.space.ProjectReq;
import cn.com.cowain.ibms.rest.resp.space.ProjectAreaResp;
import cn.com.cowain.ibms.rest.resp.space.ProjectResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;

import java.util.List;

/**
 * 项目管理逻辑处理service
 *
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2020/12/21 13:40
 */
public interface ProjectService {

    String DEFAULT_PROJECT_NUMBER = "00000000";

    /*
     *查询所有项目列表
     */
    List<ProjectResp> searchProjectList();


    /*
     *
     *根据项目名称或者项目编号 page size 模糊查询
     */

    /**
     * 查询项目列表（分页）
     *
     * @param req 参数
     * @return 项目列表
     * @author Yang.Lee
     * @date 2021/11/11 14:15
     **/
    PageBean<ProjectResp> search(ProjectPageReq req);


    /**
     * 新增项目信息
     *
     * @param projectReq
     * @return
     */
    ServiceResult save(ProjectReq projectReq);


    /**
     * 根据项目ID查询项目信息
     *
     * @param id
     * @return
     */
    ServiceResult get(String id);

    /**
     * 根据项目名称或者编号模糊查询
     *
     * @return
     */
    List<ProjectResp> findProjectList(String projectName);

    /**
     * 根据ID名更新项目记录
     *
     * @return
     */
    ServiceResult update(String id, ProjectReq projectReq);

    /**
     * 查询项目区域列表
     *
     * @return
     */
    List<ProjectAreaResp> searchProjectAreaList();

    /**
     * 删除项目
     *
     * @param id 项目ID
     * @return 业务结果
     * @author Yang.Lee
     * @date 2021/3/27 13:48
     **/
    ServiceResult delete(String id);

    /**
     * PC端查询项目区域列表
     *
     * @return
     */
    List<ProjectAreaResp> searchPcProjectAreaList();

    /**
     * 创建项目区域
     *
     * @param req 参数
     * @return 结果，成功是返回数据主键id
     * @author Yang.Lee
     * @date 2021/11/9 9:32
     **/
    ServiceResult createProjectArea(ProjectAreaEditReq req);

    /**
     * 获取项目区域列表
     *
     * @return 项目区域列表
     * @author Yang.Lee
     * @date 2021/11/9 9:33
     **/
    List<ProjectAreaResp> getProjectArea();
}
