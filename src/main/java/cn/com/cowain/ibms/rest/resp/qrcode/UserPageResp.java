package cn.com.cowain.ibms.rest.resp.qrcode;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/21 9:45
 */
@Data
@Builder
@ApiModel("门磁人员分页数据")
public class UserPageResp {

    @ApiModelProperty(value = "序号")
    private Integer seq;

    @ApiModelProperty(value = "人员姓名", position = 2)
    private String name;

    @ApiModelProperty(value = "人员工号", position = 3)
    private String jobNum;

    @ApiModelProperty(value = "部门", position = 4)
    private String dept;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "通行生效时间", position = 6)
    private LocalDateTime startTime;
}
