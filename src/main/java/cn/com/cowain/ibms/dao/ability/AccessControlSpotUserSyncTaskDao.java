package cn.com.cowain.ibms.dao.ability;

import cn.com.cowain.ibms.entity.iot.AccessControlSpotUserSyncTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccessControlSpotUserSyncTaskDao extends JpaRepository<AccessControlSpotUserSyncTask, String>, JpaSpecificationExecutor<AccessControlSpotUserSyncTask> {
    List<AccessControlSpotUserSyncTask> findBySpotId(String id);

    List<AccessControlSpotUserSyncTask> findBySpotIdOrderByCreatedTimeDesc(String spotId);
}
