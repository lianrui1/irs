package cn.com.cowain.ibms.service.qrcode;

import cn.com.cowain.ibms.entity.distinguish.StaffGroup;
import cn.com.cowain.ibms.entity.qrcode.QrcodeAccessRules;
import cn.com.cowain.ibms.entity.qrcode.QrcodeAccessRulesToStaffGroup;
import cn.com.cowain.ibms.service.bean.ServiceResult;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/4/7 10:31
 */
public interface QrcodeAccessRulesToStaffGroupService {

    /**
     * 保存权限与用户组关系
     *
     * @param qrCodeAccessRules     二维码权限
     * @param staffGroupCollections 用户组
     * @return 操作结果
     * @author Yang.Lee
     * @date 2021/4/7 10:32
     **/
    void saveGroupIdsByQrcodeRule(@NotNull QrcodeAccessRules qrCodeAccessRules, @NotNull Collection<StaffGroup> staffGroupCollections);

    /**
     * 根据权限查询对应关系列表
     *
     * @param ruleId 权限ID
     * @return 权限关系list
     * @author Yang.Lee
     * @date 2021/4/8 10:04
     **/
    List<QrcodeAccessRulesToStaffGroup> getListByRules(String ruleId);

    /**
     * 根据门磁id查询关系列表
     *
     * @param doorMagneticId 门磁ID
     * @return 权限关系list
     * @author Yang.Lee
     * @date 2021/4/9 9:19
     **/
    List<QrcodeAccessRulesToStaffGroup> getListByDoorMagneticId(String doorMagneticId);

    /**
     * 根据权限写入关系
     *
     * @param rule     通行权限
     * @param groupIds 人员组id列表
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/3/10 15:26
     **/
    ServiceResult saveGroupIdsByRule(@NotNull QrcodeAccessRules rule, @NotNull List<StaffGroup> groupIds);

    /**
     * 删除权限关系
     *
     * @param qrcodeAccessRulesToStaffGroups 权限关系
     * @return 操作结果
     * @author Yang.Lee
     * @date 2021/5/6 10:27
     **/
    ServiceResult deleteQrcodeAccessRulesToStaffGroup(@NotEmpty QrcodeAccessRulesToStaffGroup... qrcodeAccessRulesToStaffGroups);

    /**
     * 根据人员组Id查询关系列表
     *
     * @param staffGroupId 人员组id
     * @return 关系列表
     */
    List<QrcodeAccessRulesToStaffGroup> getListByStaffGroupId(String staffGroupId);
}
