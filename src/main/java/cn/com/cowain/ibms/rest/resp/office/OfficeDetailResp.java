package cn.com.cowain.ibms.rest.resp.office;

import cn.com.cowain.ibms.rest.resp.device.AirConditionerResp;
import cn.com.cowain.ibms.rest.resp.device.CurtainResp;
import cn.com.cowain.ibms.rest.resp.device.TbDataResp;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author feng
 * @title: OfficeDetailResp
 * @projectName ibms
 * @Date 2021/12/20 15:07
 */
@Data
public class OfficeDetailResp {


    @ApiModelProperty(value = "空调列表", required = true)
    List<AirConditionerResp> airList;

    @ApiModelProperty(value = "温度，阳光，噪音，湿度检测", required = true)
    List<TbDataResp>   tbList;

    @ApiModelProperty(value = "窗帘列表", required = true)
    List<CurtainResp> cutainList;
}
