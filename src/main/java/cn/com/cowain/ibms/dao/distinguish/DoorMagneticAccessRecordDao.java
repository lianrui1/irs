package cn.com.cowain.ibms.dao.distinguish;

import cn.com.cowain.ibms.entity.distinguish.DoorMagneticAccessRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author Yang.Lee
 * @date 2021/8/17 10:07
 */
public interface DoorMagneticAccessRecordDao extends JpaRepository<DoorMagneticAccessRecord, String>, JpaSpecificationExecutor<DoorMagneticAccessRecord> {
}
