package cn.com.cowain.ibms.service.iot.impl;

import cn.com.cowain.ibms.dao.iot.AccessControlSpotUserDao;
import cn.com.cowain.ibms.dao.iot.AccessControlTimePlanDao;
import cn.com.cowain.ibms.entity.iot.AccessControlTimePlan;
import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.req.iot.AccessControlBindTimePlanReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.iot.AccessControlTimePlanResp;
import cn.com.cowain.ibms.service.iot.AccessControlTimePlanService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.ibms.utils.RedisUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @author wei.cheng
 * @date 2022/09/28 16:10
 */
@Service
public class AccessControlTimePlanServiceImpl implements AccessControlTimePlanService {
    @Resource
    private AccessControlSpotUserDao accessControlSpotUserDao;
    @Resource
    private AccessControlTimePlanDao accessControlTimePlanDao;
    @Resource
    private RedisUtil redisUtil;

    @Override
    @Transactional
    public ResponseEntity<JsonResult<IdResp>> bindTimePlan(AccessControlBindTimePlanReq req) {
        boolean hasSpotUser = accessControlSpotUserDao.existsByAccessControlSpotIdAndUserHrId(req.getSpotId(), req.getUserHrId());
        if (!hasSpotUser) {
            return ResponseEntity.ok(JsonResult.error("用户还未拥有门禁点权限", null, ErrConst.E01));
        }
        AccessControlTimePlan accessControlTimePlan = accessControlTimePlanDao.findFirstByUserHrIdAndSpotIdOrderByCreatedTimeDesc(req.getUserHrId(), req.getSpotId());
        if (Objects.isNull(accessControlTimePlan)) {
            accessControlTimePlan = new AccessControlTimePlan();
            accessControlTimePlan.setSpotId(req.getSpotId());
            accessControlTimePlan.setUserHrId(req.getUserHrId());
        }
        accessControlTimePlan.setWorkDays(req.getWorkDays());
        accessControlTimePlanDao.save(accessControlTimePlan);
        deleteCacheInfo(req.getSpotId(), req.getUserHrId());
        return ResponseEntity.ok(JsonResult.ok("OK", new IdResp(accessControlTimePlan.getId())));
    }


    @Override
    @Transactional
    public ResponseEntity<JsonResult<IdResp>> deleteTimePlan(String spotId, String userHrId) {
        List<AccessControlTimePlan> accessControlTimePlanList = accessControlTimePlanDao.findByUserHrIdAndSpotId(userHrId, spotId);
        if (CollUtil.isEmpty(accessControlTimePlanList)) {
            return ResponseEntity.ok(JsonResult.ok("OK", null));
        }
        accessControlTimePlanList.forEach(accessControlTimePlan -> accessControlTimePlan.setIsDelete(1));
        accessControlTimePlanDao.saveAll(accessControlTimePlanList);
        deleteCacheInfo(spotId, userHrId);
        return ResponseEntity.ok(JsonResult.ok("OK", new IdResp(accessControlTimePlanList.get(0).getId())));
    }

    /**
     * 删除缓存信息
     *
     * @param spotId
     * @param userHrId
     */
    private void deleteCacheInfo(String spotId, String userHrId) {
        TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
            @Override
            public void afterCommit() {
                // 事务提交后删除删除可能存在的旧缓存信息
                redisUtil.del(redisUtil.createAccessControlTimePlanKey(IConst.ACCESS_CONTROL_TIME_PLAN, spotId, userHrId, RedisUtil.Separator.COLON));
            }
        });
    }

    @Override
    public AccessControlTimePlanResp getAccessControlTimePlan(String spotId, String userHrId) {
        String key = redisUtil.createAccessControlTimePlanKey(IConst.ACCESS_CONTROL_TIME_PLAN, spotId, userHrId, RedisUtil.Separator.COLON);
        AccessControlTimePlanResp result = null;
        if (redisUtil.hasKey(key)) {
            Object value = redisUtil.get(key);
            if (ObjectUtils.isNotEmpty(value)) {
                result = JSON.parseObject(JSON.toJSONString(value), AccessControlTimePlanResp.class);
            }
        } else {
            AccessControlTimePlan accessControlTimePlan = accessControlTimePlanDao.findFirstByUserHrIdAndSpotIdOrderByCreatedTimeDesc(userHrId, spotId);
            if (Objects.isNull(accessControlTimePlan)) {
                // 不存在时间计划时，redis设空值，以防缓存穿透
                redisUtil.set(key, StrUtil.EMPTY, RedisUtil.ONE_DAY_SECOND);
            } else {
                result = new AccessControlTimePlanResp();
                result.setWorkDays(accessControlTimePlan.getWorkDays());
                redisUtil.set(key, result, RedisUtil.ONE_DAY_SECOND);
            }
        }
        return result;
    }
}
