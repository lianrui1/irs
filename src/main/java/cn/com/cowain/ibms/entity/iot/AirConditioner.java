package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 空调模型
 * @author Yang.Lee
 * @date 2021/2/18 14:11
 */
@Data
@Entity
@Table(name = "iot_air_conditioner")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_air_conditioner", comment = "空调设备表")
public class AirConditioner extends BaseEntity {

    private static final long serialVersionUID = -8487365540974994176L;

    /**
     * 开关
     */
    @Column(name = "setPower", columnDefinition = "varchar(10) COMMENT '开关'")
    private String setPower;

    /**
     * 温度
     */
    @Column(name = "temperature", columnDefinition = "integer COMMENT '温度'")
    private int temperature;

    /**
     * 风向
     */
    @Column(name = "wind_direction", columnDefinition = "varchar(50) COMMENT '风向'")
    private String windDirection;

    /**
     * 风向
     */
    @Column(name = "FanAuto", columnDefinition = "varchar(50) COMMENT '风向 手动自动'")
    private String setFanAuto;

    /**
     * 风速
     */
    @Column(name = "wind_speed", columnDefinition = "varchar(50) COMMENT '风速'")
    private String  windSpeed;

    /**
     * 模式
     */
    @Column(name = "mode", columnDefinition = "varchar(50) COMMENT '模式'")
    private String mode;

    /**
     * 模式
     */
    @Column(name = "setSwing", columnDefinition = "varchar(50) COMMENT '设置扫风模式 ，无扫风，上下扫风，左右扫风，上下左右扫风 noSwing,updownSwing,leftrightSwing,aroundSwing'")
    private String setSwing;

    /**
     * iot 设备
     */
    @OneToOne
    @JoinColumn(name = "device_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK。设备表主键id'")
    private IotDevice device;

    @Column(name = "isOne", nullable = false, columnDefinition = "tinyint(1) default 0 COMMENT '空调是否单个命令下发'")
    private int isOne;
}
