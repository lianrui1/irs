package cn.com.cowain.ibms.rest.office;

import cn.com.cowain.ibms.rest.req.working_mode.BandingReq;
import cn.com.cowain.ibms.rest.req.working_mode.ChoiceModelReq;
import cn.com.cowain.ibms.rest.req.working_mode.WelcomeWordReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.iot.QrCodeResp;
import cn.com.cowain.ibms.rest.resp.office.TbMeetingDetailResp;
import cn.com.cowain.ibms.rest.resp.room.RoomBandInfoResp;
import cn.com.cowain.ibms.rest.resp.room.RoomResp;
import cn.com.cowain.ibms.service.ReservationRecordService;
import cn.com.cowain.ibms.service.RoomService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.meeting.IntelligentMeetingAppService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author feng
 * @title: MeetingRoomController
 * @projectName ibms
 * @Date 2021/12/27 18:33
 * 智能会议室详情，创建会议，会议室绑定等
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/meetingroom")
@Api(tags = IConst.INTELLIGENT_MEETING)
public class MeetingRoomController {

    @Autowired
    RoomService roomService;

    @Resource
    private IntelligentMeetingAppService intelligentMeetingAppService;

    @Resource
    private ReservationRecordService reservationRecordService;

    @GetMapping("/td/{roomId}")
    @ApiOperation(value = "会议室详情传感器信息查询", tags = {IConst.INTELLIGENT_MEETING, IConst.V22})
    public ResponseEntity<JsonResult<TbMeetingDetailResp>> tdDetail(@PathVariable String roomId) {
        return null;
    }

    @GetMapping("/roominfo/{roomId}")
    @ApiOperation(value = "办公室详情", tags = {IConst.INTELLIGENT_MEETING, IConst.V22})
    public ResponseEntity<JsonResult<RoomBandInfoResp>> roomInfo(@PathVariable String roomId) {
        return null;
    }

    /**
     * 办公室列表查询
     *
     * @author:yanyz
     * @Date 2021/12/28
     */
    @GetMapping("/roomlistAll/{projectid}")
    @ApiOperation(value = "办公室列表", tags = {IConst.INTELLIGENT_MEETING, IConst.V22})
    public ResponseEntity<JsonResult<List<RoomResp>>> roomList(@PathVariable String projectid) {
        List<RoomResp> result = roomService.findBySpaceProjectIdAndSpaceIsShow(projectid);
        return ResponseEntity.ok(JsonResult.ok("OK", result));
    }


    /**
     * 二维码生成
     *
     * @author:yanyz
     * @Date 2022/1/5
     */
    @GetMapping("/roomlist/{roomId}")
    @ApiOperation(value = "二维码生成", tags = {IConst.INTELLIGENT_MEETING, IConst.V22})
    public ResponseEntity<JsonResult<QrCodeResp>> getQRCode(@ApiParam(name = "roomId", value = "办公室ID", required = true) @PathVariable String roomId) {
        ServiceResult result = intelligentMeetingAppService.createQrCode(roomId);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E14));
        }
        QrCodeResp resp = (QrCodeResp) result.getObject();
        return ResponseEntity.ok(JsonResult.ok("OK", resp));
    }

    @PutMapping("/choice")
    @ApiOperation(value = "选择场景-web", tags = {IConst.INTELLIGENT_MEETING, IConst.V22})
    public ResponseEntity<JsonResult<String>> choice(@RequestBody ChoiceModelReq req) {
        return null;
    }

    /**
     * 设备与办公室绑定
     *
     * @author:yanyz
     * @Date 2021/12/30
     */
    @PostMapping("/banding")
    @ApiOperation(value = "设备与办公室绑定", tags = {IConst.INTELLIGENT_MEETING, IConst.V22})
    public ResponseEntity<JsonResult<IdResp>> banding(@RequestBody BandingReq req) {
        ServiceResult banding = intelligentMeetingAppService.banding(req);
        if (!banding.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(banding.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.ok("OK", (IdResp) banding.getObject()));
    }

    /**
     * @author:yanyz
     * @Date 2021/12/31
     */
    @PutMapping("/banding")
    @ApiOperation(value = "修改设备与办公室绑定", tags = {IConst.INTELLIGENT_MEETING, IConst.V22})
    public ResponseEntity<JsonResult<IdResp>> updateBanding(@RequestBody BandingReq req) {
        ServiceResult updateBanding = intelligentMeetingAppService.updateBanding(req);
        if (!updateBanding.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(updateBanding.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.ok("OK", (IdResp) updateBanding.getObject()));
    }

    /**
     * 设置欢迎语
     *
     * @author:yanyz
     * @Date 2021/12/31
     */
    @PostMapping("/welcome")
    @ApiOperation(value = "设置欢迎语", tags = {IConst.INTELLIGENT_MEETING, IConst.V22, IConst.V25})
    public ResponseEntity<JsonResult<String>> welCome(@Validated @RequestBody WelcomeWordReq req, BindingResult bindingResult) {
        ServiceResult welcome = reservationRecordService.welcome(req);
        if (!welcome.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(welcome.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.ok("OK", (String) welcome.getObject()));
    }
}
