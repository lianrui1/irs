package cn.com.cowain.ibms.enumeration;

/**
 * 警戒抓拍状态
 *
 * @author: yanzy
 * @date: 2022/4/13 10:04
 */
public enum AlertCaptureorStatus {

    VIEWED("已查看"),

    NOT_VIEWED("未查看");

    private String name;

    AlertCaptureorStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}