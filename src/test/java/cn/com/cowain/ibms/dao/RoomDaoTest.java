package cn.com.cowain.ibms.dao;

import cn.com.cowain.ibms.entity.Room;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.resp.RoomWithReservationResp;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/3 19:37
 */
@Slf4j
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RoomDaoTest {

    @Autowired
    private RoomDao roomDao;

    @Test
    public void test0() {
        Assertions.assertNotNull(roomDao);
    }

    @Test
    public void test1() {
        Room room = new Room();
        room.setName("测试会议室1");
        room.setCapacity(20);
        room.setBuilding("A栋");
        room.setFloor("3F");
        room.setIsOpen(1);
        room.setSeq(1);
        roomDao.save(room);
    }

    @Test
    public void test2() {
        Optional<Room> roomOptional = roomDao.findById("a6599669-aa71-42fe-b131-1ad9d34cc07b");
        boolean present = roomOptional.isPresent();
        if (present) {
            Room room = roomOptional.get();
            System.out.println("room:" + room);
        } else {
            System.out.println("对象不存在");
        }

    }

    @Test
    public void test3() {
        //先查
        Optional<Room> roomOptional = roomDao.findById("a6599669-aa71-42fe-b131-1ad9d34cc07b");
        boolean present = roomOptional.isPresent();
        if (present) {
            Room room = roomOptional.get();
            System.out.println("room:" + room);
            room.setCapacity(room.getCapacity()+1);
            roomDao.save(room);
        }
    }


    @Test
    public void test4Delete111() {
        //先查
        Optional<Room> roomOptional = roomDao.findById("a6599669-aa71-42fe-b131-1ad9d34cc07b");
        boolean present = roomOptional.isPresent();
        if (present) {
            Room room = roomOptional.get();
            System.out.println("room:" + room);
            room.setCapacity(room.getCapacity()+1);
            roomDao.delete(room);
        }
    }

    @Test
    public void test4Max() {
        //先查
        Optional<Room> roomOptional = roomDao.findById("a6599669-aa71-42fe-b131-1ad9d34cc07b");
        boolean present = roomOptional.isPresent();
        if (present) {
            Room room = roomOptional.get();
            System.out.println("room:" + room);
            room.setCapacity(room.getCapacity()+1);
            roomDao.delete(room);
//            roomDao.se
        }
    }

    @Test
    public void test4Delete22() {
        roomDao.deleteById("b612d66f-bbff-42ca-be28-6b876921dff5");
    }

    @Test
    public void testFindAll() {
        System.out.println(roomDao.findAll());

    }

    @Test
    public void testUsedTimeListSearch(){

        String empNo = "CWA3238";
        String projectId = "";
        int page = 3;
        int size = 10;

        Sort.Order order = new Sort.Order(Sort.Direction.DESC,"used_time");
        Sort.Order order2 = new Sort.Order(Sort.Direction.ASC,"seq");

        Sort sort = Sort.by(order, order2);

        List<Room> roomPage = roomDao.searchWithUsedTime(empNo, projectId);



        PageBean<Room> pageBean = new PageBean<>(page, size);

        int pageSize = roomPage.size();
        pageBean.setTotalElements(pageSize);
        pageBean.setTotalPages(pageSize / size + 1);

        int subStart = (page - 1) * size;
        int subEnd = subStart + size;

        if(subStart > pageSize){
            subStart = pageSize;
        }

        if(subEnd > pageSize){
            subEnd = pageSize;
        }

        pageBean.setList(roomPage.subList(subStart, subEnd));

        log.info(JSON.toJSONString(pageBean));
    }

}