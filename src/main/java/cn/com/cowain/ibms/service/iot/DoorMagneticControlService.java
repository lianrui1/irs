package cn.com.cowain.ibms.service.iot;

import cn.com.cowain.ibms.service.bean.ServiceResult;

/**
 * @author Yang.Lee
 * @date 2021/4/19 10:45
 */
public interface DoorMagneticControlService {

    /**
     * 门磁开门
     *
     * @param doorMagneticNum 门磁编号
     * @return
     */
    ServiceResult open(String doorMagneticNum);
}
