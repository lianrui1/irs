package cn.com.cowain.ibms.feign.task_center.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wei.cheng
 * @date 2022/03/10 15:05
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateTaskResp {
    /**
     * task.id
     */
    private Long id;
}
