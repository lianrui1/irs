package cn.com.cowain.ibms.rest.req.distinguish;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author Yang.Lee
 * @date 2021/7/8 15:02
 */
@Data
@ApiModel("添加设备用户请求")
public class DeviceStaffReq {

    @ApiModelProperty(value = "员工工号", required = true)
    private String empNo;

    @ApiModelProperty(value = "设备ID", required = true)
    private String deviceId;

    @ApiModelProperty(value = "面板机权限开始时间")
    private LocalDateTime startTime;

    @ApiModelProperty(value = "面板机权限结束时间")
    private LocalDateTime endTime;


}
