package cn.com.cowain.ibms.exceptions;

/**
 * 业务逻辑不匹配  异常
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/7/20
 */
public class BizLogicNotMatchException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public BizLogicNotMatchException(String message) {
        super(message);
    }
}
