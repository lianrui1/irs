package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.enumeration.iot.DoorMagneticSource;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 门磁设备表
 *
 * @author cuiEnming
 * @date 2020/11/19  18:51
 * @since s2
 */

@Data
@Entity
@Table(name = "iot_door_magnetic")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_door_magnetic", comment = "门磁")
public class DoorMagnetic extends BaseEntity {

    /**
     *编号
     */
    @Column(name = "number", nullable = false, columnDefinition = "varchar(50) COMMENT '编号'")
    private String number;

    /**
     *名称
     */
    @Column(name = "name", columnDefinition = "varchar(30) COMMENT '名称'")
    private String name;

    /**
     * 门磁mac地址
     */
    @Column(name = "door_magnetic_mac", columnDefinition = "varchar(20) COMMENT '门磁设备mac地址'")
    private String doorMagneticMac;

    /**
     * iot 设备
     */
    @ManyToOne
    @JoinColumn(name = "device_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK'")
    private IotDevice device;

    /**
     * 华为ID
     */
    @Column(name = "huawei_id", columnDefinition = "varchar(50) COMMENT '华为云硬件ID'")
    private String huaweiId;

    /**
     * 设别源
     **/
    @Enumerated(EnumType.STRING)
    @Column(name = "source", nullable = false, columnDefinition = "varchar(20) COMMENT '设备源'")
    private DoorMagneticSource doorMagneticSource;
}
