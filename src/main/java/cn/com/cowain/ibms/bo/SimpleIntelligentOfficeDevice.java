package cn.com.cowain.ibms.bo;

import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.enumeration.iot.UsableStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author wei.cheng
 * @date 2022/10/17 13:57
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class SimpleIntelligentOfficeDevice implements Serializable {

    /**
     * 设备可用状态
     */
    private UsableStatus usableStatus;

    /**
     * 设备别名
     */
    private String deviceAlisa;

    /**
     * 设备别名数据，json字符串
     */
    private String deviceAlisaProperties;

    private String deviceId;

    private String hwDeviceId;

    private DeviceType deviceType;

    private String sn;

    private String deviceName;

    /**
     * 华为云id
     */
    @ApiModelProperty(value = "是否在线", required = true)
    private DeviceStatus hwStatus;
    /**
     * 创建时间
     */
    private LocalDateTime createdTime;
}
