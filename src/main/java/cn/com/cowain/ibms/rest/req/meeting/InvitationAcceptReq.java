package cn.com.cowain.ibms.rest.req.meeting;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2021/6/25 9:30
 */
@Data
@ApiModel("访客接受邀请数据请求对象")
public class InvitationAcceptReq {

    @ApiModelProperty(value = "受邀者姓名", required = true, example = "张三", position = 1)
    private String name;

    @ApiModelProperty(value = "受邀者单位", required = true, example = "中南海", position = 2)
    private String company;

    @ApiModelProperty(value = "受邀者照片", required = true, example = "xxxxxxxx", position = 3)
    private String photoUrl;

    @ApiModelProperty(value = "受邀者手机号码", required = true, example = "12300990099", position = 4)
    private String mobile;

    @ApiModelProperty(value = "访客邀请记录码", required = true, example = "", position = 5)
    private String code;

    @ApiModelProperty(value = "短信验证码", required = true, example = "", position = 6)
    private String verificationCode;
}
