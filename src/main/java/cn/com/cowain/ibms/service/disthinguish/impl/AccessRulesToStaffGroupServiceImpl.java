package cn.com.cowain.ibms.service.disthinguish.impl;

import cn.com.cowain.ibms.dao.distinguish.AccessRulesToStaffGroupDao;
import cn.com.cowain.ibms.entity.distinguish.AccessRules;
import cn.com.cowain.ibms.entity.distinguish.AccessRulesToStaffGroup;
import cn.com.cowain.ibms.entity.distinguish.StaffGroup;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.disthinguish.AccessRulesToStaffGroupService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * 行权限 - 人员组关系Service
 *
 * @author Yang.Lee
 * @date 2021/3/10 15:27
 */
@Service
public class AccessRulesToStaffGroupServiceImpl implements AccessRulesToStaffGroupService {

    @Resource
    private AccessRulesToStaffGroupDao accessRulesToStaffGroupDao;

    /**
     * 根据权限写入关系
     *
     * @param rule   通行权限
     * @param groups 人员组列表
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/3/10 15:26
     **/
    @Override
    @Transactional
    public ServiceResult saveGroupIdsByRule(@NotNull AccessRules rule, @NotNull List<StaffGroup> groups) {

        List<AccessRulesToStaffGroup> access2GroupList = new ArrayList<>();
        groups.forEach(group -> {

            AccessRulesToStaffGroup a2s = new AccessRulesToStaffGroup();
            a2s.setRule(rule);
            a2s.setGroup(group);
            access2GroupList.add(a2s);

        });
        accessRulesToStaffGroupDao.saveAll(access2GroupList);
        return ServiceResult.ok();
    }
}
