package cn.com.cowain.ibms.service;

import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.rest.resp.EhrDeptResp;
import cn.com.cowain.ibms.rest.resp.EhrStaffResp;
import cn.com.cowain.ibms.rest.resp.EhrStaffRespPage;
import cn.com.cowain.ibms.rest.resp.EhrStaffRespRoot;
import com.alibaba.fastjson.JSONObject;

import java.util.List;
import java.util.Map;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/4 15:14
 */
public interface EhrRemoteService {

    /**
     * 查询部门树
     *
     * @return
     */
    EhrDeptResp getDepartmentTree();

    /**
     * 根据部门ID查询员工
     *
     * @param deptId 部门ID
     * @return EhrStaffRespPage 分页数据
     */
    EhrStaffRespPage getUserByDeptId(String deptId);

    /**
     * 根据员工姓名模糊查找
     *
     * @param name
     * @return
     */
    EhrStaffRespRoot findSysUserByNameContains(String name);

    /**
     * 根据 stId 查询员工信息
     *
     * @param sysId
     * @return
     */
    SysUser getInfoById(String sysId);

    /**
     * 根据 User ID 查询员工信息
     *
     * @param userId
     * @return
     */
    SysUser getInfoByUserId(String userId);

    /**
     * 根据员工工号 查询员工 openId
     *
     * @param empNoList
     * @return
     */
    List<EhrStaffResp> findByEmpNo(List<String> empNoList);

    /**
     * 检查 EHR 服务是否可用
     *
     * @return
     */
    boolean serviceCheck();

    /**
     * 通过code获得基本用户信息
     *
     * @param code code
     * @return
     */
    JSONObject getAuth2UserInfo(String code);

    /**
     * 获取多层部门
     *
     * @param jobNumber
     * @return Map<工号, 层级字符串>
     */
    Map<String, String> getUserDept(String[] jobNumber);

    /**
     * 根据员工工号查询用户信息
     *
     * @param hrId 员工工号
     * @return 员工数据
     */
    SysUser getUserByHrId(String hrId);
}
