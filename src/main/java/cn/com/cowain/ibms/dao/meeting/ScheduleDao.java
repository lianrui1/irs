package cn.com.cowain.ibms.dao.meeting;

import cn.com.cowain.ibms.entity.meeting.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author Yang.Lee
 * @date 2021/6/21 19:55
 */
@Repository
public interface ScheduleDao extends JpaRepository<Schedule, String> {

    Optional<Schedule> findByEmpNoAndReservationRecordId(String empNo, String reservationId);
}
