package cn.com.cowain.ibms.rest.resp.iot;

import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.lang.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/2/18 10:00
 */
@Data
@ApiModel("微信端设备数据响应对象")
public class DeviceWechatResp {

    @ApiModelProperty(value = "设备Id", required = true)
    private String deviceId;

    @ApiModelProperty(value = "设备名称", required = true, position = 1, example = "空调XO0001")
    private String deviceName;

    @ApiModelProperty(value = "产品名称", required = true, position = 2, example = "空调")
    private String productName;

    @ApiModelProperty(value = "设备状态（ONLINE：设备在线。OFFLINE：设备离线。ABNORMAL：设备异常。INACTIVE：设备未激活。FROZEN：设备冻结）",
            required = true, position = 3, example = "ONLINE")
    private DeviceStatus status;

    @ApiModelProperty(
            value = "设备类型（DEFAULT：未知类型设备；DOOR_PLATE：门牌；DOOR_MAGNETIC：门磁，LIGHT：灯；SCREEN：屏幕；" +
                    "AIR_CONDITIONER：空调；ILLUMINATION_SENSING：光照度传感器）",
            required = true,
            position = 4,
            example = "DOOR_PLATE")
    private DeviceType type;

    @ApiModelProperty(value = "类型名称")
    private String typeName;

    @ApiModelProperty(value = "空调温度")
    private Integer airTemp;


    /**
     * 对象转换，将IotDevice对象转为DeviceWechatResp对象
     *
     * @param source IotDevice对象
     * @return DeviceWechatResp对象
     */
    public static DeviceWechatResp convert(@NonNull IotDevice source) {

        DeviceWechatResp target = new DeviceWechatResp();

        target.setDeviceId(source.getId());
        target.setDeviceName(source.getDeviceName());
        target.setStatus(source.getHwStatus());
        target.setType(source.getDeviceType());
        target.setTypeName(source.getDeviceType().getName());
        target.setProductName(source.getHwProductName());

        return target;
    }

    /**
     * 对象转换,将IotDevice集合转为DeviceWechatResp集合
     *
     * @param sourceList 待转换的IotDevice集合
     * @return DeviceWechatResp集合
     */
    public static List<DeviceWechatResp> convertList(Collection<IotDevice> sourceList) {

        // 如果是元数据是空则返回一个空集合
        if (sourceList == null || sourceList.isEmpty()) {
            return new ArrayList<>();
        }

        List<DeviceWechatResp> targetList = new ArrayList<>();
        sourceList.forEach(source -> targetList.add(convert(source)));

        return targetList;
    }
}
