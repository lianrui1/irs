package cn.com.cowain.ibms.feign.task_center.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wei.cheng
 * @date 2022/03/10 15:07
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateTaskReq {
    /**
     * 流程到达审批人时间，unix时间秒级戳
     */
    private Long arrivalTimestamp;
    /**
     * 任务申请人-中文名
     */
    private String claimer;
    /**
     * 任务申请人-工号
     */
    private String claimerUid;
    /**
     * 发起人视角流程详情地址
     */
    private String claimerUrl;
    /**
     * 根据模板的定义需要传入的数据，格式：K-V
     */
    private Object data;
    /**
     * 系统来源。各个系统英文简称。默认为"IMBS"
     */
    private String fromSys;
    /**
     * 审批环节状态，`1`审批中，`2`已批准，`3`已驳回，`4`已撤回
     */
    private Integer operateResult;
    /**
     * 审批任务创建时间，unix时间秒级戳
     */
    private Long startTimestamp;
    /**
     * 审批任务状态，`1`代表待办，`2`代表已办
     */
    private Integer status;
    /**
     * 审批任务单号
     */
    private String taskId;
    /**
     * 模版ID
     */
    private String templateId;
    /**
     * 审批任务类型-中文名
     */
    private String type;
    /**
     * 用户工号-给谁的任务
     */
    private String ucId;
    /**
     * 用户名-给谁的任务
     */
    private String ucName;
    /**
     * 任务详情地址
     */
    private String url;
}
