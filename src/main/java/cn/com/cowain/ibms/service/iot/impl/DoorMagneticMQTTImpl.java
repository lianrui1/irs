package cn.com.cowain.ibms.service.iot.impl;

import cn.com.cowain.ibms.dao.iot.DoorMagneticDao;
import cn.com.cowain.ibms.entity.iot.DoorMagnetic;
import cn.com.cowain.ibms.feign.mqtt_connector.ks.MagneticKSApi;
import cn.com.cowain.ibms.feign.mqtt_connector.nt.MagneticNTApi;
import cn.com.cowain.ibms.service.bean.ServiceMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.iot.DoorMagneticControlService;
import cn.com.cowain.sfp.comm.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Optional;

/**
 * 门磁开门（mqtt）mqtt
 * @author Yang.Lee
 * @date 2021/1/28 16:18
 */
@Slf4j
@Service("doorMagneticMQTTService")
public class DoorMagneticMQTTImpl implements DoorMagneticControlService {

    @Autowired
    private DoorMagneticDao doorMagneticDao;

    @Resource
    private MagneticKSApi magneticKSApi;

    @Resource
    private MagneticNTApi magneticNTApi;

    /**
     * 门磁开门
     *
     * @param doorMagneticNum 门磁编号
     * @return
     */
    @Override
    public ServiceResult open(String doorMagneticNum) {

        log.debug("进入mqtt开门方法，门磁编号{}", doorMagneticNum);

        Optional<DoorMagnetic> doorMagneticOptional = doorMagneticDao.findByNumber(doorMagneticNum);
        if(!doorMagneticOptional.isPresent()){
            return ServiceResult.error(ServiceMessage.NOT_FOUNT_DOOR_MAGNETISM);
        }

        DoorMagnetic doorMagnetic = doorMagneticOptional.get();

        JsonResult<String> result;
        // 因为操作错误，KS-NEW025发到了南通。所以这个码往南通发请求
        if(doorMagneticNum.startsWith("KS") && !doorMagneticNum.equals("KS-NEW025")){
            log.debug("向昆山MQTT发送消息");
            result = magneticKSApi.open(doorMagnetic.getDoorMagneticMac());
        } else{
            log.debug("向南通MQTT发送消息");
            result = magneticNTApi.open(doorMagnetic.getDoorMagneticMac());
        }

        return result.getStatus() == 1 ? ServiceResult.ok() : ServiceResult.error(result.getMsg());
    }
}
