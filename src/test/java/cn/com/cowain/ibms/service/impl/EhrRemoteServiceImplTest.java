package cn.com.cowain.ibms.service.impl;

import cn.com.cowain.ibms.rest.bean.EhrDeptBean;
import cn.com.cowain.ibms.rest.resp.EhrDeptResp;
import cn.com.cowain.ibms.service.EhrRemoteService;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author: cuiEnming
 * @title: EhrRemoteServiceImplTest
 * @date 2020/11/24  15:29
 * @since s2
 */
@SpringBootTest
class EhrRemoteServiceImplTest {
    @Autowired
    private EhrRemoteService ehrRemoteService;


    @Test
    void getDepartmentTree() {
        EhrDeptResp departmentTree = ehrRemoteService.getDepartmentTree();
        List<EhrDeptBean> treeList = departmentTree.getTreeList();
        for (EhrDeptBean bean : treeList) {
            print("", bean);
        }
    }

    private void print(String parentStr, EhrDeptBean ehrDeptBean) {
        if(StringUtils.isNotBlank(parentStr)&&parentStr.contains("科瑞恩自动化")){
            System.out.print(parentStr+"/");
        }
        System.out.println(ehrDeptBean.getRoName());

        List<EhrDeptBean> treeList = ehrDeptBean.getList();
        if(treeList != null){
            for (EhrDeptBean bean : treeList) {
                if(StringUtils.isNoneBlank(parentStr)){
                    System.out.print("存入redis的key:"+bean.getRoName());
                    System.out.print(", 存入redis的value:");
                    print(parentStr+"/"+ehrDeptBean.getRoName(), bean);
                }else{
                    System.out.print("存入redis的key:"+bean.getRoName());
                    System.out.print(", 存入redis的value:");
                    print(ehrDeptBean.getRoName(), bean);
                }
            }
        }
    }

}