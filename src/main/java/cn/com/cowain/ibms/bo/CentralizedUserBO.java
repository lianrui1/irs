package cn.com.cowain.ibms.bo;

import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.enumeration.iot.ApproverType;
import cn.com.cowain.ibms.enumeration.iot.DeviceControlStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class CentralizedUserBO {
    @ApiModelProperty(value = "事务ID（唯一标识）", required = true)
    @NotBlank
    private String traceId;

    @ApiModelProperty(value = "集控码", required = true)
    @NotBlank
    private String centralizedCode;

    @ApiModelProperty(value = "最新执行的设备名称", required = true)
    @NotBlank
    private String deviceName;

    @Column(name = "hrId", nullable = false, columnDefinition = "varchar(30) COMMENT '人员工号'")
    private String hrId;

    @Column(name = "name", nullable = false, columnDefinition = "varchar(30) COMMENT '人员名称'")
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false, columnDefinition = "varchar(30) COMMENT '人员类型'")
    private ApproverType type;

    private String controlName;

    @Enumerated(EnumType.STRING)
    private DeviceControlStatus status;

    private List<IotDevice> iotDevices;
}
