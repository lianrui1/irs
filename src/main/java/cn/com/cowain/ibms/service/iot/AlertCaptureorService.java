package cn.com.cowain.ibms.service.iot;

import cn.com.cowain.ibms.mq.bean.AlertCaptureorMessage;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.iot.AlertCaptureorDetailReq;
import cn.com.cowain.ibms.rest.req.iot.AlertCaptureorGetExistOfDayQueryReq;
import cn.com.cowain.ibms.rest.req.iot.AlertCaptureorReq;
import cn.com.cowain.ibms.rest.resp.iot.AlertCaptureorResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;

/**
 * 警戒抓拍service
 *
 * @author: yanzy
 * @date: 2022/4/13 14:13
 */
public interface AlertCaptureorService {

    /**
     * 查询警戒抓拍列表(分页)
     *
     * @author: yanzy
     * @data: 2022/4/13 14:04:48
     */
    PageBean<AlertCaptureorResp> getPage(AlertCaptureorReq req);

    /**
     *  根据id查询
     *
     * @author: yanzy
     * @data: 2022/4/13 19:04:02
     */
    ServiceResult getAlertCaptureorDetail(AlertCaptureorDetailReq req);

    /**
     * 消费警戒抓拍mq消息保存数据
     *
     * @param alertCaptureorMessage
     * @author wei.cheng
     * @date 2022/4/13 4:00 下午
     **/
    ServiceResult createAlertCaptureor(AlertCaptureorMessage alertCaptureorMessage);

    /**
     * 查询日历表中日期是否存在警戒抓拍记录
     *
     * @param req
     * @author wei.cheng
     * @date 2022/4/14 10:21 上午
     **/
    ServiceResult getAlertCaptureorExistOfDay(AlertCaptureorGetExistOfDayQueryReq req);
}
