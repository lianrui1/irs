package cn.com.cowain.ibms.dao;

import cn.com.cowain.ibms.entity.SysUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/15/20
 */
@SpringBootTest
class SysUserDaoTest {

    @Autowired
    private SysUserDao sysUserDao;

    @Test
    void findBySysId() {
        Optional<SysUser> optional = sysUserDao.findBySysId("testId22");
        System.out.println(optional);
    }
}