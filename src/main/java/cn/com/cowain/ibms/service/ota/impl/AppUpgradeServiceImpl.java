package cn.com.cowain.ibms.service.ota.impl;

import cn.com.cowain.ibms.dao.ota.AppUpgradeDao;
import cn.com.cowain.ibms.entity.ota.OTAUpgrade;
import cn.com.cowain.ibms.enumeration.ota.Application;
import cn.com.cowain.ibms.enumeration.ota.UpgradeStatus;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.oat.UpgradePageReq;
import cn.com.cowain.ibms.rest.req.oat.UpgradeTaskCreateReq;
import cn.com.cowain.ibms.rest.resp.ota.FileResp;
import cn.com.cowain.ibms.rest.resp.ota.UpgradeTaskDetailResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.ota.AppUpgradeService;
import cn.com.cowain.ibms.utils.DateUtil;
import cn.com.cowain.ibms.utils.FastDfsPool;
import cn.com.cowain.ibms.utils.FileUtil;
import cn.com.cowain.sfp.xunfei.util.EncryptUtil;
import lombok.extern.slf4j.Slf4j;
import net.dongliu.apk.parser.ApkFile;
import net.dongliu.apk.parser.bean.ApkMeta;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.entity.ContentType;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.criteria.Predicate;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/4/16 10:10
 */
@Slf4j
@Service
public class AppUpgradeServiceImpl implements AppUpgradeService {

    @Autowired
    private AppUpgradeDao appUpgradeDao;

    private static final long HALF_HOUR = 60L * 0;

    @Value("${fastdfs.fileUrl}")
    private String fastDfsHttpUrl;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public PageBean<UpgradeTaskDetailResp> search(UpgradePageReq req) {
        int page = req.getPage();
        int size = req.getSize();
        // 创建分页查询对象
        Sort sort = Sort.by(req.getOrders());
        Pageable pageable = PageRequest.of(page, size, sort);

        Page<OTAUpgrade> otaUpgradePage = appUpgradeDao.findAll((root, criteriaQuery, criteriaBuilder) -> {

            // 构造查询条件
            List<Predicate> list = new ArrayList<>();

            // 升级应用
            if (StringUtils.isNotEmpty(req.getApplication())) {
                list.add(criteriaBuilder.equal(root.get("application").as(String.class), req.getApplication()));
            }
            // 版本号
            String versionOTA = req.getVersionOTA();
            if (StringUtils.isNotEmpty(versionOTA)) {
                list.add(criteriaBuilder.like(root.get("versionOTA").as(String.class), "%" + versionOTA + "%"));
            }
            // 任务状态
            if (StringUtils.isNotEmpty(req.getStatus())) {
                list.add(criteriaBuilder.equal(root.get("status").as(String.class), req.getStatus()));
            }


            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, pageable);

        // 数据对象转换
        List<UpgradeTaskDetailResp> contentList = convertList(otaUpgradePage.getContent());
        PageBean<UpgradeTaskDetailResp> pageBean = new PageBean<>();
        pageBean.setList(contentList);
        pageBean.setTotalElements(otaUpgradePage.getTotalElements());
        pageBean.setTotalPages((int) Math.ceil((double) otaUpgradePage.getTotalElements() / (double) size));
        pageBean.setPage(page);
        pageBean.setSize(size);
        return pageBean;
    }

    @Override
    @Transactional
    public ServiceResult save(UpgradeTaskCreateReq upgradeTaskCreateReq) {
        // 赋值
        OTAUpgrade otaUpgrade = new OTAUpgrade();
        BeanUtils.copyProperties(upgradeTaskCreateReq, otaUpgrade);
        // 下载升级应用安装包
        if (upgradeTaskCreateReq.getAppPackageUrl() != null && upgradeTaskCreateReq.getAppPackageUrl().length() > 0) {

            FileInputStream inputStream = null;
            ApkFile apkFile = null;
            try {
                String suffix = upgradeTaskCreateReq.getAppPackageUrl().substring(upgradeTaskCreateReq.getAppPackageUrl().lastIndexOf("."));
                // 判断是否是APK
                if (!suffix.equals(".apk")) {
                    return ServiceResult.error("请传入正确的APK");
                }
                HttpHeaders headers = new HttpHeaders();
                HttpEntity<Resource> httpEntity = new HttpEntity<>(headers);
                ResponseEntity<byte[]> response = restTemplate.exchange(upgradeTaskCreateReq.getAppPackageUrl(), HttpMethod.GET, httpEntity, byte[].class);
                byte[] bytes = response.getBody();
                if (bytes == null || bytes.length == 0) {
                    log.error("从fastDfs下载APK文件异常！！！下载地址：{}", upgradeTaskCreateReq.getAppPackageUrl());
                }
                // 临时source文件
                String sourceFileName = UUID.randomUUID().toString() + ".apk";
                File file = FileUtil.create(sourceFileName, bytes);
                inputStream = new FileInputStream(file);
                MultipartFile multipartFile = new MockMultipartFile(file.getName(), file.getName(), ContentType.APPLICATION_OCTET_STREAM.toString(), inputStream);
                if (file.exists() && file.isFile()) {
                    apkFile = new ApkFile(file);
                    ApkMeta apkMeta = apkFile.getApkMeta();
                    // 版本号
                    otaUpgrade.setVersionOTA(apkMeta.getVersionName());
                    // 安装包大小
                    double packageSize =  (double)file.length() / 1048576;
                    otaUpgrade.setAppSize(packageSize + "M");
                    // 文件地址
                    otaUpgrade.setAppPackageUrl(upgradeTaskCreateReq.getAppPackageUrl());
                    // 会在本地产生临时文件，用完后需要删除
                    FileUtil.deleteFiles(file);
                }
                // 对文件进行md5加密
                String md5 = EncryptUtil.md5File(multipartFile.getBytes());
                otaUpgrade.setAppPackageMD5(md5);
            } catch (Exception e) {
                log.error("从fastdfs服务下载安装包失败！！！， url: " + fastDfsHttpUrl + upgradeTaskCreateReq.getAppPackageUrl(), e);
                return ServiceResult.error("从fastdfs服务下载安装包失败！！！");
            } finally {
                if(inputStream != null){
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        log.error(e.getMessage(), e);
                    }
                }

                if(apkFile != null){
                    try {
                        apkFile.close();
                    } catch (IOException e) {
                        log.error(e.getMessage(), e);
                    }
                }
            }
        }
        // 任务状态为应用
        otaUpgrade.setStatus(UpgradeStatus.APPLY);
        // 判断文件MD5是否一致
        if (!upgradeTaskCreateReq.getAppPackageMD5().equals(otaUpgrade.getAppPackageMD5())) {
            return ServiceResult.error("md5不正确,请输入正确的md5");
        }

        otaUpgrade.setOriginalFileName(upgradeTaskCreateReq.getOriginalFileName());
        otaUpgrade.setForceUpgrade(upgradeTaskCreateReq.isForceUpdate());
        // 保存
        OTAUpgrade ota = appUpgradeDao.save(otaUpgrade);
        // 当前新增的应用任务为启用 其余为废弃
        ArrayList<OTAUpgrade> lists = new ArrayList<>();
        // 查询除新增任务外的所有当前应用任务
        List<OTAUpgrade> list = appUpgradeDao.findByIdNotAndApplication(ota.getId(), ota.getApplication());
        for (OTAUpgrade otaUpgrade1 : list) {
            otaUpgrade1.setStatus(UpgradeStatus.DISCARD);
            lists.add(otaUpgrade1);
        }
        // 保存
        appUpgradeDao.saveAll(lists);
        return ServiceResult.ok();
    }

    @Override
    @Transactional
    public ServiceResult get(String id) {
        // 根据ID获取应用任务
        Optional<OTAUpgrade> byId = appUpgradeDao.findById(id);
        if (!byId.isPresent()) {
            return ServiceResult.error("ID有误,查询失败");
        }
        OTAUpgrade otaUpgrade = byId.get();
        UpgradeTaskDetailResp upgradeTaskDetailResp = new UpgradeTaskDetailResp();
        // 赋值
        BeanUtils.copyProperties(otaUpgrade, upgradeTaskDetailResp);
        // 应用名称
        upgradeTaskDetailResp.setApplicationName(otaUpgrade.getApplication().getName());
        upgradeTaskDetailResp.setForceUpdate(otaUpgrade.isForceUpgrade());
        return ServiceResult.ok(upgradeTaskDetailResp);
    }

    @Override
    @Transactional
    public ServiceResult update(String id, UpgradeTaskCreateReq upgradeTaskCreateReq) {
        // 根据ID获取应用任务
        Optional<OTAUpgrade> byId = appUpgradeDao.findById(id);
        if (!byId.isPresent()) {
            return ServiceResult.error("ID有误,更新失败");
        }
        OTAUpgrade otaUpgrade = byId.get();
        // 判断是否已过任务升级时间
        LocalDateTime now = LocalDateTime.now();
        if (DateUtil.isExceed(otaUpgrade.getUpgradeTime(), now, HALF_HOUR)) {
            return ServiceResult.error("已过任务升级时间,不可编辑");
        }
        BeanUtils.copyProperties(upgradeTaskCreateReq, otaUpgrade);
        appUpgradeDao.save(otaUpgrade);
        return ServiceResult.ok();
    }

    @Override
    @Transactional
    public ServiceResult delete(String id) {
        // 根据ID获取应用任务
        Optional<OTAUpgrade> byId = appUpgradeDao.findById(id);
        if (!byId.isPresent()) {
            return ServiceResult.error("ID有误,删除失败");
        }
        OTAUpgrade otaUpgrade = byId.get();
        otaUpgrade.setIsDelete(1);
        appUpgradeDao.save(otaUpgrade);
        return ServiceResult.ok();
    }

    @Override
    @Transactional
    public ServiceResult getTask(Application application, String version) {
        // 根据升级应用和版本号查询是否有升级任务
        Optional<OTAUpgrade> op = appUpgradeDao.findByApplicationAndVersionOTANotAndStatusNotAndUpgradeTimeLessThan(application, version, UpgradeStatus.DISCARD, LocalDateTime.now());

        if(!op.isPresent()){
            return ServiceResult.ok();
        }

        UpgradeTaskDetailResp upgradeTaskDetailResp = new UpgradeTaskDetailResp();

        OTAUpgrade otaUpgrade = op.get();
        // 赋值
        BeanUtils.copyProperties(otaUpgrade, upgradeTaskDetailResp);
        upgradeTaskDetailResp.setApplicationName(otaUpgrade.getApplication().getName());
        upgradeTaskDetailResp.setForceUpdate(otaUpgrade.isForceUpgrade());

        return ServiceResult.ok(upgradeTaskDetailResp);
    }

    @Override
    @Transactional
    public ServiceResult upload(MultipartFile multipartFile) {
        FileResp fileResp = new FileResp();
        ApkFile apkFile = null;
        try {
            // 临时file文件
            File file = new File(UUID.randomUUID().toString() + ".apk");
            // 将MultipartFile对象转file对象
            file = FileUtil.multipartFileToFile(multipartFile, file);
            apkFile = new ApkFile(file);
            ApkMeta apkMeta = apkFile.getApkMeta();
            // 版本号
            fileResp.setVersionOTA(apkMeta.getVersionName());
            // 安装包大小
            double packageSize =  (double)file.length() / 1048576;
            fileResp.setAppSize(packageSize + "M");
            // 将文件上传至fastdfs
            String url = FastDfsPool.upload(file);
            // 文件地址
            fileResp.setAppPackageUrl(url);
            // 文件
            String url1 = new StringBuilder(StringUtils.substringBefore(new StringBuilder(url).reverse().toString(), "/")).reverse().toString();
            fileResp.setApp(url1);
            // 会在本地产生临时文件，用完后需要删除
            // 关闭流
            apkFile.close();
            FileUtil.deleteFiles(file);
            // 对文件进行md5加密
            String md5 = EncryptUtil.md5File(multipartFile.getBytes());
            fileResp.setAppPackageMD5(md5);
        } catch (Exception e) {
            log.error("文件上传失败: " + e);
            return ServiceResult.error("文件上传失败");
        } finally {
            if(apkFile !=null){
                try {
                    apkFile.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
        return ServiceResult.ok(fileResp);
    }

    @Override
    @Transactional
    public ServiceResult download(String id) {
        Optional<OTAUpgrade> byId = appUpgradeDao.findById(id);
        if (!byId.isPresent()) {
            return ServiceResult.error("ID有误，不存在此升级任务");
        }
        OTAUpgrade otaUpgrade = byId.get();
        otaUpgrade.setDownload(otaUpgrade.getDownload() + 1);
        return ServiceResult.ok();
    }

    /**
     * 对象转换
     *
     * @return 目标对象
     **/
    private List<UpgradeTaskDetailResp> convertList(List<OTAUpgrade> upgradeList) {
        List<UpgradeTaskDetailResp> result = new ArrayList<>();
        if (upgradeList.isEmpty()) {
            return result;
        }

        upgradeList.forEach(obj -> result.add(convert(obj)));

        return result;
    }

    /**
     * 对象转换
     *
     * @return 目标对象
     **/
    private UpgradeTaskDetailResp convert(OTAUpgrade otaUpgrade) {
        return UpgradeTaskDetailResp.builder()
                .id(otaUpgrade.getId())
                .taskName(otaUpgrade.getTaskName())
                .appPackageUrl(new StringBuilder(StringUtils.substringBefore(new StringBuilder(otaUpgrade.getAppPackageUrl()).reverse().toString(), "/")).reverse().toString())
                .appSize(otaUpgrade.getAppSize())
                .download(otaUpgrade.getDownload())
                .createdTime(otaUpgrade.getCreatedTime())
                .appPackageMD5(otaUpgrade.getAppPackageMD5())
                .application(otaUpgrade.getApplication())
                .applicationName(otaUpgrade.getApplication().getName())
                .upgradeTime(otaUpgrade.getUpgradeTime())
                .content(otaUpgrade.getContent())
                .forceUpdate(otaUpgrade.isForceUpgrade())
                .build();
    }
}
