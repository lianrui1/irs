package cn.com.cowain.ibms.rest;

import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.ibms.rest.resp.TimeResp;
import cn.com.cowain.ibms.utils.IConst;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/4 19:56
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/time")
@Api(tags = IConst.MODULE_ELSE)
public class TimeController {

    /**
     * 获取
     *
     * @return
     */
    @GetMapping
    @ApiOperation(value = "获取服务器端系统日期", tags = IConst.MODULE_ELSE)
    public ResponseEntity<JsonResult<TimeResp>> getToday() {
        LocalDate now = LocalDate.now();
        String todayStr = now.toString();
        log.debug("today:" + todayStr);
        TimeResp timeResp = new TimeResp();
        timeResp.setTime(todayStr);
        JsonResult<TimeResp> jsonResult = JsonResult.ok("OK", timeResp);
        return ResponseEntity.ok(jsonResult);
    }

}
