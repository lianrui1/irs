package cn.com.cowain.ibms.service.bean.amap.weather;

import cn.com.cowain.ibms.service.bean.amap.AMapResp;
import lombok.Data;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/4/19 16:15
 */
@Data
public class Weather extends AMapResp {

    private List<Lives> lives;

    private List<Forecast> forecast;
}
