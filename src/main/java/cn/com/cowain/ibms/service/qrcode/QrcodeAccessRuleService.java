package cn.com.cowain.ibms.service.qrcode;

import cn.com.cowain.ibms.entity.qrcode.QrcodeAccessRules;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.distinguish.HkFaceRecordPageReq;
import cn.com.cowain.ibms.rest.req.qrcode.QrcodeAccessRulePageReq;
import cn.com.cowain.ibms.rest.req.qrcode.QrcodeAccessRuleReq;
import cn.com.cowain.ibms.rest.resp.qrcode.FaceRecordPageResp;
import cn.com.cowain.ibms.rest.resp.qrcode.QrcodeAccessRuleDetailResp;
import cn.com.cowain.ibms.rest.resp.qrcode.QrcodeAccessRulePageResp;
import cn.com.cowain.ibms.rest.resp.qrcode.UserPageResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import org.springframework.data.domain.Page;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/4/7 10:20
 */
public interface QrcodeAccessRuleService {

    /**
     * 创建二维码通行权限
     *
     * @param qrcodeAccessRuleReq 创建参数
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/4/6 16:25
     **/
    ServiceResult createAccessRule(QrcodeAccessRuleReq qrcodeAccessRuleReq);

    /**
     * 修改二维码通行权限
     *
     * @param id                  权限ID
     * @param qrcodeAccessRuleReq 创建参数
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/4/6 16:25
     **/
    ServiceResult updateAccessRule(String id, QrcodeAccessRuleReq qrcodeAccessRuleReq);

    /**
     * 删除通行权限
     *
     * @param id 权限ID
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/4/6 16:25
     **/
    ServiceResult deleteAccessRule(String id);

    /**
     * 获取通行权限分页数据
     *
     * @param req 查询对象
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/4/6 16:30
     **/
    PageBean<QrcodeAccessRulePageResp> getAccessRulePage(QrcodeAccessRulePageReq req);

    /**
     * 查询二维码通行权限
     *
     * @param id 权限ID
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/4/6 16:29
     **/
    QrcodeAccessRuleDetailResp getAccessRuleDetailByAccessId(String id);

    /*
     * 对象转换
     *
     * @param qrcodeAccessRules 待转对象
     * @return 转换结果
     * @author Yang.Lee
     * @date 2021/4/8 9:43
     **/
    QrcodeAccessRuleDetailResp convert(QrcodeAccessRules qrcodeAccessRules);

    /*
     * 对象转换
     *
     * @param qrcodeAccessRules 待转对象
     * @return 转换结果
     * @author Yang.Lee
     * @date 2021/4/8 9:43
     **/
    QrcodeAccessRulePageResp convertPage(QrcodeAccessRules qrcodeAccessRules);

    /*
     * 对象转换
     *
     * @param qrcodeAccessRules 待转对象
     * @return 转换结果
     * @author Yang.Lee
     * @date 2021/4/8 9:43
     **/
    List<QrcodeAccessRulePageResp> convert(Collection<QrcodeAccessRules> qrcodeAccessRulesCollection);

    /**
     * 对象转换
     *
     * @param source 待转对象
     * @return 转换结果
     * @author Yang.Lee
     * @date 2021/4/8 16:55
     **/
    PageBean<QrcodeAccessRulePageResp> convert(Page<QrcodeAccessRules> source);

    /**
     * 判断用户是否有门磁权限
     *
     * @param dmNo  门磁编号
     * @param empNo 用户工号
     * @return true: 有权限。 false: 无权限
     * @author Yang.Lee
     * @date 2021/4/9 9:13
     **/
    ServiceResult checkUserAuth(String dmNo, String empNo);

    /**
     * 查询二维码通行权限
     *
     * @param deviceId 设备Id
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/4/6 16:29
     **/
    @Nullable
    QrcodeAccessRuleDetailResp getByDeviceId(String deviceId);

    /**
     * 人员组删除后的联动操作
     *
     * @param staffGroupId 人员组ID
     * @return 处理结果
     */
    ServiceResult staffGroupRemoveCheck(String staffGroupId);

    /**
     * 获取设备人员列表
     *
     * @param name
     * @param ruleId
     * @param page
     * @param size
     * @return
     */
    PageBean<UserPageResp> userList(String name, String ruleId, int page, int size);

    /**
     * 获取设备通行记录
     *
     * @param req
     * @return
     */
    PageBean<FaceRecordPageResp> accessRecord(HkFaceRecordPageReq req);
}
