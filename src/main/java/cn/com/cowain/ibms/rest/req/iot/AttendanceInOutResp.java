package cn.com.cowain.ibms.rest.req.iot;

import cn.com.cowain.ibms.rest.resp.ChartDataResp;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2022/5/4 14:27
 */
@Data
@ApiModel("考勤设备进出人次数据")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AttendanceInOutResp {

    @ApiModelProperty(value = "人员入数据", required = true, example = "1", position = 1)
    private List<ChartDataResp> in;

    @ApiModelProperty(value = "人员出数据", required = true, example = "1", position = 2)
    private List<ChartDataResp> out;
}
