package cn.com.cowain.ibms.enumeration;

/**
 * 人员导入状态
 *
 * @author: yanzy
 * @date: 2022/4/8 10:59
 */
public enum  UserImportStatus {

    DOING("执行中"),

    SUCCESS("成功"),

    FAILED("失败"),

    DEL_SUCCESS("删除成功"),

    DEL_FAILED("删除失败")
    ;

    UserImportStatus(String name){
        this.name = name;
    }

    private  String name;

    public String getName(){
        return this.name;
    }
}
