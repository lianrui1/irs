package cn.com.cowain.ibms.rest.iot;

import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.Sorts;
import cn.com.cowain.ibms.rest.req.iot.ElevatorSpotAuthSaveReq;
import cn.com.cowain.ibms.rest.req.iot.ElevatorSpotBindDeviceReq;
import cn.com.cowain.ibms.rest.req.iot.ElevatorSpotPageReq;
import cn.com.cowain.ibms.rest.req.iot.ElevatorSpotUserPageReq;
import cn.com.cowain.ibms.rest.resp.iot.ElevatorSpotFloorPageResp;
import cn.com.cowain.ibms.rest.resp.iot.ElevatorSpotUserPageResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.iot.ElevatorSpotService;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/3/23 2:20
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/elevatorSpot")
@Api(tags = IConst.MODULE_ELEVATOR_SPOT)
public class ElevatorSpotController {

    @Autowired
    private ElevatorSpotService elevatorSpotService;

    @GetMapping("/user/page")
    @ApiOperation(value = "梯控按人员列表分页", tags = {IConst.MODULE_ELEVATOR_SPOT, IConst.V27})
    public ResponseEntity<JsonResult<PageBean<ElevatorSpotUserPageResp>>> getUserPage(ElevatorSpotUserPageReq req) {

        return ResponseEntity.ok(JsonResult.ok(elevatorSpotService.getUserPage(req)));
    }

    @PostMapping
    @ApiOperation(value = "分配权限", tags = {IConst.MODULE_ELEVATOR_SPOT, IConst.V27})
    public ResponseEntity<JsonResult<String>> create(@RequestBody @Validated ElevatorSpotAuthSaveReq req, BindingResult bindingResult) {

        ServiceResult result = elevatorSpotService.create(req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, result.getErrorCode()));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @PutMapping
    @ApiOperation(value = "修改梯控权限", tags = {IConst.MODULE_ELEVATOR_SPOT, IConst.V27})
    public ResponseEntity<JsonResult<String>> update(@RequestBody @Validated ElevatorSpotAuthSaveReq req, BindingResult bindingResult) {

        ServiceResult result = elevatorSpotService.update(req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, result.getErrorCode()));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @DeleteMapping("/{empNo}")
    @ApiOperation(value = "删除人员所有梯控权限", tags = {IConst.MODULE_ELEVATOR_SPOT, IConst.V27})
    public ResponseEntity<JsonResult<String>> deleteAuth(@PathVariable @ApiParam("人员工号") String empNo) {

        ServiceResult result = elevatorSpotService.deleteAuth(empNo);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, result.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @GetMapping("/floor/page")
    @ApiOperation(value = "梯控按楼层列表分页", tags = {IConst.MODULE_ELEVATOR_SPOT, IConst.V27})
    public ResponseEntity<JsonResult<PageBean<ElevatorSpotFloorPageResp>>> getFloorPage(ElevatorSpotPageReq req) {
        // 添加默认的排序方式：按照创建时间升序查询
        req.addOrder(Sorts.getDefaultAsc());
        ServiceResult result = elevatorSpotService.getFloorPage(req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, result.getErrorCode()));
        }
        return ResponseEntity.ok(JsonResult.ok((PageBean<ElevatorSpotFloorPageResp>) result.getObject()));
    }

    @DeleteMapping("/{empNo}/{spaceId}")
    @ApiOperation(value = "删除人员指定梯控权限", tags = {IConst.MODULE_ELEVATOR_SPOT, IConst.V27})
    public ResponseEntity<JsonResult<String>> deleteFloorUserAuth(@PathVariable @ApiParam("人员工号") String empNo, @PathVariable @ApiParam("空间ID") String spaceId) {

        ServiceResult result = elevatorSpotService.deleteFloorUserAuth(empNo, spaceId);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, result.getErrorCode()));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @PutMapping("/bind")
    @ApiOperation(value = "绑定设备", tags = {IConst.MODULE_ELEVATOR_SPOT, IConst.V27})
    public ResponseEntity<JsonResult<String>> bindDevice(@RequestBody @Validated ElevatorSpotBindDeviceReq req, BindingResult bindingResult) {

        ServiceResult result = elevatorSpotService.bindDevice(req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, result.getErrorCode()));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @PutMapping("/unbind")
    @ApiOperation(value = "解绑设备", tags = {IConst.MODULE_ELEVATOR_SPOT, IConst.V27})
    public ResponseEntity<JsonResult<String>> unbindDevice(@RequestBody @Validated ElevatorSpotBindDeviceReq req, BindingResult bindingResult) {

        ServiceResult result = elevatorSpotService.unbindDevice(req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, result.getErrorCode()));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }
}
