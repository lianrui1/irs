package cn.com.cowain.ibms.rest.req.office;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author feng
 * @title: UserOfficeReq
 * @projectName bims
 * @Date 2021/12/7 16:23
 */
@Data
@ApiModel("完善信息实体")
public class UserOfficeReq implements Serializable   {
    @ApiModelProperty(value = "办公室id")
    String officeId;
    @ApiModelProperty(value = "办公室名称")
    String officeName;
    @ApiModelProperty(value = "初始化后的办公室id")
    String intellgentOfficeId;
}
