package cn.com.cowain.ibms.service.office.strategy.deviceType;

import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.rest.resp.device.OfficeCommonDeviceResp;
import cn.com.cowain.ibms.rest.resp.device.TbDataResp;
import cn.com.cowain.ibms.rest.resp.device.centralized.CentralizedControlDeviceFilter;
import cn.com.cowain.ibms.rest.resp.office.ShowDeviceResp;
import cn.com.cowain.ibms.rest.resp.office.TbDataDetailResp;
import cn.com.cowain.ibms.service.device.HwProductsService;
import cn.com.cowain.ibms.service.office.DevicePropertiesService;
import cn.com.cowain.ibms.service.office.strategy.DeviceTypeStrategy;
import cn.hutool.extra.spring.SpringUtil;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.Optional;

@Log4j2
public class SensorBodySensingStrategy implements DeviceTypeStrategy {
    private final DevicePropertiesService devicePropertiesService= SpringUtil.getBean(DevicePropertiesService.class);
    private final DeviceDao deviceDao= SpringUtil.getBean(DeviceDao.class);
    private final HwProductsService productsService= SpringUtil.getBean(HwProductsService.class);
    @Override
    public void addCommonDeviceResp(ShowDeviceResp resp, List<OfficeCommonDeviceResp> commonDeviceRespList) {
        log.info("进入传感器设备的策略方法！");
        TbDataDetailResp tbDataDetailResp=devicePropertiesService.getPropertiesFromDeviceRedis(resp.getHwDeviceId(), resp.getDeviceType(), TbDataDetailResp.class);

        if (tbDataDetailResp==null) {
            tbDataDetailResp=new TbDataDetailResp();
            String tbId = productsService.findTbId(resp.getHwDeviceId());
            if (tbId == null) {
                log.error("查询失败 设备id有误" + resp.getHwDeviceId());
                return;
            }
            Optional<IotDevice> iotDeviceOptional =deviceDao.findByHwDeviceId(resp.getHwDeviceId());
            List<TbDataResp> ill = productsService.findNowData(tbId);
            if (CollectionUtils.isNotEmpty(ill)&&iotDeviceOptional.isPresent()){
                BeanUtils.copyProperties(ill.get(0), tbDataDetailResp);
                devicePropertiesService.addPropertiesToDeviceRedis(iotDeviceOptional.get(),tbDataDetailResp,TbDataDetailResp.class);
                tbDataDetailResp.setHwStatus(String.valueOf(iotDeviceOptional.get().getHwStatus()));

                log.info("查询结果：{},存入Redis成功！",tbDataDetailResp);
            }


        }
        tbDataDetailResp.setHwDeviceId(resp.getHwDeviceId());
        tbDataDetailResp.setId(resp.getDeviceId());
        tbDataDetailResp.setDeviceName(resp.getDeviceName());
        commonDeviceRespList.add(new OfficeCommonDeviceResp(tbDataDetailResp, resp.getDeviceTypeName()));
    }

    @Override
    public void filterDevicePropertiesStatus(IotDevice iotDevice, CentralizedControlDeviceFilter controlDeviceFilter, String action) {

    }


}
