package cn.com.cowain.ibms.rest.distinguish;

import cn.com.cowain.ibms.enumeration.ability.Ability;
import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.distinguish.AccessRecordPageReq;
import cn.com.cowain.ibms.rest.req.distinguish.HqAccessRecordReq;
import cn.com.cowain.ibms.rest.resp.EquipmentDataResp;
import cn.com.cowain.ibms.rest.resp.SearchStaffDataResp;
import cn.com.cowain.ibms.rest.resp.distinguish.AccessRecordPageResp;
import cn.com.cowain.ibms.rest.resp.distinguish.WorkTimeCheckResp;
import cn.com.cowain.ibms.service.ability.AbilityService;
import cn.com.cowain.ibms.service.disthinguish.AccessRecordHqService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * 通行记录Controller
 *
 * @author Yang.Lee
 * @date 2021/3/12 10:12
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/accessRecord")
@Api(tags = IConst.DISTINGUISH)
public class AccessRecordController {
    @Resource
    private AccessRecordHqService accessRecordService;

    @Resource
    private AbilityService abilityService;

    /**
     * 查询通行记录分页数据
     *
     * @param param 查询参数
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/3/15 14:43
     **/
    @GetMapping("/page")
    @ApiOperation("获取通行记录列表（分页）")
    public ResponseEntity<JsonResult<PageBean<AccessRecordPageResp>>> getPage(AccessRecordPageReq param) {

        // 默认根据创建时间倒叙查询数据
        param.addOrderDesc("dataTime");
        PageBean<AccessRecordPageResp> pageBean = accessRecordService.getPage(param);

        return ResponseEntity.ok(JsonResult.ok("", pageBean));
    }

    /**
     * 查询考勤数据
     *
     * @param param 查询参数
     * @return 查询结果
     */
    @GetMapping("/workTimeCheck/list")
    @ApiOperation("获取考勤数据")
    public ResponseEntity<JsonResult<List<WorkTimeCheckResp>>> getWorkTimeCheckList(AccessRecordPageReq param) {

        return ResponseEntity.ok(JsonResult.ok("OK", accessRecordService.getWorkTimeCheckList(param.getFrom(), param.getTo())));
    }

    /**
     * 查询总部考勤数据
     *
     * @param param 查询参数
     * @return 查询结果
     */
    @GetMapping("/hq/workTimeCheck/list")
    @ApiOperation("获取考勤数据")
    public ResponseEntity<JsonResult<List<SearchStaffDataResp>>> getHqWorkTimeCheckList(HqAccessRecordReq param) {
        List<SearchStaffDataResp> hqWorkTimeCheckList = accessRecordService.getHqWorkTimeCheckList(param.getFrom(), param.getTo());
        if (Optional.ofNullable(hqWorkTimeCheckList).isPresent()) {
            return ResponseEntity.ok(JsonResult.ok("OK", hqWorkTimeCheckList));
        }
        return ResponseEntity.ok(JsonResult.error("服务下线", null, ErrConst.E500));
    }

    /**
     * 获取设备列表
     *
     * @return
     */
    @GetMapping("/hq/equipment/list")
    @ApiOperation("获取设备列表")
    public ResponseEntity<JsonResult<List<EquipmentDataResp>>> getHqEquipmentList() {

        return ResponseEntity.ok(JsonResult.ok("OK", abilityService.getAbilityEquipmentList(Ability.ATTENDANCE)));
    }

    /**
     * 获取海康设备通行记录列表
     *
     * @param param
     * @return
     */
    @GetMapping("/page/hk/device")
    @ApiOperation(value = "获取海康设备通行记录列表（分页）", tags = "${project.sprint}")
    public ResponseEntity<JsonResult<PageBean<AccessRecordPageResp>>> getHkDevicePage(AccessRecordPageReq param) {

        return ResponseEntity.ok().build();
    }


}
