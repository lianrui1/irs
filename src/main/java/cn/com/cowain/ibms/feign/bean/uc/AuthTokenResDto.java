package cn.com.cowain.ibms.feign.bean.uc;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Yang.Lee
 * @date 2021/12/20 17:40
 */
@Data
public class AuthTokenResDto extends CommonResDto implements Serializable {
    private String token;
    private Long expire;
}
