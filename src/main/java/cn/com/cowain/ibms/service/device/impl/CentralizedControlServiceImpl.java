package cn.com.cowain.ibms.service.device.impl;

import cn.com.cowain.ibms.bo.CentralizedUserBO;
import cn.com.cowain.ibms.bo.DeviceCountBO;
import cn.com.cowain.ibms.component.DynamicsDataComponent;
import cn.com.cowain.ibms.dao.device.CentralizedCodeDeviceRelationDao;
import cn.com.cowain.ibms.dao.device.CentralizedControlAdminDao;
import cn.com.cowain.ibms.dao.device.CentralizedControlCodeDao;
import cn.com.cowain.ibms.dao.device.CentralizedControlRecordDao;
import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.dao.iot.SpaceDao;
import cn.com.cowain.ibms.entity.MessageBody;
import cn.com.cowain.ibms.entity.MessagePoolRecord;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.centralized.CentralizedCodeDeviceRelation;
import cn.com.cowain.ibms.entity.centralized.CentralizedControlCode;
import cn.com.cowain.ibms.entity.iot.HwProductProperties;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.iot.centralizedControl.CentralizedControlAdmin;
import cn.com.cowain.ibms.entity.iot.centralizedControl.CentralizedControlRecord;
import cn.com.cowain.ibms.entity.iot.centralizedControl.TaskDetail;
import cn.com.cowain.ibms.entity.iot.centralizedControl.TaskProgress;
import cn.com.cowain.ibms.enumeration.centralized.TraceStatus;
import cn.com.cowain.ibms.enumeration.integration_control.ControlDeviceRange;
import cn.com.cowain.ibms.enumeration.iot.*;
import cn.com.cowain.ibms.event.dto.CentralizedCodeCacheData;
import cn.com.cowain.ibms.event.process.RefreshCentralizedCodeCacheDataEvent;
import cn.com.cowain.ibms.event.process.RefreshDynamicsDataEvent;
import cn.com.cowain.ibms.exceptions.CommandExecuteException;
import cn.com.cowain.ibms.exceptions.IdempotentException;
import cn.com.cowain.ibms.feign.bean.pass_v2.PassV2UserResp;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.SingleControlCommandIssuedReq;
import cn.com.cowain.ibms.rest.req.device.FailDeviceWechatMsgReq;
import cn.com.cowain.ibms.rest.req.device.centralized.*;
import cn.com.cowain.ibms.rest.req.iot.centralized.BatchCommandReq;
import cn.com.cowain.ibms.rest.req.iot.centralized.CommandTemplate;
import cn.com.cowain.ibms.rest.req.iot.centralized.PropertiesTemplate;
import cn.com.cowain.ibms.rest.req.iot.centralized.SingleCommandReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.device.FailDeviceRecordResp;
import cn.com.cowain.ibms.rest.resp.device.centralized.*;
import cn.com.cowain.ibms.service.MessageSendService;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.Guest;
import cn.com.cowain.ibms.service.device.CentralizedControlService;
import cn.com.cowain.ibms.service.device.HwProductsService;
import cn.com.cowain.ibms.service.guest.GuestService;
import cn.com.cowain.ibms.service.iot.IotDeviceCacheService;
import cn.com.cowain.ibms.service.office.strategy.FunctionExecutor;
import cn.com.cowain.ibms.service.office.strategy.StrategyFactory;
import cn.com.cowain.ibms.service.space.SpaceService;
import cn.com.cowain.ibms.utils.*;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.sfp.comm.utils.JwtUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import static cn.com.cowain.ibms.utils.IConst.CENTRALIZED_CONTROL_CODE;

/**
 * @author wei.cheng
 * @date 2022/09/16 13:53
 */
@Slf4j
@Service
public class CentralizedControlServiceImpl implements CentralizedControlService {
    @Autowired
    private CentralizedControlCodeDao centralizedControlCodeDao;
    @Autowired
    private CentralizedCodeDeviceRelationDao centralizedCodeDeviceRelationDao;
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;
    @Autowired
    private SpaceService spaceService;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private DeviceDao deviceDao;
    @Autowired
    private SpaceDao spaceDao;
    @Value("${cowain.send.message.vx.msgTemplateId}")
    private String vxMsgTemplateId;
    @Value("${cowain.command.issued.huawei.url}")
    private String commandIssuedUrl;
    @Autowired
    private MessageSendService sendService;

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserService;

    @Autowired
    private CentralizedControlAdminDao centralizedControlAdminDao;

    @Autowired
    private CentralizedControlRecordDao centralizedControlRecordDao;

    @Autowired
    private IotDeviceCacheService iotDeviceCacheService;
    @Autowired
    private GuestService guestService;

    @Autowired
    private DynamicsDataComponent dynamicsDataComponent;

    @Override
    @Transactional
    public ResponseEntity<JsonResult<IdResp>> createCentralizedControlCode(CentralizedControlCodeCreateReq req) {
        // 获取当前用户工号
        String hrId = JwtUtil.getHrId(TokenUtils.getToken());
        if (centralizedControlCodeDao.existsByName(req.getName().trim())) {
            return ResponseEntity.ok(JsonResult.error("集控码名称已存在", null, ErrConst.E01));
        }
        // 构建集控码
        CentralizedControlCode centralizedControlCode = buildCentralizedControlCode(req, hrId);
        centralizedControlCodeDao.save(centralizedControlCode);
        // 集控码控制范围为部分时，保存关联的设备
        if (ControlDeviceRange.PART.equals(req.getControlDeviceRange()) && CollUtil.isNotEmpty(req.getDeviceIds())) {
            List<CentralizedCodeDeviceRelation> centralizedCodeDeviceRelations = req.getDeviceIds().stream().distinct().map(deviceId ->
                    new CentralizedCodeDeviceRelation(centralizedControlCode.getId(), deviceId, hrId, hrId)).collect(Collectors.toList());
            centralizedCodeDeviceRelationDao.saveAll(centralizedCodeDeviceRelations);
        }
        // 创建或更新集控码的缓存数据
        applicationEventPublisher.publishEvent(new RefreshCentralizedCodeCacheDataEvent(centralizedControlCode));
        return ResponseEntity.ok(JsonResult.ok("OK", IdResp.builder().id(centralizedControlCode.getId()).build()));
    }

    private CentralizedControlCode buildCentralizedControlCode(CentralizedControlCodeCreateReq req, String hrId) {
        CentralizedControlCode centralizedControlCode = new CentralizedControlCode();
        centralizedControlCode.setName(req.getName().trim());
        centralizedControlCode.setProjectId(req.getProjectId());
        centralizedControlCode.setSpaceId(req.getSpaceId());
        centralizedControlCode.setControlDeviceType(req.getControlDeviceTypeList().stream().map(DeviceType::name).collect(Collectors.joining(",")));
        centralizedControlCode.setControlDeviceRange(req.getControlDeviceRange());
        centralizedControlCode.setAdminHrId(req.getAdminHrId());
        centralizedControlCode.setAdminName(req.getAdminName());
        centralizedControlCode.setCreatedBy(hrId);
        centralizedControlCode.setUpdatedBy(hrId);
        return centralizedControlCode;
    }

    @Override
    @Transactional
    public ResponseEntity<JsonResult<IdResp>> updateCentralizedControlCode(String id, CentralizedControlCodeUpdateReq req) {
        // 获取当前用户工号
        String hrId = JwtUtil.getHrId(TokenUtils.getToken());
        // 获取集控码
        Optional<CentralizedControlCode> centralizedControlCodeOptional = centralizedControlCodeDao.findById(id);
        if (centralizedControlCodeOptional.isEmpty()) {
            return ResponseEntity.ok(JsonResult.error("集控码不存在", null, ErrConst.E06));
        }
        CentralizedControlCode centralizedControlCode = centralizedControlCodeOptional.get();
        if (!StringUtils.equalsIgnoreCase(req.getName(), centralizedControlCode.getName()) && centralizedControlCodeDao.existsByName(req.getName().trim())) {
            return ResponseEntity.ok(JsonResult.error("集控码名称已存在", null, ErrConst.E01));
        }
        // 更新集控码
        updateCentralizedControlCode(req, hrId, centralizedControlCode);
        centralizedControlCodeDao.save(centralizedControlCode);
        // 更新集控码关联设备
        updateRelationDevice(id, req, hrId, centralizedControlCode);
        // 创建或更新集控码的缓存数据
        applicationEventPublisher.publishEvent(new RefreshCentralizedCodeCacheDataEvent(centralizedControlCode));
        return ResponseEntity.ok(JsonResult.ok("OK", IdResp.builder().id(id).build()));
    }

    private void updateCentralizedControlCode(CentralizedControlCodeUpdateReq req, String hrId, CentralizedControlCode centralizedControlCode) {
        centralizedControlCode.setName(req.getName().trim());
        centralizedControlCode.setControlDeviceRange(req.getControlDeviceRange());
        centralizedControlCode.setControlDeviceType(req.getControlDeviceTypeList().stream().map(DeviceType::name).collect(Collectors.joining(",")));
        centralizedControlCode.setProjectId(req.getProjectId());
        centralizedControlCode.setSpaceId(req.getSpaceId());
        centralizedControlCode.setUpdatedBy(hrId);
        centralizedControlCode.setAdminHrId(req.getAdminHrId());
        centralizedControlCode.setAdminName(req.getAdminName());
    }

    private void updateRelationDevice(String id, CentralizedControlCodeUpdateReq req, String hrId, CentralizedControlCode centralizedControlCode) {
        if (ControlDeviceRange.ALL.equals(req.getControlDeviceRange())) {
            centralizedCodeDeviceRelationDao.deleteAllByCentralizedControlCodeId(id);
        } else if (ControlDeviceRange.PART.equals(req.getControlDeviceRange())) {
            List<CentralizedCodeDeviceRelation> centralizedCodeDeviceRelations = centralizedCodeDeviceRelationDao.findAllByCentralizedControlCodeId(id);
            List<CentralizedCodeDeviceRelation> needDeletedCentralizedCodeDeviceRelations = centralizedCodeDeviceRelations.stream()
                    .filter(c -> !Optional.ofNullable(req.getDeviceIds()).orElse(new ArrayList<>()).contains(c.getDeviceId())).collect(Collectors.toList());
            List<String> deviceIds = centralizedCodeDeviceRelations.stream().map(CentralizedCodeDeviceRelation::getDeviceId).collect(Collectors.toList());
            if (CollUtil.isNotEmpty(req.getDeviceIds())) {
                List<CentralizedCodeDeviceRelation> needInsertCentralizedCodeDeviceRelations = req.getDeviceIds().stream().distinct()
                        .filter(deviceId -> !deviceIds.contains(deviceId)).map(deviceId ->
                                new CentralizedCodeDeviceRelation(centralizedControlCode.getId(), deviceId, hrId, hrId)).collect(Collectors.toList());
                if (CollUtil.isNotEmpty(needInsertCentralizedCodeDeviceRelations)) {
                    centralizedCodeDeviceRelationDao.saveAll(needInsertCentralizedCodeDeviceRelations);
                }
            }
            if (CollUtil.isNotEmpty(needDeletedCentralizedCodeDeviceRelations)) {
                needDeletedCentralizedCodeDeviceRelations.forEach(centralizedCodeDeviceRelation -> centralizedCodeDeviceRelation.setIsDelete(1));
                centralizedCodeDeviceRelationDao.saveAll(needDeletedCentralizedCodeDeviceRelations);
            }
        }
    }

    @Override
    public ResponseEntity<JsonResult<PageBean<CentralizedControlCodeResp>>> searchCentralizedControlCode(CentralizedControlCodeSearchReq req) {
        Page<CentralizedControlCode> page = centralizedControlCodeDao.findAll((root, query, criteriaBuilder) -> {
            List<Predicate> list = new ArrayList<>();
            if (StringUtils.isNotBlank(req.getProjectId())) {
                list.add(criteriaBuilder.equal(root.get("projectId"), req.getProjectId()));
            }
            if (StringUtils.isNotBlank(req.getSpaceId())) {
                List<String> spaceIds = spaceService.getChildIdListByParentIdFromCache(req.getSpaceId());
                spaceIds.add(req.getSpaceId());
                if (spaceIds.size() == 1) {
                    list.add(criteriaBuilder.equal(root.get("spaceId"), req.getSpaceId()));
                } else {
                    list.add(criteriaBuilder.in(root.get("spaceId")).value(spaceIds));
                }
            }
            if (StringUtils.isNotBlank(req.getName())) {
                String like = "%" + req.getName() + "%";
                list.add(criteriaBuilder.like(root.get("name"), like));
            }
            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, PageRequest.of(req.getPage(), req.getSize(), Sort.by(Sort.Direction.DESC, "createdTime")));
        List<CentralizedControlCodeResp> list = new ArrayList<>();
        page.get().forEach(code -> {
            CentralizedControlCodeResp centralizedControlCodeResp = new CentralizedControlCodeResp();
            centralizedControlCodeResp.setId(code.getId());
            centralizedControlCodeResp.setName(code.getName());
            centralizedControlCodeResp.setAdminHrId(code.getAdminHrId());
            centralizedControlCodeResp.setAdminName(code.getAdminName());
            Space space = spaceService.getSpaceFromCacheFirst(code.getSpaceId());
            if (Objects.nonNull(space)) {
                centralizedControlCodeResp.setControlArea(SpaceService.getFullSpaceName(space));
            }
            centralizedControlCodeResp.setCountAir(getDeviceSize(code.getId(), DeviceType.AIR_CONDITIONER));
            centralizedControlCodeResp.setCountLamp(getDeviceSize(code.getId(), DeviceType.FOUR_WAY_SWITCH));
            list.add(centralizedControlCodeResp);
        });
        PageBean<CentralizedControlCodeResp> pageBean = new PageBean<>();
        pageBean.setList(list);
        pageBean.setSize(req.getSize());
        pageBean.setPage(req.getPage());
        pageBean.setTotalPages(page.getTotalPages());
        pageBean.setTotalElements(page.getTotalElements());
        return ResponseEntity.ok(JsonResult.ok("OK", pageBean));
    }

    /**
     * 获取缓存中集体码对应类型设备的数量
     *
     * @param centralizedControlCodeId
     * @param deviceType
     * @return
     */
    private long getDeviceSize(String centralizedControlCodeId, DeviceType deviceType) {
        String controlDeviceListKey = redisUtil.createCentralizedCodeDeviceListKey(CENTRALIZED_CONTROL_CODE, deviceType.name(), centralizedControlCodeId,
                RedisUtil.Separator.COLON);
        return redisUtil.lGetListSize(controlDeviceListKey);
    }

    @Override
    public CentralizedCodeCacheData refreshCentralizedCodeCacheData(CentralizedControlCode code) {
        CentralizedCodeCacheData centralizedCodeCacheData = new CentralizedCodeCacheData();
        centralizedCodeCacheData.setName(code.getName());
        centralizedCodeCacheData.setProjectId(code.getProjectId());
        centralizedCodeCacheData.setSpaceId(code.getSpaceId());
        centralizedCodeCacheData.setControlDeviceRange(code.getControlDeviceRange());
        List<DeviceType> controlDeviceTypeList = Arrays.stream(code.getControlDeviceType().split(",")).map(DeviceType::valueOf).collect(Collectors.toList());
        centralizedCodeCacheData.setControlDeviceTypeList(controlDeviceTypeList);
        MultiValueMap<String, String> deviceTypeToDeviceIdListMap = new LinkedMultiValueMap<>();
        if (ControlDeviceRange.ALL.equals(code.getControlDeviceRange())) {
            List<String> spaceIds = spaceService.getChildIdListByParentIdFromCache(code.getSpaceId());
            spaceIds.add(code.getSpaceId());
            List<Object[]> iotDeviceList = deviceDao.findAllBySpaceIdInAndDeviceTypeIn(spaceIds,
                    controlDeviceTypeList.stream().map(DeviceType::name).collect(Collectors.toList()));
            addDeviceToMap(deviceTypeToDeviceIdListMap, iotDeviceList);
        } else if (ControlDeviceRange.PART.equals(code.getControlDeviceRange())) {
            List<Object[]> iotDeviceList = deviceDao.findAllByCentralizedControlCodeId(code.getId());
            addDeviceToMap(deviceTypeToDeviceIdListMap, iotDeviceList);
        }
        //集控码数据存入缓存
        String key = redisUtil.createCentralizedControlKey(CENTRALIZED_CONTROL_CODE, code.getId(), RedisUtil.Separator.COLON);
        Map<String, Object> map = DataUtils.targetObjectCastMap(centralizedCodeCacheData, CentralizedCodeCacheData.class);
        redisUtil.hmset(key, map, false);
        //集控码控制的设备id存入缓存
        for (DeviceType deviceType : controlDeviceTypeList) {
            List<String> deviceIds = deviceTypeToDeviceIdListMap.get(deviceType.name());
            if (Objects.isNull(deviceIds)) {
                deviceIds = new ArrayList<>();
            }
            String controlDeviceListKey = redisUtil.createCentralizedCodeDeviceListKey(CENTRALIZED_CONTROL_CODE, deviceType.name(), code.getId(), RedisUtil.Separator.COLON);
            redisUtil.del(controlDeviceListKey);
            deviceIds.forEach(deviceId -> redisUtil.lSet(controlDeviceListKey, deviceId, false));
        }
        return centralizedCodeCacheData;
    }

    private void addDeviceToMap(MultiValueMap<String, String> deviceTypeToDeviceIdListMap, List<Object[]> iotDeviceList) {
        if (CollUtil.isEmpty(iotDeviceList)) {
            return;
        }
        for (Object[] objects : iotDeviceList) {
            String deviceId = (String) objects[0];
            String deviceType = (String) objects[1];
            if (StrUtil.isNotBlank(deviceId) && StrUtil.isNotBlank(deviceType)) {
                deviceTypeToDeviceIdListMap.add(deviceType, deviceId);
            }
        }
    }

    /**
     * 获取缓存中集控码数据，如果缓存不存在则刷新缓存并返回
     *
     * @param centralizedControlCodeId
     * @return
     */
    private CentralizedCodeCacheData getCentralizedCodeCacheData(String centralizedControlCodeId) {
        String key = redisUtil.createCentralizedControlKey(CENTRALIZED_CONTROL_CODE, centralizedControlCodeId, RedisUtil.Separator.COLON);
        CentralizedCodeCacheData centralizedCodeCacheData;
        if (redisUtil.hasKey(key)) {
            Map<Object, Object> redisMap = redisUtil.hmget(key);
            centralizedCodeCacheData = DataUtils.mapCastTargetObject(redisMap, CentralizedCodeCacheData.class);
        } else {
            Optional<CentralizedControlCode> centralizedControlCodeOptional = centralizedControlCodeDao.findById(centralizedControlCodeId);
            centralizedCodeCacheData = centralizedControlCodeOptional.map(this::refreshCentralizedCodeCacheData).orElse(null);
        }
        return centralizedCodeCacheData;
    }

    @Override
    public ResponseEntity<JsonResult<CentralizedControlCodeDetailH5Resp>> getCentralizedControlCodeDetailH5(String id, String uid) {
        CentralizedCodeCacheData centralizedCodeCacheData = getCentralizedCodeCacheData(id);
        if (Objects.isNull(centralizedCodeCacheData)) {
            return ResponseEntity.ok(JsonResult.error("当前二维码已失效", null, ErrConst.NOT_FOUND));
        }
        JsonResult<String> check = permissionCheck(uid);
        if (1 != check.getStatus()) {
            return ResponseEntity.ok(JsonResult.error(check.getMsg(), null, check.getCode()));
        }
        CentralizedControlCodeDetailH5Resp resp = new CentralizedControlCodeDetailH5Resp();
        resp.setId(id);
        resp.setName(centralizedCodeCacheData.getName());
        Space space = spaceService.getSpaceFromCacheFirst(centralizedCodeCacheData.getSpaceId());
        resp.setControlArea(SpaceService.getFullSpaceName(space, space.getName()));
        // 另起一个线程获取空调设备的统计数据
        Future<DeviceCountBO> getAirDeviceCountBOTask = ThreadPoolUtils.EXECUTOR_SERVICE_GET_CENTRALIZED_CONTROL_DEVICE_STATS.submit(() ->
                getDeviceCountBO(DeviceType.AIR_CONDITIONER, id));
        DeviceCountBO lampDeviceCountBO;
        DeviceCountBO airDeviceCountBO;
        try {
            lampDeviceCountBO = getDeviceCountBO(DeviceType.FOUR_WAY_SWITCH, id);
            airDeviceCountBO = getAirDeviceCountBOTask.get();
        } catch (Exception e) {
            log.error("获取集成码详情异常", e);
            // 异常线程打断
            Thread.currentThread().interrupt();
            return ResponseEntity.ok(JsonResult.error("系统异常", null, ErrConst.E99));
        }
        resp.setCountAir(airDeviceCountBO.getCount());
        resp.setCountOpenAir(airDeviceCountBO.getCountOpen());
        resp.setCountLamp(lampDeviceCountBO.getCount());
        resp.setCountOpenLamp(lampDeviceCountBO.getCountOpen());
        return ResponseEntity.ok(JsonResult.ok(resp));
    }

    /**
     * 获取集控码下某种类型的设备数据统计
     *
     * @param deviceType
     * @param centralizedControlCodeId
     * @return
     */
    private DeviceCountBO getDeviceCountBO(DeviceType deviceType, String centralizedControlCodeId) throws ExecutionException, InterruptedException {
        String controlDeviceListKey = redisUtil.createCentralizedCodeDeviceListKey(CENTRALIZED_CONTROL_CODE, deviceType.name(), centralizedControlCodeId, RedisUtil.Separator.COLON);
        List<String> deviceIds = redisUtil.lGet(controlDeviceListKey, 0, -1).stream().map(Object::toString).collect(Collectors.toList());
        List<List<String>> list = ListUtils.partition(deviceIds, 10);
        List<Future<DeviceCountBO>> futureList = new ArrayList<>();
        list.forEach(l -> futureList.add(ThreadPoolUtils.EXECUTOR_SERVICE_GET_CENTRALIZED_CONTROL_DEVICE_STATS.submit(() -> getDeviceCountBO(l))));
        List<IotDevice> iotDeviceList = new ArrayList<>();
        List<String> openDeviceSns = new ArrayList<>();
        for (Future<DeviceCountBO> future : futureList) {
            DeviceCountBO deviceCountBO = future.get();
            iotDeviceList.addAll(deviceCountBO.getDevices());
            openDeviceSns.addAll(deviceCountBO.getOpenDeviceIds());
        }
        return new DeviceCountBO(iotDeviceList, openDeviceSns);
    }

    /**
     * 获取设备的统计数
     *
     * @param deviceIds
     * @return
     */
    private DeviceCountBO getDeviceCountBO(List<String> deviceIds) {
        if (CollectionUtils.isEmpty(deviceIds)) {
            return new DeviceCountBO();
        }
        //获取集控码之下的设备列表
        List<IotDevice> devices = new ArrayList<>();
        deviceIds.forEach(deviceId -> getIotDeviceFromRedis(deviceId, devices));
        List<String> openDeviceIds = new ArrayList<>();
        for (IotDevice device : devices) {
            /*
             * 使用策略模式处理不同设备类型的逻辑
             * 传入设备类型的枚举值，FunctionExecutor通过策略工厂拿到相应的设备类型的对象，
             * 然后去执行对应的功能模块
             */
            FunctionExecutor functionExecutor = new FunctionExecutor(StrategyFactory.getDeviceTypeStrategy(String.valueOf(device.getDeviceType())));
            boolean isOpen = functionExecutor.isOpen(device);
            if (isOpen) {
                openDeviceIds.add(device.getId());
            }
        }
        return new DeviceCountBO(devices, openDeviceIds);
    }

    /**
     * 权限校验
     *
     * @param uid
     * @return
     */
    private JsonResult<String> permissionCheck(String uid) {
        if (StrUtil.isBlank(uid)) {
            return JsonResult.error("uid为空", null, ErrConst.E01);
        }
        String key = redisUtil.createValidUidKey(IConst.CENTRALIZED_CONTROL_VALID_UID, uid, RedisUtil.Separator.COLON);
        if (redisUtil.hasKey(key)) {
            return JsonResult.ok("OK");
        }
        // 获取用户信息
        Optional<PassV2UserResp> userRespOptional = sysUserService.getUserByUid(uid);
        if (userRespOptional.isEmpty()) {
            return JsonResult.error("请至《园区通行证》进行认证", null, ErrConst.PASS_V2_UNAUTHORIZED);
        }
        PassV2UserResp user = userRespOptional.get();
        if (PersonType.EMPLOYEE.equals(user.getPersonType())) {
            // 员工可以进行集控码控制
            log.info("员工可以进行集控码控制");
        } else {
            String hrId = user.getHrId();
            if (StrUtil.isBlank(hrId)) {
                Optional<Guest> guestOptional = guestService.getByMobile(user.getPhone());
                if (guestOptional.isEmpty()) {
                    return JsonResult.error("未找到用户信息", null, ErrConst.FORBIDDEN);
                }
                hrId = guestOptional.get().getHrId();
            }
            if (!sysUserService.isLabour(hrId)) {
                return JsonResult.error("无权限", null, ErrConst.FORBIDDEN);
            }
        }
        // 核验到用户有权限时，在redis保存用户的uid，有效期为30分钟，有效期内无需在校验
        redisUtil.set(key, "", RedisUtil.HALF_AN_HOUR_SECOND);
        return JsonResult.okNoData("OK");
    }

    @Override
    public ResponseEntity<JsonResult<String>> deleteCentralizedControlCode(String id) {

        Optional<CentralizedControlCode> centralizedCodeOp = centralizedControlCodeDao.findById(id);
        if (!centralizedCodeOp.isPresent()) {
            return ResponseEntity.ok(JsonResult.error("未找到对应集控码", null, null));
        }

        CentralizedControlCode centralizedControlCode = centralizedCodeOp.get();
        //数据库删除
        centralizedControlCode.setIsDelete(1);
        //保存数据库
        centralizedControlCodeDao.save(centralizedControlCode);

        //删除集控码缓存redis
        //创建key
        String key = redisUtil.createCentralizedControlKey(CENTRALIZED_CONTROL_CODE, centralizedControlCode.getId(), RedisUtil.Separator.COLON);
        if (redisUtil.hasKey(key)) {
            //删除
            redisUtil.del(key);
        }


        //删除集控码下关联设备
        if (ControlDeviceRange.PART.equals(centralizedControlCode.getControlDeviceRange())) {
            //根据集控码id关联查询设备
            List<CentralizedCodeDeviceRelation> deviceRelationList = centralizedCodeDeviceRelationDao.findByCentralizedControlCodeId(centralizedControlCode.getId());

            //数据库删除
            deviceRelationList.forEach(it -> {
                it.setIsDelete(1);
                centralizedCodeDeviceRelationDao.save(it);
            });

            //删除关联设备redis
            List<DeviceType> controlDeviceTypeList = new ArrayList<>();
            controlDeviceTypeList.add(DeviceType.AIR_CONDITIONER);
            controlDeviceTypeList.add(DeviceType.FOUR_WAY_SWITCH);

            for (DeviceType deviceType : controlDeviceTypeList) {
                String controlDeviceListKey = redisUtil.createCentralizedCodeDeviceListKey(CENTRALIZED_CONTROL_CODE, deviceType.name(), centralizedControlCode.getId(), RedisUtil.Separator.COLON);
                if (redisUtil.hasKey(key)) {
                    redisUtil.del(controlDeviceListKey);
                }
            }
        }

        return ResponseEntity.ok(JsonResult.ok("OK", "删除成功"));
    }

    @Override
    public ResponseEntity<JsonResult<CentralizedCodeControlDeviceListH5Resp>> getCentralizedControlCodeDeviceListH5(String id,
                                                                                                                    CentralizedControlCodeDeviceListH5Req req) {
        CentralizedCodeCacheData centralizedCodeCacheData = getCentralizedCodeCacheData(id);
        if (Objects.isNull(centralizedCodeCacheData)) {
            return ResponseEntity.ok(JsonResult.error("当前二维码已失效", null, ErrConst.NOT_FOUND));
        }
        JsonResult<String> check = permissionCheck(req.getUid());
        if (1 != check.getStatus()) {
            return ResponseEntity.ok(JsonResult.error(check.getMsg(), null, check.getCode()));
        }
        DeviceCountBO deviceCountBO;
        try {
            deviceCountBO = getDeviceCountBO(req.getDeviceType(), id);
        } catch (Exception e) {
            // 异常线程打断
            Thread.currentThread().interrupt();
            return ResponseEntity.ok(JsonResult.error("系统异常", null, ErrConst.E99));
        }
        CentralizedCodeControlDeviceListH5Resp resp = new CentralizedCodeControlDeviceListH5Resp();
        resp.setCount(deviceCountBO.getCount());
        resp.setCountOpen(deviceCountBO.getCountOpen());
        List<CentralizedCodeControlDeviceH5Resp> deviceList = new ArrayList<>();
        deviceCountBO.getDevices().forEach(device -> {
            CentralizedCodeControlDeviceH5Resp centralizedCodeControlDeviceH5Resp = new CentralizedCodeControlDeviceH5Resp();
            centralizedCodeControlDeviceH5Resp.setId(device.getId());
            centralizedCodeControlDeviceH5Resp.setName(device.getDeviceName());
            centralizedCodeControlDeviceH5Resp.setDeviceType(device.getDeviceType());
            DeviceStatus deviceStatus = device.getHwStatus();
            centralizedCodeControlDeviceH5Resp.setStatus(deviceStatus);
            centralizedCodeControlDeviceH5Resp.setStatusDesc(Objects.nonNull(deviceStatus) ? deviceStatus.getName() : null);
            centralizedCodeControlDeviceH5Resp.setIsOn(deviceCountBO.getOpenDeviceIds().contains(device.getId()));
            deviceList.add(centralizedCodeControlDeviceH5Resp);
        });
        resp.setDeviceList(PageBean.createPageBeanByAllData(deviceList, req.getPage(), req.getSize()));
        return ResponseEntity.ok(JsonResult.ok(resp));
    }

    @Override
    public ResponseEntity<JsonResult<CentralizedControlCommandIssuedResp>> centralizedCommandIssued(CentralizedControlCommandIssuedReq req) {
        CentralizedControlCommandIssuedResp resp = new CentralizedControlCommandIssuedResp();
        CentralizedUserBO centralizedUserBO = new CentralizedUserBO();
        log.info("请求参数CentralizedControlCommandIssuedReq:{}", req);
        if (StringUtils.isEmpty(req.getUid()) || !userAuth(req.getUid(), centralizedUserBO)) {
            return ResponseEntity.ok(JsonResult.error("未找到用户信息！", null, ErrConst.E01));
        }
        if (StringUtils.isEmpty(req.getTraceId())) {
            return ResponseEntity.ok(JsonResult.error("事务ID不能为空！", null, ErrConst.E01));
        }
        if (StringUtils.isEmpty(req.getCentralizedCode())) {
            return ResponseEntity.ok(JsonResult.error("集控码不能为空！", resp, ErrConst.E01));
        }
        Optional<CentralizedControlCode> centralizedControlCodeOptional = centralizedControlCodeDao.findById(req.getCentralizedCode());
        if (centralizedControlCodeOptional.isEmpty()) {
            return ResponseEntity.ok(JsonResult.error("集控码不存在！", resp, ErrConst.E01));
        }
        if (CollUtil.isEmpty(req.getControlDeviceTypeList())) {
            return ResponseEntity.ok(JsonResult.error("设备类型不能为空！", resp, ErrConst.E01));
        }
        if (StringUtils.isEmpty(req.getAction())) {
            return ResponseEntity.ok(JsonResult.error("操作字段不能为空！", resp, ErrConst.E01));
        }
        if (StringUtils.isEmpty(req.getControlMode())) {
            return ResponseEntity.ok(JsonResult.error("操作模式不能为空！", resp, ErrConst.E01));
        }
        //获取集控码之下的设备列表
        List<DeviceType> deviceTypes = req.getControlDeviceTypeList();
        List<IotDevice> iotDevices = new ArrayList<>();

        if (CollUtil.isNotEmpty(req.getRetryDeviceIdList())) {
            req.getRetryDeviceIdList().forEach(deviceId ->
                    getIotDeviceFromRedis(deviceId, iotDevices)
            );
        } else {
            deviceTypes.forEach(deviceType ->
                    getDeviceControlToRedisByCentralizedCode(centralizedControlCodeOptional.get(), deviceType, iotDevices)
            );
        }
        if (CollUtil.isEmpty(iotDevices)) {
            return ResponseEntity.ok(JsonResult.error("无可控设备！", resp, ErrConst.E01));
        }
        centralizedUserBO.setIotDevices(iotDevices);
        if (isHasKey(req.getTraceId(), req.getCentralizedCode())) {
            String traceId = getLock(req.getCentralizedCode());
            resp.setTraceId(traceId);
            resp.setTraceStatus(TraceStatus.PROCESSING);
            return ResponseEntity.ok(JsonResult.ok(resp));
        }
        ThreadPoolUtils.EXECUTOR_SERVICE_REDIS.submit(() -> {
            setActionToRedis(req.getTraceId(), req.getAction());
            BeanUtils.copyProperties(req, centralizedUserBO);
            centralizedUserBO.setStatus(DeviceControlStatus.PROCESSING);
            setControlNameToCentralizedUserBO(req, centralizedUserBO);
            dynamicsDataComponent.setDynamicsDataMap(req.getTraceId(), centralizedUserBO);
            log.info("map中缓存的对象centralizedUserBO=={}", centralizedUserBO);
            saveControlRecord(req.getTraceId(), DeviceControlStatus.PROCESSING);
        });
        //发送命令并返回状态
        return ResponseEntity.ok(sendCommandAndReturn(iotDevices, resp, req));
    }

    private void setControlNameToCentralizedUserBO(CentralizedControlCommandIssuedReq req, CentralizedUserBO centralizedUserBO) {
        DeviceType deviceType = req.getControlDeviceTypeList().get(0);
        if (StringUtils.equals(req.getAction(), IConst.SET_POWER_OFF) && StringUtils.equals(req.getControlMode(), IConst.CONTROL_MODE_BATCH)) {
            if (req.getControlDeviceTypeList().size() == 1 && StringUtils.equals(String.valueOf(deviceType), DeviceType.FOUR_WAY_SWITCH.name())) {
                centralizedUserBO.setControlName(ControlName.LIGHT_OFF.getName());
            } else if (req.getControlDeviceTypeList().size() == 1 && StringUtils.equals(String.valueOf(deviceType), DeviceType.AIR_CONDITIONER.name())) {
                centralizedUserBO.setControlName(ControlName.AIR_OFF.getName());
            } else {
                centralizedUserBO.setControlName(ControlName.ALL_OFF.getName());
            }
        } else if (StringUtils.equals(req.getAction(), IConst.SET_POWER_ON) && StringUtils.equals(req.getControlMode(), IConst.CONTROL_MODE_BATCH)) {
            if (req.getControlDeviceTypeList().size() == 1 && StringUtils.equals(String.valueOf(deviceType), DeviceType.FOUR_WAY_SWITCH.name())) {
                centralizedUserBO.setControlName(ControlName.LIGHT_ON.getName());
            } else if (req.getControlDeviceTypeList().size() == 1 && StringUtils.equals(String.valueOf(deviceType), DeviceType.AIR_CONDITIONER.name())) {
                centralizedUserBO.setControlName(ControlName.AIR_ON.getName());
            } else {
                centralizedUserBO.setControlName(ControlName.ALL_ON.getName());
            }
        } else if (StringUtils.equals(req.getControlMode(), IConst.CONTROL_MODE_SINGLE)) {
            List<IotDevice> iotDevices = centralizedUserBO.getIotDevices();
            if (CollUtil.isNotEmpty(iotDevices)) {
                deviceType = iotDevices.get(0).getDeviceType();
            }
            if (StringUtils.equals(req.getAction(), IConst.SET_POWER_OFF)) {
                centralizedUserBO.setControlName(deviceType.getName() + ControlName.SINGLE_OFF.getName());
            } else {
                centralizedUserBO.setControlName(deviceType.getName() + ControlName.SINGLE_ON.getName());
            }
        }

    }

    private void setControlNameToCentralizedUserBOSingle(SingleControlCommandIssuedReq req, CentralizedUserBO centralizedUserBO) {
        if (StringUtils.equals(req.getAction(), IConst.SET_POWER_OFF)) {
            centralizedUserBO.setControlName(req.getDeviceType().getName() + ControlName.SINGLE_OFF.getName());
        } else {
            centralizedUserBO.setControlName(req.getDeviceType().getName() + ControlName.SINGLE_ON.getName());
        }
    }

    @Override
    public ResponseEntity<JsonResult<CentralizedControlCommandIssuedResp>> singleCommandIssued(SingleControlCommandIssuedReq req) {
        CentralizedControlCommandIssuedResp resp = new CentralizedControlCommandIssuedResp();
        CentralizedUserBO centralizedUserBO = new CentralizedUserBO();
        if (!userAuth(req.getUid(), centralizedUserBO)) {
            return ResponseEntity.ok(JsonResult.error("未找到用户信息！", null, ErrConst.E01));

        }
        if (isHasKey(req.getTraceId(), req.getCentralizedCode())) {
            String traceId = getLock(req.getCentralizedCode());
            resp.setTraceId(traceId);
            resp.setTraceStatus(TraceStatus.PROCESSING);
            return ResponseEntity.ok(JsonResult.error("有任务在执行！请稍后。。。", resp, ErrConst.E10000));
        }
        ThreadPoolUtils.EXECUTOR_SERVICE_REDIS.submit(() -> {
            setActionToRedis(req.getTraceId(), req.getAction());
            BeanUtils.copyProperties(req, centralizedUserBO);
            centralizedUserBO.setStatus(DeviceControlStatus.SUCCESS);
            centralizedUserBO.setCentralizedCode(req.getCentralizedCode());
            setControlNameToCentralizedUserBOSingle(req, centralizedUserBO);
            dynamicsDataComponent.setDynamicsDataMap(req.getTraceId(), centralizedUserBO);
            log.info("map中缓存的对象centralizedUserBO=={}", centralizedUserBO);
        });


        CentralizedControlCommandIssuedReq centralizedControlCommandIssuedReq = new CentralizedControlCommandIssuedReq();
        BeanUtils.copyProperties(req, centralizedControlCommandIssuedReq);
        return ResponseEntity.ok(sendCommandAndReturnSingle(req.getDeviceId(), centralizedControlCommandIssuedReq));
    }

    private boolean userAuth(String uid, CentralizedUserBO centralizedUserBO) {
        Optional<PassV2UserResp> userRespOptional = sysUserService.getUserByUid(uid);
        if (userRespOptional.isEmpty()) {
            return false;
        }
        centralizedUserBO.setHrId(userRespOptional.get().getHrId());
        centralizedUserBO.setName(userRespOptional.get().getName());
        if (StringUtils.equals(String.valueOf(userRespOptional.get().getPersonType()), String.valueOf(PersonType.EMPLOYEE))) {
            centralizedUserBO.setType(ApproverType.STAFF);
        } else {
            centralizedUserBO.setType(ApproverType.LABOR);
        }
        return true;
    }


    private void setActionToRedis(String traceId, String action) {
        String tactionKey = redisUtil.createLockKey(IConst.ACTION, traceId);
        redisUtil.set(tactionKey, action, 3600);
    }

    private String getActionToRedis(String traceId) {
        String tactionKey = redisUtil.createLockKey(IConst.ACTION, traceId);
        Object o = redisUtil.get(tactionKey);
        if (null != o) {
            return String.valueOf(o);
        }
        return "";
    }

    /**
     * 是否存在key
     *
     * @param traceId
     * @param code
     * @return
     */
    public boolean isHasKey(String traceId, String code) {
        String traceKey = redisUtil.createLockKey(IConst.TRACE_ID, code);
        log.info(traceKey);
        if (redisUtil.hasKey(traceKey)) {
            return true;
        }
        redisUtil.set(traceKey, traceId, 15);
        return false;
    }

    /**
     * 是否存在key
     *
     * @param code
     * @return
     */
    public String getLock(String code) {
        String traceKey = redisUtil.createLockKey(IConst.TRACE_ID, code);
        log.info(traceKey);
        return (String) redisUtil.get(traceKey);
    }

    /**
     * 锁续命
     *
     * @param traceId
     * @return
     */
    public void resumeLock(String traceId) {
        CentralizedUserBO centralizedUserBO = getCentralizedUserBOByTraceId(traceId);
        String code = "-1";
        if (StringUtils.isNotEmpty(centralizedUserBO.getCentralizedCode())) {
            code = centralizedUserBO.getCentralizedCode();
        }
        String traceKey = redisUtil.createLockKey(IConst.TRACE_ID, code);
        if (redisUtil.hasKey(traceKey)) {
            redisUtil.set(traceKey, traceId, 15);
        }
    }

    /**
     * 解锁
     *
     * @param traceId
     * @return
     */
    public void unLock(String traceId) {
        CentralizedUserBO centralizedUserBO = getCentralizedUserBOByTraceId(traceId);
        String code = "-1";
        if (StringUtils.isNotEmpty(centralizedUserBO.getCentralizedCode())) {
            code = centralizedUserBO.getCentralizedCode();
        }
        String traceKey = redisUtil.createLockKey(IConst.TRACE_ID, code);
        redisUtil.del(traceKey);
        applicationEventPublisher.publishEvent(new RefreshDynamicsDataEvent(centralizedUserBO));
    }

    /**
     * 筛选有效设备并发送命令
     * 1.无效设备和离线设备分开处理
     * 2.无效设备通常指设备状态和目标状态一致的设备
     * 3.无效设备直接当执行成功处理
     * 4.离线设备直接当执行失败设备处理
     */
    private JsonResult<CentralizedControlCommandIssuedResp> sendCommandAndReturn(List<IotDevice> iotDevices, CentralizedControlCommandIssuedResp resp, CentralizedControlCommandIssuedReq req) {
        try {
            CentralizedControlDeviceFilter deviceFilter = filterIotDevice(iotDevices, req.getAction());
            JsonResult<T> jsonResult = handleVailDeviceBatch(deviceFilter.getValidDevices(), req.getAction(), req.getTraceId());
            resp.setTraceStatus(TraceStatus.PROCESSING);
            if (StringUtils.equals(jsonResult.getCode(), IConst.HTTP_CODE_SUCCESS)) {

                return JsonResult.ok("指令发送成功！", resp);
            } else {
                if (StringUtils.equals(jsonResult.getCode(), ErrConst.E26)) {
                    resp.setTraceStatus(TraceStatus.FINISH);
                    return JsonResult.ok(jsonResult.getMsg(), resp);
                }
                ThreadPoolUtils.EXECUTOR_SERVICE_EXCEPTION.submit(() -> {
                    saveControlRecord(req.getTraceId(), DeviceControlStatus.FAIL);
                });
                return JsonResult.error("指令发送失败:" + jsonResult.getMsg(), resp, IConst.HTTP_CODE_FAIL);
            }
        } catch (Exception exception) {
            unLock(req.getTraceId());
            return JsonResult.error("指令发送失败！", resp, IConst.HTTP_CODE_FAIL);
        }

    }

    /**
     * 将失败的记录更新入缓存
     *
     * @param traceId
     * @param deviceControlStatus
     */
    private void saveControlRecord(String traceId, DeviceControlStatus deviceControlStatus) {
        CentralizedUserBO centralizedUserBO = getCentralizedUserBOByTraceId(traceId);
        centralizedUserBO.setStatus(deviceControlStatus);
        setCentralizedUserBOToMap(traceId, centralizedUserBO);
        saveControlRecord(traceId);
    }

    /**
     * 筛选有效设备并发送命令
     * 1.无效设备和离线设备分开处理
     * 2.无效设备通常指设备状态和目标状态一致的设备
     * 3.无效设备直接当执行成功处理
     * 4.离线设备直接当执行失败设备处理
     */
    private JsonResult<CentralizedControlCommandIssuedResp> sendCommandAndReturnSingle(String deviceId, CentralizedControlCommandIssuedReq req) {
        try {
            List<IotDevice> iotDevices = new LinkedList<>();
            getIotDeviceFromRedis(deviceId, iotDevices);
            CentralizedControlCommandIssuedResp resp;
            if (CollUtil.isEmpty(iotDevices)) {
                return JsonResult.error("设备不存在！", null, IConst.HTTP_CODE_FAIL);
            }
            JsonResult<T> jsonResult = handleVailDeviceSingle(iotDevices.get(0), req.getAction(), req.getTraceId());
            if (StringUtils.equals(jsonResult.getCode(), IConst.HTTP_CODE_SUCCESS)) {
                iotDevices.clear();
                resp = wrapCentralizedControlResp(iotDevices, req, TraceStatus.PROCESSING);
                resp.setTraceId(req.getTraceId());
//                resp.setSurplusCount(0);
//                resp.setTotal(1);
                saveControlRecord(req.getTraceId());
                return JsonResult.ok("指令发送成功！", resp);
            } else {
                //resp = wrapCentralizedControlResp(iotDevices, req, TraceStatus.FINISH);
//                resp.setSurplusCount(0);
//                resp.setTotal(1);
                log.info("执行异常，释放锁！");
                unLock(req.getTraceId());
                return JsonResult.error("指令发送失败！", wrapCentralizedControlResp(iotDevices, req, TraceStatus.FINISH), IConst.HTTP_CODE_FAIL);
            }
        } catch (Exception e) {
            log.info("遇到异常，释放锁！");
            unLock(req.getTraceId());
            return JsonResult.error("指令发送失败！", null, IConst.HTTP_CODE_FAIL);

        }
    }

    /**
     * 包装集控返回
     *
     * @param iotDevices
     * @param req
     * @param traceStatus
     */
    private CentralizedControlCommandIssuedResp wrapCentralizedControlResp(List<IotDevice> iotDevices, CentralizedControlCommandIssuedReq req, TraceStatus traceStatus) {
        CentralizedControlCommandIssuedResp resp = wrapRespDeviceList(iotDevices);
        BeanUtils.copyProperties(req, resp);
        resp.setTraceStatus(traceStatus);
        return resp;
    }

    @Autowired
    private RestTemplateUtil restTemplateUtil;
    @Autowired
    private HwProductsService productsService;

    /**
     * 处理有效设备方法：
     * 1.将设备通过类型分组
     * 2.将设备通过产品ID分组
     * 3.组装模板
     * 4.发送指令
     *
     * @param validDevices
     * @param action
     * @param traceId
     * @return
     */
    private JsonResult<T> handleVailDeviceBatch(List<IotDevice> validDevices, String action, @NotBlank String traceId) {
        try {
            BatchCommandReq batchCommandReq = new BatchCommandReq();
            //将设备通过类型分组
            if (CollUtil.isEmpty(validDevices) && StringUtils.equals(action, IConst.SET_POWER_ON)) {
                return JsonResult.error("设备已开启请勿重复操作！", null, ErrConst.E26);
            } else if (CollUtil.isEmpty(validDevices) && StringUtils.equals(action, IConst.SET_POWER_OFF)) {
                return JsonResult.error("设备已关闭请勿重复操作！", null, ErrConst.E26);
            }
            Map<DeviceType, List<IotDevice>> deviceTypeListMap = validDevices.stream().collect(Collectors.groupingBy(IotDevice::getDeviceType));
            deviceTypeListMap.forEach((deviceType, iotDevices) -> {
                makeBatchTemplate(batchCommandReq, deviceType, iotDevices, action, traceId);
            });
            log.info("=====================================");
            log.info("发送指令入参：{}", batchCommandReq);
            JsonResult jsonResult = restTemplateUtil.postForObject(commandIssuedUrl + "/batch", batchCommandReq, JsonResult.class);
            log.info("接收到返回：{}", jsonResult);
            log.info("=====================================");
            if (jsonResult.getStatus() == IConst.HTTP_STATUS_SUCCESS) {
                return JsonResult.ok("success", null);
            } else {
                return JsonResult.error(jsonResult.getMsg(), null, "-1");
            }
        } catch (Exception e) {
            log.info("指令发送异常：{}", e.getMessage());
            unLock(traceId);
            throw new CommandExecuteException("指令发送错误！");
        }
    }

    /**
     * 单个控制设备方法：
     * 1.组装模板
     * 2.发送指令
     *
     * @param validDevice
     * @param action
     * @param traceId
     * @return
     */
    private JsonResult<T> handleVailDeviceSingle(IotDevice validDevice, String action, @NotBlank String traceId) {
        try {
            SingleCommandReq singleCommandReq = new SingleCommandReq();
            //将设备通过类型分组
            singleCommandReq.setTraceId(traceId);
            makeTemplateSingle(singleCommandReq, validDevice.getDeviceType(), validDevice, action);
            JsonResult jsonResult = restTemplateUtil.postForObject(commandIssuedUrl + "/single", singleCommandReq, JsonResult.class);
            if (jsonResult.getStatus() == IConst.HTTP_STATUS_SUCCESS) {
                return JsonResult.ok("success", null);
            } else {
                return JsonResult.error(jsonResult.getMsg(), null, IConst.HTTP_CODE_FAIL);
            }
        } catch (Exception e) {
            log.info("指令发送异常：{}", e.getMessage());
            throw new IdempotentException("指令发送错误！");
        }
    }

    private void makeTemplateSingle(SingleCommandReq singleCommandReq, DeviceType deviceType, IotDevice validDevice, String action) {

        //将设备通过产品ID分组可以避免重复查询产品属性
        JsonResult<HwProductProperties> serviceResult = productsService.findProductPropertiesByHwProductId(validDevice.getHwProductId(), deviceType);
        if (serviceResult.getStatus() != IConst.HTTP_STATUS_SUCCESS) {
            log.error("指令发送异常：{}模板未找到！", deviceType.getName());
            throw new CommandExecuteException("指令发送异常：" + deviceType.getName() + "指令模板未找到！");
        }
        List<String> deviceIds = new LinkedList<>();
        deviceIds.add(validDevice.getHwDeviceId());
        if (StringUtils.equals(String.valueOf(deviceType), String.valueOf(DeviceType.AIR_CONDITIONER))) {
            CommandTemplate commandTemplate = new CommandTemplate();
            makeCommandTemplate(serviceResult.getData(), commandTemplate, action);
            commandTemplate.setDeviceIds(deviceIds);
            singleCommandReq.setCommandTemplate(commandTemplate);
        } else if (StringUtils.equals(String.valueOf(deviceType), String.valueOf(DeviceType.FOUR_WAY_SWITCH))) {
            PropertiesTemplate propertiesTemplate = new PropertiesTemplate();
            makePropertiesTemplate(serviceResult.getData(), action, propertiesTemplate);
            propertiesTemplate.setDeviceIds(deviceIds);
            singleCommandReq.setPropertiesTemplate(propertiesTemplate);
        }

    }

    /**
     * 制作指令模板和属性模板
     *
     * @param batchCommandReq
     * @param deviceType
     * @param validDevices
     * @param action
     * @param traceId
     */
    private void makeBatchTemplate(BatchCommandReq batchCommandReq, DeviceType deviceType, List<IotDevice> validDevices, String action, @NotBlank String traceId) {
        List<CommandTemplate> commandTemplates = new LinkedList<>();
        List<PropertiesTemplate> propertiesTemplates = new LinkedList<>();
        if (CollUtil.isEmpty(validDevices)) {
            throw new CommandExecuteException("找不到可控制的设备！");
        }
        //将设备通过产品ID分组可以避免重复查询产品属性
        Map<String, List<IotDevice>> produceMap = validDevices.stream().collect(Collectors.groupingBy(IotDevice::getHwProductId));
        for (Map.Entry<String, List<IotDevice>> entry : produceMap.entrySet()) {
            String productId = entry.getKey();
            List<IotDevice> iotDeviceList = entry.getValue();
            JsonResult<HwProductProperties> serviceResult = productsService.findProductPropertiesByHwProductId(productId, deviceType);
            if (serviceResult.getStatus() != IConst.HTTP_STATUS_SUCCESS) {
                log.error("指令发送异常：{}模板未找到！", deviceType.getName());
                Runnable runnable = () -> {
                    saveControlRecord(traceId);
                    throw new CommandExecuteException("指令发送异常：" + deviceType.getName() + "指令模板未找到！");
                };
                ThreadPoolUtils.EXECUTOR_SERVICE_EXCEPTION.submit(runnable);
                continue;
            }
            List<String> deviceIds = new LinkedList<>();
            iotDeviceList.forEach(iotDevice ->
                    deviceIds.add(iotDevice.getHwDeviceId())
            );
            if (StringUtils.equals(String.valueOf(deviceType), String.valueOf(DeviceType.AIR_CONDITIONER))) {
                CommandTemplate commandTemplate = new CommandTemplate();
                makeCommandTemplate(serviceResult.getData(), commandTemplate, action);
                commandTemplate.setDeviceIds(deviceIds);
                commandTemplates.add(commandTemplate);
                batchCommandReq.setCommandTemplates(commandTemplates);

            } else if (StringUtils.equals(String.valueOf(deviceType), String.valueOf(DeviceType.FOUR_WAY_SWITCH))) {
                PropertiesTemplate propertiesTemplate = new PropertiesTemplate();
                makePropertiesTemplate(serviceResult.getData(), action, propertiesTemplate);
                propertiesTemplate.setDeviceIds(deviceIds);
                propertiesTemplates.add(propertiesTemplate);
                batchCommandReq.setPropertiesTemplates(propertiesTemplates);

            }
        }

        batchCommandReq.setTraceId(traceId);
    }

    ReentrantLock reentrantLock = new ReentrantLock(true);

    /**
     * 保存失败设备列表
     *
     * @param traceId
     */
    @Transactional
    public void saveControlRecord(String traceId) {
        reentrantLock.lock();
        try {

            Map<String, CentralizedUserBO> map = dynamicsDataComponent.getDynamicsDataMap();
            if (map.containsKey(traceId)) {
                CentralizedUserBO centralizedUserBO = map.get(traceId);
                CentralizedControlRecord centralizedControlRecord;
                Optional<CentralizedControlRecord> centralizedControlRecordOptional = centralizedControlRecordDao.findCentralizedControlRecordByTraceId(traceId);
                if (centralizedControlRecordOptional.isEmpty()) {
                    centralizedControlRecord = new CentralizedControlRecord();
                } else {
                    centralizedControlRecord = centralizedControlRecordOptional.get();
                }
                centralizedControlRecord.setCentralizedControlCodeId(centralizedUserBO.getCentralizedCode());
                centralizedControlRecord.setControlName(centralizedUserBO.getControlName());
                centralizedControlRecord.setControlTime(LocalDateTime.now());
                centralizedControlRecord.setHrId(centralizedUserBO.getHrId());
                centralizedControlRecord.setName(centralizedUserBO.getName());
                centralizedControlRecord.setTraceId(traceId);
                centralizedControlRecord.setType(centralizedUserBO.getType());
                BeanUtils.copyProperties(centralizedUserBO, centralizedControlRecord);
                //centralizedControlRecord.setStatus(DeviceControlStatus.SUCCESS);
                centralizedControlRecordDao.save(centralizedControlRecord);
            }
        } finally {
            reentrantLock.unlock();
        }

    }

    public CentralizedUserBO getCentralizedUserBOByTraceId(String traceId) {
        Map<String, CentralizedUserBO> map = dynamicsDataComponent.getDynamicsDataMap();
        if (map.containsKey(traceId)) {
            return map.get(traceId);
        }
        return new CentralizedUserBO();
    }

    public void setCentralizedUserBOToMap(String traceId, CentralizedUserBO centralizedUserBO) {
        Map<String, CentralizedUserBO> map = dynamicsDataComponent.getDynamicsDataMap();
        map.put(traceId, centralizedUserBO);
    }

    /**
     * 返回失败设备列表
     *
     * @param iotDeviceList
     */
    private CentralizedControlCommandIssuedResp wrapRespDeviceList(List<IotDevice> iotDeviceList) {
        CentralizedControlCommandIssuedResp resp = new CentralizedControlCommandIssuedResp();
        List<FailDeviceRecordResp> failDeviceRecordResps = new LinkedList<>();
        if (CollUtil.isEmpty(iotDeviceList)) {
            resp.setList(failDeviceRecordResps);
            return resp;
        }
        resp.setTotal(iotDeviceList.size());
        iotDeviceList.forEach(iotDevice -> {
            FailDeviceRecordResp failDeviceRecordResp = new FailDeviceRecordResp();
            failDeviceRecordResp.setDeviceName(iotDevice.getDeviceName());
            failDeviceRecordResp.setDeviceTypeName(iotDevice.getDeviceType() == null ? DeviceType.DEFAULT.getName() : iotDevice.getDeviceType().getName());
            failDeviceRecordResp.setStatus(iotDevice.getHwStatus());
            BeanUtils.copyProperties(iotDevice, failDeviceRecordResp);
            failDeviceRecordResps.add(failDeviceRecordResp);
        });
        resp.setList(failDeviceRecordResps);
        return resp;
    }

    /**
     * 制作属性模板
     *
     * @param hwProductProperties
     * @param propertiesTemplate
     * @param action
     * @param propertiesTemplate
     */
    private void makePropertiesTemplate(HwProductProperties hwProductProperties, String action, PropertiesTemplate propertiesTemplate) {

        propertiesTemplate.setLight1(action);
        propertiesTemplate.setLight2(action);
        propertiesTemplate.setLight3(action);
        propertiesTemplate.setLight4(action);
        propertiesTemplate.setServiceId(hwProductProperties.getServiceId());
    }

    /**
     * 制作指令模板
     *
     * @param hwProductProperties
     * @param commandTemplate
     * @param action
     * @param commandTemplate
     */
    public void makeCommandTemplate(HwProductProperties hwProductProperties, CommandTemplate commandTemplate, String action) {
        int powerValue = -1;
        if (StringUtils.equals(action, IConst.SET_POWER_ON)) {
            powerValue = IConst.SET_POWER_VALUE_ON;
        } else if (StringUtils.equals(action, IConst.SET_POWER_OFF)) {
            powerValue = IConst.SET_POWER_VALUE_OFF;
        }
        commandTemplate.setParas("{" + "\"setPower\":" + powerValue + "}");
        commandTemplate.setCommandName(hwProductProperties.getCommandName());
        commandTemplate.setServiceId(hwProductProperties.getServiceId());
    }

    //筛选有效设备
    private CentralizedControlDeviceFilter filterIotDevice(List<IotDevice> iotDevices, String targetAction) {
        CentralizedControlDeviceFilter controlDeviceFilter = new CentralizedControlDeviceFilter();
        iotDevices.forEach(iotDevice -> {
            /*
             * 使用策略模式处理不同设备类型的逻辑
             * 传入设备类型的枚举值，FunctionExecutor通过策略工厂拿到相应的设备类型的对象，
             * 然后去执行对应的功能模块
             */
            FunctionExecutor functionExecutor = new FunctionExecutor(StrategyFactory.getDeviceTypeStrategy(String.valueOf(iotDevice.getDeviceType())));
            functionExecutor.filterCommonDevice(iotDevice, controlDeviceFilter, targetAction);

        });
        return controlDeviceFilter;
    }

    @Override
    public ResponseEntity<JsonResult<PageBean<CentralizedControlCodeSelectDeviceResp>>> getCentralizedControlCodeSelectDevice(String spaceId, CentralizedControlCodeSelectDeviceReq req) {

        //查询空间
        Optional<Space> spaceOp = spaceDao.findById(spaceId);
        if (spaceOp.isEmpty()) {
            return ResponseEntity.ok(JsonResult.error("未找到该空间", null, ErrConst.E01));
        }

        //封装and语句
        List<Predicate> listAnd = new ArrayList<>();
        //封装or语句
        List<Predicate> listOr = new ArrayList<>();

        Page<IotDevice> devices = deviceDao.findAll((root, query, criteriaBuilder) -> {


            listOr.add(criteriaBuilder.equal(root.get("deviceType"), DeviceType.AIR_CONDITIONER));
            listOr.add(criteriaBuilder.equal(root.get("deviceType"), DeviceType.FOUR_WAY_SWITCH));

            //spaceId
            if (StringUtils.isNotEmpty(spaceId)) {
                List<String> spaceList = spaceService.getChildIdListByParentIdFromCache(spaceId);
                spaceList.add(spaceId);
                if (!CollectionUtils.isEmpty(spaceList)) {
                    CriteriaBuilder.In<Object> inInitiator = criteriaBuilder.in(root.get("space").get("id"));
                    spaceList.forEach(inInitiator::value);
                    listAnd.add(inInitiator);
                }
            }

            //设备类型
            if (ObjectUtils.isNotEmpty(req.getDeviceType())) {
                listAnd.add(criteriaBuilder.equal(root.get("deviceType"), req.getDeviceType()));
            }

            //设备名称
            if (StringUtils.isNotBlank(req.getDeviceName())) {
                String like = "%" + req.getDeviceName() + "%";
                listAnd.add(criteriaBuilder.or(criteriaBuilder.like(root.get("deviceName"), like), criteriaBuilder.like(root.get("sn"), like)));
            }

            Predicate[] array_and = new Predicate[listAnd.size()];
            Predicate Pre_And = criteriaBuilder.and(listAnd.toArray(array_and));
            Predicate[] array_or = new Predicate[listOr.size()];
            Predicate Pre_Or = criteriaBuilder.or(listOr.toArray(array_or));
            if (!CollectionUtils.isEmpty(listOr)) {
                return query.where(Pre_And, Pre_Or).getRestriction();
            } else {
                return query.where(Pre_And).getRestriction();
            }

        }, PageRequest.of(req.getPage(), req.getSize(), Sort.by(req.getOrders())));

        PageBean<CentralizedControlCodeSelectDeviceResp> pageBean = new PageBean<>();
        pageBean.setPage(devices.getPageable().getPageNumber());
        pageBean.setSize(devices.getPageable().getPageSize());
        pageBean.setTotalPages(devices.getTotalPages());
        pageBean.setTotalElements(devices.getTotalElements());
        pageBean.setList(devices.getContent()
                .stream()
                .map(CentralizedControlCodeSelectDeviceResp::convent)
                .collect(Collectors.toList())
        );

        return ResponseEntity.ok(JsonResult.ok("OK", pageBean));
    }

    @Override
    public ResponseEntity<JsonResult<CentralizedControlCommandIssuedResp>> processPolling(String traceId) {

        /*Optional<CentralizedControlRecord> recordOp = centralizedControlRecordDao.findByTraceId(traceId);
        if(recordOp.isEmpty()){
            return ResponseEntity.ok(JsonResult.error("traceID有误,数据不存在", null, null));
        }
        CentralizedControlRecord record = recordOp.get();*/

        CentralizedUserBO centralizedUserBOByTraceId = getCentralizedUserBOByTraceId(traceId);

        // 任务执行状态返回对象
        CentralizedControlCommandIssuedResp commandIssuedResp = new CentralizedControlCommandIssuedResp();
        commandIssuedResp.setTraceId(traceId);
        commandIssuedResp.setTraceStatus(TraceStatus.FINISH);
        commandIssuedResp.setCentralizedCode(centralizedUserBOByTraceId.getCentralizedCode());

        // 获取控制成功设备
        JsonResult<List<IotDevice>> successResult = getDeviceControlToRedis(traceId, "success");
        List<IotDevice> successDeviceList = successResult.getData();
        if (!CollectionUtils.isEmpty(successDeviceList)) {
            commandIssuedResp.setDeviceName(successDeviceList.get(successDeviceList.size() - 1).getDeviceName());
        } else {
            commandIssuedResp.setDeviceName("");
        }

        // 获取控制失败设备
        JsonResult<List<IotDevice>> failResult = getDeviceControlToRedis(traceId, "fail");
        List<IotDevice> failDeviceList = failResult.getData();
        List<FailDeviceRecordResp> respList = new ArrayList<>();
        failDeviceList.forEach(device -> {
            FailDeviceRecordResp resp = new FailDeviceRecordResp();
            resp.setId(device.getId());
            resp.setHwDeviceId(device.getHwDeviceId());
            resp.setDeviceName(device.getDeviceName());
            resp.setStatus(device.getHwStatus());
            resp.setStatusDesc(device.getHwStatus().getName());
            resp.setDeviceType(device.getDeviceType());
            resp.setDeviceTypeName(device.getDeviceType().getName());
            resp.setAddress(device.getProject().getProjectName() + " / " + SpaceService.getFullSpaceName(device.getSpace(), device.getSpace().getName()));
            resp.setIp("");
            /*
             * 使用策略模式处理不同设备类型的逻辑
             * 传入设备类型的枚举值，FunctionExecutor通过策略工厂拿到相应的设备类型的对象，
             * 然后去执行对应的功能模块
             */
            FunctionExecutor functionExecutor = new FunctionExecutor(StrategyFactory.getDeviceTypeStrategy(String.valueOf(device.getDeviceType())));
            resp.setIsOn(functionExecutor.isOpen(device));
            respList.add(resp);
        });
        if (StringUtils.equals("1", failResult.getCode())) {
            commandIssuedResp.setTraceStatus(TraceStatus.PROCESSING);
        } else {
            Optional<CentralizedControlRecord> controlRecordOp = centralizedControlRecordDao.findByTraceId(traceId);
            if (controlRecordOp.isPresent()) {
                CentralizedControlRecord record = controlRecordOp.get();
                Optional<CentralizedControlCode> codeOp = centralizedControlCodeDao.findById(record.getCentralizedControlCodeId());
                if (codeOp.isPresent()) {
                    CentralizedControlCode controlCode = codeOp.get();
                    commandIssuedResp.setAdminHrId(controlCode.getAdminHrId());
                    commandIssuedResp.setAdminName(controlCode.getAdminName());
                }
            }
        }
        commandIssuedResp.setList(respList);
        // 获取总执行设备
        // int total = (int) (getDeviceSize(centralizedUserBOByTraceId.getCentralizedCode(), DeviceType.AIR_CONDITIONER) + getDeviceSize(centralizedUserBOByTraceId.getCentralizedCode(), DeviceType.FOUR_WAY_SWITCH));
        // 获取执行设备数量
        TaskProgress taskProgress = new TaskProgress();
        TaskProgress numByTraceId = getNumByTraceId(traceId, taskProgress);
        commandIssuedResp.setTotal(numByTraceId.getTotal());
        // 获取执行动作
        String actionToRedis = getActionToRedis(traceId);
        if (StringUtils.isNotEmpty(actionToRedis)) {
            commandIssuedResp.setControlStatus(actionToRedis);
        } else {
            commandIssuedResp.setControlStatus("");
        }
        if (StringUtils.equals("on", actionToRedis)) {
            commandIssuedResp.setSurplusCount(numByTraceId.getExecuted());
        } else {
            commandIssuedResp.setSurplusCount(numByTraceId.getTotal() - numByTraceId.getExecuted());
        }

        return ResponseEntity.ok(JsonResult.ok("OK", commandIssuedResp));
    }

    private TaskProgress getNumByTraceId(String traceId, TaskProgress taskProgress) {

        int total = 0;
        int executed = 0;
        String traceIdRedisKey = redisUtil.createDeviceIdKey(IConst.CENTRALIZED, traceId, RedisUtil.Separator.COLON);
        // List<String> taskIDs = redisUtil.lGet(traceIdRedisKey, 0, 1, String.class);
        List<String> taskIDs = (List<String>) redisUtil.lGet(traceIdRedisKey, 0, -1);
        for (String taskId : taskIDs) {
            String taskContentRedisKey = redisUtil.createDeviceIdKey(IConst.CENTRALIZED_TASK, taskId, RedisUtil.Separator.COLON);
            Map<Object, Object> map = redisUtil.hmget(taskContentRedisKey);
            TaskDetail taskDetail = DataUtils.mapCastTargetObject(map, TaskDetail.class);
            if (taskDetail != null) {
                total += taskDetail.getTask_progress().getTotal();
                executed += taskDetail.getTask_progress().getFail() + taskDetail.getTask_progress().getSuccess();
            }
        }
        taskProgress.setTotal(total);
        taskProgress.setExecuted(executed);
        return taskProgress;
    }

    @Override
    public JsonResult<String> wechatMsg(FailDeviceWechatMsgReq req) {

        Optional<CentralizedControlCode> codeOp = centralizedControlCodeDao.findById(req.getCentralizedControlCodeId());
        if (codeOp.isEmpty()) {
            return JsonResult.error("当前没有集控码,无法上报", null, null);
        }
        CentralizedControlCode code = codeOp.get();
        List<IotDevice> deviceList = deviceDao.findByIdIn(req.getDeviceIds());
        SysUser user;
        try {
            user = sysUserService.getUserByEmpNo(code.getAdminHrId());
        } catch (Exception e) {
            log.error("获取人员信息失败:" + code.getAdminHrId());
            log.error(e.getMessage());
            return JsonResult.error("人员不存在", null, null);
        }

        Optional<PassV2UserResp> userRespOptional = sysUserService.getUserByUid(req.getUid());
        if (userRespOptional.isEmpty()) {
            return JsonResult.error("人员不存在", null, null);
        }
        PassV2UserResp userResp = userRespOptional.get();
        /*SysUser sysUser;
        try {
            sysUser = sysUserService.getUserByEmpNo(JwtUtil.getHrId(TokenUtils.getToken()));
        } catch (Exception e) {
            log.error("获取人员信息失败:" + code.getAdminHrId());
            log.error(e.getMessage());
            return JsonResult.error("人员不存在", null, null);
        }*/

        for(IotDevice device : deviceList){
            // 推送设备上报消息提
            MessagePoolRecord messagePoolRecord = new MessagePoolRecord();
            messagePoolRecord.setMsgType(3);
            messagePoolRecord.setPriority(0);
            messagePoolRecord.setSendType(0);
            messagePoolRecord.setReceiveType(501);
            messagePoolRecord.setMsgTitle("设备执行失败通知");

            messagePoolRecord.setReceivePerson(user.getWxOpenId());
            messagePoolRecord.setReceivePersonId(user.getWxOpenId());
            messagePoolRecord.setMsgDetailsUrl("");
            messagePoolRecord.setMsgTemplateId(vxMsgTemplateId);
            messagePoolRecord.setTipType(1);
            MessageBody messageBody = new MessageBody();
            messageBody.setTitle("你好，" + userResp.getName() + "/" + userResp.getHrId() + "给你提报了设备执行失败故障，请及时联系并处理！");
            messageBody.setType("智能控制失败");
            messageBody.setDepart("待处理");
            messageBody.setTime(DateUtils.formatLocalDateTime(LocalDateTime.now(), DateUtils.PATTERN_DATETIME));
            messageBody.setRemark("设备位置：" + device.getProject().getProjectArea().getName() + "/" + SpaceService.getFullSpaceName(device.getSpace(), device.getSpace().getName())
                    + ";" + device.getHwDeviceName());
            messagePoolRecord.setMsgContent(JSON.toJSONString(messageBody));

            // 微信消息推送
            sendService.send(messagePoolRecord);
        }
        return JsonResult.okNoData("OK");
    }

    @Override
    public ResponseEntity<JsonResult<String>> saveAdmin(CentralizedControlAdminReq req) {

        if (!CollectionUtils.isEmpty(centralizedControlAdminDao.findAll())) {
            return ResponseEntity.ok(JsonResult.error("管理员已存在，不可重复添加", null, null));
        }
        CentralizedControlAdmin admin = new CentralizedControlAdmin();
        BeanUtils.copyProperties(req, admin);
        try {
            SysUser sysUser = sysUserService.getUserByEmpNo(req.getHrId());
            admin.setName(sysUser.getNameZh());
        } catch (Exception e) {
            return ResponseEntity.ok(JsonResult.error("工号不存在,不可添加", null, null));
        }

        // 保存管理员
        centralizedControlAdminDao.save(admin);
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @Override
    public ResponseEntity<JsonResult<CentralizedControlAdminReq>> getAdmin() {
        List<CentralizedControlAdmin> all = centralizedControlAdminDao.findAll();
        CentralizedControlAdminReq adminReq = new CentralizedControlAdminReq();
        if (!CollectionUtils.isEmpty(all)) {
            BeanUtils.copyProperties(all.get(0), adminReq);
        }
        return ResponseEntity.ok(JsonResult.ok("OK", adminReq));
    }

    @Override
    public ResponseEntity<JsonResult<String>> delAdmin(String id) {

        Optional<CentralizedControlAdmin> adminOp = centralizedControlAdminDao.findById(id);
        if (adminOp.isEmpty()) {
            return ResponseEntity.ok(JsonResult.error("id有误，数据不存在", null, null));
        }
        CentralizedControlAdmin admin = adminOp.get();
        admin.setIsDelete(1);
        // 保存管理员
        centralizedControlAdminDao.save(admin);
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @Override
    public ResponseEntity<JsonResult<CentralizedControlCodeDetailResp>> getCentralizedControlCodeDetail(String id) {

        //根据id查询
        Optional<CentralizedControlCode> centralizedControlCodeOp = centralizedControlCodeDao.findById(id);

        //centralizedControlCodeOp 不存在
        if (!centralizedControlCodeOp.isPresent()) {
            return ResponseEntity.ok(JsonResult.error("未找到对应的集控码", null, ErrConst.E01));
        }

        //获取CentralizedControlCode对象
        CentralizedControlCode code = centralizedControlCodeOp.get();

        //创建返回参数
        CentralizedControlCodeDetailResp resp = new CentralizedControlCodeDetailResp();
        resp.setId(id);
        resp.setName(code.getName());
        resp.setProjectId(code.getProjectId());
        resp.setSpaceId(code.getSpaceId());
        resp.setControlDeviceRange(code.getControlDeviceRange());
        resp.setAdminHrId(code.getAdminHrId());
        resp.setAdminName(code.getAdminName());
        //控制的设备类型列表
        List<DeviceType> controlDeviceTypeList = Arrays.stream(code.getControlDeviceType().split(",")).map(DeviceType::valueOf).collect(Collectors.toList());
        resp.setControlDeviceTypeList(controlDeviceTypeList);
        //控制部分设备详情
        if (ControlDeviceRange.PART.equals(code.getControlDeviceRange())) {
            //创建控制部分设备详情List
            List<CentralizedControlCodeSelectDeviceResp> deviceRespList = new ArrayList<>();

            //获取集控码部分设备
            List<IotDevice> codeDeviceList = getCentralizedCodeDevice(code);

            if (!CollectionUtils.isEmpty(codeDeviceList)) {
                codeDeviceList.forEach(it -> {
                    //创建控制部分设备详情对象
                    CentralizedControlCodeSelectDeviceResp deviceResp = new CentralizedControlCodeSelectDeviceResp();
                    deviceResp.setDeviceId(it.getId());
                    deviceResp.setDeviceName(it.getDeviceName());
                    deviceResp.setDeviceSn(it.getSn());
                    deviceResp.setHwStatus(it.getHwStatus());
                    deviceResp.setDeviceType(it.getDeviceType());
                    if (ObjectUtils.isNotEmpty(it.getDeviceType())) {
                        deviceResp.setDeviceTypeDesc(it.getDeviceType().getName());
                    }
                    deviceRespList.add(deviceResp);
                });
            }
            resp.setDeviceRespList(deviceRespList);
        }

        return ResponseEntity.ok(JsonResult.ok("OK", resp));
    }

    private List<IotDevice> getCentralizedCodeDevice(CentralizedControlCode code) {

        List<IotDevice> codeDeviceList = new ArrayList<>();

        //根据集控码id查询关联设备
        List<CentralizedCodeDeviceRelation> relationList = centralizedCodeDeviceRelationDao.findByCentralizedControlCodeId(code.getId());

        //根据设备id查询缓存中设备信息
        relationList.forEach(it -> {
            //创建deviceKey module + deviceId + separator
            String deviceKey = redisUtil.createDeviceIdKey(IConst.HW_DEVICE_MODULE, it.getDeviceId(), RedisUtil.Separator.COLON);
            //判断缓存中key是否存在
            if (redisUtil.hasKey(deviceKey)) {
                //缓存中获取设备信息
                Map<Object, Object> redisMap = redisUtil.hmget(deviceKey);
                IotDevice device = DataUtils.mapCastTargetObject(redisMap, IotDevice.class);
                device.setId(it.getDeviceId());
                codeDeviceList.add(device);
            } else {
                //缓存中不存在
                //根据设备id查询设备信息，存入缓存
                Optional<IotDevice> deviceOp = deviceDao.findById(it.getDeviceId());
                if (deviceOp.isPresent()) {
                    IotDevice iotDevice = deviceOp.get();
                    //传入缓存
                    iotDeviceCacheService.deviceRedis(iotDevice);
                    codeDeviceList.add(iotDevice);
                }
            }
        });

        return codeDeviceList;
    }

    @Override
    public ResponseEntity<JsonResult<PageBean<CentralizedControlCodeSelectDeviceResp>>> getCentralizedControlCodeControlDevice(CentralizedControlCodeControlDeviceReq req) {
        int page = req.getPage();
        int size = req.getSize();
        // 创建分页查询对象

        Optional<CentralizedControlCode> centralizedControlCodeOp = centralizedControlCodeDao.findById(req.getId());
        if (!centralizedControlCodeOp.isPresent()) {
            return ResponseEntity.ok(JsonResult.error("未找到对应集控码", null, ErrConst.E01));
        }
        CentralizedControlCode centralizedControlCode = centralizedControlCodeOp.get();

        if (StringUtils.isEmpty(centralizedControlCode.getSpaceId())) {
            return ResponseEntity.ok(JsonResult.error("该集控码未绑定空间", null, ErrConst.E01));
        }

        //创建返回list
        List<CentralizedControlCodeSelectDeviceResp> respList = new ArrayList<>();

        //查询集控码下绑定设备
        List<IotDevice> codeBindingDevice = new ArrayList<>();
        getDeviceControlToRedisByCentralizedCode(centralizedControlCode, req.getDeviceType(), codeBindingDevice);
        //非空判断
        if (!CollectionUtils.isEmpty(codeBindingDevice)) {
            codeBindingDevice.forEach(it -> {
                if (req.getDeviceType().equals(it.getDeviceType())) {
                    CentralizedControlCodeSelectDeviceResp resp = new CentralizedControlCodeSelectDeviceResp();
                    resp.setDeviceId(it.getId());
                    resp.setDeviceName(it.getDeviceName());
                    resp.setDeviceSn(it.getSn());
                    /*
                     * 使用策略模式处理不同设备类型的逻辑
                     * 传入设备类型的枚举值，FunctionExecutor通过策略工厂拿到相应的设备类型的对象，
                     * 然后去执行对应的功能模块
                     */
                    FunctionExecutor functionExecutor = new FunctionExecutor(StrategyFactory.getDeviceTypeStrategy(String.valueOf(it.getDeviceType())));
                    DeviceStatus deviceStatus = functionExecutor.getNewestHwStatus(it);
                    if (Objects.nonNull(deviceStatus)) {
                        it.setHwStatus(deviceStatus);
                    }
                    resp.setHwStatus(it.getHwStatus());
                    resp.setDeviceType(it.getDeviceType());
                    if (ObjectUtils.isNotEmpty(it.getDeviceType())) {
                        resp.setDeviceTypeDesc(it.getDeviceType().getName());
                    }
                    respList.add(resp);
                }
            });
        }

        PageBean<CentralizedControlCodeSelectDeviceResp> pageBean = PageBean.createPageBeanByAllData(respList, page, size);

        return ResponseEntity.ok(JsonResult.ok("OK", pageBean));
    }

    @Override
    public ResponseEntity<JsonResult<PageBean<RecordResp>>> recordPage(RecordReq req) {

        PageBean<RecordResp> pageBean = new PageBean<>();

        Sort sort = Sort.by(Sort.Direction.DESC, "controlTime");
        Pageable pageable = PageRequest.of(req.getPage(), req.getSize(), sort);
        Page<CentralizedControlRecord> recordPage = centralizedControlRecordDao.findAll((root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> list = new ArrayList<>();

            list.add(criteriaBuilder.equal(root.get("centralizedControlCodeId"), req.getId()));
            // 姓名 工号
            if (StringUtils.isNotEmpty(req.getKeyword())) {
                String like = "%" + req.getKeyword() + "%";
                list.add(criteriaBuilder.or(criteriaBuilder.like(root.get("hrId"), like), criteriaBuilder.like(root.get("name"), like)));
            }

            // 开始时间
            if (req.getStartTime() != null) {
                list.add(criteriaBuilder.greaterThanOrEqualTo(root.get("controlTime"),
                        req.getStartTime()));
            }
            // 结束时间
            if (req.getEndTime() != null) {
                list.add(criteriaBuilder.lessThanOrEqualTo(root.get("controlTime"),
                        req.getEndTime()));
            }
            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, pageable);

        List<RecordResp> respList = new ArrayList<>();

        recordPage.getContent().forEach(record -> {
            RecordResp recordResp = new RecordResp();
            BeanUtils.copyProperties(record, recordResp);
            recordResp.setTypeName(record.getType().getName());
            recordResp.setStatusName(record.getStatus().getName());
            respList.add(recordResp);
        });
        pageBean.setPage(req.getPage());
        pageBean.setSize(req.getSize());
        pageBean.setList(respList);
        pageBean.setTotalPages(recordPage.getTotalPages());
        pageBean.setTotalElements(recordPage.getTotalElements());

        return ResponseEntity.ok(JsonResult.ok("OK", pageBean));
    }

    @Override
    public ResponseEntity<JsonResult<PageBean<FailDeviceRecordResp>>> failDeviceRecord(String traceID, int page, int size) {

        // 返回list
        List<FailDeviceRecordResp> respList = new ArrayList<>();
        JsonResult<List<IotDevice>> jsonResult = getDeviceControlToRedis(traceID, "fail");
        List<IotDevice> deviceList = jsonResult.getData();
        deviceList.forEach(device -> {
            FailDeviceRecordResp resp = new FailDeviceRecordResp();
            resp.setId(device.getId());
            resp.setHwDeviceId(device.getHwDeviceId());
            resp.setDeviceName(device.getDeviceName());
            resp.setStatus(device.getHwStatus());
            resp.setDeviceType(device.getDeviceType());
            resp.setDeviceTypeName(device.getDeviceType().getName());
            resp.setAddress(device.getProject().getProjectName() + " / " + SpaceService.getFullSpaceName(device.getSpace(), device.getSpace().getName()));
            resp.setIp("");
            respList.add(resp);
        });

        PageBean<FailDeviceRecordResp> pageBean = PageBean.createPageBeanByAllData(respList, page, size);
        JsonResult<PageBean<FailDeviceRecordResp>> result = JsonResult.ok("OK", pageBean);
        result.setCode(jsonResult.getCode());
        return ResponseEntity.ok(result);
    }

    private JsonResult<List<IotDevice>> getDeviceControlToRedis(String traceID, String status) {

        JsonResult<List<IotDevice>> jsonResult = JsonResult.ok("OK", null);

        List<IotDevice> deviceList = new ArrayList<>();

        String traceIdRedisKey = redisUtil.createDeviceIdKey(IConst.CENTRALIZED, traceID, RedisUtil.Separator.COLON);
        // List<String> taskIDs = redisUtil.lGet(traceIdRedisKey, 0, 1, String.class);
        List<String> taskIDs = (List<String>) redisUtil.lGet(traceIdRedisKey, 0, -1);
        // 根据tracId更新集控控制记录状态
        if (getTaskDetail(taskIDs, jsonResult)) {

            updateRecordStatus(traceID);
            unLock(traceID);
        } else {
            resumeLock(traceID);
            AtomicInteger count = new AtomicInteger(0);
            Runnable runnable = () -> {
                boolean flag=true;
                count.incrementAndGet();
                if(count.get() > 15){
                    flag=false;
                }
                while (flag) {
                    if( !getTaskDetail(taskIDs, jsonResult)){
                        ThreadUtil.sleep(3000);
                    }else {
                        updateRecordStatus(traceID);
                        flag=false;
                    }
                }
            };
            ThreadPoolUtils.EXECUTOR_SERVICE_REDIS.submit(runnable);
        }
        // 对记录解锁
        if (!getTaskDetailTime(taskIDs, traceID)) {
            unLock(traceID);
            Optional<CentralizedControlRecord> controlRecordOp = centralizedControlRecordDao.findByTraceId(traceID);
            if (controlRecordOp.isPresent()) {
                CentralizedControlRecord controlRecord = controlRecordOp.get();
                controlRecord.setStatus(DeviceControlStatus.FAIL);
                centralizedControlRecordDao.save(controlRecord);
            }
            jsonResult.setCode("0");
        }
        /*for (String taskId : taskIDs) {
            String taskContentRedisKey = redisUtil.createDeviceIdKey(IConst.CENTRALIZED_TASK, taskId, RedisUtil.Separator.COLON);
            Map<Object, Object> map = redisUtil.hmget(taskContentRedisKey);
            TaskDetail taskDetail = DataUtils.mapCastTargetObject(map, TaskDetail.class);
            if (taskDetail != null && (StringUtils.equals(taskDetail.getStatus(), "WAITTING") || StringUtils.equals(taskDetail.getStatus(), "PROCESSING") || StringUtils.equals(taskDetail.getStatus(), "FAILWAITRETRY"))) {
                jsonResult.setCode("1");
                break;
            }
        }*/

        String taskDetailRedisKey = redisUtil.createDeviceRealKey(IConst.CENTRALIZED_TRACE, status, traceID, RedisUtil.Separator.COLON);
        List<TaskDetail> taskDetailList = redisUtil.lGet(taskDetailRedisKey, 0, -1, TaskDetail.class);
        if (CollectionUtils.isEmpty(taskDetailList)) {
            jsonResult.setData(deviceList);
            return jsonResult;
        }
        taskDetailList.forEach(taskDetail -> {
            Optional<IotDevice> deviceOp = deviceDao.findByHwDeviceId(taskDetail.getTarget());
            deviceOp.ifPresent(deviceList::add);
        });
        jsonResult.setData(deviceList);
        return jsonResult;
    }

    private void updateRecordStatus(String traceID) {
        Optional<CentralizedControlRecord> controlRecordOp = centralizedControlRecordDao.findByTraceId(traceID);
        if (controlRecordOp.isPresent()) {
            CentralizedControlRecord controlRecord = controlRecordOp.get();
            String taskDetailRedisKey = redisUtil.createDeviceRealKey(IConst.CENTRALIZED_TRACE, "fail", traceID, RedisUtil.Separator.COLON);
            List<TaskDetail> taskDetailList = redisUtil.lGet(taskDetailRedisKey, 0, -1, TaskDetail.class);
            if (!CollectionUtils.isEmpty(taskDetailList)) {
                controlRecord.setStatus(DeviceControlStatus.FAIL);
            } else {
                controlRecord.setStatus(DeviceControlStatus.SUCCESS);
            }
            centralizedControlRecordDao.save(controlRecord);
        }
    }

    private boolean getTaskDetail(List<String> taskIDs, JsonResult jsonResult) {
        for (String taskId : taskIDs) {
            String taskContentRedisKey = redisUtil.createDeviceIdKey(IConst.CENTRALIZED_TASK, taskId, RedisUtil.Separator.COLON);
            Map<Object, Object> map = redisUtil.hmget(taskContentRedisKey);
            TaskDetail taskDetail = DataUtils.mapCastTargetObject(map, TaskDetail.class);
            if (ObjectUtils.isNotEmpty(taskDetail) && StringUtils.equals(taskDetail.getStatus(), "WAITTING") || StringUtils.equals(taskDetail.getStatus(), "PROCESSING") || StringUtils.equals(taskDetail.getStatus(), "FAILWAITRETRY")) {
                jsonResult.setCode("1");
                return false;
            }
        }
        return true;
    }

    Map<String, Integer> traceNum = new ConcurrentHashMap<>();

    private boolean getTaskDetailTime(List<String> taskIDs, String traceID) {
        List<Long> longs = new ArrayList<>();
        for (String taskId : taskIDs) {
            String taskContentRedisKey = redisUtil.createDeviceIdKey(IConst.CENTRALIZED_TASK, taskId, RedisUtil.Separator.COLON);
            Map<Object, Object> map = redisUtil.hmget(taskContentRedisKey);
            TaskDetail taskDetail = DataUtils.mapCastTargetObject(map, TaskDetail.class);
            if (ObjectUtils.isNotEmpty(taskDetail)) {
                longs.add(taskDetail.getT());
            }
        }
        longs.sort(Collections.reverseOrder());
        if (CollectionUtils.isEmpty(longs)) {
            if (traceNum.containsKey(traceID)) {
                Integer count = traceNum.get(traceID);
                if (count > 5) {
                    return false;
                } else {
                    traceNum.put(traceID, ++count);
                    return true;
                }
            } else {
                traceNum.put(traceID, 0);
                return true;
            }
        }
        return (Calendar.getInstance().getTimeInMillis() - longs.get(0)) / 60000 <= 2;
    }

    private void getDeviceControlToRedisByCentralizedCode(CentralizedControlCode centralizedCode, DeviceType deviceType, List<IotDevice> iotDevices) {
        //创建key
        String controlDeviceListKey = redisUtil.createCentralizedCodeDeviceListKey(CENTRALIZED_CONTROL_CODE, deviceType.name(), centralizedCode.getId(), RedisUtil.Separator.COLON);
        //判断key是否存在

        if (redisUtil.hasKey(controlDeviceListKey)) {
            //根据key从redis中获取关联设备idList
            List<String> deviceList = redisUtil.lGet(controlDeviceListKey, 0, -1).stream().map(Object::toString).collect(Collectors.toList());
            if (CollUtil.isNotEmpty(deviceList)) {
                deviceList.forEach(deviceId ->
                        getIotDeviceFromRedisFilterDeviceType(deviceId, iotDevices, deviceType)
                );
            }
        } else {
            if (StringUtils.equals(centralizedCode.getControlDeviceRange().name(), ControlDeviceRange.PART.name())) {
                //集控码控制部分设备，查询集控码设备关联表
                List<CentralizedCodeDeviceRelation> relations = centralizedCodeDeviceRelationDao.findAllByCentralizedControlCodeId(centralizedCode.getId());
                //根据设备id查询redis中所有设备信息
                relations.forEach(relation ->
                        getIotDeviceFromRedisFilterDeviceType(relation.getDeviceId(), iotDevices, deviceType)
                );
            } else {
                //所有设备
                //从redis中查询所有空间
                List<String> spaceList = spaceService.getChildIdListByParentIdFromCache(centralizedCode.getSpaceId());
                spaceList.add(centralizedCode.getSpaceId());
                spaceList.forEach(it -> {
                    //根据空间id和设备类型查询设备
                    List<IotDevice> deviceList = deviceDao.findBySpaceId(it);
                    deviceList.forEach(device -> {
                        if (StringUtils.equals(deviceType.name(), String.valueOf(device.getDeviceType()))) {
                            iotDevices.add(device);
                        }
                    });
                });
            }
        }

    }

    /**
     * 从Redis中获取设备数据
     *
     * @param deviceId
     * @param iotDevices
     */
    private void getIotDeviceFromRedis(String deviceId, List<IotDevice> iotDevices) {
        String deviceKey = redisUtil.createDeviceIdKey(IConst.HW_DEVICE_MODULE, deviceId, RedisUtil.Separator.COLON);

        if (redisUtil.hasKey(deviceKey)) {
            Map<Object, Object> map = redisUtil.hmget(deviceKey);
            IotDevice iotDevice = DataUtils.mapCastTargetObject(map, IotDevice.class);
            if (Objects.nonNull(iotDevice)) {
                iotDevice.setId(deviceId);
                iotDevices.add(iotDevice);
            }
            log.info("从Redis中获取数据{}", iotDevice);
        } else {
            Optional<IotDevice> iotDeviceOptional = deviceDao.findById(deviceId);
            iotDeviceOptional.ifPresent(iotDevices::add);
        }
    }

    /**
     * 从Redis中获取设备数据
     *
     * @param deviceId
     * @param iotDevices
     */
    private void getIotDeviceFromRedisFilterDeviceType(String deviceId, List<IotDevice> iotDevices, DeviceType deviceType) {
        String deviceKey = redisUtil.createDeviceIdKey(IConst.HW_DEVICE_MODULE, deviceId, RedisUtil.Separator.COLON);

        if (redisUtil.hasKey(deviceKey)) {
            Map<Object, Object> map = redisUtil.hmget(deviceKey);
            IotDevice iotDevice = DataUtils.mapCastTargetObject(map, IotDevice.class);
            if (Objects.nonNull(iotDevice) && StringUtils.equals(deviceType.name(), String.valueOf(iotDevice.getDeviceType()))) {
                iotDevice.setId(deviceId);
                iotDevices.add(iotDevice);
            }
            log.info("从Redis中获取数据{}", iotDevice);
        } else {
            Optional<IotDevice> iotDeviceOptional = deviceDao.findByIdAndDeviceType(deviceId, deviceType);
            iotDeviceOptional.ifPresent(iotDevices::add);
        }
    }
}
