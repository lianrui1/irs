package cn.com.cowain.ibms.dao.qrcode;

import cn.com.cowain.ibms.entity.qrcode.QrcodeAccessRulesToStaffGroup;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/4/7 10:38
 */
public interface QrcodeAccessRuleToStaffGroupDao extends JpaRepository<QrcodeAccessRulesToStaffGroup, String> {

    /**
     * 根据权限查询关系列表
     *
     * @param ruleId 权限id
     * @return 关系list
     * @author Yang.Lee
     * @date 2021/4/8 10:06
     **/
    List<QrcodeAccessRulesToStaffGroup> findByRuleId(String ruleId);

    /**
     * 根据门磁id查询关系列表
     *
     * @param doorMagneticId 门磁Id
     * @return 关系list
     * @author Yang.Lee
     * @date 2021/4/9 9:21
     **/
    List<QrcodeAccessRulesToStaffGroup> findByRuleDoorMagneticId(String doorMagneticId);

    /**
     * 根据人员组id查询关系列表
     *
     * @param staffGroupId 人员组ID
     * @return 关系list
     */
    List<QrcodeAccessRulesToStaffGroup> findByGroupId(String staffGroupId);
}
