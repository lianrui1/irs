package cn.com.cowain.ibms.service.iot;

import cn.com.cowain.ibms.rest.resp.iot.AccessControlSpotApproverPageReq;
import cn.com.cowain.ibms.service.bean.ServiceResult;

public interface AccessControlSpotApproverService {


    // 门禁点通行权限分配人列表
    ServiceResult accessControlSpotApproverPage(int page, int size);

    // 添加门禁点通行权限分配人
    ServiceResult accessControlSpotApproverSave(AccessControlSpotApproverPageReq req);

    // 获取门禁点通行权限分配人详情
    ServiceResult accessControlSpotApproverDetails(String id);

    // 删除门禁点通行权限分配人
    ServiceResult accessControlSpotApproverDelete(String id);

    // 编辑门禁点通行权限分配人
    ServiceResult accessControlSpotApproverUpdate(String id, AccessControlSpotApproverPageReq req);
}
