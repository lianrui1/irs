package cn.com.cowain.ibms.rest.req.distinguish;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/8/3 10:49
 */
@Data
public class ImportFailedReq {

    private String errorDataId;
    private String errorKsHkId;
    private String errorNtHkId;
    private int totalCount;
}
