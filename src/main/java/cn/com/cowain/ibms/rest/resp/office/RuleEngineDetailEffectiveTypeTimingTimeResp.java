package cn.com.cowain.ibms.rest.resp.office;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalTime;
import java.util.List;

/**
 * 生效方式:定时生效时间
 *
 * @author: yanzy
 * @date: 2022/2/14 20:29
 */
@Data
@ApiModel("生效方式:定时生效时间返回对象")
public class RuleEngineDetailEffectiveTypeTimingTimeResp {
    @ApiModelProperty(value = "星期 1,2,3,4,5,6,7")
    private List<String> weeks;

    @ApiModelProperty(value = "开始时间")
    @JsonFormat(pattern = "HH:mm")
    private LocalTime startTime;

    @ApiModelProperty(value = "结束时间")
    @JsonFormat(pattern = "HH:mm")
    private LocalTime endTime;
}
