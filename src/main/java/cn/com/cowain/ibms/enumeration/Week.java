package cn.com.cowain.ibms.enumeration;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/6 15:48
 */
public enum  Week {

    MONDAY,

    TUESDAY,

    WEDNESDAY,

    THURSDAY,

    FRIDAY,

    SATURDAY,

    SUNDAY
}
