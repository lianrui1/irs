package cn.com.cowain.ibms.enumeration.iot;

public enum DeviceLinkStatus {

    /**
     * 正常
     */
    NORMAL("启用"),

    /**
     * 禁用
     */
    DISABLE("禁用"),

    /**
     * 失效
     */
    INVALID("失效");

    DeviceLinkStatus(String name) {
        this.name = name;
    }

    private String name;

    public String getName() {
        return this.name;
    }
}
