package cn.com.cowain.ibms.mq.bean;

import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2022/3/3 9:59
 */
@Data
public class BizReturnMessage {

    private String resourceId;

    private String failCode;

    private String failMsg;

    private String jobNo;
}
