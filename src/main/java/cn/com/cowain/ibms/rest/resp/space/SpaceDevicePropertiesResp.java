package cn.com.cowain.ibms.rest.resp.space;

import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.rest.resp.DeviceResp;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/3/2 17:19
 */
@Data
@ApiModel("空间下设备属性响应对象")
public class SpaceDevicePropertiesResp {

    @ApiModelProperty(value = "设备Id", required = true)
    private String deviceId;

    @ApiModelProperty(value = "设备名称", required = true, position = 1, example = "空调XO0001")
    private String deviceName;

    @ApiModelProperty(value = "设备状态（ONLINE：设备在线。OFFLINE：设备离线。ABNORMAL：设备异常。INACTIVE：设备未激活。FROZEN：设备冻结）",
            required = true, position = 2, example = "ONLINE")
    private DeviceStatus hwStatus;

    @ApiModelProperty(
            value = "设备类型（DEFAULT：未知类型设备；DOOR_PLATE：门牌；DOOR_MAGNETIC：门磁，LIGHT：灯；SCREEN：屏幕；" +
                    "AIR_CONDITIONER：空调；ILLUMINATION_SENSING：光照度传感器）",
            required = true,
            position = 3,
            example = "DOOR_PLATE")
    private DeviceType deviceType;

    @ApiModelProperty(value =  "HW设备ID", required = true)
    private String hwDeviceId;

    @ApiModelProperty(value = "设备属性")
    private DeviceResp deviceResp;
}
