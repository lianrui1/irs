package cn.com.cowain.ibms.rest.req.meeting;


import lombok.Data;

/**
 *
 * @author: yanzy
 * @date: 2022/5/13 15:32
 */
@Data
public class InvitationUpdateReq {

    // 访问地址 公司表oa_company 公司id
    private String address;

    // 最新审批人 审批人工号（非经理需要审批
    private String assginee;

    // 员工：访问地点公司id
    private String companyId;

    // 开始时间
    private String startTime;

    // 结束时间
    private String endTime;

    // 事由
    private String reasonCode;

    // 访客类型code
    private String roleCode;

    // 操作。修改时新增了会议室[1]，取消了会议室[-1]，修改了会议室[0]。无变化[2]
    private Integer meetingOperate;

    //访客单号
    private String visitorRecordNo;

    private InvitationMeeting meeting;

    //备注
    private String remarks;
}
