package cn.com.cowain.ibms.rest.resp.iot;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2020/12/1 17:59
 */
@Data
public class DevicePasswordCheckResp {

    public DevicePasswordCheckResp(){}

    /**
     * 构造函数
     * @param isValided
     */
    public DevicePasswordCheckResp(boolean isValided){
        this.isValided = isValided;
    }

    /**
     * 会议室是否拥有门磁
     */
    @ApiModelProperty(value = "设备密码是否有效", required = true, example = "true: 有效; false: 无效")
    private boolean isValided;
}
