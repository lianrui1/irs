package cn.com.cowain.ibms.service.impl;

import cn.com.cowain.ibms.component.WeChatSendMsgUtils;
import cn.com.cowain.ibms.dao.ReservationRecordDao;
import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.entity.ReservationRecordItem;
import cn.com.cowain.ibms.entity.meeting.GuestInvitationRecord;
import cn.com.cowain.ibms.rest.req.InviteeStatusReq;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.WeChatService;
import cn.com.cowain.ibms.service.bean.WeChatMsg;
import cn.com.cowain.ibms.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Optional;
import java.util.Set;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/14/20
 */
@Slf4j
@Service
public class WeChatServiceImpl implements WeChatService {

    private WeChatSendMsgUtils weChatSendMsgUtils;

    @Resource(name="sysUserServiceUC")
    private SysUserService sysUserServiceUC;

    private ReservationRecordDao reservationRecordDao;

    @Value("${wechat.msg.template.service-remind}")
    private String templateId;

    @Value("${cowain.emp.executor.list}")
    private Set<String> executorList;

    @Value("${wechat.details.url}")
    private String  detailsUrl;

    private static final String WITH_OUT_WECHAT_ID = "没有wxid";

    private static final String TO_USER_WECHAT_ID = "toUserWxId(userid) :";

    public WeChatServiceImpl(WeChatSendMsgUtils weChatSendMsgUtils,
                             ReservationRecordDao reservationRecordDao) {
        this.weChatSendMsgUtils = weChatSendMsgUtils;
        this.reservationRecordDao = reservationRecordDao;
    }

    /**
     * @param url        跳转url
     * @param toUserWxId 目标wxId
     * @param weChatMsg  信息数组/集合
     * @return
     * @author 胡荆陵
     */
    @Override
    public String sendMsg(String url, String toUserWxId, WeChatMsg weChatMsg) {
        log.debug(templateId);
        return weChatSendMsgUtils.sendMsg3Lines(templateId, url, toUserWxId, weChatMsg);
    }

    /**
     * 创建会议时发送 weChat推送
     *
     * @param empNo           参会人工号
     * @param reservationRecord 会议预约会议信息
     * @return
     * @author 张宇鑫
     */
    @Override
    public void createMeetingReminders(String empNo, ReservationRecord reservationRecord) {
        //会议详情跳转页
        String url = detailsUrl +  reservationRecord.getId();
        //参会人微信opinionId
        String toUserWxId = sysUserServiceUC.getUserByEmpNo(empNo).getWxOpenId();
        if (StringUtils.isNoneEmpty(toUserWxId)) {
            //发起人姓名
            String initiatorName = sysUserServiceUC.getUserByEmpNo(reservationRecord.getInitiatorEmpNo()).getNameZh();
            //会议主题
            String topic = reservationRecord.getTopic();
            if(topic.equals("无主题")){
                topic = "";
            }else{
                topic = "-"+topic;
            }
            // 推送 标题
            String first = String.format("来自%s的会议邀请%s", initiatorName,topic);
            //会议主题-会议室名称
            String keyword1 = "会议邀请-" + reservationRecord.getRoom().getName();
            //服务状态
            String keyword2 = "已邀请";
            //是否是行政人员 1 true  0 false
            String ketword4 = "0";
            WeChatMsg weChatMsg = createRecordWeChatMsg(reservationRecord, first, keyword1, keyword2 ,ketword4);
            String msgId = sendMsg(url, toUserWxId, weChatMsg);
            log.debug(msgId);
        } else {
            log.info("用户工号" + empNo + WITH_OUT_WECHAT_ID);
        }
    }


    /**
     * 拒绝会议时发送 weChat推送
     *
     * @param id               会议id
     * @param inviteeStatusReq 参会人信息
     * @author 张宇鑫
     */
    @Override
    public void channelMeetingReminders(String id, InviteeStatusReq inviteeStatusReq) {
        //获取会议记录
        Optional<ReservationRecord> reservationRecordOptional = reservationRecordDao.findById(id);
        if (reservationRecordOptional.isPresent()) {
            //会议详情跳转页
            String url = detailsUrl + id;
            //发起人微信opinionId
            String toUserWxId = sysUserServiceUC.getUserByEmpNo(reservationRecordOptional.get().getInitiatorEmpNo()).getWxOpenId();
            if (toUserWxId != null) {
                //参与人姓名
                String ownerName = sysUserServiceUC.getUserByEmpNo(inviteeStatusReq.getEmpNo()).getNameZh();
                // 推送 标题
                String first = String.format("您好，%s拒绝了你的会议邀请", ownerName);
                //会议主题
                String keyword1 = "会议拒绝-" + reservationRecordOptional.get().getRoom().getName();
                //会议室名称
                String keyword2 = "已拒绝";
                //会议开始时间
                String timeFrom = DateUtils.formatLocalDateTime(reservationRecordOptional.get().getFrom(), DateUtils.PATTERN_DATETIME1);
                String timeTo = DateUtils.formatLocalDateTime(reservationRecordOptional.get().getTo(), DateUtils.PATTERN_TIME1);
                String remark = reservationRecordOptional.get().getRemark();
                WeChatMsg weChatMsg = WeChatMsg.builder().first(first)
                        .keyword1(keyword1).keyword2(keyword2).keyword3(timeFrom+"-"+timeTo).remark(remark).build();
                String msgId = sendMsg(url, toUserWxId, weChatMsg);
                log.debug(msgId);
            } else {
                log.info("用户id" + reservationRecordOptional.get().getInitiator() + WITH_OUT_WECHAT_ID);
            }
        }
    }

    /**
     * 更新会议时 weChat 推送提醒
     *
     * @param reservationRecord 会议状态
     * @param pushUserSet       要推送的人员id
     */
    @Override
    public void changeMeetingReminders(ReservationRecord reservationRecord, Set<String> pushUserSet) {
        log.info("pushUserSet:" + pushUserSet);
        String initiatorName = sysUserServiceUC.getUserByEmpNo(reservationRecord.getInitiatorEmpNo()).getNameZh();
        log.info("initiatorName: " + initiatorName);
        pushUserSet.forEach(it -> {
                if (!it.equals(reservationRecord.getInitiator())) {
                    String url = detailsUrl+ reservationRecord.getId();
                    String toUserWxId = sysUserServiceUC.getUserByEmpNo(it).getWxOpenId();
                    if (toUserWxId != null) {
                        //会议主题
                        String topic = reservationRecord.getTopic();
                        if(topic.equals("无主题")){
                            topic = "";
                        }else{
                            topic = "-"+topic;
                        }
                        // 推送 标题
                        String first = String.format("%s已变更会议%s", initiatorName,topic);
                        //会议主题+会议开始时间
                        String keyword1 = "会议变更-" + reservationRecord.getRoom().getName();
                        //会议状态
                        String keyword2 = "已变更";
                        ////是否是行政人员 1 true  0 false
                        String ketword4 = "0";
                        WeChatMsg weChatMsg = createRecordWeChatMsg(reservationRecord, first, keyword1, keyword2 ,ketword4);
                        String msgId = sendMsg(url, toUserWxId, weChatMsg);
                        log.debug(msgId);
                    } else {
                        log.info("用户id" + reservationRecord.getInitiator() + WITH_OUT_WECHAT_ID);
                    }
                }
        });
    }

    /**
     * 特殊要求提提
     *
     * @param reservationRecord 会议信息
     */
    @Override
    public void specialRequirementsReminders(ReservationRecord reservationRecord) {
        //是否是行政人员 1 true  0 false
        String ketword4 = "1";
        //会议详情url
        String url = detailsUrl +  reservationRecord.getId()+"&type="+ketword4;
        log.info("会议详情url:+"+url);
        String finalServiceName = reservationRecord.getService().replace(",", "},{");
        executorList.forEach(toUserWxId -> {
            log.info(TO_USER_WECHAT_ID + toUserWxId);
            // 推送 标题
            String first = String.format("【会议需求】%s的会议需要%s", sysUserServiceUC.getUserByEmpNo(reservationRecord.getInitiatorEmpNo()).getNameZh(), "{"+finalServiceName+"}");
            //会议主题
            String keyword1 = "会议特殊要求-" + reservationRecord.getRoom().getName();
            //会议室名称
            String keyword2 = "已发送";

            WeChatMsg weChatMsg = createRecordWeChatMsg(reservationRecord, first, keyword1, keyword2 ,ketword4);
            String msgId = sendMsg(url, toUserWxId, weChatMsg);
            log.debug(msgId);
        });

    }

    /**
     * 会议即将开始提醒
     */
    @Override
    public void requirementsComingReminder(ReservationRecordItem reservationRecordItem, Integer timeMinute,String topic) {
        //会议详情url
        String url = detailsUrl + reservationRecordItem.getReservationRecord().getId();
        String toUserWxId = sysUserServiceUC.getUserByEmpNo(reservationRecordItem.getOwnerEmpNo()).getWxOpenId();
        log.info(TO_USER_WECHAT_ID + toUserWxId);
        if (toUserWxId != null) {
            //会议主题
            if(topic.equals("无主题")){
                topic = "";
            }else{
                topic = "-"+topic;
            }
            // 推送 标题
            String first = String.format("%s的会议将在%s分钟后开始%s", sysUserServiceUC.getUserByEmpNo(reservationRecordItem.getInitiatorEmpNo()).getNameZh(), timeMinute,topic);
            //会议主题
            String keyword1 = "会议提醒-" + reservationRecordItem.getReservationRecord().getRoom().getName();
            //会议室名称
            String keyword2 = "已提醒";
            WeChatMsg weChatMsg = createRecordItemWeChatMsg(reservationRecordItem, first, keyword1, keyword2);
            String msgId = sendMsg(url, toUserWxId, weChatMsg);
            log.debug(msgId);
        }
    }

    @Override
    public void requirementsComingReminderEnd(ReservationRecordItem reservationRecordItem, Integer timeMinute, String topic) {
        //会议详情url
        String url = detailsUrl + reservationRecordItem.getReservationRecord().getId();
        String toUserWxId = sysUserServiceUC.getUserByEmpNo(reservationRecordItem.getOwnerEmpNo()).getWxOpenId();
        log.info(TO_USER_WECHAT_ID + toUserWxId);
        if (toUserWxId != null) {
            // 推送 标题
            String first = String.format("你好，你的会议还有%s分钟即将结束，如已结束请忽略",  timeMinute );
            //会议主题
            String keyword1 = "会议结束提醒";
            //会议室名称
            String keyword2 = "会议即将结束";
            // 会议备注
            reservationRecordItem.getReservationRecord().setRemark("会议结束前，请随手关闭电源，若无操系统将会在10分钟后自行设备复位");
            WeChatMsg weChatMsg = createRecordItemWeChatMsg(reservationRecordItem, first, keyword1, keyword2);
            String msgId = sendMsg(url, toUserWxId, weChatMsg);
            log.debug(msgId);
        }
    }

    /**
     * 取消会提提醒
     *
     * @param reservationRecordItem
     */
    @Override
    public void channelRequirementsReminders(ReservationRecordItem reservationRecordItem,String topic) {
        //会议详情url
        String url = detailsUrl + reservationRecordItem.getReservationRecord().getId();
        String toUserWxId = sysUserServiceUC.getUserByEmpNo(reservationRecordItem.getOwnerEmpNo()).getWxOpenId();
        log.info(TO_USER_WECHAT_ID + toUserWxId);
        if (toUserWxId != null) {
            //会议主题
            if(topic.equals("无主题")){
                topic = "";
            }else{
                topic = "-"+topic;
            }
            // 推送 标题
            String first = String.format("%s已取消会议%s", sysUserServiceUC.getUserByEmpNo(reservationRecordItem.getInitiatorEmpNo()).getNameZh(),topic);
            //会议主题
            String keyword1 = "会议取消"+ "-" + reservationRecordItem.getReservationRecord().getRoom().getName();
            //会议室名称
            String keyword2 = "已取消";
            WeChatMsg weChatMsg = createRecordItemWeChatMsg(reservationRecordItem, first, keyword1, keyword2);
            String msgId = sendMsg(url, toUserWxId, weChatMsg);
            log.debug(msgId);
        }
    }

    @Override
    public void refuse(GuestInvitationRecord invitationRecord, ReservationRecord reservationRecord) {
        //会议详情url
        String url = detailsUrl + reservationRecord.getId();
        String toUserWxId = sysUserServiceUC.getUserByEmpNo(reservationRecord.getInitiatorEmpNo()).getWxOpenId();
        log.info(TO_USER_WECHAT_ID + toUserWxId);
        if (toUserWxId != null) {
            String topic = reservationRecord.getTopic();
            // 会议主题
            if(topic.equals("无主题")){
                topic = "";
            }else{
                topic = "-"+topic;
            }
            // 访客手机后四位
            String mobile = invitationRecord.getMobile().substring(invitationRecord.getMobile().length() - 4);
            String first = String.format("您的访客%s-%s拒绝了您的%s邀请，请知晓！",  invitationRecord.getName() ,mobile , topic);
            //会议主题
            String keyword1 = String.format("会议提醒%s" , topic);
            //会议室名称
            String keyword2 = "访客拒绝";
            // 会议备注
            WeChatMsg weChatMsg = createRecordWeChatMsg(reservationRecord, first, keyword1, keyword2 ,"0");
            String msgId = sendMsg(url, toUserWxId, weChatMsg);
            log.debug(msgId);
        }
    }

    @Override
    public void accept(GuestInvitationRecord record, ReservationRecord reservationRecord) {
        //会议详情url
        String url = detailsUrl + reservationRecord.getId();
        String toUserWxId = sysUserServiceUC.getUserByEmpNo(reservationRecord.getInitiatorEmpNo()).getWxOpenId();
        log.info(TO_USER_WECHAT_ID + toUserWxId);
        if (toUserWxId != null) {
            String topic = reservationRecord.getTopic();
            //会议主题
            if(topic.equals("无主题")){
                topic = "";
            }else{
                topic = "-"+topic;
            }
            // 访客手机后四位
            String mobile = record.getMobile().substring(record.getMobile().length() - 4);
            String first = String.format("您的访客%s-%s接收了您的%s邀请，请知晓！",  record.getName() ,mobile , topic);
            //会议主题
            String keyword1 = String.format("会议提醒%s" , topic);
            //会议室名称
            String keyword2 = "访客接受";
            // 会议备注
            WeChatMsg weChatMsg = createRecordWeChatMsg(reservationRecord, first, keyword1, keyword2 ,"0");
            String msgId = sendMsg(url, toUserWxId, weChatMsg);
            log.debug(msgId);
        }
    }

    @Override
    public void refuseAll(ReservationRecord reservationRecord) {
        //会议详情url
        String url = detailsUrl + reservationRecord.getId();
        String toUserWxId = sysUserServiceUC.getUserByEmpNo(reservationRecord.getInitiatorEmpNo()).getWxOpenId();
        log.info(TO_USER_WECHAT_ID + toUserWxId);
        if (toUserWxId != null) {
            String topic = reservationRecord.getTopic();
            //会议主题
            if(topic.equals("无主题")){
                topic = "";
            }else{
                topic = "-"+topic;
            }
            String first = String.format("您的%s会议已自动取消，会议室将释放，请知晓！", topic);
            //会议主题
            String keyword1 = String.format("会议提醒%s" , topic);
            //会议室名称
            String keyword2 = "会议取消";
            // 会议备注
            WeChatMsg weChatMsg = createRecordWeChatMsg(reservationRecord, first, keyword1, keyword2 ,"0");
            String msgId = sendMsg(url, toUserWxId, weChatMsg);
            log.debug(msgId);
        }
    }

    @Override
    public void deviceStatusChangeMsgPush(String openId, WeChatMsg weChatMsg) {
        log.info("发送设备状态变更消息推送");
        weChatSendMsgUtils.sendDeviceStatusChangeMsgPush(templateId, openId, weChatMsg);
    }


    /**
     * 创建微信消息对象
     * @param reservationRecordItem
     * @param first
     * @param keyword1
     * @param keyword2
     * @return
     */
    private WeChatMsg createRecordItemWeChatMsg(ReservationRecordItem reservationRecordItem, String first, String keyword1, String keyword2){

        String timeFrom = DateUtils.formatLocalDateTime(reservationRecordItem.getReservationRecord().getFrom(), DateUtils.PATTERN_DATETIME1);
        String timeTo = DateUtils.formatLocalDateTime(reservationRecordItem.getReservationRecord().getTo(), DateUtils.PATTERN_TIME1);
        String remark = reservationRecordItem.getReservationRecord().getRemark();
        return WeChatMsg.builder().first(first)
                .keyword1(keyword1).keyword2(keyword2).keyword3(timeFrom+"-"+timeTo).remark(remark).build();
    }

    /**
     * 创建微信消息对象
     * @param reservationRecord
     * @param first
     * @param keyword1
     * @param keyword2
     * @return
     */
    private WeChatMsg createRecordWeChatMsg(ReservationRecord reservationRecord, String first, String keyword1, String keyword2 ,String ketword4){

        String timeFrom = DateUtils.formatLocalDateTime(reservationRecord.getFrom(), DateUtils.PATTERN_DATETIME1);
        String timeTo = DateUtils.formatLocalDateTime(reservationRecord.getTo(), DateUtils.PATTERN_TIME1);
        String remark = reservationRecord.getRemark();
        return WeChatMsg.builder().first(first)
                .keyword1(keyword1).keyword2(keyword2).keyword3(timeFrom+"-"+timeTo).ketword4(ketword4).remark(remark).build();
    }
}
