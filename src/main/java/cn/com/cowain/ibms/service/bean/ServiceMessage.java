package cn.com.cowain.ibms.service.bean;

import javax.validation.constraints.NotNull;

/**
 * 业务消息处理类
 *
 * @author Yang.Lee
 * @date 2021/3/10 9:54
 */
public class ServiceMessage {

    public static final String NOT_FOUND_PROJECT = "未找到项目数据";

    public static final String NOT_FOUND_SPACE = "未找到空间数据";

    public static final String NOT_FOUND_DEVICE_CLOUD = "未找到人脸识别设备";

    public static final String NOT_FOUND_STAFF_GROUP = "未找到用户组";

    public static final String NOT_FOUND_TIME_PLAN = "未找到时间计划";

    public static final String NOT_FOUND_ACCESS_RULES = "未找到通行权限";

    public static final String NOT_FOUNT_DOOR_MAGNETISM = "未找到门磁";

    public static final String API_CALLED_ERROR = "API调用失败";

    public static final String NOT_FOUNT_QRCODE_ACCESS_RULES = "未找到二维码通行权限";

    public static final String UNAUTHORIZED = "未授权";

    public static final String NOT_FOUND_MEETING_ROOM = "未找到会议室";

    public static final String NOT_FOUND_MEETING_SCHEDULE = "未找到日程数据";

    public static final String ERR_DEVICE_ID = "设备id有误";

    public static final String ERR_STATUS = "设备不在线,不可控制";

    public static final String NOT_FOUND_DOORPLATE = "未找到门牌";

    public static final String PERMISSION_DENIED = "无权限";

    public static final String MOBILE_FORMAT_ERROR = "手机号格式错误";

    public static final String FILE_FORMAT_ERROR = "文件格式错误";

    public static final String FILE_READ_ERROR = "文件读取异常";

    public static final String NOT_FOUND_DEVICE = "未找到设备数据";

    public static final String NOT_FOUND_DEVICE_GROUP = "未找到设备群组数据";

    public static final String NOT_FOUND_STAFF = "未找到员工信息";

    public static final String NOT_FOUND_STAFF_UC = "在UC系统未找到员工信息";

    public static final String NOT_FOUND_ACCESS_CONTROL_SPOT = "未找到门禁点";

    public static final String NOT_FOUND_MEETING_RESERVATION = "未找到会议记录";

    public static final String NOT_FOUND_OPEN_ABILITY = "未找到开放能力";

    public static final String NOT_FOUND_MEETING_FILE = "未找到会议文档";

    public static final String NOT_FOUND_ACCESS_CONTROL_SPOT_APPLICATION_RECORD = "未找到门禁授权申请";

    /**
     * 获取业务消息,将消息主题和消息后缀通过”：“连接
     *
     * @param message: 消息主体
     * @param append:  消息后缀
     * @return 业务消息
     * @author Yang.Lee
     * @date 2021/3/10 10:22
     **/
    public static String getMessageWithColon(@NotNull final String message, final String append) {

        return new StringBuffer(message)
                .append(":")
                .append(append)
                .toString();
    }
}
