package cn.com.cowain.ibms.service.iot;

import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.enumeration.iot.DoorPlateStatus;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.iot.DoorPlateReq;
import cn.com.cowain.ibms.rest.req.iot.DoorPlateSortReq;
import cn.com.cowain.ibms.rest.resp.iot.DoorOpenResp;
import cn.com.cowain.ibms.rest.resp.iot.doorplate.DoorPlateSearchResp;
import cn.com.cowain.ibms.rest.resp.iot.doorplate.DoorPlateStatusDetailResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;

import java.time.LocalDate;
import java.util.List;

/**
 * @author: cuiEnming
 * @title: DoorPlateService
 * @date 2020/11/24  14:58
 * @since s2
 */
public interface DoorPlateService {

    /**
     * 创建门牌
     *
     * @param doorPlateReq
     * @return
     * @author Yang.Lee
     */
    ServiceResult save(DoorPlateReq doorPlateReq);

    /**
     * 更新门牌
     *
     * @param doorPlaceId  门牌ID
     * @param doorPlateReq 门牌信息
     * @param updatePassword 是否需要更新设备密码
     * @return
     * @author Yang.Lee
     */
    ServiceResult update(String doorPlaceId, DoorPlateReq doorPlateReq, boolean updatePassword);

    /**
     * 查询当前门牌状态
     *
     * @param doorPlateId 门牌ID
     * @return
     * @author Yang.Lee
     */
    ServiceResult status(String doorPlateId);

    /**
     * 模糊查询门牌列表(分页)
     *
     * @param doorPlateName  模糊查询关键字
     * @param page  页码
     * @param size  页长
     * @return
     * @author Yang.Lee
     */
    PageBean<DoorPlateSearchResp> search(String doorPlateName, int page, int size);

    /**
     * 对门牌数据进行排序
     *
     * @param doorPlateSortReq
     * @return
     * @author Yang.Lee
     */
    ServiceResult sort(DoorPlateSortReq doorPlateSortReq);

    /**
     * 删除门牌信息
     * @param id 门牌ID
     * @return
     * @author Yang.Lee
     */
    ServiceResult delete(String id);

    /**
     * 检查设备密码
     * @param id       门牌ID
     * @param password 设备密码
     * @return
     */
    ServiceResult checkDevicePassword(String id, String password);

    /**
     * 查询当前门牌状态
     * @param reservationRecord
     * @param nextRecord
     * @return
     */
    DoorPlateStatus checkCurrentRoomStatus(ReservationRecord reservationRecord, ReservationRecord nextRecord);

    /**
     * 更新所有门牌密码
     *
     */
    ServiceResult updatePassword(String password);

    /**
     * 输入密码开门
     *
     */
    ServiceResult open(DoorOpenResp doorOpenResp);


    /**
     * PC查询当前门牌状态
     *
     * @param doorPlateId 门牌ID
     * @return
     */
    ServiceResult statusPC(String doorPlateId);

    /**
     * 根据门磁ID开门
     *
     */
    ServiceResult openDoor(String id);

    /**
     * 根据设备硬件id查询当前门牌状态
     *
     * @param hardwareId 设备硬件id
     * @return 查询结果
     * @author Yang.Lee
     */
    ServiceResult statusByHardwareId(String hardwareId);

    /**
     * 获取门牌状态列表
     *
     * @param roomId 会议室ID
     * @param date 日期
     * @return 门牌状态列表
     * @author Yang.Lee
     * @date 2021/4/21 16:29
     **/
    List<DoorPlateStatusDetailResp> getStatusDetailList(String roomId, LocalDate date);
}
