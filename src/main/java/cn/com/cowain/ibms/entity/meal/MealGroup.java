package cn.com.cowain.ibms.entity.meal;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 报餐组
 *
 * @author Yang.Lee
 * @date 2022/1/10 15:40
 */
@Data
@Entity
@Table(name = "ibms_meal_group")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_meal_group", comment = "IBMS 报餐组")
@Where(clause = "is_del=0")
public class MealGroup extends BaseEntity {

    /**
     * 关联时间计划名
     */
    @ManyToOne
    @JoinColumn(name = "time_plan_id", nullable = false, columnDefinition = "varchar(36) COMMENT '关联时间计划ID'")
    private KsHtTimePlan ksHtTimePlan;


    /**
     * 组名
     */
    @Column(name = "name", nullable = false, columnDefinition = "varchar(500)  COMMENT '组名'")
    private String name;
}
