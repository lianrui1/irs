package cn.com.cowain.ibms.enumeration.ability;

/**
 * @author Yang.Lee
 * @date 2022/1/27 17:44
 */
public enum TaskType {

    MQ,

    API;
}
