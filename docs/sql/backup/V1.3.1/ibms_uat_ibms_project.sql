-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 10.0.1.232    Database: ibms_uat
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ibms_project`
--

DROP TABLE IF EXISTS `ibms_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ibms_project` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `created_by` varchar(36) DEFAULT NULL COMMENT '创建人',
  `created_time` datetime(6) DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(6) DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime(6) DEFAULT NULL COMMENT '更新时间',
  `version` int(11) DEFAULT NULL COMMENT '版本号',
  `address` varchar(36) NOT NULL COMMENT '项目详细地址',
  `city_id` varchar(36) NOT NULL COMMENT '市级编号',
  `city_name` varchar(36) NOT NULL COMMENT '市级名称',
  `district_id` varchar(36) NOT NULL COMMENT '区级编号',
  `district_name` varchar(36) NOT NULL COMMENT '区级名称',
  `project_no` varchar(50) NOT NULL COMMENT '项目编号',
  `project_name` varchar(36) NOT NULL COMMENT '项目名称',
  `province_id` varchar(36) NOT NULL COMMENT '省级编号',
  `province_name` varchar(36) NOT NULL COMMENT '省级名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='项目记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ibms_project`
--

LOCK TABLES `ibms_project` WRITE;
/*!40000 ALTER TABLE `ibms_project` DISABLE KEYS */;
INSERT INTO `ibms_project` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `address`, `city_id`, `city_name`, `district_id`, `district_name`, `project_no`, `project_name`, `province_id`, `province_name`) VALUES ('0509ceb4-ae32-4703-a6dd-6f56168d8387',NULL,'2021-01-12 17:46:02.692810',0,NULL,'2021-01-12 17:46:02.692810',0,'恒盛路1299号','849','苏州市','857','昆山市','00008362','测试项目','810','江苏省');
INSERT INTO `ibms_project` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `address`, `city_id`, `city_name`, `district_id`, `district_name`, `project_no`, `project_name`, `province_id`, `province_name`) VALUES ('123',NULL,'2020-12-30 18:10:03.000000',0,NULL,'2020-12-30 18:10:06.000000',1,' ',' ',' ',' ',' ','00000000','DEFAULT PROJECT',' ',' ');
INSERT INTO `ibms_project` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `address`, `city_id`, `city_name`, `district_id`, `district_name`, `project_no`, `project_name`, `province_id`, `province_name`) VALUES ('1d017fe8-cdb8-4e01-b12b-4a83df700647',NULL,'2021-01-12 18:08:51.242514',1,NULL,'2021-01-12 18:08:51.242514',1,'测','2','北京市','11','通州区','00001264','项目002','1','北京市');
INSERT INTO `ibms_project` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `address`, `city_id`, `city_name`, `district_id`, `district_name`, `project_no`, `project_name`, `province_id`, `province_name`) VALUES ('1ef2f184-d3c7-4657-bbca-3fcfa7facfd1',NULL,'2021-01-12 18:00:32.183189',0,NULL,'2021-01-12 18:00:32.183189',0,'期威锋网额范围俄文无分文未未123好测试','1362','青岛市','1366','崂山区','00001263','测试项目0119','1350','山东省');
INSERT INTO `ibms_project` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `address`, `city_id`, `city_name`, `district_id`, `district_name`, `project_no`, `project_name`, `province_id`, `province_name`) VALUES ('658eeaf7-614a-4b00-ba48-95b837c5b13a',NULL,'2021-01-12 17:54:52.251609',1,NULL,'2021-01-12 17:54:52.251609',1,'老城镇','1032','芜湖市','1035','鸠江区','00000763','测试组件','1021','安徽省');
INSERT INTO `ibms_project` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `address`, `city_id`, `city_name`, `district_id`, `district_name`, `project_no`, `project_name`, `province_id`, `province_name`) VALUES ('7e0e3902-129e-4b78-a624-0f3f56efb7b3',NULL,'2021-01-12 17:37:28.304924',0,NULL,'2021-01-12 17:53:23.452370',3,'测试得分是大多数郭德纲吗','1158','厦门市','1161','湖里区','0000061','测试项目011800023','1143','福建省');
INSERT INTO `ibms_project` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `address`, `city_id`, `city_name`, `district_id`, `district_name`, `project_no`, `project_name`, `province_id`, `province_name`) VALUES ('a7aa659b-0d55-4733-9263-f2cd7b56e45a',NULL,'2021-01-14 16:56:20.518510',0,NULL,'2021-01-14 17:53:12.968648',6,'123123','1032','芜湖市','1035','鸠江区','00000764','12312312','1021','安徽省');
INSERT INTO `ibms_project` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `address`, `city_id`, `city_name`, `district_id`, `district_name`, `project_no`, `project_name`, `province_id`, `province_name`) VALUES ('c9f3171e-529f-4ec6-b85e-49b47c401eec',NULL,'2021-01-12 17:36:17.455487',1,NULL,'2021-01-12 17:36:44.317397',3,'测试1234测试','2216','三沙市','2219','中沙群岛的岛礁及其海域','0000231','汇金大厦项目0118001','2205','海南省');
/*!40000 ALTER TABLE `ibms_project` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-25 15:53:42
