package cn.com.cowain.ibms.rest.resp.distinguish;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/21 9:45
 */
@Data
@ApiModel("海康人员分页数据")
public class HkUserPageResp {

    @ApiModelProperty(value = "序号")
    private Integer seq;

    @ApiModelProperty(value = "人员ID", position = 1)
    private String personId;

    @ApiModelProperty(value = "人员姓名", position = 2)
    private String name;

    @ApiModelProperty(value = "人员工号", position = 3)
    private String jobNum;

    @ApiModelProperty(value = "部门", position = 4)
    private String dept;

    @ApiModelProperty(value = "人员照片", position = 5)
    private String photoUrl;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "通行生效时间", position = 6)
    private LocalDateTime startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "通行结束时间", position = 7)
    private LocalDateTime endTime;

    @ApiModelProperty(value = "设备ID", position = 9)
    private String deviceId;

}
