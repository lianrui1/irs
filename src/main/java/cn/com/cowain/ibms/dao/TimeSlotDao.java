package cn.com.cowain.ibms.dao;

import cn.com.cowain.ibms.bo.SimpleTimeSlot;
import cn.com.cowain.ibms.entity.TimeSlot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * Room Dao
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/3 19:36
 */
@Repository
public interface TimeSlotDao extends JpaRepository<TimeSlot, String> {

    /**
     * 根据room_id 删除
     *
     * @param id
     * @return
     */
    void deleteByRoomId(String id);

    /**
     * 根据room_id 查找
     *
     * @param id
     * @return
     */
    Set<TimeSlot> findByRoomId(String id);

    /**
     * 根据 room_id 查找,按 seq 排序
     *
     * @param id
     * @return
     */
    List<TimeSlot> findByRoomIdOrderBySeqAsc(String id);

    /**
     * 根据 room_id 查找,按 seq 倒叙排序
     *
     * @author: yanzy
     * @data: 2022/4/25 17:04:01
     */
    List<TimeSlot> findByRoomIdOrderBySeqDesc(String id);

    /**
     * 批量查询会议室的TimeSlot数据
     *
     * @param roomIds
     * @return
     */
    @Query(
            value = "select new cn.com.cowain.ibms.bo.SimpleTimeSlot(t.room.id, t.from, t.to) from TimeSlot t where t.room.id in (:roomIds) and t.isDelete = 0 order by t.seq asc"
    )
    List<SimpleTimeSlot> findSimpleTimeSlotByRoomIds(@Param("roomIds") List<String> roomIds);
}
