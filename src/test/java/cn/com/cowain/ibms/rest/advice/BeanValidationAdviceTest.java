package cn.com.cowain.ibms.rest.advice;

import com.alibaba.fastjson.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/21/20
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BeanValidationAdviceTest {

    @LocalServerPort
    private int port;

    @Autowired
    private RestTemplate restTemplate;

    private String getPath() {
        return "http://localhost:" + port + API_BASE;
    }

    @Test
    void validationFailed() {
    }

    @Test
    void numberFormat() {
        // header
        String url = getPath() + "/room/sort";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        // json
        String s = "{\"data\":[{\"id\":\"id123\",\"seq\":1231231231231231231231312312}]}";
        HttpEntity<Object> httpEntity = new HttpEntity<>(s, headers);
        Assertions.assertThrows(HttpClientErrorException.class,
                () -> restTemplate.exchange(url, HttpMethod.PUT, httpEntity, JSONObject.class));
    }

    @Test
    void fileUpload() {
    }

}
