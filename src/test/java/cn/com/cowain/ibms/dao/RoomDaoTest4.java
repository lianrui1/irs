package cn.com.cowain.ibms.dao;

import cn.com.cowain.ibms.entity.ReservationRecord;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/3 19:37
 */
@SpringBootTest
public class RoomDaoTest4 {

    @Autowired
    private ReservationRecordDao reservationRecordDao;

    @Test
    public void test0() {
        Assertions.assertNotNull(reservationRecordDao);
    }


    @Test
    public void test1() {
        LocalDate now = LocalDate.now();
        Set<ReservationRecord> reservationRecord = reservationRecordDao.findByDateGreaterThanEqualAndRoomId(now, "0bc3a7b2-e2a2-4c36-baee-f058b9269222");
        System.out.println(reservationRecord);
        Integer reservationRecordSize = reservationRecord.size();
        if (reservationRecordSize != 0) {
            List<ReservationRecord> reservationRecordList = new ArrayList<>();
            reservationRecord.forEach(it -> {
                it.setIsDelete(1);
                reservationRecordList.add(it);
            });
            reservationRecordDao.saveAll(reservationRecordList);
        }
    }

}
