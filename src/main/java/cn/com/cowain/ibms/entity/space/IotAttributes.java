package cn.com.cowain.ibms.entity.space;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * IOT 数据点
 */
@Data
@Entity
@Table(name = "iot_attributes")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_attributes", comment = "产品关联属性")
public class IotAttributes extends BaseEntity {

    /**
     * 属性名称
     */
    @Column(name = "name", nullable = false, columnDefinition = "varchar(20) COMMENT '属性名称'")
    private String name;

    /**
     * 数据点id
     */
    @Column(name = "dict_id", nullable = false, columnDefinition = "varchar(36) COMMENT ''")
    private String dictId;


    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK'")
    private IotProduct iotProduct;

}
