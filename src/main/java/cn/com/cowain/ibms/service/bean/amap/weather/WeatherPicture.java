package cn.com.cowain.ibms.service.bean.amap.weather;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Yang.Lee
 * @date 2021/4/30 14:18
 */
@Data
public class WeatherPicture implements Serializable {

    /**
     * 天气现象
     **/
    private String weather;

    /**
     * 背景图Url
     **/
    private String backgroundImg;

    /**
     * 背景图 2x Url
     **/
    private String backgroundImg2X;

    /**
     * 白色 icon Url
     **/
    private String iconWhite;

    /**
     * 白色 icon 2X Url
     **/
    private String iconWhite2X;
}
