package cn.com.cowain.ibms.dao.device;

import cn.com.cowain.ibms.entity.space.IotProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/2/26 16:01
 */
@Repository
public interface HwProductsDao extends JpaRepository<IotProduct, String> {
}
