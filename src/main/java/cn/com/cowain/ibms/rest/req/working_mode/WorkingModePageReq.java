package cn.com.cowain.ibms.rest.req.working_mode;

import cn.com.cowain.ibms.enumeration.iot.UsableStatus;
import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/12/6 16:43
 */
@Data
@ApiModel("会议室场景列表请求对象")
public class WorkingModePageReq extends PageReq {

    @ApiModelProperty(value = "会议室ID", required = true)
    private String officeId;

    @ApiModelProperty(value = "是否启用 DISABLED禁用 ENABLE启用")
    private UsableStatus status;

    @ApiModelProperty(value = "排序方式 0按控制时间排序 1按创建时间和模式状态排序", required = true)
    private int seq;
}
