package cn.com.cowain.ibms.service.bean.amap.geo;

import cn.com.cowain.ibms.service.bean.amap.AMapResp;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * 逆地理解析响应
 * @author Yang.Lee
 * @date 2021/4/20 14:34
 */
@Data
public class ReGeo extends AMapResp {

    @JSONField(name = "regeocodes")
    private List<ReGeoCode> regeoCodeList;

    @JSONField(name = "regeocode")
    private ReGeoCode regeoCode;
}
