package cn.com.cowain.ibms.rest.resp.working_mode;

import cn.com.cowain.ibms.enumeration.iot.UsableStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/10/18 14:42
 */
@Data
@ApiModel("工作模式场景返回列表")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WorkingModePageResp {

    @ApiModelProperty(value = "场景名称id", required = true)
    private String id;

    @ApiModelProperty(value = "场景名称图标", required = true)
    private String imgUrl;

    @ApiModelProperty(value = "场景名称", required = true)
    private String name;

    @ApiModelProperty(value = "用途")
    private String purpose;

    @ApiModelProperty(value = "是否启用 DISABLED禁用 ENABLE启用")
    private UsableStatus status;

    @ApiModelProperty(value = "是否自定义场景 0系统 1自定义", required = true)
    private Integer isDefault;
}
