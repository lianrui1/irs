package cn.com.cowain.ibms.rest.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 授权 Bean
 *
 * @author Hu Jingling
 */
@Data
@ApiModel
public class AuthReq {

    @NotBlank
    @ApiModelProperty(value = "code", required = true, position = 0)
    private String code;

}
