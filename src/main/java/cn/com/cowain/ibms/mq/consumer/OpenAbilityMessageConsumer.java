package cn.com.cowain.ibms.mq.consumer;

import cn.com.cowain.ibms.mq.Topic;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * 开放能力mq consumer
 *
 * @author Yang.Lee
 * @date 2022/1/26 15:46
 */
@Slf4j
@RocketMQMessageListener(
        topic = "${rocketmq.env}-" + Topic.OPEN_ABILITY,
        consumerGroup = "${rocketmq.group}-" + "openAbility"
)
@Component
public class OpenAbilityMessageConsumer implements RocketMQListener<String> {

//    @Resource
//    private AbilityService abilityService;

    @Override
    public void onMessage(String message) {
        log.debug("接收到open ability消息， {}", message);

//        try {
//            OpenAbilityMessage abilityMessage = JSON.parseObject(message, OpenAbilityMessage.class);
//            abilityService.process(abilityMessage);
//        } catch (Exception e) {
//            log.error(e.getMessage(), e);
//        }
        log.debug("消息处理结束");

    }
}
