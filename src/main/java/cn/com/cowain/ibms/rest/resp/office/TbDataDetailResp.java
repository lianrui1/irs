package cn.com.cowain.ibms.rest.resp.office;

import cn.com.cowain.ibms.rest.resp.device.TbDataResp;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * @author feng
 * @title: AirConditionerStatusDetailResp
 * @projectName ibms
 * @Date 2021/12/21 14:07
 */
@Data
public class TbDataDetailResp extends TbDataResp {
    String id;
    String hwDeviceId;
    String hwStatus;
    Boolean allDisable=Boolean.FALSE;
    String deviceName;

    public void putValue(TbDataResp resp){
        if(StringUtils.isNotEmpty(resp.getPm2_5())){
            this.setPm2_5(resp.getPm2_5());
        }
        if(StringUtils.isNotEmpty(resp.getLuminance())){
            setLuminance(resp.getLuminance());
        }
        if(StringUtils.isNotEmpty(resp.getNoise())){
            setNoise(resp.getNoise());
        }
        if(StringUtils.isNotEmpty(resp.getTemperature())){
            setTemperature(resp.getTemperature());
        }
        if(StringUtils.isNotEmpty(resp.getHumidity())){
            setHumidity(resp.getHumidity());
        }
        if(StringUtils.isNotEmpty(resp.getPm10())){
            setPm10(resp.getPm10());
        }
        if(StringUtils.isNotEmpty(resp.getIsPeopleThere()))
        {
            setIsPeopleThere(resp.getIsPeopleThere());
        }
    }
    private String luminance;
    // 温度
    private String temperature;
    // 湿度
    private String humidity;
    // 噪音
    private String noise;
    // 空气质量10
    private String pm10;
    // 空气质量pm2_5
    private String pm2_5;
    // 人体传感器
    private String isPeopleThere;

    public void putNullValue(){
        this.setIsPeopleThere(StringUtils.isEmpty(getIsPeopleThere())?"":getIsPeopleThere());
        this.setPm2_5(StringUtils.isEmpty(getPm2_5())?"":getPm2_5());
        this.setPm10(StringUtils.isEmpty(getPm10())?"":getPm10());
        this.setNoise(StringUtils.isEmpty(getNoise())?"":getNoise());
        this.setHumidity(StringUtils.isEmpty(getHumidity())?"":getHumidity());
        this.setTemperature(StringUtils.isEmpty(getTemperature())?"":getTemperature());
        this.setLuminance(StringUtils.isEmpty(getLuminance())?"":getLuminance());
    }

}
