package cn.com.cowain.ibms.rest;

import cn.com.cowain.ibms.rest.resp.UploadResp;
import cn.com.cowain.ibms.service.UploadService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.ibms.utils.PicUtils;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/4/22 11:00
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/upload")
@Api(tags = IConst.UPLOAD_FILE)
public class UploadController {

    @Autowired
    private UploadService uploadService;

    @PostMapping("/file")
    @ApiOperation(value = "文件上传", tags = {IConst.MODULE_OTA_UPGRADE, IConst.V25})
    public ResponseEntity<JsonResult<UploadResp>> upload(MultipartFile multipartFile) {
        log.debug("文件上传: " + multipartFile);
        ServiceResult result = uploadService.upload(multipartFile);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.ok("ok", (UploadResp) result.getObject()));
    }

    @PostMapping("/uploadAndCheckFace")
    @ApiOperation(value = "上传人脸照片并校验", tags = {IConst.UPLOAD_FILE, IConst.V26})
    public ResponseEntity<JsonResult<UploadResp>> uploadAndCheckFace(MultipartFile file) {
        try {
            if (file.getBytes().length > 200 * 1024) {
                InputStream inputStream = PicUtils.getPhotoFitHIK(file);
                if (Objects.isNull(inputStream)) {
                    return ResponseEntity.ok(JsonResult.error("文件不能为空", null, ErrConst.E01));
                }
                file = new MockMultipartFile(file.getName(), file.getOriginalFilename(), file.getContentType(), IOUtils.toByteArray(inputStream));
            }
        } catch (IOException e) {
            return ResponseEntity.ok(JsonResult.error("文件处理失败", null, ErrConst.E01));
        }
        ServiceResult result = uploadService.uploadAndCheckFace(file);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, result.getErrorCode()));
        }
        return ResponseEntity.ok(JsonResult.ok("ok", (UploadResp) result.getObject()));
    }
}
