package cn.com.cowain.ibms.rest.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/4 21:15
 */
@Data
@ApiModel("EHR 员工数据")
public class EhrStaffResp implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String stId;

    public String getSysId() {
        this.sysId=stId;
        return this.sysId;
    }

    @ApiModelProperty(value = "系统ID", required = true, position = 0)
    private String sysId;

    @ApiModelProperty(value = "中文名", required = true, position = 1)
    private String nameZh;

    @ApiModelProperty(value = "英文名", required = true, position = 2)
    private String nameEn;

    @ApiModelProperty(value = "工号", required = true, position = 3)
    private String hrId;

    @ApiModelProperty(value = "部门名", required = true, position = 4)
    private String stDeptName;

    @ApiModelProperty(value = "微信openId", required = true, position = 5)
    private String wxId;

    @ApiModelProperty(value = "微信头像", required = true, position = 6)
    private String headImgUrl;

    /**
     * H5前端专用(后端不用)
     * todo:后面要优化,去除
     */
    @ApiModelProperty(value = "H5前端专用(后端不用)", required = true, position = 6)
    private boolean show = false;

}
