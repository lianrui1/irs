package cn.com.cowain.ibms.service.workingmode;

import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.working_mode.WorkingModeSaveReq;
import cn.com.cowain.ibms.rest.req.working_mode.WorkingModeUpdateReq;
import cn.com.cowain.ibms.rest.resp.working_mode.ManagementPageResp;
import cn.com.cowain.ibms.rest.resp.working_mode.WorkingModePageResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface WorkingModeService {

    // 添加工作模式场景
    ServiceResult create(String hrId, WorkingModeSaveReq req);

    // 查询工作模式列表分页
    PageBean<WorkingModePageResp> getPage(String spaceId, Pageable pageable);

    // 删除工作模式
    ServiceResult delete(String id, String hrId);

    // 查询工作模式详情
    ServiceResult detail(String id);

    // 添加人员空间内设备控制权限
    ServiceResult managementCreate(String id, List<String> empNos, String token, String isApplet);

    // 查询权限人员列表
    PageBean<ManagementPageResp> managementPage(String id, int page, int size);

    // 删除人员权限
    ServiceResult managementDelete(String id, List<String> empNos, String token);

    // 人员是否拥有入口权限
    ServiceResult rule(String spaceId, String hrId);

    // 编辑场景模式
    ServiceResult update(String hrId, String id, WorkingModeUpdateReq req);

    //PC删除权限人员
    ServiceResult managementDeletePc(String spaceId, List<String> empNos);
}
