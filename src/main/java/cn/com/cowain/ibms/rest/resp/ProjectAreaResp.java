package cn.com.cowain.ibms.rest.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/8 9:50
 */
@Data
@ApiModel("项目区域响应对象")
public class ProjectAreaResp {


    @ApiModelProperty(value = "区域ID", required = true)
    private String areaId;

    @ApiModelProperty(value = "区域名称", required = true, position = 1)
    private String areaName;
}
