package cn.com.cowain.ibms.entity.centralized;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author wei.cheng
 * @date 2022/09/15 16:51
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "ibms_centralized_code_device_relation")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "ibms_centralized_code_device_relation", comment = "集控码关联设备，仅用于控制范围为部分的集控码")
public class CentralizedCodeDeviceRelation extends BaseEntity {

    @Column(name = "centralized_control_code_id", nullable = false, columnDefinition = "varchar(36) COMMENT '集控码id'")
    private String centralizedControlCodeId;

    @Column(name = "device_id", nullable = false, columnDefinition = "varchar(36) COMMENT '设备id'")
    private String deviceId;

    public CentralizedCodeDeviceRelation(String centralizedControlCodeId, String deviceId, String createdBy, String updatedBy) {
        this.centralizedControlCodeId = centralizedControlCodeId;
        this.deviceId = deviceId;
        this.setCreatedBy(createdBy);
        this.setUpdatedBy(updatedBy);
    }
}
