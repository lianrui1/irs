package cn.com.cowain.ibms.rest;

import cn.com.cowain.ibms.annotation.Idempotent;
import cn.com.cowain.ibms.dao.RoomDao;
import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.entity.Room;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.enumeration.ReservationRecordStatus;
import cn.com.cowain.ibms.exceptions.UnAuthorizedException;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.bean.ReservationRecordBean;
import cn.com.cowain.ibms.rest.req.*;
import cn.com.cowain.ibms.rest.req.meeting.AttendeeReq;
import cn.com.cowain.ibms.rest.req.meeting.RecommendMeetingReq;
import cn.com.cowain.ibms.rest.req.meeting.ReservationPageReq;
import cn.com.cowain.ibms.rest.resp.*;
import cn.com.cowain.ibms.rest.resp.audio.AudioDetailResp;
import cn.com.cowain.ibms.rest.resp.meeting.MeetingDateResp;
import cn.com.cowain.ibms.rest.resp.meeting.OngoingMeetingResp;
import cn.com.cowain.ibms.rest.resp.meeting.ReservationPageResp;
import cn.com.cowain.ibms.service.ReservationRecordService;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.utils.DateUtil;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.ibms.utils.TokenUtils;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.sfp.comm.utils.JwtUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * ReservationRecord Controller
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/4 17:52
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/reservation")
@Api(tags = IConst.MODULE_RESERVATION)
public class ReservationRecordController {

    private ReservationRecordService reservationRecordService;

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserServiceUC;

    @Autowired
    private RoomDao roomDao;

    public ReservationRecordController(ReservationRecordService reservationRecordService) {
        this.reservationRecordService = reservationRecordService;
    }

    /**
     * 新增会议室预约记录
     *
     * @param req
     * @param bindingResult
     * @return
     * @author 胡荆陵
     */
    @PostMapping
    @ApiOperation(value = "新增会议室预约记录", tags = {IConst.MODULE_RESERVATION, IConst.V28})
    @Idempotent(value = API_BASE + "/reservation/post", expireTime = 3L)
    public ResponseEntity<JsonResult> save(@RequestHeader(name = "token") String token, @RequestBody @Validated ReservationAddReq req,
                                           BindingResult bindingResult) {
        log.debug("save req:" + req);
        // 2021-01-06: 根据新需求，会议室预约时可选择当前时间半小时前的时段，因此取消参数校验中的@Future注解，在代码中进行时间校验
        LocalDateTime formTime = req.getFrom();
        LocalDateTime nowTime = LocalDateTime.now();
        if (!DateUtil.isTimeAvailable(formTime, nowTime)) {
            return ResponseEntity.ok(JsonResult.error("会议开始时间不能早于当前时间半小时", null, ErrConst.E01));
        }

        //校验预订时间 date
        LocalDate date = req.getDate();
        LocalDate today = LocalDate.now();
        if (date.isBefore(today)) {
            //返回错误信息
            return ResponseEntity.ok(JsonResult.error("不能选择过去的日期", date, ErrConst.E01));
        }
        //检查该会议室,在相同时间段是否存在预约记录
        ServiceResult serviceResult = reservationRecordService.checkReservation(req);
        if (!serviceResult.isSuccess()) {
            //返回错误信息
            return ResponseEntity.ok(JsonResult.error("该会议室已存在预约记录", serviceResult.getObject(), ErrConst.E12));
        }
        //校验参会人是否大于会议室人数容量
        List<SysUserBean> participantList = req.getParticipantList();
        if (Objects.nonNull(participantList)) {
            Optional<Room> byId = roomDao.findById(req.getRoomId());
            if (byId.isPresent()) {
                Room room = byId.get();
                if (room.getCapacity() < participantList.size()) {
                    //返回错误信息
                    return ResponseEntity.ok(JsonResult.error("参会人超出本会议室人数容量", participantList, ErrConst.E01));
                }
            }
        }
        //保存
        log.debug("新增会议室预约记录即将调用reservationRecordService.save方法");
        ReservationRecord rr = reservationRecordService.save(req, token);
        IdResp idResp = new IdResp();
        idResp.setId(rr.getId());
        JsonResult<IdResp> jsonResult = JsonResult.ok("数据保存成功", idResp);
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 更新会议室预约记录
     *
     * @param req
     * @param bindingResult
     * @return
     * @author 张宇鑫
     */
    @PutMapping("/{id}")
    @ApiOperation(value = "更新会议室预约记录", tags = {IConst.MODULE_RESERVATION, IConst.V28})
    public ResponseEntity<JsonResult<String>> update(@PathVariable String id, @RequestHeader(name = "token") String token, @RequestBody @Validated ReservationUpdateReq req,
                                                     BindingResult bindingResult) {
        log.debug("update req:" + req);
        ServiceResult serviceResult = reservationRecordService.update(id, req, token);
        JsonResult<String> jsonResult = JsonResult.okNoData("OK");
        if (!serviceResult.isSuccess()) {
            jsonResult = JsonResult.error(serviceResult.getObject().toString(), null, ErrConst.E17);
        }
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 按指定日期分页查询会议室和对应预约记录
     *
     * @param page
     * @param size
     * @return
     * @author Hu Jingling
     */
    @GetMapping
    @ApiOperation(value = "按指定日期分页查询会议室和对应预约记录", tags = IConst.MODULE_RESERVATION)
    public ResponseEntity<JsonResult<PageBean<RoomWithReservationResp>>> findPage(
            @RequestHeader(name = "Authorization", required = false) String token,
            @ApiParam(name = "projectId", value = "项目ID") @RequestParam(required = false, defaultValue = "") String projectId,
            @ApiParam(name = "date", value = "日期", required = true, example = "2000-12-31") @RequestParam String date,
            @ApiParam(name = "page", value = "页码", defaultValue = "0") @RequestParam(value = "page", required = false, defaultValue = "0") int page,
            @ApiParam(name = "size", value = "每页记录数", defaultValue = "10") @RequestParam(value = "size", required = false, defaultValue = "10") int size) {

        if (StringUtils.isEmpty(token)) {
            throw new UnAuthorizedException("未授权");
        }

        // 获取用户工号
        String empNo = JwtUtil.getHrId(token);
        //初始化pageBean
        PageBean<RoomWithReservationResp> pageBean = new PageBean<>(page, size);
        //字符串转date
        LocalDate localDate = DateUtil.convert(date);
        pageBean = reservationRecordService.findByHrId(empNo, projectId, localDate, pageBean);
        JsonResult<PageBean<RoomWithReservationResp>> jsonResult = JsonResult.ok("OK", pageBean);
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 指定日期,指定会议室,查询预约记录
     *
     * @param roomId 指定会议室
     * @param date   指定日期
     * @return
     * @author Hu Jingling
     */
    @GetMapping("/byRoomAndDate")
    @ApiOperation(value = "指定日期,指定会议室,查询预约记录", tags = {IConst.MODULE_RESERVATION, IConst.V25})
    public ResponseEntity<JsonResult<RoomWithReservationResp>> findByRoom(
            @ApiParam(name = "userId", value = "用户工号") @RequestParam(required = false, defaultValue = "") String empNo,
            @ApiParam(name = "roomId", value = "会议室ID", required = true) @RequestParam String roomId,
            @ApiParam(name = "date", value = "日期", required = true, example = "2000-12-31") @RequestParam String date) {
        log.debug("findByRoom()...");
        log.debug("roomId:" + roomId);
        log.debug("date:" + date);
        //字符串转date
        LocalDate localDate = DateUtil.convert(date);
        RoomWithReservationResp roomWithReservationResp = reservationRecordService.findBySysUserAndRoomId(empNo, roomId, localDate);
        JsonResult<RoomWithReservationResp> jsonResult = JsonResult.ok("OK", roomWithReservationResp);
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 获取用户发起的会议列表
     *
     * @param reservationGetReq: 请求参数
     * @param result:            参数校验结果
     * @return 用户发起的会议列表
     * @author Yang.Lee
     * @date 2021/3/3 17:00
     **/
    @GetMapping("/search/list")
    @ApiOperation(value = "用户获取会议列表", tags = {IConst.MODULE_RESERVATION, IConst.V25})
    public ResponseEntity<JsonResult<PageBean<ReservationRecordBean>>> search(@Validated ReservationGetReq reservationGetReq, BindingResult result) {

        PageBean<ReservationRecordBean> pageBean = reservationRecordService.getReservationRecordItemPage(reservationGetReq);
        JsonResult<PageBean<ReservationRecordBean>> jsonResult = JsonResult.ok("OK", pageBean);

        return ResponseEntity.ok(jsonResult);
    }

    @GetMapping("/search/userAndRoomRecordList")
    @ApiOperation(value = "用户获取会议列表", tags = IConst.MODULE_RESERVATION)
    public ResponseEntity<JsonResult<UserAndRoomRecordResp>> userAndRoomRecordList(@RequestParam(value = "empNo", required = false) @ApiParam(name = "empNo", value = "员工工号") String empNo,
                                                                                   @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                                                                                   @RequestParam(value = "size", required = false, defaultValue = "10") int size,
                                                                                   @RequestParam(value = "days", required = false, defaultValue = "7") int days,
                                                                                   @ApiParam(name = "projectId", value = "项目ID") @RequestParam(required = false, defaultValue = "") String projectId,
                                                                                   @ApiParam(name = "date", value = "日期", required = true, example = "2000-12-31") @RequestParam String date) {
        // 用户会议列表
        ReservationGetReq reservationGetReq = new ReservationGetReq();
        reservationGetReq.setEmpNo(empNo);
        reservationGetReq.setDays(days);
        reservationGetReq.setPage(page);
        reservationGetReq.setSize(size);
        reservationGetReq.setProjectId(projectId);
        PageBean<ReservationRecordBean> pageBean = reservationRecordService.getReservationRecordItemPage(reservationGetReq);

        PageBean<RoomWithReservationResp> pageBean2 = new PageBean<>(page, size);
        //字符串转date
        LocalDate localDate = DateUtil.convert(date);
        pageBean2 = reservationRecordService.findByHrId(empNo, projectId, localDate, pageBean2);

        UserAndRoomRecordResp resp = new UserAndRoomRecordResp();
        resp.setReservationRecordItemPage(pageBean);
        resp.setUserRecordItemPage(pageBean2);

        return ResponseEntity.ok(JsonResult.ok("", resp));
    }


    /**
     * 根据人员id 会议id获取 会议室详情页面
     *
     * @param recordId 会议室id
     * @return
     */
    @GetMapping("/search/details/administrative")
    @ApiOperation(value = "行政人员查看会议详情", tags = IConst.MODULE_RESERVATION)
    public ResponseEntity<JsonResult<ReservationRecordDetailsResp>> searchDetailsAdministrative(@RequestParam(value = "recordId", required = false) @ApiParam(name = "recordId", value = "会议id") String recordId) {
        //字符串转date
        ReservationRecordDetailsResp reservationRecordDetailsResp = reservationRecordService.getAdministrativeReservationRecordDetails(recordId);
        JsonResult<ReservationRecordDetailsResp> jsonResult = JsonResult.ok("OK", reservationRecordDetailsResp);
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 根据人员id 会议id获取 会议室详情页面
     *
     * @param ownerEmpNo 人员工号
     * @param recordId   会议室id
     * @return
     */
    @GetMapping("/search/details/")
    @ApiOperation(value = "会议详情", tags = {IConst.MODULE_RESERVATION, IConst.V25})
    public ResponseEntity<JsonResult<ReservationRecordDetailsResp>> searchDetails(@RequestParam(value = "ownerEmpNo", required = false)
                                                                                  @ApiParam(name = "ownerEmpNo", value = "员工工号")
                                                                                          String ownerEmpNo,
                                                                                  @RequestParam(value = "recordId", required = false)
                                                                                  @ApiParam(name = "recordId", value = "会议id")
                                                                                          String recordId
            , @RequestHeader(name = "token", required = false) String token) {
        ServiceResult result = reservationRecordService.getReservationRecordDetails(ownerEmpNo, recordId, token);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, result.getErrorCode()));
        }
        ReservationRecordDetailsResp reservationRecordDetailsResp = (ReservationRecordDetailsResp) result.getObject();
        // 判断该会议室是否被删除
        Optional<Room> roomOptional = roomDao.findById(reservationRecordDetailsResp.getRoomId());
        if (!roomOptional.isPresent()) {
            reservationRecordDetailsResp.setRemove(1);
        } else {
            reservationRecordDetailsResp.setRemove(0);
        }
        JsonResult<ReservationRecordDetailsResp> jsonResult = JsonResult.ok("OK", reservationRecordDetailsResp);
        return ResponseEntity.ok(jsonResult);
    }


    /**
     * 取消会议预约记录(发起人操作)
     *
     * @param id
     * @return
     * @author 张宇鑫
     */
    @PutMapping("/cancel/{id}")
    @ApiOperation(value = "取消会议预约记录(发起人操作)", tags = IConst.MODULE_RESERVATION)
    public ResponseEntity<JsonResult<String>> cancel(@PathVariable String id, @RequestHeader(name = "token") String token) {
        log.debug("cancel id:" + id);
        ServiceResult serviceResult = reservationRecordService.cancel(id, token);
        JsonResult<String> jsonResult = JsonResult.ok("OK", "取消成功");
        if (!serviceResult.isSuccess()) {
            jsonResult = JsonResult.error(serviceResult.getObject().toString(), null, ErrConst.E16);
        }
        return ResponseEntity.ok(jsonResult);
    }

    @PutMapping("/finish/{id}")
    @ApiOperation(value = "结束会议预约记录(发起人操作)", tags = {IConst.MODULE_RESERVATION, IConst.V25})
    public ResponseEntity<JsonResult<String>> finish(@PathVariable String id) {
        log.debug("cancel id:" + id);

        // 获取工号
        String hrId = JwtUtil.getHrId(TokenUtils.getToken());

        ServiceResult serviceResult = reservationRecordService.finish(id, hrId);
        JsonResult<String> jsonResult = JsonResult.okNoData("OK");
        if (!serviceResult.isSuccess()) {
            jsonResult = JsonResult.error(serviceResult.getObject().toString(), null, ErrConst.E16);
        }
        return ResponseEntity.ok(jsonResult);
    }


    /**
     * 会议被邀请人接受、拒绝邀请
     *
     * @param id
     * @return
     * @author 张宇鑫
     */
    @PutMapping("/invitee/{id}")
    @ApiOperation(value = "会议被邀请人接受、拒绝邀请", tags = IConst.MODULE_RESERVATION)
    public ResponseEntity<JsonResult<String>> updateInviteeStatus(@PathVariable String id, @RequestBody @Validated InviteeStatusReq inviteeStatusReq, BindingResult bindingResult) {
        log.debug("updateInviteeStatus id:" + id);
        log.debug("updateInviteeStatus id:" + inviteeStatusReq);
        reservationRecordService.updateInviteeStatus(id, inviteeStatusReq);
        JsonResult<String> jsonResult = JsonResult.okNoData("OK");
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 查询会议冲突
     *
     * @param reservationConflictReq
     * @return
     */
    @ApiOperation("查询会议冲突")
    @PostMapping("/conflict/")
    public ResponseEntity<JsonResult<List<ReservationConflictResp>>> conflict(@RequestBody ReservationConflictReq reservationConflictReq) {
        log.debug("conflict, reservationConflictReq:" + reservationConflictReq);
        List<ReservationConflictResp> reservationConflictRespList = reservationRecordService.conflictDetect(reservationConflictReq);
        JsonResult<List<ReservationConflictResp>> jsonResult = JsonResult.ok("OK", reservationConflictRespList);
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 根据用户工号查询最近联系人
     *
     * @param empNo 用户工号
     * @return 最近联系人信息
     */
    @GetMapping("/user/{empNo}/recentList")
    @ApiOperation(value = "根据用户工号查询最近联系人", tags = IConst.MODULE_RESERVATION)
    public ResponseEntity<JsonResult<List<EhrStaffResp>>> recentList(@PathVariable @ApiParam(value = "员工工号", required = true) String empNo) {

        List<String> participantList = reservationRecordService.findRecentListByUserEmpNo(empNo);
        List<EhrStaffResp> ehrStaffList = new ArrayList<>();
        for (String e : participantList) {
            SysUser sysUser = sysUserServiceUC.getUserByEmpNo(e);
            if (sysUser != null && StringUtils.isNotBlank(sysUser.getEmpNo())) {
                EhrStaffResp staff = new EhrStaffResp();
                staff.setNameZh(sysUser.getNameZh());
                staff.setStId(sysUser.getSysId());
                Map<String, String> deptMap = reservationRecordService.getUserDepartmentName(sysUser.getEmpNo().split(","));
                staff.setStDeptName(deptMap.get(sysUser.getEmpNo()));
                staff.setHrId(sysUser.getEmpNo());
                staff.setWxId(sysUser.getWxOpenId());
                staff.setHeadImgUrl(sysUser.getHeadImgUrl());
                ehrStaffList.add(staff);
            }
        }
        JsonResult<List<EhrStaffResp>> jsonResult = JsonResult.ok("OK", ehrStaffList);
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 根据 ID 删除会议预约
     *
     * @param id 部门ID
     * @return
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "根据 ID 删除会议预约", tags = IConst.MODULE_RESERVATION)
    public ResponseEntity<JsonResult<Object>> delete(@PathVariable String id, @RequestHeader("Authorization") String token) {
        log.debug("delete, id:" + id);


        ServiceResult serviceResult = reservationRecordService.deleteById(id, JwtUtil.getHrId(token));
        JsonResult<Object> jsonResult = JsonResult.ok("OK", serviceResult.getObject());
        if (!serviceResult.isSuccess()) {
            jsonResult = JsonResult.error(String.valueOf(serviceResult.getObject()), null, ErrConst.E01);
        }

        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 会议室id获取最合适的可预约段
     *
     * @param roomId
     * @return
     */
    @GetMapping("/room/next-valid-time")
    @ApiOperation(value = "会议室id获取最合适的可预约段", tags = IConst.MODULE_RESERVATION)
    public ResponseEntity<JsonResult<AppointmentsTimeCanBeMdeResp>> nextValidTime(@ApiParam(value = "会议室ID", required = true) String roomId, @ApiParam(value = "日期", required = true) String date) {
        log.debug("nextValidTime,  roomId:" + roomId);
        AppointmentsTimeCanBeMdeResp appointmentsTimeCanBeMdeResp = reservationRecordService.nextValidTime(roomId, date);
        JsonResult<AppointmentsTimeCanBeMdeResp> jsonResult = JsonResult.ok("OK", appointmentsTimeCanBeMdeResp);
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 会议室门磁查询
     *
     * @param roomId 会议室ID
     * @return
     * @author Yang.Lee
     */
    @GetMapping("/doorMagnetic/{roomId}")
    @ApiOperation(value = "会议室门磁查询", tags = IConst.MODULE_ROOM)
    public ResponseEntity<JsonResult<RoomDoorMagneticResp>> doorMagneticSearch(@PathVariable @ApiParam(value = "会议室ID", required = true) String roomId) {

        JsonResult<RoomDoorMagneticResp> jsonResult = null;
        // 查询是否拥有门磁
        ServiceResult result = reservationRecordService.ifRoomHasDoorMagnetic(roomId);
        if (!result.isSuccess()) {
            jsonResult = JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01);

            return ResponseEntity.ok(jsonResult);
        }
        jsonResult = JsonResult.ok("OK", (RoomDoorMagneticResp) result.getObject());

        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 获取用户正在进行的会议列表
     *
     * @param token 身份令牌
     * @return 会议列表
     * @author Yang.Lee
     * @date 2021/6/8 18:51
     **/
    @GetMapping("/ongoing/list")
    @ApiOperation(value = "正在进行会议查询", tags = IConst.MODULE_RESERVATION)
    public ResponseEntity<JsonResult<List<OngoingMeetingResp>>> getUserOngoingMeeting(@RequestHeader("Authorization") String token) {

        return ResponseEntity.ok(JsonResult.ok(reservationRecordService.getOngoingMeetingList(JwtUtil.getHrId(token))));
    }

    /**
     * 查询会议分页数据
     *
     * @param req 请求参数
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/7/15 17:31
     **/
    @GetMapping("/page")
    @ApiOperation(value = "PC会议分页数据", tags = "${project.sprint}")
    public ResponseEntity<JsonResult<PageBean<ReservationPageResp>>> getReservationPage(ReservationPageReq req) {

        req.addOrderDesc("date");
        req.addOrderDesc("from");
        return ResponseEntity.ok(JsonResult.ok(reservationRecordService.getReservationPage(req)));
    }

    /**
     * 获取设备类型列表
     *
     * @return 设别类型列表
     * @author Yang.Lee
     * @date 2021/3/30 10:47
     **/
    @GetMapping("/status/list")
    @ApiOperation(value = "获取会议状态列表", tags = "${project.sprint}")
    public ResponseEntity<JsonResult<List<BaseListResp>>> getReservationStatusList() {

        List<BaseListResp> statusList = new ArrayList<>();
        for (ReservationRecordStatus status : ReservationRecordStatus.values()) {
            statusList.add(new BaseListResp(status.toString(), status.getName()));
        }

        return ResponseEntity.ok(JsonResult.ok(statusList));
    }

    @GetMapping("/audio/list/{id}")
    @ApiOperation(value = "获取会议记录音频列表", tags = {IConst.MODULE_RESERVATION, IConst.V25})
    public ResponseEntity<JsonResult<List<AudioDetailResp>>> getReservationAudioList(@PathVariable("id") @ApiParam(value = "会议记录ID", required = true) String id) {

        return ResponseEntity.ok(JsonResult.ok(reservationRecordService.getReservationAudioList(id)));
    }

    @PostMapping("/upload/url")
    @ApiOperation(value = "上传会议记录链接", tags = {IConst.MODULE_RESERVATION, IConst.V28})
    public ResponseEntity<JsonResult<String>> uploadUrl(@RequestBody @Validated ReservationUploadUrlReq reservationUploadUrlReq, BindingResult bindingResult) {

        ServiceResult serviceResult = reservationRecordService.uploadUrl(reservationUploadUrlReq);
        JsonResult<String> jsonResult = JsonResult.okNoData("OK");
        if (!serviceResult.isSuccess()) {
            jsonResult = JsonResult.error(serviceResult.getObject().toString(), null, ErrConst.E17);
        }
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 快速预约会议(推荐会议)
     *
     * @author: yanzy
     * @data: 2022/4/18 17:04:21
     */
    @GetMapping("/recommend/meeting")
    @ApiOperation(value = "获取推荐会议室",tags = {IConst.MODULE_QUICK_APPOINTMENT_MEETING,IConst.V29})
    public ResponseEntity<JsonResult<PageBean<RoomWithReservationResp>>> getRecommendMeeting(@ApiParam(name = "page", value = "页码", defaultValue = "0") @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                                                                                             @ApiParam(name = "size", value = "每页记录数", defaultValue = "1") @RequestParam(value = "size", required = false, defaultValue = "1") int size,
                                                                                             RecommendMeetingReq req){
        //初始化pageBean
        PageBean<RoomWithReservationResp> pageBean = new PageBean<>(page, size);
        PageBean recommendMeeting = reservationRecordService.getRecommendMeeting(pageBean, req);
        JsonResult<PageBean<RoomWithReservationResp>> jsonResult = JsonResult.ok("OK", recommendMeeting);
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 查询预定时间最近的时间空闲会议室
     *
     * @author: yanzy
     * @data: 2022/4/19 10:04:39
     */
    @GetMapping("/recommend/recent/time")
    @ApiOperation(value = "查询预定时间最近的时间空闲会议室",tags = {IConst.MODULE_QUICK_APPOINTMENT_MEETING,IConst.V29})
    public ResponseEntity<JsonResult<MeetingDateResp>> getRecentTime(RecommendMeetingReq req){

        JsonResult<MeetingDateResp> jsonResult;
        ServiceResult result = reservationRecordService.getRecentTime(req);
        if (!result.isSuccess()){
            jsonResult = JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01);
        }else {
            jsonResult = JsonResult.ok("OK",(MeetingDateResp) result.getObject());
        }
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 查询会议室最晚截止时间
     *
     * @author: yanzy
     * @data: 2022/4/19 14:04:46
     */
    @GetMapping("/recommend/meeting/deadline")
    @ApiOperation(value = "查询最晚会议室截止时间",tags = {IConst.MODULE_QUICK_APPOINTMENT_MEETING,IConst.V29})
    public ResponseEntity<JsonResult<String>> getMeetingDeadline(){
        ServiceResult meetingDeadline = reservationRecordService.getMeetingDeadline();
        return ResponseEntity.ok(JsonResult.ok("OK",(String) meetingDeadline.getObject()));
    }

    /***
     * 邀请参会人
     *
     * @author: yanzy
     * @data: 2022/4/19 13:04:06
     */
    @PutMapping("/recommend/invite")
    @ApiOperation(value = "邀请参会人",tags = {IConst.MODULE_QUICK_APPOINTMENT_MEETING,IConst.V29})
    public ResponseEntity<JsonResult<String>> inviteParticipants(@RequestHeader(name = "token") String token,@RequestBody @Validated AttendeeReq req){
        ServiceResult serviceResult = reservationRecordService.inviteParticipants(req, token);
        JsonResult<String> jsonResult = JsonResult.ok("OK", (String) serviceResult.getObject());
        if (!serviceResult.isSuccess()){
            jsonResult = JsonResult.error(serviceResult.getObject().toString(), null, ErrConst.E01);
        }
        return ResponseEntity.ok(jsonResult);
    }

    /***
     * 查询访客类型
     *
     * @author: yanzy
     * @data: 2022/4/19 14:04:17
     */
    @GetMapping("/recommend/visitor/type")
    @ApiOperation(value = "查询访客类型",tags = {IConst.MODULE_QUICK_APPOINTMENT_MEETING,IConst.V29})
    public ResponseEntity<JsonResult<List<VisitorTypeResp>>> getVisitorType(){
        List<VisitorTypeResp> respList = reservationRecordService.getVisitorType();
        return ResponseEntity.ok(JsonResult.ok(respList));
    }

    /**
     * 查询访客系统对应地址
     *
     * @author: yanzy
     * @data: 2022/4/19 14:04:49
     */
    @GetMapping("/recommend/visitor/{projectId}")
    @ApiOperation(value = "查询访客系统对应地址",tags = {IConst.MODULE_QUICK_APPOINTMENT_MEETING,IConst.V29})
    public ResponseEntity<JsonResult<String>> getVisitorAddress(@PathVariable String projectId){
        ServiceResult visitorAddress = reservationRecordService.getVisitorAddress(projectId);
        return ResponseEntity.ok(JsonResult.ok("OK",(String) visitorAddress.getObject()));
    }

}
