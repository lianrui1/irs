package cn.com.cowain.ibms.rest.req.working_mode;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author feng
 * @title: ChoiceModel 更换模式请求参数
 * @projectName ibms
 * @Date 2021/12/28 13:48
 */
@Data
public class ChoiceModelReq {
    @ApiModelProperty(value = "大屏设备Id")
    private String intelligentMeetingAppId;
    @ApiModelProperty(value = "模式Id")
    private String modelId;
}
