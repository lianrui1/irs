package cn.com.cowain.ibms.rest.req.working_mode;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author feng
 * @title: ModeChangeReq
 * @projectName ibms
 * @Date 2021/12/30 17:39
 */
@Data
@ApiModel("变更模式")
public class ModeChangeReq {
    @ApiModelProperty(value = "当前设备id")
    private String appId;
    @ApiModelProperty(value = "模式Id")
    private String modeId;
}
