package cn.com.cowain.ibms.feign.iotc.enumeration;

/**
 * @author Yang.Lee
 * @date 2022/2/14 19:18
 */
public enum MethodCode {

    /**
     * 创建鸿图员工组
     **/
    CREATE_GROUP("createGroup"),

    /**
     * 删除鸿图员工组
     **/
    REMOVE_GROUP("deleteMemberGroup"),

    /**
     * 旷视鸿图设备开门
     **/
    OPEN_KSHT_DOOR("openKHSTDoor"),

    /**
     * 从人员组中移除用户
     **/
    REMOVE_USER_FROM_GROUP("removeUserFromGroup"),

    /***
     *
     * ksht批量添加用户组人员
     */
    BATCH_ADD_PERSON("batchAddPerson"),

    /**
     * 查询设备通行记录
     **/
    GET_REPLENISH_RECORD("getReplenishRecord"),

    /***
     * 获取遥测数据
     */
    POST_TELEMETRY("postTelemetry"),

    /***
     * 规则链删除
     */
    DELETE_RULECHAIN("deleteRuleChain"),

    /***
     * 规则链新增
     */
    CREATE_RULECHAIN("createRuleChain"),

    /***
     * 规则链修改
     */
    UPDATE_RULECHAIN("updateRuleChain"),
    /**
     * 查询人脸照片
     **/
    FIND_PERSON_PIC("findPersonPic"),

    /**
     * 配置报餐权限
     **/
    CONFIG_PERMISSION("configPermission"),

    /**
     * 查看Hk Ks 平台是否存在
     */
    GET_HK_KS_USER("getHkKsUser");


    private String name;

    MethodCode(String name) {
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
}
