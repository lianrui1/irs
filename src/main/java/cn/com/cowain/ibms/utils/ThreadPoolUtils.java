package cn.com.cowain.ibms.utils;


import com.google.common.util.concurrent.ThreadFactoryBuilder;

import java.util.concurrent.*;

public class ThreadPoolUtils {
    //添加数据到Redis的线程池工厂
    private static final ThreadFactory THREAD_FACTORY_REDIS = new ThreadFactoryBuilder().setNameFormat("redis-pool-%d").build();

    //添加Redis的线程池
    public static final ExecutorService EXECUTOR_SERVICE_REDIS = new ThreadPoolExecutor(8, 9, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingDeque<>(1024), THREAD_FACTORY_REDIS, new ThreadPoolExecutor.CallerRunsPolicy());

    //抛出异常的线程池工厂
    private static final ThreadFactory THREAD_FACTORY_EXCEPTION = new ThreadFactoryBuilder().setNameFormat("error-pool-%d").build();

    //添加Redis的线程池
    public static final ExecutorService EXECUTOR_SERVICE_EXCEPTION = new ThreadPoolExecutor(4, 5, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingDeque<>(1024), THREAD_FACTORY_EXCEPTION, new ThreadPoolExecutor.CallerRunsPolicy());

    //查询集控码设备统计线程
    private static final ThreadFactory THREAD_FACTORY_GET_CENTRALIZED_CONTROL_DEVICE_STATS = new ThreadFactoryBuilder().setNameFormat("get-centralized-control-device-stats-pool-%d").build();

    //查询集控码设备统计线程池
    public static final ExecutorService EXECUTOR_SERVICE_GET_CENTRALIZED_CONTROL_DEVICE_STATS = new ThreadPoolExecutor(4, 16, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingDeque<>(1024), THREAD_FACTORY_GET_CENTRALIZED_CONTROL_DEVICE_STATS, new ThreadPoolExecutor.CallerRunsPolicy());
}
