package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.AccessControlTimePlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author wei.cheng
 * @date 2022/09/28 14:01
 */
@Repository
public interface AccessControlTimePlanDao extends JpaRepository<AccessControlTimePlan, String>, JpaSpecificationExecutor<AccessControlTimePlan> {
    /**
     * 查询用户在门禁点绑定的时间计划
     *
     * @param spotId
     * @param userHrId
     * @return
     */
    AccessControlTimePlan findFirstByUserHrIdAndSpotIdOrderByCreatedTimeDesc(String userHrId, String spotId);

    /**
     * 查询用户在门禁点绑定的时间计划
     *
     * @param spotId
     * @param userHrId
     * @return
     */
    List<AccessControlTimePlan> findByUserHrIdAndSpotId(String userHrId, String spotId);
}
