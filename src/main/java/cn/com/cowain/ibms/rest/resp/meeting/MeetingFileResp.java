package cn.com.cowain.ibms.rest.resp.meeting;

import cn.com.cowain.ibms.rest.resp.StaffResp;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author wei.cheng
 * @date 2022/02/22 17:16
 */
@Data
@ApiModel("会议室文档详情")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MeetingFileResp implements Serializable {

    @ApiModelProperty(value = "主键ID", required = true, position = 0)
    private String id;

    /**
     * 会议预约记录ID，外键
     */
    @ApiModelProperty(value = "会议id", required = true, position = 1)
    private String reservationRecordId;

    @ApiModelProperty(value = "文件URL", required = true, position = 2, example = "http://tests.cowain.cn/group1/M00/00/0C/rBIQA2IUxcSACgUHAAIqBPKjJ5A851.pdf")
    private String url;

    @ApiModelProperty(value = "源文件文件名", required = true, position = 3, example = "测试文件.pdf")
    private String originalFileName;

    @ApiModelProperty(value = "文件大小，单位为B", required = true, position = 4)
    private Long size;

    @ApiModelProperty(value = "文件名，不包括扩展名", required = true, position = 5, example = "测试文件")
    private String fileName;

    @ApiModelProperty(value = "文件扩展名", required = true, position = 6, example = "pdf")
    private String fileExt;

    @ApiModelProperty(value = "WPS预览文件URL", required = true, position = 7,
            example = "https://wwo.wps.cn/office/w/471eba5030?_w_fname=测试文件.pdf&_w_userid=33&_w_appid=d8f99da")
    private String wpsPreviewUrl;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间", required = true, position = 8)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "会议文档创建者", required = true, position = 10)
    private StaffResp creator;
}
