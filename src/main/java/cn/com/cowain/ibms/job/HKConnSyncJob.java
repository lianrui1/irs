package cn.com.cowain.ibms.job;

import cn.com.cowain.ibms.dao.distinguish.HkDeviceDao;
import cn.com.cowain.ibms.dao.iot.AccessControlSpotDao;
import cn.com.cowain.ibms.dao.iot.AccessControlSpotUserDao;
import cn.com.cowain.ibms.entity.iot.AccessControlSpot;
import cn.com.cowain.ibms.entity.iot.AccessControlSpotUser;
import cn.com.cowain.ibms.entity.iot.HkDevice;
import cn.com.cowain.ibms.enumeration.HkAddress;
import cn.com.cowain.ibms.enumeration.iot.AccessControlSpotLevel;
import cn.com.cowain.ibms.enumeration.iot.AccessControlSpotType;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.resp.distinguish.HkUserPageResp;
import cn.com.cowain.ibms.utils.RestTemplateUtil;
import cn.com.cowain.sfp.comm.JsonResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Yang.Lee
 * @date 2021/12/21 16:40
 */
@Slf4j
@Component
public class HKConnSyncJob {

    @Resource
    private HkDeviceDao hkDeviceDao;

    @Resource
    private AccessControlSpotDao accessControlSpotDao;

    @Resource
    private AccessControlSpotUserDao accessControlSpotUserDao;

    @Resource
    private RestTemplateUtil restTemplateUtil;
    /**
     * 向面板机同步权限数据，每天执行一次
     * @return
     * @author Yang.Lee
     * @date 2021/12/21 16:42
     **/
//    @Scheduled(cron = "0 0 1 * * ? ")
    public void sync(){
        log.info("开始面板机同步");
        List<HkDevice> hkDeviceList = hkDeviceDao.findAll();
        List<HkDevice> ksList = hkDeviceList.stream().filter(obj -> HkAddress.KS.equals(obj.getAddress())).collect(Collectors.toList());
        List<HkDevice> ntList = hkDeviceList.stream().filter(obj -> HkAddress.NT.equals(obj.getAddress())).collect(Collectors.toList());

        log.info("size all {}, size ks {}, size nt {}", hkDeviceList.size(), ksList.size(), ntList.size());

        sync(ksList);
        sync(ntList);
    }

    private void sync(List<HkDevice> deviceList){
        deviceList.forEach(obj -> {

            // 查询这个设备对应的门禁点是什么
            Optional<AccessControlSpot> accessControlSpotOptional = accessControlSpotDao.findByDeviceId(obj.getDevice().getId());
            AccessControlSpot accessControlSpot;
            if(accessControlSpotOptional.isEmpty()){
                accessControlSpot = new AccessControlSpot();
                accessControlSpot.setSpace(obj.getDevice().getSpace());
                accessControlSpot.setName(obj.getDevice().getDeviceName());
                accessControlSpot.setAddress("");
                accessControlSpot.setDevice(obj.getDevice());
                accessControlSpot.setAccessControlSpotType(AccessControlSpotType.FACE_MACHINE);
                accessControlSpot.setAccessControlSpotLevel(AccessControlSpotLevel.NORMAL);

                accessControlSpotDao.save(accessControlSpot);
            } else{
                accessControlSpot = accessControlSpotOptional.get();
            }

            String url = "http://sfp-api.cowain.cn/ibms/api/v1/access/hk/user/page?deviceId="+obj.getDevice().getId()+"&name=";

            List<HkUserPageResp> dataList = getHkData(url);

            if(CollectionUtils.isEmpty(dataList)){
                log.error(" empty hk conn list, id {}" , obj.getId());
                return ;
            }

            // list转map
            Map<String, HkUserPageResp> newDataMap = dataList.stream().collect(Collectors.toMap(HkUserPageResp::getJobNum, e -> e, (oldData, newData) -> newData));

            // 找出需要添加和删除的数据，不做修改操作
            List<AccessControlSpotUser> accessControlSpotUserList = accessControlSpotUserDao.findByAccessControlSpotId(accessControlSpot.getId());
            Set<String> dbUserHrIdSet = accessControlSpotUserList.stream().map(AccessControlSpotUser::getUserHrId).collect(Collectors.toSet());
            Set<String> reqUserHrIdSet = newDataMap.keySet();

            Set<String> add = new HashSet<>(reqUserHrIdSet);
            add.removeAll(dbUserHrIdSet);

            List<AccessControlSpotUser> addList = new ArrayList<>();
            add.forEach(e -> {

                HkUserPageResp hkUserPageResp = newDataMap.get(e);
                AccessControlSpotUser accessControlSpotUser = new AccessControlSpotUser();
                accessControlSpotUser.setAccessControlSpot(accessControlSpot);
                accessControlSpotUser.setUserHrId(hkUserPageResp.getJobNum());
                accessControlSpotUser.setUserName(hkUserPageResp.getName());
                accessControlSpotUser.setAccessStartTime(hkUserPageResp.getStartTime() == null ? LocalDateTime.now() : hkUserPageResp.getStartTime());
                accessControlSpotUser.setAccessEndTime(hkUserPageResp.getEndTime() == null ? LocalDateTime.now() : hkUserPageResp.getEndTime() );

                addList.add(accessControlSpotUser);

            });
            accessControlSpotUserDao.saveAll(addList);

            Set<String> del = new HashSet<>(dbUserHrIdSet);
            del.removeAll(reqUserHrIdSet);

            List<AccessControlSpotUser> delList = new ArrayList<>();
            del.forEach(e ->
                accessControlSpotUserList.forEach(o -> {
                    if(o.getUserHrId().equals(e)){
                        delList.add(o);
                        return ;
                    }
                })
            );

            delList.forEach(e -> e.setIsDelete(1));
            accessControlSpotUserDao.saveAll(delList);
        });
    }

    private List<HkUserPageResp> getHkData(String url){

        int limit = 1000;
        int page = 0;
        long totalData;
        List<HkUserPageResp> totalList = new ArrayList<>();

        do {

            String finalUrl = new StringBuffer(url)
                    .append("&page=")
                    .append(page)
                    .append("&size=")
                    .append(limit)
                    .toString();
            String result;
            try{
                result = restTemplateUtil.get(finalUrl);
            } catch (Exception e){
                log.error("error get list url {}" , finalUrl);
                return new ArrayList<>();
            }

            JsonResult<PageBean<HkUserPageResp>> pageJson = JSON.parseObject(result, new TypeReference<>(JsonResult.class) {});
            totalData = pageJson.getData().getTotalElements();

            List<HkUserPageResp> dataList = pageJson.getData().getList();


            if(!CollectionUtils.isEmpty(dataList)){
                totalList.addAll(dataList);
            }
            page++;
        }
        while (totalData > totalList.size());

        return totalList;
    }
}
