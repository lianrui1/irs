package cn.com.cowain.ibms.rest.resp;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author feng
 * @title: NodeSit 监控点列表
 * @projectName bims
 * @Date 2021/11/18 11:01
 */
@Data
public class LocationNodeResp {

    @ApiModelProperty("id")
    String id;
    /** 当前节点名称 */
    @ApiModelProperty("当前节点名称")
    String name;
    /** 当前节点深度 */
    @ApiModelProperty("当前节点深度")
    Integer currentNode;
    /**监控名称列表 */
    @ApiModelProperty("子节点 列表")
    List<MonitorNodeResp> monitorLocationRespList;
}
