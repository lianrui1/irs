package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author Yang.Lee
 * @date 2021/10/21 9:32
 */
@Data
@Entity
@Table(name = "iot_device_group")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_device_group", comment = "设备群组表")
public class DeviceGroup extends BaseEntity {

    @Column(name = "name", nullable = false, columnDefinition = "varchar(15) COMMENT '群组名称'")
    private String name;

    @Column(name = "description", nullable = false, columnDefinition = "varchar(100) COMMENT '群组描述'")
    private String description;

    @ManyToOne
    @JoinColumn(name = "parent_id", columnDefinition = "varchar(36) COMMENT 'FK 父节点id'")
    private DeviceGroup parentGroup;

    @Column(name = "level", nullable = false, columnDefinition = "tinyint(2) COMMENT '群组层级'")
    private int level;

    @Column(name = "number", nullable = false, columnDefinition = "varchar(20) COMMENT '群组编号'")
    private String number;
}
