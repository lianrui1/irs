package cn.com.cowain.ibms.rest.resp.meeting;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/8 14:04
 */
@Data
public class FaceResp {

    private String filePath;

    private String message;

    private String picIsOk;
}
