package cn.com.cowain.ibms.enumeration.integration_control;

/**
 * @author wei.cheng
 * @date 2022/09/15 16:46
 */
public enum ControlDeviceRange {

    /**
     * 所有
     */
    ALL("所有"),

    /**
     * 部分
     **/
    PART("部分");

    String name;

    ControlDeviceRange(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
