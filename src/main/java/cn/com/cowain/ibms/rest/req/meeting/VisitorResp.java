package cn.com.cowain.ibms.rest.req.meeting;

import cn.com.cowain.ibms.entity.meeting.EhrUserAuth;
import cn.com.cowain.ibms.entity.meeting.EhrVisitor;
import lombok.Data;

import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/17 15:33
 */
@Data
public class VisitorResp {

    // 邀请函信息
    private EhrVisitor recordInfoDto;

    private List<EhrUserAuth> userAuthList;

}
