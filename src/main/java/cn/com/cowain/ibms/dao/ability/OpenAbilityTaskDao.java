package cn.com.cowain.ibms.dao.ability;

import cn.com.cowain.ibms.entity.ability.OpenAbilityTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author Yang.Lee
 * @date 2022/1/27 17:29
 */
public interface OpenAbilityTaskDao extends JpaRepository<OpenAbilityTask, String>, JpaSpecificationExecutor<OpenAbilityTask> {
}
