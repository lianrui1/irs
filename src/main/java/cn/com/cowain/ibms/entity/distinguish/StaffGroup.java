package cn.com.cowain.ibms.entity.distinguish;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 人员组对象
 * @author Yang.Lee
 * @date 2021/3/4 19:40
 */
@Data
@Entity
@Table(name = "ibms_distinguish_staff_group")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_distinguish_staff_group", comment = "人脸识别人员组表")
@Where(clause = "is_del=0")
public class StaffGroup extends BaseEntity {

    @Column(name = "name", nullable = false, columnDefinition = "varchar(256)  COMMENT '人员组名称'")
    private String name;

    @Column(name = "count", nullable = false, columnDefinition = "integer(10)  COMMENT '组内人数'")
    private int count;

    @Column(name = "seq", nullable = false, columnDefinition = "integer COMMENT '排序'")
    private int seq;
}
