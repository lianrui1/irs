package cn.com.cowain.ibms.rest.resp;

import cn.com.cowain.ibms.entity.SysUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 员工信息数据响应对象
 *
 * @author Yang.Lee
 * @date 2021/2/8 9:24
 */
@Data
@ApiModel("员工数据")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StaffResp implements Serializable {

    private static final long serialVersionUID = 6194601131372181185L;

    private String stId;

    public String getSysId() {
        this.sysId = stId;
        return this.sysId;
    }

    @ApiModelProperty(value = "系统ID", required = true)
    private String sysId;

    @ApiModelProperty(value = "中文名", required = true, position = 1)
    private String nameZh;

    @ApiModelProperty(value = "英文名", required = true, position = 2)
    private String nameEn;

    @ApiModelProperty(value = "工号", required = true, position = 3)
    private String hrId;

    @ApiModelProperty(value = "部门名", required = true, position = 4)
    private String stDeptName;

    @ApiModelProperty(value = "微信openId", required = true, position = 5)
    private String wxId;

    @ApiModelProperty(value = "微信头像", required = true, position = 6)
    private String headImgUrl;

    @ApiModelProperty(value = "部门名称", required = true, position = 7)
    private String departName;

    /**
     * 对象类型转换
     *
     * @param source 原类型
     * @return 转换后类型
     * @author Yang.Lee
     * @date 2021/11/15 15:41
     **/
    public static StaffResp convert(SysUser source) {

        return StaffResp.builder()
                .stId(source.getSysId())
                .sysId(source.getSysId())
                .nameZh(source.getNameZh())
                .nameEn(source.getNameEn())
                .hrId(source.getEmpNo())
                .stDeptName(source.getFullDepartmentName())
                .wxId(source.getWxOpenId())
                .headImgUrl(source.getHeadImgUrl())
                .build();
    }
}
