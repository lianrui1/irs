package cn.com.cowain.ibms.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/4/8 17:27
 */
@Data
@Entity
@Table(name = "ibms_user_room_used_time")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_user_room_used_time", comment = "用户会议室使用时长记录表")
@Where(clause = "is_del=0")
public class RoomUsedTime extends BaseEntity{

    /**
     * 使用时长
     */
    @Column(name = "used_time", nullable = false, columnDefinition = "int(11)  COMMENT '使用时长, 单位分钟'")
    private long usedTime;

    /**
     * 员工工号
     */
    @Column(name = "user_emp_no", nullable = false, columnDefinition = "varchar(20)  COMMENT '员工工号'")
    private String empNo;

    @ManyToOne
    @JoinColumn(name = "room_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK'")
    private Room room;
}
