package cn.com.cowain.ibms.rest.req.office;

import cn.com.cowain.ibms.enumeration.RuleEngIneDetailTriggerCondition;
import cn.com.cowain.ibms.enumeration.iot.UsableStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.util.List;

/**
 * 规则引擎详情请求参数
 *
 * @author: yanzy
 * @date: 2022/1/27 18:11
 */
@Data
@ApiModel("创建规则引擎详情请求对象")
public class RuleEngineDetailReq {

    @ApiModelProperty(value = "办公室id",required = true)
    private String officeId;

    @ApiModelProperty(value = "联动规则小图片url",required = true)
    private String imgUrl;

    @Length(max = 20,message = "名称最长为20")
    @ApiModelProperty(value = "联动规则名称",required = true)
    private String name;

    @Length(max = 20,message = "简述最长为20")
    @ApiModelProperty(value = "联动规则简述")
    private String purpose;

    @ApiModelProperty(value = "条件关系",required = true)
    private RuleEngIneDetailTriggerCondition triggerCondition;

    @ApiModelProperty(value = "触发条件",required = true)
    private List<RuleEngIneDetailTriggerReq> triggerReqs;

    @ApiModelProperty(value = "执行任务",required = true)
    private List<RuleEngIneDetailActuatorReq> actuatorReqs;

    @ApiModelProperty(value = "是否启用",required = true)
    private UsableStatus usableStatus;

    @ApiModelProperty(value = "生效方式",required = true)
    private RuleEngineDetailEffectiveTypeReq effectiveTypeReq;
}
