package cn.com.cowain.ibms.rest.resp.space;

import cn.com.cowain.ibms.rest.resp.iot.IotDeviceResp;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 *  @title
 *  @Description 描述
 *  @author jf.sui
 *  @Date
 */
@Data
@ApiModel("空间详情（包含设备）")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SpaceAllDetailShowRoomResp  implements Serializable {



    @ApiModelProperty(value = "空间详情", position = 1)
    private ShowRoomSpaceDetailResp detail;

    @ApiModelProperty(value = "设备列表", position = 8)
    private List<IotDeviceResp> deviceList;

}
