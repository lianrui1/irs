package cn.com.cowain.ibms.rest.req.working_mode;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author feng
 * @title: ChoiceModel 更换模式请求参数
 * @projectName ibms
 * @Date 2021/12/28 13:48
 */
@Data
public class WelcomeWordReq {

    @NotBlank
    @ApiModelProperty(value = "会议记录Id", position = 1)
    private String recordId;

    @Length(max = 100)
    @ApiModelProperty(value = "欢迎语设置,最大100", required = true, position = 2)
    private String welcomeWords;
}
