package cn.com.cowain.ibms.rest.req;

import lombok.Data;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/3 21:16
 */
@Data
public class RoomAddReq {

    private String id;

    private String name;

    /**
     * 人数容量
     */
    private int capacity;

    /**
     * 楼栋
     */
    private String building;

    /**
     * 楼层
     */
    private String floor;

    /**
     * 是否开放
     * 0:否
     * 1:是
     */
    private int isOpen;

    /**
     * 包含设备
     */
    private String devices;

}
