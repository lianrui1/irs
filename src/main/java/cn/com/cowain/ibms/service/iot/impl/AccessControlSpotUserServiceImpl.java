package cn.com.cowain.ibms.service.iot.impl;

import cn.com.cowain.ibms.dao.iot.AccessControlSpotDao;
import cn.com.cowain.ibms.dao.iot.AccessControlSpotKSHTMemberGroupDao;
import cn.com.cowain.ibms.dao.iot.AccessControlSpotUserDao;
import cn.com.cowain.ibms.entity.iot.AccessControlSpot;
import cn.com.cowain.ibms.entity.iot.AccessControlSpotKSHTMemberGroup;
import cn.com.cowain.ibms.entity.iot.AccessControlSpotUser;
import cn.com.cowain.ibms.enumeration.iot.KSHTMemberGroupType;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.iot.AccessControlSpotUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author tql
 * @Description 门禁点-员工 实现类
 * @Date 22-5-20 下午2:09
 * @Version 1.0
 */
@Service
@Slf4j
public class AccessControlSpotUserServiceImpl implements AccessControlSpotUserService {

    @Resource
    private AccessControlSpotUserDao accessControlSpotUserDao;

    @Resource
    private AccessControlSpotDao accessControlSpotDao;


    @Resource
    private AccessControlSpotKSHTMemberGroupDao accessControlSpotKSHTMemberGroupDao;

    /**
     * @param hrId
     * @return
     * @description 根据工号查询人员所在旷视鸿图人员组
     * @author tql
     * @date 22-5-20
     */
    @Override
    public List<AccessControlSpotKSHTMemberGroup> findKsHtUserGroupByHrId(String hrId) {
        List<AccessControlSpotKSHTMemberGroup> list = new ArrayList<>();
        List<AccessControlSpotUser> userHrId = accessControlSpotUserDao.findByUserHrId(hrId);
        boolean isVisitor = SysUserService.isVisitor(hrId);
        if (!CollectionUtils.isEmpty(userHrId)) {
            List<String> spotIds = userHrId.stream().map(AccessControlSpotUser::getAccessControlSpot).map(AccessControlSpot::getId).collect(Collectors.toList());
            List<AccessControlSpot> byIdIn = accessControlSpotDao.findByIdIn(spotIds);
            if (!CollectionUtils.isEmpty(byIdIn)) {
                List<AccessControlSpotKSHTMemberGroup> userGroups = accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotIdIn(byIdIn.stream()
                        .map(AccessControlSpot::getId).collect(Collectors.toSet()));
                userGroups.removeIf(g -> {
                    if (isVisitor) {
                        return KSHTMemberGroupType.STAFF.equals(g.getGroupType()) || KSHTMemberGroupType.MEAL_STAFF.equals(g.getGroupType());
                    } else {
                        return KSHTMemberGroupType.VISITOR.equals(g.getGroupType()) || KSHTMemberGroupType.MEAL_VISITOR.equals(g.getGroupType());
                    }
                });
                list.addAll(userGroups);
            }
        }

        return list;
    }
}
