package cn.com.cowain.ibms.rest.meeting;

import cn.com.cowain.ibms.annotation.Idempotent;
import cn.com.cowain.ibms.rest.req.meeting.CustomUserGroupEditReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.meeting.CustomUserGroupResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.meeting.CustomUserGroupService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.sfp.comm.utils.JwtUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author Yang.Lee
 * @date 2021/6/8 18:12
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/customUserGroup")
@Api(tags = IConst.MODULE_CUSTOM_USER_GROUP)
public class CustomUserGroupController {

    @Autowired
    private CustomUserGroupService customUserGroupService;


    /**
     * 新建用户组
     *
     * @param req           请求参数
     * @param bindingResult 数据校验对象
     * @return 创建结果，成功时返回数据的id
     * @author Yang.Lee
     * @date 2021/6/8 18:29
     **/
    @PostMapping
    @ApiOperation(value = "新增自定义用户组", tags = IConst.MODULE_CUSTOM_USER_GROUP)
    @Idempotent(value = API_BASE + "/customUserGroup/post", expireTime = 2L)
    public ResponseEntity<JsonResult<IdResp>> create(@RequestBody @Validated CustomUserGroupEditReq req, @RequestHeader("Authorization") String token ,BindingResult bindingResult) {

        // 从token中解析员工工号
        String hrId = JwtUtil.getHrId(token);
        ServiceResult result = customUserGroupService.save(req ,hrId);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.ok("OK", (IdResp) result.getObject()));

    }

    /**
     * 编辑用户组
     *
     * @param id            数据ID
     * @param req           请求参数
     * @param bindingResult 数据校验对象
     * @return 更新结果
     * @author Yang.Lee
     * @date 2021/6/8 18:29
     **/
    @PutMapping("/{id}")
    @ApiOperation(value = "更新自定义用户组", tags = IConst.MODULE_CUSTOM_USER_GROUP)
    public ResponseEntity<JsonResult<String>> update(@PathVariable String id, @RequestBody @Validated CustomUserGroupEditReq req ,BindingResult bindingResult) {

        // 从token中解析员工工号
        ServiceResult result = customUserGroupService.update(req ,id);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));

    }

    /**
     * 删除用户组
     *
     * @param id 用户组ID
     * @return 删除结果
     * @author Yang.Lee
     * @date 2021/6/8 18:32
     **/
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除自定义用户组", tags = IConst.MODULE_CUSTOM_USER_GROUP)
    public ResponseEntity<JsonResult<String>> delete(@PathVariable String id) {

        ServiceResult result = customUserGroupService.delete(id);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));

    }

    /**
     * 查询用户的用户组列表
     *
     * @param token 身份令牌，包含用户工号
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/6/8 18:36
     **/
    @GetMapping("/list")
    @ApiOperation(value = "查询自定义用户组列表", tags = IConst.MODULE_CUSTOM_USER_GROUP)
    public ResponseEntity<JsonResult<List<CustomUserGroupResp>>> getList(@RequestHeader("Authorization") String token) {

        // 从token中解析员工工号
        String hrId = JwtUtil.getHrId(token);
        ServiceResult result = customUserGroupService.list(hrId);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.ok("ok", (List<CustomUserGroupResp>)result.getObject()));
    }

    /**
     * 查询用户的用户组信息
     *
     * @param token 身份令牌，包含用户工号
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/6/8 18:36
     **/
    @GetMapping("/{id}")
    @ApiOperation(value = "查询用户组信息", tags = IConst.MODULE_CUSTOM_USER_GROUP)
    public ResponseEntity<JsonResult<CustomUserGroupResp>> get(@PathVariable String id , @RequestHeader("Authorization") String token) {

        // 从token中解析员工工号
        String hrId = JwtUtil.getHrId(token);
        ServiceResult result = customUserGroupService.get(id , hrId);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        // 优先读取redis中数据
        return ResponseEntity.ok(JsonResult.ok("ok", (CustomUserGroupResp)result.getObject()));
    }

}
