package cn.com.cowain.ibms.rest.resp.meal;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author wei.cheng
 * @date 2022/06/13 17:04
 */
@Data
public class DeleteHtAcrossPermissionConfigReq {

    @ApiModelProperty(value = "配置名称", required = true, example = "KS_")
    private String name;
}
