package cn.com.cowain.ibms.annotation;

import java.lang.annotation.*;

/**
 * 幂等注解
 * <p>该注解使用在controller类的方法中，为标注的api方法添加幂等性</p>
 * @author Yang.Lee
 * @date 2020/12/16 13:12
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Idempotent {
    /**
     * 幂等名称，作为redis缓存Key的一部分,相同路径不同方法的请求需要进行区分
     */
    String value();

    /**
     * 幂等过期时间(单位：秒)，即：在此时间段内，对API进行幂等处理。
     */
    long expireTime() default 1L;

}
