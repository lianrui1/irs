package cn.com.cowain.ibms.rest.req.space;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * @author yuchunlei
 * @version 1.0.0
 * @createTime 2020年12月17日 17:41:00
 */
@Data
@ApiModel("设备信息")
public class IotDeviceCreateReq implements Serializable {


    private static final long serialVersionUID = -9148075454042606685L;

    @NotBlank(message = "设备名称不能为空")
    @Size(min = 1, max = 20, message = "设备名称长度在1-20之间")
    @ApiModelProperty(value = "设备名称", required = true, position = 0)
    private String deviceName;

    @ApiModelProperty(value = "产品Id", required = true, position = 2)
    private String productId;

    @NotBlank(message = "所属项目不能为空")
    @ApiModelProperty(value = "所属项目", required = true, position = 3)
    private String projectId;

    @ApiModelProperty(value = "所属空间", required = true, position = 4)
    private String spaceId;

    @ApiModelProperty(value = "设备位置",  position = 6)
    private String address;

    @ApiModelProperty(value = "是否可控",  position = 7)
    private int spaceControl;

    @ApiModelProperty(value = "应用列表",  position = 8)
    private List<String> applicationList;
}
