package cn.com.cowain.ibms.dao.meeting;

import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.entity.meeting.InvitationCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface InvitationCardDao extends JpaRepository<InvitationCard, String> {


    // 根据code获取详细信息
    Optional<InvitationCard> findByInvitationCode(String code);

    // 根据会议记录获取邀请卡
    Optional<InvitationCard> findByReservationRecord(ReservationRecord reservationRecord);

    // 根据邀请函单号获取邀请函信息
    Optional<InvitationCard> findByVisitorRecordNo(String visitorRecordNo);
}
