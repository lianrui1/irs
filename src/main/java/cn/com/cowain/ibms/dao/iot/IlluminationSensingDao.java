package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.IlluminationSensing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

/**
 * @author wei.cheng
 * @date 2022/03/29 18:40
 */
public interface IlluminationSensingDao extends JpaRepository<IlluminationSensing, String>, JpaSpecificationExecutor<IlluminationSensing> {
    /**
     * 查询deviceId = {@code deviceId}的IlluminationSensing
     *
     * @param deviceId
     * @return
     */
    Optional<IlluminationSensing> findByDeviceId(String deviceId);
}
