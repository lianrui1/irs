package cn.com.cowain.ibms.rest.resp.iot;

import cn.com.cowain.ibms.enumeration.iot.DeviceLinkStatus;
import cn.com.cowain.ibms.utils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 设备联动分页相应
 */
@Data
@ApiModel("设备联动分页相应")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DeviceLinkPageResp {

    @ApiModelProperty(value = "联动ID", required = true, example = "123", position = 1)
    private String id;

    @ApiModelProperty(value = "联动编号", required = true, example = "LD123", position = 2)
    private String number;

    @ApiModelProperty(value = "设备位置", example = "南通大厅", position = 3)
    private String address;

    @ApiModelProperty(value = "设备名称列表", required = true, position = 4)
    private List<String> deviceNameList;

    @ApiModelProperty(value = "联动状态", required = true, example = "NORMAL：正常；DISABLE：禁用；INVALID：失效", position = 5)
    private DeviceLinkStatus status;

    @ApiModelProperty(value = "联动状态名称", required = true, example = "正常", position = 6)
    private String statusName;

    @JsonFormat(pattern = DateUtils.PATTERN_DATETIME1)
    @ApiModelProperty(value = "数据创建时间", required = true, example = "2021-09-02 12:34", position = 7)
    private LocalDateTime createTime;
}
