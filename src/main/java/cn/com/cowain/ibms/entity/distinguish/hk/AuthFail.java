package cn.com.cowain.ibms.entity.distinguish.hk;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/8/4 11:24
 */
@Data
public class AuthFail {

    private String startTime;
    private String endTime;
    private String jobNo;
    private String msg;
}
