package cn.com.cowain.ibms.mq.producer;

import cn.com.cowain.ibms.mq.Topic;
import cn.com.cowain.ibms.mq.bean.IotDeviceControlMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 设备控制指令消息生产者
 *
 * @author Yang.Lee
 * @date 2021/6/17 13:42
 */
@Slf4j
@Component
public class IotDeviceMessageProducer {

    @Value("${redis.key.projectName}-${rocketmq.group}-")
    private String topic;

    @Resource
    private RocketMQTemplate rocketMQTemplate;

    /**
     * 发送设备控制指令
     *
     * @param deviceId 设备ID
     * @param cmd      控制指令
     * @return 消息发送结果
     * @author Yang.Lee
     * @date 2021/6/17 13:43
     **/
    public ServiceResult deviceControl(String deviceId, String cmd) {

        String messageStr = JSON.toJSONString(IotDeviceControlMessage.builder().deviceId(deviceId).cmd(cmd).build());
        Message<String> message = new GenericMessage<>(messageStr);
        SendResult sendResult = rocketMQTemplate.syncSend(topic+Topic.DEVICE_CONTROL, message);

        if (!sendResult.getSendStatus().equals(SendStatus.SEND_OK)) {
            log.error("设备控制消息发送失败：" + sendResult);
            return ServiceResult.error(sendResult.toString());
        }

        return ServiceResult.ok();
    }
}
