package cn.com.cowain.ibms.rest.resp.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2020/12/2 14:04
 */
@Data
@ApiModel("二维码响应对象")
public class QrCodeResp {

    @ApiModelProperty("图片内容（base64）")
    private String base64Content;
}
