package cn.com.cowain.ibms.rest;

import cn.com.cowain.ibms.job.HealthCheckJob;
import cn.com.cowain.ibms.job.StaffJob;
import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.utils.IConst;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * 定时任务api
 *
 * @author Yang.Lee
 * @date 2022/1/18 15:23
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/timedTask")
@Api(tags = IConst.MODULE_TIMED_TASK)
public class TimedTaskController {

    @Resource
    private HealthCheckJob healthCheckJob;

    @Resource
    private StaffJob staffJob;


    @GetMapping("/hk/health/check")
    @ApiOperation(value = "海康心跳查询", tags = IConst.MODULE_TIMED_TASK)
    public ResponseEntity<JsonResult<String>> doHKHealthCheckJob() {

        log.info("定时任务接口 - 海康心跳查询 - 开始运行");
        healthCheckJob.run();

        return ResponseEntity.ok(JsonResult.okNoData(null));
    }

    @GetMapping("/staff/leave/check")
    @ApiOperation(value = "员工离职查询", tags = IConst.MODULE_TIMED_TASK)
    public ResponseEntity<JsonResult<String>> doStaffCheckJob(){

        log.info("定时任务接口 - 员工离职查询 - 开始运行");
        staffJob.check();

        return ResponseEntity.ok(JsonResult.okNoData(null));
    }

    @GetMapping("/staff/visitor/check")
    @ApiOperation(value = "访客数据更新", tags = IConst.MODULE_TIMED_TASK)
    public ResponseEntity<JsonResult<String>> doVisitorJob(){

        log.info("定时任务接口 - 访客查询 - 开始运行");
        staffJob.visitor();

        return ResponseEntity.ok(JsonResult.okNoData(null));
    }

}
