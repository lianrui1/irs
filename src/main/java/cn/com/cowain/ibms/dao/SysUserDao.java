package cn.com.cowain.ibms.dao;

import cn.com.cowain.ibms.bo.SimpleSysUser;
import cn.com.cowain.ibms.entity.SysUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * SysUser Dao
 *
 * @author 张宇鑫
 * @version 1.0
 * @date 2020/8/10 16:06
 */
@Repository
public interface SysUserDao extends JpaRepository<SysUser, String>, JpaSpecificationExecutor<SysUser> {

    /**
     * 根据用户id 获取用户信息
     *
     * @param sysId
     * @return Optional<SysUser>
     */
    Optional<SysUser> findBySysId(String sysId);

    /**
     * 根据工号获取员工信息
     *
     * @param empNo
     * @return
     */
    Optional<SysUser> findByEmpNo(String empNo);

    /**
     * 根据ucId查询用户信息
     *
     * @param ucUserId uc用户id
     * @return 员工信息
     */
    Optional<SysUser> findByUcUserId(String ucUserId);

    // 获取员工状态
    List<SysUser> findByStatusIsNull();

    // 根据ID获取人员信息
    List<SysUser> findByIdIn(List<String> ids);

    /**
     * 查询empNo in {@code empNoList}的sysUser
     *
     * @param empNoList
     * @return
     */
    List<SysUser> findAllByEmpNoIn(List<String> empNoList);

    /**
     * 根据工号获取员工部分属性
     *
     * @param empNos
     * @return
     */
    @Query(
            value = "select new cn.com.cowain.ibms.bo.SimpleSysUser(sysId, nameZh, nameEn, fullDepartmentName, headImgUrl, empNo) from SysUser where empNo in (:empNos) and isDelete = 0"
    )
    List<SimpleSysUser> findSimpleSysUserByEmpNos(@Param("empNos") List<String> empNos);

    /**
     * 批量根据工号获取员工的名称
     *
      * @param empNos
     * @return
     */
    @Query(
            value = "select new cn.com.cowain.ibms.bo.SimpleSysUser(empNo, nameZh) from SysUser where empNo in (:empNos)"
    )
    List<SimpleSysUser> findEmpNpAndNameZh(@Param("empNos") List<String> empNos);
}
