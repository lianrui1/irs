package cn.com.cowain.ibms.mq.consumer;

import cn.com.cowain.ibms.mq.Topic;
import cn.com.cowain.ibms.mq.bean.IotDeviceControlMessage;
import cn.com.cowain.ibms.service.space.IotDeviceService;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 设备控制消息消费者
 *
 * @author Yang.Lee
 * @date 2021/6/17 14:31
 */
@Slf4j
@RocketMQMessageListener(
        topic = "${redis.key.projectName}-${rocketmq.group}-"+Topic.DEVICE_CONTROL,
        consumerGroup = "${redis.key.projectName}-${rocketmq.group}"
)
@Component
public class IotDeviceMessageConsumer implements RocketMQListener<String> {

    @Resource
    private IotDeviceService iotDeviceService;

    @Override
    @Transactional
    public void onMessage(String message) {

        IotDeviceControlMessage iotDeviceControlMessage = JSON.parseObject(message, IotDeviceControlMessage.class);

        String[] strings = iotDeviceControlMessage.getDeviceId().split(",");
        for(int i = 0;i<strings.length;i++){
            iotDeviceService.control(strings[i], iotDeviceControlMessage.getCmd());
        }
    }
}
