package cn.com.cowain.ibms.entity.voice;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.ReservationRecord;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 会议语音对象
 * @author Yang.Lee
 * @date 2020/12/29 9:48
 */
@Data
@Entity
@Table(name = "audio_record")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "audio_record", comment = "语音记录数据表")
public class AudioRecord extends BaseEntity {


    /**
     * 会议预约记录ID，外键
     */
    @ManyToOne
    @JoinColumn(name = "reservation_record_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK'")
    private ReservationRecord reservationRecord;

    /**
     * 语音开始时间
     */
    @Column(name = "start_time", columnDefinition = "datetime COMMENT '语音开始时间'")
    private LocalDateTime startTime;

    /**
     * 语音结束时间
     */
    @Column(name = "end_time",  columnDefinition = "datetime COMMENT '语音结束时间'")
    private LocalDateTime endTime;

    /**
     * pcm音频文件路径
     */
    @Column(name = "pcm_file_path",  columnDefinition = "varchar(80) COMMENT '语音文件路径'")
    private String pcmFilePath;

    /**
     * 音频文件路径
     */
    @Column(name = "audio_file_path", columnDefinition = "varchar(80) COMMENT '音频文件路径'")
    private String audioFilePath;

    /**
     * 文本文件路径
     */
    @Column(name = "text_file_path",  columnDefinition = "varchar(80) COMMENT '文本文件路径'")
    private String textFilePath;

    @Column(name = "rt_text_file_path",  columnDefinition = "varchar(80) COMMENT '实时翻译文本路径'")
    private String rtTextFilePath;
}
