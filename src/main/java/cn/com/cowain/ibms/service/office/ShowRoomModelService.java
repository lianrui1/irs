package cn.com.cowain.ibms.service.office;

import cn.com.cowain.ibms.feign.bean.common.CommonReq;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.PageReq;
import cn.com.cowain.ibms.rest.req.working_mode.ShowRoomWorkingModeSaveReq;
import cn.com.cowain.ibms.rest.resp.iot.IotDeviceResp;
import cn.com.cowain.ibms.rest.resp.working_mode.DefaultWorkingModePageResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;

import java.util.List;

/**
 * @author feng
 * @title: ShowRoomModelService
 * @projectName ibms
 * @Date 2022/3/31 15:36
 */
public interface ShowRoomModelService {
    ServiceResult save(ShowRoomWorkingModeSaveReq req) ;
    ServiceResult controlWorkingMode(String id);
    PageBean<DefaultWorkingModePageResp> getDefaultWorkingModePage(String spaceId,Integer showroomCentralize, PageReq req);
    ServiceResult changeDeviceName(CommonReq req) ;
    ServiceResult update(ShowRoomWorkingModeSaveReq req);
    ServiceResult detail(String id) ;

     List<IotDeviceResp> search(String  spaceId) ;
     ServiceResult delette(String id) ;

}
