package cn.com.cowain.ibms.mq.bean;

import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/1/19 16:17
 */
@Data
public class DeviceStatusChangeMsgPushMessage {

    // 设备华为云ID
    private String deviceHwId;

    // 设备状态
    private DeviceStatus status;
}

