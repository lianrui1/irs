package cn.com.cowain.ibms.entity.meeting;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.Room;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author Yang.Lee
 * @date 2021/12/27 10:47
 */
@Data
@Entity
@Table(name = "ibms_intelligent_meeting_app")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_intelligent_meeting_app", comment = "智能会议大屏应用表")
@Where(clause = "is_del=0")
public class IntelligentMeetingApp extends BaseEntity {

    @Column(name = "serial_number", nullable = false, columnDefinition = "varchar(50) COMMENT '设备序列号'")
    private String serialNumber;

    @ManyToOne
    @JoinColumn(name = "room_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 会议室ID'")
    private Room room;
}
