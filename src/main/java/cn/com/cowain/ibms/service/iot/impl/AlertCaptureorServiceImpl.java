package cn.com.cowain.ibms.service.iot.impl;


import cn.com.cowain.ibms.dao.iot.AlertCaptureorDao;
import cn.com.cowain.ibms.dao.iot.AlertSpotDao;
import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.entity.MessageBody;
import cn.com.cowain.ibms.entity.MessagePoolRecord;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.iot.AlertCaptureor;
import cn.com.cowain.ibms.entity.iot.AlertSpot;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.enumeration.AlertCaptureorStatus;
import cn.com.cowain.ibms.enumeration.AlertSubType;
import cn.com.cowain.ibms.enumeration.AlertType;
import cn.com.cowain.ibms.mq.bean.AlertCaptureorMessage;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.iot.AlertCaptureorDetailReq;
import cn.com.cowain.ibms.rest.req.iot.AlertCaptureorGetExistOfDayQueryReq;
import cn.com.cowain.ibms.rest.req.iot.AlertCaptureorReq;
import cn.com.cowain.ibms.rest.resp.iot.AlertCaptureorResp;
import cn.com.cowain.ibms.service.MessageSendService;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.iot.AlertCaptureorService;
import cn.com.cowain.ibms.utils.DateUtils;
import cn.com.cowain.ibms.utils.RedisUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author: yanzy
 * @date: 2022/4/13 14:17
 */
@Service
@Slf4j
public class AlertCaptureorServiceImpl implements AlertCaptureorService {

    public static final String ALERT_CAPTUREOR_EXIST_DAY_SET = "alert-captureor-exist-day-set";
    @Resource
    private AlertCaptureorDao alertCaptureorDao;
    @Resource
    private AlertSpotDao alertSpotDao;
    @Resource
    private DeviceDao deviceDao;
    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserServiceUC;
    @Resource
    private RedisUtil redisUtil;
    @Resource
    private MessageSendService messageSendService;
    @Value("${cowain.send.message.vx.msgTemplateId}")
    private String vxMsgTemplateId;
    @Value("${alert-captureor.detail-url}")
    private String detailUrl;
    @Value("#{'${alert-captureor.recipient-hr-ids}'.split(',')}")
    private List<String> recipientHrIds;


    @Override
    public ServiceResult createAlertCaptureor(AlertCaptureorMessage message) {
        AlertType alertType = getAlertType(message.getAlarmType());
        if (Objects.isNull(alertType)) {
            log.warn("ibms暂不支持消费该类型的警戒抓拍事件, alertType:{}", message.getAlarmType());
            return ServiceResult.error("ibms暂不支持消费该类型的警戒抓拍事件");
        }
        if (StringUtils.isBlank(message.getDeviceId())) {
            log.warn("警戒抓拍事件通知中设备号为空");
            return ServiceResult.error("警戒抓拍事件通知中设备号为空");
        }
        Optional<IotDevice> iotDeviceOptional = deviceDao.findBySn(message.getDeviceId());
        if (iotDeviceOptional.isEmpty()) {
            log.warn("根据警戒抓拍事件通知中设备号未找到对应的设备, sn:{}", message.getDeviceId());
            return ServiceResult.error("根据警戒抓拍事件通知中设备号未找到对应的设备");
        }
        IotDevice iotDevice = iotDeviceOptional.get();
        List<AlertSpot> alertSpotList = alertSpotDao.findAllByDeviceId(iotDevice.getId());
        if (CollectionUtils.isEmpty(alertSpotList)) {
            log.warn("设备未绑定警戒点, id:{}", iotDevice.getId());
            return ServiceResult.error("设备未绑定警戒点");
        }
        if (alertSpotList.size() > 1) {
            log.warn("设备绑定了多个警戒点，id:{}", iotDevice.getId());
            alertSpotList.sort((a1, a2) -> org.apache.commons.lang3.ObjectUtils.compare(a2.getCreatedTime(), a1.getCreatedTime()));
        }
        AlertSpot alertSpot = alertSpotList.get(0);
        AlertCaptureor alertCaptureor = new AlertCaptureor();
        alertCaptureor.setName(message.getName());
        alertCaptureor.setAddress(alertSpot.getAddress());
        alertCaptureor.setSpace(alertSpot.getSpace());
        alertCaptureor.setDevice(iotDevice);
        alertCaptureor.setAlertSpot(alertSpot);
        alertCaptureor.setAlertType(alertType);
        alertCaptureor.setAlertType2(AlertSubType.getAlertSubTypeByRequest(message.getAlarmSubType()));
        alertCaptureor.setAlertTime(getAlertTime(message.getAlertTime()));
        alertCaptureor.setSnapPicture(getSnapPicture(message));
        if (StringUtils.isNotBlank(message.getJobNo())) {
            SysUser sysUser = sysUserServiceUC.getUserByEmpNo(message.getJobNo());
            if (Objects.nonNull(sysUser)) {
                alertCaptureor.setUserHrId(sysUser.getEmpNo());
                alertCaptureor.setUserName(sysUser.getNameZh());
                alertCaptureor.setUserDepartment(sysUser.getDeptName());
            } else {
                alertCaptureor.setUserHrId(message.getJobNo());
                alertCaptureor.setUserName(message.getName());
                alertCaptureor.setUserDepartment("");
            }
        }
        alertCaptureor.setStatus(AlertCaptureorStatus.NOT_VIEWED);
        alertCaptureorDao.save(alertCaptureor);
        // redis缓存中存在报警记录的日期添加当前报警激励的报警日期
        redisUtil.sSet(getAlertCaptureorExistDayListKey(), DateUtils.formatLocalDateTime(alertCaptureor.getAlertTime(), DateUtils.PATTERN_DATE));
        // 发送微信消息通知给管理人员
        sendMessage(alertCaptureor);
        return ServiceResult.ok();
    }

    private void sendMessage(AlertCaptureor alertCaptureor) {
        if (CollectionUtils.isEmpty(recipientHrIds)) {
            log.warn("没有警戒抓拍消息接受人");
            return;
        }
        List<SysUser> users = recipientHrIds.stream().map(hrId -> sysUserServiceUC.getUserByEmpNo(hrId))
                .filter(s -> Objects.nonNull(s) && StringUtils.isNotBlank(s.getWxOpenId())).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(users)) {
            log.warn("根据工号无法找到对应人员的wxOpenId");
            return;
        }
        users.forEach(user -> {
            MessagePoolRecord messagePoolRecord = new MessagePoolRecord();
            messagePoolRecord.setMsgFromSys(8);
            messagePoolRecord.setMsgType(3);
            messagePoolRecord.setPriority(0);
            messagePoolRecord.setSendType(0);
            messagePoolRecord.setReceiveType(501);
            messagePoolRecord.setMsgTitle("警戒抓拍服务提醒");
            MessageBody messageBody = new MessageBody();
            messageBody.setTitle(getMessageTitle(alertCaptureor));
            messageBody.setType(alertCaptureor.getAlertType().getName());
            messageBody.setDepart("已推送");
            messageBody.setTime(DateUtils.formatLocalDateTime(alertCaptureor.getAlertTime(), DateUtils.PATTERN_DATETIME));
            messageBody.setRemark(getRemark(alertCaptureor));
            messagePoolRecord.setMsgContent(JSON.toJSONString(messageBody));
            messagePoolRecord.setMsgDetailsUrl(detailUrl + alertCaptureor.getId());
            messagePoolRecord.setMsgTemplateId(vxMsgTemplateId);
            messagePoolRecord.setTipType(1);
            messagePoolRecord.setReceivePerson(user.getWxOpenId());
            messagePoolRecord.setReceivePersonId(user.getWxOpenId());
            try {
                messageSendService.send(messagePoolRecord);
            } catch (Exception e) {
                log.error("发送消息中心失败", e);
            }
        });
    }

    /**
     * 获取推送消息标题
     *
     * @param alertCaptureor
     * @return
     */
    private String getMessageTitle(AlertCaptureor alertCaptureor) {
        return String.format("警戒抓拍提醒您，有人%s；", alertCaptureor.getAlertType().getName().replaceFirst("人员", ""));
    }

    /**
     * 获取推送消息备注
     *
     * @param alertCaptureor
     * @return
     */
    private String getRemark(AlertCaptureor alertCaptureor) {
        return String.format("违规人员：%s;%n违规地点：%s", getUserInMessage(alertCaptureor), getAddressInMessage(alertCaptureor));
    }

    /**
     * 获取推送消息中入侵地点
     *
     * @param alertCaptureor
     * @return
     */
    private String getAddressInMessage(AlertCaptureor alertCaptureor) {
        return alertCaptureor.getAddress();
    }

    /**
     * 获取推送消息中入侵人员
     *
     * @param alertCaptureor
     * @return
     */
    private String getUserInMessage(AlertCaptureor alertCaptureor) {
        String result;
        if (StringUtils.isNotBlank(alertCaptureor.getUserHrId())) {
            List<String> list = new ArrayList<>();
            if (StringUtils.isNotBlank(alertCaptureor.getUserName())) {
                list.add(alertCaptureor.getUserName());
            }
            if (StringUtils.isNotBlank(alertCaptureor.getUserHrId())) {
                list.add(alertCaptureor.getUserHrId());
            }
            if (StringUtils.isNotBlank(alertCaptureor.getUserDepartment())) {
                list.add(alertCaptureor.getUserDepartment());
            }
            result = StringUtils.join(list, "/");
        } else {
            result = "陌生人";
        }
        return result;
    }

    /**
     * 获取警戒抓拍时间
     *
     * @param alertTime
     * @return
     */
    private LocalDateTime getAlertTime(String alertTime) {
        LocalDateTime localDateTime;
        try {
            localDateTime = DateUtils.parseLocalDateTime(alertTime, DateUtils.PATTERN_DATETIME);
        } catch (Exception e) {
            log.warn("警戒抓拍事件中警戒事件格式不正确，alertTime:{}", alertTime);
            localDateTime = LocalDateTime.now();
        }
        return localDateTime;
    }

    /**
     * 获取警戒抓拍类型
     *
     * @param alarmTypeInMessage
     * @return
     */
    private AlertType getAlertType(Integer alarmTypeInMessage) {
        if (Objects.isNull(alarmTypeInMessage)) {
            return null;
        }
        AlertType alertType;
        switch (alarmTypeInMessage) {
            case 2:
                alertType = AlertType.INTRUSION;
                break;
            case 6:
                alertType = AlertType.LINGER;
                break;
            case 5:
                alertType = AlertType.SURMOUNT;
                break;
            case 17:
                alertType = AlertType.PLAY_PHONE;
                break;
            case 18:
                alertType = AlertType.PHONE;
                break;
            case 16:
                alertType = AlertType.SMOKE;
                break;
            default:
                alertType = null;
        }
        return alertType;
    }

    /**
     * 获取抓拍图片
     *
     * @param message
     * @return
     */
    private String getSnapPicture(AlertCaptureorMessage message) {
        if (StringUtils.isNotBlank(message.getCaptureImg())) {
            return message.getCaptureImg();
        }
        if (StringUtils.isNotBlank(message.getFullImg())) {
            return message.getFullImg();
        }
        if (StringUtils.isNotBlank(message.getGroupImg())) {
            return message.getGroupImg();
        }
        return null;
    }

    @Override
    public ServiceResult getAlertCaptureorDetail(AlertCaptureorDetailReq req) {

        Optional<AlertCaptureor> alertCaptureorOp = alertCaptureorDao.findById(req.getId());
        if (alertCaptureorOp.isEmpty()) {
            return ServiceResult.error("id有误,未找到警戒抓拍信息");
        }

        AlertCaptureor alertCaptureor = alertCaptureorOp.get();

        //返回详情
        AlertCaptureorResp alertCaptureorResp = new AlertCaptureorResp();
        alertCaptureorResp.setId(alertCaptureor.getId());
        alertCaptureorResp.setAlertType(alertCaptureor.getAlertType().getName());
        if (alertCaptureor.getUserHrId() != null) {
            alertCaptureorResp.setUserHrId(alertCaptureor.getUserHrId());
            alertCaptureorResp.setUserName(alertCaptureor.getUserName());
            alertCaptureorResp.setUserDepartment(alertCaptureor.getUserDepartment());
        } else {
            alertCaptureorResp.setUserName("陌生人");
        }
        alertCaptureorResp.setAddress(alertCaptureor.getAddress());
        alertCaptureorResp.setAlertTime(alertCaptureor.getAlertTime());
        alertCaptureorResp.setSnapPicture(alertCaptureor.getSnapPicture());

        //判断是否设置为已读
        if (!req.getIsView().isEmpty()) {
            alertCaptureor.setStatus(AlertCaptureorStatus.VIEWED);
            alertCaptureorDao.save(alertCaptureor);
        }
        return ServiceResult.ok(alertCaptureorResp);
    }

    @Override
    public PageBean<AlertCaptureorResp> getPage(AlertCaptureorReq req) {

        //封装and语句
        List<Predicate> listAnd = new ArrayList<>();
        //封装or语句
        List<Predicate> listOr = new ArrayList<>();

        Page<AlertCaptureor> alertCaptureors = alertCaptureorDao.findAll((root, query, criteriaBuilder) -> {
            //审批状态
            if (ObjectUtils.isNotEmpty(req.getStatus())) {
                listAnd.add(criteriaBuilder.equal(root.get("status"), req.getStatus()));
            }

            //监控点
            if (StringUtils.isNotBlank(req.getAlertSpotId())) {
                listAnd.add(criteriaBuilder.equal(root.get("alertSpot").get("id"), req.getAlertSpotId()));
            }

            //报警类型
            if (ObjectUtils.isNotEmpty(req.getAlertType())) {
                listAnd.add(criteriaBuilder.equal(root.get("alertType"), req.getAlertType()));
            }

            //抓拍时间
            if (ObjectUtils.isNotEmpty(req.getAlertTimeStartTime()) && ObjectUtils.isNotEmpty(req.getAlertTimeEndTime())) {
                listAnd.add(criteriaBuilder.greaterThanOrEqualTo(root.get("alertTime"), req.getAlertTimeStartTime()));
                listAnd.add(criteriaBuilder.lessThanOrEqualTo(root.get("alertTime"), req.getAlertTimeEndTime()));
            }

            //(H5)抓拍时间
            if (ObjectUtils.isNotEmpty(req.getAlertTimeDay())) {
                //获取年月日
                int year = req.getAlertTimeDay().getYear();
                int month = req.getAlertTimeDay().getMonth().getValue();
                int day = req.getAlertTimeDay().getDayOfMonth();

                //定义开始时间、结束时间
                LocalDateTime createDateStart = LocalDateTime.of(year, month, day, 00, 00, 00);
                LocalDateTime createDateEnd = LocalDateTime.of(year, month, day, 23, 59, 59);

                listAnd.add(criteriaBuilder.greaterThanOrEqualTo(root.get("alertTime"), createDateStart));
                listAnd.add(criteriaBuilder.lessThanOrEqualTo(root.get("alertTime"), createDateEnd));
            }

            Predicate[] array_and = new Predicate[listAnd.size()];
            Predicate Pre_And = criteriaBuilder.and(listAnd.toArray(array_and));
            Predicate[] array_or = new Predicate[listAnd.size()];
            Predicate Pre_Or = criteriaBuilder.or(listOr.toArray(array_or));
            if (!org.springframework.util.CollectionUtils.isEmpty(listOr)) {
                return query.where(Pre_And, Pre_Or).getRestriction();
            } else {
                return query.where(Pre_And).getRestriction();
            }

        }, PageRequest.of(req.getPage(), req.getSize(), Sort.by(req.getOrders())));

        PageBean<AlertCaptureorResp> pageBean = new PageBean<>();
        pageBean.setPage(alertCaptureors.getPageable().getPageNumber());
        pageBean.setSize(alertCaptureors.getPageable().getPageSize());
        pageBean.setTotalPages(alertCaptureors.getTotalPages());
        pageBean.setTotalElements(alertCaptureors.getTotalElements());
        pageBean.setList(alertCaptureors.getContent()
                .stream()
                .map(AlertCaptureorResp::convert)
                .collect(Collectors.toList()));
        return pageBean;
    }

    @Override
    public ServiceResult getAlertCaptureorExistOfDay(AlertCaptureorGetExistOfDayQueryReq req) {
        if (StringUtils.isBlank(req.getStartDate()) || StringUtils.isBlank(req.getEndDate())) {
            return ServiceResult.error("startDate和endDate必须都有值");
        }
        LocalDate startDate;
        try {
            startDate = DateUtils.parseLocalDate(req.getStartDate(), DateUtils.PATTERN_DATE);
        } catch (Exception e) {
            return ServiceResult.error("startDate格式不正确");
        }
        LocalDate endDate;
        try {
            endDate = DateUtils.parseLocalDate(req.getEndDate(), DateUtils.PATTERN_DATE);
        } catch (Exception e) {
            return ServiceResult.error("endDate格式不正确");
        }
        if (startDate.isAfter(endDate)) {
            return ServiceResult.error("startDate不能大于endDate");
        }
        String key = getAlertCaptureorExistDayListKey();
        Set<String> dayList = new HashSet<>();
        if (redisUtil.hasKey(key)) {
            Set<Object> valueList = redisUtil.sGet(key);
            for (Object o : valueList) {
                dayList.add((String) o);
            }
        } else {
            dayList.addAll(refreshAlertCaptureorExistDayList());
        }
        Map<String, Boolean> result = new LinkedHashMap<>();
        for (LocalDate i = startDate; !i.isAfter(endDate); i = i.plusDays(1)) {
            String day = DateUtils.formatLocalDate(i, DateUtils.PATTERN_DATE);
            result.put(day, dayList.contains(day));
        }
        return ServiceResult.ok(result);
    }

    @PostConstruct
    public void initAlertCaptureorExistDayList() {
        log.info("开始初始化警戒抓拍记录存在的日期列表");
        refreshAlertCaptureorExistDayList();
    }

    /**
     * 刷新redis中警戒抓拍记录存在的日期列表
     *
     * @return
     */
    private Set<String> refreshAlertCaptureorExistDayList() {
        Set<String> dayList = alertCaptureorDao.findAlertTimeDay();
        String key = getAlertCaptureorExistDayListKey();
        redisUtil.del(key);
        if (CollectionUtils.isEmpty(dayList)) {
            // 由于redis set类型数据value不能为空，当不存在记录时，新增一条空数据
            dayList.add(StringUtils.EMPTY);
        }
        redisUtil.sSet(key, dayList.toArray());
        return dayList;
    }

    /**
     * 获取redis中存在警戒抓拍记录的列表的key值
     *
     * @return
     */
    private String getAlertCaptureorExistDayListKey() {
        return redisUtil.createRealKey(ALERT_CAPTUREOR_EXIST_DAY_SET);
    }
}
