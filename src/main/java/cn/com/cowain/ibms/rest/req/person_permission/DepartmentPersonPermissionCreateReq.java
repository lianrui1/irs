package cn.com.cowain.ibms.rest.req.person_permission;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

/**
 * 创建设备人员权限请求参数对象
 *
 * @author Yang.Lee
 * @date 2021/3/19 20:32
 */
@Data
@ApiModel("组织设备人员权限")
public class DepartmentPersonPermissionCreateReq implements Serializable {

    private static final long serialVersionUID = 8221601774855330853L;

    @ApiModelProperty(value = "departments", required = true, position = 0)
    private List<String> departments;

    @ApiModelProperty(value = "设备ids", required = true, position = 1)
    private List<String> deviceId;

    @NotBlank(message = "spaceId不能为空")
    @ApiModelProperty(value = "spaceId", required = true, position = 2)
    private String spaceId;
}
