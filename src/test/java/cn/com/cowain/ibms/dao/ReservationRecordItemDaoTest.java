package cn.com.cowain.ibms.dao;


import cn.com.cowain.ibms.entity.ReservationRecordItem;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.bean.ReservationRecordBean;
import cn.com.cowain.ibms.utils.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@SpringBootTest
public class ReservationRecordItemDaoTest {

    @Autowired
    private ReservationRecordItemDao reservationRecordItemDao;

    @Test
    public void search() {
//        Sort sort = Sort.by(Sort.Direction.DESC, "from");
        Pageable pageable = PageRequest.of(0, 7);

//        Page<ReservationRecordItem> reservationRecordItemSet = reservationRecordItemDao.findByOwner("testId111",pageable);
//        Page<ReservationRecordItem> reservationRecordItemSet = reservationRecordItemDao.findByOwnerOrderByReservationRecordFromDesc("testId111",pageable);
        LocalDateTime localDateTime = LocalDateTime.now().minusDays(7);
//        Page<ReservationRecordItem> reservationRecordItemSet = reservationRecordItemDao.findByReservationRecordFromGreaterThan(localDateTime,pageable);
//        Page<ReservationRecordItem> reservationRecordItemSet = reservationRecordItemDao.findByOwnerAndReservationRecordFromGreaterThan("testId111",localDateTime,pageable);
//        Page<ReservationRecordItem> reservationRecordItemSet = reservationRecordItemDao.findByOwnerOrInitiator("testId111", "testId111",pageable);
        String Owner = "testId111";
        Page<ReservationRecordItem> reservationRecordItemPage = reservationRecordItemDao.findByOwnerEmpNoAndReservationRecordFromGreaterThanAndReservationRecordRoomSpaceProjectIdContainsOrderByReservationRecordFromDesc(Owner, localDateTime, "123",pageable);
        PageBean pageBean = new PageBean();
        List<ReservationRecordBean> reservationRecordItemList = new ArrayList<>();
        if (reservationRecordItemPage.getContent().size() != 0) {


            reservationRecordItemPage.getContent().forEach(it -> {
                ReservationRecordBean reservationRecordBean = new ReservationRecordBean();
                reservationRecordBean.setTopic(it.getReservationRecord().getTopic());
                reservationRecordBean.setTimeFrom(it.getReservationRecord().getFrom());
                reservationRecordBean.setTimeTo(it.getReservationRecord().getTo());
                reservationRecordBean.setRoomName(it.getReservationRecord().getRoom().getName());
                reservationRecordBean.setRoomBuilding(it.getReservationRecord().getRoom().getBuilding());
                reservationRecordBean.setFloor(it.getReservationRecord().getRoom().getFloor());
                reservationRecordBean.setInitiator(it.getInitiator());
//                reservationRecordBean.setReservationRecordStatus(it.getStatus().name());
                if (it.getInitiator().equals(Owner)) {
                    reservationRecordBean.setIsOwn(1);
                }
                reservationRecordItemList.add(reservationRecordBean);

            });
            pageBean.setPage(pageable.getPageNumber());  //当前页码
            pageBean.setSize(pageable.getPageSize());  //每页记录数
            pageBean.setTotalElements(reservationRecordItemPage.getTotalElements());  //总记录数
            pageBean.setTotalPages(reservationRecordItemPage.getTotalPages());  //总页数
            pageBean.setList(reservationRecordItemList);

        }

//        if (reservationRecordItemSet.size() != 0) {
//            reservationRecordItemSet.forEach(it -> {
//                String Topic = it.getReservationRecord().getTopic();
//                LocalDateTime TimeForm = it.getReservationRecord().getFrom();
//                LocalDateTime TimeTo = it.getReservationRecord().getTo();
//                String RoomName = it.getReservationRecord().getRoom().getName();
//                String RoomBuilding = it.getReservationRecord().getRoom().getBuilding();
//                String Floor = it.getReservationRecord().getRoom().getFloor();
//                String Initiator = it.getInitiator();
//                String ReservationRecordStatus = it.getReservationRecord().getStatus().name();
//                  private Integer IsOwn;
//
//
//
//            });
//        }
        log.info(String.valueOf(reservationRecordItemPage));
    }

    @Test
    public void findByOwnerAndReservationRecordDate() {
        LocalDate date = DateUtil.convert("2020-08-12");
        List<ReservationRecordItem> list = reservationRecordItemDao.findByOwnerEmpNoAndReservationRecordDate("testId111", date);
        System.out.println(list);
    }

}
