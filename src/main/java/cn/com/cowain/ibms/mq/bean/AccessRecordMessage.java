package cn.com.cowain.ibms.mq.bean;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author tql
 * @Description 通行记录消息
 * @Date 21-12-9 下午1:43
 * @Version 1.0
 */
@Data
public class AccessRecordMessage {

    /**
     * 工号
     */
    private String jobNo;

    /**
     * 姓名
     */
    private String name;
    /**
     * 卡号
     */
    private String cardNo;
    /**
     * 海康人员编号
     */
    private String personId;

    /**
     * 门禁点编号
     */
    private String doorId;

    /**
     * 门禁点名称
     */
    private String doorName;

    /**
     * 识别成功的时间
     */
    private LocalDateTime happentime;


    /**
     * 进出方向
     */
    private String type;

    /**
     * 人员类型
     */
    private String personType;

    /**
     * 来源类型
     */
    private String resourceIndexCode;


    /**
     * 抓拍图片
     */
    private String capturePicture;

    /**
     * 底库图片
     */
    private String defaultPicture;

    /**
     * 通行状态
     */
    private String accessStatus;
}
