package cn.com.cowain.ibms.rest.resp.department;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 组织树响应信息
 * @author Yang.Lee
 * @date 2021/2/5 10:51
 */
@Data
@ApiModel("组织部门树")
public class DepartmentTreeResp implements Serializable {

    private static final long serialVersionUID = 3873201300178724840L;

    /**
     * 该字段为旧版从EHR同步数据遗留的冗余字段，在新版数据结构中无任何意义。
     * 保留该字段是为了保证前端接收数据的完整性
     */
    @ApiModelProperty(value = "消息")
    private String msg;

    /**
     * 该字段为旧版从EHR同步数据遗留的冗余字段，在新版数据结构中无任何意义。
     * 保留该字段是为了保证前端接收数据的完整性
     */
    @ApiModelProperty(value = "代号", position = 1)
    private Integer code;

    @ApiModelProperty(value = "部门列表", required = true, position = 2)
    private List<DepartmentResp> treeList;

}
