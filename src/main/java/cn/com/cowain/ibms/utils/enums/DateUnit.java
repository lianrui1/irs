package cn.com.cowain.ibms.utils.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author: cuiEnming
 * @title: DateUnit
 * @description: 时间枚举类
 * @date 2020/11/21  14:37
 */
@Getter
@AllArgsConstructor
public enum DateUnit {
    /**
     * 毫秒
     * */
    MILLIS("毫秒"),
    /**
     *秒
     * */
    SECOND("秒"),
    /**
     *分钟
     * */
    MINUTE("分钟"),
    /**
     *小时
     * */
    HOUR("小时"),
    /**
     *天
     * */
    DAY("天");

    private String desc;
}
