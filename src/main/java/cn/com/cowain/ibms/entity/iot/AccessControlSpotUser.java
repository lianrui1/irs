package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 门禁点-通行人权限表
 *
 * @author Yang.Lee
 * @date 2021/11/17 13:50
 */
@Data
@Entity
@Table(name = "iot_access_control_spot_user",indexes = {@Index(columnList = "user_hr_id,access_control_spot_id")})
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_access_control_spot_user", comment = "门禁点-员工表")
public class AccessControlSpotUser extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "access_control_spot_id", nullable = false, columnDefinition = "varchar(36) COMMENT '门禁点Id'")
    private AccessControlSpot accessControlSpot;

    @Column(name = "user_hr_id", nullable = false, columnDefinition = "varchar(50) COMMENT '用户工号'")
    private String userHrId;

    @Column(name = "user_name", columnDefinition = "varchar(50) COMMENT '用户姓名'")
    private String userName;

    @Column(name = "is_vip", columnDefinition = "tinyint(1) default 0 COMMENT '是否是VIP'")
    private int isVip;

    @Column(name = "access_start_time", nullable = false, columnDefinition = "datetime COMMENT '用户通行生效开始时间'")
    private LocalDateTime accessStartTime;

    @Column(name = "access_end_time", nullable = false, columnDefinition = "datetime COMMENT '用户通行生效结束时间'")
    private LocalDateTime accessEndTime;

    @Column(name = "ability_name", columnDefinition = "varchar(36) COMMENT '权限来源'")
    private String abilityName;
}
