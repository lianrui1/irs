package cn.com.cowain.ibms.dao.device;

import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.rest.resp.device.CircuitBreaker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.time.LocalDateTime;
import java.util.List;

public interface CircuitBreakerDao extends JpaRepository<CircuitBreaker, String>, JpaSpecificationExecutor<CircuitBreaker> {




    // 根据设备及日期范围查出期间用电数据
    List<CircuitBreaker> findByIotDeviceAndCreatedTimeGreaterThanEqualAndCreatedTimeLessThanEqualOrderByCreatedTime(IotDevice device, LocalDateTime startTime, LocalDateTime endTime);
}
