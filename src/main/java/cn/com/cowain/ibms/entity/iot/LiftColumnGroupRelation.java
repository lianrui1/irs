package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author Yang.Lee
 * @date 2022/6/24 10:08
 */
@Data
@Entity
@Table(name = "iot_left_column_group_relation")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_left_column_group_relation", comment = "升降柱组-设备关系表")
public class LiftColumnGroupRelation extends BaseEntity {

    /**
     * 升降柱组
     **/
    @ManyToOne
    @JoinColumn(name = "group_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK。升降柱组表主键id'")
    private LiftColumnGroup columnGroup;

    /**
     * iot 设备
     */
    @ManyToOne
    @JoinColumn(name = "device_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK。设备表主键id'")
    private IotDevice device;

    @Column(name = "control_type", nullable = false, columnDefinition = "tinyint(1)  COMMENT '控制类型，1 升，2 降'")
    private int controlType;
}
