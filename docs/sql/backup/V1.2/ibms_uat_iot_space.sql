-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 10.0.1.232    Database: ibms_uat
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `iot_space`
--

DROP TABLE IF EXISTS `iot_space`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `iot_space` (
  `id` varchar(36) NOT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `is_del` tinyint(1) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `version` int(10) DEFAULT NULL,
  `s_level` int(2) DEFAULT NULL,
  `s_name` varchar(255) DEFAULT NULL,
  `s_no` varchar(8) DEFAULT NULL,
  `s_remark` varchar(30) DEFAULT NULL,
  `s_type` varchar(255) DEFAULT NULL,
  `parent_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK91tpb1hoy1lpibwy0q9n2thbp` (`parent_id`) USING BTREE,
  KEY `FKmdjhar3l5y7jd0xsjt478nshy` (`project_id`) USING BTREE,
  CONSTRAINT `FK91tpb1hoy1lpibwy0q9n2thbp` FOREIGN KEY (`parent_id`) REFERENCES `iot_space` (`id`),
  CONSTRAINT `FKmdjhar3l5y7jd0xsjt478nshy` FOREIGN KEY (`project_id`) REFERENCES `ibms_project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `iot_space`
--

LOCK TABLES `iot_space` WRITE;
/*!40000 ALTER TABLE `iot_space` DISABLE KEYS */;
INSERT INTO `iot_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `s_level`, `s_name`, `s_no`, `s_remark`, `s_type`, `parent_id`, `project_id`) VALUES ('123',NULL,'2020-12-31 13:27:04',0,NULL,'2020-12-31 13:27:07',0,0,'DEFAULT MAGNETIC',NULL,NULL,'DEFAULT',NULL,'123');
INSERT INTO `iot_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `s_level`, `s_name`, `s_no`, `s_remark`, `s_type`, `parent_id`, `project_id`) VALUES ('15c5ad65-ab30-43b8-a5dc-38446e31a847',NULL,'2020-12-31 09:47:19',0,NULL,'2020-12-31 09:47:19',0,0,'临时会议室','',NULL,'ROOM',NULL,'123');
INSERT INTO `iot_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `s_level`, `s_name`, `s_no`, `s_remark`, `s_type`, `parent_id`, `project_id`) VALUES ('17007a67-0272-4f83-bc76-fac755a106b5',NULL,'2020-12-31 08:10:19',0,NULL,'2020-12-31 08:10:19',0,0,'天玑',NULL,NULL,'ROOM',NULL,'123');
INSERT INTO `iot_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `s_level`, `s_name`, `s_no`, `s_remark`, `s_type`, `parent_id`, `project_id`) VALUES ('1f53db55-dc98-444b-8cef-74762900930b',NULL,'2020-12-30 20:23:18',0,NULL,'2020-12-30 20:23:18',0,0,'产品研发中心',NULL,NULL,'ROOM',NULL,'123');
INSERT INTO `iot_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `s_level`, `s_name`, `s_no`, `s_remark`, `s_type`, `parent_id`, `project_id`) VALUES ('207832a5-aee9-4af3-9a7d-0c53bb45de0d',NULL,'2021-01-12 17:45:22',1,NULL,'2021-01-12 17:45:22',0,2,'33221','0101',NULL,'DEFAULT','a9b7c023-ca31-4587-afb4-d509e002014e','123');
INSERT INTO `iot_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `s_level`, `s_name`, `s_no`, `s_remark`, `s_type`, `parent_id`, `project_id`) VALUES ('49d2c5c1-8889-404b-afcb-bb0c9e7f650f',NULL,'2021-01-12 17:47:13',0,NULL,'2021-01-12 17:47:13',1,2,'Q楼','0101',NULL,'DEFAULT','a7cce572-6cbf-4d64-941e-b6706bd48b06','7e0e3902-129e-4b78-a624-0f3f56efb7b3');
INSERT INTO `iot_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `s_level`, `s_name`, `s_no`, `s_remark`, `s_type`, `parent_id`, `project_id`) VALUES ('49e19c29-9e64-4588-9e00-474b4152040b',NULL,'2021-01-12 17:38:30',1,NULL,'2021-01-12 17:38:30',1,1,'测试汇金大厦','01',NULL,'DEFAULT',NULL,'7e0e3902-129e-4b78-a624-0f3f56efb7b3');
INSERT INTO `iot_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `s_level`, `s_name`, `s_no`, `s_remark`, `s_type`, `parent_id`, `project_id`) VALUES ('57452c93-af90-4ba7-bd02-4267eb20c417',NULL,'2020-12-30 20:23:57',0,NULL,'2020-12-30 20:23:57',0,0,'长庚',NULL,NULL,'ROOM',NULL,'123');
INSERT INTO `iot_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `s_level`, `s_name`, `s_no`, `s_remark`, `s_type`, `parent_id`, `project_id`) VALUES ('6ceea129-054c-4b92-b9f7-a825770c70eb',NULL,'2021-01-12 18:01:01',1,NULL,'2021-01-12 18:01:01',0,1,'大厦测试','01','测试','DEFAULT',NULL,'1ef2f184-d3c7-4657-bbca-3fcfa7facfd1');
INSERT INTO `iot_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `s_level`, `s_name`, `s_no`, `s_remark`, `s_type`, `parent_id`, `project_id`) VALUES ('839b114d-87bc-4e3a-a8be-f274916d0416',NULL,'2020-12-30 20:24:26',0,NULL,'2020-12-30 20:24:26',0,0,'启明',NULL,NULL,'ROOM',NULL,'123');
INSERT INTO `iot_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `s_level`, `s_name`, `s_no`, `s_remark`, `s_type`, `parent_id`, `project_id`) VALUES ('877df00f-f3cc-4fd7-9e71-37a1fcc3d40f',NULL,'2020-12-16 11:01:51',0,NULL,'2020-12-16 11:01:51',0,0,'产品研发中心花桥',NULL,NULL,'ROOM',NULL,'123');
INSERT INTO `iot_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `s_level`, `s_name`, `s_no`, `s_remark`, `s_type`, `parent_id`, `project_id`) VALUES ('8791cef8-be55-4f35-868f-4dce51f8a9f8',NULL,'2020-12-15 18:20:20',0,NULL,'2020-12-15 18:20:20',0,0,'产品研发中心',NULL,NULL,'ROOM',NULL,'123');
INSERT INTO `iot_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `s_level`, `s_name`, `s_no`, `s_remark`, `s_type`, `parent_id`, `project_id`) VALUES ('92ef84ad-3441-4b55-aad8-657b6da330f9',NULL,'2021-01-12 17:01:07',0,NULL,'2021-01-12 17:01:07',0,2,'d楼','0101',NULL,'DEFAULT','d6b3def5-a26e-4c92-bb9e-d9f407cc5d83','123');
INSERT INTO `iot_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `s_level`, `s_name`, `s_no`, `s_remark`, `s_type`, `parent_id`, `project_id`) VALUES ('93f86f8b-190f-41bb-b9ed-899484f76c33',NULL,'2021-01-12 18:01:28',1,NULL,'2021-01-12 18:01:28',0,2,'B栋','0101','测试','DEFAULT','6ceea129-054c-4b92-b9f7-a825770c70eb','1ef2f184-d3c7-4657-bbca-3fcfa7facfd1');
INSERT INTO `iot_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `s_level`, `s_name`, `s_no`, `s_remark`, `s_type`, `parent_id`, `project_id`) VALUES ('a37a9ae4-e881-4278-b0d3-d5ef11a9d858',NULL,'2021-01-12 17:40:52',1,NULL,'2021-01-12 17:40:52',0,2,'H栋','0102',NULL,'DEFAULT','49e19c29-9e64-4588-9e00-474b4152040b','7e0e3902-129e-4b78-a624-0f3f56efb7b3');
INSERT INTO `iot_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `s_level`, `s_name`, `s_no`, `s_remark`, `s_type`, `parent_id`, `project_id`) VALUES ('a7cce572-6cbf-4d64-941e-b6706bd48b06',NULL,'2021-01-12 17:46:57',0,NULL,'2021-01-12 17:46:57',2,1,'大厦','01',NULL,'DEFAULT',NULL,'7e0e3902-129e-4b78-a624-0f3f56efb7b3');
INSERT INTO `iot_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `s_level`, `s_name`, `s_no`, `s_remark`, `s_type`, `parent_id`, `project_id`) VALUES ('a9b7c023-ca31-4587-afb4-d509e002014e',NULL,'2021-01-12 17:44:37',1,NULL,'2021-01-12 17:44:37',0,1,'测试专属','01',NULL,'DEFAULT',NULL,'123');
INSERT INTO `iot_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `s_level`, `s_name`, `s_no`, `s_remark`, `s_type`, `parent_id`, `project_id`) VALUES ('c422a744-e317-464c-bf9d-0b01fd724c45',NULL,'2021-01-12 17:38:49',1,NULL,'2021-01-12 17:38:49',0,2,'A栋','0101','测试11222','DEFAULT','49e19c29-9e64-4588-9e00-474b4152040b','7e0e3902-129e-4b78-a624-0f3f56efb7b3');
INSERT INTO `iot_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `s_level`, `s_name`, `s_no`, `s_remark`, `s_type`, `parent_id`, `project_id`) VALUES ('c5ace133-966c-47ca-a165-22a6076816f2',NULL,'2021-01-12 17:40:10',1,NULL,'2021-01-12 17:40:10',0,4,'会议室001','01010101','4445555666','DEFAULT','ed7c3396-c109-4113-8881-d5e82b079bc4','7e0e3902-129e-4b78-a624-0f3f56efb7b3');
INSERT INTO `iot_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `s_level`, `s_name`, `s_no`, `s_remark`, `s_type`, `parent_id`, `project_id`) VALUES ('d6b3def5-a26e-4c92-bb9e-d9f407cc5d83',NULL,'2021-01-12 17:00:50',0,NULL,'2021-01-12 17:00:50',5,1,'测试544','01',NULL,'DEFAULT',NULL,'123');
INSERT INTO `iot_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `s_level`, `s_name`, `s_no`, `s_remark`, `s_type`, `parent_id`, `project_id`) VALUES ('da4031f2-9c6b-4b54-9120-37894fb59bc5',NULL,'2021-01-12 18:04:42',0,NULL,'2021-01-12 18:04:42',2,3,'A层','010101','测试','DEFAULT','49d2c5c1-8889-404b-afcb-bb0c9e7f650f','7e0e3902-129e-4b78-a624-0f3f56efb7b3');
INSERT INTO `iot_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `s_level`, `s_name`, `s_no`, `s_remark`, `s_type`, `parent_id`, `project_id`) VALUES ('ed7c3396-c109-4113-8881-d5e82b079bc4',NULL,'2021-01-12 17:39:43',1,NULL,'2021-01-12 17:39:43',0,3,'F层','010101','33333','DEFAULT','c422a744-e317-464c-bf9d-0b01fd724c45','7e0e3902-129e-4b78-a624-0f3f56efb7b3');
INSERT INTO `iot_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `s_level`, `s_name`, `s_no`, `s_remark`, `s_type`, `parent_id`, `project_id`) VALUES ('f2fa5212-5b2d-4728-abfd-e80913135f4e',NULL,'2021-01-12 18:05:54',0,NULL,'2021-01-12 18:05:54',0,4,'会议室1','01010101','测试','DEFAULT','da4031f2-9c6b-4b54-9120-37894fb59bc5','7e0e3902-129e-4b78-a624-0f3f56efb7b3');
INSERT INTO `iot_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `s_level`, `s_name`, `s_no`, `s_remark`, `s_type`, `parent_id`, `project_id`) VALUES ('fbe9a8fe-635c-44c3-9fa0-2d64a30733fc',NULL,'2020-12-16 14:17:19',0,NULL,'2020-12-16 14:17:19',0,0,'启明',NULL,NULL,'ROOM',NULL,'123');
/*!40000 ALTER TABLE `iot_space` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-21 19:00:39
