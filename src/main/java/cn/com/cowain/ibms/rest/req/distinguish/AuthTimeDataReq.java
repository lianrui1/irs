package cn.com.cowain.ibms.rest.req.distinguish;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author wei.cheng
 * @date 2022/03/22 17:24
 */
@Data
@ApiModel("排班时间段")
public class AuthTimeDataReq {

    @ApiModelProperty(value = "权限类型：0工作时间段 1休假时间段", required = true)
    private String type;

    @ApiModelProperty(value = "人员工号", required = true)
    private String jobNo;

    @ApiModelProperty(value = "权限开始时间", required = true)
    private String startTime;

    @ApiModelProperty(value = "权限结束时间", required = true)
    private String endTime;
}
