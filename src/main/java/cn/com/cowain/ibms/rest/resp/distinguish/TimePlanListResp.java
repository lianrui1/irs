package cn.com.cowain.ibms.rest.resp.distinguish;

import cn.com.cowain.ibms.rest.resp.BaseListResp;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * 时间计划列表返回对象
 * @author Yang.Lee
 * @date 2021/3/11 13:30
 */
@Data
@ApiModel("时间计划列表数据输出对象")
public class TimePlanListResp extends BaseListResp {

}
