package cn.com.cowain.ibms.rest.resp.qrcode;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2021/4/19 10:01
 */
@Data
@Builder
@ApiModel("二维码通行权限分页查询结果对象")
public class QrcodeAccessRulePageResp {

    @ApiModelProperty(value = "权限ID", required = true, example = "123abc", position = 1)
    private String id;

    @ApiModelProperty(value = "项目名称", required = true, example = "昆山工厂", position = 2)
    private String projectName;

    @ApiModelProperty(value = "空间名称", example = "789hij", position = 3)
    private String spaceName;

    @ApiModelProperty(value = "设备名称", required = true, example = "人脸识别A", position = 4)
    private String magneticDeviceName;

    @ApiModelProperty(value = "设备位置", required = true, example = "汇金大厦", position = 5)
    private String deviceAddress;

    @ApiModelProperty(value = "人员组", required = true, position = 6)
    private String staffGroupNames;

    @ApiModelProperty(value = "门磁设备id", required = true, example = "123abc", position = 7)
    private String deviceId;
}
