package cn.com.cowain.ibms.rest.resp.device;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/2/28 16:41
 */
@Data
public class HwDevicesResp {
    @JsonProperty(value = "device_id")
    private String deviceId;

    @JsonProperty(value = "device_name")
    private String deviceName;

    @JsonProperty(value = "node_type")
    private String nodeType;

    @JsonProperty(value = "product_id")
    private String productId;

    @JsonProperty(value = "product_name")
    private String productName;

    @JsonProperty(value = "status")
    private String status;

    @JsonProperty(value = "node_id")
    private String nodeId;

    @JsonProperty(value = "gateway_id")
    private String gatewayId;

}
