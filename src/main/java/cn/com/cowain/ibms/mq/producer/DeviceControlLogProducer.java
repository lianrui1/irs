package cn.com.cowain.ibms.mq.producer;


import cn.com.cowain.ibms.mq.Topic;
import cn.com.cowain.ibms.rest.resp.device.DeviceControlLogMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Slf4j
@Component
public class DeviceControlLogProducer {

    @Resource
    private RocketMQTemplate rocketMQTemplate;

    public ServiceResult push(DeviceControlLogMessage deviceControlLogMessage) {

        String topic = Topic.get(Topic.DEVICE_CONTROL_LOG);
        String messageStr = JSON.toJSONString(deviceControlLogMessage);

        log.debug("推送数据到topic : {}. 数据： {}", topic, messageStr);
        Message<String> message = new GenericMessage<>(messageStr);

        SendResult sendResult = rocketMQTemplate.syncSend(topic + ":sendRecordMsg", message);

        if (!sendResult.getSendStatus().equals(SendStatus.SEND_OK)) {
            log.error("设备控制记录推送失败：" + sendResult);
            return ServiceResult.error(sendResult.toString());
        }

        return ServiceResult.ok();
    }
}
