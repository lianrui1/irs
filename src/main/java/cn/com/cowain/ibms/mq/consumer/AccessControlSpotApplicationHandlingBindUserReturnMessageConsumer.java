package cn.com.cowain.ibms.mq.consumer;

import cn.com.cowain.ibms.mq.Topic;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 门禁点授权申请通过请求iotc权限下发回调处理
 *
 * @author wei.cheng
 * @date 2022/03/22 13:26
 */
@Slf4j
@RocketMQMessageListener(
        topic = "${rocketmq.env}-" + Topic.ACCESS_APPLICATION_HANDLING_BIND_USER_TO_IOTC_MESSAGE_BACK,
        consumerGroup = "${rocketmq.group}-" + Topic.ACCESS_APPLICATION_HANDLING_BIND_USER_TO_IOTC_MESSAGE_BACK
)
@Component
public class AccessControlSpotApplicationHandlingBindUserReturnMessageConsumer implements RocketMQListener<String> {
//    @Resource
//    private AccessControlSpotApplicationRecordService accessControlSpotApplicationRecordService;

    @Override
    public void onMessage(@RequestBody String data) {
//        log.debug("AccessControlSpotApplicationHandlingBindUserReturnMessageConsumer onMessage, data:{}", data);
//        try {
//            // 解析数据
//
//            BizReturnMessage message = JSON.parseObject(data, BizReturnMessage.class);
//            // 保存结果
//            ServiceResult serviceResult = accessControlSpotApplicationRecordService.saveReturnMessage(message,
//                    Topic.ACCESS_APPLICATION_HANDLING_BIND_USER_TO_IOTC_MESSAGE_BACK);
//
//            log.debug("result:{}", serviceResult);
//        } catch (Exception e) {
//            log.error(e.getMessage(), e);
//        }
    }
}
