package cn.com.cowain.ibms.rest.req.iot;

import cn.com.cowain.ibms.rest.req.PageReq;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * @author feng
 * @title: MonitorReq
 * @projectName bims
 * @Date 2021/11/25 13:34
 */
@Data
public class MonitorReq extends PageReq {
    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "开始时间", required = true ,example = "2021-10-10 10:10:10")
    private LocalDateTime startTime;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "结束时间", required = true,example = "2021-10-10 10:10:10")
    private LocalDateTime endTime;

    @NotNull
    @ApiModelProperty(value = "空间id", required = true,example = "asdfajsdg")
    private String  spaceId;


}
