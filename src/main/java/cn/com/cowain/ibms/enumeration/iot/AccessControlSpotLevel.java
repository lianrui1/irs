package cn.com.cowain.ibms.enumeration.iot;

/**
 * 门禁等级
 *
 * @author Yang.Lee
 * @date 2021/11/19 13:52
 */
public enum AccessControlSpotLevel {

    NORMAL("普通门禁", 0),

    MANAGER("高管门禁", 1);

    private String name;

    private int code;

    AccessControlSpotLevel(String name, int code){
        this.name = name;
        this.code = code;
    }

    public String getName(){
        return this.name;
    }
}
