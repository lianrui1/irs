package cn.com.cowain.ibms.utils.aliyun;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 短消息发送工具类
 *
 * @author yang.lee
 * @date 2017年11月24日 下午5:51:30
 */
@Slf4j
@Component
public class SMSSender {

    //产品域名,开发者无需替换
    private static final String DOMAIN = "dysmsapi.aliyuncs.com";
    /**
     * api 版本，不可修改
     **/
    private static final String VERSION = "2017-05-25";

    /**
     * API 执行动作
     **/
    private static final String ACTION = "SendSms";

    private static final String REGIN_ID = "cn-hangzhou";

    /**
     * 应用keyID
     **/
    private static String ACCESS_KEY_ID;

    @Value("${aliyun.sms.key}")
    public void setAccessKeyId(String accessKeyId) {
        setStaticAccessKeyId(accessKeyId);
    }
    private static void setStaticAccessKeyId(String accessKeyId){
        SMSSender.ACCESS_KEY_ID = accessKeyId;
    }

    /**
     * 应用key secret
     **/
    private static String ACCESS_KEY_SECRET;

    @Value("${aliyun.sms.secret}")
    public void setAccessKeySecret(String accessKeySecret) {
        setStaticAccessKeySecret(accessKeySecret);
    }
    private static void setStaticAccessKeySecret(String accessKeySecret){
        SMSSender.ACCESS_KEY_SECRET = accessKeySecret;
    }

    /**
     * 短信签名
     **/
    private static String SIGN_NAME = "江苏科瑞恩";

    @Value("${aliyun.sms.signName}")
    public void setSignName(String signName) {
        setStaticSignName(signName);
    }
    private static void setStaticSignName(String name){
        SMSSender.SIGN_NAME = name;
    }

    private static IAcsClient client;

    /**
     * 初始化
     *
     * @author Yang.Lee
     * @date 2021/6/30 13:50
     **/
    @PostConstruct
    public void init() {
        DefaultProfile profile = DefaultProfile.getProfile(REGIN_ID, ACCESS_KEY_ID, ACCESS_KEY_SECRET);
        setClient(new DefaultAcsClient(profile));
    }

    private static void setClient(IAcsClient client){
        SMSSender.client = client;
    }

    /**
     * @param mobile       接收短信息的手机号码
     * @param templateCode 短消息模版ID
     * @param content      短消息内容，json字符串，详见阿里云短信API
     * @return 消息发送结果。若发生异常，则返回null
     * @author yang.lee
     * @date 2017年11月24日 下午6:14:32
     */
    public static void sendSms(String mobile, String templateCode, String content) {

        log.info("准备发送短消息，手机：{}， 模板：{}， 内容：{}", mobile, templateCode, content);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain(DOMAIN);
        request.setSysVersion(VERSION);
        request.setSysAction(ACTION);
        request.putQueryParameter("PhoneNumbers", mobile);
        request.putQueryParameter("SignName", SIGN_NAME);
        request.putQueryParameter("TemplateCode", templateCode);
        request.putQueryParameter("TemplateParam", content);
        try {
            CommonResponse response = client.getCommonResponse(request);
            if (!response.getHttpResponse().isSuccess()) {
                log.error("短消息发送失败！HTTP请求异常。响应内容：{}", response.getHttpResponse());
            } else {
                log.info("短消息发送成功");
            }
        } catch (ServerException e) {
            log.error(e.getMessage(), e);
        } catch (ClientException e) {
            log.error(e.getMessage(), e);
        }

    }
}
