package cn.com.cowain.ibms.mq.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 设备控制消息
 * @author Yang.Lee
 * @date 2021/6/17 13:54
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class IotDeviceControlMessage {

    private String deviceId;

    private String cmd;
}
