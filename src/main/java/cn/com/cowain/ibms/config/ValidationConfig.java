package cn.com.cowain.ibms.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * spring aop 配置信息类
 *
 * @author Hu Jingling
 * @since 2019-04-18
 */
@Slf4j
@Configuration
@ComponentScan("cn.com.cowain.ibms.aop")
@EnableAspectJAutoProxy
public class ValidationConfig {

    /**
     * 构造方法
     */
    public ValidationConfig() {
        log.debug("ValidationConfig ...");
    }

}
