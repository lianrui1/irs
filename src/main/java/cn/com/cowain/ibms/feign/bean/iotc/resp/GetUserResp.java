package cn.com.cowain.ibms.feign.bean.iotc.resp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 设备平台中人员是否存在返回参数
 *
 * @author: yanzy
 * @date: 2022/4/2 13:49
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GetUserResp {

    private Boolean isHk;

    private Boolean isKs;
}
