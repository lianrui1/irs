package cn.com.cowain.ibms.rest.resp.office;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/12 17:55
 */
@Data
@ApiModel("管理员响应对象")
public class SpaceAdminResp {

    @ApiModelProperty(value = "管理员工号")
    private String adminEmpNo;

    @ApiModelProperty(value = "管理员姓名")
    private String adminName;

    @ApiModelProperty(value = "管理员部门")
    private String adminDept;
}
