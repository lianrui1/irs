package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.ElevatorSpotDeviceRelation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ElevatorSpotDeviceRelationDao extends JpaRepository<ElevatorSpotDeviceRelation, String>, JpaSpecificationExecutor<ElevatorSpotDeviceRelation> {
    Optional<ElevatorSpotDeviceRelation> findByElevatorSpotIdAndDeviceId(String id, String deviceId);

    /**
     * 批量查询梯控点的ElevatorSpotDeviceRelation列表
     *
     * @param elevtorSpotIdList
     * @return
     */
    List<ElevatorSpotDeviceRelation> findAllByElevatorSpotIdIn(List<String> elevtorSpotIdList);
}
