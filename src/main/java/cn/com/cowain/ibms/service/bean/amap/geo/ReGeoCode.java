package cn.com.cowain.ibms.service.bean.amap.geo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * 逆地理编码
 *
 * @author Yang.Lee
 * @date 2021/4/20 13:50
 */
@Data
public class ReGeoCode {

    /**
     * 结构化地址信息
     **/
    @JSONField(name = "formatted_address")
    private String formattedAddress;

    /**
     * 地址元素
     **/
    private AddressComponent addressComponent;

    /**
     * poi信息列表
     **/
    private List<Poi> pois;

    /**
     * 道路信息列表
     **/
    private List<Road> roads;

    /**
     * 道路交叉口列表
     **/
    @JSONField(name = "roadinters")
    private List<RoadInter> roadInters;

    /**
     * aoi信息列表
     **/
    private List<Aoi> aois;
}
