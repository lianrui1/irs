package cn.com.cowain.ibms.entity.space;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author Yang.Lee
 * @date 2021/11/10 14:41
 */
@Data
@Entity
@Table(name = "ibms_project_admin")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_project_admin", comment = "项目管理员表")
@Where(clause = "is_del=0")
public class ProjectAdmin extends BaseEntity {

    @Column(name = "admin_empNo", columnDefinition = "varchar(20) COMMENT '管理员工号'")
    private String adminEmpNo;

    @ManyToOne
    @JoinColumn(name = "project_id", nullable = false, columnDefinition = "varchar(36) COMMENT '项目id'")
    private Project project;
}
