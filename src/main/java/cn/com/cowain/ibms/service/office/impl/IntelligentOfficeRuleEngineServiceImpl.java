package cn.com.cowain.ibms.service.office.impl;

import cn.com.cowain.ibms.dao.device.DeviceModeStatusDao;
import cn.com.cowain.ibms.dao.device.DeviceWorkingModeDao;
import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.dao.iot.SpaceDao;
import cn.com.cowain.ibms.dao.office.IntelligentOfficeDao;
import cn.com.cowain.ibms.dao.office.IntelligentOfficeDeviceDao;
import cn.com.cowain.ibms.dao.office.IntelligentOfficeRuleEngineDao;
import cn.com.cowain.ibms.dao.space.SpaceAdminDao;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.office.*;
import cn.com.cowain.ibms.entity.space.SpaceAdmin;
import cn.com.cowain.ibms.enumeration.RuleEngIneDetailTriggerCondition;
import cn.com.cowain.ibms.enumeration.RuleEngIneDetailTriggerDeviceCondition;
import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.enumeration.iot.UsableStatus;
import cn.com.cowain.ibms.feign.iotc.IOTCApi;
import cn.com.cowain.ibms.feign.iotc.bean.*;
import cn.com.cowain.ibms.feign.iotc.enumeration.MethodCode;
import cn.com.cowain.ibms.feign.iotc.enumeration.ModuleCode;
import cn.com.cowain.ibms.rest.req.iot.GroupBindingReq;
import cn.com.cowain.ibms.rest.req.office.*;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.office.*;
import cn.com.cowain.ibms.rest.resp.working_mode.DeviceModeStatusDetailResp;
import cn.com.cowain.ibms.rest.resp.working_mode.WorkingModeDetailResp;
import cn.com.cowain.ibms.service.bean.DoorMagneticHWParasReq;
import cn.com.cowain.ibms.service.bean.DoorMagneticHWReq;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.device.HwProductsService;
import cn.com.cowain.ibms.service.office.IntelligentOfficeDeviceService;
import cn.com.cowain.ibms.service.office.IntelligentOfficeRuleEngineService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.TokenUtils;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.sfp.comm.utils.JwtUtil;
import cn.com.cowain.sfp.xunfei.util.EncryptUtil;
import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.*;

import static cn.com.cowain.ibms.enumeration.iot.DeviceType.*;


/**
 * IntelligentOfficeRuleEngineServiceImpl
 *
 * @author: yanzy
 * @date: 2022/2/7 09:41
 */
@Slf4j
@Service
public class IntelligentOfficeRuleEngineServiceImpl implements IntelligentOfficeRuleEngineService {

    @Resource
    private IntelligentOfficeDao intelligentOfficeDao;

    @Resource
    private DeviceDao iotDeviceDao;

    @Resource
    private IntelligentOfficeDeviceDao intelligentOfficeDeviceDao;

    @Resource
    private SpaceAdminDao spaceAdminDao;

    @Resource
    private IntelligentOfficeRuleEngineDao intelligentOfficeRuleEngineDao;

    @Resource
    private DeviceDao deviceDao;

    @Resource
    private DeviceWorkingModeDao workingModeDao;

    @Resource
    private DeviceModeStatusDao modeStatusDao;

    @Resource
    private HwProductsService productsService;

    @Resource
    private IOTCApi iotcApi;

    @Resource
    private IntelligentOfficeDeviceService intelligentOfficeDeviceService;

    @Resource
    private SpaceDao spaceDao;


    @Override
    public List<ControlledDeviceResp> findControlledDeviceAll(String officeId, String hrId) {

        List<ControlledDeviceResp> listResp = new ArrayList<>();

        Optional<IntelligentOffice> intelligentOffice = intelligentOfficeDao.findById(officeId);
        if (intelligentOffice.isEmpty()) {
            log.error("未查到该办公室");
            return listResp;
        }

        //查询该空间下是否存在子空间
        List<Space> spaceList = spaceDao.findAllByIdOrParentId(intelligentOffice.get().getSpace().getId(), intelligentOffice.get().getSpace().getId());

        List<String> spaceIds = new ArrayList<>();
        //所有空间id
        spaceList.forEach(it -> spaceIds.add(it.getId()));
        //查询该空间下所有的可控设备
        List<IotDevice> controlList = iotDeviceDao.findBySpaceIdInAndSpaceControlAndStatusOrderByCreatedTimeDesc(spaceIds, 1, 1);

        if (CollectionUtils.isEmpty(controlList)) {
            return listResp;
        }

        Map<String,IntelligentOfficeDevice> deviceIdToIntelligentOfficeMap = intelligentOfficeDeviceService.
                getDeviceIdToIntelligentOfficeDeviceMapOfUserHrIdAndOfficeId(hrId, intelligentOffice.get());

        for (IotDevice device : controlList) {
            //根据设备id、工号和办事id获取设备信息
            IntelligentOfficeDevice officeDevice = deviceIdToIntelligentOfficeMap.get(device.getId());
            if (Objects.nonNull(officeDevice) && UsableStatus.ENABLE.equals(officeDevice.getUsableStatus())) {
                listResp.add(ControlledDeviceResp.convert(device, officeDevice));
            }
        }
        return listResp;
    }


    @Override
    public List<SensorDeviceResp> findSensorDevice(String officeId, String hrId) {
        Optional<IntelligentOffice> intelligentOffice = intelligentOfficeDao.findById(officeId);

        List<SensorDeviceResp> listResp = new ArrayList<>();

        if (intelligentOffice.isEmpty()) {
            log.error("未查到该办公室");
            return listResp;
        }

        //查询该空间下是否存在子空间
        List<Space> spaceList = spaceDao.findAllByIdOrParentId(intelligentOffice.get().getSpace().getId(), intelligentOffice.get().getSpace().getId());

        List<String> spaceIds = new ArrayList<>();
        //所有空间id
        spaceList.forEach(it -> spaceIds.add(it.getId()));

        List<DeviceType> collect = Arrays.asList(ILLUMINATION_SENSING, TEMPERATURE_AND_HUMIDITY_SENSOR, TEMPERATURE_AND_HUMIDITY_AND_ILLUMINATION_SENSOR, NOISE_SENSOR, PM_SENSOR, SENSOR_BODY_SENSING);
        List<IotDevice> deviceTypeIn = iotDeviceDao.findBySpaceIdInAndStatusAndDeviceTypeInOrderByCreatedTimeDesc(spaceIds, 1, collect);

        Map<String,IntelligentOfficeDevice> deviceIdToIntelligentOfficeMap = intelligentOfficeDeviceService.
                getDeviceIdToIntelligentOfficeDeviceMapOfUserHrIdAndOfficeId(hrId, intelligentOffice.get());

        for (IotDevice deviceUser : deviceTypeIn) {
            //根据设备id、工号和办事id获取设备信息
            IntelligentOfficeDevice byDeviceId = deviceIdToIntelligentOfficeMap.get(deviceUser.getId());
            if (byDeviceId != null){
                if (UsableStatus.ENABLE.equals(byDeviceId.getUsableStatus())) {
                    SensorDeviceResp sensorDeviceResp = new SensorDeviceResp();
                    sensorDeviceResp.setDeviceId(deviceUser.getId());
                    sensorDeviceResp.setDeviceName(deviceUser.getDeviceName());
                    sensorDeviceResp.setDeviceType(deviceUser.getDeviceType());
                    sensorDeviceResp.setDeviceTypeName(deviceUser.getDeviceType().getName());
                    sensorDeviceResp.setCreatedTime(deviceUser.getCreatedTime());

                    //判断是否有别名
                    if (byDeviceId.getDeviceAlisa() != null) {
                        sensorDeviceResp.setDeviceName(byDeviceId.getDeviceAlisa());
                    } else {
                        sensorDeviceResp.setDeviceName(deviceUser.getDeviceName());
                    }
                    //存入子别名
                    if (StringUtils.isNotBlank(byDeviceId.getDeviceAlisaProperties())){
                        sensorDeviceResp.setAdditionalName(JSON.parseObject(byDeviceId.getDeviceAlisaProperties(), DeviceNameEditReq.AdditionalName.class));
                    }
                    listResp.add(sensorDeviceResp);
                }
            }else {
                SensorDeviceResp sensorDeviceResp = new SensorDeviceResp();
                sensorDeviceResp.setDeviceId(deviceUser.getId());
                sensorDeviceResp.setDeviceName(deviceUser.getDeviceName());
                sensorDeviceResp.setDeviceType(deviceUser.getDeviceType());
                sensorDeviceResp.setDeviceTypeName(deviceUser.getDeviceType().getName());
                sensorDeviceResp.setCreatedTime(deviceUser.getCreatedTime());
                sensorDeviceResp.setDeviceName(deviceUser.getDeviceName());
                listResp.add(sensorDeviceResp);
            }
        }
        return listResp;

    }


    @Override
    @Transactional
    public ServiceResult pcCreatRuleEngine(DefaultCreateRuleChainReq req) {

        //根据会议室Id获取会议室信息
        Optional<IntelligentOffice> intelligentOffice = intelligentOfficeDao.findById(req.getOfficeId());
        if (intelligentOffice.isEmpty()) {
            return ServiceResult.error("办公室不存在");
        }

        //判断是否有相同的联动规则名称
        Optional<IntelligentOfficeRuleEngine> byName = intelligentOfficeRuleEngineDao.findByNameAndIntelligentOfficeId(req.getName(), req.getOfficeId());
        if (!byName.isEmpty()) {
            return ServiceResult.error("添加失败，联动规则名称已经存在");
        }

        //添加规则
        IntelligentOfficeRuleEngine intelligentOfficeRuleEngine = new IntelligentOfficeRuleEngine();
        intelligentOfficeRuleEngine.setLogo(req.getImgUrl());
        intelligentOfficeRuleEngine.setName(req.getName());
        intelligentOfficeRuleEngine.setUsableStatus(req.getUsableStatus());
        intelligentOfficeRuleEngine.setDescribe(req.getPurpose());
        intelligentOfficeRuleEngine.setIntelligentOffice(intelligentOffice.get());

        intelligentOfficeRuleEngine.setTrigger(JSON.toJSONString(req.getTriggerReqs()));
        intelligentOfficeRuleEngine.setTriggerCondition(req.getTriggerCondition());

        intelligentOfficeRuleEngine.setActuator(JSON.toJSONString(req.getActuatorReqs()));
        intelligentOfficeRuleEngine.setEffectiveType(JSON.toJSONString(req.getEffectiveTypeReq()));
        //保存
        intelligentOfficeRuleEngineDao.save(intelligentOfficeRuleEngine);


        //规则链创建接收对象
        CreateRuleChainReq createRuleChainReq = new CreateRuleChainReq();

        //根据spaceId查询管理员
        List<SpaceAdmin> byAdminEmpNo = spaceAdminDao.findBySpaceId(req.getSpaceId());
        for (SpaceAdmin spaceAdmin : byAdminEmpNo) {
            //取出管理员名称
            String adminName = spaceAdmin.getAdminName();
            //添加创建人名称
            createRuleChainReq.setUserName(adminName);
            //添加规则链Id
            createRuleChainReq.setRuleChainId(intelligentOfficeRuleEngine.getId());
            //添加规则链名称
            String s = EncryptUtil.md5(intelligentOffice.get().getId());
            createRuleChainReq.setRuleChainName(req.getName()+s);
        }

        //判断条件
        if (RuleEngIneDetailTriggerCondition.WHOLE.equals(req.getTriggerCondition())) {
            createRuleChainReq.setIsAll(1);
        } else {
            createRuleChainReq.setIsAll(2);
        }

        //创建 传感器执行条件
        List<PropertiesReq> propertiesReqs = new ArrayList<>();

        //创建 人体传感器执行条件 的集合
        List<BodySensorReq> bodySensorReqs = new ArrayList<>();

        //执行条件
        for (RuleEngIneDetailTriggerReq ruleEngIneDetailTriggerReq : req.getTriggerReqs()) {

            //定时
            if (ruleEngIneDetailTriggerReq.getTiming() != null) {
                //定时小时
                createRuleChainReq.setHour(ruleEngIneDetailTriggerReq.getTiming().getHour());
                //定时分钟
                createRuleChainReq.setMinutes(ruleEngIneDetailTriggerReq.getTiming().getMinute());
            }

            PropertiesReq propertiesReq = new PropertiesReq();

            List<Conditions> conditionsList = new ArrayList<>();
            //传感器设备不为空
            if (ruleEngIneDetailTriggerReq.getSensorReqs() != null) {

                for (RuleEngIneDetailTriggerSensorReq ruleEngIneDetailTriggerSensorReq : ruleEngIneDetailTriggerReq.getSensorReqs()) {

                    //创建 规则判断条件值 对象
                    Conditions condition = new Conditions();

                    //创建 人体传感器执行条件 对象
                    BodySensorReq bodySensorReq = new BodySensorReq();

                    String key = ruleEngIneDetailTriggerSensorReq.getKey();

                    String deviceIdMd5 = EncryptUtil.md5(ruleEngIneDetailTriggerReq.getDeviceId());
                    switch (key) {
                        case "Noise":
                            condition.setKey(key + deviceIdMd5);
                            if ("1".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("50");
                                condition.setCompare("<");
                            } else if ("2".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("70");
                                condition.setCompare("<");
                            } else if ("3".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("90");
                                condition.setCompare("<");
                            } else {
                                condition.setValue("90");
                                condition.setCompare(">");
                            }
                            conditionsList.add(condition);
                            break;

                        case "PM2.5":
                            condition.setKey("PM2_5" + deviceIdMd5);
                            if ("1".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("75");
                                condition.setCompare("<");
                            } else if ("2".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("115");
                                condition.setCompare("<");
                            } else if ("3".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("150");
                                condition.setCompare("<");
                            } else {
                                condition.setValue("150");
                                condition.setCompare(">");
                            }
                            conditionsList.add(condition);
                            break;

                        case "PM10":
                            condition.setKey(key + deviceIdMd5);
                            if ("1".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("50");
                                condition.setCompare("<");
                            } else if ("2".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("100");
                                condition.setCompare("<");
                            } else if ("3".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("150");
                                condition.setCompare("<");
                            } else {
                                condition.setValue("150");
                                condition.setCompare(">");
                            }
                            conditionsList.add(condition);
                            break;

                        case "people":
                            bodySensorReq.setKey(key + deviceIdMd5);
                            bodySensorReq.setTimeKey("timeKey" + deviceIdMd5);
                            bodySensorReq.setTimeValue(Integer.valueOf(ruleEngIneDetailTriggerSensorReq.getValue()));

                            if (RuleEngIneDetailTriggerDeviceCondition.HAVE.equals(ruleEngIneDetailTriggerSensorReq.getDeviceCondition())) {
                                bodySensorReq.setValue("true");
                            } else if (RuleEngIneDetailTriggerDeviceCondition.NOTHING.equals(ruleEngIneDetailTriggerSensorReq.getDeviceCondition())) {
                                bodySensorReq.setValue("false");
                            }
                            bodySensorReqs.add(bodySensorReq);
                            break;

                        default:
                            condition.setKey(key + deviceIdMd5);
                            condition.setValue(ruleEngIneDetailTriggerSensorReq.getValue());
                            if (RuleEngIneDetailTriggerDeviceCondition.GREATER.equals(ruleEngIneDetailTriggerSensorReq.getDeviceCondition())) {
                                condition.setCompare(">");
                            } else {
                                condition.setCompare("<");
                            }
                            conditionsList.add(condition);
                            break;
                    }
                }
                propertiesReq.setConditionsList(conditionsList);
            }
            if (!CollUtil.isEmpty(conditionsList)) {
                propertiesReqs.add(propertiesReq);
            }
        }

        createRuleChainReq.setPropertiesList(propertiesReqs);
        createRuleChainReq.setBodySensorList(bodySensorReqs);

        if (req.getEffectiveTypeReq().getEffectiveTypeTimingTimeReq() != null) {
            createRuleChainReq.setStartTimeHour(req.getEffectiveTypeReq().getEffectiveTypeTimingTimeReq().getStartTime().getHour());
            createRuleChainReq.setStartTimeMinutes(req.getEffectiveTypeReq().getEffectiveTypeTimingTimeReq().getStartTime().getMinute());
            createRuleChainReq.setEndTimeHour(req.getEffectiveTypeReq().getEffectiveTypeTimingTimeReq().getEndTime().getHour());
            createRuleChainReq.setEndTimeMinutes(req.getEffectiveTypeReq().getEffectiveTypeTimingTimeReq().getEndTime().getMinute());

            StringBuilder stringBuilder = new StringBuilder();
            String resultString = "";
            for (String w : req.getEffectiveTypeReq().getEffectiveTypeTimingTimeReq().getWeeks()) {
                stringBuilder.append(w);
            }
            resultString = stringBuilder.toString();
            createRuleChainReq.setDays(resultString);
        }

        GroupBindingReq groupBindingReq = new GroupBindingReq();
        groupBindingReq.setChainSite("cloud");
        groupBindingReq.setChannelType("3");
        groupBindingReq.setJsonArg(JSON.toJSONString(createRuleChainReq));
        groupBindingReq.setMethodCode(MethodCode.CREATE_RULECHAIN.getName());
        groupBindingReq.setModuleCode(ModuleCode.HW.getName());
        JsonResult<Object> objectJsonResult = iotcApi.postToConn(groupBindingReq);
        if (objectJsonResult.getStatus() == 0) {

            log.error("默认规则创建失败原因：" + objectJsonResult.getMsg() + "code" + objectJsonResult.getCode());
            // 默认规则创建失败，手动回滚事务,返回异常
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return ServiceResult.error(objectJsonResult.getMsg());
        }
        //返回Id
        IdResp idResp = new IdResp();
        idResp.setId(intelligentOfficeRuleEngine.getId());
        return ServiceResult.ok(idResp);

    }

    @Override
    @Transactional
    public ServiceResult creatRuleEngine(RuleEngineDetailReq req) {
        //从token中解析员工工号
        String hrId = JwtUtil.getHrId(TokenUtils.getToken());

        //根据会议室Id获取会议室信息
        Optional<IntelligentOffice> intelligentOffice = intelligentOfficeDao.findById(req.getOfficeId());
        if (intelligentOffice.isEmpty()) {
            return ServiceResult.error("办公室不存在");
        }

        //判断创建人是否是该空间管理员
        Optional<SpaceAdmin> spaceAdminOptional = spaceAdminDao.findBySpaceIdAndAdminEmpNo(intelligentOffice.get().getSpace().getId(), hrId);
        if (spaceAdminOptional.isEmpty()) {
            return ServiceResult.error("添加失败，无权限");
        }

        //判断是否有相同的联动规则名称
        Optional<IntelligentOfficeRuleEngine> byName = intelligentOfficeRuleEngineDao.findByNameAndIntelligentOfficeId(req.getName(), req.getOfficeId());
        if (!byName.isEmpty()) {
            return ServiceResult.error("添加失败，联动规则名称已经存在");
        }

        //添加规则
        IntelligentOfficeRuleEngine intelligentOfficeRuleEngine = new IntelligentOfficeRuleEngine();
        intelligentOfficeRuleEngine.setLogo(req.getImgUrl());
        intelligentOfficeRuleEngine.setName(req.getName());
        intelligentOfficeRuleEngine.setUsableStatus(req.getUsableStatus());
        intelligentOfficeRuleEngine.setDescribe(req.getPurpose());
        intelligentOfficeRuleEngine.setIntelligentOffice(intelligentOffice.get());

        intelligentOfficeRuleEngine.setTrigger(JSON.toJSONString(req.getTriggerReqs()));
        intelligentOfficeRuleEngine.setTriggerCondition(req.getTriggerCondition());

        intelligentOfficeRuleEngine.setActuator(JSON.toJSONString(req.getActuatorReqs()));
        intelligentOfficeRuleEngine.setEffectiveType(JSON.toJSONString(req.getEffectiveTypeReq()));
        //保存
        intelligentOfficeRuleEngineDao.save(intelligentOfficeRuleEngine);


        //规则链创建接收对象
        CreateRuleChainReq createRuleChainReq = new CreateRuleChainReq();

        //根据hrid查询管理员
        List<SpaceAdmin> byAdminEmpNo = spaceAdminDao.findByAdminEmpNo(hrId);

        for (SpaceAdmin spaceAdmin : byAdminEmpNo) {
            //取出管理员名称
            String adminName = spaceAdmin.getAdminName();
            //添加创建人名称
            createRuleChainReq.setUserName(adminName);
            //添加规则链Id
            createRuleChainReq.setRuleChainId(intelligentOfficeRuleEngine.getId());
            //添加规则链名称
            String s = EncryptUtil.md5(intelligentOffice.get().getId());
            createRuleChainReq.setRuleChainName(req.getName()+s);
        }

        //判断条件
        if (RuleEngIneDetailTriggerCondition.WHOLE.equals(req.getTriggerCondition())) {
            createRuleChainReq.setIsAll(1);
        } else {
            createRuleChainReq.setIsAll(2);
        }

        //创建 传感器执行条件
        List<PropertiesReq> propertiesReqs = new ArrayList<>();

        //创建 人体传感器执行条件 的集合
        List<BodySensorReq> bodySensorReqs = new ArrayList<>();

        //执行条件
        for (RuleEngIneDetailTriggerReq ruleEngIneDetailTriggerReq : req.getTriggerReqs()) {

            //定时
            if (ruleEngIneDetailTriggerReq.getTiming() != null) {
                //定时小时
                createRuleChainReq.setHour(ruleEngIneDetailTriggerReq.getTiming().getHour());
                //定时分钟
                createRuleChainReq.setMinutes(ruleEngIneDetailTriggerReq.getTiming().getMinute());
            }

            PropertiesReq propertiesReq = new PropertiesReq();

            List<Conditions> conditionsList = new ArrayList<>();
            //传感器设备不为空
            if (ruleEngIneDetailTriggerReq.getSensorReqs() != null) {

                for (RuleEngIneDetailTriggerSensorReq ruleEngIneDetailTriggerSensorReq : ruleEngIneDetailTriggerReq.getSensorReqs()) {

                    //创建 规则判断条件值 对象
                    Conditions condition = new Conditions();

                    //创建 人体传感器执行条件 对象
                    BodySensorReq bodySensorReq = new BodySensorReq();

                    String key = ruleEngIneDetailTriggerSensorReq.getKey();

                    String deviceIdMd5 = EncryptUtil.md5(ruleEngIneDetailTriggerReq.getDeviceId());
                    switch (key) {
                        case "Noise":
                            condition.setKey(key + deviceIdMd5);
                            if ("1".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("50");
                                condition.setCompare("<");
                            } else if ("2".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("70");
                                condition.setCompare("<");
                            } else if ("3".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("90");
                                condition.setCompare("<");
                            } else {
                                condition.setValue("90");
                                condition.setCompare(">");
                            }
                            conditionsList.add(condition);
                            break;

                        case "PM2.5":
                            condition.setKey("PM2_5" + deviceIdMd5);
                            if ("1".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("75");
                                condition.setCompare("<");
                            } else if ("2".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("115");
                                condition.setCompare("<");
                            } else if ("3".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("150");
                                condition.setCompare("<");
                            } else {
                                condition.setValue("150");
                                condition.setCompare(">");
                            }
                            conditionsList.add(condition);
                            break;

                        case "PM10":
                            condition.setKey(key + deviceIdMd5);
                            if ("1".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("50");
                                condition.setCompare("<");
                            } else if ("2".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("100");
                                condition.setCompare("<");
                            } else if ("3".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("150");
                                condition.setCompare("<");
                            } else {
                                condition.setValue("150");
                                condition.setCompare(">");
                            }
                            conditionsList.add(condition);
                            break;

                        case "people":
                            bodySensorReq.setKey(key + deviceIdMd5);
                            bodySensorReq.setTimeKey("timeKey" + deviceIdMd5);
                            bodySensorReq.setTimeValue(Integer.valueOf(ruleEngIneDetailTriggerSensorReq.getValue()));

                            if (RuleEngIneDetailTriggerDeviceCondition.HAVE.equals(ruleEngIneDetailTriggerSensorReq.getDeviceCondition())) {
                                bodySensorReq.setValue("true");
                            } else if (RuleEngIneDetailTriggerDeviceCondition.NOTHING.equals(ruleEngIneDetailTriggerSensorReq.getDeviceCondition())) {
                                bodySensorReq.setValue("false");
                            }
                            bodySensorReqs.add(bodySensorReq);
                            break;

                        default:
                            condition.setKey(key + deviceIdMd5);
                            condition.setValue(ruleEngIneDetailTriggerSensorReq.getValue());
                            if (RuleEngIneDetailTriggerDeviceCondition.GREATER.equals(ruleEngIneDetailTriggerSensorReq.getDeviceCondition())) {
                                condition.setCompare(">");
                            } else {
                                condition.setCompare("<");
                            }
                            conditionsList.add(condition);
                            break;
                    }
                }
                propertiesReq.setConditionsList(conditionsList);
            }
            if (!CollUtil.isEmpty(conditionsList)) {
                propertiesReqs.add(propertiesReq);
            }
        }

        createRuleChainReq.setPropertiesList(propertiesReqs);
        createRuleChainReq.setBodySensorList(bodySensorReqs);

        if (req.getEffectiveTypeReq().getEffectiveTypeTimingTimeReq() != null) {
            createRuleChainReq.setStartTimeHour(req.getEffectiveTypeReq().getEffectiveTypeTimingTimeReq().getStartTime().getHour());
            createRuleChainReq.setStartTimeMinutes(req.getEffectiveTypeReq().getEffectiveTypeTimingTimeReq().getStartTime().getMinute());
            createRuleChainReq.setEndTimeHour(req.getEffectiveTypeReq().getEffectiveTypeTimingTimeReq().getEndTime().getHour());
            createRuleChainReq.setEndTimeMinutes(req.getEffectiveTypeReq().getEffectiveTypeTimingTimeReq().getEndTime().getMinute());

            StringBuilder stringBuilder = new StringBuilder();
            String resultString = "";
            for (String w : req.getEffectiveTypeReq().getEffectiveTypeTimingTimeReq().getWeeks()) {
                stringBuilder.append(w);
            }
            resultString = stringBuilder.toString();
            createRuleChainReq.setDays(resultString);
        }

        GroupBindingReq groupBindingReq = new GroupBindingReq();
        groupBindingReq.setChainSite("cloud");
        groupBindingReq.setChannelType("3");
        groupBindingReq.setJsonArg(JSON.toJSONString(createRuleChainReq));
        groupBindingReq.setMethodCode(MethodCode.CREATE_RULECHAIN.getName());
        groupBindingReq.setModuleCode(ModuleCode.HW.getName());
        JsonResult<Object> objectJsonResult = iotcApi.postToConn(groupBindingReq);
        if (objectJsonResult.getStatus() == 0) {

            log.error("规则创建失败原因：" + objectJsonResult.getMsg() + "code" + objectJsonResult.getCode());
            // 联动规则创建失败，手动回滚事务,返回异常
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return ServiceResult.error(objectJsonResult.getMsg());
        }
        //返回Id
        IdResp idResp = new IdResp();
        idResp.setId(intelligentOfficeRuleEngine.getId());
        return ServiceResult.ok(idResp);

    }

    @Override
    public ServiceResult findRuleEngineDetail(String id, String hrId) {

        //判断联动规则是否存在
        Optional<IntelligentOfficeRuleEngine> ruleEngineOp = intelligentOfficeRuleEngineDao.findById(id);
        if (ruleEngineOp.isEmpty()) {
            return ServiceResult.error("联动规则不存在");
        }
        IntelligentOfficeRuleEngine ruleEngine = ruleEngineOp.get();

        //添加详情
        RuleEngIneDetailResp ruleEngIneDetailResp = new RuleEngIneDetailResp();
        ruleEngIneDetailResp.setId(ruleEngine.getId());
        ruleEngIneDetailResp.setImgUrl(ruleEngine.getLogo());
        ruleEngIneDetailResp.setName(ruleEngine.getName());
        ruleEngIneDetailResp.setPurpose(ruleEngine.getDescribe());
        ruleEngIneDetailResp.setTriggerCondition(ruleEngine.getTriggerCondition());

        //触发条件详情
        List<RuleEngIneDetailTriggerResp> triggerResps = JSON.parseArray(ruleEngine.getTrigger(), RuleEngIneDetailTriggerResp.class);
        ruleEngIneDetailResp.setTriggerReqs(triggerResps);

        //获取用户对会议下所有设备的别名
       Map<String,IntelligentOfficeDevice> deviceIdToIntelligentOfficeMap = intelligentOfficeDeviceService.
                getDeviceIdToIntelligentOfficeDeviceMapOfUserHrIdAndOfficeId(hrId, ruleEngine.getIntelligentOffice());

        //获取设备id
        for (RuleEngIneDetailTriggerResp triggerResp : triggerResps) {

            SensorDeviceResp deviceResp = new SensorDeviceResp();

            if (triggerResp.getDeviceId() != null) {
                Optional<IotDevice> deviceById = deviceDao.findById(triggerResp.getDeviceId());
                if (deviceById.isEmpty()) {
                    return ServiceResult.error("设备id有误", ErrConst.E01);
                }
                IotDevice device = deviceById.get();
                deviceResp.setDeviceType(device.getDeviceType());
                deviceResp.setDeviceTypeName(device.getDeviceType().getName());

                //查询别名
                IntelligentOfficeDevice intelligentOfficeDevice = deviceIdToIntelligentOfficeMap.get(triggerResp.getDeviceId());
                if (intelligentOfficeDevice != null) {
                    if (intelligentOfficeDevice.getDeviceAlisa() != null) {
                        deviceResp.setDeviceName(intelligentOfficeDevice.getDeviceAlisa());
                    } else {
                        deviceResp.setDeviceName(device.getDeviceName());
                    }

                    deviceResp.setAdditionalName(JSON.parseObject(intelligentOfficeDevice.getDeviceAlisaProperties(), DeviceNameEditReq.AdditionalName.class));
                }
            }
            triggerResp.setSensorDeviceResp(deviceResp);
        }

        //执行任务详情
        List<RuleEngIneDetailActuatorResp> actuatorResps = JSON.parseArray(ruleEngine.getActuator(), RuleEngIneDetailActuatorResp.class);
        ruleEngIneDetailResp.setActuatorReqs(actuatorResps);

        //获取设备id
        for (RuleEngIneDetailActuatorResp actuatorResp : actuatorResps) {

            ControlledDeviceResp deviceResp = new ControlledDeviceResp();

            //执行设备
            if (actuatorResp.getDeviceId() != null) {
                Optional<IotDevice> deviceById = deviceDao.findById(actuatorResp.getDeviceId());
                if (deviceById.isEmpty()) {
                    return ServiceResult.error("设备id有误", ErrConst.E01);
                }
                IotDevice device = deviceById.get();
                deviceResp.setDeviceType(device.getDeviceType());
                deviceResp.setDeviceTypeName(device.getDeviceType().getName());

                //查询别名
                IntelligentOfficeDevice intelligentOfficeDevice = deviceIdToIntelligentOfficeMap.get(actuatorResp.getDeviceId());

                if (intelligentOfficeDevice != null) {
                    deviceResp.setDeviceName(StringUtils.isEmpty(intelligentOfficeDevice.getDeviceAlisa()) ? device.getDeviceName()
                            : intelligentOfficeDevice.getDeviceAlisa());
                    deviceResp.setAdditionalName(StringUtils.isEmpty(intelligentOfficeDevice.getDeviceAlisaProperties())
                            ? DeviceNameEditReq.AdditionalName.empty()
                            : JSON.parseObject(intelligentOfficeDevice.getDeviceAlisaProperties(), DeviceNameEditReq.AdditionalName.class));
                }
            }
            actuatorResp.setControlledDeviceResp(deviceResp);

            //场景
            if (actuatorResp.getWorkingModeId() != null) {
                Optional<DeviceWorkingMode> workingModeOp = workingModeDao.findById(actuatorResp.getWorkingModeId());
                if (workingModeOp.isEmpty()) {
                    return ServiceResult.error("id有误,模式不存在");
                }
                DeviceWorkingMode workingMode = workingModeOp.get();

                // 详情返回对象
                WorkingModeDetailResp workingModeDetailResp = new WorkingModeDetailResp();
                workingModeDetailResp.setId(workingMode.getId());
                workingModeDetailResp.setImgUrl(workingMode.getImg());
                workingModeDetailResp.setName(workingMode.getName());
                workingModeDetailResp.setSpaceId(workingMode.getSpace().getId());
                workingModeDetailResp.setSpaceName(workingMode.getSpace().getName());
                workingModeDetailResp.setIsDefault(workingMode.getIsDefault());

                // 模式下设备
                List<DeviceModeStatusDetailResp> detailResps = new ArrayList<>();
                List<DeviceModeStatus> modeStatusList = modeStatusDao.findByModeId(workingMode.getId());
                modeStatusList.forEach(it -> {

                    DeviceModeStatusDetailResp statusDetailResp = new DeviceModeStatusDetailResp();
                    IotDevice device = it.getDevice();
                    statusDetailResp.setId(device.getId());
                    statusDetailResp.setDeviceName(device.getDeviceName());
                    if ("on".equals(it.getStatus()) || "open".equals(it.getStatus())) {
                        statusDetailResp.setStatus(true);
                    } else {
                        statusDetailResp.setStatus(false);
                    }
                    statusDetailResp.setDeviceType(device.getDeviceType());
                    statusDetailResp.setHwDeviceId(device.getHwDeviceId());
                    detailResps.add(statusDetailResp);
                });

                workingModeDetailResp.setDetailResps(detailResps);
                actuatorResp.setWorkingModeDetailResp(workingModeDetailResp);
            }

        }

        ruleEngIneDetailResp.setUsableStatus(ruleEngine.getUsableStatus());

        ruleEngIneDetailResp.setEffectiveTypeReq(JSON.parseObject(ruleEngine.getEffectiveType(), RuleEngineDetailEffectiveTypeResp.class));

        return ServiceResult.ok(ruleEngIneDetailResp);
    }

    @Override
    @Transactional
    public ServiceResult updateRileEngine(String id, RuleEngineDetailReq req) {

        //从token中解析员工工号
        String hrId = JwtUtil.getHrId(TokenUtils.getToken());

        //根据联动规则id查询规则信息
        Optional<IntelligentOfficeRuleEngine> ruleEngineOp = intelligentOfficeRuleEngineDao.findById(id);
        if (ruleEngineOp.isEmpty()) {
            return ServiceResult.error("联动规则不存在，编辑失败");
        }
        IntelligentOfficeRuleEngine ruleEngine = ruleEngineOp.get();

        Optional<IntelligentOffice> officeByIdOp = intelligentOfficeDao.findById(req.getOfficeId());
        if (officeByIdOp.isEmpty()) {
            return ServiceResult.error("办公室不存在");
        }
        IntelligentOffice intelligentOffice = officeByIdOp.get();

        Optional<SpaceAdmin> bySpaceIdAndAdminEmpNo = spaceAdminDao.findBySpaceIdAndAdminEmpNo(intelligentOffice.getSpace().getId(), hrId);
        if (bySpaceIdAndAdminEmpNo.isEmpty()) {
            return ServiceResult.error("修改失败，无权限");
        }

        ruleEngine.setLogo(req.getImgUrl());
        ruleEngine.setName(req.getName());
        ruleEngine.setUsableStatus(req.getUsableStatus());
        ruleEngine.setDescribe(req.getPurpose());
        ruleEngine.setTriggerCondition(req.getTriggerCondition());
        ruleEngine.setIntelligentOffice(intelligentOffice);

        ruleEngine.setTrigger(JSON.toJSONString(req.getTriggerReqs()));
        ruleEngine.setTriggerCondition(req.getTriggerCondition());

        ruleEngine.setActuator(JSON.toJSONString(req.getActuatorReqs()));
        ruleEngine.setEffectiveType(JSON.toJSONString(req.getEffectiveTypeReq()));

        intelligentOfficeRuleEngineDao.save(ruleEngine);

        //规则链创建接收对象
        CreateRuleChainReq createRuleChainReq = new CreateRuleChainReq();

        //根据hrid查询管理员
        List<SpaceAdmin> byAdminEmpNo = spaceAdminDao.findByAdminEmpNo(hrId);

        for (SpaceAdmin spaceAdmin : byAdminEmpNo) {
            //取出管理员名称
            String adminName = spaceAdmin.getAdminName();
            //添加创建人名称
            createRuleChainReq.setUserName(adminName);
            //添加规则链Id
            createRuleChainReq.setRuleChainId(id);
            //添加规则链名称
            String s = EncryptUtil.md5(intelligentOffice.getId());
            createRuleChainReq.setRuleChainName(req.getName()+s);
        }

        //判断条件
        if (RuleEngIneDetailTriggerCondition.WHOLE.equals(req.getTriggerCondition())) {
            createRuleChainReq.setIsAll(1);
        } else {
            createRuleChainReq.setIsAll(2);
        }

        //创建 传感器执行条件
        List<PropertiesReq> propertiesReqs = new ArrayList<>();

        //创建 人体传感器执行条件 的集合
        List<BodySensorReq> bodySensorReqs = new ArrayList<>();

        //执行条件
        for (RuleEngIneDetailTriggerReq ruleEngIneDetailTriggerReq : req.getTriggerReqs()) {

            //定时
            if (ruleEngIneDetailTriggerReq.getTiming() != null) {
                //定时小时
                createRuleChainReq.setHour(ruleEngIneDetailTriggerReq.getTiming().getHour());
                //定时分钟
                createRuleChainReq.setMinutes(ruleEngIneDetailTriggerReq.getTiming().getMinute());
            }

            PropertiesReq propertiesReq = new PropertiesReq();

            List<Conditions> conditionsList = new ArrayList<>();
            //传感器设备不为空
            if (ruleEngIneDetailTriggerReq.getSensorReqs() != null) {

                for (RuleEngIneDetailTriggerSensorReq ruleEngIneDetailTriggerSensorReq : ruleEngIneDetailTriggerReq.getSensorReqs()) {

                    //创建 规则判断条件值 对象
                    Conditions condition = new Conditions();

                    //创建 人体传感器执行条件 对象
                    BodySensorReq bodySensorReq = new BodySensorReq();

                    String key = ruleEngIneDetailTriggerSensorReq.getKey();

                    String deviceIdMd5 = EncryptUtil.md5(ruleEngIneDetailTriggerReq.getDeviceId());
                    switch (key) {
                        case "Noise":
                            condition.setKey(key + deviceIdMd5);
                            if ("1".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("50");
                                condition.setCompare("<");
                            } else if ("2".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("70");
                                condition.setCompare("<");
                            } else if ("3".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("90");
                                condition.setCompare("<");
                            } else {
                                condition.setValue("90");
                                condition.setCompare(">");
                            }
                            conditionsList.add(condition);
                            break;

                        case "PM2.5":
                            condition.setKey("PM2_5" + deviceIdMd5);
                            if ("1".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("75");
                                condition.setCompare("<");
                            } else if ("2".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("115");
                                condition.setCompare("<");
                            } else if ("3".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("150");
                                condition.setCompare("<");
                            } else {
                                condition.setValue("150");
                                condition.setCompare(">");
                            }
                            conditionsList.add(condition);
                            break;

                        case "PM10":
                            condition.setKey(key + deviceIdMd5);
                            if ("1".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("50");
                                condition.setCompare("<");
                            } else if ("2".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("100");
                                condition.setCompare("<");
                            } else if ("3".equals(ruleEngIneDetailTriggerSensorReq.getValue())) {
                                condition.setValue("150");
                                condition.setCompare("<");
                            } else {
                                condition.setValue("150");
                                condition.setCompare(">");
                            }
                            conditionsList.add(condition);
                            break;

                        case "people":
                            bodySensorReq.setKey(key+deviceIdMd5);
                            bodySensorReq.setTimeKey("timeKey" + deviceIdMd5);
                            bodySensorReq.setTimeValue(Integer.valueOf(ruleEngIneDetailTriggerSensorReq.getValue()));

                            if (RuleEngIneDetailTriggerDeviceCondition.HAVE.equals(ruleEngIneDetailTriggerSensorReq.getDeviceCondition())) {
                                bodySensorReq.setValue("true");
                            } else if (RuleEngIneDetailTriggerDeviceCondition.NOTHING.equals(ruleEngIneDetailTriggerSensorReq.getDeviceCondition())) {
                                bodySensorReq.setValue("false");
                            }
                            bodySensorReqs.add(bodySensorReq);
                            break;

                        default:
                            condition.setKey(key + deviceIdMd5);
                            condition.setValue(ruleEngIneDetailTriggerSensorReq.getValue());
                            if (RuleEngIneDetailTriggerDeviceCondition.GREATER.equals(ruleEngIneDetailTriggerSensorReq.getDeviceCondition())) {
                                condition.setCompare(">");
                            } else {
                                condition.setCompare("<");
                            }
                            conditionsList.add(condition);
                            break;
                    }
                }
                propertiesReq.setConditionsList(conditionsList);
            }
            if (!CollUtil.isEmpty(conditionsList)) {
                propertiesReqs.add(propertiesReq);
            }
        }

        createRuleChainReq.setPropertiesList(propertiesReqs);
        createRuleChainReq.setBodySensorList(bodySensorReqs);

        if (req.getEffectiveTypeReq().getEffectiveTypeTimingTimeReq() != null) {
            createRuleChainReq.setStartTimeHour(req.getEffectiveTypeReq().getEffectiveTypeTimingTimeReq().getStartTime().getHour());
            createRuleChainReq.setStartTimeMinutes(req.getEffectiveTypeReq().getEffectiveTypeTimingTimeReq().getStartTime().getMinute());
            createRuleChainReq.setEndTimeHour(req.getEffectiveTypeReq().getEffectiveTypeTimingTimeReq().getEndTime().getHour());
            createRuleChainReq.setEndTimeMinutes(req.getEffectiveTypeReq().getEffectiveTypeTimingTimeReq().getEndTime().getMinute());

            StringBuilder stringBuilder = new StringBuilder();
            String resultString = "";
            for (String w : req.getEffectiveTypeReq().getEffectiveTypeTimingTimeReq().getWeeks()) {
                stringBuilder.append(w);
            }
            resultString = stringBuilder.toString();
            createRuleChainReq.setDays(resultString);
        }

        GroupBindingReq groupBindingReq = new GroupBindingReq();
        groupBindingReq.setChainSite("cloud");
        groupBindingReq.setChannelType("3");
        groupBindingReq.setJsonArg(JSON.toJSONString(createRuleChainReq));
        groupBindingReq.setMethodCode(MethodCode.UPDATE_RULECHAIN.getName());
        groupBindingReq.setModuleCode(ModuleCode.HW.getName());
        JsonResult<Object> objectJsonResult = iotcApi.postToConn(groupBindingReq);
        if (objectJsonResult.getStatus() == 0) {

            log.error("规则编辑失败原因：" + objectJsonResult.getMsg() + "code" + objectJsonResult.getCode());
            // 联动规则修改失败，手动回滚事务,返回异常
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return ServiceResult.error(objectJsonResult.getMsg());
        }


        return ServiceResult.ok();
    }

    @Override
    @Transactional
    public ServiceResult pcDeleteRuleEngine(String id) {

        Optional<IntelligentOfficeRuleEngine> ruleEngineOp = intelligentOfficeRuleEngineDao.findById(id);
        if (ruleEngineOp.isEmpty()) {
            return ServiceResult.error("id有误联动规则不存在，删除失败");
        }
        IntelligentOfficeRuleEngine ruleEngine = ruleEngineOp.get();

        ruleEngine.setIsDelete(1);
        intelligentOfficeRuleEngineDao.save(ruleEngine);

        Telemetry telemetry = new Telemetry();
        telemetry.setRuleChainId(id);

        GroupBindingReq groupBindingReq = new GroupBindingReq();
        groupBindingReq.setChainSite("cloud");
        groupBindingReq.setChannelType("3");
        groupBindingReq.setJsonArg(JSON.toJSONString(telemetry));
        groupBindingReq.setMethodCode(MethodCode.DELETE_RULECHAIN.getName());
        groupBindingReq.setModuleCode(ModuleCode.HW.getName());
        JsonResult<Object> objectJsonResult = iotcApi.postToConn(groupBindingReq);
        if (objectJsonResult.getStatus() == 0) {

            log.error("默认规则删除失败原因：" + objectJsonResult.getMsg() + "code" + objectJsonResult.getCode());
            // 默认联动规则删除失败，手动回滚事务,返回异常
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return ServiceResult.error(objectJsonResult.getMsg());
        }

        return ServiceResult.ok();
    }

    @Override
    @Transactional
    public ServiceResult deleteRuleEngine(String id, String hrId) {

        Optional<IntelligentOfficeRuleEngine> ruleEngineOp = intelligentOfficeRuleEngineDao.findById(id);
        if (ruleEngineOp.isEmpty()) {
            return ServiceResult.error("id有误联动规则不存在，删除失败");
        }
        IntelligentOfficeRuleEngine ruleEngine = ruleEngineOp.get();

        Optional<IntelligentOffice> officeByIdOp = intelligentOfficeDao.findById(ruleEngine.getIntelligentOffice().getId());
        if (officeByIdOp.isEmpty()) {
            return ServiceResult.error(" 办公室不存在");
        }
        IntelligentOffice intelligentOffice = officeByIdOp.get();

        Optional<SpaceAdmin> spaceAdminOp = spaceAdminDao.findBySpaceIdAndAdminEmpNo(intelligentOffice.getSpace().getId(), hrId);
        if (spaceAdminOp.isEmpty()) {
            return ServiceResult.error("删除失败，无权限");
        }

        ruleEngine.setIsDelete(1);
        intelligentOfficeRuleEngineDao.save(ruleEngine);

        Telemetry telemetry = new Telemetry();
        telemetry.setRuleChainId(id);

        GroupBindingReq groupBindingReq = new GroupBindingReq();
        groupBindingReq.setChainSite("cloud");
        groupBindingReq.setChannelType("3");
        groupBindingReq.setJsonArg(JSON.toJSONString(telemetry));
        groupBindingReq.setMethodCode(MethodCode.DELETE_RULECHAIN.getName());
        groupBindingReq.setModuleCode(ModuleCode.HW.getName());
        JsonResult<Object> objectJsonResult = iotcApi.postToConn(groupBindingReq);
        if (objectJsonResult.getStatus() == 0) {

            log.error("规则删除失败原因：" + objectJsonResult.getMsg() + "code" + objectJsonResult.getCode());
            //  联动规则删除失败，手动回滚事务,返回异常
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return ServiceResult.error(objectJsonResult.getMsg());
        }

        return ServiceResult.ok();
    }

    @Override
    public ServiceResult ruleEngineControl(RuleEngineControlReq req) {

        Optional<IntelligentOfficeRuleEngine> ruleEngineOp = intelligentOfficeRuleEngineDao.findById(req.getRuleChainId());
        if (ruleEngineOp.isPresent() && UsableStatus.ENABLE.equals(ruleEngineOp.get().getUsableStatus())) {
            IntelligentOfficeRuleEngine ruleEngine = ruleEngineOp.get();
            log.info("执行规则:" + ruleEngine.getName());
            List<RuleEngIneDetailActuatorResp> actuatorResps = JSON.parseArray(ruleEngine.getActuator(), RuleEngIneDetailActuatorResp.class);

            // 对规则进行触发
            for (RuleEngIneDetailActuatorResp actuator : actuatorResps) {

                // 执行模式
                if (StringUtils.isNotBlank(actuator.getWorkingModeId())) {

                    Optional<DeviceWorkingMode> workingModeOp = workingModeDao.findById(actuator.getWorkingModeId());
                    if (workingModeOp.isEmpty()) {
                        return ServiceResult.error("控制失败,模式不存在");
                    }
                    DeviceWorkingMode workingMode = workingModeOp.get();
                    // 控制时间
                    workingMode.setControlTime(LocalDateTime.now());
                    workingModeDao.save(workingMode);

                    // 循环控制设备
                    List<DeviceModeStatus> list = modeStatusDao.findByModeId(actuator.getWorkingModeId());
                    for (DeviceModeStatus deviceModeStatus : list) {
                        deviceControl(deviceModeStatus, ruleEngine.getName());
                    }
                }

                // 执行规则绑定设备
                if (StringUtils.isNotBlank(actuator.getDeviceId())) {

                    Optional<IotDevice> deviceOp = deviceDao.findById(actuator.getDeviceId());
                    if (deviceOp.isPresent()) {
                        DeviceModeStatus deviceModeStatus = new DeviceModeStatus();
                        deviceModeStatus.setDevice(deviceOp.get());
                        deviceModeStatus.setProperties(JSON.toJSONString(actuator));
                        deviceControl(deviceModeStatus, ruleEngine.getName());
                    }
                }
            }

        }
        return ServiceResult.ok();
    }

    @Override
    public ServiceResult findControlledDevice(String id, List<String> deviceId, String hrId) {

        //根据办公室id查询该办公室下的所有联动规则
        List<IntelligentOfficeRuleEngine> ruleEngineList = intelligentOfficeRuleEngineDao.findByIntelligentOfficeIdOrderByCreatedTimeDesc(id);
        for (IntelligentOfficeRuleEngine intelligentOfficeRuleEngine : ruleEngineList) {
            // 将json格式的actuator转换
            List<RuleEngIneDetailActuatorResp> detailActuatorResps = JSON.parseArray(intelligentOfficeRuleEngine.getActuator(), RuleEngIneDetailActuatorResp.class);
            for (RuleEngIneDetailActuatorResp actuatorResp : detailActuatorResps) {
                //取出设备id
                if (actuatorResp.getDeviceId() != null) {
                    // 不为空，判断是否在选择的设备中
                    for (String device : deviceId) {
                        if (device.equals(actuatorResp.getDeviceId())) {
                            IntelligentOfficeDevice intelligentOfficeDevice = intelligentOfficeDeviceDao.findByDeviceIdAndUserHrIdAndIntelligentOfficeId(actuatorResp.getDeviceId(), hrId, id);
                            if (intelligentOfficeDevice.getDeviceAlisa() != null) {
                                return ServiceResult.error("该办公室下联动规则中，该“" + intelligentOfficeDevice.getDeviceAlisa() + "”设备已存在");
                            } else {
                                Optional<IotDevice> byId = deviceDao.findById(actuatorResp.getDeviceId());
                                if (byId.isEmpty()) {
                                    return ServiceResult.error("设备id有误");
                                }
                                return ServiceResult.error("该办公室下联动规则中，该“" + byId.get().getDeviceName() + "”设备已存在");
                            }
                        }
                    }
                }
            }
        }

        return ServiceResult.ok();
    }

    private void deviceControl(DeviceModeStatus deviceModeStatus, String ruleEngineName) {

        IotDevice device = deviceModeStatus.getDevice();
        if (DeviceStatus.ONLINE.equals(device.getHwStatus())) {

            // 获取设备所属产品的服务ID和命令名称
            ServiceResult serviceResult = productsService.findProductProperties(device);
            if(!serviceResult.isSuccess()){
                return;
            }
            DoorMagneticHWReq hwReq = (DoorMagneticHWReq) serviceResult.getObject();
            DoorMagneticHWParasReq paras = JSON.parseObject(deviceModeStatus.getProperties(), DoorMagneticHWParasReq.class);
            String hwDeviceId = device.getHwDeviceId();
            // 下发控制设备
            hwReq.setDeviceId(device.getId());
            hwReq.setDevice_id(hwDeviceId);
            hwReq.setParas(paras);

            // 空调
            if (DeviceType.AIR_CONDITIONER.equals(device.getDeviceType())) {
                ServiceResult result = productsService.command(hwReq, "", ruleEngineName);
                if (!result.isSuccess()) {
                    log.error("规则模式执行失败:" + ruleEngineName);
                    log.error("执行设备:" + device.getHwDeviceName());
                }
            }

            // 窗帘
            if (DeviceType.CURTAINS.equals(device.getDeviceType())) {
                ServiceResult result = productsService.command(hwReq, "", ruleEngineName);
                if (!result.isSuccess()) {
                    log.error("规则模式执行失败:" + ruleEngineName);
                    log.error("执行设备:" + device.getHwDeviceName());
                }
            }

            // 门磁
            if (DeviceType.DOOR_MAGNETIC.equals(device.getDeviceType())) {
                ServiceResult result = productsService.command(hwReq, "", ruleEngineName);
                if (!result.isSuccess()) {
                    log.error("规则模式执行失败:" + ruleEngineName);
                    log.error("执行设备:" + device.getHwDeviceName());
                }
            }

            // 四路开关
            if (DeviceType.FOUR_WAY_SWITCH.equals(device.getDeviceType())) {
                ServiceResult result = productsService.updateSwitch(hwReq, "", ruleEngineName);
                if (!result.isSuccess()) {
                    log.error("规则模式执行失败:" + ruleEngineName);
                    log.error("执行设备:" + device.getHwDeviceName());
                }
            }
        }
    }

    @Override
    public List<RuleEngineResp> findRuleEngine(String id) {

        List<RuleEngineResp> ruleEngineResps = new ArrayList<>();

        Optional<IntelligentOffice> intelligentOffice = intelligentOfficeDao.findById(id);
        if (intelligentOffice.isEmpty()) {
            log.error("该办公室不存在");
            return ruleEngineResps;
        }

        //查询当前
        List<IntelligentOfficeRuleEngine> ruleEngineList = intelligentOfficeRuleEngineDao.findByIntelligentOfficeIdOrderByCreatedTimeDesc(id);
        for (IntelligentOfficeRuleEngine intelligentOfficeRuleEngine : ruleEngineList) {

            RuleEngineResp ruleEngineResp = new RuleEngineResp();
            ruleEngineResp.setRuleEngineId(intelligentOfficeRuleEngine.getId());
            ruleEngineResp.setRuleEngineLogoImgUrl(intelligentOfficeRuleEngine.getLogo());
            ruleEngineResp.setRuleEngineName(intelligentOfficeRuleEngine.getName());
            ruleEngineResp.setUsableStatus(intelligentOfficeRuleEngine.getUsableStatus());
            ruleEngineResp.setRuleEngineSketch(intelligentOfficeRuleEngine.getDescribe());
            ruleEngineResp.setCreatedTime(intelligentOfficeRuleEngine.getCreatedTime());
            ruleEngineResps.add(ruleEngineResp);
        }
        return ruleEngineResps;
    }
}
