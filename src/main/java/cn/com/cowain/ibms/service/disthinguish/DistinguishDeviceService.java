package cn.com.cowain.ibms.service.disthinguish;

import cn.com.cowain.ibms.rest.resp.distinguish.DistinguishDeviceListResp;

import javax.annotation.Nullable;
import java.util.List;

/**
 * 人脸识别设备Service
 *
 * @author Yang.Lee
 * @date 2021/3/11 14:13
 */
public interface DistinguishDeviceService {

    /**
     * 查询人脸识别设备列表
     *
     * @param projectId 项目ID
     * @param spaceId   空间ID
     * @return 设备列表
     * @author Yang.Lee
     * @date 2021/3/11 14:15
     **/
    List<DistinguishDeviceListResp> getList(@Nullable String projectId, @Nullable String spaceId);
}
