package cn.com.cowain.ibms.enumeration;

/**
 * 权限状态申请枚举
 * Yang.Lee
 * @date 2022/7/18 14:10
 */
public enum PermissionApplicationStatus {

    PASS("有权限"),

    UNDER_APPROVAL( "审批中"),

    REFUSED("申请备拒绝"),

    NO_PERMISSION("无权限"),

    CURRENT_TIME_NO_PERMISSION("当前时间无权限");

    private  String name;

    PermissionApplicationStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
