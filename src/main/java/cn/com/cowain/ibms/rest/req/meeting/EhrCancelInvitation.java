package cn.com.cowain.ibms.rest.req.meeting;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/7 16:22
 */
@Data
public class EhrCancelInvitation {

    // 邀请函单号
    private String visitorRecordNo;

    // 状态  0:取消，40001  40002同意拒绝
    private String status;
}
