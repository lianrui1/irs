package cn.com.cowain.ibms.rest.req;

import cn.com.cowain.ibms.enumeration.ReservationRecordItemStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * UserStatusBean
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/4 18:47
 */
@Data
@ApiModel("用户数据")
public class UserStatusBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 用户中心的全局唯一id
     */
    @ApiModelProperty(name = "用户中心的全局唯一id", required = true)
    private String sysId;

    /**
     * 员工编号
     */
    @ApiModelProperty(name = "员工编号", required = true)
    private String empNo;

    /**
     * 部门名称
     */
    @ApiModelProperty(name = "部门名称", required = true)
    private String deptName;

    /**
     * 部门ID，预留
     */
    @ApiModelProperty(name = "部门ID", required = true)
    private String deptId;

    /**
     * 参加会议人状态
     */
    @ApiModelProperty(name = "参会人状态", required = true)
    private ReservationRecordItemStatus status;

    /**
     * 是否是外部人员
     */
    @ApiModelProperty(value = "是否是访客，1 是； 0 否")
    private Integer isGuest;

    @ApiModelProperty(value = "访客邀请记录code")
    private String code;

    @ApiModelProperty(value = "参会人姓名")
    private String nameZh;

    /**
     * 是否是外部人员
     */
    @ApiModelProperty(value = "是否是会议组成员，1 是； 0 否")
    private Integer isGroup;
}
