package cn.com.cowain.ibms.rest.resp;

import cn.com.cowain.ibms.entity.iot.VideoMonitorSpot;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author feng
 * @title: NodeSit 监控点列表
 * @projectName bims
 * @Date 2021/11/18 11:01
 */
@Data
public class MonitorNodeResp {
    public MonitorNodeResp(){}
    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("视频监控点名称")
    private String name;

    @ApiModelProperty("具体位置")
    private String address;

    @ApiModelProperty("视频监控点所属空间ID")
    private String spaceId;

    @ApiModelProperty("视频监控点关联设备ID")
    private String deviceId;
   public MonitorNodeResp (VideoMonitorSpot videoMonitorSpot){
       this.id=videoMonitorSpot.getId();
       this.name=videoMonitorSpot.getName();
       this.address=videoMonitorSpot.getAddress();
       this.spaceId=videoMonitorSpot.getSpace().getId();
       this.deviceId=videoMonitorSpot.getDevice().getId();
   }
}
