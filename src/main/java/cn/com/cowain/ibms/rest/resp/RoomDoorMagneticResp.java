package cn.com.cowain.ibms.rest.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2020/11/28 14:26
 */
@Data
@ApiModel("预定记录")
public class RoomDoorMagneticResp {

    /**
     * 构造函数
     * @param hasMagnetic
     */
    public RoomDoorMagneticResp(boolean hasMagnetic){
        this.hasMagnetic = hasMagnetic;
    }

    /**
     * 会议室是否拥有门磁
     */
    @ApiModelProperty(value = "会议室是否拥有门磁", required = true, example = "true: 拥有; false: 未拥有", position = 0)
    private boolean hasMagnetic;
}
