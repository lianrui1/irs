package cn.com.cowain.ibms.entity.office;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.enumeration.RuleEngIneDetailTriggerCondition;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 *
 * @author: yanzy
 * @date: 2022/2/15 19:47
 */
@Data
@Entity
@Table(name = "ibms_intelligent_office_default_rule_engine")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "ibms_intelligent_office_default_rule_engine", comment = "智能办公室规则默认引擎表")
public class IntelligentOfficeDefaultRuleEngine extends BaseEntity {

    @Column(name = "`name`", nullable = false, columnDefinition = "varchar(20)  COMMENT '办公室名称'")
    private String name;

    @Column(name = "logo", nullable = false, columnDefinition = "varchar(100)  COMMENT 'logo'")
    private String logo;

    @Column(name = "`trigger`", nullable = false, columnDefinition = "varchar(2000)  COMMENT '触发条件'")
    private String trigger;

    @Column(name = "actuator", nullable = false, columnDefinition = "varchar(2000)  COMMENT '执行条件'")
    private String actuator;

    @Column(name = "`effective_type`", nullable = false, columnDefinition = "varchar(2000)  COMMENT '生效方式。一直 / 定时。  json格式'")
    private String effectiveType;

    @Column(name = "trigger_condition", nullable = false, columnDefinition = "varchar(50) COMMENT '条件关系'")
    @Enumerated(EnumType.STRING)
    private RuleEngIneDetailTriggerCondition triggerCondition;


}
