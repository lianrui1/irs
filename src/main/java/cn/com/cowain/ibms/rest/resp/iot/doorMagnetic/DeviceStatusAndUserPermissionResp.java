package cn.com.cowain.ibms.rest.resp.iot.doorMagnetic;

import cn.com.cowain.ibms.enumeration.PermissionApplicationStatus;
import cn.com.cowain.ibms.rest.resp.StaffResp;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2022/7/13 11:14
 */
@Data
@Builder
//@AllArgsConstructor
//@NoArgsConstructor
@ApiModel("设备状态及用户权限响应对象")
public class DeviceStatusAndUserPermissionResp {

    @ApiModelProperty(value = "用户权限状态", example = "true : 有权限； false : 无权限", position = 1)
    private PermissionApplicationStatus status;

    @ApiModelProperty(value = "设备状态信息，当用户无权限时不返回该字段。", position = 2)
    private DoorMagneticStatusResp deviceStatus;

    @ApiModelProperty(value = "审批人管理员列表", position = 3)
    private List<StaffResp> approvalAdminList;

    @ApiModelProperty(value = "设备管理员列表", position = 4)
    private List<StaffResp> deviceAdminList;
}
