package cn.com.cowain.ibms.mq.producer;

import cn.com.cowain.ibms.mq.Topic;
import cn.com.cowain.ibms.rest.resp.distinguish.WorkTimeCheckResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 旷视通行记录数据producer
 *
 * @author Yang.Lee
 * @date 2022/2/23 9:21
 */
@Slf4j
@Component
public class AccessRecordProducer {

    @Resource
    private RocketMQTemplate rocketMQTemplate;

    public ServiceResult push(WorkTimeCheckResp resp) {

        String topic = Topic.get(Topic.MEGVII_ACCESS_RECORD);
        String messageStr = JSON.toJSONString(resp);

        log.debug("推送数据到topic : {}. 数据： {}", topic, messageStr);
        Message<String> message = new GenericMessage<>(messageStr);
        SendResult sendResult = rocketMQTemplate.syncSend(topic + ":sendRecordMsg", message);
        if (!sendResult.getSendStatus().equals(SendStatus.SEND_OK)) {
            log.error("旷视通行记录推送失败：" + sendResult);
            return ServiceResult.error(sendResult.toString());
        }

        return ServiceResult.ok();
    }
}
