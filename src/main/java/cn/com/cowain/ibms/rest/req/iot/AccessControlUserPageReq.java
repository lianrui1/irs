package cn.com.cowain.ibms.rest.req.iot;

import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/18 15:33
 */
@Data
@ApiModel("用户列表分页请求对象")
public class AccessControlUserPageReq extends PageReq {

    @ApiModelProperty(value = "人员类型0员工 1访客")
    private Integer userType;

    @ApiModelProperty(value = "人员姓名/工号")
    private String keyword;
}
