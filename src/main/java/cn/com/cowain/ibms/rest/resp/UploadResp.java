package cn.com.cowain.ibms.rest.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/4/25 8:57
 */
@Data
@ApiModel("文件上传返回数据")
public class UploadResp {
    @ApiModelProperty(value = "文件URL", required = true)
    private String url;

    @ApiModelProperty(value = "源文件文件名", required = true)
    private String originalFileName;

    @ApiModelProperty(value = "文件大小，单位为B", required = true)
    private Long size;
}
