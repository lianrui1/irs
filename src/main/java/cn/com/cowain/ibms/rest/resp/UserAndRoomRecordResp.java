package cn.com.cowain.ibms.rest.resp;

import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.bean.ReservationRecordBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2021/1/4 19:52
 */
@Data
public class UserAndRoomRecordResp {

    @ApiModelProperty(value = "会议记录分页列表", required = true)
    PageBean<ReservationRecordBean> reservationRecordItemPage;

    @ApiModelProperty(value = "用户参与的会议记录分页列表", required = true)
    PageBean<RoomWithReservationResp> userRecordItemPage;
}
