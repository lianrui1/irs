package cn.com.cowain.ibms.dao.distinguish;

import cn.com.cowain.ibms.entity.distinguish.DeviceCloud;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

/**
 * 人脸识别设备DAO类
 *
 * @author Yang.Lee
 * @date 2021/3/10 10:06
 */
public interface DeviceCloudDao extends JpaRepository<DeviceCloud, String>, JpaSpecificationExecutor<DeviceCloud> {

    // 根据扫脸设备唯一编码获取云端设备
    List<DeviceCloud> findByCloudDeviceId(String snCode);

    /**
     * 根据设备ID查询旷世设备
     *
     * @param deviceId 设备ID
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/4/23 9:20
     **/
    Optional<DeviceCloud> findByIotDeviceId(String deviceId);
}
