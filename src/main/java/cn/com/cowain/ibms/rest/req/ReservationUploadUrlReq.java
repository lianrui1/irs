package cn.com.cowain.ibms.rest.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/4/7 13:25
 */
@Data
@ApiModel("上传链接接收对象")
public class ReservationUploadUrlReq {

    @ApiModelProperty(value = "会议记录ID")
    private String reservationRecordId;

    @ApiModelProperty(value = "链接地址")
    private List<String> urlList;

    @ApiModelProperty(value = "链接说明")
    private String urlRemark;
}
