package cn.com.cowain.ibms.rest.resp.office;

import cn.com.cowain.ibms.enumeration.RuleEngIneDetailTriggerCondition;
import cn.com.cowain.ibms.enumeration.iot.UsableStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.util.List;

/**
 * 联动规则详情返回对象
 *
 * @author: yanzy
 * @date: 2022/2/11 09:58
 */
@Data
@ApiModel("联动规则详情返回对象")
public class RuleEngIneDetailResp {

    @ApiModelProperty(value = "联动规则id",required = true)
    private String id;

    @ApiModelProperty(value = "联动规则小图片url",required = true)
    private String imgUrl;

    @Length(max = 20,message = "名称最长为20")
    @ApiModelProperty(value = "联动规则名称",required = true)
    private String name;

    @Length(max = 20,message = "简述最长为20")
    @ApiModelProperty(value = "联动规则简述")
    private String purpose;

    @ApiModelProperty(value = "条件关系",required = true)
    private RuleEngIneDetailTriggerCondition triggerCondition;

    @ApiModelProperty(value = "触发条件",required = true)
    private List<RuleEngIneDetailTriggerResp> triggerReqs;

    @ApiModelProperty(value = "执行任务",required = true)
    private List<RuleEngIneDetailActuatorResp> actuatorReqs;

    @ApiModelProperty(value = "是否启用",required = true)
    private UsableStatus usableStatus;

    @ApiModelProperty(value = "生效方式",required = true)
    private RuleEngineDetailEffectiveTypeResp effectiveTypeReq;
}
