package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.Curtain;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CurtainDao extends JpaRepository<Curtain, String> {

    // 根据设备获取窗帘
    Optional<Curtain> findByDevice(IotDevice iotDevice);

    Optional<Curtain> findByDeviceHwDeviceId(String deviceId);
}
