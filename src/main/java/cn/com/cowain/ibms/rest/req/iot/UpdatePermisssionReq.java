package cn.com.cowain.ibms.rest.req.iot;

import lombok.Data;

/**
 * @author feng
 * @title: PermisssionReq
 * @projectName ibms
 * @Date 2022/1/12 15:22
 */
@Data
public class UpdatePermisssionReq {
    /**
    　　* @title:
    　　* @description 新组UUID列表
    　　* @author jfsui
    　　* @date 2022/1/12 16:39
    　　*/
    String [] newGroupUuidList;
    /**
    　　* @title:
    　　* @description 设备UUID sn
    　　* @author jfsui
    　　* @date 2022/1/12 16:39
    　　*/
    String deviceUuid;
}
