package cn.com.cowain.ibms.service.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/16 16:40
 */
@Data
public class Services {
    @JsonProperty(value = "service_id")
    private String serviceId;
    private DoorMagneticHWParasReq properties;
}
