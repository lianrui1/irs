package cn.com.cowain.ibms.rest.req.iot.centralized;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * 华为云批量下发命令
 *
 * @author wei.cheng
 * @date 2022/09/21 14:11
 */
@Data
public class BatchCommandReq {
    /**
     * 跟踪连ID
     */
    @NotEmpty
    private String traceId;
    /**
     * 创建的批量下发任务列表
     */
    private List<CommandTemplate> commandTemplates;

    private List<PropertiesTemplate> propertiesTemplates;


}
