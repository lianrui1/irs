package cn.com.cowain.ibms.rest.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/4 20:12
 */
@Data
@ApiModel("EHR 部门数据")
public class EhrDeptBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "组织ID", required = true, position = 0)
    private String orId;

    @ApiModelProperty(value = "组织名", required = true, position = 1)
    private String roName;

    /**
     * H5前端专用(后端不用)
     * todo:后面要优化,去除
     */
    @ApiModelProperty(value = "H5前端专用(后端不用)", required = true, position = 2)
    private boolean show = false;

    @ApiModelProperty(value = "数据列表", required = true, position = 3)
    private List<EhrDeptBean> list;

}
