package cn.com.cowain.ibms.rest.resp.office;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author feng
 * @title: AirConditionerStatusDetailResp
 * @projectName ibms
 * @Date 2021/12/21 14:07
 */
@Data
public class TbMeetingDetailResp {
    @ApiModelProperty(value = "设备Id", required = true)
    String id;
    @ApiModelProperty(value = "华为云Id", required = true)
    String hwDeviceId;
    @ApiModelProperty(value = "设备状态", required = true)
    String hwStatus;
    @ApiModelProperty(value = "设备名称", required = true)
    String deviceName;

    @ApiModelProperty(value = "亮度", required = true)
    private String luminance;
    @ApiModelProperty(value = "温度", required = true)
    private String temperature;

    @ApiModelProperty(value = "湿度", required = true)
    private String humidity;

    @ApiModelProperty(value = "噪音", required = true)
    private String noise;

    @ApiModelProperty(value = "空气质量10", required = true)
    private String pm10;

    @ApiModelProperty(value = "空气质量pm2_5", required = true)
    private String pm2_5;

    @ApiModelProperty(value = "人体传感器", required = true)
    private String isPeopleThere;
    private String value;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime ts;
}
