package cn.com.cowain.ibms.service.office.impl;

import cn.com.cowain.ibms.config.prop.UcProperties;
import cn.com.cowain.ibms.dao.office.IntelligentOfficeDao;
import cn.com.cowain.ibms.dao.office.IntelligentOfficeInitializationDao;
import cn.com.cowain.ibms.dao.office.IntelligentOfficeUserDao;
import cn.com.cowain.ibms.dao.office.IntelligentOfficeUserGuideDao;
import cn.com.cowain.ibms.dao.space.SpaceAdminDao;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.office.IntelligentOffice;
import cn.com.cowain.ibms.entity.office.IntelligentOfficeInitialization;
import cn.com.cowain.ibms.entity.office.IntelligentOfficeUser;
import cn.com.cowain.ibms.entity.office.IntelligentOfficeUserGuide;
import cn.com.cowain.ibms.entity.space.SpaceAdmin;
import cn.com.cowain.ibms.enumeration.iot.IntelligentOfficeStatus;
import cn.com.cowain.ibms.feign.bean.uc.AuthTokenResDto;
import cn.com.cowain.ibms.feign.uc.UCApiV1;
import cn.com.cowain.ibms.rest.resp.office.UserLoginResp;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.office.LoginService;
import cn.com.cowain.ibms.utils.RestTemplateUtil;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.sfp.comm.utils.JwtUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Yang.Lee
 * @date 2021/12/20 17:45
 */
@Slf4j
@Service
public class LoginServiceImpl implements LoginService {

    public static final String NULL = "null";
    @Resource
    private UCApiV1 ucApiV1;

    @Autowired
    private UcProperties ucProperties;

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserService;

    @Resource
    private IntelligentOfficeDao intelligentOfficeDao;

    @Resource
    private SpaceAdminDao spaceAdminDao;

    @Resource
    private IntelligentOfficeInitializationDao intelligentOfficeInitializationDao;

    @Resource
    private IntelligentOfficeUserDao intelligentOfficeUserDao;

    @Resource
    private IntelligentOfficeUserGuideDao intelligentOfficeUserGuideDao;

    @Resource
    private RestTemplateUtil restTemplateUtil;


    /**
     * 智能办公室登录（小程序登录）
     *
     * @param code UC V2 返回的code
     * @return 用户登录信息
     * @author Yang.Lee
     * @date 2021/12/20 17:43
     **/
    @Override
    public ServiceResult login(String code) {
        log.debug("小程序登录，code : {}", code);
        // 获取token
        String token = getToken(code);
        log.debug("获取token，token : {},", token);
        // 获取UC V2的用户的工号
        // uc调用接口设置header中的token值
        Map<String, String> headerMap = new HashMap<>(1);
        headerMap.put("UC-Token", token);
        String hrInfoRes = restTemplateUtil.get(ucProperties.getHrInfoUrl(), headerMap);
        log.debug("员工信息：{}", hrInfoRes);
        //工号
        String hrId;
        if (StringUtils.isBlank(hrInfoRes) || NULL.equals(hrInfoRes)) {
            return ServiceResult.error("请绑定员工账号");
        } else {
            JSONObject hrInfoResp = JSON.parseObject(hrInfoRes);
            // 工号
            hrId = hrInfoResp.getString("hr_id");
        }

        // 获取UC V1的用户信息
        SysUser sysUser = sysUserService.getUserByEmpNo(hrId);

        JsonResult<String> jsonResult = ucApiV1.getUCToken(sysUser.getEmpNo());
        log.debug("ucApiV1 getUCToken result: {}", JSON.toJSONString(jsonResult));


        if (jsonResult.getStatus() == 0) {

            String errorMsg = "向UC获取authorization失败";
            log.error(errorMsg);
            return ServiceResult.error(errorMsg);
        }

        String authorization = jsonResult.getData();

        // 查询该用户是哪些智能会议室的管理员
        return ServiceResult.ok(getByUser(authorization));
    }

    /**
     * 根据token登录
     *
     * @param token token
     * @return 用户登录信息
     * @author Yang.Lee
     * @date 2021/12/21 13:15
     **/
    @Override
    public ServiceResult loginByToken(String token) {

        return ServiceResult.ok(getByUser(token));
    }

    /**
     * 根据token获取登录信息
     *
     * @param token token
     * @return 用户登录信息
     * @author Yang.Lee
     * @date 2021/12/23 17:31
     **/
    private UserLoginResp getByUser(String token) {
        log.debug("getByUser token {}", token);
        String userHrId = JwtUtil.getHrId(token);

        // 获取UC V1的用户信息
        SysUser sysUser = sysUserService.getUserByEmpNo(userHrId);

        // 查询该用户是哪些智能会议室的管理员
        UserLoginResp userLoginResp = new UserLoginResp();
        userLoginResp.setUserHrId(sysUser.getEmpNo());
        userLoginResp.setUserName(sysUser.getNameZh());
        userLoginResp.setHeadImgUrl(sysUser.getHeadImgUrl());
        userLoginResp.setDepartmentName(sysUser.getFullDepartmentName());
        userLoginResp.setAuthorization(token);
        List<UserLoginResp.UserOfficeInfo> userOfficeInfoList = new ArrayList<>();

        // 查询用户是管理员的数据
        List<SpaceAdmin> spaceAdminList = spaceAdminDao.findByAdminEmpNo(sysUser.getEmpNo());
        Set<String> spaceIdSet = spaceAdminList.stream().map(SpaceAdmin::getSpace).map(Space::getId).collect(Collectors.toSet());

        List<IntelligentOffice> intelligentOfficeList = intelligentOfficeDao.findBySpaceIdInAndStatus(spaceIdSet, IntelligentOfficeStatus.ENABLE);
        userOfficeInfoList.addAll(intelligentOfficeList.stream().map(obj -> {

            Optional<IntelligentOfficeInitialization> initOptional = intelligentOfficeInitializationDao.
                    findByUserHrIdAndIntelligentOfficeId(sysUser.getEmpNo(), obj.getId());
            UserLoginResp.UserOfficeInfo userOfficeInfo = new UserLoginResp.UserOfficeInfo();

            userOfficeInfo.setId(obj.getName());
            userOfficeInfo.setName(initOptional.isEmpty() ? obj.getName() : initOptional.get().getAlisa());
            userOfficeInfo.setIsInit(initOptional.isEmpty() ? 0 : 1);
            userOfficeInfo.setIsAdmin(1);

            return userOfficeInfo;
        }).collect(Collectors.toList()));

        // 查询用户不是管理员的数据
        List<IntelligentOfficeUser> officeUserList =
                intelligentOfficeUserDao.findByUserHrIdAndIntelligentOfficeStatus(sysUser.getEmpNo(), IntelligentOfficeStatus.ENABLE);

        userOfficeInfoList.addAll(officeUserList.stream().map(obj -> {

            Optional<IntelligentOfficeInitialization> initOptional = intelligentOfficeInitializationDao.
                    findByUserHrIdAndIntelligentOfficeId(obj.getInvitedUserHrId(), obj.getIntelligentOffice().getId());
            UserLoginResp.UserOfficeInfo userOfficeInfo = new UserLoginResp.UserOfficeInfo();

            userOfficeInfo.setId(obj.getIntelligentOffice().getId());
            userOfficeInfo.setName(initOptional.isEmpty() ? obj.getIntelligentOffice().getName() : initOptional.get().getAlisa());
            userOfficeInfo.setIsInit(initOptional.isEmpty() ? 0 : 1);
            userOfficeInfo.setIsAdmin(1);
            userOfficeInfo.setSpaceId(obj.getIntelligentOffice().getSpace().getId());

            return userOfficeInfo;

        }).collect(Collectors.toList()));

        userLoginResp.setOfficeList(userOfficeInfoList);
        Optional<IntelligentOfficeUserGuide> userGuideOptional = intelligentOfficeUserGuideDao.findByUserHrId(sysUser.getEmpNo());
        userLoginResp.setIsGuide(userGuideOptional.isEmpty() ? 0 : 1);

        return userLoginResp;
    }

    /**
     * 向UC V2 获取token
     *
     * @param code UC返回的code
     * @return token
     * @author Yang.Lee
     * @date 2021/12/20 18:03
     **/
    public String getToken(String code) {
        // 向UC v2 请求token
        AuthTokenResDto tokenRes = restTemplateUtil.get(ucProperties.getAccessTokenUrl() + code, null, AuthTokenResDto.class);
        log.info("从UC V2 获取token，结果 {}", JSON.toJSONString(tokenRes));
        // token入缓存
        return tokenRes.getToken();
    }
}
