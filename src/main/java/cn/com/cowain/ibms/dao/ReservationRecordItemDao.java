package cn.com.cowain.ibms.dao;

import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.entity.ReservationRecordItem;
import cn.com.cowain.ibms.enumeration.ReservationRecordItemStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * ReservationRecordItem Dao
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/4 18:41
 */
@Repository
public interface ReservationRecordItemDao extends JpaRepository<ReservationRecordItem, String>, JpaSpecificationExecutor<ReservationRecordItem> {

    /**
     * 根据会议发起人 ID,查询最近联系人列表
     *
     * @param initiatorEmpNo 会议发起人 工号
     * @param ownerEmpNp     会议参与 工号
     * @param pageable       分页参数
     * @return 最近联系人列表(分页)
     */
    Page<ReservationRecordItem> findByInitiatorEmpNoEqualsAndOwnerEmpNoIsNot(String initiatorEmpNo, String ownerEmpNp, Pageable pageable);


    /**
     * 根据 人员id from天后 以page形式 降序获取
     *
     * @param owner
     * @param from
     * @param pageable
     * @return
     */
    Page<ReservationRecordItem> findByOwnerEmpNoAndReservationRecordFromGreaterThanAndReservationRecordRoomSpaceProjectIdContainsOrderByReservationRecordFromDesc(String owner, LocalDateTime from, String projectId, Pageable pageable);

    /**
     * 根据 参会人员id 会议id 查询
     *
     * @param recordId
     * @param ownerId
     * @return
     */
    Optional<ReservationRecordItem> findByReservationRecordIdAndOwnerEmpNo(String recordId, String ownerId);

    /**
     * 根据 参会人员工号 会议id 查询
     *
     * @param recordId   会议ID
     * @param ownerEmpNo 员工工号
     * @return 会议预约明细
     */
    Optional<ReservationRecordItem> findByReservationRecordIdAndOwner(String recordId, String ownerEmpNo);

    /**
     * 根据 人员id 查询
     *
     * @param ownerId
     * @return
     */
    Optional<ReservationRecordItem> findByOwner(String ownerId);

    /**
     * 根据 会议id 查询
     *
     * @param recordId
     * @return
     */
    Set<ReservationRecordItem> findByReservationRecordId(String recordId);

    /**
     * 根据会议id和预定记录明细状态 查询
     *    
     * @author: yanzy
     * @data: 2022/5/3 13:05:14
     */
    Set<ReservationRecordItem> findByReservationRecordIdAndStatus(String recordId, ReservationRecordItemStatus status);

    /**
     * 根据owner和日期查询
     *
     * @param userId 用户ID
     * @param date   日期
     * @return List<ReservationRecordItem>
     */
    List<ReservationRecordItem> findByOwnerEmpNoAndReservationRecordDate(String userId, LocalDate date);

    /**
     * 根据会议发起人 ID,查询最近联系人列表
     *
     * @param reservationRecord 会议 ID
     * @return
     */
    List<ReservationRecordItem> findByReservationRecord(ReservationRecord reservationRecord);

    /**
     * 根据会议ID,查询
     *
     * @param recordId 会议 ID
     * @return
     */
    List<ReservationRecordItem> findByReservationRecordIdOrderByOwner(String recordId);

    /**
     * 判断工号为{@code ownerEmpNo}的用户是否参与会议{@code recordId}
     *
     * @param recordId
     * @param ownerEmpNo
     * @return
     */
    Boolean existsByReservationRecordIdAndOwnerEmpNo(String recordId, String ownerEmpNo);


    String COUNT_SQL = "select count(*) from ibms_reservation_record_item ri left join ibms_reservation_record r on ri.record_id = r.id " +
                    "where ri_owner_emp_no = :ownerEmpNo and if(:topicLike != '' and :topicLikeOrRoomIds is null, r.r_topic like :topicLike, 1=1) " +
            " and if(:topicLike != '' and :topicLikeOrRoomIds is not null, (r.r_topic like :topicLike or r.room_id in (:topicLikeOrRoomIds)), 1=1) " +
            " and if(:from != '', r.r_from >= :from, 1=1) and if(:to != '', r.r_to <= :to, 1=1) and if(:roomIds is not null, r.room_id in (:roomIds), 1=1)" +
            " and r.is_del = 0 and ri.is_del = 0";
    String QUERY_SQL ="select ri.ri_initiator_emp_no as initiatorEmpNo, r.r_topic as topic, r.r_from as rFrom, r.r_to as rTo, r.room_id as roomId, ri.ri_initiator as initiator, " +
            "ri.ri_status as status, r.r_status as rStatus, r.r_remark as remark, r.id as rId, r.r_service as service, r.r_date as rDate" +
            " from ibms_reservation_record_item ri left join ibms_reservation_record r on ri.record_id = r.id " +
            "where ri_owner_emp_no = :ownerEmpNo and if(:topicLike != '' and :topicLikeOrRoomIds is null, r.r_topic like :topicLike, 1=1) " +
            " and if(:topicLike != '' and :topicLikeOrRoomIds is not null, (r.r_topic like :topicLike or r.room_id in (:topicLikeOrRoomIds)), 1=1) " +
            " and if(:from != '', r.r_from >= :from, 1=1) and if(:to != '', r.r_to <= :to, 1=1) and if(:roomIds is not null, r.room_id in (:roomIds), 1=1)" +
            " and r.is_del = 0 and ri.is_del = 0";
    @Query(
            value = QUERY_SQL ,
            countQuery = COUNT_SQL,
            nativeQuery = true
    )
    Page<Map<String,Object>> selectPage(@Param("ownerEmpNo") String ownerEmpNo,
                                           @Param("topicLike") String topicLike, @Param("topicLikeOrRoomIds")List<String> topicLikeOrRoomIds,
                                           @Param("from") String from, @Param("to") String to,
                                           @Param("roomIds")List<String> roomIds,
                                           Pageable pageable);
}
