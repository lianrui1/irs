package cn.com.cowain.ibms.entity.office;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author Yang.Lee
 * @date 2021/8/27 16:11
 */
@Data
@Entity
@Table(name = "iot_devcie_mode_status")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_devcie_mode_status", comment = "设备工作模式对应设备表")
public class DeviceModeStatus extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "mode", nullable = false, columnDefinition = "varchar(36) COMMENT '工作模式'")
    private DeviceWorkingMode mode;

    @ManyToOne
    @JoinColumn(name = "device_id", nullable = false, columnDefinition = "varchar(36) COMMENT '设备id'")
    private IotDevice device;

    @Column(name = "status", columnDefinition = "varchar(50) COMMENT '设备状态'")
    private String status;

    @Column(name = "properties", nullable = false, columnDefinition = "varchar(500) COMMENT '设备属性'")
    private String properties;

}
