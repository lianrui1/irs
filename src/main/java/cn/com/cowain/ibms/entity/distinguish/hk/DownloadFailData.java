package cn.com.cowain.ibms.entity.distinguish.hk;

import lombok.Data;

import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/8/4 13:24
 */
@Data
public class DownloadFailData {
    private List<DownloadFail> downloadFailList;
    private String equipmentIp;
}
