package cn.com.cowain.ibms.rest.advice;

import cn.com.cowain.ibms.component.DeviceDictionaryComponent;
import cn.com.cowain.ibms.component.SpaceAdminRedis;
import cn.com.cowain.ibms.dao.ReservationRecordDao;
import cn.com.cowain.ibms.dao.iot.AccessControlSpotDao;
import cn.com.cowain.ibms.dao.iot.DoorMagneticDao;
import cn.com.cowain.ibms.dao.space.SpaceAdminDao;
import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.entity.iot.AccessControlSpot;
import cn.com.cowain.ibms.entity.iot.DoorMagnetic;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.space.SpaceAdmin;
import cn.com.cowain.ibms.enumeration.ReservationRecordStatus;
import cn.com.cowain.ibms.enumeration.iot.AuthType;
import cn.com.cowain.ibms.utils.DataUtils;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.ibms.utils.RedisUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.StopWatch;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/21/20
 */
@Log4j2
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class DataExceptionAdviceTest {

    @LocalServerPort
    private int port;

    @Autowired
    private RestTemplate restTemplate;

    @MockBean
    private ReservationRecordDao reservationRecordDao;

    private String getPath() {
        return "http://localhost:" + port + API_BASE;
    }

    @Resource
    private AccessControlSpotDao accessControlSpotDao;

    @Test
    void noSuchElement() {
        String id = "notExistId";
        // header
        String url = getPath() + "/reservation/" + id;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        // httpEntity
        HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        // 断言
        Assertions.assertThrows(HttpClientErrorException.class, () -> restTemplate.exchange(url, HttpMethod.DELETE, httpEntity, JSONObject.class));
    }

    @Test
    void bizLogicNotMatch() {
        String id = "notExistId";
        // header
        String url = getPath() + "/reservation/" + id;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        // httpEntity
        HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        //mock
        ReservationRecord record = new ReservationRecord();
        record.setStatus(ReservationRecordStatus.DEFAULT);
        Optional<ReservationRecord> optional = Optional.of(record);
        Mockito.when(reservationRecordDao.findById(id)).thenReturn(optional);
        // 断言
        Assertions.assertThrows(HttpServerErrorException.class, () -> restTemplate.exchange(url, HttpMethod.DELETE, httpEntity, JSONObject.class));
    }

    @Test
    void getData() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start("access1");
        IotDevice iotDevice = new IotDevice();
        iotDevice.setId("3c44820f-026f-46df-9bcb-1fc923967a35");
        Optional<AccessControlSpot> accessControlSpotOptional = accessControlSpotDao.findByDevice(iotDevice);
        accessControlSpotOptional.ifPresent(accessControlSpot -> {
            System.out.println("对象：" + accessControlSpot);
        });
        stopWatch.stop();
        System.out.println(stopWatch.prettyPrint());


    }

    @Resource
    private DoorMagneticDao doorMagneticDao;

    @Test
    void getSpot() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start("access2");
        Optional<DoorMagnetic> doorMagneticOptional = doorMagneticDao.findByNumber("HQHT");
        DoorMagnetic doorMagnetic = doorMagneticOptional.get();
        Optional<AccessControlSpot> accessControlSpotOptional = accessControlSpotDao.findByDevice(doorMagnetic.getDevice());

        accessControlSpotOptional.ifPresent(accessControlSpot -> {
            System.out.println("对象：" + accessControlSpot);
        });
        stopWatch.stop();
        System.out.println(stopWatch.prettyPrint());


    }

    @Test
    public void invokeEnum() throws ClassNotFoundException {

        String name = "ALL_PASS";
        Class<?> clazz = Class.forName("cn.com.cowain.ibms.enumeration.iot.AuthType");
        Object[] enumConstants = clazz.getEnumConstants();

        Optional<Object> select = Arrays.stream(enumConstants)
                .filter(item -> item.toString().equalsIgnoreCase(name))
                .findFirst();

        if (select.isPresent()) {
            AuthType enumValue = (AuthType) select.get();
            System.out.println(enumValue.name());
        }
    }

    @Autowired
    RedisUtil redisUtil;

    @Test
    public void getValue() {
        String key = "ibms:test:device-iot:DOOR_MAGNETIC:01nR0tkA";
        Map<Object, Object> map = redisUtil.hmget(key);
        DoorMagnetic doorMagnetic = DataUtils.mapCastTargetObject(map, DoorMagnetic.class);
        log.info("门磁对象：{}", doorMagnetic);
        log.info("设备的ID为：【{}】", doorMagnetic.getDevice().getId());

    }

    @Autowired
    private SpaceAdminDao spaceAdminDao;
    @Autowired
    DeviceDictionaryComponent deviceDictionaryComponent;

    //    @Autowired
//    private
    @Test
    public void setSpace() {
        List<SpaceAdminRedis> spaceAdminRedisList = redisUtil.lGet("ibms:test:space-admin:CWA3236", 0, -1, SpaceAdminRedis.class);
        List<SpaceAdmin> spaceAdminList = new ArrayList<>();
        spaceAdminRedisList.forEach(spaceAdminRedis -> {
           // deviceDictionaryComponent.setSpaceInRedis(spaceAdminRedis.getSpaceId());
            log.info("在Redis中加入:{}",spaceAdminRedis.getSpaceId());
        });
//        List<Space> byDeviceIds = spaceAdminList.stream().map(SpaceAdmin::getSpace).collect(Collectors.toList());
//        log.info("spaceAdminList：【{}】", spaceAdminList);
//        log.info("空间ID为：【{}】", byDeviceIds);

    }

    @Test
    public void getSpace() {
        List<SpaceAdminRedis> spaceAdminRedisList = redisUtil.lGet("ibms:test:space-admin:CWA3236", 0, -1, SpaceAdminRedis.class);
        List<SpaceAdmin> spaceAdminList = new ArrayList<>();
        spaceAdminRedisList.forEach(spaceAdminRedis -> {
            Space space;
            final String spaceKey = redisUtil.createSpaceAdminRealKey(IConst.SPACE_MODULE, spaceAdminRedis.getSpaceId(), RedisUtil.Separator.COLON);
            Object o = redisUtil.get(spaceKey);
            space=JSON.toJavaObject((JSON) o,Space.class);
            log.info("强转对象：【{}】，转换为：【{}】",o,space);
            SpaceAdmin spaceAdmin=new SpaceAdmin();
            BeanUtils.copyProperties(spaceAdminRedis,spaceAdmin);
            spaceAdmin.setSpace(space);
            spaceAdminList.add(spaceAdmin);
        });
        List<Space> byDeviceIds = spaceAdminList.stream().map(SpaceAdmin::getSpace).collect(Collectors.toList());
        log.info("spaceAdminList：【{}】", spaceAdminList);
        log.info("空间ID为：【{}】", byDeviceIds);

    }
}
