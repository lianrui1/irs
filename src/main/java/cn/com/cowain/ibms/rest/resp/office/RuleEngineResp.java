package cn.com.cowain.ibms.rest.resp.office;

import cn.com.cowain.ibms.enumeration.iot.UsableStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 查询智能办公室规则引擎返回参数
 *
 * @author: yanzy
 * @date: 2022/1/27 14:45
 */

@Data
public class RuleEngineResp implements Serializable {

    //规则引擎id
    @ApiModelProperty(value = "规则引擎id",required = true)
    private String ruleEngineId;

    //规则引擎名称
    @ApiModelProperty(value = "规则引擎名称",required = true)
    private String ruleEngineName;

    //规则引擎logo
    @ApiModelProperty(value = "规则引擎loge",required = true)
    private String ruleEngineLogoImgUrl;

    //规则引擎可用状态
    @ApiModelProperty(value = "规则引擎可用状态",required = true)
    private UsableStatus usableStatus;

    //规则引擎简述
    @ApiModelProperty(value = "规则引擎简述",required = true)
    private String ruleEngineSketch;

    @ApiModelProperty(value = "规则引擎创建时间")
    private LocalDateTime createdTime;
}
