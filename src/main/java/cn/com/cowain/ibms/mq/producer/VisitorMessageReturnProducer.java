package cn.com.cowain.ibms.mq.producer;

import cn.com.cowain.ibms.mq.Topic;
import cn.com.cowain.ibms.mq.bean.BizReturnMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**AbilityServiceImpl
 * @author Yang.Lee
 * @date 2022/3/3 10:14
 */
@Slf4j
@Component
public class VisitorMessageReturnProducer {

    @Resource
    private RocketMQTemplate rocketMQTemplate;

    public ServiceResult push(BizReturnMessage resp, String tag) {

        String topic = Topic.get(Topic.VISITOR_RETURN);
        String messageStr = JSON.toJSONString(resp);

        log.debug("推送数据到topic : {}. 数据： {}", topic, messageStr);
        Message<String> message = new GenericMessage<>(messageStr);
        SendResult sendResult = rocketMQTemplate.syncSend(topic + ":" + tag, message);

        if (!sendResult.getSendStatus().equals(SendStatus.SEND_OK)) {
            log.error("访客消息推送失败：" + sendResult);
            return ServiceResult.error(sendResult.toString());
        }

        return ServiceResult.ok();
    }

}
