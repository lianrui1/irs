package cn.com.cowain.ibms.rest.req.distinguish;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/8/3 9:48
 */
@Data
public class HkResultReq {

    private String uuid;
}
