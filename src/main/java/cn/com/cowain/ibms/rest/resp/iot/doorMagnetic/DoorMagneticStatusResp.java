package cn.com.cowain.ibms.rest.resp.iot.doorMagnetic;

import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2022/7/7 17:32
 */
@Builder
//@AllArgsConstructor
//@NoArgsConstructor
@ApiModel("门磁状态响应对象")
@Data
public class DoorMagneticStatusResp {

    @ApiModelProperty(value = "设备状态。ONLINE：设备在线。OFFLINE：设备离线。ABNORMAL：设备异常。INACTIVE：设备未激活。FROZEN：设备冻结",
            required = true, example = "ONLINE")
    private DeviceStatus status;

    @ApiModelProperty(value = "设备id", example = "aa123-bb123")
    private String deviceId;

    @ApiModelProperty(value = "设备名称", example = "设备A")
    private String deviceName;

    @ApiModelProperty(value = "设备SN", example = "123ABCabc")
    private String sn;

    @ApiModelProperty(value = "设备位置", example = "昆山新工厂 / 1F / 办公室X")
    private String address;
}
