package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author wei.cheng
 * @date 2022/04/15 15:52
 */
@Data
@ApiModel("创建警戒点请求参数")
public class AlertSpotCreateReq {

    @ApiModelProperty(value = "名称", required = true)
    @NotBlank
    private String name;

    @ApiModelProperty(value = "具体位置", required = true)
    @NotBlank
    private String address;

    @ApiModelProperty(value = "设备ID", required = true)
    @NotBlank
    private String deviceId;

    @ApiModelProperty(value = "所属空间ID", required = true)
    @NotBlank
    private String spaceId;

    @ApiModelProperty(value = "流媒体地址")
    private String msUrl;
}
