package cn.com.cowain.ibms.exceptions;

import cn.com.cowain.ibms.aop.FieldErrorWrapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ValidationFailedExceptionTest
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/21/20
 */
@SpringBootTest
class ValidationFailedExceptionTest {

    @Test
    void getFieldErrorWrapper() {
        FieldErrorWrapper mock = Mockito.mock(FieldErrorWrapper.class);
        Map<String, List<String>> map = new HashMap<>();
        map.put("key", Arrays.asList("aa", "bb"));
        mock.setErrMsg(map);
        ValidationFailedException exception = new ValidationFailedException(mock);
        Assertions.assertNotNull(exception.getFieldErrorWrapper());
    }

}
