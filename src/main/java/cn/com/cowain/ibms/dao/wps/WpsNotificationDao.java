package cn.com.cowain.ibms.dao.wps;

import cn.com.cowain.ibms.entity.wps.WpsNotification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author wei.cheng
 * @date 2022/02/23 19:02
 */
@Repository
public interface WpsNotificationDao extends JpaRepository<WpsNotification, String> {
}
