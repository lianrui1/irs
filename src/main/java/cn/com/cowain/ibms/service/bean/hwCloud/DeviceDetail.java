package cn.com.cowain.ibms.service.bean.hwCloud;

import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.utils.DateUtils;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Yang.Lee
 * @date 2022/7/13 14:11
 */
@Data
@ApiModel("华为云设备详情对象")
public class DeviceDetail {

    @JSONField(name = "device_id")
    @JsonProperty("device_id")
    private String deviceId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateUtils.PATTERN_DATETIME2_WITH_ZONE)
    @JSONField(name = "create_time")
    @JsonProperty("create_time")
    private LocalDateTime createTime;

    @JSONField(name = "auth_info")
    @JsonProperty("auth_info")
    private AuthInfo authInfo;

    @JSONField(name = "product_name")
    @JsonProperty("product_name")
    private String productName;

    @JSONField(name = "gateway_id")
    @JsonProperty("gateway_id")
    private String gatewayId;

    private List<Object> tags;

    @JSONField(name = "app_name")
    @JsonProperty("app_name")
    private String appName;

    @JSONField(name = "device_name")
    @JsonProperty("device_name")
    private String deviceName;

    @JSONField(name = "node_type")
    @JsonProperty("node_type")
    private String nodeType;

    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private String productId;

    @JSONField(name = "app_id")
    @JsonProperty("app_id")
    private String appId;

    @JSONField(name = "node_id")
    @JsonProperty("node_id")
    private String nodeId;

    private DeviceStatus status;

    @Data
    public static class AuthInfo{

        @JSONField(name = "auth_type")
        private String authType;

        @JSONField(name = "secure_access")
        private Boolean secureAccess;

        private String secret;

        private Long timeout;
    }
}
