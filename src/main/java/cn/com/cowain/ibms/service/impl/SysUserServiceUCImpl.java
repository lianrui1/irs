package cn.com.cowain.ibms.service.impl;

import cn.com.cowain.ibms.bo.SimpleSysUser;
import cn.com.cowain.ibms.dao.SysUserDao;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.feign.bean.ehr.CheckLabourResp;
import cn.com.cowain.ibms.feign.bean.pass_v2.PassV2UserResp;
import cn.com.cowain.ibms.feign.ehr.EHRAPi;
import cn.com.cowain.ibms.rest.resp.EhrVisitorResp;
import cn.com.cowain.ibms.rest.resp.distinguish.StaffPageResp;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.utils.RedisUtil;
import cn.com.cowain.ibms.utils.RestTemplateUtil;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.sfp.comm.utils.JwtUtil;
import cn.com.cowain.sfp.xunfei.util.EncryptUtil;
import cn.com.cowain.usercenter.api.service.UserCenterStaffApi;
import cn.com.cowain.usercenter.common.resp.OrganizationToTreeResp;
import cn.com.cowain.usercenter.common.resp.PassportRespForFeign;
import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/**
 * 用户Service， UC实现类
 *
 * @author Yang.Lee
 * @date 2021/2/4 16:19
 */
@Slf4j
@Service("sysUserServiceUC")
public class SysUserServiceUCImpl implements SysUserService {
    @Resource
    private UserCenterStaffApi userCenterStaffApi;

    @Resource
    private SysUserDao sysUserDao;

    @Autowired
    private RestTemplateUtil restTemplateUtil;

    @Resource
    private EHRAPi ehraPi;

    @Value("${cowain.ehr.visitor.url}")
    private String visitorUrl;

    @Value("${cowain.passV2.userInfo.url}")
    private String getUserInfoByUIdUrl;

    @Resource
    private RedisUtil redisUtil;
//    /**
//     * 保存用户信息(根据 stId)
//     * 该接口在对接UC后已弃用
//     *
//     * @param stId
//     * @return
//     * @author 张宇鑫
//     */
//    @Override
//    public SysUser trySaveIfNeed(String stId) {
//
//        SysUser sysUser = getUserByStId(Long.parseLong(stId));
//
//        Optional<SysUser> sysUserOptional = sysUserDao.findByEmpNo(sysUser.getEmpNo());
//        if (!sysUserOptional.isPresent()) {
//            sysUserDao.save(sysUser);
//        }
//
//        return sysUser;
//    }

//    /**
//     * 将用户信息 存入 redis mysql
//     *
//     * @param sysUser
//     * @return
//     * @author 张宇鑫
//     */
//    @Override
//    public void tryToCreateRedis(SysUser sysUser) {
//
//    }

//    /**
//     * 根据sysId 获取用户信息
//     *
//     * @param sysId
//     * @return
//     * @author 张宇鑫
//     */
//    @Override
//    public SysUser getSysUserInfo(String sysId) {
//        return null;
//    }

//    /**
//     * 根据userId 获取用户信息
//     *
//     * @param userId
//     * @return 用户信息
//     * @author cem
//     */
//
//    @Override
//    public SysUser trySaveIfNeedWithUserId(String userId) {
//        return null;
//    }

    /**
     * 通过工号查询用户信息，并存入数据库中
     *
     * @param empNo : 员工工号
     * @return 员工信息
     * @Author Yang.Lee
     * @date 2021/3/3 10:52
     **/
    @Override
    @Transactional
    public SysUser trySaveIfNeed(String empNo) {
        SysUser sysUser = getUserByEmpNo(empNo);

        Optional<SysUser> sysUserOptional = sysUserDao.findByEmpNo(sysUser.getEmpNo());
        if (!sysUserOptional.isPresent()) {
            sysUserDao.save(sysUser);
        }

        return sysUser;
    }

    /**
     * 根据员工编号获取员工信息
     *
     * @param empNo 员工编号
     * @return 员工数据
     */
    @Override
    public SysUser getUserByEmpNo(String empNo) {

        SysUser sysUser;
        Optional<SysUser> sysUserOptional = sysUserDao.findByEmpNo(empNo);

        // 从redis 中查询数据，如果没有则从UC同步
        if (sysUserOptional.isEmpty()) {

            sysUser = syncUserByEmpNoAndSave(empNo);
            if (sysUser == null) return null;

        } else {
            sysUser = sysUserOptional.get();
        }
        return sysUser;
    }

    /**
     * 通过员工编号同步用户数据，并保存
     *
     * @param empNo
     * @return
     */
    private SysUser syncUserByEmpNoAndSave(String empNo) {
        SysUser sysUser;
        sysUser = syncUserByEmpNo(empNo);// 如果获取到的数据不是null，则存入缓存
        if (sysUser != null) {
            sysUserDao.save(sysUser);
        } else {
            log.error("从UC同步用户数据异常！！！工号：{}", empNo);
            return null;
        }
        return sysUser;
    }

    /**
     * 根据token获取用户信息
     *
     * @param token 登录后返回的token令牌
     * @return 员工信息
     */
    @Override
    @Transactional
    public SysUser getUserByToken(String token) {

        String hrId = JwtUtil.getHrId(token);
        return getUserByEmpNo(hrId);
    }

    /**
     * 获取所有用户信息（包含所有员工及访客）
     *
     * @return 员工列表
     * @author Yang.Lee
     * @date 2021/12/14 10:57
     **/
    @Override
    public List<StaffPageResp> syncStaffList() {

        // 向ehr获取所有访客信息
        List<StaffPageResp> staffList = new ArrayList<>(syncGuestList());

        try {
            // 获取所有员工
            JsonResult<List<PassportRespForFeign>> result = userCenterStaffApi.passportPage();
            if (result.getStatus() != 1) {
                log.error("从UC同步部员工数据失败！！！UC返回信息：" + result);
            } else {
                List<PassportRespForFeign> ucStaffList = result.getData();
                ucStaffList.forEach(it -> {
                    StaffPageResp staff = new StaffPageResp();
                    staff.setId(EncryptUtil.md5(it.getJobNo()));
                    staff.setEmpNo(it.getJobNo());
                    staff.setName(it.getRealName());
                    staff.setDept(String.valueOf(it.getDeptId()));
                    staff.setIsStaff(0);
                    staffList.add(staff);
                });
            }
        } catch (Exception e) {
            log.error("向UC拉取员工信息失败");
            log.error(e.getMessage());
        }

        return staffList;
    }

    @Override
    public List<StaffPageResp> syncGuestList() {
        List<StaffPageResp> staffList = new ArrayList<>();

        // 向ehr获取所有访客信息
        try {
            log.info("向EHR拉取所有访客信息");
            String data = restTemplateUtil.get(visitorUrl);
            JSONObject jsonData = JSON.parseObject(data);
            JSONArray jsonIndex = jsonData.getJSONArray("list");
            List<EhrVisitorResp> list = JSON.parseArray(jsonIndex.toJSONString(), EhrVisitorResp.class);
            list.forEach(it -> {
                StaffPageResp staff = new StaffPageResp();
                staff.setId(EncryptUtil.md5(it.getJobNum()));
                staff.setEmpNo(it.getJobNum());
                staff.setName(it.getName());
                staff.setDept("");
                staff.setMobile(it.getPhone());
                staff.setIsStaff(1);
                staffList.add(staff);
            });
        } catch (Exception e) {
            log.error("向EHR拉取访客信息失败");
            log.error(e.getMessage());
        }
        return staffList;
    }

    /**
     * 根据Uid查询用户信息
     *
     * @param uid ucId
     * @return 用户数据，查询不到时返回Optional.empty()
     * @author Yang.Lee
     * @date 2022/7/14 14:58
     **/
    @Override
    public Optional<PassV2UserResp> getUserByUid(String uid) {

        // 先从缓存中获取信息，如果没有则去远程获取
        String realKey = redisUtil.createRealKey("ucUser-" + uid);

        /*if(redisUtil.hasKey(realKey)){

            String val = redisUtil.get(realKey).toString();
            PassV2UserResp resp = JSON.parseObject( val, PassV2UserResp.class);
            return Optional.of(resp);
        }*/

        // 发送请求到通行2.0查询
        String url = getUserInfoByUIdUrl.replace("{uid}", uid);
        JsonResult<PassV2UserResp> result = restTemplateUtil.get(url, new ParameterizedTypeReference<JsonResult<PassV2UserResp>>() {
        });

        redisUtil.set(realKey, result.getData(), RedisUtil.FIFTEEN_MIN_SECOND);

        if(result.getStatus() == 1){
            return Optional.of(result.getData());
        }

        return Optional.empty();
    }

    /**
     * 通过员工编号同步用户数据
     *
     * @param empNo 员工编号
     * @return 员工信息
     */
    public SysUser syncUserByEmpNo(String empNo) {

        JsonResult<PassportRespForFeign> result = userCenterStaffApi.passport(empNo);

        if (result.getStatus() == 1) {

            PassportRespForFeign data = result.getData();
            SysUser sysUser = convert(data);

            JsonResult<OrganizationToTreeResp> fullOrgResult = userCenterStaffApi.listByJobnoForUp(empNo);
            if (fullOrgResult.getStatus() == 1) {
                OrganizationToTreeResp organizationToTreeResp = fullOrgResult.getData();

                if (organizationToTreeResp.getChildren() != null && sysUser != null) {
                    sysUser.setFullDepartmentName(getFullOrgName(organizationToTreeResp.getChildren().get(0)));
                }
            }

            return sysUser;
        } else {
            log.error("从UC同步用户数据失败！！！UC返回信息：" + result);
            return null;
        }
    }

    private String getFullOrgName(OrganizationToTreeResp organizationToTreeResp) {

        StringBuilder sb = new StringBuilder(organizationToTreeResp.getOrgName());
        List<OrganizationToTreeResp> childrenList = organizationToTreeResp.getChildren();

        if (childrenList != null) {
            childrenList.forEach(obj ->
                sb.append("/").append(getFullOrgName(obj))
            );
        }

        return sb.toString();
    }

    /**
     * 对象转换
     *
     * @param data 被转转的对象，UC返回的对象
     * @return SysUser对象
     */
    private SysUser convert(PassportRespForFeign data) {

        if (data == null) {
            return null;
        }

        SysUser sysUser = new SysUser();

        sysUser.setSysId(data.getUserId());
        sysUser.setDeptId(data.getDepartCode());
        sysUser.setDeptName(data.getDepartName());
        sysUser.setEmpNo(data.getJobNo());
        sysUser.setNameZh(data.getRealName());
        sysUser.setJobRankName(data.getStPositionName());
        sysUser.setNameEn(data.getEnName());
        sysUser.setWxOpenId(data.getOpenId());
        sysUser.setHeadImgUrl(data.getHeadImgUrl());
        sysUser.setMobile(data.getMobile());
        return sysUser;
    }

    @Override
    public Boolean isLabour(String hrId) {
        try {
            JsonResult<CheckLabourResp> resp = ehraPi.checkLabourResp(hrId);
            if (resp.getStatus() != 1) {
                log.error("请求ehr查询人员是否是劳务异常, msg:{}", resp.getMsg());
                return false;
            } else {
                return resp.getData().getLabour();
            }
        } catch (Exception e) {
            log.error("请求ehr查询人员是否是劳务异常", e);
        }
        return false;
    }

    @Override
    public Map<String, String> batchGetNameByEmpNos(List<String> empNos) {
        Map<String,String> map = new HashMap<>();
        if(CollUtil.isEmpty(empNos)){
            return map;
        }
        List<SimpleSysUser> simpleSysUsers = sysUserDao.findEmpNpAndNameZh(empNos);
        simpleSysUsers.forEach(simpleSysUser -> map.put(simpleSysUser.getEmpNo(), simpleSysUser.getNameZh()));
        List<String> notFoundEmpNos = ListUtils.removeAll(empNos, map.keySet());
        notFoundEmpNos.forEach(empNo->{
            SysUser sysUser = syncUserByEmpNoAndSave(empNo);
            if(Objects.nonNull(sysUser)){
                map.put(empNo, sysUser.getNameZh());
            }
        });
        return map;
    }
}
