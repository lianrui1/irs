package cn.com.cowain.ibms.entity.meeting;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.ReservationRecord;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 用户会议签到Entity
 *
 * @author Yang.Lee
 * @date 2021/6/8 18:06
 */
@Data
@Entity
@Table(name = "ibms_meeting_attendance")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_meeting_attendance", comment = "会议签到表")
@Where(clause = "is_del=0")
public class Attendance extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "record_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 会议ID'")
    private ReservationRecord reservationRecord;

    @Column(name = "user_emp_no", nullable = false, columnDefinition = "varchar(36) COMMENT '签到用户工号'")
    private String userEmpNo;
}
