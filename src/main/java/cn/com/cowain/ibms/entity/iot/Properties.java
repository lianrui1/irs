package cn.com.cowain.ibms.entity.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/8 10:49
 */
@Data
@ApiModel("空间属性")
public class Properties implements Serializable {

    @ApiModelProperty(value = "面积(m2)")
    private String spaceArea;

    @Length(max = 10)
    @Pattern(regexp = "^[a-z0-9A-Z]{0,10}$", message = "空间编号校验失败，只允许输入英文数字，且长度不超过10")
    @ApiModelProperty(value = "空间编号", required = true)
    private String spaceNumber;

    @ApiModelProperty(value = "用途类型", required = true)
    private String purposeId;
}
