-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 10.0.1.232    Database: ibms_uat
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `iot_device`
--

DROP TABLE IF EXISTS `iot_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `iot_device` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `created_by` varchar(36) DEFAULT NULL COMMENT '创建人',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(36) DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int(11) DEFAULT NULL COMMENT '版本号',
  `device_name` varchar(50) DEFAULT NULL COMMENT '设备名称',
  `device_type` varchar(50) DEFAULT NULL COMMENT '设备类型类型',
  `password` varchar(50) DEFAULT NULL COMMENT '设备密码',
  `sn` varchar(50) DEFAULT NULL COMMENT 'SN码',
  `status` tinyint(1) NOT NULL COMMENT '设备状态 0:禁用;1:启用',
  `product_id` varchar(36) DEFAULT NULL COMMENT 'FK',
  `project_id` varchar(36) DEFAULT NULL COMMENT 'FK',
  `space_id` varchar(36) DEFAULT NULL COMMENT 'FK',
  `device_no` int(4) DEFAULT NULL COMMENT '设备编号',
  `iot_node_type` varchar(50) DEFAULT NULL COMMENT 'IOT网络节点 类型',
  `seq` int(11) DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FKcy01v8t61d9sp78mhrewha089` (`product_id`) USING BTREE,
  KEY `FKnmyhjnlnfada4l3x6evhemfpe` (`project_id`) USING BTREE,
  KEY `FKt5mrehslkbjn6eu9ja91obd7y` (`space_id`) USING BTREE,
  CONSTRAINT `FKcy01v8t61d9sp78mhrewha089` FOREIGN KEY (`product_id`) REFERENCES `iot_product` (`id`),
  CONSTRAINT `FKnmyhjnlnfada4l3x6evhemfpe` FOREIGN KEY (`project_id`) REFERENCES `ibms_project` (`id`),
  CONSTRAINT `FKt5mrehslkbjn6eu9ja91obd7y` FOREIGN KEY (`space_id`) REFERENCES `iot_space` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='iot设备';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `iot_device`
--

LOCK TABLES `iot_device` WRITE;
/*!40000 ALTER TABLE `iot_device` DISABLE KEYS */;
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('06f31666-443c-4d24-ab27-7cc0d7c30b52',NULL,'2021-01-12 17:38:19',1,NULL,'2021-01-12 17:40:20',3,'测试133','DEFAULT',NULL,'0000000001-010001',1,'333','123','877df00f-f3cc-4fd7-9e71-37a1fcc3d40f',1,'GATEWAY',1);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('08ce2a99-c807-4ada-8976-9dec5ace9e88',NULL,'2020-12-30 20:29:00',0,NULL,'2020-12-30 20:29:00',2,'DEFAULT_DOOR_PLATE','DOOR_PLATE','cowain',NULL,1,'333','123','123',0,'DEFAULT',7);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('123',NULL,'2020-12-30 20:05:30',0,NULL,'2020-12-30 20:05:33',3,'123','DOOR_MAGNETIC','cowain',NULL,1,'333','123','123',0,'DEFAULT',8);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('1fc7e9ac-611f-493d-946d-a7fbbc636266',NULL,'2021-01-12 15:45:37',0,NULL,'2021-01-12 15:45:37',2,'DEFAULT_DOOR_PLATE','DOOR_PLATE','cowain',NULL,1,'333','123','17007a67-0272-4f83-bc76-fac755a106b5',0,'DEFAULT',6);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('2f646c72-111f-4610-9b32-61fe8182de3b',NULL,'2021-01-06 18:41:36',0,NULL,'2021-01-06 18:41:36',3,'DEFAULT_DOOR_PLATE','DOOR_PLATE','cowain',NULL,1,'333','123','1f53db55-dc98-444b-8cef-74762900930b',0,'DEFAULT',4);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('3047883a-43de-4364-880b-320e8f5d50b9',NULL,'2021-01-14 15:59:13',0,NULL,'2021-01-14 15:59:13',0,NULL,'DOOR_PLATE','cowain',NULL,1,'333','123','17007a67-0272-4f83-bc76-fac755a106b5',0,'DEFAULT',11);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('3370a4f6-2378-47c3-8513-f400ed19f173',NULL,'2021-01-14 16:03:08',0,NULL,'2021-01-14 16:03:08',0,'table 测试设备1','DEFAULT',NULL,'00000000020010004',1,'1ba90cba-9126-4de0-862d-2353ee8117f6','123','17007a67-0272-4f83-bc76-fac755a106b5',4,'DIRECT_DEVICE',14);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('4150c508-fc0d-407e-8fe4-4a34a8947cd1',NULL,'2021-01-06 18:08:46',0,NULL,'2021-01-06 18:08:46',2,'DEFAULT_DOOR_PLATE','DOOR_PLATE','cowain',NULL,1,'333','123','1f53db55-dc98-444b-8cef-74762900930b',0,'DEFAULT',5);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('45741b54-c080-40b2-a804-13a6024a0aa2',NULL,'2021-01-08 09:23:18',0,NULL,'2021-01-08 09:23:18',3,'DEFAULT_DOOR_PLATE','DOOR_PLATE','cowain',NULL,1,'333','123','17007a67-0272-4f83-bc76-fac755a106b5',0,'DEFAULT',3);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('4bcfd171-e711-4248-9561-5353b3785c33',NULL,'2021-01-14 16:01:05',0,NULL,'2021-01-14 16:01:05',0,'table 测试设备','DEFAULT',NULL,'000006101-010003',1,'333','7e0e3902-129e-4b78-a624-0f3f56efb7b3','f2fa5212-5b2d-4728-abfd-e80913135f4e',3,'GATEWAY',13);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('511a8015-3993-4fa1-8151-f6543bb48d23',NULL,'2021-01-05 15:23:51',0,NULL,'2021-01-05 15:23:51',3,'DEFAULT_DOOR_PLATE','DOOR_PLATE','cowain',NULL,1,'333','123','1f53db55-dc98-444b-8cef-74762900930b',0,'DEFAULT',2);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('5646bc13-31db-4f7a-85bb-f46dd148873a',NULL,'2020-12-31 11:22:46',0,NULL,'2020-12-31 11:22:46',2,'DEFAULT_DOOR_PLATE','DOOR_PLATE','cowain',NULL,1,'333','123','17007a67-0272-4f83-bc76-fac755a106b5',0,'DEFAULT',0);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('6288bd48-cd0f-4da0-9a96-b53527289363',NULL,'2021-01-12 15:42:51',0,NULL,'2021-01-12 15:42:51',3,'DEFAULT_DOOR_PLATE','DOOR_PLATE','cowain',NULL,1,'333','123','17007a67-0272-4f83-bc76-fac755a106b5',0,'DEFAULT',1);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('6a1cd0d1-55f9-4986-92e8-d7458d9847da',NULL,'2021-01-12 15:45:05',0,NULL,'2021-01-12 15:45:05',2,'DEFAULT_DOOR_PLATE','DOOR_PLATE','',NULL,1,'333','123','1f53db55-dc98-444b-8cef-74762900930b',0,'DEFAULT',9);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('716c30e4-4d09-48ed-b986-c1a922a323e4',NULL,'2021-01-13 15:14:10',0,NULL,'2021-01-13 15:14:10',0,NULL,'DOOR_PLATE','cowain',NULL,1,'333','123','17007a67-0272-4f83-bc76-fac755a106b5',0,'DEFAULT',10);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('72aa01a1-7b55-48df-9f7c-c7fc4ccfd3fe',NULL,'2021-01-12 15:45:47',0,NULL,'2021-01-12 15:45:47',0,'DEFAULT_DOOR_PLATE','DOOR_PLATE','cowain',NULL,1,'333','123','1f53db55-dc98-444b-8cef-74762900930b',0,'DEFAULT',0);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('82a54cf4-baa2-499b-af7b-22bb50ac755a',NULL,'2021-01-12 17:03:16',1,NULL,'2021-01-12 17:03:16',0,'哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈','DEFAULT',NULL,'00000000030000001',1,NULL,'123',NULL,1,'GATEWAY_SUB_DEVICE',1);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('8ac1b16d-c38e-4317-8780-702234f8c0d6',NULL,'2021-01-06 17:45:31',0,NULL,'2021-01-06 17:45:31',0,'DEFAULT_DOOR_PLATE','DOOR_PLATE','cowain',NULL,1,'333','123','1f53db55-dc98-444b-8cef-74762900930b',0,'DEFAULT',0);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('9d9ed2ff-054b-4bc1-8d40-2281046971fd',NULL,'2021-01-14 18:28:44',1,NULL,'2021-01-14 18:28:53',1,'table &^%&____12312','DEFAULT',NULL,'000006101-010007',1,'333','7e0e3902-129e-4b78-a624-0f3f56efb7b3','49d2c5c1-8889-404b-afcb-bb0c9e7f650f',7,'GATEWAY',17);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('a2cc3929-6ebd-4952-8d24-0b0c53ca6721',NULL,'2021-01-12 17:42:53',0,NULL,'2021-01-12 17:42:53',4,'设备0112001','DEFAULT',NULL,'0000061010010001',1,'1ba90cba-9126-4de0-862d-2353ee8117f6','7e0e3902-129e-4b78-a624-0f3f56efb7b3','f2fa5212-5b2d-4728-abfd-e80913135f4e',1,'GATEWAY',0);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('d2d351f0-5216-4888-8d93-718eaf5eacf9',NULL,'2021-01-14 16:00:23',0,NULL,'2021-01-14 16:00:23',0,'13123','DEFAULT',NULL,'0000836202-010002',1,'333','0509ceb4-ae32-4703-a6dd-6f56168d8387',NULL,2,'DIRECT_DEVICE',12);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('e1068166-8389-4d87-b67b-4fde4f4f467a',NULL,'2020-12-30 20:16:22',1,NULL,'2020-12-30 20:16:22',0,'DEFAULT_DOOR_PLATE','DOOR_PLATE','cowain',NULL,1,'333','123','877df00f-f3cc-4fd7-9e71-37a1fcc3d40f',0,'DEFAULT',0);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('e28739ed-358c-4770-aadd-13095490e5eb',NULL,'2021-01-21 16:23:18',0,NULL,'2021-01-21 16:23:18',0,NULL,'DOOR_PLATE','cowain',NULL,1,'333','123','1f53db55-dc98-444b-8cef-74762900930b',0,'DEFAULT',18);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('e36cbbbe-665b-4616-a221-8b94a41a855a',NULL,'2021-01-21 16:23:16',0,NULL,'2021-01-21 16:23:16',0,NULL,'DOOR_PLATE','cowain',NULL,1,'333','123','1f53db55-dc98-444b-8cef-74762900930b',0,'DEFAULT',17);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('e7065df1-100a-4864-934b-b6e026d3c223',NULL,'2021-01-13 09:37:50',1,NULL,'2021-01-13 09:37:50',0,NULL,'DOOR_PLATE','cowain',NULL,1,'333','123','57452c93-af90-4ba7-bd02-4267eb20c417',0,'DEFAULT',10);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('ea9ccfa2-5ccb-47e4-99b8-924ad5036627',NULL,'2021-01-14 16:09:59',0,NULL,'2021-01-14 16:09:59',0,'table &^%&____123123','DEFAULT',NULL,'00001263020010006',1,'1ba90cba-9126-4de0-862d-2353ee8117f6','1ef2f184-d3c7-4657-bbca-3fcfa7facfd1',NULL,6,'DIRECT_DEVICE',16);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('f2e35d9c-4e4f-47dc-895e-c7c3b2146077',NULL,'2021-01-14 16:09:27',0,NULL,'2021-01-14 16:09:27',0,'213@@@@','DEFAULT',NULL,'00008362020010005',1,'1ba90cba-9126-4de0-862d-2353ee8117f6','0509ceb4-ae32-4703-a6dd-6f56168d8387',NULL,5,'DIRECT_DEVICE',15);
INSERT INTO `iot_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_name`, `device_type`, `password`, `sn`, `status`, `product_id`, `project_id`, `space_id`, `device_no`, `iot_node_type`, `seq`) VALUES ('ff566fe6-12ed-4eed-816f-37d664602dcf',NULL,'2020-12-30 20:02:39',0,NULL,'2020-12-30 20:02:43',1,'huaqiao','DOOR_MAGNETIC','cowain',NULL,1,'333','123','123',0,'DEFAULT',0);
/*!40000 ALTER TABLE `iot_device` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-25 15:53:44
