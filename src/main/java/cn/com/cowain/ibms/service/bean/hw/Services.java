package cn.com.cowain.ibms.service.bean.hw;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/7/18 21:21
 */
@Data
public class Services {
    @JsonProperty(value = "service_id")
    private String serviceId;
    private List<HwProperties> properties;
}
