package cn.com.cowain.ibms.mq.bean;

/**
 * 门磁状态枚举
 * @author Yang.Lee
 * @date 2020/12/7 9:54
 */
public enum DoorMagnetismStatus {

    /**
     * 开门状态
     */
    OPEN("请求开门"),

    CLOSE("请求关门");

    DoorMagnetismStatus(String message){
        this.message = message;
    }

    private String message;

    public String getMessage() {
        return message;
    }
}
