-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 10.0.1.232    Database: ibms_uat
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ibms_room_device`
--

DROP TABLE IF EXISTS `ibms_room_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ibms_room_device` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `created_by` varchar(36) DEFAULT NULL COMMENT '创建人',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(36) DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int(11) DEFAULT NULL COMMENT '版本号',
  `rd_name` varchar(50) NOT NULL COMMENT '名称',
  `rd_spec` varchar(100) DEFAULT NULL COMMENT '规格，预留',
  `room_id` varchar(36) NOT NULL COMMENT 'FK',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FKdtnor7j2h7x76tb8vxpss6j9v` (`room_id`) USING BTREE,
  CONSTRAINT `FKdtnor7j2h7x76tb8vxpss6j9v` FOREIGN KEY (`room_id`) REFERENCES `ibms_room` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='会议室设备';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ibms_room_device`
--

LOCK TABLES `ibms_room_device` WRITE;
/*!40000 ALTER TABLE `ibms_room_device` DISABLE KEYS */;
INSERT INTO `ibms_room_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `rd_name`, `rd_spec`, `room_id`) VALUES ('0fb7667e-9ba1-4b20-9dbc-e63576f687b7',NULL,'2020-12-30 20:24:26',0,NULL,'2020-12-30 20:24:26',0,'电视',NULL,'e8e1e58b-6db8-4ca4-8133-7146d70a2177');
INSERT INTO `ibms_room_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `rd_name`, `rd_spec`, `room_id`) VALUES ('1a745d98-23b2-4eae-bd9e-6d01afe6a5b9',NULL,'2020-12-31 08:10:19',0,NULL,'2020-12-31 08:10:19',0,'投影仪',NULL,'4af6f5da-98f0-4fcc-9120-2b7f12b74426');
INSERT INTO `ibms_room_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `rd_name`, `rd_spec`, `room_id`) VALUES ('245e337c-7b31-4a27-9c09-4aaf8884e48f',NULL,'2020-12-30 20:23:57',0,NULL,'2020-12-30 20:23:57',0,'投影仪',NULL,'11de355e-dd8e-4b3a-b6c7-483874d2cac2');
INSERT INTO `ibms_room_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `rd_name`, `rd_spec`, `room_id`) VALUES ('27d166fc-541b-42b1-b22b-9cc33fd38d47',NULL,'2020-12-30 20:23:18',0,NULL,'2020-12-30 20:23:18',0,'电视',NULL,'ed2dc26f-cd02-4142-86e1-eee8e0753d63');
INSERT INTO `ibms_room_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `rd_name`, `rd_spec`, `room_id`) VALUES ('a5adadd4-357e-4654-9d20-77071a70de0f',NULL,'2020-12-30 20:24:26',0,NULL,'2020-12-30 20:24:26',0,'投影仪',NULL,'e8e1e58b-6db8-4ca4-8133-7146d70a2177');
INSERT INTO `ibms_room_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `rd_name`, `rd_spec`, `room_id`) VALUES ('b18a3bfd-21a2-4b67-acee-ed1cd2edbcda',NULL,'2020-12-30 20:23:18',0,NULL,'2020-12-30 20:23:18',0,'空调',NULL,'ed2dc26f-cd02-4142-86e1-eee8e0753d63');
INSERT INTO `ibms_room_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `rd_name`, `rd_spec`, `room_id`) VALUES ('b6116d97-d14c-44cf-ba52-e6fe94a7e108',NULL,'2020-12-31 08:10:19',0,NULL,'2020-12-31 08:10:19',0,'空调',NULL,'4af6f5da-98f0-4fcc-9120-2b7f12b74426');
INSERT INTO `ibms_room_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `rd_name`, `rd_spec`, `room_id`) VALUES ('cdd8e042-6b64-4f14-8987-d48dd9dc9b94',NULL,'2020-12-31 08:10:19',0,NULL,'2020-12-31 08:10:19',0,'LED大屏',NULL,'4af6f5da-98f0-4fcc-9120-2b7f12b74426');
INSERT INTO `ibms_room_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `rd_name`, `rd_spec`, `room_id`) VALUES ('f3d3f729-8b4d-462b-9108-8e95493573f2',NULL,'2020-12-30 20:24:26',0,NULL,'2020-12-30 20:24:26',0,'空调',NULL,'e8e1e58b-6db8-4ca4-8133-7146d70a2177');
INSERT INTO `ibms_room_device` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `rd_name`, `rd_spec`, `room_id`) VALUES ('ff75e2d3-3faf-44e1-bc4e-68a9ecc53034',NULL,'2020-12-30 20:23:57',0,NULL,'2020-12-30 20:23:57',0,'空调',NULL,'11de355e-dd8e-4b3a-b6c7-483874d2cac2');
/*!40000 ALTER TABLE `ibms_room_device` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-25 15:53:42
