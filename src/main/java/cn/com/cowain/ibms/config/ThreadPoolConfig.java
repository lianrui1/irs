package cn.com.cowain.ibms.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/17/20
 */
@Slf4j
@Configuration
public class ThreadPoolConfig {

    public ThreadPoolConfig() {
        log.info("ThreadPoolConfig()...");
    }

    @Bean
    public ThreadPoolTaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
        // 线程池维护线程的最少数量
        pool.setCorePoolSize(5);
        // 线程池维护线程的最大数量
        pool.setMaxPoolSize(200);
        // shutdown 时等待任务完成
        pool.setWaitForTasksToCompleteOnShutdown(true);
        return pool;
    }

}
