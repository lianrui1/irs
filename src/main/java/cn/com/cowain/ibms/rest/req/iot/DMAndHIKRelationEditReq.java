package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * 门磁和面板机关系编辑请求对象
 *
 * @author Yang.Lee
 * @date 2021/10/11 10:11
 */
@Data
@ApiModel("门磁和面板机关系编辑请求对象")
public class DMAndHIKRelationEditReq {

    @NotBlank
    @ApiModelProperty(value = "门磁设备id", required = true, example = "1", position = 1)
    private String doorMagneticDeviceId;

    @ApiModelProperty(value = "海康面板机ID")
    private List<String> hikDeviceIdList;
}
