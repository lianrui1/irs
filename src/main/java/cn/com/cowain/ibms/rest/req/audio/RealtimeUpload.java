package cn.com.cowain.ibms.rest.req.audio;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalTime;

/**
 * @author Yang.Lee
 * @date 2021/2/1 9:58
 */
@Data
@ApiModel("实时转写补上传文件参数对象")
public class RealtimeUpload {

    @ApiModelProperty(value = "音频文件，pcm格式", required = true)
    private MultipartFile file;

    @DateTimeFormat(pattern = "HH:mm:ss" )
    @ApiModelProperty(value = "音频相对于整个会议的开始时间", required = true, example = "00:00:02", position = 1)
    private LocalTime startTime;

    @DateTimeFormat(pattern = "HH:mm:ss" )
    @ApiModelProperty(value = "音频相对于整个会议的结束时间", required = true, example = "01:01:03", position = 2)
    private LocalTime endTime;
}
