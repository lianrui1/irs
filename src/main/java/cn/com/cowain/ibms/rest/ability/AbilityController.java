package cn.com.cowain.ibms.rest.ability;

import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.ability.AbilityReq;
import cn.com.cowain.ibms.rest.resp.ability.AbilityDetailResp;
import cn.com.cowain.ibms.rest.resp.ability.AbilityNodeResp;
import cn.com.cowain.ibms.rest.resp.space.ProjectSpaceTreeResp;
import cn.com.cowain.ibms.service.ability.AbilityService;
import cn.com.cowain.ibms.service.ability.OpenAbilityAccessControlSpotService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author feng
 * @title: AbilityController
 * @projectName ibms
 * @Date 2022/1/11 10:22
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/ability")
@Api(tags = IConst.MODULE_ABILITY)
public class AbilityController {

    @Resource
    private OpenAbilityAccessControlSpotService openAbilityAccessControlSpotService;
    @Resource
    private AbilityService abilityService;

    @PostMapping("/detail/{id}")
    @ApiOperation(value = "能力配置详情", tags = {IConst.MODULE_ABILITY, IConst.V23})
    public ResponseEntity<JsonResult<AbilityDetailResp>> basicAbilityDetail(@PathVariable @ApiParam("基础信息id") String id) {

        ServiceResult result = abilityService.findDetail(id);
        if (result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.ok("OK", (AbilityDetailResp) result.getObject()));
        } else {
            return ResponseEntity.ok(JsonResult.error(result.getObject().toString(), null, ErrConst.E99));
        }
    }

    /**
     * 获取能力下绑定的门禁点树
     *
     * @param openAbilityId 开发能力id
     * @return 门禁点树
     * @author yanzy
     * @date 2022-01-14
     */
    @GetMapping("/treelist/hasdevice/{openAbilityId}")
    @ApiOperation(value = "详情页--左侧树", tags = {IConst.MODULE_ABILITY, IConst.V23})
    public ResponseEntity<JsonResult<List<ProjectSpaceTreeResp>>> hasDeviceTree(@PathVariable @ApiParam(name = "openAbilityId", value = "开发能力id", required = true) String openAbilityId) {
        return ResponseEntity.ok(JsonResult.ok("OK", openAbilityAccessControlSpotService.getHasDeviceTree(openAbilityId)));

    }

    /**
     * 获取能力下未绑定的门禁点树
     *
     * @param openAbilityId 开发能力id
     * @return 门禁点树
     * @author yanzy
     * @date 2022-01-18
     */
    @GetMapping("/treelist/nobind/{openAbilityId}")
    @ApiOperation(value = "绑定页面--左侧树", tags = {IConst.MODULE_ABILITY, IConst.V23})
    public ResponseEntity<JsonResult<List<ProjectSpaceTreeResp>>> noBind(@PathVariable @ApiParam(name = "openAbilityId", value = "开发能力id", required = true) String openAbilityId) {
        return ResponseEntity.ok(JsonResult.ok("OK", openAbilityAccessControlSpotService.getNoBind(openAbilityId)));
    }

    @PostMapping("/detail/devicedetaillist")
    @ApiOperation(value = "详情分页查询", tags = {IConst.MODULE_ABILITY, IConst.V23})
    public ResponseEntity<JsonResult<PageBean<AbilityNodeResp>>> deviceList(@RequestBody AbilityReq req) {
        return ResponseEntity.ok(JsonResult.ok(abilityService.getAccessSpotController(req, true)));
    }

    @PostMapping("/detail/nobinddetaillist")
    @ApiOperation(value = "绑定分页查询", tags = {IConst.MODULE_ABILITY, IConst.V23})
    public ResponseEntity<JsonResult<PageBean<AbilityNodeResp>>> noBindingDetail(@RequestBody AbilityReq req) {
        //绑定分页的查询，需要限制一下
        return ResponseEntity.ok(JsonResult.ok(abilityService.getAccessSpotController(req, false)));
    }

    @PostMapping("/detail/bind")
    @ApiOperation(value = "能力与设备绑定", tags = {IConst.MODULE_ABILITY, IConst.V23})
    public ResponseEntity<JsonResult<String>> bindAbility(@RequestBody List<AbilityReq> req) {
        ServiceResult result = abilityService.bindingSpot(req);
        if (result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.ok(null));
        } else {
            JsonResult<String> jsonResult = JsonResult.error(result.getObject().toString(), null, result.getErrorCode());
            return ResponseEntity.ok(jsonResult);
        }
    }

    @PostMapping("/delete/{id}")
    @ApiOperation(value = "解绑", tags = {IConst.MODULE_ABILITY, IConst.V23})
    public ResponseEntity<JsonResult<String>> deleteBindAbility(@PathVariable @ApiParam("同行权限id") String id) {
        ServiceResult result = abilityService.deleteBindAbility(id);
        if (result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.ok(null));
        } else {
            JsonResult<String> jsonResult = JsonResult.error(result.getObject().toString(), null, ErrConst.E99);
            return ResponseEntity.ok(jsonResult);
        }
    }


    @GetMapping("/list")
    @ApiOperation(value = "获取能力列表", tags = {IConst.MODULE_IOTC_APPLICATION, IConst.V23})
    public ResponseEntity<JsonResult<List<AbilityDetailResp>>> getList() {

        return ResponseEntity.ok(JsonResult.ok(abilityService.getList()));
    }
}
