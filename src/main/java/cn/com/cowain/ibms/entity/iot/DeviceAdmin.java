package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 设备管理员
 *
 * @author Yang.Lee
 * @date 2022/7/15 9:36
 */
@Data
@Entity
@Table(name = "iot_device_admin")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_device_admin", comment = "设备管理员表")
public class DeviceAdmin extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "device_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK。设备表主键id'")
    private IotDevice device;

    @Column(name = "admin_hr_id", nullable = false, columnDefinition = "datetime COMMENT '管理员工号'")
    private String adminHrId;

    @Column(name = "admin_name", nullable = false, columnDefinition = "datetime COMMENT '管理员姓名'")
    private String adminName;
}
