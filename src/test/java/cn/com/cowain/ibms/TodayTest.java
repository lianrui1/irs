package cn.com.cowain.ibms;

import cn.com.cowain.ibms.dao.iot.*;
import cn.com.cowain.ibms.entity.iot.AccessControlSpot;
import cn.com.cowain.ibms.entity.iot.AccessControlSpotAccessRecord;
import cn.com.cowain.ibms.entity.iot.DeviceAccessRecord;
import cn.com.cowain.ibms.entity.space.IotProduct;
import cn.com.cowain.ibms.enumeration.iot.DeviceAccessRecordType;
import cn.com.cowain.ibms.enumeration.iot.PersonType;
import cn.com.cowain.ibms.rest.resp.device.HwProductsResp;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.DigestUtils;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * DdlTool
 */
public class TodayTest {

    @Resource
    private RestTemplate restTemplate;

    @Resource
    private DeviceDao deviceDao;

    @Resource
    private AccessControlSpotDao accessControlSpotDao;

    @Resource
    private DeviceAccessRecordDao deviceAccessRecordDao;

    @Resource
    private AccessControlSpotAccessRecordDao accessControlSpotAccessRecordDao;

    @Test
    public void generateTables() {

        LocalDateTime now = LocalDateTime.now();
        String s = now.toString();
        System.out.println("模拟数据");
        Optional<AccessControlSpot> byId = accessControlSpotDao.findById("bdda4605-757b-45b5-88af-8e4f9d620d79");

        for (int i = 0; i < 10; i++) {
            DeviceAccessRecord deviceAccessRecord = new DeviceAccessRecord();
            AccessControlSpotAccessRecord accessRecord = new AccessControlSpotAccessRecord();
            deviceAccessRecord.setEmpNo("CWA3996");
            deviceAccessRecord.setNameZh("曾西安");
            deviceAccessRecord.setAccessStatus("通行");
            deviceAccessRecord.setDepartment("ROT/PD/POD");
            deviceAccessRecord.setCapturePicture("");
            deviceAccessRecord.setDefaultPicture("");
            deviceAccessRecord.setDataTime(now);
            // 设备id
            deviceAccessRecord.setIotDevice(byId.get().getDevice());
            // 设备来源
            deviceAccessRecord.setRecordType(DeviceAccessRecordType.MEGVII);
            // 员工类型
            deviceAccessRecord.setStaffType(PersonType.EMPLOYEE);

            // 设备通行记录
            deviceAccessRecordDao.save(deviceAccessRecord);
            accessRecord.setAccessControlSpot(byId.get());
            accessRecord.setDeviceAccessRecord(deviceAccessRecord);
            // 门禁点记录
            accessControlSpotAccessRecordDao.save(accessRecord);
        }


        System.out.println(s);
    }

    @Test
    public void test1() {
        DataFactory dataFactory = new DataFactory();
        dataFactory.getName();
        dataFactory.getAddress();
        dataFactory.getBirthDate();
        dataFactory.getEmailAddress();
        dataFactory.getCity();
    }

    @Test
    public void test2() {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyyMMdd'T'HHmmss'Z'");
        List<IotProduct> productsList = new ArrayList<>();
        // 发送Http请求
        HttpHeaders headers = new HttpHeaders();
        RestTemplate restTemplate = new RestTemplate();
        // 获取华为云产品列表
        HttpEntity<IotProduct> requestGet = new HttpEntity<>(headers);
        ResponseEntity<String> response = restTemplate.exchange("http://test.ehrapi.cowain.cn/iot-conn/api/v1/telemetry/get/products", HttpMethod.GET, requestGet, String.class);
        String body = response.getBody();
        // 解析数据
        JSONObject jsonObject = JSONObject.parseObject(body);
        JSONObject jsonData = jsonObject.getJSONObject("data");
        JSONArray jsonIndex = jsonData.getJSONArray("products");
        List<HwProductsResp> list = JSONObject.parseArray(jsonIndex.toJSONString(), HwProductsResp.class);
        for (HwProductsResp hwProducts : list) {
            IotProduct iotProduct = new IotProduct();
            BeanUtils.copyProperties(hwProducts, iotProduct);
            iotProduct.setHwProductId(hwProducts.getProductId());
            iotProduct.setHwName(hwProducts.getName());
            iotProduct.setHwDeviceType(hwProducts.getDeviceType());
            iotProduct.setHwProtocolType(hwProducts.getProtocolType());
            iotProduct.setHwDataFormat(hwProducts.getDataFormat());
            iotProduct.setHwManufacturerName(hwProducts.getManufacturerName());
            iotProduct.setHwIndustry(hwProducts.getIndustry());
            iotProduct.setHwDescription(hwProducts.getDescription());
            iotProduct.setHwCreateTime(LocalDateTime.parse(hwProducts.getCreateTime(), df));
            productsList.add(iotProduct);
        }
    }

    @Test
    public void test3() {
        String s = DigestUtils.md5DigestAsHex("1234".getBytes());
        System.out.println(s);
        System.out.println("81dc9bdb52d04dc20036dbd8313ed055");
    }
}
