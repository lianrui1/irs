package cn.com.cowain.ibms.feign;

import cn.com.cowain.sfp.comm.JsonResult;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Yang.Lee
 * @date 2021/10/20 13:45
 */
public interface ResultReader {


    /**
     * 获取jsonResult结果
     *
     * @param jsonResult 查询结果
     * @return 结果
     * @author Yang.Lee
     * @date 2021/9/18 16:08
     **/
    static <E> Optional<E> get(JsonResult<E> jsonResult) {

        Optional<E> result;

        if (jsonResult.getStatus() == 1) {
            result = Optional.ofNullable(jsonResult.getData());
        } else {
            result = Optional.empty();
        }

        return result;
    }

    /**
     * 获取jsonResult结果
     *
     * @param jsonResult 查询结果
     * @return 结果
     * @author Yang.Lee
     * @date 2021/9/18 16:08
     **/
    static <E> List<E> getList(JsonResult<List<E>> jsonResult) {

        if (jsonResult.getStatus() == 1) {
            return jsonResult.getData();
        } else {
            return new ArrayList<>();
        }
    }

    /**
     * 获取jsonResult结果
     *
     * @param jsonResult 查询结果
     * @return 结果
     * @author Yang.Lee
     * @date 2021/9/18 16:08
     **/
    @Nullable
    static <E> E getObject(JsonResult<E> jsonResult) {

        if (jsonResult.getStatus() == 1) {
            return jsonResult.getData();
        } else {
            return null;
        }
    }
}
