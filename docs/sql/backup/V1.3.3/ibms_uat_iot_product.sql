-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 10.0.1.232    Database: ibms_uat
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `iot_product`
--

DROP TABLE IF EXISTS `iot_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `iot_product` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `created_by` varchar(36) DEFAULT NULL COMMENT '创建人',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(36) DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int(11) DEFAULT NULL COMMENT '版本号',
  `device_type` varchar(50) DEFAULT NULL COMMENT '设备类型',
  `iot_node_type` varchar(50) DEFAULT NULL COMMENT 'IOT网络节点 类型',
  `prod_name` varchar(20) NOT NULL COMMENT '产品名称',
  `prod_seq` int(2) DEFAULT NULL COMMENT '排序',
  `product_no` int(3) DEFAULT NULL COMMENT '产品编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='产品';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `iot_product`
--

LOCK TABLES `iot_product` WRITE;
/*!40000 ALTER TABLE `iot_product` DISABLE KEYS */;
INSERT INTO `iot_product` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_type`, `iot_node_type`, `prod_name`, `prod_seq`, `product_no`) VALUES ('1ba90cba-9126-4de0-862d-2353ee8117f6',NULL,'2021-01-12 17:41:25',1,NULL,'2021-01-28 09:29:45',4,NULL,'GATEWAY','产品001测试',0,1);
INSERT INTO `iot_product` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_type`, `iot_node_type`, `prod_name`, `prod_seq`, `product_no`) VALUES ('2838747d-c2e1-472b-8cad-4bbbfa973124',NULL,'2021-01-12 17:43:42',1,NULL,'2021-01-28 09:29:51',2,NULL,'GATEWAY','就是好玩',0,1);
INSERT INTO `iot_product` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_type`, `iot_node_type`, `prod_name`, `prod_seq`, `product_no`) VALUES ('333',NULL,'2020-12-29 16:17:37',0,NULL,'2021-01-27 17:06:57',3,'DOOR_PLATE','DIRECT_DEVICE','DEFAULT_DOORPLATE',-1,-1);
INSERT INTO `iot_product` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_type`, `iot_node_type`, `prod_name`, `prod_seq`, `product_no`) VALUES ('7562048a-89db-4ba3-866e-dff048c3461f',NULL,'2021-01-12 17:42:15',1,NULL,'2021-01-28 09:29:48',2,NULL,'GATEWAY_SUB_DEVICE','产品0112002',0,2);
INSERT INTO `iot_product` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_type`, `iot_node_type`, `prod_name`, `prod_seq`, `product_no`) VALUES ('75feceb1-b841-48d5-a8d2-3e0034787ded',NULL,'2021-01-12 17:50:38',1,NULL,'2021-01-28 09:29:53',2,NULL,'GATEWAY','产品0118',0,2);
/*!40000 ALTER TABLE `iot_product` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-05 14:21:17
