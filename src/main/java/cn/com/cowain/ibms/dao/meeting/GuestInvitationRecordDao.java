package cn.com.cowain.ibms.dao.meeting;

import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.entity.meeting.GuestInvitationRecord;
import cn.com.cowain.ibms.entity.meeting.InvitationCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GuestInvitationRecordDao extends JpaRepository<GuestInvitationRecord, String> {

    // 根据邀请函及手机号获取邀请记录
    Optional<GuestInvitationRecord> findByInvitationCardAndMobile(InvitationCard invitationCard, String mobile);

    // 根据会议记录获取访客邀请记录
    List<GuestInvitationRecord> findByInvitationCardReservationRecord(ReservationRecord reservationRecord);

    // 根据邀请函及访客邀请记录码获取邀请记录
    Optional<GuestInvitationRecord> findByInvitationCardAndCode(InvitationCard invitationCard, String code);

    // 根据会议记录和手机号获取访客邀请记录
    Optional<GuestInvitationRecord> findByInvitationCardReservationRecordIdAndMobile(String reservationId, String empNo);

    Optional<GuestInvitationRecord> findByInvitationCardReservationRecordIdAndCode(String reservationId, String code);

    // 根据访客code获取邀请记录
    Optional<GuestInvitationRecord> findByCode(String recordCode);

    // 根据邀请人code获取被邀请人
    List<GuestInvitationRecord> findByInviterCode(String inviterCode);
}
