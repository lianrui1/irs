package cn.com.cowain.ibms.rest.resp.office;

import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.rest.req.office.DeviceNameEditReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 传感器智能设备返回参数
 *
 * @author: yanzy
 * @date: 2022/1/27 13:46
 */

@Data
public class SensorDeviceResp {

    //设备id
    @ApiModelProperty(value = "设备id",required = true)
    private String deviceId;

    //设备名称
    @ApiModelProperty(value = "设备名称",required = true)
    private String deviceName;

    //设备类型
    @ApiModelProperty(value = "设备类型",required = true)
    private DeviceType deviceType;

    //设备类型名称
    @ApiModelProperty(value = "设备类型名称",required = true)
    private String deviceTypeName;

    //创建时间
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createdTime;


    @ApiModelProperty(value = "附加子名称", required = true)
    private DeviceNameEditReq.AdditionalName additionalName;

}
