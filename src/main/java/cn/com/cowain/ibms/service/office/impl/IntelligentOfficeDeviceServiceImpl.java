package cn.com.cowain.ibms.service.office.impl;

import cn.com.cowain.ibms.dao.iot.SpaceDao;
import cn.com.cowain.ibms.dao.office.IntelligentOfficeDeviceDao;
import cn.com.cowain.ibms.dao.office.IntelligentOfficeUserDao;
import cn.com.cowain.ibms.dao.space.SpaceAdminDao;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.office.IntelligentOffice;
import cn.com.cowain.ibms.entity.office.IntelligentOfficeDevice;
import cn.com.cowain.ibms.entity.office.IntelligentOfficeUser;
import cn.com.cowain.ibms.service.office.IntelligentOfficeDeviceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * @author wei.cheng
 * @date 2022/02/28 17:28
 */
@Slf4j
@Service
public class IntelligentOfficeDeviceServiceImpl implements IntelligentOfficeDeviceService {
    @Autowired
    private IntelligentOfficeUserDao intelligentOfficeUserDao;
    @Autowired
    private SpaceAdminDao spaceAdminDao;
    @Autowired
    private IntelligentOfficeDeviceDao intelligentOfficeDeviceDao;
    @Autowired
    private SpaceDao spaceDao;

    @Override
    public Map<String, IntelligentOfficeDevice> getDeviceIdToIntelligentOfficeDeviceMapOfUserHrIdAndOfficeId(String userHrId,
                                                                                                             IntelligentOffice intelligentOffice) {
        return getIntelligentOfficeDeviceListOfUserHrIdAndOfficeId(userHrId, intelligentOffice).stream().collect(
                Collectors.toMap(o -> o.getDevice().getId(), o -> o));
    }

    @Override
    public List<IntelligentOfficeDevice> getIntelligentOfficeDeviceListOfUserHrIdAndOfficeId(String userHrId, IntelligentOffice intelligentOffice) {
        Space space = intelligentOffice.getSpace();
        List<IntelligentOfficeDevice> intelligentOfficeDeviceList;
        //判断用户是否是空间的管理员
        if (spaceAdminDao.existsByAdminEmpNoAndSpaceId(userHrId, space.getId())) {
            intelligentOfficeDeviceList = intelligentOfficeDeviceDao.findAllByUserHrIdAndIntelligentOfficeId(userHrId, intelligentOffice.getId());
        } else {
            Optional<IntelligentOfficeUser> intelligentOfficeUserOptional = intelligentOfficeUserDao.findByIntelligentOfficeIdAndUserHrId(
                    intelligentOffice.getId(), userHrId);
            if (intelligentOfficeUserOptional.isPresent()) {
                intelligentOfficeDeviceList = intelligentOfficeDeviceDao.findAllByUserHrIdAndIntelligentOfficeId(
                        intelligentOfficeUserOptional.get().getInvitedUserHrId(), intelligentOffice.getId());
            } else {
                intelligentOfficeDeviceList = intelligentOfficeDeviceDao.findAllByUserHrIdAndIntelligentOfficeId(userHrId, intelligentOffice.getId());
            }
        }
        return intelligentOfficeDeviceList;
    }

    @Override
    public List<IntelligentOfficeDevice> getDeviceListOfHrIdAndSpaceId(String hrId, IntelligentOffice intelligentOffice) {

        List<String> spaceIds = new ArrayList<>();
        Space space = intelligentOffice.getSpace();
        spaceIds.add(space.getId());

        // 获取空间下所有子空间
        getListBySpace(spaceIds, space);
        List<IntelligentOfficeDevice> officeDeviceList = intelligentOfficeDeviceDao.findAllByUserHrIdAndIntelligentOfficeIdOrderByCreatedTimeDesc(hrId, intelligentOffice.getId());
        List<String> collect = officeDeviceList.stream().map(officeDevice -> officeDevice.getDevice().getId()).collect(Collectors.toList());
        List<IntelligentOfficeDevice> intelligentOfficeDeviceList = intelligentOfficeDeviceDao.findByUserHrIdAndDeviceSpaceIdInAndDeviceIdNotInOrderByCreatedTimeDesc(hrId, collect, spaceIds);

        intelligentOfficeDeviceList.addAll(officeDeviceList);
        //判断用户是否是空间的管理员
        /*if (spaceAdminDao.existsByAdminEmpNoAndSpaceId(hrId, space.getId())) {
            intelligentOfficeDeviceList = intelligentOfficeDeviceDao.findByUserHrIdAndDeviceSpaceIdInOrderByCreatedTimeDesc(hrId, spaceIds);
            // intelligentOfficeDeviceList = intelligentOfficeDeviceDao.findAllByUserHrIdAndIntelligentOfficeId(userHrId, intelligentOffice.getId());
        }else {

            Optional<IntelligentOfficeUser> intelligentOfficeUserOptional = intelligentOfficeUserDao.findByIntelligentOfficeIdAndUserHrId(
                    intelligentOffice.getId(), hrId);
            // 获取所有空间Id
            List<IntelligentOfficeUser> officeUserList = intelligentOfficeUserDao.findByUserHrId(hrId);
            *//*List<String> spaceIdList = officeUserList.stream().map(IntelligentOfficeUser::getIntelligentOffice).collect(Collectors.toList())
                    .stream().map(IntelligentOffice::getSpace).collect(Collectors.toList())
                    .stream().map(Space::getId).collect(Collectors.toList());*//*
            List<String> spaceIdList = new ArrayList<>();
            officeUserList.stream().map(IntelligentOfficeUser::getIntelligentOffice).collect(Collectors.toList())
                    .stream().map(IntelligentOffice::getSpace).collect(Collectors.toList())
                    .stream().map(Space::getId).collect(Collectors.toList()).forEach(userSpace -> spaceIds.forEach(spaceId ->{
                        if(userSpace.equals(spaceId)){
                            spaceIdList.add(userSpace);
                        }
                    }));
            if(officeUserList.size() > 0 && intelligentOfficeUserOptional.isPresent()){
                spaceIdList.add(space.getId());
                intelligentOfficeDeviceList = intelligentOfficeDeviceDao.findByUserHrIdAndDeviceSpaceIdInOrderByCreatedTimeDesc(intelligentOfficeUserOptional.get().getInvitedUserHrId(), spaceIdList);
            }
        }*/
        /*intelligentOfficeDeviceList = intelligentOfficeDeviceList.stream().collect(collectingAndThen(
                toCollection(() -> new TreeSet<>(comparing(n -> n.getDevice().getId()))), ArrayList::new));*/

        return intelligentOfficeDeviceList;
    }

    @Override
    public void getListBySpace(List<String> spaceIds, Space space) {

        List<Space> spaceList = spaceDao.findAllByParentId(space.getId());
        for (Space space1 : spaceList) {

            // 递归获取子节点的子节点列表
            // 获取空间下所有子空间
            getListBySpace(spaceIds, space1);

            spaceIds.add(space1.getId());
        }
    }
}
