package cn.com.cowain.ibms.dao;

import cn.com.cowain.ibms.entity.Room;
import cn.com.cowain.ibms.entity.RoomUsedTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoomUsedTimeDao extends JpaRepository<RoomUsedTime, String> {

    // 根据员工工号及会议室获取用户会议室使用时长
    Optional<RoomUsedTime> findByEmpNoAndRoom(String empNo, Room room);

    List<RoomUsedTime> findByEmpNoAndRoomSpaceProjectIdContainsOrderByUsedTimeDesc(String hrId, String projectId);
}
