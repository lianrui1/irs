package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * DoorPlate
 *
 * @author cuiEnming
 * @date 2020/11/19  18:51
 * @since s2
 */
@Data
@Entity
@Table(name = "iot_door_plate")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_door_plate", comment = "门牌")
public class DoorPlate extends BaseEntity {

    /**
     * 门牌名称
     */
    @Column(name = "name", nullable = false, columnDefinition = "varchar(20) COMMENT '门牌名称'")
    private String name;

    /**
     * 编号
     */
    @Column(name = "magnetic_num", columnDefinition = "varchar(50) COMMENT '门磁设备编号'")
    private String magneticNum;

    /**
     * iot 设备
     */
    @ManyToOne
    @JoinColumn(name = "device_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK'")
    private IotDevice device;

    /**
     * 排序
     */
    @Column(name = "seq", columnDefinition = "integer COMMENT '排序'")
    private int seq;

    /**
     * 门磁开门密码
     */
    @Column(name = "open_password", columnDefinition = "varchar(20) COMMENT '门磁开门密码'")
    private String openPassword;

    /**
     * 门牌密码
     */
    @Column(name = "password", columnDefinition = "varchar(20) COMMENT '门牌密码'")
    private String password;

    /**
     * 设备硬件编号
     **/
    @Column(name = "hardware_id", columnDefinition = "varchar(50) COMMENT '设备硬件编号，唯一'")
    private String hardwareId;
}
