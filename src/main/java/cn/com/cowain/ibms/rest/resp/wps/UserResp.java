package cn.com.cowain.ibms.rest.resp.wps;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wei.cheng
 * @date 2022/02/22 15:18
 */
@Data
@ApiModel("用户详情")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserResp {

    @ApiModelProperty(value = "用户id，长度小于32", required = true, position = 1)
    private String id;

    @ApiModelProperty(value = "用户名称", required = true, position = 2)
    private String name;

    @ApiModelProperty(value = "用户操作权限，`write`可编辑，`read`预览", required = true, position = 3)
    private String permission;

    @ApiModelProperty(value = "用户头像地址", position = 4)
    @JsonProperty(value = "avatar_url")
    private String avatarUrl;
}
