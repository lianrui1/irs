package cn.com.cowain.ibms.entity.meeting;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.ReservationRecord;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author Yang.Lee
 * @date 2021/6/21 19:49
 */
@Data
@Entity
@Table(name = "ibms_meeting_schedule")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_meeting_schedule", comment = "会议日程记录")
@Where(clause = "is_del=0")
public class Schedule extends BaseEntity {

    @Column(name = "emp_no", nullable = false, columnDefinition = "varchar(50) COMMENT '员工ID'")
    private String empNo;

    @Column(name = "task_center_id", columnDefinition = "varchar(50) COMMENT '任务中心id'")
    private String taskCenterId;

    @ManyToOne
    @JoinColumn(name = "record_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK'")
    private ReservationRecord reservationRecord;
}
