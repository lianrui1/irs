package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.ability.OpenAbility;
import cn.com.cowain.ibms.enumeration.iot.KSHTMemberGroupType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 旷视鸿图人员组对象
 *
 * @author Yang.Lee
 * @date 2022/2/14 10:41
 */
@Data
@Entity
@Table(name = "iot_access_control_spot_ksht_member_group")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_access_control_spot_ksht_member_group", comment = "鸿图员工组表")
public class AccessControlSpotKSHTMemberGroup extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "access_control_spot_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK。门禁点id'")
    private AccessControlSpot accessControlSpot;

    @Column(name = "group_name", columnDefinition = "varchar(100) COMMENT '鸿图人员组名称'")
    private String groupName;


    @Enumerated(EnumType.STRING)
    @Column(name = "group_type", nullable = false, columnDefinition = "varchar(20) COMMENT '人员组类型'")
    private KSHTMemberGroupType groupType;

    @ManyToOne
    @JoinColumn(name = "ability_id", nullable = false, columnDefinition = "varchar(36) COMMENT '开放能力ID'")
    private OpenAbility openAbility;
}
