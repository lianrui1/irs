package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.ElevatorSpot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ElevatorSpotDao extends JpaRepository<ElevatorSpot, String>, JpaSpecificationExecutor<ElevatorSpot> {


    // 根据空间ID获取梯控点位
    Optional<ElevatorSpot> findBySpaceId(String spaceId);
}
