package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.DeviceGroupItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

/**
 * @author Yang.Lee
 * @date 2021/10/21 13:30
 */
public interface DeviceGroupItemDao extends JpaRepository<DeviceGroupItem, String>, JpaSpecificationExecutor<DeviceGroupItem> {

    /**
     * 根据群组Id查询
     *
     * @param groupId 群组ID
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/10/21 17:10
     **/
    List<DeviceGroupItem> findByDeviceGroupId(String groupId);

    /**
     * 根据群组ID和设备ID查询数据
     *
     * @param groupId  群组ID
     * @param deviceId 设备ID
     * @return 关系详情
     * @author Yang.Lee
     * @date 2021/10/25 9:23
     **/
    Optional<DeviceGroupItem> findByDeviceGroupIdAndIotDeviceId(String groupId, String deviceId);

    /**
     * 根据群组id查询
     *
     * @param groupId  群组ID
     * @param pageable 分页对象
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/10/25 13:21
     **/
    Page<DeviceGroupItem> findByDeviceGroupId(String groupId, Pageable pageable);

    /**
     * 查询iotDeviceId = {@code iotDeviceId}的DeviceGroupItem
     *
     * @param iotDeviceId
     * @return
     */
    List<DeviceGroupItem> findAllByIotDeviceId(String iotDeviceId);
}
