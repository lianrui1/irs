package cn.com.cowain.ibms.service.space;

import cn.com.cowain.ibms.entity.space.IotAttributesDict;
import cn.com.cowain.ibms.entity.space.IotProduct;
import cn.com.cowain.ibms.enumeration.IotNodeType;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.space.ProductReq;
import cn.com.cowain.ibms.rest.resp.iot.IotProductResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;

import java.util.List;

/**
 * Description
 *
 * @author yuchunlei
 * @version 1.0.0
 * @createTime 2020年12月21日 08:55:00
 */
public interface IotProductService {

    /**
     * 新增
     *
     * @param productReq
     * @return
     */
    ServiceResult save(ProductReq productReq);


    /**
     * 查询数据点
     *
     * @return
     */
    List<IotAttributesDict> getIotAttributesDictList();


    /**
     * 分页查询产品列表
     *
     * @param searchName
     * @param page
     * @param size
     * @return
     */
    PageBean<IotProductResp> search(String searchName, IotNodeType nodeType, int page, int size);

    /**
     * iot产品详情
     *
     * @param id
     * @return
     */
    IotProductResp getIotProductInfo(String id);


    /**
     * 更新iot产品
     *
     * @param id
     * @param productReq
     * @return
     */
    ServiceResult update(String id, ProductReq productReq);

    /**
     * 删除iot产品
     *
     * @param id
     * @return
     */
    ServiceResult deleteById(String id);

    /**
     * 从iotc获取产品列表
     *
     * @return 产品列表
     * @author Yang.Lee
     * @date 2021/10/25 11:26
     **/
    List<IotProduct> getListFromIOTC();

    /**
     * 保存产品数据
     *
     * @param productList 产品列表
     * @author Yang.Lee
     * @date 2021/10/25 13:07
     **/
    void saveProduct(List<IotProduct> productList);
}
