package cn.com.cowain.ibms.rest.resp.device;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/2 13:23
 */
@Data
public class TbDataResp {

    // 亮度
    private String luminance;
    // 温度
    private String temperature;
    // 湿度
    private String humidity;
    // 噪音
    private String noise;
    // 空气质量10
    private String pm10;
    // 空气质量pm2_5
    private String pm2_5;
    // 人体传感器
    private String isPeopleThere;

    private String value;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime ts;
}
