package cn.com.cowain.ibms.rest.distinguish;

import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.distinguish.DeviceStaffPageReq;
import cn.com.cowain.ibms.rest.req.distinguish.DeviceStaffReq;
import cn.com.cowain.ibms.rest.resp.distinguish.DeviceStaffPageResp;
import cn.com.cowain.ibms.rest.resp.distinguish.DistinguishDeviceListResp;
import cn.com.cowain.ibms.service.disthinguish.DistinguishDeviceService;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * 人脸识别设备Controller
 *
 * @author Yang.Lee
 * @date 2021/3/11 14:00
 */
@RestController
@RequestMapping(API_BASE + "/distinguish/device")
@Api(tags = IConst.DISTINGUISH)
public class DistinguishDeviceController {

    @Resource
    private DistinguishDeviceService distinguishDeviceService;

    /**
     * 查询人脸识别设备列表
     *
     * @param projectId 项目ID, 可选
     * @param spaceId   空间ID,可选
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/3/11 14:11
     **/
    @GetMapping("list")
    @ApiOperation(value = "PC查询人脸识别设备列表", tags = IConst.DISTINGUISH)
    public ResponseEntity<JsonResult<List<DistinguishDeviceListResp>>> getList(@RequestParam(required = false) @ApiParam(value = "项目ID") String projectId,
                                                                               @RequestParam(required = false) @ApiParam(value = "空间ID") String spaceId) {

        return ResponseEntity.ok(JsonResult.ok("", distinguishDeviceService.getList(projectId, spaceId)));
    }

    @GetMapping("/add/staff")
    @ApiOperation(value = "向设备内添加单个人员", tags = "${project.sprint}")
    public ResponseEntity<JsonResult<String>> addStaff(@RequestBody @Validated DeviceStaffReq req){

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/remove/staff")
    @ApiOperation(value = "向设备内删除单个人员", tags = "${project.sprint}")
    public ResponseEntity<JsonResult<String>> removeStaff(@RequestBody @Validated DeviceStaffReq req){

        return ResponseEntity.ok().build();
    }

    @GetMapping("/{deviceId}/staff/page")
    @ApiOperation(value = "查询有某设备权限的人员列表", tags = "${project.sprint}")
    public ResponseEntity<JsonResult<PageBean<DeviceStaffPageResp>>> getDeviceStaff(@RequestParam  DeviceStaffPageReq req){

        return ResponseEntity.ok().build();
    }

    @PostMapping("/upload/addStaff")
    @ApiOperation(value = "上传批量添加面板机人员", tags = "${project.sprint}")
    public ResponseEntity<cn.com.cowain.ibms.rest.bean.JsonResult<String>> uploadAddStaff(@RequestParam String[] deviceIds, @RequestParam MultipartFile file) {

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/upload/removeStaff")
    @ApiOperation(value = "上传批量删除面板机人员", tags = "${project.sprint}")
    public ResponseEntity<cn.com.cowain.ibms.rest.bean.JsonResult<String>> uploadRemoveStaff(@RequestParam String[] deviceIds, @RequestParam MultipartFile file) {

        return ResponseEntity.ok().build();
    }
}
