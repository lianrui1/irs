package cn.com.cowain.ibms.utils;

/**
 * Error code 常量类
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/12/20
 */
public interface ErrConst {

    String E01 = "VALIDATION_FAILED";

    String E02 = "GENERIC_FORMAT_EXCEPTION";

    String E03 = "FILE_UPLOAD_EXCEPTION";

    String E04 = "PARAMETER_NULL_EXCEPTION";

    /**
     * 幂等异常
     */
    String E05 = "IDEMPOTENT_EXCEPTION";

    /**
     * 数据查询异常
     */
    String E06 = "DATA_SEARCH_EXCEPTION";

    /**
     * 设备未找到异常
     **/
    String E07 = "DEVICE_NOT_FOUND_EXCEPTION";

    String E11 = "NO_SUCH_ELEMENT_EXCEPTION";

    String E12 = "BIZ_LOGIC_NOT_MATCH_EXCEPTION";

    String E13 = "REMOTE_SERVICE_UNAVAILABLE_EXCEPTION";

    /**
     * 二维码生成异常
     */
    String E14 = "CREATE_QRCODE_EXCEPTION";

    /**
     * 链接过多异常
     */
    String E15 = "CONNECTION_TOO_MANY";

    /**
     * 会议预约取消异常
     **/
    String E16 = "MEETING_CANCEL_FAILED";

    /**
     * 会议编辑异常
     **/
    String E17 = "RESERVATION_UPDATE_ERROR";

    /**
     * 门磁开门异常
     **/
    String E18 = "MAGNETIC_OPEN_ERROR";

    /**
     * 第三方http请求异常
     **/
    String E20 = "THIRD_PART_HTTP_REQUEST_EXCEPTION";

    /**
     * 短消息异常
     **/
    String E21 = "SMS_ERROR";

    /**
     * Excel单元格格式错误
     **/
    String E22 = "EXCEL_CELL_FORMAT_ERROR";

    /**
     * 文件格式错误
     **/
    String E23 = "FILE_FORMAT_ERROR";

    /**
     * 权限申请异常
     **/
    String E24 = "PERMISSION_APPLICATION_EXCEPTION";

    /**
     * 未知错误
     */
    String E401 = "UNAUTHORIZED";

    /**
     * 请求异常
     */
    String E500 = "HTTP_EXCEPTION";

    /**
     * 未知错误
     */
    String E99 = "UNKNOWN_EXCEPTION";
    /**
     *  @title
     *  @Description 描述
     *  @author jf.sui
     *  @Date 参数异常
     */
    String E00001 = "PARAM_EXCEPTION";
    /**
     *  @title
     *  @Description 描述
     *  @author jf.sui
     *  @Date 程序异常
     */
    String E00002 = "PROGRAM_EXCEPTION";
    /**
     *  @title
     *  @Description 执行中
     *  @author
     *  @Date 控制设备执行中
     */
    String E10000 = "PROCESSING";

    String E25 = "COMMAND_EXCEPTION";

    /**
     * 无权限
     */
    String FORBIDDEN = "FORBIDDEN";

    /**
     * 通行2.0未授权
     */
    String PASS_V2_UNAUTHORIZED = "PASS_V2_UNAUTHORIZED";

    String NOT_FOUND = "NOT_FOUND";
    String E26 = "REPEAT_EXCEPTION";

}
