package cn.com.cowain.ibms.rest.resp.iot;

import cn.com.cowain.ibms.enumeration.iot.PersonType;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalTime;

/**
 * @Author tql
 * @Description PersonPassPageResp
 * @Date 21-11-9 下午9:05
 * @Version 1.0
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel("人员通行分页数据响应对象")
public class PersonPassPageResp {

    @ApiModelProperty(value = "通行人员信息")
    private String personMsg;

    @ApiModelProperty(value = "部门")
    private String department;

    @ApiModelProperty(value = "人员类型")
    private PersonType personType;

    @ApiModelProperty(value = "门禁点")
    private String controlSpot;

    @ApiModelProperty(value = "门禁类型")
    private String controlType;

    @ApiModelProperty(value = "门禁点位置")
    private String controlSpotLocation;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "通行时间")
    private LocalTime passTime;

    @ApiModelProperty(value = "状态")
    private String status;
}
