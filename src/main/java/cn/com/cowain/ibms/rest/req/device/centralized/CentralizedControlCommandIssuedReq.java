package cn.com.cowain.ibms.rest.req.device.centralized;

import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author wei.cheng
 * @date 2022/09/15 17:11
 */
@Data
@ApiModel(value = "集控命令下发")
public class CentralizedControlCommandIssuedReq {

    @ApiModelProperty(value = "事务ID（唯一标识）", required = true)
    private String traceId;

    @ApiModelProperty(value = "集控码id", required = true)
    private String centralizedCode;

    @ApiModelProperty(value = "控制的设备类型列表", required = true)
    private List<DeviceType> controlDeviceTypeList;

    @ApiModelProperty(value = "用户ucId", example = "123abcABC", required = true, position = 1)
    private String uid;

    @ApiModelProperty(value = "执行动作：on关 off开", required = true)
    private String action;

    @ApiModelProperty(value = "控制模式：batch 批量  single 单个 ", required = true)
    private String controlMode;

    @ApiModelProperty(value = "重试控制的设备列表（可为空）")
    private List<String> retryDeviceIdList;
}
