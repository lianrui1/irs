package cn.com.cowain.ibms.rest.resp.space;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2020/12/23 13:24
 */
@Data
@ApiModel("空间层级响应对象")
public class SpaceLevelResp {

    @ApiModelProperty(value = "最大层级", required = true)
    private Integer max;
}
