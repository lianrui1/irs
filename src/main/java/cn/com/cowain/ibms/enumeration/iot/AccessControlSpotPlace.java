package cn.com.cowain.ibms.enumeration.iot;

/**
 * 门禁点位置枚举
 *
 * @author Yang.Lee
 * @date 2022/2/17 15:06
 */
public enum AccessControlSpotPlace {

    KS("昆山", 1),

    NT("南通", 2),

    NT_SY("南通善营", 6),

    DL("大连", 7),

    WF("潍坊", 5),

    NB("宁波", 4 ),

    DG("东莞", 0),

    KS_NEW("昆山新工厂", 8),

    HQ("花桥", 3 ),

    SZ_SY("深圳善营", 0),

    OTHER("其他", -1);

    AccessControlSpotPlace(String name, int ehrId) {
        this.name = name;
        this.ehrId = ehrId;
    }

    String name;

    int ehrId;
    public String getName() {
        return this.name;
    }

    public int geyEhrId(){
        return this.ehrId;
    }
}
