package cn.com.cowain.ibms.dao;

import cn.com.cowain.ibms.entity.OpeningHours;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;


/**
 * ReservationRecord Dao
 *
 * @author 张宇鑫
 * @version 1.0
 * @date 2020/8/6 19:06
 */
@Repository
public interface OpeningHoursDao extends JpaRepository<OpeningHours, String> {

    /**
     * 根据roomId查询 开放时间
     *
     * @param roomId
     * @return
     */
    List<OpeningHours> findByRoomIdOrderByFromAsc(String roomId);

    /**
     * 根据room_id 删除
     *
     * @param id
     * @return
     */
    void deleteByRoomId(String id);

    /**
     * 根据room_id 查找
     *
     * @param id
     * @return
     */
    Set<OpeningHours> findByRoomId(String id);

    /**
     * 按开发结束时间查询
     *
     * @author: yanzy
     * @data: 2022/4/19 16:04:29
     */
    List<OpeningHours> findAllByRoomIsOpenOrderByToDesc(int isOpen);

}
