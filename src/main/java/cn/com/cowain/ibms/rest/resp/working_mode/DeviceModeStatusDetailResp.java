package cn.com.cowain.ibms.rest.resp.working_mode;

import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.rest.req.office.DeviceNameEditReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/10/18 15:14
 */
@Data
@ApiModel("设备状态返回对象")
public class DeviceModeStatusDetailResp {

    @ApiModelProperty(value = "设备ID", required = true)
    private String id;

    @ApiModelProperty(value = "设备华为云ID", required = true)
    private String hwDeviceId;

    @ApiModelProperty(value = "设备名称", required = true)
    private String deviceName;

    @ApiModelProperty(value = "设备类型", required = true)
    private DeviceType deviceType;

    @ApiModelProperty(value = "设备类型名称", required = true)
    private String deviceTypeName;

    @ApiModelProperty(value = "设备状态 true开 false关", required = true)
    private boolean status;

    @ApiModelProperty(value = "门磁 窗帘开门 open close", position = 1)
    private String action;

    @ApiModelProperty(value = "空调的打开与关闭 on off", position = 2)
    private String setPower;

    @ApiModelProperty(value = "空调温度", position = 3)
    private int setTemp;

    @ApiModelProperty(value = "空调制冷制热模式 cool heat", position = 4)
    private String setMode;

    @ApiModelProperty(value = "空调风速 小 中 大 自动 min med max auto", position = 5)
    private String setFanSpeed;

    @ApiModelProperty(value = "空调风向 上 中 下 up med down", position = 6)
    private String setFanDirection;

    @ApiModelProperty(value = "空调风向 手动 自动 true false", position = 7)
    private String setFanAuto;

    @ApiModelProperty(value = "设置扫风模式 ，无扫风，上下扫风，左右扫风，上下左右扫风 noSwing,updownSwing,leftrightSwing,aroundSwing")
    private String setSwing;

    @ApiModelProperty(value = "四路开关 on off", position = 8)
    private String light1;

    @ApiModelProperty(value = "四路开关 on off", position = 9)
    private String light2;

    @ApiModelProperty(value = "四路开关 on off", position = 10)
    private String light3;

    @ApiModelProperty(value = "四路开关 on off", position = 11)
    private String light4;

    @ApiModelProperty(value = "附加名称", position = 12)
    private DeviceNameEditReq.AdditionalName additionalName;
}
