package cn.com.cowain.ibms.rest.resp;

import lombok.Data;

/**
 * @Author tql
 * @Description EquipmentDataResp
 * @Date 21-08-19 下午9:05
 * @Version 1.0
 */
@Data
public class EquipmentDataResp {


    private String ccDeviceCode;


    /**
     * equipName
     */
    private String name;


    /**
     * 类型
     */
    private String optionType;

    /**
     * 设备类型
     */
    private String deviceType;


    /**
     * 刷脸类型
     */
    private String faceType;


    /**
     * 设备ip
     */
    private String ip;

}
