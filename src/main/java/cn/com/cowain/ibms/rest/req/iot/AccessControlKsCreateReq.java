package cn.com.cowain.ibms.rest.req.iot;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/18 19:31
 */
@Data
@ApiModel("ks添加人员门禁点权限请求对象")
public class AccessControlKsCreateReq {

    @NotEmpty
    @ApiModelProperty(value = "门禁点ID", required = true)
    private String id;

    @NotEmpty
    @ApiModelProperty(value = "人员组ID", required = true)
    private List<String> userGroups;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "通行开始时间", required = true, position = 7)
    private LocalDateTime startTime;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "通行结束时间", required = true, position = 8)
    private LocalDateTime endTime;
}
