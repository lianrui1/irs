package cn.com.cowain.ibms.dao.audio;

import cn.com.cowain.ibms.entity.voice.AudioRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * @author Yang.Lee
 * @date 2021/1/4 9:58
 */
public interface AudioDao extends JpaRepository<AudioRecord, String> {
    /*
     *根据会议室名称获取会议音频记录  分页
     */
    Page<AudioRecord> findByReservationRecordRoomNameContainsAndReservationRecordInitiatorEmpNo(String name, String sysId, Pageable pageableAudio);

    /*
     *根据会议室名称和预定日期获取会议音频记录  分页
     */
    Page<AudioRecord> findByReservationRecordRoomNameContainsAndReservationRecordDateAndReservationRecordInitiatorEmpNo(String name, LocalDate localDate, String empNo, Pageable pageableAudio);

    /*
     * 根据音频记录ID和用户唯一ID查询音频记录
     */
    Optional<AudioRecord> findByIdAndReservationRecordInitiatorEmpNo(String id, String empNo);

    // 根据会议记录ID获取音频
    List<AudioRecord> findByReservationRecordIdAndAudioFilePathNotNullOrderByCreatedTimeDesc(String id);
}
