package cn.com.cowain.ibms.dao.distinguish;

import cn.com.cowain.ibms.entity.iot.HkDevice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface HkDeviceDao extends JpaRepository<HkDevice ,String>, JpaSpecificationExecutor<HkDevice> {
    Optional<HkDevice> findByDeviceId(String deviceId);

    // 获取海康设备信息
    List<HkDevice> findByDeviceIdIn(List<String> deviceIds);

    // 根据hk设备id获取ip
    List<HkDevice> findByIdIn(List<String> deviceIds);
}
