package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 门禁点审批人
 *
 * @author Yang.Lee
 * @date 2022/3/8 10:50
 */
@Data
@Entity
@Table(name = "iot_access_control_spot_approver")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_access_control_spot_approver", comment = "审批人")
public class AccessControlSpotApprover extends BaseEntity {

    @Column(name = "hr_id", nullable = false, columnDefinition = "varchar(30) COMMENT '审批人工号'")
    private String userHrId;

    @Column(name = "approve_type", nullable = false, columnDefinition = "varchar(300) COMMENT '审批类型，可多个。使用”,“分割'")
    private String approveType;
}
