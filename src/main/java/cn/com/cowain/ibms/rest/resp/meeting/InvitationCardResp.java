package cn.com.cowain.ibms.rest.resp.meeting;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author Yang.Lee
 * @date 2021/6/25 9:39
 */
@Data
@ApiModel("邀请卡详情")
public class InvitationCardResp implements Serializable {

    @ApiModelProperty(value = "邀请人", required = true, example = "张三", position = 1)
    private String host;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "开始时间", required = true, example = "2099-01-01 12:34:56", position = 2)
    private LocalDateTime startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "结束时间", required = true, example = "2099-01-01 12:34:56", position = 3)
    private LocalDateTime endTime;

    @ApiModelProperty(value = "事由", required = true, example = "面试", position = 4)
    private String reason;

    @ApiModelProperty(value = "是否VIP", required = true, example = "否", position = 5)
    private String isVip;

    @ApiModelProperty(value = "来访单位", required = true, example = "科瑞恩", position = 6)
    private String visitCompany;

    @ApiModelProperty(value = "来访地址", required = true, example = "昆山市", position = 7)
    private String address;

    @ApiModelProperty(value = "邀请码", required = true, example = "BCDA123", position = 8)
    private String invitationCode;

    @ApiModelProperty(value = "审批备注", required = true, example = "", position = 9)
    private String invitationRemark;

    /**
     * 会议开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "会议开始时间", required = true, position = 10)
    private LocalDateTime timeFrom;

    /**
     * 会议结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "会议结束时间", required = true, position = 11)
    private LocalDateTime timeTo;

    /**
     * 会议室
     */
    @ApiModelProperty(value = "会议室", required = true, position = 12)
    private String room;

    /**
     * 访客工号
     */
    @ApiModelProperty(value = "访客工号", required = true, position = 13)
    private String jobNum;

    /**
     * 邀请函单号
     */
    @ApiModelProperty(value = "邀请函单号", required = true, position = 14)
    private String visitorRecordNo;

    /**
     * 访客状态
     */
    @ApiModelProperty(value = "访客状态", required = true, position = 15)
    private String visitorStatus;

    /**
     * 访客姓名
     */
    @ApiModelProperty(value = "访客姓名", required = true, position = 16)
    private String name;

    /**
     * 访客姓名
     */
    @ApiModelProperty(value = "访客单位", required = true, position = 17)
    private String company;

    /**
     * 访客照片
     */
    @ApiModelProperty(value = "访客照片", required = true, position = 18)
    private String photoUrl;

    /**
     * 访客手机号
     */
    @ApiModelProperty(value = "访客手机号", required = true, position = 19)
    private String mobile;

    /**
     * 事由编码
     */
    @ApiModelProperty(value = "事由编码", required = true, position = 20)
    private String reasonCode;

    @ApiModelProperty(value = "会议主题", required = true, position = 21)
    private String topic;

    private String isSignIn;

    @ApiModelProperty(value = "访客类型代码code",position = 22)
    private String roleCode;
}
