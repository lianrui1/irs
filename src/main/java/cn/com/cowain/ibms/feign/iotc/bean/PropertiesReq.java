package cn.com.cowain.ibms.feign.iotc.bean;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;


/**
 *
 * @author: yanzy
 * @date: 2022/2/17 18:57
 */
@Data
public class PropertiesReq {

        @ApiModelProperty(value = "规则判断条件值")
        private List<Conditions> conditionsList;
}
