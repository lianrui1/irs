package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Yang.Lee
 * @date 2022/6/24 13:34
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("升降柱控制请求参数")
public class LiftColumnControlReq {

    @ApiModelProperty(value = "控制指令。 up: 升柱子， down:降柱子", example = "down", required = true)
    private String cmd;
}
