package cn.com.cowain.ibms.websocket.service.impl;

import cn.com.cowain.ibms.rest.resp.audio.RTASTResultResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.utils.RedisUtil;
import cn.com.cowain.ibms.utils.RegexUtil;
import cn.com.cowain.ibms.websocket.bean.RTASRBean;
import cn.com.cowain.ibms.websocket.client.RTASRClient;
import cn.com.cowain.ibms.websocket.service.RTASRService;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import javax.websocket.Session;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author Yang.Lee
 * @date 2021/1/18 15:05
 */
@Slf4j
@Service
public class RTASRServiceImpl implements RTASRService {

    private static final CopyOnWriteArrayList<RTASRBean> rtasrClients = new CopyOnWriteArrayList<>();

    private static RedisUtil redisUtil;

    public RTASRServiceImpl(RedisUtil redisUtil) {
        setRedisUtilValue(redisUtil);
    }
    public static void setRedisUtilValue(RedisUtil redisUtil){
        RTASRServiceImpl.redisUtil = redisUtil;
    }

    /**
     * 建立链接
     *
     * @param session
     * @return
     */
    @Override
    public ServiceResult open(Session session) throws URISyntaxException, InterruptedException {

        // 判断是否已经达到讯飞并发上线
        if(RTASRClient.isConnectExceedLimit()){
            return ServiceResult.error("实时语音转写连接已达到并发上线，无法在进行连接");
        }

        // 参数校验
        ServiceResult result = parameterCheck(session);
        if (!result.isSuccess()) {
            return result;
        }

        Map<String, List<String>> paramMap = (Map) result.getObject();
        // 音频ID
        String audioId = paramMap.get("id").get(0);
        // 获取讯飞ws client对象
        RTASRBean bean = getClientBySessionId(session.getId());

        // 如果是新的session，则创建一个新的bean
        if (bean == null) {
            bean = new RTASRBean(audioId, session, RTASRClient.getInstance(audioId));

            //  创建讯飞ws client后立即进行握手
            bean.getClient().handshake();

            // 将新的bean加入当前正在连接的集合中
            rtasrClients.add(bean);
        }

        return ServiceResult.ok();
    }

    /**
     * 获取音频文件，执行转写操作
     *
     * @param session
     * @param source
     * @return
     */
    @Override
    public ServiceResult translate(Session session, byte[] source) throws InterruptedException, URISyntaxException {

        // 参数校验
        ServiceResult result = parameterCheck(session);
        if (!result.isSuccess()) {
            return result;
        }

        // 获取讯飞ws client对象
        RTASRBean bean = getClientBySessionId(session.getId());

        // 发送实时语音转写消息
        bean.getClient().translate(source);
        return ServiceResult.ok();
    }

    /**
     * 音频上传完成，断开转写客户端
     *
     * @param session
     * @return
     */
    @Override
    public ServiceResult close(Session session) throws IOException {
        // 获取讯飞ws client对象
        RTASRBean bean = getClientBySessionId(session.getId());
        if (bean == null) {
            return ServiceResult.error("关闭讯飞ws客户端失败，未找到对应客户端。session id = " + session.getId());
        }

        // 关闭客户端
        session.close();
        bean.getClient().close();

        return ServiceResult.ok();
    }

    /**
     * 根据SessionID获取讯飞客户端对象
     *
     * @param sessionId
     * @return 如果存在则返回客户端对象；如果不存在，则返回null
     */
    public RTASRBean getClientBySessionId(@NonNull String sessionId) {

        RTASRBean bean = null;

        for (RTASRBean client : rtasrClients) {
            if (sessionId.equals(client.getSession().getId())) {
                bean = client;
                break;
            }
        }
        return bean;
    }

    /**
     * 参数校验
     *
     * @param session
     * @return
     */
    public ServiceResult parameterCheck(Session session) {

        Map<String, List<String>> paramMap = session.getRequestParameterMap();
        if (!paramMap.containsKey("id")) {
            return ServiceResult.error("未找到参数：id");
        }

        return ServiceResult.ok(paramMap);
    }

    /**
     * 将实施转写结果存入redis（实时录音过程）
     * @param audioId
     * @param rtastResultResp
     */
    public static synchronized void writeToCache(String audioId, RTASTResultResp rtastResultResp) {

        String key = "rtasr-" + audioId;
        writeToRedis(key, rtastResultResp);
    }

    /**
     * 将实施转写结果存入redis（将实施转写结果存入redis录音完成后补上传）
     * @param audioId
     * @param rtastResultResp
     */
    public static synchronized void writeToCache2(String audioId, RTASTResultResp rtastResultResp) {

        String key = "rtasr-end-" + audioId;
        writeToRedis(key, rtastResultResp);
    }

    /**
     * 向redis中写入转写结果
     * @param key
     * @param rtastResultResp
     */
    private static synchronized void writeToRedis(String key, RTASTResultResp rtastResultResp){
        String redisKey = redisUtil.createRealKey(key);

        List<RTASTResultResp> rtastResultResps = null;
        // 判断是否已存在数据
        if (Boolean.TRUE.equals(redisUtil.hasKey(redisKey))) {
            rtastResultResps = JSON.parseArray(String.valueOf(redisUtil.get(redisKey)), RTASTResultResp.class);
            // 判断当前句子的第一个字符是否时标点符号，如果是则将第一个字符转到上一句末尾
            String text = rtastResultResp.getText();
            if(text != null && !text.isEmpty()){
                String first = text.substring(0,1);
                if(RegexUtil.match(first, RegexUtil.PUNCTUATION)){
                    // 第一个字符是标点，则将当前数据中的标点删除，并添加到上一个元素的末尾
                    rtastResultResp.setText(text.substring(1));
                    RTASTResultResp lastElement = rtastResultResps.get(rtastResultResps.size() - 1);
                    lastElement.setText(lastElement.getText() + first);
                }
            }

        } else {
            rtastResultResps = new ArrayList<>();
        }

        // 加入新数据
        rtastResultResps.add(rtastResultResp);
        // 将数据写入redis
        redisUtil.set(redisKey, rtastResultResps);
    }
}
