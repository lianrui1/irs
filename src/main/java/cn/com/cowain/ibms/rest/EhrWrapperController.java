package cn.com.cowain.ibms.rest;

import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.ibms.rest.req.AuthReq;
import cn.com.cowain.ibms.rest.resp.EhrDeptResp;
import cn.com.cowain.ibms.rest.resp.EhrStaffRespPage;
import cn.com.cowain.ibms.rest.resp.EhrStaffRespRoot;
import cn.com.cowain.ibms.service.EhrRemoteService;
import cn.com.cowain.ibms.utils.IConst;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/4 20:32
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/ehr")
@Api(tags = IConst.MODULE_EHR)
public class EhrWrapperController {

    @Autowired
    private EhrRemoteService ehrRemoteService;

    /**
     * 查询部门树
     *
     * @return
     */
    @GetMapping("/department/tree")
    @ApiOperation(value = "查询部门树", tags = IConst.MODULE_EHR)
    public ResponseEntity<JsonResult<EhrDeptResp>> departmentTree() {
        log.debug("departmentTree()...");
        EhrDeptResp ehrDeptResp = ehrRemoteService.getDepartmentTree();
        JsonResult<EhrDeptResp> jsonResult = JsonResult.ok("OK", ehrDeptResp);
        return ResponseEntity.ok(jsonResult);
    }


    /**
     * 根据部门ID查询下属员工
     *
     * @return
     */
    @GetMapping("/department/{deptId}/user")
    @ApiOperation(value = "根据部门ID查询下属员工", tags = IConst.MODULE_EHR)
    public ResponseEntity<JsonResult<EhrStaffRespPage>> getUserByDeptId(
            @ApiParam(name = "deptId", value = "部门ID", required = true) @PathVariable String deptId) {
        log.debug("getUserByDeptId, deptId:" + deptId);
        EhrStaffRespPage staffPage = ehrRemoteService.getUserByDeptId(deptId);

        JsonResult<EhrStaffRespPage> jsonResult = JsonResult.ok("OK", staffPage);
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 根据员工姓名模糊查找
     *
     * @return
     */
    @GetMapping("/user")
    @ApiOperation(value = "根据员工姓名模糊查找", tags = IConst.MODULE_EHR)
    public ResponseEntity<JsonResult<EhrStaffRespRoot>> findUserByName(
            @ApiParam(name = "name", value = "模糊名", required = true) @RequestParam String name) {
        log.debug("findUserByName, name:" + name);
        EhrStaffRespRoot ehrStaffRespRoot = ehrRemoteService.findSysUserByNameContains(name);
        JsonResult<EhrStaffRespRoot> jsonResult = JsonResult.ok("OK", ehrStaffRespRoot);
        return ResponseEntity.ok(jsonResult);
    }

    /**
     *
     * 通过code获得基本用户信息
     *
     * @param authReq
     * @return
     */
    @PostMapping("/getOAuth2UserInfo")
    @ApiOperation(value = "通过code获得基本用户信息(使用时要打印数据,以便优化代码)", tags = IConst.MODULE_EHR)
    public ResponseEntity<JsonResult<JSONObject>> getAuth2UserInfo(@Validated AuthReq authReq, BindingResult bindingResult) {
        log.debug("getAuth2UserInfo, authReq:" + authReq);
        JSONObject jsonObject = ehrRemoteService.getAuth2UserInfo(authReq.getCode());
        JsonResult<JSONObject> jsonResult = JsonResult.ok("OK", jsonObject);
        return ResponseEntity.ok(jsonResult);
    }

}
