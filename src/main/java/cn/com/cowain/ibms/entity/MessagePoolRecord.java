package cn.com.cowain.ibms.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import java.security.Timestamp;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/3/24 19:59
 */
@Data
public class MessagePoolRecord extends Model<MessagePoolRecord> {

    private static final long serialVersionUID = -5152761771162336860L;


    @ApiModelProperty(value = "消息来源 1pms 2ehr 3wms 4recruit 5ips 6ipcs 7cms 8ibms 9bpm ", required = true)
    @Range(min = 1, max = 99, message = "消息来源范围错误")
    private int msgFromSys = 8;

    @ApiModelProperty(value = "消息类型 0默认 1待办 2预警 3通知 4日程", required = true)
    @Range(min = 0, max = 4, message = "消息类型范围错误")
    private int msgType;

    @ApiModelProperty(value = "消息优先级 0默认 2一般 4较优先 6优先", required = true)
    @Range(min = 0, max = 6, message = "消息优先级错误")
    private int priority;

    @ApiModelProperty("推送类型 0实时推送 1定时推送 默认是实时")
    @Range(min = 0, max = 1, message = "推送类型范围错误")
    private int sendType;

    @ApiModelProperty(value = "消息接受类型 三位格式 1xx站内信 2xx企业微信 3xx短信 4xx邮件 5xx微信公众号", required = true)
    @Range(min = 100, max = 599, message = "接受类型超出范围")
    private int receiveType;

    @ApiModelProperty(value = "消息接收人 工号/邮箱地址/用户id/openid/手机号码/用户名称", required = true)
    private String receivePersonId;

    @ApiModelProperty(value = "提醒方式 0默认 1红点/闹钟 2弹窗 3通知栏 4交表", required = true)
    @Range(min = 1, max = 4, message = "提醒类型超出范围")
    private int tipType;

    @ApiModelProperty(value = "消息详情跳转路径", required = true)
    private String msgDetailsUrl;

    /**
     * 发送微信消息时 是weixin的openid
     * 发送短消息时 是手机号
     * 发送站内信 是用户名称
     */
    @ApiModelProperty(value = "消息接收人 不同方式 代表不同的属性 微信号/手机号/用户名称/邮箱地址", required = true)
    private String receivePerson;

    @ApiModelProperty("发送时间")
    private Timestamp sendTimeStr;

    // "msgTag": "[{\"name\":\"测试招聘介绍.pdf\",\"url\":\"https://file.cowainglobal.com/img/M00/01/6B/rBIQr2CHbfeAHv-3AAMh6PltCtE313.pdf\"},{\"name\":\"测试offer.doc\",\"url\":\"https://file.cowainglobal.com/img/M00/01/69/rBIQr2CGmAuAZtU9AAKcm5-hKOk984.doc\"}]"
    @ApiModelProperty("消息标签 在发送邮件的时候这个代表的是附件地址")
    private String msgTag;

    @ApiModelProperty(value = "消息标题", required = true)
    private String msgTitle;

    @ApiModelProperty(value = "消息内容 不同方式 不同格式", required = true)
    private String msgContent;

    @ApiModelProperty(value = "消息模板id", required = true)
    private String msgTemplateId;
}