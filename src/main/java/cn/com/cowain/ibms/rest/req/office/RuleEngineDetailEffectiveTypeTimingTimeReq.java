package cn.com.cowain.ibms.rest.req.office;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalTime;
import java.util.List;

/**
 * 生效方式:定时生效时间
 *
 * @author: yanzy
 * @date: 2022/2/9 14:52
 */
@Data
@ApiModel("生效方式:定时生效时间请求对象")
public class RuleEngineDetailEffectiveTypeTimingTimeReq {

    @ApiModelProperty(value = "星期 1,2,3,4,5,6,7",required = true)
    private List<String> weeks;

    @JsonFormat(pattern = "HH:mm")
    @ApiModelProperty(value = "开始时间",required = true)
    private LocalTime startTime;

    @JsonFormat(pattern = "HH:mm")
    @ApiModelProperty(value = "结束时间",required = true)
    private LocalTime endTime;
}
