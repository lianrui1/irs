package cn.com.cowain.ibms.component;

import cn.com.cowain.ibms.enumeration.iot.IntelligentOfficeStatus;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
public class IntelligentOfficeRedis {

    private String name;

    private String space;

    @Enumerated(EnumType.STRING)
    private IntelligentOfficeStatus status;
}
