package cn.com.cowain.ibms.rest.iot;

import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.Sorts;
import cn.com.cowain.ibms.rest.req.iot.AlertCaptureorDetailReq;
import cn.com.cowain.ibms.rest.req.iot.AlertCaptureorGetExistOfDayQueryReq;
import cn.com.cowain.ibms.rest.req.iot.AlertCaptureorReq;
import cn.com.cowain.ibms.rest.resp.iot.AlertCaptureorResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.iot.AlertCaptureorService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * 警戒抓拍
 *
 * @author: yanzy
 * @date: 2022/4/13 14:07
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/alertCaptureor")
@Api(tags = IConst.MODULE_ALERT_CAPTUREOR)
public class AlertCaptureorController {

    @Resource
    private AlertCaptureorService alertCaptureorService;

    @GetMapping("/page")
    @ApiOperation(value = "警戒抓拍分页查询列表", tags = {IConst.MODULE_ALERT_CAPTUREOR, IConst.V28})
    public ResponseEntity<JsonResult<PageBean<AlertCaptureorResp>>> getPage(AlertCaptureorReq req) {
        req.addOrderDesc(Sorts.CREATED_TIME);
        PageBean<AlertCaptureorResp> page = alertCaptureorService.getPage(req);
        return ResponseEntity.ok(JsonResult.ok(page));
    }

    @GetMapping("/day/exist")
    @ApiOperation(value = "查询日期是否存在警戒抓拍记录", tags = {IConst.MODULE_ALERT_CAPTUREOR, IConst.V28})
    public ResponseEntity<JsonResult<Map<String, Boolean>>> getAlertCaptureorExistOfDay(AlertCaptureorGetExistOfDayQueryReq req) {
        ServiceResult result = alertCaptureorService.getAlertCaptureorExistOfDay(req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.ok((Map<String, Boolean>) result.getObject()));
    }

    @GetMapping("/detail")
    @ApiOperation(value = "查看警戒抓拍详情",tags = {IConst.MODULE_ALERT_CAPTUREOR,IConst.V28})
    public ResponseEntity<JsonResult<AlertCaptureorResp>> getAlertCaptureorDetail(AlertCaptureorDetailReq req){
        ServiceResult alertCaptureorDetail = alertCaptureorService.getAlertCaptureorDetail(req);
        if (!alertCaptureorDetail.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(alertCaptureorDetail.getObject()),null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.ok("OK",(AlertCaptureorResp) alertCaptureorDetail.getObject()));
    }
}
