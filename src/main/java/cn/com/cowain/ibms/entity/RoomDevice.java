package cn.com.cowain.ibms.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * 房间设备
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/7/31 13:47
 */
@Data
@Entity
@Table(name = "ibms_room_device")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_room_device", comment = "会议室设备")
public class RoomDevice extends BaseEntity {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    @Column(name = "rd_name", nullable = false, columnDefinition = "varchar(50) COMMENT '名称'")
    private String name;

    /**
     * 规格，预留
     */
    @Column(name = "rd_spec", columnDefinition = "varchar(100) COMMENT '规格，预留'")
    private String spec;

    /**
     * 关联Room
     */
    @ManyToOne
    @JoinColumn(name = "room_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK'")
    private Room room;

}
