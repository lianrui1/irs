package cn.com.cowain.ibms.rest.resp.distinguish;

import cn.com.cowain.ibms.entity.iot.DeviceAccessRecord;
import cn.com.cowain.ibms.enumeration.iot.PersonType;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 考勤resp
 */
@Data
@Builder
@ApiModel("考勤查询")
public class WorkTimeCheckResp {

    @ApiModelProperty(value = "工号")
    private String hrId;

    @ApiModelProperty(value = "打卡类型 1代表出 2代表入")
    private Integer type ;

    @ApiModelProperty(value = "地点")
    private String checkinSite;

    @ApiModelProperty(value = "设备名称")
    private String ccDeviceId;

    @ApiModelProperty(value = "人员类型，1代表内部员工 2代表劳务工 3代表其他")
    private int personType;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "刷卡时间")
    private LocalDateTime checkinTime;

    private String ccDeviceCode;


    public static WorkTimeCheckResp convert(DeviceAccessRecord record){

        return WorkTimeCheckResp.builder()
                .hrId(record.getEmpNo())
                // 不再使用设备sn作为唯一标识，使用门禁点ID
//                .ccDeviceCode(record.getIotDevice().getSn())
                .ccDeviceId(record.getIotDevice().getDeviceName())
                .checkinSite(record.getIotDevice().getProject().getProjectName())
                .type(0)
                .personType(PersonType.EMPLOYEE.equals(record.getStaffType()) ? 1 : 2)
                .checkinTime(record.getDataTime())
                .build();
    }
}
