package cn.com.cowain.ibms.service.disthinguish.impl;

import cn.com.cowain.ibms.dao.distinguish.DeviceCloudDao;
import cn.com.cowain.ibms.entity.distinguish.DeviceCloud;
import cn.com.cowain.ibms.rest.resp.distinguish.DistinguishDeviceListResp;
import cn.com.cowain.ibms.service.disthinguish.DistinguishDeviceService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * 人脸识别设备Service逻辑实现类
 *
 * @author Yang.Lee
 * @date 2021/3/11 14:15
 */
@Service
public class DistinguishDeviceServiceImpl implements DistinguishDeviceService {

    @Resource
    private DeviceCloudDao deviceCloudDao;

    /**
     * 查询人脸识别设备列表
     *
     * @param projectId 项目ID
     * @param spaceId   空间ID
     * @return 设备列表
     * @author Yang.Lee
     * @date 2021/3/11 14:15
     **/
    @Override
    public List<DistinguishDeviceListResp> getList(@Nullable String projectId, @Nullable String spaceId) {

        // 查询列表数据
        List<DeviceCloud> deviceCloudList = deviceCloudDao.findAll((root, criteriaQuery, criteriaBuilder) -> {
            //集合 用于封装查询条件
            List<Predicate> list = new ArrayList<>();
            // 项目ID
            if (StringUtils.isNotEmpty(projectId)) {
                list.add(criteriaBuilder.equal(root.get("iotDevice").get("project").get("id"), projectId));
            }
            // 项目空间ID
            if (StringUtils.isNotEmpty(spaceId)) {
                list.add(criteriaBuilder.equal(root.get("iotDevice").get("space").get("id"), spaceId));
            }
            return criteriaBuilder.and(list.toArray(new Predicate[list.size()]));
        });


        return convert(deviceCloudList);
    }

    /**
     * 对象转换
     *
     * @param sourceList 待转对象
     * @return 转换目标对象
     * @author Yang.Lee
     * @date 2021/3/11 14:31
     **/
    public List<DistinguishDeviceListResp> convert(List<DeviceCloud> sourceList) {
        List<DistinguishDeviceListResp> result = new ArrayList<>();

        if (sourceList == null || sourceList.isEmpty()) {
            return result;
        }
        sourceList.forEach(obj -> result.add(convert(obj)));
        return result;
    }

    /**
     * 对象转换
     *
     * @param source 待转对象
     * @return 转换目标对象
     * @author Yang.Lee
     * @date 2021/3/11 14:29
     **/
    public DistinguishDeviceListResp convert(@NotNull DeviceCloud source) {
        DistinguishDeviceListResp result = new DistinguishDeviceListResp();
        result.setId(source.getId());
        result.setName(source.getName());
        return result;
    }
}
