package cn.com.cowain.ibms.rest.resp.weather;

import cn.com.cowain.ibms.service.bean.amap.weather.WeatherPicture;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Yang.Lee
 * @date 2021/4/15 9:32
 */
@Data
@ApiModel("天气数据")
public class BaseWeatherResp implements Serializable {

    private static final long serialVersionUID = 2803908560686544184L;

    @ApiModelProperty(value = "城市", position = 1)
    private String city;

    @ApiModelProperty(value = "天气", position = 2)
    private String weather;

    @ApiModelProperty(value = "天气图标", position = 3)
    private WeatherPicture weatherPicture;

    @ApiModelProperty(value = "温度，单位：℃", position = 4)
    private String temperature;

    @ApiModelProperty(value = "数据时间", position = 5)
    private String dataTime;

    @ApiModelProperty(value = "风向描述", position = 6)
    private String windDirection;
}
