package cn.com.cowain.ibms.rest.resp.working_mode;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 *  @title 场景列表展示
 *  @Description 描述
 *  @author jf.sui
 *  @Date
 */
@Data
@ApiModel("场景列表响应对象")
public class WebWorkingModePageResp {

    @ApiModelProperty(value = "场景名称id", required = true)
    private String id;

    @ApiModelProperty(value = "场景名称图标", required = true)
    private String imgUrl;

    @ApiModelProperty(value = "场景名称", required = true)
    private String name;

    @ApiModelProperty(value = "用途")
    private String purpose;

    @ApiModelProperty(value = "是否添加")
    private boolean add;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "创建时间", required = true)
    private LocalDateTime createTime;
}
