# 说明

## 1. 项目信息
### 1.1 gitlab project ID
68

### 1.2 目录结构
- src：ibms 程序主模块
- docs: 各种文件

## 2. 初始化
### 2.1 创建数据库
create database ibms_dev CHARSET=utf8mb4;

### 2.2 生成表结构
执行 cn.com.cowain.ibms.DdlToolTest.generateTables() 方法

## 3. 开发技巧

### 3.1 安装插件
- lombok
- Alibaba Java Coding Guidelines

### 3.2 请求参数校验
- 加 Validated 注解
- BindingResult bindingResult，必须是最后一个参数
- 在请求对象的属性上，加Validation注解，https://blog.csdn.net/u010084868/article/details/52607025

## 4. 三级环境
### 4.1 开发环境

#### 4.1.1 服务
##### 数据库   
IP: 10.0.0.230 
Port: :31031
Schema: ibms_dev  
username: root   
password: root

##### redis  
172.16.10.71:26379

#### 4.1.2 工具
##### CI
http://172.16.10.53:8080  
developer / developer

##### swagger
http://172.16.10.71:18080/swagger-ui.html

##### yapi  
http://172.16.10.53:8098/project/15/interface/api


## 5. 构建指令

### 5.1 maven

#### 代码规范检查
```shell
mvn clean install spotbugs:check pmd:check sonar:sonar -Dmaven.test.skip=true
```

#### docker deploy
```
mvn clean package
docker build -t registry.cn-hangzhou.aliyuncs.com/cowain-aliyun/ibms-dev:2020-08-07 .
docker push registry.cn-hangzhou.aliyuncs.com/cowain-aliyun/ibms-dev:2020-08-07
```

#### docker 仓库
```
sudo docker login --username=hujingling185 registry.cn-hangzhou.aliyuncs.com
cowain666
```

#### docker run 
```
docker run -d --name ibms-dev -p 18080:18080 registry.cn-hangzhou.aliyuncs.com/cowain-aliyun/ibms-dev:2020-08-07
```

### 5.2 gradle
#### 5.2.1 compile
```
gradle classes --warning-mode all
```

#### 5.2.2 package
```
gradle clean build -x test
```

### 5.2.3 code coverage
配置需要测试的 Junit 类
找到 build.gradle 文件, 查找 include testCases,
在其中追加即可.

#### 5.2.3.1 执行命令
```
gradle clean check
gradlew clean check
```

#### 5.2.3.2 通过率报告
/ibms/build/reports/tests/test/index.html

#### 5.2.3.3 覆盖率报告
/ibms/build/codeCoverageHtml/index.html
