package cn.com.cowain.ibms.rest.req.distinguish;

import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2021/7/8 15:11
 */
@Data
public class DeviceStaffPageReq extends PageReq {

    @ApiModelProperty(value = "关键字（姓名，工号）")
    private String keyWord;
}
