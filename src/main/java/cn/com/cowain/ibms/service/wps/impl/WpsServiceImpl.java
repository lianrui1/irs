package cn.com.cowain.ibms.service.wps.impl;

import cn.com.cowain.ibms.enumeration.wps.WpsScene;
import cn.com.cowain.ibms.service.wps.WpsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.axis.encoding.Base64;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/**
 * @author wei.cheng
 * @date 2022/02/23 15:36
 */
@Slf4j
@Service
public class WpsServiceImpl implements WpsService {
    private static final String W_SIGNATURE = "_w_signature";
    private static final Map<String, String> FILE_EXT_TO_TYPE_MAP = new HashMap<>();

    static {
        //doc, dot, wps, wpt, docx, dotx, docm, dotm, rtf
        FILE_EXT_TO_TYPE_MAP.put("doc", "w");
        FILE_EXT_TO_TYPE_MAP.put("dot", "w");
        FILE_EXT_TO_TYPE_MAP.put("wps", "w");
        FILE_EXT_TO_TYPE_MAP.put("wpt", "w");
        FILE_EXT_TO_TYPE_MAP.put("docx", "w");
        FILE_EXT_TO_TYPE_MAP.put("dotx", "w");
        FILE_EXT_TO_TYPE_MAP.put("docm", "w");
        FILE_EXT_TO_TYPE_MAP.put("dotm", "w");
        FILE_EXT_TO_TYPE_MAP.put("rtf", "w");
        //xls, xlt, et, xlsx, xltx, csv, xlsm, xltm
        FILE_EXT_TO_TYPE_MAP.put("xls", "s");
        FILE_EXT_TO_TYPE_MAP.put("xlt", "s");
        FILE_EXT_TO_TYPE_MAP.put("et", "s");
        FILE_EXT_TO_TYPE_MAP.put("xlsx", "s");
        FILE_EXT_TO_TYPE_MAP.put("xltx", "s");
        FILE_EXT_TO_TYPE_MAP.put("csv", "s");
        FILE_EXT_TO_TYPE_MAP.put("xlsm", "s");
        FILE_EXT_TO_TYPE_MAP.put("xltm", "s");
        //ppt, pptx, pptm, ppsx, ppsm, pps, potx, potm, dpt, dps
        FILE_EXT_TO_TYPE_MAP.put("ppt", "p");
        FILE_EXT_TO_TYPE_MAP.put("pptx", "p");
        FILE_EXT_TO_TYPE_MAP.put("pptm", "p");
        FILE_EXT_TO_TYPE_MAP.put("ppsx", "p");
        FILE_EXT_TO_TYPE_MAP.put("ppsm", "p");
        FILE_EXT_TO_TYPE_MAP.put("pps", "p");
        FILE_EXT_TO_TYPE_MAP.put("potx", "p");
        FILE_EXT_TO_TYPE_MAP.put("potm", "p");
        FILE_EXT_TO_TYPE_MAP.put("dpt", "p");
        FILE_EXT_TO_TYPE_MAP.put("dps", "p");
        //pdf
        FILE_EXT_TO_TYPE_MAP.put("pdf", "f");
    }

    @Value("${wps.domain}")
    private String wpsDomain;
    @Value("${wps.app-id}")
    private String appId;
    @Value("${wps.app-secret}")
    private String appSecret;

    @Override
    public String buildWpsPreviewUrl(String fileId, String fileExt, String token, WpsScene wpsScene) {
        if (StringUtils.isEmpty(fileId) || StringUtils.isEmpty(fileExt) || Objects.isNull(wpsScene)) {
            return null;
        }
        String type = FILE_EXT_TO_TYPE_MAP.get(fileExt);
        if (StringUtils.isEmpty(type)) {
            log.error("文件类型无法使用WPS进行预览，fileExt:{}", fileExt);
            return null;
        }
        String url = wpsDomain + "/office/" + FILE_EXT_TO_TYPE_MAP.get(fileExt) + "/" + fileId + "?";
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("_w_appid", appId);
        paramMap.put("_w_fileid", fileId);
        paramMap.put("_w_scene", wpsScene.name());
        if (StringUtils.isNotEmpty(token)) {
            paramMap.put("_w_token", token);
        }
        String signature = getSignature(paramMap, appSecret);
        url += getUrlParam(paramMap) + "&_w_signature=" + signature;
        log.info("wps previewUrl is {}", url);
        return url;
    }

    private String getUrlParam(Map<String, String> params) {
        StringBuilder builder = new StringBuilder();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (builder.length() > 0) {
                builder.append('&');
            }
            builder.append(URLEncoder.encode(entry.getKey(), StandardCharsets.UTF_8)).append('=')
                    .append(URLEncoder.encode(entry.getValue(), StandardCharsets.UTF_8));
        }
        return builder.toString();
    }

    private String getSignature(Map<String, String> params, String appSecret) {
        List<String> keys = new ArrayList<>();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            keys.add(entry.getKey());
        }
        // 将所有参数按key的升序排序
        keys.sort(String::compareTo);
        // 构造签名的源字符串
        StringBuilder contents = new StringBuilder();
        for (String key : keys) {
            if (W_SIGNATURE.equals(key)) {
                continue;
            }
            contents.append(key).append("=").append(params.get(key));
        }
        contents.append("_w_secretkey=").append(appSecret);
        // 进行hmac sha1 签名
        byte[] bytes = hmacSha1(appSecret.getBytes(StandardCharsets.UTF_8), contents.toString().getBytes(StandardCharsets.UTF_8));
        //字符串经过Base64编码
        String sign = Base64.encode(Objects.requireNonNull(bytes));
        sign = URLEncoder.encode(sign, StandardCharsets.UTF_8);
        log.info("构建wps签名，sign:{}", sign);
        return sign;
    }

    private byte[] hmacSha1(byte[] key, byte[] data) {
        try {
            SecretKeySpec signingKey = new SecretKeySpec(key, "HmacSHA1");
            Mac mac = Mac.getInstance(signingKey.getAlgorithm());
            mac.init(signingKey);
            return mac.doFinal(data);
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            log.error("error hmacSha1, e:{}", e.getMessage());
            throw new RuntimeException("进行hmac sha1签名加密失败");
        }
    }
}
