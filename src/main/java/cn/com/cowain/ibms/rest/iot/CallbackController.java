package cn.com.cowain.ibms.rest.iot;

import cn.com.cowain.ibms.rest.req.iot.CallBackDuerosReq;
import cn.com.cowain.ibms.service.bean.DoorMagneticHWParasReq;
import cn.com.cowain.ibms.service.bean.DoorMagneticHWReq;
import cn.com.cowain.ibms.service.device.HwProductsService;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @Author tql
 * @Description 接口回调控制器
 * @Date 22-8-23 下午3:44
 * @Version 1.0
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/callback")
@Api(tags = IConst.MODULE_CALLBACK)
public class CallbackController {

    @Autowired
    private HwProductsService hwProductsService;

    /**
     * 接口回调
     *
     * @param callbackReq
     * @return
     */
    @PostMapping()
    @ApiOperation(value = "接口回调", tags = IConst.MODULE_CALLBACK)
    public ResponseEntity<JsonResult<String>> record(@RequestBody @Validated CallBackDuerosReq callbackReq) {
        log.info("接口回调数据：" + JSON.toJSONString(callbackReq));
        DoorMagneticHWReq hwReq = new DoorMagneticHWReq();
        DoorMagneticHWParasReq paras = new DoorMagneticHWParasReq();
        // 控制窗帘
        if ("BD_all_lights_room".equals(callbackReq.getPayload().getAppliance().getApplianceId())) {
            // 灯操作
            if ("TurnOffRequest".equals(callbackReq.getHeader().getName())) {
                // 关灯
                paras.setLight1("off");
                paras.setLight2("off");
                paras.setLight3("off");
                paras.setLight4("off");
            } else if ("TurnOnRequest".equals(callbackReq.getHeader().getName())) {
                //开灯
                paras.setLight1("on");
                paras.setLight2("on");
                paras.setLight3("on");
                paras.setLight4("on");
            }
            hwReq.setParas(paras);
            hwReq.setDeviceId("42066fd6-0a6a-4264-89be-83e47425aede");
            hwReq.setDevice_id("5ff3bf058a9ec802e487d52f_HQ_ISW4TEST");
            hwReq.setService_id("switch");
            hwReq.setCommand_name("");
            hwProductsService.updateSwitch(hwReq, "", "灯");

        }
        return ResponseEntity.ok(JsonResult.ok("设备命令操作成功", null));
    }
}
