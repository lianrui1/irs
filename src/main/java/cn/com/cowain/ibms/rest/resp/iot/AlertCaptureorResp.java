package cn.com.cowain.ibms.rest.resp.iot;

import cn.com.cowain.ibms.entity.iot.AlertCaptureor;
import cn.com.cowain.ibms.enumeration.AlertCaptureorStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * 警戒抓拍记录返回参数
 *
 * @author: yanzy
 * @date: 2022/4/13 13:32
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("警戒抓拍记录返回参数")
public class AlertCaptureorResp {

    @ApiModelProperty(value = "警戒抓拍记录id", required = true, position = 1)
    private String id;

    @ApiModelProperty(value = "查询状态(name)", position = 2)
    private String status;

    @ApiModelProperty(value = "空间名称", position = 3)
    private String spaceName;

    @ApiModelProperty(value = "警戒点名称", position = 4)
    private String name;

    @ApiModelProperty(value = "抓拍类型", position = 5)
    private String alertType;

    @ApiModelProperty(value = "抓拍子类型", position = 6)
    private String alertType2;

    @ApiModelProperty(value = "目标类型", position = 7)
    private String targetType;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "抓拍时间", required = true, position = 8)
    private LocalDateTime alertTime;

    @ApiModelProperty(value = "抓拍图片", required = true, position = 9)
    private String snapPicture;

    @ApiModelProperty(value = "人员工号", position = 10)
    private String userHrId;

    @ApiModelProperty(value = "人员姓名", position = 11)
    private String userName;

    @ApiModelProperty(value = "抓拍用户部门", position = 12)
    private String userDepartment;

    @ApiModelProperty(value = "具体位置",position = 13)
    private String address;

    @ApiModelProperty(value = "查看状态",position = 14)
    private AlertCaptureorStatus statusType;

    /**
     * 对象转换
     *
     * @author: yanzy
     * @data: 2022/4/13 15:04:33
     */
    public static AlertCaptureorResp convert(AlertCaptureor alertCaptureor) {

        //目标类型
        String targetType = "";
        if (alertCaptureor.getUserHrId() != null) {
            targetType = "人员";
        } else {
            targetType = "陌生人员";
        }

        return AlertCaptureorResp.builder()
                //id
                .id(alertCaptureor.getId())
                .status(alertCaptureor.getStatus().getName())
                .spaceName(alertCaptureor.getSpace().getName())
                .name(alertCaptureor.getName())
                .alertType(alertCaptureor.getAlertType().getName())
                .alertType2(Objects.nonNull(alertCaptureor.getAlertType2()) ? alertCaptureor.getAlertType2().getName() : null)
                .targetType(targetType)
                .alertTime(alertCaptureor.getAlertTime())
                .snapPicture(alertCaptureor.getSnapPicture())
                .userHrId(alertCaptureor.getUserHrId())
                .userName(alertCaptureor.getUserName())
                .userDepartment(alertCaptureor.getUserDepartment())
                .address(alertCaptureor.getAddress())
                .statusType(alertCaptureor.getStatus())
                .build();
    }
}
