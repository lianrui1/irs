package cn.com.cowain.ibms.entity.distinguish;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 通行权限 - 用户组 关系对象
 * @author Yang.Lee
 * @date 2021/3/10 9:08
 */
@Data
@Entity
@Table(name = "ibms_access_rules_staff_group")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_access_rules_staff_group", comment = "通行权限-用户组 关系表")
@Where(clause = "is_del=0")
public class AccessRulesToStaffGroup extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "rule_id", nullable = false, columnDefinition = "varchar(36)  COMMENT 'FK 通行权限ID'")
    private AccessRules rule;

    @ManyToOne
    @JoinColumn(name = "staff_group_id", nullable = false, columnDefinition = "varchar(36)  COMMENT 'FK 人员组ID'")
    private StaffGroup group;
}
