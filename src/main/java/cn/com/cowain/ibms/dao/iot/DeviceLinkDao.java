package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.DeviceLink;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface DeviceLinkDao extends JpaRepository<DeviceLink, String>, JpaSpecificationExecutor<DeviceLink> {

    /**
     * 查询当天创建的记录数
     *
     * @return
     */
    @Query(nativeQuery = true, value = "select count(id) from iot_device_link where to_days(now()) = to_days(created_time)")
    int getTodayTotalNumber();
}
