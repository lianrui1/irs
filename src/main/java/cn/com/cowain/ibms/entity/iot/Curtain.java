package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/15 16:09
 */
@Data
@Entity
@Table(name = "iot_curtain")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_curtain", comment = "窗帘设备表")
public class Curtain extends BaseEntity {

    // 动作
    private String action;

    /**
     * iot 设备
     */
    @OneToOne
    @JoinColumn(name = "device_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK。设备表主键id'")
    private IotDevice device;

    @Column(name = "isOne", nullable = false, columnDefinition = "tinyint(1) default 0 COMMENT '窗帘是否单个命令下发'")
    private int isOne;
}
