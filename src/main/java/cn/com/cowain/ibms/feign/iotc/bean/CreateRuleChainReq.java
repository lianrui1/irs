package cn.com.cowain.ibms.feign.iotc.bean;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 规则链创建接收对象
 *
 * @author: yanzy
 * @date: 2022/2/17 18:40
 */
@Data
@ApiModel("规则链创建接收对象")
public class CreateRuleChainReq {

    @ApiModelProperty(value = "创建人姓名", required = true)
    private String userName;

    @NotEmpty
    @ApiModelProperty(value = "规则链Id", required = true)
    private String ruleChainId;

    @NotEmpty
    @ApiModelProperty(value = "规则链名称", required = true)
    private String ruleChainName;

    @NotNull
    @ApiModelProperty(value = "判断条件", example = "全部1/任一2")
    private Integer isAll;

    @ApiModelProperty(value = "定时小时")
    private Integer hour;

    @ApiModelProperty(value = "定时分钟")
    private Integer minutes;

    @ApiModelProperty(value = "传感器执行条件")
    private List<PropertiesReq> propertiesList;

    @ApiModelProperty(value = "人体传感器执行条件")
    private List<BodySensorReq> bodySensorList;

    @ApiModelProperty(value = "开始时间小时", example = "5")
    private Integer startTimeHour;

    @ApiModelProperty(value = "开始时间分钟", example = "5")
    private Integer startTimeMinutes;

    @ApiModelProperty(value = "结束时间小时", example = "5")
    private Integer endTimeHour;

    @ApiModelProperty(value = "结束时间分钟", example = "5")
    private Integer endTimeMinutes;

    @ApiModelProperty(value = "限定日期", example = "567")
    private String days;


}
