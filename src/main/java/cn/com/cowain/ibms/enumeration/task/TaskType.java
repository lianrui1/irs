package cn.com.cowain.ibms.enumeration.task;

/**
 * @author wei.cheng
 * @date 2022/03/10 16:22
 */
public enum TaskType {
    /**
     * 通行权限申请
     */
    ACCESS_CONTROL_SPOT_APPLICATION("通行权限申请");

    private String name;

    TaskType(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
