package cn.com.cowain.ibms.service.disthinguish.impl;

import cn.com.cowain.ibms.dao.distinguish.*;
import cn.com.cowain.ibms.dao.iot.SpaceDao;
import cn.com.cowain.ibms.dao.space.ProjectDao;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.distinguish.*;
import cn.com.cowain.ibms.entity.distinguish.hk.AuthFail;
import cn.com.cowain.ibms.entity.distinguish.hk.DownloadFailData;
import cn.com.cowain.ibms.entity.iot.HkDevice;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.space.Project;
import cn.com.cowain.ibms.enumeration.HkAddress;
import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.distinguish.*;
import cn.com.cowain.ibms.rest.req.ms_device.AccessRuleCreateReq;
import cn.com.cowain.ibms.rest.req.ms_device.AccessRuleUpdateReq;
import cn.com.cowain.ibms.rest.resp.EhrVisitorResp;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.distinguish.*;
import cn.com.cowain.ibms.service.ExcelService;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.HkAddUser;
import cn.com.cowain.ibms.service.bean.ServiceMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.disthinguish.AccessRulesService;
import cn.com.cowain.ibms.service.disthinguish.AccessRulesToStaffGroupService;
import cn.com.cowain.ibms.service.disthinguish.StaffGroupClient;
import cn.com.cowain.ibms.service.disthinguish.StaffGroupService;
import cn.com.cowain.ibms.service.iot.DoorPlateService;
import cn.com.cowain.ibms.service.ms_device.MSDeviceService;
import cn.com.cowain.ibms.service.space.SpaceService;
import cn.com.cowain.ibms.utils.*;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.sfp.xunfei.util.EncryptUtil;
import cn.com.cowain.usercenter.api.service.UserCenterStaffApi;
import cn.com.cowain.usercenter.common.resp.PassportRespForFeign;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Nullable;
import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 通行权限逻辑实现类
 *
 * @author Yang.Lee
 * @date 2021/3/10 9:04
 */
@Slf4j
@Service
public class AccessRuleServiceImpl implements AccessRulesService {

    @Resource
    private ProjectDao projectDao;

    @Resource
    private SpaceDao spaceDao;

    @Resource
    private DeviceCloudDao deviceCloudDao;

    @Resource
    private StaffGroupDao staffGroupDao;

    @Resource
    private TimePlanDao timePlanDao;

    @Resource
    private AccessRulesDao accessRulesDao;

    @Resource
    private AccessRulesToStaffGroupDao accessRulesToStaffGroupDao;

    @Resource
    private StaffGroupMembersDao membersDao;

    @Resource
    private AccessRulesToStaffGroupService accessRulesToStaffGroupService;

    @Resource
    private DoorPlateService doorPlateService;

    @Resource
    private StaffGroupService staffGroupService;

    @Resource
    private MSDeviceService msDeviceService;

    @Resource
    private StaffGroupClient staffGroupClient;

    @Autowired
    private RestTemplateUtil restTemplateUtil;

    @Autowired
    private HkDeviceDao hkDeviceDao;

    @Value("${cowain.hk.user.ks.url}")
    private String hkUserKsUrl;

    @Value("${cowain.hk.user.nt.url}")
    private String hkUserNtUrl;

    @Value("${cowain.hk.face.record.ks.url}")
    private String hkFaceRecordKsUrl;

    @Value("${cowain.hk.face.record.nt.url}")
    private String hkFaceRecordNtUrl;

    @Value("${cowain.hk.user.delete.ks.url}")
    private String hkDeleteUserKsUrl;

    @Value("${cowain.hk.user.delete.nt.url}")
    private String hkDeleteUserNtUrl;

    @Value("${cowain.hk.user.time.ks.url}")
    private String hkUpdateTimeKsUrl;

    @Value("${cowain.hk.user.time.nt.url}")
    private String hkUpdateTimeNtUrl;

    @Value("${cowain.hk.user.add.ks.url}")
    private String hkUserAddKsUrl;

    @Value("${cowain.hk.user.add.nt.url}")
    private String hkUserAddNtUrl;

    @Value("${cowain.hk.user.add.more.ks.url}")
    private String hkUserAddMoreKsUrl;

    @Value("${cowain.hk.user.add.more.nt.url}")
    private String hkUserAddMoreNtUrl;

    @Value("${cowain.hk.user.face.ks.url}")
    private String hkUpdateFaceKsUrl;

    @Value("${cowain.hk.user.face.nt.url}")
    private String hkUpdateFaceNtUrl;

    @Value("${cowain.hk.result.ks.url}")
    private String hkResultKsUrl;

    @Value("${cowain.hk.result.nt.url}")
    private String hkResultNtUrl;

    @Value("${cowain.hk.personId.ks.url}")
    private String hkFindPidKsUrl;

    @Value("${cowain.hk.personId.nt.url}")
    private String hkFindPidNtUrl;

    @Value("${cowain.ehr.visitor.url}")
    private String visitorUrl;

    @Resource
    private RedisUtil redisUtil;

    @Resource
    private ExcelService excelService;

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserServiceUC;

    @Autowired
    private RestTemplate restTemplate;

    @Resource
    private UserCenterStaffApi userCenterStaffApi;

    private static final String STAFFKEY = "staffList";
    /**
     * 新建通行权限
     *
     * @param req : 请求参数
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/3/10 9:03
     **/
    @Override
    @Transactional
    public ServiceResult save(AccessRulesReq req) {

        // 参数校验
        ServiceResult result = paramConvert(req);
        if (!result.isSuccess()) {
            return result;
        }
        AccessRules accessRules = (AccessRules) result.getObject();
        // 验证用户组是否存在
        List<String> groupIds = req.getStaffGroups();
        Optional<StaffGroup> staffGroupOptional;
        for (String groupId : groupIds) {
            staffGroupOptional = staffGroupDao.findById(groupId);
            if (!staffGroupOptional.isPresent()) {
                return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_STAFF_GROUP, groupId));
            }
            // 向旷世新增人员组
            StaffGroupReq staffGroupReq = new StaffGroupReq();
            StaffGroup staffGroup = staffGroupOptional.get();
            // 确认本人员组在旷世是否存在
            GroupExistReq groupExistReq = new GroupExistReq();
            groupExistReq.setGroupName(staffGroup.getName());
            String exist = staffGroupClient.exist(groupExistReq);
            JSONObject existResult = JSON.parseObject(exist);
            String msg = existResult.getString("msg");
            if(msg.equals("用户组不存在")){
                List<StaffGroupMembers> staffGroupMembers = membersDao.findByGroupId(staffGroup);
                staffGroupReq.setMembersList(staffGroupMembers);
                staffGroupReq.setName(staffGroup.getName());
                staffGroupReq.setStaffGroup(staffGroup);
                log.info("向KS创建人员组");
                ServiceResult result1 = staffGroupService.saveStaffGroup(staffGroupReq);
                if(!result1.isSuccess()){
                    return ServiceResult.error(ServiceMessage.getMessageWithColon(result1.getObject().toString(), groupId));
                }
            }
        }
        // 校验该设备是否已经有了权限信息
        Optional<AccessRules> accessRulesOptional = accessRulesDao.findByDeviceCloud(accessRules.getDeviceCloud());
        if(accessRulesOptional.isPresent()){
            // 如果设别已存在通行权限，则不可新建
            return ServiceResult.error(ServiceMessage.getMessageWithColon("该设备已存在通行权限", req.getDistinguishDeviceId()));
        }

        // 保存通行计划数据
        accessRulesDao.save(accessRules);

        // 保存通行计划 - 用户组关系
        List<StaffGroup> groups = staffGroupDao.findAllById(req.getStaffGroups());
        accessRulesToStaffGroupService.saveGroupIdsByRule(accessRules, groups);

        // 业务处理成功后，将数据发送给ms-device
        // 构建请求参数
        AccessRuleCreateReq param = new AccessRuleCreateReq();
        // 旷世设备id数组，使用旷世云返回的设备id
        param.setDeviceUuidList(new String[]{accessRules.getDeviceCloud().getKsDeviceId()});

        // 人员组id数组，使用ibms中的业务id
        List<String> groupIdList = new ArrayList<>();
        groups.forEach(obj -> groupIdList.add(obj.getId()));
        param.setGroupUuidList(groupIdList.toArray(new String[0]));
        log.info("向旷视设备添加权限: " + JSON.toJSONString(param));
        // 发送请求
        result = msDeviceService.createAccessRule(param);

        if(!result.isSuccess()){
            // 业务处理失败，手动回滚事务,返回异常
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return result;
        }

        // 新建成功，返回新增的id
        IdResp idResp = new IdResp();
        idResp.setId(accessRules.getId());

        return ServiceResult.ok(idResp);
    }

    /**
     * 更新通行权限
     *
     * @param id  记录id
     * @param req 请求参数
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/3/10 14:34
     **/
    @Override
    @Transactional
    public ServiceResult update(String id, AccessRulesReq req) {

        // 参数校验
        ServiceResult result = paramConvert(req);
        if (!result.isSuccess()) {
            return result;
        }
        // 查询数据库中判断是否存在记录
        Optional<AccessRules> accessRulesOptional = accessRulesDao.findById(id);
        if (!accessRulesOptional.isPresent()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_ACCESS_RULES, id));
        }

        // 更新请求数据
        AccessRules targetRules = (AccessRules) result.getObject();
        // 数据库里的原数据
        AccessRules sourceRules = accessRulesOptional.get();
        // 将需要更新的数据赋值给sourceRules对象
        sourceRules.setProject(targetRules.getProject());
        sourceRules.setSpace(targetRules.getSpace());
        sourceRules.setDeviceSource(sourceRules.getDeviceSource());
        sourceRules.setTimePlan(sourceRules.getTimePlan());
        // 保存通行记录
        accessRulesDao.save(sourceRules);

        // 编辑rules - staff group 关系信息
        // 操作步骤：1.对比原数据库中已有的关系和更新请求中的数据，找到需要删除的数据以及需要添加的数据
        // 2. 删除已经不存在的关系，添加新的关系
        Set<AccessRulesToStaffGroup> rule2GroupList = accessRulesToStaffGroupDao.findByRuleId(id);  // 数据库中已存在的关系列表
        Set<AccessRulesToStaffGroup> deletedSet = new HashSet<>();   // 需要删除的数据集合
        Set<AccessRulesToStaffGroup> noUpdateSet = new HashSet<>();   // 依然存在的数据集合
        rule2GroupList.forEach(rule2Group -> {
            if (!req.getStaffGroups().contains(rule2Group.getGroup().getId())) {
                // 设置为删除
                rule2Group.setIsDelete(1);
                deletedSet.add(rule2Group);
            } else {
                noUpdateSet.add(rule2Group);
            }
        });

        // 删除无效关系
        if (!deletedSet.isEmpty()) {
            accessRulesToStaffGroupDao.saveAll(deletedSet);
        }

        // 验证用户组是否存在
        List<String> groupIds = req.getStaffGroups();
        Optional<StaffGroup> staffGroupOptional;
        for (String groupId : groupIds) {
            staffGroupOptional = staffGroupDao.findById(groupId);
            if (!staffGroupOptional.isPresent()) {
                return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_STAFF_GROUP, groupId));
            }
            // 向旷世新增人员组
            StaffGroupReq staffGroupReq = new StaffGroupReq();
            StaffGroup staffGroup = staffGroupOptional.get();
            // 确认本人员组在旷世是否存在
            GroupExistReq groupExistReq = new GroupExistReq();
            groupExistReq.setGroupName(staffGroup.getName());
            String exist = staffGroupClient.exist(groupExistReq);
            JSONObject existResult = JSON.parseObject(exist);
            String msg = existResult.getString("msg");
            if(msg.equals("用户组不存在")){
                List<StaffGroupMembers> staffGroupMembers = membersDao.findByGroupId(staffGroup);
                staffGroupReq.setMembersList(staffGroupMembers);
                staffGroupReq.setName(staffGroup.getName());
                staffGroupReq.setStaffGroup(staffGroup);
                log.info("向KS创建人员组");
                ServiceResult result1 = staffGroupService.saveStaffGroup(staffGroupReq);
                if(!result1.isSuccess()){
                    return ServiceResult.error(ServiceMessage.getMessageWithColon(result1.getObject().toString(), groupId));
                }
            }
        }

        // 查询出需要添加的所有人员组集合，再减去不需要变化的数据，剩下的数据都新增到关系表中
        List<StaffGroup> targetGroups = staffGroupDao.findAllById(req.getStaffGroups());

        if (!targetGroups.isEmpty()) {
            // 需要修改的对象集合
            List<StaffGroup> noChange = new ArrayList<>();
            targetGroups.forEach(obj -> {
                for (AccessRulesToStaffGroup access2Group : noUpdateSet) {
                    if (obj.getId().equals(access2Group.getGroup().getId())) {
                        noChange.add(obj);
                    }
                }
            });
            targetGroups.removeAll(noChange);
            if (!targetGroups.isEmpty()) {
                accessRulesToStaffGroupService.saveGroupIdsByRule(sourceRules, targetGroups);
            }
        }

        // 业务处理成功后，将数据发送给ms-device
        // 构建请求参数
        AccessRuleUpdateReq param = new AccessRuleUpdateReq();
        // 旷世设备id数组，使用旷世云返回的设备id
        param.setDeviceUuid(sourceRules.getDeviceCloud().getKsDeviceId());
        param.setNewGroupUuidList(req.getStaffGroups());
        // 发送请求
        result = msDeviceService.updateAccessRule(param);

        if(!result.isSuccess()){
            // 业务处理失败，手动回滚事务,返回异常
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return result;
        }

        return ServiceResult.ok();
    }

    /**
     * 查询通行权限（分页）
     *
     * @param deviceName 设备名称，模糊搜索
     * @param deviceId   设备ID，可选
     * @param projectId  项目ID
     * @param page       页码
     * @param size       页长
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/3/10 19:01
     **/
    @Override
    @Transactional
    public PageBean<AccessRulePageResp> getPage(String deviceName, String deviceId, String projectId, int page, int size) {

        Sort sort = Sort.by(Sort.Direction.DESC, "createdTime");
        Pageable pageable = PageRequest.of(page, size, sort);
        //查询条件
        Specification<AccessRules> specification = (root, criteriaQuery, criteriaBuilder) -> {
            //集合 用于封装查询条件
            List<Predicate> list = new ArrayList<>();
            // 项目ID
            if (StringUtils.isNotEmpty(projectId)) {
                list.add(criteriaBuilder.equal(root.get("project").get("id"), projectId));
            }
            // 设备名称
            if (StringUtils.isNotEmpty(deviceName)) {
                list.add(criteriaBuilder.like(root.get("deviceCloud").get("name").as(String.class), "%" + deviceName + "%"));
            }

            if (StringUtils.isNotEmpty(deviceId)) {
                // 设备ID
                list.add(criteriaBuilder.equal(root.get("deviceCloud").get("id"), deviceId));
            }
            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        };

        Page<AccessRules> resultPage = accessRulesDao.findAll(specification, pageable);

        return convert(resultPage, pageable);
    }

    @Override
    public PageBean<AccessRulePageResp> getPage(AccessRulesPageReq req) {
//        Sort sort = Sort.by(Sort.Direction.DESC, "createdTime");
        //查询条件
        return null;
    }

    @Override
    public ServiceResult addHkUser(HkUserAddReq req) {
        // 根据设备id获取ip
        Optional<HkDevice> hkOp = hkDeviceDao.findByDeviceId(req.getDeviceId());
        if(!hkOp.isPresent()){
            return ServiceResult.error("设备id有误");
        }

        // 判断通行时间
        if(req.getStartTime().isAfter(req.getEndTime()) && req.getEndTime().isBefore(LocalDateTime.now())){
            return ServiceResult.error("请输入正确的通行时间");
        }


        int count = (req.getJobNum().size() + 500 - 1) / 500;
        for (int i = 0; i < count; i++) {
            int j = (i + 1) * 500;
            List<String> subList = req.getJobNum().subList(i * 500, Math.min(j, req.getJobNum().size()));
            UserAddReq userAddReq = new UserAddReq();
            List<String> strings = new ArrayList<>();
            strings.add(hkOp.get().getIp());
            userAddReq.setKey(strings);
            List<AuthConfigData> dataList = new ArrayList<>();
            for(String jobNo : subList){
                AuthConfigData data = new AuthConfigData();
                data.setJobNo(jobNo);
                data.setStartTime(req.getStartTime() + ":00+08:00");
                data.setEndTime(req.getEndTime() + ":59+08:00");
                dataList.add(data);
            }
            userAddReq.setData(dataList);
            if (i == count - 1) {
                userAddReq.setOver("1");
            } else {
                userAddReq.setOver("0");
            }
            String requestParam = JSON.toJSONString(userAddReq);
            log.debug("json参数 ： " + requestParam);
            String s;
            // 判断设备所属地
            if(HkAddress.KS.equals(hkOp.get().getAddress())){
                s = restTemplateUtil.jsonPost(hkUserAddKsUrl, requestParam, String.class);
            }else {
                s = restTemplateUtil.jsonPost(hkUserAddNtUrl, requestParam, String.class);
            }
            log.debug("添加结果: "  + s);
            JSONObject jsonObject = JSON.parseObject(s);
            if("-1".equals(jsonObject.getString("code"))){
                return ServiceResult.error("当前正有人员添加,请稍后操作");
            }
            JSONObject data = jsonObject.getJSONObject("data");
            JSONArray fail_data = data.getJSONArray("fail_data");
            List<HkAddUser> list = JSON.parseArray(fail_data.toJSONString(), HkAddUser.class);
            if(!list.isEmpty()){
                String msgs = "";
                for(HkAddUser user : list){
                    String msg = user.getJobNo() + "-" + user.getMsg() + ";";
                    log.error("失败人员: " + msg);
                    msgs += msg;
                }
                return ServiceResult.error(msgs);
            }
        }

        return ServiceResult.ok();
    }

    @Override
    public PageBean<HkUserPageResp> hkUserList(String name, String deviceId, int page, int size) {

        PageBean<HkUserPageResp> pageBean = new PageBean<>();
        pageBean.setPage(page);
        pageBean.setSize(size);
        // 根据设备id获取ip
        Optional<HkDevice> hkOp = hkDeviceDao.findByDeviceId(deviceId);
        if(hkOp.isPresent()){
            HkDevice hkDevice = hkOp.get();
            StringBuilder urlTemplate = new StringBuilder();
            // 判断设备所属地
            if(HkAddress.KS.equals(hkDevice.getAddress())){
                urlTemplate.append(hkUserKsUrl).append("?page=").append(page)
                        .append("&size=").append(size).append("&resourceCode=").append(hkDevice.getResourceIndexCode())
                        .append("&jobNum=").append(name);
            }else {
                urlTemplate.append(hkUserNtUrl).append("?page=").append(page)
                        .append("&size=").append(size).append("&resourceCode=").append(hkDevice.getResourceIndexCode())
                        .append("&jobNum=").append(name);
            }
            String s = restTemplateUtil.get(urlTemplate.toString());
            JSONObject jsonObject = JSON.parseObject(s);
            JSONObject jsonData = jsonObject.getJSONObject("data");
            Integer totalPages = jsonData.getInteger("totalPages");
            Integer totalElements = jsonData.getInteger("totalElements");
            JSONArray jsonIndex = jsonData.getJSONArray("list");
            List<HkUserPageResp> list = JSON.parseArray(jsonIndex.toJSONString(),HkUserPageResp.class);
            list.forEach(it -> it.setDeviceId(deviceId));
            pageBean.setList(list);
            if(list.isEmpty()){
                pageBean.setList(new ArrayList<>());
            }
            pageBean.setTotalPages(totalPages);
            pageBean.setTotalElements(totalElements);
        }
        return pageBean;
    }

    @Override
    public PageBean<HkFaceRecordPageResp> hkFaceRecord(HkFaceRecordPageReq req) {
        PageBean<HkFaceRecordPageResp> pageBean = new PageBean<>();
        pageBean.setPage(req.getPage());
        pageBean.setSize(req.getSize());
        // 根据设备id获取ip
        Optional<HkDevice> hkOp = hkDeviceDao.findByDeviceId(req.getDeviceId());
        if(hkOp.isPresent()){
            HkDevice hkDevice = hkOp.get();
            HkFaceRecordReq hkFaceRecordReq = new HkFaceRecordReq();
            BeanUtils.copyProperties(req, hkFaceRecordReq);
            hkFaceRecordReq.setResourceCode(hkDevice.getResourceIndexCode());
            String requestParam = JSON.toJSONString(hkFaceRecordReq);
            log.debug("json参数 ： " + requestParam);
            String s;
            if(HkAddress.KS.equals(hkDevice.getAddress())){
                s = restTemplateUtil.jsonPost(hkFaceRecordKsUrl, requestParam, String.class);
            }else {
                s = restTemplateUtil.jsonPost(hkFaceRecordNtUrl, requestParam, String.class);
            }
            JSONObject jsonObject = JSON.parseObject(s);
            JSONObject jsonData = jsonObject.getJSONObject("data");
            Integer totalPages = jsonData.getInteger("totalPages");
            Integer totalElements = jsonData.getInteger("totalElements");
            JSONArray jsonIndex = jsonData.getJSONArray("list");
            List<HkFaceRecordPageResp> list = new ArrayList<>();
            if(null != jsonIndex){
                list = JSON.parseArray(jsonIndex.toJSONString(),HkFaceRecordPageResp.class);
                pageBean.setList(list);
            }
            if(list.isEmpty()){
                pageBean.setList(new ArrayList<>());
            }
            pageBean.setTotalPages(totalPages);
            pageBean.setTotalElements(totalElements);
        }
        return pageBean;
    }

    @Override
    @Transactional
    public ServiceResult deleteHkUser(String deviceId, List<String> jobNos) {
        // 根据设备id获取ip
        Optional<HkDevice> hkOp = hkDeviceDao.findByDeviceId(deviceId);
        if(hkOp.isPresent()){
            HkDevice hkDevice = hkOp.get();
            UserAddReq userAddReq = new UserAddReq();
            List<String> strings = new ArrayList<>();
            strings.add(hkDevice.getIp());
            userAddReq.setKey(strings);
            List<AuthConfigData> dataList = new ArrayList<>();
            for(String jobNo : jobNos){
                AuthConfigData data = new AuthConfigData();
                data.setJobNo(jobNo);
                dataList.add(data);
            }
            userAddReq.setData(dataList);
            userAddReq.setOver("1");
            String requestParam = JSON.toJSONString(userAddReq);
            log.debug("json参数 ： " + requestParam);
            try {
                if(HkAddress.KS.equals(hkDevice.getAddress())){
                    restTemplateUtil.jsonPost(hkDeleteUserKsUrl, requestParam, String.class);
                }else {
                    restTemplateUtil.jsonPost(hkDeleteUserNtUrl, requestParam, String.class);
                }
            }catch (Exception e){
                log.error("设备IP: " + hkDevice.getIp());
                log.error("删除人员: " + jobNos);
                return ServiceResult.error("删除人员失败");
            }

        }else {
            return ServiceResult.error("设备ID有误");
        }
        return ServiceResult.ok();
    }

    @Override
    public ServiceResult updateUserHkTime(UserTimeReq req) {
        // 比较通行时间
        if(req.getStartTime().isAfter(req.getEndTime())){
            return ServiceResult.error("请输入正确的通行时间");
        }
        List<AuthConfigData> data = new ArrayList<>();
        AuthConfigData authConfigData = new AuthConfigData();
        // 根据设备id获取ip
        Optional<HkDevice> hkOp = hkDeviceDao.findByDeviceId(req.getDeviceId());
        if(hkOp.isPresent()){
            HkDevice hkDevice = hkOp.get();
            HkUserTimeReq timeReq = new HkUserTimeReq();
            authConfigData.setJobNo(req.getJobNo());
            authConfigData.setStartTime(req.getStartTime() + ":00+08:00");
            authConfigData.setEndTime(req.getEndTime().toString() + ":59+08:00");
            data.add(authConfigData);
            timeReq.setIp(hkDevice.getIp());
            timeReq.setData(data);
            timeReq.setOver("1");
            String requestParam = JSON.toJSONString(timeReq);
            log.debug("json参数 ： " + requestParam);
            try {
                if(HkAddress.KS.equals(hkDevice.getAddress())){
                    restTemplateUtil.jsonPost(hkUpdateTimeKsUrl, requestParam, String.class);
                }else {
                    restTemplateUtil.jsonPost(hkUpdateTimeNtUrl, requestParam, String.class);
                }
            }catch (Exception e){
                log.error("设备IP: " + hkDevice.getIp());
                log.error("更新人员: " + req.getJobNo());
                return ServiceResult.error("更新通行时间失败");
            }

        }else {
            return ServiceResult.error("设备ID有误");
        }
        return ServiceResult.ok();
    }

    @Override
    public ServiceResult updateFace(HkUpdateFaceReq req) {
        // 根据设备id获取ip
        Optional<HkDevice> hkOp = hkDeviceDao.findByDeviceId(req.getDeviceId());
        if(hkOp.isPresent()){
            HkDevice hkDevice = hkOp.get();
            HkUpdateFaceReq hkUpdateFaceReq = new HkUpdateFaceReq();
            hkUpdateFaceReq.setPersonId(req.getPersonId());
            hkUpdateFaceReq.setFaceData(req.getFaceData());
            String requestParam = JSON.toJSONString(hkUpdateFaceReq);
            log.debug("json参数 ： " + requestParam);
            try {
                String s;
                if(HkAddress.KS.equals(hkDevice.getAddress())){
                    s = restTemplateUtil.jsonPost(hkUpdateFaceKsUrl, requestParam, String.class);
                }else {
                    s = restTemplateUtil.jsonPost(hkUpdateFaceNtUrl, requestParam, String.class);
                }
                JSONObject jsonObject = JSON.parseObject(s);
                if("0".equals(jsonObject.getString("status"))){
                    return ServiceResult.error("人脸修改失败");
                }
            }catch (Exception e){
                log.error("设备IP: " + hkDevice.getIp());
                return ServiceResult.error("更新人脸失败");
            }

        }else {
            return ServiceResult.error("设备ID有误");
        }
        return ServiceResult.ok();
    }

    @Override
    public ServiceResult uploadAccessRules(MultipartFile file, List<String> deviceIds) {
        // 获取海康设备信息
        List<HkDevice> hkDeviceList = hkDeviceDao.findByDeviceIdIn(deviceIds);
        // 所有设备ip
        List<String> ksIps = new ArrayList<>();
        List<String> ntIps = new ArrayList<>();
        hkDeviceList.forEach(it -> {
            if(HkAddress.KS.equals(it.getAddress())){
                ksIps.add(it.getIp());
            }else {
                ntIps.add(it.getIp());
            }
        });
        ServiceResult result;
        // 读取excel
        try {
            result = excelService.readAccessRulesExcel(file);
            if (!result.isSuccess() && ErrConst.E23.equals(result.getErrorCode())) {
                return result;
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ServiceResult.error("excel文件内容格式异常");
        }
        // 所有上传的数据行
        List<String[]> dataList = (List<String[]>) result.getObject();
        if(dataList.size() > 800){
            return ServiceResult.error("数据上传过多,请分批次上传,每次最多800人");
        }
        // 数据总数
        int totalCount = dataList.size();
        // 有错误的数据集合
        List<String[]> errorList = getErrorExcelData(dataList);
        // 所有数据去除错误数据，剩下的就是正常数据，准备写入数据库
        dataList.removeAll(errorList);
        // 获取向海康发送的人员集合
        List<AuthConfigData> data = getHkUserList(dataList);
        // 向海康批量新增人员
        UserAddReq userAddReq = new UserAddReq();
        userAddReq.setData(data);
        userAddReq.setOver("1");
        // 判断设备所属地
        String ksUUID = "";
        String ntUUID = "";
        try {
            JSONObject jsonObjectKs = getJSONObject(ksIps, dataList, errorList, userAddReq, hkUserAddMoreKsUrl);
            if(jsonObjectKs != null){
                if("-1".equals(jsonObjectKs.getString("code"))){
                    return ServiceResult.error("当前正有海康任务进行,请稍后操作");
                }
                JSONObject dataJson = jsonObjectKs.getJSONObject("data");
                ksUUID = dataJson.getString("taskId");
            }
            JSONObject jsonObjectNt = getJSONObject(ntIps, dataList, errorList, userAddReq, hkUserAddMoreNtUrl);
            if(jsonObjectNt != null){
                if("-1".equals(jsonObjectNt.getString("code"))){
                    return ServiceResult.error("当前正有海康任务进行,请稍后操作");
                }
                JSONObject dataJson = jsonObjectNt.getJSONObject("data");
                ntUUID = dataJson.getString("taskId");
            }
        }catch (Exception e){
            log.error(e.getMessage(), e);
            return ServiceResult.error("海康新增人员失败");
        }



        String uuid = UUID.randomUUID().toString();

        if (!dataList.isEmpty()) {
            // 存取当前配置通行时间
            String redisKey = redisUtil.createRealKey("AccessRulesImportSuccess-" + uuid);
            redisUtil.set(redisKey, dataList, RedisUtil.ONE_DAY_SECOND);
        }
        if (!dataList.isEmpty()) {
            // 存取当前配置通行时间
            String redisKey = redisUtil.createRealKey("AccessRulesImportTime-" + uuid);
            redisUtil.set(redisKey, dataList.get(0)[2] + "," +dataList.get(0)[3], RedisUtil.ONE_DAY_SECOND);
        }
        if (!errorList.isEmpty()) {
            // 将失败数据存入缓存，供用户下载
            String redisKey = redisUtil.createRealKey("AccessRulesImportError-" + uuid);
            redisUtil.set(redisKey, errorList, RedisUtil.ONE_DAY_SECOND);
        }

        return ServiceResult.ok(StaffGroupImportResp.builder()
                .totalCount(totalCount)
                .successCount(dataList.size())
                .errorCount(errorList.size())
                .errorDataId(uuid)
                .ksUUID(ksUUID)
                .ntUUID(ntUUID)
                .build());
    }

    private JSONObject getJSONObject(List<String> ips, List<String[]> dataList, List<String[]> errorList, UserAddReq userAddReq, String hkUserAddMoreNtUrl) {
        JSONObject jsonObject = null;
        if(!ips.isEmpty()){
            userAddReq.setKey(ips);
            String requestParam = JSON.toJSONString(userAddReq);
            log.debug("json参数 ： " + requestParam);
            String s = restTemplateUtil.jsonPost(hkUserAddMoreNtUrl, requestParam, String.class);
            jsonObject = addErrorList(errorList, s, dataList);
        }
        return jsonObject;
    }

    private JSONObject addErrorList(List<String[]> errorList, String s, List<String[]> dataList) {
        log.debug("添加结果: "  + s);
        JSONObject jsonObject = JSON.parseObject(s);
        JSONObject dataJson = jsonObject.getJSONObject("data");
        if(dataJson != null){
            JSONArray failData = dataJson.getJSONArray("fail_data");
            if(failData != null){
                List<HkAddUser> list = JSON.parseArray(failData.toJSONString(), HkAddUser.class);
                if(!list.isEmpty()){
                    for(HkAddUser user : list){
                        log.error("HK添加人员失败: " + user.getJobNo());
                        String[] cells = new String[5];
                        dataList.forEach(it->{
                            if(it[1].equals(user.getJobNo())){
                                cells[0] = it[0];
                                cells[1] = it[1];
                                cells[2] = it[2];
                                cells[3] = it[3];
                                cells[4] = user.getMsg();
                            }
                        });
                        errorList.add(cells);
                    }
                }
            }
        }
        return jsonObject;
    }

    @Override
    public ServiceResult uploadDeleteAccessRules(MultipartFile file, List<String> deviceIds) {
        // 获取海康设备信息
        List<HkDevice> hkDeviceList = hkDeviceDao.findByDeviceIdIn(deviceIds);
        // 所有设备ip
        List<String> ksIps = new ArrayList<>();
        List<String> ntIps = new ArrayList<>();
        hkDeviceList.forEach(it -> {
            if(HkAddress.KS.equals(it.getAddress())){
                ksIps.add(it.getIp());
            }else {
                ntIps.add(it.getIp());
            }
        });
        ServiceResult result;
        // 读取excel
        try {
            result = excelService.readDeleteAccessRulesExcel(file);
            if (!result.isSuccess() && ErrConst.E23.equals(result.getErrorCode())) {
                return result;
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ServiceResult.error("excel文件内容格式异常");
        }
        // 所有上传的数据行
        List<String[]> dataList = (List<String[]>) result.getObject();
        if(dataList.size() > 800){
            return ServiceResult.error("数据上传过多,请分批次上传,每次最多800人");
        }
        // 数据总数
        int totalCount = dataList.size();
        // 有错误的数据集合
        List<String[]> errorList = getErrorDeleteExcelData(dataList);
        // 所有数据去除错误数据，剩下的就是正常数据，准备写入数据库
        dataList.removeAll(errorList);
        // 获取向海康删除的人员集合
        List<AuthConfigData> data = getDelHkUserList(dataList);
        // 向海康批量新增人员
        UserAddReq userAddReq = new UserAddReq();
        userAddReq.setData(data);
        userAddReq.setOver("1");
        // 判断设备所属地
        JSONObject jsonObjectKs = null;
        JSONObject jsonObjectNt = null;
        try {
            if(!ksIps.isEmpty()){
                userAddReq.setKey(ksIps);
                String requestParam = JSON.toJSONString(userAddReq);
                log.debug("json参数 ： " + requestParam);
                String s = restTemplateUtil.jsonPost(hkDeleteUserKsUrl, requestParam, String.class);
                jsonObjectKs = JSON.parseObject(s);
            }
            if(!ntIps.isEmpty()){
                userAddReq.setKey(ntIps);
                String requestParam = JSON.toJSONString(userAddReq);
                log.debug("json参数 ： " + requestParam);
                String s = restTemplateUtil.jsonPost(hkDeleteUserNtUrl, requestParam, String.class);
                jsonObjectNt = JSON.parseObject(s);
            }
        }catch (Exception e){
            log.error(e.getMessage() ,e);
            return ServiceResult.error("海康人员删除失败");
        }

        boolean ksSuccess = jsonObjectKs != null && "-1".equals(jsonObjectKs.getString("code"));
        boolean ntSuccess = jsonObjectNt != null && "-1".equals(jsonObjectNt.getString("code"));
        if(ksSuccess || ntSuccess){
            return ServiceResult.error("当前正有海康任务执行,请稍后操作");
        }

        String uuid = UUID.randomUUID().toString();
        if (!errorList.isEmpty()) {
            // 将失败数据存入缓存，供用户下载
            String redisKey = redisUtil.createRealKey("AccessRulesImportError-" + uuid);
            redisUtil.set(redisKey, errorList, RedisUtil.ONE_DAY_SECOND);
        }

        return ServiceResult.ok(StaffGroupImportResp.builder()
                .totalCount(totalCount)
                .successCount(data.size())
                .errorCount(errorList.size())
                .errorDataId(uuid)
                .build());
    }

    @Override
    public byte[] getImportErrorDataFileByte(String errorDataId) {
        List<String[]> errorList = new ArrayList<>();
        String redisKey = redisUtil.createRealKey("AccessRulesImportError-" + errorDataId);
        if (!Boolean.TRUE.equals(redisUtil.hasKey(redisKey))) {
            return new byte[0];
        }

        getErrorList(redisKey, errorList);

        return excelService.createUploadAccessRulesErrorExcel(errorList);
    }

    @Override
    public byte[] getDelUserImportErrorDataFileByte(String errorDataId) {
        List<String[]> errorList = new ArrayList<>();
        String redisKey = redisUtil.createRealKey("AccessRulesImportError-" + errorDataId);
        if (!Boolean.TRUE.equals(redisUtil.hasKey(redisKey))) {
            return new byte[0];
        }

        getErrorList(redisKey, errorList);

        return excelService.createDelAccessRulesErrorExcel(errorList);
    }

    @Override
    public ServiceResult hkResult(ImportFailedReq req) {

        HkResultResp resp = new HkResultResp();

        // 获取失败的人员
        List<String[]> errorList = new ArrayList<>();
        String redisKey = redisUtil.createRealKey("AccessRulesImportError-" + req.getErrorDataId());
        if (Boolean.TRUE.equals(redisUtil.hasKey(redisKey))) {
            getErrorList(redisKey, errorList);
        }

        // 获取成功的人员
        List<String[]> dataList = new ArrayList<>();
        String redisSuccessKey = redisUtil.createRealKey("AccessRulesImportSuccess-" + req.getErrorDataId());
        if (Boolean.TRUE.equals(redisUtil.hasKey(redisSuccessKey))) {
            getErrorList(redisSuccessKey, dataList);
        }
        dataList.addAll(errorList);
        // 获取配置通行时间
        String time = "";
        String redisTimeKey = redisUtil.createRealKey("AccessRulesImportTime-" + req.getErrorDataId());
        if(Boolean.TRUE.equals(redisUtil.hasKey(redisTimeKey))){
            time = redisUtil.get(redisTimeKey).toString();
        }
        HkResult ksResult = null;
        HkResult ntResult = null;
        int total = 0;
        int ksPercent = 0;
        int ntPercent = 0;
        if(StringUtils.isNotBlank(req.getErrorKsHkId())){
            String s = restTemplateUtil.jsonPost(hkResultKsUrl, req.getErrorKsHkId(), String.class);
            ksResult = getHkErrorList(s, errorList, time, dataList);
            ksPercent = ksResult.getPercent();
            total += 1;
        }
        if(StringUtils.isNotBlank(req.getErrorNtHkId())){
            String s = restTemplateUtil.jsonPost(hkResultNtUrl, req.getErrorNtHkId(), String.class);
            ntResult = getHkErrorList(s, errorList, time, dataList);
            ntPercent = ntResult.getPercent();
            total += 1;
        }

        if(null == ksResult && ntResult != null){
            resp.setOver(ntResult.getOver());
        }else if(null == ntResult && ksResult != null){
            resp.setOver(ksResult.getOver());
        }else if(null != ksResult && null != ntResult){
            if(1 == ksResult.getOver() && 1 == ntResult.getOver()){
                resp.setOver(1);
            }else {
                resp.setOver(0);
            }
        }else {
            resp.setOver(1);
        }

        if(total == 0 && ksPercent == 0 && ntPercent == 0){
            resp.setPercent(100);
        }else {
            if(total != 0){
                resp.setPercent(((ksPercent + ntPercent) * 100) / (total * 100));
            }

        }

        // 将失败数据存入redis
        if (Boolean.TRUE.equals(redisUtil.hasKey(redisKey))) {
            resp.setErrorDataId(req.getErrorDataId());
            redisUtil.set(redisKey, errorList, RedisUtil.ONE_DAY_SECOND);
        }else {
            String uuid = UUID.randomUUID().toString();
            resp.setErrorDataId(uuid);
            // 将失败数据存入缓存，供用户下载
            redisKey = redisUtil.createRealKey("AccessRulesImportError-" + uuid);
            redisUtil.set(redisKey, errorList, RedisUtil.ONE_DAY_SECOND);
        }


        resp.setErrorCount(errorList.size());
        resp.setTotalCount(req.getTotalCount());
        resp.setSuccessCount(req.getTotalCount() - errorList.size());
        return ServiceResult.ok(resp);
    }

    @Override
    public PageBean<StaffPageResp> findByStaff(String keyword, Pageable pageable, String dept, Integer isStaff) {

        String realKey = redisUtil.createRealKey(STAFFKEY);
        // 判断redis是否存在数据
        if (Boolean.TRUE.equals(redisUtil.hasKey(realKey))) {
            List<StaffPageResp> staffList = JSON.parseArray(String.valueOf(redisUtil.get(realKey)), StaffPageResp.class);

            List<StaffPageResp> collect = staffList;
            // 根据姓名或工号筛选
            if(StringUtils.isNotBlank(keyword)){
                collect = collect.stream().
                        filter(staff -> staff.getEmpNo().indexOf(keyword) > -1 || staff.getName().indexOf(keyword) > -1)
                        .collect(Collectors.toList());
            }

            // 根据部门筛选
            if(StringUtils.isNotBlank(dept)){
                List<String> strings = new ArrayList<>();
                strings.add(dept);
                collect = collect.stream().filter(staff -> strings.contains(staff.getDept())).collect(Collectors.toList());
            }

            // 筛选是否员工或访客
            if(2 != isStaff){
                List<Integer> integers = new ArrayList<>();
                integers.add(isStaff);
                collect = collect.stream().filter(staff -> integers.contains(staff.getIsStaff())).collect(Collectors.toList());
            }
            PageBean<StaffPageResp> data = PageBean.createPageBeanByAllData(collect, pageable.getPageNumber(), pageable.getPageSize());
            data.getList().forEach(it ->{
                try {
                    SysUser sysUser = sysUserServiceUC.getUserByEmpNo(it.getEmpNo());
                    if(null != sysUser){
                        it.setDept(sysUser.getFullDepartmentName());
                    }
                }catch (Exception e){
                    log.error("获取人员信息失败" + e.getMessage());
                }
            });
            return data;
        }

        PageBean<StaffPageResp> staffPage = new PageBean<>();
        staffPage.setList(new ArrayList<>());
        return staffPage;
        /*Page<SysUser> sysUserPage =  sysUserDao.findAll((root, criteriaQuery, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();

            // 是否员工
            if(isStaff != null){
                list.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("isStaff").as(Integer.class), isStaff)));
            }

            // 部门
            if(StringUtils.isNotBlank(dept)){
                list.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("deptId"), dept)));
            }

            // 姓名/工号模糊查
            if(StringUtils.isNotBlank(keyword)){
                String like = "%" + keyword + "%";
                list.add(criteriaBuilder.and(criteriaBuilder.or(
                        criteriaBuilder.like(root.get("empNo"), like),
                        criteriaBuilder.like(root.get("nameZh"), like)
                )));
            }
            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, pageable);


        List<StaffPageResp> staffPageList = new ArrayList<>();
        sysUserPage.getContent().forEach(it ->{
            StaffPageResp staffResp = new StaffPageResp();
            staffResp.setDept(it.getDeptName());
            staffResp.setEmpNo(it.getEmpNo());
            staffResp.setId(it.getId());
            staffResp.setImgUrl(it.getHkServerPhoto());
            staffResp.setName(it.getNameZh());
            staffResp.setIsStaff(it.getIsStaff());
            if(staffResp.getIsStaff() == 1){
                staffResp.setName(staffResp.getName().substring(0, 1) + "*");
                staffResp.setImgUrl("");
                staffResp.setEmpNo("");
            }
            staffPageList.add(staffResp);
        });

        staffPage.setList(staffPageList);
        staffPage.setTotalPages(sysUserPage.getTotalPages());
        staffPage.setTotalElements(sysUserPage.getTotalElements());
        staffPage.setPage(pageable.getPageNumber());
        staffPage.setSize(pageable.getPageSize());
        return staffPage;*/
    }

    @Override
    public void getStaff() {

        List<StaffPageResp> staffList = new ArrayList<>();

        // 向ehr获取所有访客信息
        try {
            log.info("向EHR拉取所有访客信息");
            String data = restTemplateUtil.get(visitorUrl);
            JSONObject jsonData = JSON.parseObject(data);
            JSONArray jsonIndex = jsonData.getJSONArray("list");
            List<EhrVisitorResp> list = JSON.parseArray(jsonIndex.toJSONString(),EhrVisitorResp.class);
            list.forEach(it ->{
                StaffPageResp staff = new StaffPageResp();
                staff.setId(EncryptUtil.md5(it.getJobNum()));
                staff.setEmpNo(it.getJobNum());
                staff.setName(it.getName());
                staff.setMobile(it.getPhone());
                staff.setDept("");
                staff.setIsStaff(1);
                staffList.add(staff);
            });
        }catch (Exception e){
            log.error("向EHR拉取访客信息失败");
            log.error(e.getMessage());
        }

        try {
            // 获取所有员工
            JsonResult<List<PassportRespForFeign>> result = userCenterStaffApi.passportPage();
            if (result.getStatus() != 1) {
                log.error("从UC同步部员工数据失败！！！UC返回信息：" + result.toString());
            } else {
                List<PassportRespForFeign> ucStaffList = result.getData();
                ucStaffList.forEach(it->{
                    StaffPageResp staff = new StaffPageResp();
                    staff.setId(EncryptUtil.md5(it.getJobNo()));
                    staff.setEmpNo(it.getJobNo());
                    staff.setName(it.getRealName());
                    staff.setDept(String.valueOf(it.getDeptId()));
                    staff.setIsStaff(0);
                    staffList.add(staff);
                });
            }
        }catch (Exception e){
            log.error("向UC拉取员工信息失败");
            log.error(e.getMessage());
        }

        // 保存所有人员至redis
        log.info("保存人员信息到redis");
        String realKey = redisUtil.createRealKey(STAFFKEY);
        redisUtil.set(realKey, staffList, 1860L);
    }

    @Override
    public PageBean<HkDevicePageResp> findByHkDevice(String keyword, Pageable pageable, String nodeType, String id) {

        PageBean<HkDevicePageResp> hkDevicePageResp = new PageBean<>();

        Page<HkDevice> hkDevicePage = hkDeviceDao.findAll((root, criteriaQuery, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();

            // 设备状态为在线
            list.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("device").get("hwStatus"), DeviceStatus.ONLINE)));

            // 设备位置 名称 模糊查询
            if(StringUtils.isNotBlank(keyword)){
                String like = "%" + keyword + "%";
                list.add(criteriaBuilder.and(criteriaBuilder.or(
                        criteriaBuilder.like(root.get("device").get("deviceName"), like),
                        criteriaBuilder.like(root.get("device").get("address"), like)
                )));
            }

            // 根据项目或空间获取设备
            if(StringUtils.isNotBlank(id) && StringUtils.isNotBlank(nodeType)){
                if(nodeType.equals("PROJECT")){
                    list.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("device").get("project").get("id"), id)));
                }else {
                    list.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("device").get("space").get("id"), id)));
                }
            }

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, pageable);

        List<HkDevicePageResp> hkDevicePageRespList = new ArrayList<>();
        hkDevicePage.getContent().forEach(it ->{
            HkDevicePageResp devicePageResp = new HkDevicePageResp();
            IotDevice device = it.getDevice();
            devicePageResp.setId(it.getId());
            devicePageResp.setAddress(device.getAddress());
            devicePageResp.setDeviceName(device.getDeviceName());
            devicePageResp.setHwStatus(device.getHwStatus());
            devicePageResp.setSn(device.getSn());
            hkDevicePageRespList.add(devicePageResp);
        });

        hkDevicePageResp.setList(hkDevicePageRespList);
        hkDevicePageResp.setSize(pageable.getPageSize());
        hkDevicePageResp.setPage(pageable.getPageNumber());
        hkDevicePageResp.setTotalElements(hkDevicePage.getTotalElements());
        hkDevicePageResp.setTotalPages(hkDevicePage.getTotalPages());
        return hkDevicePageResp;
    }

    @Override
    @Transactional
    public ServiceResult updateManyUserHkTime(ManyUserTimeReq req) {

        ServiceResult hkResult = hkIpsResult(req.getEndTime(), req.getStartTime(), req.getAll(), req.getDeviceIds());
        if(!hkResult.isSuccess()){
            return ServiceResult.error(hkResult.getObject());
        }

        String realKey = redisUtil.createRealKey(STAFFKEY);
        // 判断redis是否存在数据
        if (Boolean.TRUE.equals(redisUtil.hasKey(realKey))) {
            List<StaffPageResp> staffList = JSON.parseArray(String.valueOf(redisUtil.get(realKey)), StaffPageResp.class);

            // 根据ID获取人员信息
            staffList = staffList.stream().filter(staff -> req.getIds().contains(staff.getId())).collect(Collectors.toList());
            // 向海康发送的人员集合
            List<AuthConfigData> data = new ArrayList<>();
            for(StaffPageResp user : staffList){
                AuthConfigData authConfigData = new AuthConfigData();
                authConfigData.setJobNo(user.getEmpNo());
                authConfigData.setStartTime(req.getStartTime() + ":00+08:00");
                authConfigData.setEndTime(req.getEndTime().toString() + ":59+08:00");
                data.add(authConfigData);
            }

            // 下发失败人员
            List<String> errorList = new ArrayList<>();
            // 向海康批量新增人员
            UserAddReq userAddReq = new UserAddReq();
            userAddReq.setData(data);
            userAddReq.setOver("1");
            HkIpsResp hkIps = (HkIpsResp) hkResult.getObject();
            try {
                JSONObject jsonObjectKs = getHkJSONObject(hkIps.getKsIps(), errorList, userAddReq, hkUserAddMoreKsUrl, staffList);
                if(jsonObjectKs != null && "-1".equals(jsonObjectKs.getString("code"))){
                    return ServiceResult.error("当前正有海康任务进行,请稍后操作");
                }
                JSONObject jsonObjectNt = getHkJSONObject(hkIps.getNtIps(), errorList, userAddReq, hkUserAddMoreNtUrl, staffList);
                if(jsonObjectNt != null && "-1".equals(jsonObjectNt.getString("code"))){
                    return ServiceResult.error("当前正有海康任务进行,请稍后操作");
                }
            }catch (Exception e) {
                log.error(e.getMessage(), e);
                return ServiceResult.error("海康下发人员失败");
            }
            if(!errorList.isEmpty()){
                String errorUser = "";
                for(String user : errorList){
                    errorUser += user + ",";
                }
                return ServiceResult.error("失败人员:" + errorUser.substring(0, errorUser.length()-1), "2");
            }
        }else {
            return ServiceResult.error("当前无可添加人员");
        }
        return ServiceResult.ok();
    }

    private ServiceResult hkIpsResult(LocalDateTime endTime, LocalDateTime startTime, Integer all, List<String> deviceIds) {
        // 比较通行时间
        if(startTime.isAfter(endTime)){
            return ServiceResult.error("请输入正确的通行时间");
        }

        // 根据hk设备id获取ip
        List<HkDevice> hkDeviceList;
        if(all == 0){
            hkDeviceList = hkDeviceDao.findAll();
        }else {
            hkDeviceList = hkDeviceDao.findByIdIn(deviceIds);
        }
        List<String> ksIps = new ArrayList<>();
        List<String> ntIps = new ArrayList<>();
        hkDeviceList.forEach(it -> {
            if(HkAddress.KS.equals(it.getAddress())){
                ksIps.add(it.getIp());
            }else {
                ntIps.add(it.getIp());
            }
        });
        HkIpsResp hkIpsResp = new HkIpsResp();
        hkIpsResp.setKsIps(ksIps);
        hkIpsResp.setNtIps(ntIps);
        return ServiceResult.ok(hkIpsResp);
    }

    @Override
    public ServiceResult updateUserFace(UserFaceReq req) {

        // 校验时间
        ServiceResult hkResult = hkIpsResult(req.getEndTime(), req.getStartTime(), req.getAll(), req.getDeviceIds());
        if(!hkResult.isSuccess()){
            return ServiceResult.error(hkResult.getObject());
        }


        // 下载照片转base64
        String base64 = "";
        /*InputStream in = null;
        byte[] imgData;*/
        FileInputStream input = null;
        try {
            // 下载照片
            HttpHeaders headers = new HttpHeaders();
            HttpEntity<org.springframework.core.io.Resource> httpEntity = new HttpEntity<>(headers);
            ResponseEntity<byte[]> response = restTemplate.exchange(req.getFaceUrl(), HttpMethod.GET, httpEntity, byte[].class);
            byte[] bytes = response.getBody();
            if (bytes == null || bytes.length == 0) {
                log.error("从fastDfs下载APK文件异常！！！下载地址：{}", req.getFaceUrl());
            }
            // 临时文件
            String sourceFileName = UUID.randomUUID().toString() + ".jpg";
            File file = FileUtil.create(sourceFileName, bytes);

            input = new FileInputStream(file);
            MultipartFile multipartFile =new MockMultipartFile("file", file.getName(), "text/plain", IOUtils.toByteArray(input));
            if (multipartFile != null) {
                InputStream inputStream = PicUtils.getPhotoFitHIK(multipartFile);
                base64 = PicUtils.getImgInputStream(inputStream);
            }

            /*in = new FileInputStream(file);
            imgData = new byte[in.available()];
            in.close();
            // 会在本地产生临时文件，用完后需要删除*/
            input.close();
            FileUtil.deleteFiles(file);
        }catch (Exception e){
            log.error(e.getMessage());
            return ServiceResult.error("人脸修改失败");
        } finally {
            if(input != null){
                try {
                    input.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
            /*if(in != null){
                try {
                    in.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }*/
        }
        /*Encoder encoder = Base64.getEncoder();
        String base64 = encoder.encodeToString(imgData);*/

        String realKey = redisUtil.createRealKey(STAFFKEY);
        // 判断redis是否存在数据
        if (Boolean.TRUE.equals(redisUtil.hasKey(realKey))) {
            List<StaffPageResp> staffList = JSON.parseArray(String.valueOf(redisUtil.get(realKey)), StaffPageResp.class);

            // 根据ID获取人员信息
            staffList = staffList.stream().filter(staff -> req.getId().equals(staff.getId())).collect(Collectors.toList());
            if(!staffList.isEmpty()){
                // 向海康发送的人员集合
                StaffPageResp staffPageResp = staffList.get(0);
                List<AuthConfigData> data = new ArrayList<>();
                AuthConfigData authConfigData = new AuthConfigData();
                authConfigData.setJobNo(staffPageResp.getEmpNo());
                authConfigData.setStartTime(req.getStartTime() + ":00+08:00");
                authConfigData.setEndTime(req.getEndTime().toString() + ":59+08:00");
                data.add(authConfigData);

                // 向HK更新人脸
                HkIpsResp hkIps = (HkIpsResp) hkResult.getObject();
                if(!hkIps.getKsIps().isEmpty()){
                    ServiceResult result = faceResult(staffPageResp.getEmpNo(), base64, hkFindPidKsUrl, hkUpdateFaceKsUrl);
                    if(!result.isSuccess()){
                        return ServiceResult.error(result.getObject());
                    }
                }
                if(!hkIps.getNtIps().isEmpty()){
                    ServiceResult result = faceResult(staffPageResp.getEmpNo(), base64, hkFindPidNtUrl, hkUpdateFaceNtUrl);
                    if(!result.isSuccess()){
                        return ServiceResult.error(result.getObject());
                    }
                }

                // 向HK更新指定设备通行时间
                // 下发失败人员
                List<String> errorList = new ArrayList<>();
                // 向海康批量新增人员
                UserAddReq userAddReq = new UserAddReq();
                userAddReq.setData(data);
                userAddReq.setOver("1");
                try {
                    JSONObject jsonObjectKs = getHkJSONObject(hkIps.getKsIps(), errorList, userAddReq, hkUserAddMoreKsUrl, staffList);
                    if(jsonObjectKs != null && "-1".equals(jsonObjectKs.getString("code"))){
                        return ServiceResult.error("当前正有海康任务进行,请稍后操作");
                    }
                    JSONObject jsonObjectNt = getHkJSONObject(hkIps.getNtIps(), errorList, userAddReq, hkUserAddMoreNtUrl, staffList);
                    if(jsonObjectNt != null && "-1".equals(jsonObjectNt.getString("code"))){
                        return ServiceResult.error("当前正有海康任务进行,请稍后操作");
                    }
                }catch (Exception e) {
                    log.error(e.getMessage(), e);
                    return ServiceResult.error("海康下发人员失败");
                }

                if(!errorList.isEmpty()){
                    String errorUser = "";
                    for(String user : errorList){
                        errorUser += user + ",";
                    }
                    return ServiceResult.error("失败人员:" + errorUser.substring(0, errorUser.length()-1), "2");
                }
            }else {
                return ServiceResult.error("人员不存在,ID有误");
            }
        }else {
            return ServiceResult.error("当前无可添加人员");
        }

        return ServiceResult.ok();
    }

    @Override
    public byte[] downloadHkFaceRecord(PageBean<HkFaceRecordPageResp> pageBean) {

        if(null == pageBean.getList() || pageBean.getList().isEmpty()){
            return new byte[0];
        }

        List<String[]> recordList = new ArrayList<>();
        for(int i=0;i<pageBean.getList().size();i++){

            HkFaceRecordPageResp hkRecord = pageBean.getList().get(i);
            String[] cells = new String[6];
            cells[0] = String.valueOf(i);
            cells[1] = hkRecord.getName();
            cells[2] = hkRecord.getJobNum();
            cells[3] = hkRecord.getDept();
            cells[4] = hkRecord.getHappenTime();
            cells[5] = "通行";

            recordList.add(cells);
        }

        return excelService.downloadHkFaceRecord(recordList);
    }

    @Override
    @Transactional
    public ServiceResult updateKs(AccessRulesReq req) {

        // 参数校验
        ServiceResult result = paramConvert(req);
        if (!result.isSuccess()) {
            return result;
        }

        // 验证用户组是否存在
        for (String groupId : req.getStaffGroups()) {
            Optional<StaffGroup> staffGroupOptional = staffGroupDao.findById(groupId);
            if (!staffGroupOptional.isPresent()) {
                return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_STAFF_GROUP, groupId));
            }
            // 向旷世新增人员组
            StaffGroupReq staffGroupReq = new StaffGroupReq();
            StaffGroup staffGroup = staffGroupOptional.get();
            // 确认本人员组在旷世是否存在
            GroupExistReq groupExistReq = new GroupExistReq();
            groupExistReq.setGroupName(staffGroup.getName());
            String exist = staffGroupClient.exist(groupExistReq);
            JSONObject existResult = JSON.parseObject(exist);
            String msg = existResult.getString("msg");
            if(msg.equals("用户组不存在")){
                List<StaffGroupMembers> staffGroupMembers = membersDao.findByGroupId(staffGroup);
                staffGroupReq.setMembersList(staffGroupMembers);
                staffGroupReq.setName(staffGroup.getName());
                staffGroupReq.setStaffGroup(staffGroup);
                log.info("向KS创建人员组");
                ServiceResult result1 = staffGroupService.saveStaffGroup(staffGroupReq);
                if(!result1.isSuccess()){
                    return ServiceResult.error(ServiceMessage.getMessageWithColon(result1.getObject().toString(), groupId));
                }
            }
        }

        // 业务处理成功后，将数据发送给ms-device
        // 构建请求参数
        AccessRuleUpdateReq param = new AccessRuleUpdateReq();
        // 旷世设备id数组，使用旷世云返回的设备id
        AccessRules accessRules = (AccessRules) result.getObject();
        param.setDeviceUuid(accessRules.getDeviceCloud().getKsDeviceId());
        param.setNewGroupUuidList(req.getStaffGroups());
        // 发送请求
        result = msDeviceService.updateAccessRule(param);

        if(!result.isSuccess()){
            // 业务处理失败，手动回滚事务,返回异常
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return result;
        }

        return ServiceResult.ok();
    }

    private ServiceResult faceResult(String empNo, String faceUrl, String hkFindPidUrl, String hkUpdateFaceUrl) {
        try {
            // 向hk获取人员personId
            String s = restTemplateUtil.get(hkFindPidUrl + empNo);
            JSONObject jsonObject = JSON.parseObject(s);
            if("-1".equals(jsonObject.getString("code"))){
                return ServiceResult.error("工号不存在");
            }
            String personId = jsonObject.getString("data");

            // 更新人脸参数
            HkUpdateFaceReq hkUpdateFaceReq = new HkUpdateFaceReq();
            hkUpdateFaceReq.setPersonId(personId);
            hkUpdateFaceReq.setFaceData(faceUrl);
            String requestParam = JSON.toJSONString(hkUpdateFaceReq);
            log.debug("hk更新人脸json参数 ： " + requestParam);
            String faceJson = restTemplateUtil.jsonPost(hkUpdateFaceUrl, requestParam, String.class);
            JSONObject parseObject = JSON.parseObject(faceJson);
            if("0".equals(parseObject.getString("status"))){
                return ServiceResult.error("人脸修改失败");
            }
        }catch (Exception e){
            log.error(e.getMessage());
            log.error("hk人脸更新失败:" + empNo);
            return ServiceResult.error("hk人脸修改失败");
        }
        return ServiceResult.ok();
    }

    private JSONObject getHkJSONObject(List<String> ips, List<String> errorList, UserAddReq userAddReq, String hkUserAddMoreKsUrl, List<StaffPageResp> staffList) {
        JSONObject jsonObject = null;
        if (!ips.isEmpty()) {
            userAddReq.setKey(ips);
            String requestParam = JSON.toJSONString(userAddReq);
            log.debug("json参数 ： " + requestParam);
            String s = restTemplateUtil.jsonPost(hkUserAddMoreKsUrl, requestParam, String.class);
            jsonObject = addHkErrorList(errorList, s, staffList);
        }
        return jsonObject;
    }

    private JSONObject addHkErrorList(List<String> errorList, String s, List<StaffPageResp> staffList) {
        log.debug("添加结果: " + s);
        JSONObject jsonObject = JSON.parseObject(s);
        JSONObject dataJson = jsonObject.getJSONObject("data");
        if (dataJson != null) {
            JSONArray failData = dataJson.getJSONArray("fail_data");
            if (failData != null) {
                List<HkAddUser> list = JSON.parseArray(failData.toJSONString(), HkAddUser.class);
                if (!list.isEmpty()) {
                    for (HkAddUser user : list) {
                        log.error("HK添加人员失败: " + user.getJobNo());
                        staffList.forEach(it -> {
                            if (it.getEmpNo().equals(user.getJobNo())) {
                                    if(it.getIsStaff() == 0){
                                        errorList.add(it.getEmpNo());
                                    }else {
                                        errorList.add(it.getEmpNo().substring(0, 1) + "*");
                                    }
                            }
                        });
                    }
                }
            }
        }
        return jsonObject;
    }


    private HkResult getHkErrorList(String s, List<String[]> errorList, String time, List<String[]> dataList) {
        JSONObject jsonObject = JSON.parseObject(s);
        JSONObject data = jsonObject.getJSONObject("data");
        String[] split = null;
        if(StringUtils.isNotBlank(time)){
            split = time.split(",");
        }

        // 对相同的工号去重
        List<String[]> datas = new ArrayList<>();
        HashMap<String, String> dataMap = new HashMap<>();
        for (String[] user : dataList) {
            String job = user[1];
            String value = dataMap.get(job);
            if (StringUtils.isBlank(value)) {
                dataMap.put(job, job);
                datas.add(user);
            }
        }
        dataList = datas;

        // 获取添加权限失败人员
        JSONArray jsonAuthFail = data.getJSONArray("authFailList");
        List<AuthFail> authFailList = JSON.parseArray(jsonAuthFail.toJSONString(), AuthFail.class);
        if(!authFailList.isEmpty()){
            for(AuthFail authFail : authFailList){
                String[] cells = new String[5];
                SysUser sysUser = sysUserServiceUC.getUserByEmpNo(authFail.getJobNo());
                if(null != sysUser){
                    cells[0] = sysUser.getNameZh();
                }
                cells[1] = authFail.getJobNo();
                cells[2] = time(authFail.getStartTime());
                cells[3] = time(authFail.getEndTime());
                cells[4] = authFail.getMsg();
                errorList.add(cells);
            }
        }

        // 获取设备下发失败人员
        JSONArray jsonDownloadFail = data.getJSONArray("downloadFailDataList");
        List<DownloadFailData> downloadFailDataList = JSON.parseArray(jsonDownloadFail.toJSONString(), DownloadFailData.class);
        if(!downloadFailDataList.isEmpty()){
            for(DownloadFailData failData : downloadFailDataList){
                String[] finalSplit = split;
                failData.getDownloadFailList().forEach(it->{
                    String[] cells = new String[5];
                    SysUser sysUser = sysUserServiceUC.getUserByEmpNo(it.getJobNo());
                    if(null != sysUser){
                        cells[0] = sysUser.getNameZh();
                    }
                    cells[1] = it.getJobNo();
                    if(finalSplit != null){
                        cells[2] = finalSplit[0];
                        cells[3] = finalSplit[1];
                    }
                    cells[4] = it.getMsg();
                    errorList.add(cells);
                });
            }
        }

        // 对相同的工号去重
        List<String[]> strings = new ArrayList<>();
        HashMap<String, String> map = new HashMap<>();
        for (String[] user : errorList) {
            String job = user[1];
            String value = map.get(job);
            if (StringUtils.isBlank(value)) {
                map.put(job, job);
                strings.add(user);
            }
        }
        errorList.clear();
        errorList.addAll(strings);

        strings.clear();
        for(String[] error : errorList){
            for(String[] data1 : dataList){
                if(error[1].equals(data1[1])){
                    strings.add(error);
                    continue;
                }
            }
        }
        errorList.clear();
        errorList.addAll(strings);
        HkResult hkResult = new HkResult();
        hkResult.setOver(data.getInteger("over"));
        hkResult.setPercent(data.getInteger("percent"));
        return hkResult;
    }

    private String time(String time){
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date  date = df.parse(time);
            SimpleDateFormat df1 = new SimpleDateFormat ("EEE MMM dd HH:mm:ss Z yyyy", Locale.UK);
            Date date1 =  df1.parse(date.toString());
            DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return df2.format(date1);
        }catch (Exception e){
            log.error(e.getMessage(), e);
        }
        return time;
    }

    private void getErrorList(String redisKey, List<String[]> errorList) {

        JSONArray jsonArray = JSON.parseArray(redisUtil.get(redisKey).toString());

        jsonArray.stream().forEach(obj -> {
            JSONArray dataArray = JSON.parseArray(obj.toString());
            String[] strings = new String[dataArray.size()];
            for (int i = 0; i < dataArray.size(); i++) {
                strings[i] = dataArray.getString(i);
            }
            errorList.add(strings);
        });
    }

    private List<AuthConfigData> getDelHkUserList(List<String[]> dataList) {
        List<AuthConfigData> dataArrayList = new ArrayList<>();
        for (String[] data : dataList) {

            AuthConfigData authConfigData = new AuthConfigData();
            authConfigData.setJobNo(data[1]);
            dataArrayList.add(authConfigData);

        }
        return dataArrayList;
    }

    private List<String[]> getErrorDeleteExcelData(List<String[]> dataList) {
        List<String[]> errorList = new ArrayList<>();
        for (String[] data : dataList) {
            // 判断工号不能为空
            if(StringUtils.isBlank(data[1])){
                data[4] = "工号不能为空";
                errorList.add(data);
            }

            /*// 判断工号是否存在
            empNo = data[1];
            try {
                SysUser sysUser = sysUserServiceUC.getUserByEmpNo(empNo);
                if (sysUser == null) {
                    data[2] = "未查询到员工信息";
                    errorList.add(data);
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                data[2] = "未查询到员工信息";
                errorList.add(data);
            }*/

        }

        return errorList;
    }

    private List<AuthConfigData> getHkUserList(List<String[]> dataList) {

        List<AuthConfigData> dataArrayList = new ArrayList<>();
        for (String[] data : dataList) {

            AuthConfigData authConfigData = new AuthConfigData();
            authConfigData.setJobNo(data[1]);
            DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime startTime = LocalDateTime.parse(data[2],df);
            LocalDateTime endTime = LocalDateTime.parse(data[3],df);
            authConfigData.setStartTime(startTime + ":00+08:00");
            authConfigData.setEndTime(endTime + ":00+08:00");
            dataArrayList.add(authConfigData);

        }
        return dataArrayList;
    }

    public List<String[]> getErrorExcelData(List<String[]> dataList) {

        List<String[]> errorList = new ArrayList<>();
        for (String[] data : dataList) {

            // 判断工号不能为空
            if(StringUtils.isBlank(data[1])){
                data[4] = "工号不能为空";
                errorList.add(data);
            }

            /*// 判断工号是否存在
            empNo = data[1];
            try {
                SysUser sysUser = sysUserServiceUC.getUserByEmpNo(empNo);
                if (sysUser == null) {
                    data[4] = "未查询到员工信息";
                    errorList.add(data);
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                data[4] = "未查询到员工信息";
                errorList.add(data);
            }*/

            // 验证时间
            if(StringUtils.isBlank(data[2]) || StringUtils.isBlank(data[3])){
                data[4] = "时间格式错误";
                errorList.add(data);
            }else {
                DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime startTime = LocalDateTime.parse(data[2],df);
                LocalDateTime endTime = LocalDateTime.parse(data[3],df);
                if(startTime.isAfter(endTime)){
                    data[4] = "时间格式错误";
                    errorList.add(data);
                }
            }

        }

        return errorList;
    }

    /**
     * 删除同行权限
     *
     * @param id 权限ID
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/3/11 10:02
     **/
    @Override
    @Transactional
    public ServiceResult delete(String id) {

        // 判断设备是否存在
        Optional<AccessRules> accessRulesOptional = accessRulesDao.findById(id);
        if (!accessRulesOptional.isPresent()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_ACCESS_RULES, id));
        }

        AccessRules rules = accessRulesOptional.get();
        // 删除主数据
        rules.setIsDelete(1);
        accessRulesDao.save(rules);

        // 删除关系数据
        Set<AccessRulesToStaffGroup> access2group = accessRulesToStaffGroupDao.findByRuleId(id);

        access2group.forEach(obj -> obj.setIsDelete(1));
        accessRulesToStaffGroupDao.saveAll(access2group);

        // 发送请求
        ServiceResult result = msDeviceService.deleteAccessRule(rules.getDeviceCloud().getKsDeviceId());

        if(!result.isSuccess()){
            // 业务处理失败，手动回滚事务,返回异常
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return result;
        }

        return ServiceResult.ok();
    }

    @Override
    @Transactional
    public ServiceResult open(JSONObject partDaily) {
        log.debug("旷世人员信息： " + partDaily);
        // 刷脸人员
        String personName = partDaily.getString("personName");
        // 刷脸设备
        String snCode = partDaily.getString("snCode");
        // 人员照片
        String personImageUrl = partDaily.getString("personImageUrl");
        // 员工工号
        String personCode = partDaily.getString("personCode");
        if (!personName.isEmpty() && !personImageUrl.isEmpty()) {
            List<AccessRules> arrayList = new ArrayList<>();
            // 根据刷脸人员工号获取所在人员组
            Set<StaffGroupMembers> members = membersDao.findByEmpNo(personCode);
            if (members.isEmpty()) {
                return ServiceResult.error(ServiceMessage.NOT_FOUND_STAFF_GROUP);
            }
            for (StaffGroupMembers member : members) {
                // 跟据人员组获取对应通行权限
                List<AccessRulesToStaffGroup> access2Rule = accessRulesToStaffGroupDao.findByGroupId(member.getGroupId().getId());
                for (AccessRulesToStaffGroup a2r : access2Rule) {
                    // 根据人员组权限映射表获取对应权限
                    Optional<AccessRules> accessRulesOp = accessRulesDao.findById(a2r.getRule().getId());
                    if (accessRulesOp.isPresent()) {
                        arrayList.add(accessRulesOp.get());
                    } else {
                        return ServiceResult.error(ServiceMessage.NOT_FOUND_ACCESS_RULES);
                    }
                }
            }
            // 判断权限所包含的设备是否是扫脸的设备
            if (arrayList.toString().contains(snCode)) {
                List<DeviceCloud> deviceCloudList = deviceCloudDao.findByCloudDeviceId(snCode);
                for( DeviceCloud deviceCloud : deviceCloudList) {
                    ServiceResult result = doorPlateService.openDoor(deviceCloud.getIotDevice().getId());
                    if (!result.isSuccess()) {
                        return ServiceResult.error(result.getObject());
                    }
                }
            } else {
                return ServiceResult.error(ServiceMessage.NOT_FOUND_DEVICE_CLOUD);
            }
        } else {
            return ServiceResult.error(ServiceMessage.NOT_FOUND_ACCESS_RULES);
        }
        return ServiceResult.ok();
    }

    /**
     * 获取通行计划权限详情
     *
     * @param id 权限ID
     * @return 权限详情
     * @author Yang.Lee
     * @date 2021/3/12 16:35
     **/
    @Override
    @Transactional
    public AccessRuleDetailResp get(String id) {

        Optional<AccessRules> accessRulesOptional = accessRulesDao.findById(id);
        if (!accessRulesOptional.isPresent()) {
            return AccessRuleDetailResp.builder().id(id).build();
        }

        return convertDetail(accessRulesOptional.get());
    }

    /**
     * 根据设备ID查询通行权限
     *
     * @param deviceId 设备ID
     * @return 权限详情
     * @author Yang.Lee
     * @date 2021/4/23 9:19
     **/
    @Nullable
    @Override
    @Transactional
    public AccessRuleDetailResp getByDeviceId(String deviceId) {

        Optional<DeviceCloud> ksDeviceOptional = deviceCloudDao.findByIotDeviceId(deviceId);

        if(!ksDeviceOptional.isPresent()){
            return null;
        }
        DeviceCloud deviceCloud = ksDeviceOptional.get();

        Optional<AccessRules> accessRulesOptional = accessRulesDao.findByDeviceCloud(deviceCloud);

        if(!accessRulesOptional.isPresent()){
            return null;
        }

        return convertDetail(accessRulesOptional.get());
    }

    /**
     * 针对AccessRulesReq对象进行参数校验。并将对象转为AccessRules。使用ServiceResult.get()方法获取转换后对象。
     * 校验项目，空间，设备，时间计划，人员组是否存在。不进行其他业务校验。
     *
     * @param req: 请求参数
     * @return 校验结果
     * @author Yang.Lee
     * @date 2021/3/10 9:18
     **/
    @Transactional
    public ServiceResult paramConvert(AccessRulesReq req) {

        AccessRules accessRules = new AccessRules();

        // 验证项目是否存在
        Optional<Project> projectOptional = projectDao.findById(req.getProjectId());
        if (!projectOptional.isPresent()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_PROJECT, req.getProjectId()));
        }
        accessRules.setProject(projectOptional.get());

        // 验证空间是否存在
        String spaceId = req.getSpaceId();
        if (StringUtils.isNotEmpty(spaceId)) {
            Optional<Space> spaceOptional = spaceDao.findById(req.getSpaceId());
            if (!spaceOptional.isPresent()) {
                return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_SPACE, spaceId));
            }
            accessRules.setSpace(spaceOptional.get());
        }

        // 验证设备是否存在
        String deviceCloudId = req.getDistinguishDeviceId();
        Optional<DeviceCloud> deviceOptional = deviceCloudDao.findById(deviceCloudId);
        if (!deviceOptional.isPresent()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_DEVICE_CLOUD, deviceCloudId));
        }
        accessRules.setDeviceCloud(deviceOptional.get());

        // 验证用户组是否存在
        List<String> groupIds = req.getStaffGroups();
        Optional<StaffGroup> staffGroupOptional;
        for (String groupId : groupIds) {
            staffGroupOptional = staffGroupDao.findById(groupId);
            if (!staffGroupOptional.isPresent()) {
                return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_STAFF_GROUP, groupId));
            }
        }

        // 验证时间计划是否存在
        String timePlanId = req.getTimePlanId();
        Optional<TimePlan> timePlanOptional = timePlanDao.findById(timePlanId);
        if (!timePlanOptional.isPresent()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_TIME_PLAN, timePlanId));
        }
        accessRules.setTimePlan(timePlanOptional.get());

        accessRules.setDeviceSource(req.getDeviceSource());

        return ServiceResult.ok(accessRules);
    }

    /**
     * 对象转换
     *
     * @param pageResult 查询结果
     * @param pageable   查询条件
     * @return 目标对象
     * @author Yang.Lee
     * @date 2021/3/10 19:49
     **/
    public PageBean<AccessRulePageResp> convert(Page<AccessRules> pageResult, Pageable pageable) {

        PageBean<AccessRulePageResp> pageBean = new PageBean<>();
        pageBean.setPage(pageable.getPageNumber());
        pageBean.setSize(pageable.getPageSize());
        pageBean.setTotalElements(pageResult.getTotalElements());
        pageBean.setTotalPages(pageResult.getTotalPages());
        pageBean.setList(convert(pageResult.getContent()));

        return pageBean;
    }

    /**
     * 对象转换
     *
     * @param ruleList 待转对象
     * @return 结果对象
     * @author Yang.Lee
     * @date 2021/3/10 20:15
     **/
    public List<AccessRulePageResp> convert(@NotNull List<AccessRules> ruleList) {

        List<AccessRulePageResp> result = new ArrayList<>();
        ruleList.forEach(obj -> result.add(convertPage(obj)));

        return result;
    }

    /**
     * 对象转换
     *
     * @param rule 待转对象
     * @return 结果对象
     * @author Yang.Lee
     * @date 2021/3/10 20:14
     **/
    public AccessRulePageResp convertPage(@NotNull AccessRules rule) {

        AccessRulePageResp result = AccessRulePageResp.builder()
                .deviceAddress(rule.getDeviceCloud().getAddress())
                .id(rule.getId())
                .projectName(rule.getProject().getProjectName())
                .spaceName(rule.getSpace() != null ? SpaceService.getFullSpaceName(rule.getSpace(), rule.getSpace().getName()) : "")
                .deviceName(rule.getDeviceCloud().getName())
                .deviceSource(rule.getDeviceSource().getName())
                .build();

        Set<AccessRulesToStaffGroup> access2group = accessRulesToStaffGroupDao.findByRuleId(rule.getId());

        StringBuilder groupName = new StringBuilder();
        access2group.forEach(obj -> groupName.append(obj.getGroup().getName()).append(","));
        result.setStaffGroups(groupName.substring(0, groupName.length() - 1));

        return result;
    }


    /**
     * 对象转换
     *
     * @param rule 代转对象
     * @return 目标对象
     * @author Yang.Lee
     * @date 2021/3/12 17:00
     **/
    public AccessRuleDetailResp convertDetail(@NotNull AccessRules rule) {

        boolean hasSpace = rule.getSpace() != null;
        boolean hasDeviceSource = rule.getDeviceSource() != null;
        String spaceId = hasSpace ? rule.getSpace().getId() : null;
        String spaceName = hasSpace ? rule.getSpace().getName() : null;
        String deviceSource = hasDeviceSource ? rule.getDeviceSource().toString() : null;
        String deviceSourceName = hasDeviceSource ? rule.getDeviceSource().getName() : null;

        // 根据关系获取人员组数据
        Set<AccessRulesToStaffGroup> accessRulesToStaffGroups = accessRulesToStaffGroupDao.findByRuleId(rule.getId());
        List<StaffGroup> staffGroupList = new ArrayList<>();

        if (!accessRulesToStaffGroups.isEmpty()) {
            accessRulesToStaffGroups.forEach(obj -> staffGroupList.add(obj.getGroup()));
        }

        // 构建输出结果
        return AccessRuleDetailResp.builder()
                .id(rule.getId())
                .projectId(rule.getProject().getId())
                .projectName(rule.getProject().getProjectName())
                .spaceId(spaceId)
                .spaceName(spaceName)
                .distinguishDeviceId(rule.getDeviceCloud().getId())
                .distinguishDeviceName(rule.getDeviceCloud().getName())
                .deviceSource(deviceSource)
                .deviceSourceName(deviceSourceName)
                .staffGroupList(staffGroupService.convert(staffGroupList))
                .timePlanId(rule.getTimePlan().getId())
                .timePlanName(rule.getTimePlan().getName())
                .build();
    }
}
