package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author wei.cheng
 * @date 2022/03/08 15:06
 */
@Data
@ApiModel("门禁点权限申请")
public class AccessControlSpotApplicationRecordReq {

    @ApiModelProperty(value = "门禁点ID")
    @NotBlank
    private String spotId;

    @ApiModelProperty(value = "人脸图片，当门禁类型为面板机时必传")
    private String faceImg;
}
