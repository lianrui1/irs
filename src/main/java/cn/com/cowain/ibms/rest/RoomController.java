package cn.com.cowain.ibms.rest;

import cn.com.cowain.ibms.dao.RoomDao;
import cn.com.cowain.ibms.entity.Room;
import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.IdAndSeqBean;
import cn.com.cowain.ibms.rest.req.RoomReq;
import cn.com.cowain.ibms.rest.req.RoomSortReq;
import cn.com.cowain.ibms.rest.resp.RoomSearchResp;
import cn.com.cowain.ibms.service.RoomService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.ibms.utils.TokenUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/3 21:08
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/room")
@Api(tags = IConst.MODULE_ROOM)
public class RoomController {

    @Autowired
    private RoomDao roomDao;

    @Autowired
    private RoomService roomService;

    @PostMapping("/")
    @ApiOperation(value = "添加会议室", tags = IConst.MODULE_ROOM)
    public ResponseEntity<JsonResult<ServiceResult>> save(@RequestBody @Validated RoomReq roomReq, @RequestHeader("Authorization") String token ,BindingResult bindingResult) {
        log.debug("save...");

        // 保存
        log.debug("添加新会议室即将调用roomService.save");
        ServiceResult service = roomService.save(roomReq);

        JsonResult<ServiceResult> jsonResult = JsonResult.okNoData("OK");
        if (!service.isSuccess()) {
            jsonResult = JsonResult.error("操作失败", service, ErrConst.E01);
        }
        return ResponseEntity.ok(jsonResult);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "更新会议室", tags = IConst.MODULE_ROOM)
    public ResponseEntity<JsonResult<ServiceResult>> update(@PathVariable String id, @RequestBody @Validated RoomReq roomReq, @RequestHeader("Authorization") String token ,
                                                            BindingResult bindingResult) {
        //判断 参数 是否符合要求 （目前：判断openingHours 时间间隔是否大于30min ）
        boolean valid = roomService.valid(roomReq);
        if (!valid) {
            JsonResult<ServiceResult> jsonResult = JsonResult.error("校验错误", null, ErrConst.E01);
            return ResponseEntity.ok(jsonResult);
        }

        // 保存
        log.debug("更新会议室即将调用roomService.update");
        ServiceResult service = roomService.update(id, roomReq);
        JsonResult<ServiceResult> jsonResult = JsonResult.okNoData("OK");
        if (!service.isSuccess()) {
            jsonResult = JsonResult.error("操作失败", service, ErrConst.E01);
        }
        return ResponseEntity.ok(jsonResult);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除会议室", tags = IConst.MODULE_ROOM)
    public ResponseEntity<JsonResult<String>> delete(@PathVariable String id) {
        JsonResult<String> jsonResult = JsonResult.okNoData("OK");

        String token = TokenUtils.getToken();
        ServiceResult result = roomService.delete(id ,token);
        if(!result.isSuccess()){
            jsonResult = JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01) ;
        }

        return ResponseEntity.ok(jsonResult);
    }

    @GetMapping("/search")
    @ApiOperation(value = "查找会议室", tags = IConst.MODULE_ROOM)
    public ResponseEntity<JsonResult<PageBean<RoomSearchResp>>> search(
            @RequestParam(value = "name", required = false, defaultValue = "") String roomName,
            @RequestParam(value = "page", required = false, defaultValue = "0") int page,
            @RequestParam(value = "size", required = false, defaultValue = "10") int size) {

        // 参数：搜索条件, 分页页码,
        // 数据：数据，页码，总页数，
        PageBean<RoomSearchResp> pageBean = roomService.search(roomName, page, size);
        JsonResult<PageBean<RoomSearchResp>> jr = JsonResult.ok("OK", pageBean);
        return ResponseEntity.ok(jr);
    }

    @GetMapping("/search/detail/{id}")
    @ApiOperation(value = "查找会议室", tags = {IConst.MODULE_ROOM, IConst.V25})
    public ResponseEntity<JsonResult<RoomSearchResp>> getDetail(@PathVariable String id) {

        // 参数：搜索条件, 分页页码,
        // 数据：数据，页码，总页数，
        return ResponseEntity.ok(JsonResult.ok("ok", roomService.get(id)));
    }

    @PutMapping("/sort")
    @ApiOperation(value = "更新排序", tags = IConst.MODULE_ROOM)
    public ResponseEntity<JsonResult<String>> update(@RequestBody @Validated RoomSortReq roomSortReq, BindingResult bindingResult) {
        // 创建一个空LIST
        List<Room> roomList = new ArrayList<>();
        // 创建一个list用来装 roomSortReq 传过来的参数
        List<IdAndSeqBean> list = roomSortReq.getData();

        // 计算需要添加的页码排序
        int pageSeq = roomSortReq.getPage()  * roomSortReq.getSize();
        // 循环
        list.forEach(it -> {
            // 获取id
            String id = it.getId();
            // 获取排序
            Integer seq = it.getSeq() + pageSeq;
            // 根据id查询
            Optional<Room> roomOptional = roomDao.findById(id);
            // 获取信息
            Room room = roomOptional.get();
            // 设置排序
            room.setSeq(seq);
            // 添加空列表内
            roomList.add(room);
        });
        // 一次保存mysql
        roomDao.saveAll(roomList);
        JsonResult<String> jsonResult = JsonResult.okNoData("OK");
        return ResponseEntity.ok(jsonResult);
    }

    @GetMapping("/precise/search")
    @ApiOperation(value = "检测名称是否可用", tags = IConst.MODULE_ROOM)
    public ResponseEntity<JsonResult<Integer>> preciseSearchWithName(@RequestParam(value = "name") String roomName,
                                                            @RequestParam(value = "id", required = false, defaultValue = "") String id) {
        JsonResult<Integer> jsonResult = JsonResult.ok("会议名称可用", 1);
        if (!"".equals(id)) {
            // 修改会议室操作
            log.debug("有会议室id,表示为修改操作...");
            // 根据会议室id查询
            Optional<Room> optional = roomDao.findById(id);
            if (optional.isPresent()) {
                // 判断会议室名称是否有改动
                if (!optional.get().getName().equals(roomName)) {
                    log.debug("会议室名称有改动,检查名字可用性...");
                    Optional<Room> optionalRoom = roomDao.findByName(roomName);
                    // 判断修改后的名称是否与其他会议名称冲突
                    if (optionalRoom.isPresent()) {
                        jsonResult = JsonResult.ok("该名称已被使用", 0);
                    }
                } else {
                    log.debug("不知道写啥,先留着");
                }
            }
        } else {
            // 新增会议室操作
            log.debug("没有会议室id,表示为新增操作...");
            Optional<Room> optional = roomDao.findByName(roomName);
            // 判断会议室名字是否与其他会议室名称冲突
            if (optional.isPresent()) {
                jsonResult = JsonResult.ok("该名称已被使用", 0);
            }
        }
        // data 0表示不可用 1表示可用
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 查询可用会议室列表
     *
     * @return
     * @author Yang.Lee
     */
    @GetMapping("/search/list")
    @ApiOperation(value = "查询可用会议室列表(不分页)", tags = IConst.MODULE_ROOM)
    public ResponseEntity<JsonResult<List<RoomSearchResp>>> searchList(@RequestParam(required = false) String projectId) {
        List<RoomSearchResp> result;
        if(StringUtils.isBlank(projectId)){
            result = roomService.searchAvailableList();
        } else{
            result = roomService.getRoomByProjectId(projectId);
        }

        return ResponseEntity.ok(JsonResult.ok("OK", result));
    }

    /**
     * 查询会议室是否可以邀请访客
     *
     * @return
     * @author Yang.Lee
     */
    @GetMapping("/meeting/{roomId}")
    @ApiOperation(value = "查询会议室是否可以邀请访客", tags = IConst.MODULE_ROOM)
    public ResponseEntity<JsonResult<String>> meeting(@PathVariable String roomId) {
        // 查询可用会议室列表
        ServiceResult result = roomService.getMeeting(roomId);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.ok("OK",String.valueOf(result.getObject())));
    }

}
