package cn.com.cowain.ibms.rest.req.space;

import cn.com.cowain.ibms.enumeration.iot.SpaceType;
import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/8 14:05
 */
@Data
@ApiModel("空间列表（分页）请求对象")
public class SpacePageReq extends PageReq {

    @ApiModelProperty(value = "项目ID")
    private String projectId;

    @ApiModelProperty(value = "空间ID")
    private String spaceId;

    @ApiModelProperty(value = "空间类型")
    private SpaceType spaceType;

    @ApiModelProperty(value = "用途类型")
    private String purposeId;

    @ApiModelProperty(value = "空间名称/编号")
    private String keyword;
}
