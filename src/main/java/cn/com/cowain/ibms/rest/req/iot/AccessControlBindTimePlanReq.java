package cn.com.cowain.ibms.rest.req.iot;

import cn.com.cowain.ibms.rest.bean.WorkDay;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author wei.cheng
 * @date 2022/09/28 15:28
 */
@Data
@ApiModel(value = "绑定时间计划请求")
public class AccessControlBindTimePlanReq {

    @ApiModelProperty(value = "门禁点id")
    @NotBlank
    private String spotId;

    @ApiModelProperty(value = "用户工号")
    @NotBlank
    private String userHrId;

    @ApiModelProperty(value = "日程时段")
    @NotEmpty
    @Valid
    private List<WorkDay> workDays;
}
