package cn.com.cowain.ibms.enumeration.iot;

/**
 * @author wei.cheng
 * @date 2022/06/29 10:18
 */
public enum UserAuthStatus {

    APPLICABLE("0", "可申请"),

    UNDER_APPROVAL("1", "审批中"),

    PASSED("2", "审批通过");

    private final String value;

    private final String desc;

    UserAuthStatus(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public String getValue() {
        return this.value;
    }

    public String getDesc() {
        return this.desc;
    }
}
