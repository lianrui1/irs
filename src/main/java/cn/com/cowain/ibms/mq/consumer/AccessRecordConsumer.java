package cn.com.cowain.ibms.mq.consumer;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/22 11:07
 */
@Slf4j
@RocketMQMessageListener(
        topic = "${cowain.ms-device.topic}-ms-device-ks-across-record",
        consumerGroup = "${cowain.ms-device.topic}-record",
        selectorExpression = "*"
)
@Component
public class AccessRecordConsumer implements RocketMQListener<String> {

//    @Resource
//    private AccessRecordHqService accessRecordService;

    @Override
    public void onMessage(@RequestBody String data) {
//        try{
//            // 解析数据
//            JSONObject jsonObject = JSON.parseObject(data);
//            JSONArray jsonIndex = jsonObject.getJSONArray("data");
//            JSONObject accessRecordJson = jsonIndex.getJSONObject(0);
//            accessRecordService.save(accessRecordJson);
//        } catch (Exception e){
//            log.error(e.getMessage(), e);
//        }

    }
}
