package cn.com.cowain.ibms.utils;

import cn.com.cowain.ibms.utils.aliyun.SMSSender;
import com.aliyuncs.exceptions.ClientException;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author Yang.Lee
 * @date 2021/6/29 15:10
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SMSTest {

    @Test
    public void testSend() throws ClientException {

        SMSSender.sendSms("15166018573", "SMS_181196501", "{\"name\":\"Tom\", \"code\":\"" + 123333 + "\"}");
    }
}
