package cn.com.cowain.ibms.service;

import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.entity.ReservationRecordItem;
import cn.com.cowain.ibms.entity.meeting.GuestInvitationRecord;
import cn.com.cowain.ibms.rest.req.InviteeStatusReq;
import cn.com.cowain.ibms.service.bean.WeChatMsg;

import java.util.Set;

/**
 * WeChat 发送消息 Service
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/14/20
 */
public interface WeChatService {

    /**
     * 发送 3 行的微信模板
     *
     * @param url        跳转url
     * @param toUserWxId 目标wxId
     * @param weChatMsg  信息数组/集合
     * @return 消息ID
     */
    String sendMsg(String url, String toUserWxId, WeChatMsg weChatMsg);

    /**
     * 创建会议时 weChat 推送提醒
     *
     * @param ownerId           参会人id
     * @param reservationRecord 会议预约会议信息
     */
    void createMeetingReminders(String ownerId, ReservationRecord reservationRecord);

    /**
     * 拒绝会议时 weChat 推送提醒
     *
     * @param id               会议id
     * @param inviteeStatusReq 参会人信息
     */
    void channelMeetingReminders(String id, InviteeStatusReq inviteeStatusReq);

    /**
     * 更新会议时 weChat 推送提醒
     *
     * @param reservationRecord 会议信息
     * @param pushUserSet       要推送的人员id
     */
    void changeMeetingReminders(ReservationRecord reservationRecord, Set<String> pushUserSet);

    /**
     * 特殊要求提提
     *
     * @param reservationRecord 会议信息
     */
    void specialRequirementsReminders(ReservationRecord reservationRecord);

    /**
     * 会议即将开始提醒
     *
     * @param reservationRecordItem 参会人会议信息
     * @param timeMinute            间隔时间 分钟
     */
    void requirementsComingReminder(ReservationRecordItem reservationRecordItem, Integer timeMinute,String topic);


    /**
     * 会议即将结束提醒
     *
     * @param reservationRecordItem 参会人会议信息
     * @param timeMinute            间隔时间 分钟
     */
    void requirementsComingReminderEnd(ReservationRecordItem reservationRecordItem, Integer timeMinute,String topic);

    /**
     * 取消会提提醒
     * @param reservationRecordItem
     */

    void channelRequirementsReminders(ReservationRecordItem reservationRecordItem,String topic);

    // 访客拒绝会议邀请提醒
    void refuse(GuestInvitationRecord invitationRecord, ReservationRecord reservationRecord);

    // 访客接受会议邀请提醒
    void accept(GuestInvitationRecord record, ReservationRecord reservationRecord);

    // 会议室资源释放
    void refuseAll(ReservationRecord reservationRecord);

    // 设备状态变更消息推送
    void deviceStatusChangeMsgPush(String openId, WeChatMsg weChatMsg);
}
