package cn.com.cowain.ibms.bo;

import cn.com.cowain.ibms.enumeration.ReservationRecordStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author wei.cheng
 * @date 2022/10/10 16:19
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SimpleReservationRecord implements Serializable {
    /**
     * id
     */
    private String id;

    /**
     * 从时间
     */
    private LocalDateTime from;

    /**
     * 到时间
     */
    private LocalDateTime to;

    /**
     * 预定日期
     */
    private LocalDate date;

    /**
     * 会议主题
     */
    private String topic;

    /**
     * 发起人工号
     */
    private String initiatorEmpNo;

    /**
     * 状态
     */
    private ReservationRecordStatus status;

    /**
     * 会议室id
     */
    private String roomId;
}
