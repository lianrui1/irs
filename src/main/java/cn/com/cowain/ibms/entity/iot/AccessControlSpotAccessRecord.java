package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Yang.Lee
 * @date 2021/12/2 19:04
 */
@Data
@Entity
@Table(name = "iot_access_control_spot_access_record")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_access_control_spot_access_record", comment = "门禁点通行记录")
public class AccessControlSpotAccessRecord extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "access_control_spot_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK。门禁点id'")
    private AccessControlSpot accessControlSpot;

    @ManyToOne
    @JoinColumn(name = "device_access_record_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK。设备通行记录Id'")
    private DeviceAccessRecord deviceAccessRecord;
}
