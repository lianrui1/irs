package cn.com.cowain.ibms.enumeration.iot;

public enum DeviceControlStatus {

    SUCCESS("成功"),

    PROCESSING("执行中"),

    FAIL("失败");


    DeviceControlStatus(String name) {
        this.name = name;
    }

    private final String name;

    public String getName() {
        return this.name;
    }
}
