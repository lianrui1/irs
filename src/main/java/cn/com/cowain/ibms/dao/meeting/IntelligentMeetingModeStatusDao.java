package cn.com.cowain.ibms.dao.meeting;

import cn.com.cowain.ibms.entity.meeting.IntelligentMeetingAppModeStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author feng
 * @title: IntelligentMeetingAppDao
 * @projectName ibms
 * @Date 2021/12/29 10:29
 */
@Repository
public interface IntelligentMeetingModeStatusDao extends JpaRepository<IntelligentMeetingAppModeStatus, String> {
    List<IntelligentMeetingAppModeStatus> findByModeId(String modeId);
}
