package cn.com.cowain.ibms.dao;

import cn.com.cowain.ibms.entity.Room;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;
import java.util.Set;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/3 19:37
 */
@SpringBootTest
public class RoomDaoTest2 {

    @Autowired
    private RoomDao roomDao;

    @Test
    public void test0() {
        Assertions.assertNotNull(roomDao);
    }

    @Test
    public void test1() {
        Optional<Room> optionalRoom = roomDao.findByName("检查名称是否重复可用");
        System.out.println(optionalRoom.get());
    }

    @Test
    public void test12() {
        Set<Room> roomSet = roomDao.findByNameLike("%会议室%");
        System.out.println(roomSet);
    }

}