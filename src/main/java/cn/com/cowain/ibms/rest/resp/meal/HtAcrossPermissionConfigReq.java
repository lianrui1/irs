package cn.com.cowain.ibms.rest.resp.meal;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.List;

/**
 * @Author tql
 * @Description 权限配置 接参实体类
 * @Date 21-3-15 下午6:19
 * @Version 1.0
 */
@Data
public class HtAcrossPermissionConfigReq {

    @ApiModelProperty(value = "添加权限配置", required = true, example = "adsas")
    private List<AddConfig> addConfigs;


    @ApiModelProperty(value = "删除权限配置", required = true, example = "adsas")
    private DeleteConfig deleteConfigs;

    /**
     * 新增权限配置
     */
    @Data
    public static class AddConfig {

        /**
         * 时间计划名称
         */
        private String timePlanName;

        /**
         * 业务id
         */
        private String timePlanBizId;

        /**
         * 开始时间
         */
        @DateTimeFormat(pattern = "HH:mm")
        private String startTime;

        /**
         * 结束时间
         */
        @DateTimeFormat(pattern = "HH:mm")
        private String endTime;


        /**
         * 设备ids
         */
        private List<String> deviceIds;


        /**
         * 组配置
         */
        private List<GroupConfig> groupConfigs;
    }

    /**
     * 删除权限配置
     */
    @Data
    public static class DeleteConfig {

        /**
         * 时间计划ids
         */
        private List<String> timePlanBizIds;

        /**
         * 权限组ids
         */
        private List<String> groupBizIds;

    }

    /**
     * 组配置
     */
    @Data
    public static class GroupConfig {

        /**
         * 时间计划名称
         */
        private String groupName;

        /**
         * 业务id
         */
        private String groupBizId;

    }
}
