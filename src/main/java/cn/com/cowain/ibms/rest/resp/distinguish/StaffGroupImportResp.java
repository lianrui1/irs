package cn.com.cowain.ibms.rest.resp.distinguish;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Yang.Lee
 * @date 2021/7/14 13:44
 */
@Data
@Builder
@ApiModel("员工组导入结果输出对象")
@NoArgsConstructor
@AllArgsConstructor
public class StaffGroupImportResp {

    @ApiModelProperty(value = "导入总条数", required = true, position = 1)
    private int totalCount;

    @ApiModelProperty(value = "导入成功条数", required = true, position = 2)
    private int successCount;

    @ApiModelProperty(value = "导入失败条数", required = true, position = 3)
    private int errorCount;

    @ApiModelProperty(value = "导入失败数据ID")
    private String errorDataId;

    @ApiModelProperty(value = "任务id")
    private String taskId;

    private String ksUUID;

    private String ntUUID;
}
