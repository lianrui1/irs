package cn.com.cowain.ibms.enumeration.distinguish;

/**
 * @author wei.cheng
 * @date 2022/03/23 15:21
 */
public enum AuthTimeType {
    /**
     * 工作时间
     */
    WORK_DAY("0"),
    /**
     * 休假时间
     */
    HOLIDAY("1");

    private final String value;

    AuthTimeType(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
