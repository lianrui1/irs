package cn.com.cowain.ibms.rest.req.meeting;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/7 13:45
 */
@Data
@ApiModel("ehr访客接受邀请数据请求对象")
public class EhrAcceptReq {

    // 邀请函单号
    private String v;

    // 姓名
    private String name;

    // 照片url
    private String photoUrl;

    // 手机号
    private String phone;

    // 来访单位
    private String company;

    // 验证码
    private String verificationCode;
}
