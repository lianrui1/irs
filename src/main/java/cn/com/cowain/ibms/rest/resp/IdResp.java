package cn.com.cowain.ibms.rest.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * 数据 ID
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/8/20
 */
@Data
@ApiModel("ID")
@Builder
public class IdResp implements Serializable {

    public IdResp(){}

    public IdResp(String id){
        this.id = id;
    }

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "数据 ID", required = true)
    private String id;

}
