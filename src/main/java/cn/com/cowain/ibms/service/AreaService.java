package cn.com.cowain.ibms.service;

import cn.com.cowain.ibms.rest.resp.AreaResp;

import java.util.List;

public interface AreaService {

    /**
     * 获取省列表
     * @return
     */
    List<AreaResp> searchAreaProvinceList();

    /**
     * 获取市列表
     *
     * @param id
     * @return
     */
    List<AreaResp> searchAreaCityList(Long id);

    /**
     * 获取区列表
     *
     * @param id
     * @return
     */
    List<AreaResp> searchAreaDistrictList(Long id);
}
