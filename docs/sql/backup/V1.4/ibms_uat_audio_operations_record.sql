-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 10.0.1.232    Database: ibms_uat
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `audio_operations_record`
--

DROP TABLE IF EXISTS `audio_operations_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `audio_operations_record` (
  `id` varchar(36) CHARACTER SET utf8mb4 NOT NULL,
  `created_by` varchar(36) CHARACTER SET utf8mb4 DEFAULT NULL,
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(36) CHARACTER SET utf8mb4 DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int(11) DEFAULT NULL COMMENT '版本号',
  `operation_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `operation_time` datetime NOT NULL COMMENT '操作时间',
  `audio_record_id` varchar(36) COLLATE utf8mb4_bin NOT NULL COMMENT 'FK',
  PRIMARY KEY (`id`),
  KEY `FKq7o3lg1vqv63340xw9ngsi1d5` (`audio_record_id`),
  CONSTRAINT `FKq7o3lg1vqv63340xw9ngsi1d5` FOREIGN KEY (`audio_record_id`) REFERENCES `audio_record` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='语音操作记录日志数据表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audio_operations_record`
--

LOCK TABLES `audio_operations_record` WRITE;
/*!40000 ALTER TABLE `audio_operations_record` DISABLE KEYS */;
/*!40000 ALTER TABLE `audio_operations_record` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-26  9:34:27
