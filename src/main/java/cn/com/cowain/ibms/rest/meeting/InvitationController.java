package cn.com.cowain.ibms.rest.meeting;

import cn.com.cowain.ibms.feign.bean.oa.guest.InvitationReason;
import cn.com.cowain.ibms.rest.req.meeting.AccompanyReq;
import cn.com.cowain.ibms.rest.req.meeting.InvitationAcceptReq;
import cn.com.cowain.ibms.rest.req.meeting.InvitationRefuseReq;
import cn.com.cowain.ibms.rest.req.meeting.MobileInvitationReq;
import cn.com.cowain.ibms.rest.resp.meeting.AcceptResp;
import cn.com.cowain.ibms.rest.resp.meeting.FaceResp;
import cn.com.cowain.ibms.rest.resp.meeting.InvitationCardResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.meeting.InvitationCardService;
import cn.com.cowain.ibms.service.meeting.InvitationService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.sfp.comm.utils.JwtUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author Yang.Lee
 * @date 2021/6/25 9:20
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/invitation")
@Api(tags = IConst.MODULE_INVITATION)
public class InvitationController {

    @Autowired
    private InvitationCardService cardService;

    @Resource
    private InvitationService invitationService;

    /**
     * 发送短信邀请
     *
     * @param req           请求参数
     * @param bindingResult 请求校验对象
     * @return 发送结果
     * @author Yang.Lee
     * @date 2021/6/25 9:25
     **/
    @PostMapping
    @ApiOperation(value = "发送短信邀请", tags = IConst.MODULE_INVITATION)
    public ResponseEntity<JsonResult<String>> mobileInvitation(@RequestBody @Validated MobileInvitationReq req, BindingResult bindingResult) {

        return null;
    }

    /**
     * 发送短信验证码
     *
     * @param phone           请求参数
     * @return 发送结果
     * @author Yang.Lee
     * @date 2021/6/25 9:25
     **/
    @GetMapping("/verificationCode")
    @ApiOperation(value = "发送短信验证码", tags = IConst.MODULE_INVITATION)
    public ResponseEntity<JsonResult<String>> verificationCode(String phone) {

        ServiceResult result = cardService.verificationCode(phone);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    /**
     * 人脸照片校验
     *
     * @param file           请求参数
     * @return 发送结果
     * @author Yang.Lee
     * @date 2021/6/25 9:25
     **/
    @PostMapping("/uploadAndCheckFace")
    @ApiOperation(value = "人脸照片校验", tags = IConst.MODULE_INVITATION)
    public ResponseEntity<JsonResult<FaceResp>> uploadAndCheckFace(MultipartFile file) {

        ServiceResult result = cardService.uploadAndCheckFace(file);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.ok((FaceResp)result.getObject()));
    }

    /**
     * 接受短信邀约
     *
     * @param code          邀请卡CODE
     * @param req           请求参数
     * @param bindingResult 请求校验对象
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/6/25 9:36
     **/
    @PutMapping("/{code}/accept")
    @ApiOperation(value = "接受短信邀请", tags = IConst.MODULE_INVITATION)
    public ResponseEntity<JsonResult<AcceptResp>> accept(@PathVariable String code, @RequestBody @Validated InvitationAcceptReq req, BindingResult bindingResult) {

        ServiceResult result = cardService.accept(code ,req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.ok((AcceptResp)result.getObject()));
    }

    /**
     * 拒绝短信邀约
     *
     * @param code          邀请卡CODE
     * @param req           请求参数
     * @param bindingResult 请求校验对象
     * @return 处理结果
     **/
    @PutMapping("/{code}/refuse")
    @ApiOperation(value = "拒绝短信邀约", tags = IConst.MODULE_INVITATION)
    public ResponseEntity<JsonResult<String>> refuse(@PathVariable String code, @RequestBody @Validated InvitationRefuseReq req, BindingResult bindingResult) {

        ServiceResult result = cardService.refuse(code ,req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }


    /**
     * 获取邀请卡详情
     *
     * @param code 邀请卡code
     * @return 邀请卡详情
     * @author Yang.Lee
     * @date 2021/6/25 9:44
     **/
    @GetMapping("/{code}")
    @ApiOperation(value = "访客邀请卡查询", tags = IConst.MODULE_INVITATION)
    public ResponseEntity<JsonResult<InvitationCardResp>> getDetail(@PathVariable String code ,String recordCode) {

        ServiceResult result = cardService.getDetail(code ,recordCode);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.ok((InvitationCardResp)result.getObject()));
    }


    @GetMapping("/list/reason")
    @ApiOperation(value = "邀请事由列表", tags = IConst.MODULE_INVITATION)
    public ResponseEntity<JsonResult<List<InvitationReason>>> getInvitationReasonList(@RequestHeader String token, @RequestHeader("Authorization") String authorization){

        String empNo = JwtUtil.getHrId(authorization);
        return ResponseEntity.ok(JsonResult.ok(invitationService.getInvitationReason(token, empNo)));
    }

    /**
     * 访客状态查询
     *
     * @param visitorRecordNo 邀请卡单号
     * @param jobNum           访客工号
     * @return 邀请卡详情
     * @author Yang.Lee
     * @date 2021/6/25 9:44
     **/
    @GetMapping("/visitor/status")
    @ApiOperation(value = "访客状态查询", tags = IConst.MODULE_INVITATION)
    public ResponseEntity<JsonResult<InvitationCardResp>> visitorStatus(String visitorRecordNo ,String jobNum, String recordCode) {

        ServiceResult result = cardService.visitorStatus(visitorRecordNo ,jobNum ,recordCode);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.ok((InvitationCardResp)result.getObject()));
    }

    /**
     * 访客随行
     *
     * @return 邀请卡详情
     * @author Yang.Lee
     * @date 2021/6/25 9:44
     **/
    @PostMapping("/accompany")
    @ApiOperation(value = "访客随行", tags = IConst.MODULE_INVITATION)
    public ResponseEntity<JsonResult<String>> accompany(@RequestBody AccompanyReq req , BindingResult bindingResult) {

        ServiceResult result = cardService.accompany(req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }
}
