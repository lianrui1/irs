package cn.com.cowain.ibms.dao.office;

import cn.com.cowain.ibms.entity.office.IntelligentOffice;
import cn.com.cowain.ibms.enumeration.iot.IntelligentOfficeStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface IntelligentOfficeDao extends JpaRepository<IntelligentOffice, String>, JpaSpecificationExecutor<IntelligentOffice> {

    // 根据名称获取办公室信息
    Optional<IntelligentOffice> findByName(String name);

    // 根据空间获取智能办公室
    Optional<IntelligentOffice> findBySpaceId(String spaceId);

    Page<IntelligentOffice> findBySpaceIdIn(List<String> spaceIds, Pageable pageable);

    List<IntelligentOffice> findAllBySpaceIdIn(List<String> spaceIds);

    // 根据空间ID和办公室状态获取办公室
    Page<IntelligentOffice> findBySpaceIdInAndStatus(List<String> spaceIds, IntelligentOfficeStatus status, Pageable pageable);

    List<IntelligentOffice> findBySpaceIdInAndStatus(Collection<String> spaceIds, IntelligentOfficeStatus status);
}

