package cn.com.cowain.ibms.rest.resp.iot;

import cn.com.cowain.ibms.entity.iot.AccessControlSpotApplicationRecord;
import cn.com.cowain.ibms.enumeration.ApprovalStatus;
import cn.com.cowain.ibms.enumeration.iot.AccessPermissionDistributionStatus;
import cn.com.cowain.ibms.rest.resp.StaffResp;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 申请|分配记录返回参数
 *
 * @author: yanzy
 * @date: 2022/3/8 13:59
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("申请|分配记录分页列表响应")
public class AccessControlSpotApplicationRecordResp {

    @ApiModelProperty(value = "记录id", required = true, position = 1)
    private String id;

    @ApiModelProperty(value = "申请人工号", required = true, position = 2)
    private String applicantHrId;

    @ApiModelProperty(value = "申请人姓名", required = true, position = 3)
    private String applicantName;

    @ApiModelProperty(value = "申请人部门名称", position = 4)
    private String applicantDepartmentFullName;

    @ApiModelProperty(value = "人脸图片", position = 5)
    private String faceImg;

    @ApiModelProperty(value = "人员类型，员工;劳务", required = true, position = 6)
    private String personnelType;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "申请时间", required = true, position = 7)
    private LocalDateTime createdTime;

    @ApiModelProperty(value = "门禁点名称", required = true, position = 8)
    private String spotName;

    @ApiModelProperty(value = "门禁点类型", required = true, position = 9)
    private String accessControlSpotType;

    @ApiModelProperty(value = "门禁点位置", required = true, position = 10)
    private String spotAddress;

    @ApiModelProperty(value = "分配人工号", position = 11)
    private String approverHrId;

    @ApiModelProperty(value = "分配人姓名", position = 12)
    private String approverName;

    @ApiModelProperty(value = "分配人部门名称", position = 13)
    private String approverDepartmentFullName;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "分配通行开始时间", position = 14)
    private LocalDateTime accessStartTime;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "分配通行结束时间", position = 15)
    private LocalDateTime accessEndTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "分配时间", position = 16)
    private LocalDateTime approverTime;

    @ApiModelProperty(value = "分配状态", required = true, position = 17)
    private String status;

    @ApiModelProperty(value = "分配状态类型，可选值：`PENDING`审批中，`APPROVED`已批准，`DENIED`已驳回，`CANCEL`已取消", required = true, position = 18)
    private ApprovalStatus statusType;

    @ApiModelProperty(value = "门禁权限下发状态，可选值：`DOING`执行中，`SUCCESS`成功，`FAILED`失败", required = true, position = 19)
    private AccessPermissionDistributionStatus accessPermissionDistributionStatus;

    @ApiModelProperty(value = "拒绝原因", position = 20)
    private String deniedReason;

    @ApiModelProperty(value = "分配人列表", position = 21)
    private List<StaffResp> approverList;

    /**
     * 对象转换
     *
     * @author: yanzy
     * @data: 2022/3/9 11:03:38
     */
    public static AccessControlSpotApplicationRecordResp convert(AccessControlSpotApplicationRecord accessControlSpotApplicationRecord) {

        String staff = "";
        if ("CWA".equals(accessControlSpotApplicationRecord.getApplicantHrId().substring(0, 3))) {
            staff = "员工";
        }
        if ("CWS".equals(accessControlSpotApplicationRecord.getApplicantHrId().substring(0, 3))) {
            staff = "劳务";
        }

        return AccessControlSpotApplicationRecordResp.builder()
                //记录id
                .id(accessControlSpotApplicationRecord.getId())
                //申请人姓名
                .applicantName(accessControlSpotApplicationRecord.getApplicantName())
                //申请人工号
                .applicantHrId(accessControlSpotApplicationRecord.getApplicantHrId())
                //申请人部门名称
                .applicantDepartmentFullName(accessControlSpotApplicationRecord.getApplicantDepartmentFullName())
                //人脸照片
                .faceImg(accessControlSpotApplicationRecord.getFaceImg())
                //人员类型
                .personnelType(staff)
                //申请时间
                .createdTime(accessControlSpotApplicationRecord.getCreatedTime())
                //门禁点名称
                .spotName(accessControlSpotApplicationRecord.getSpotName())
                //门禁点类型
                .accessControlSpotType(accessControlSpotApplicationRecord.getAccessControlSpotType().getName())
                //门禁点位置
                .spotAddress(accessControlSpotApplicationRecord.getSpotAddress())
                //分配人工号
                .approverHrId(accessControlSpotApplicationRecord.getApproverHrId())
                //分配人姓名
                .approverName(accessControlSpotApplicationRecord.getApproverName())
                //分配人部门名称
                .approverDepartmentFullName(accessControlSpotApplicationRecord.getApproverDepartmentFullName())
                //分配通行开始时间
                .accessStartTime(accessControlSpotApplicationRecord.getAccessStartTime())
                //分配通行结束时间
                .accessEndTime(accessControlSpotApplicationRecord.getAccessEndTime())
                //分配时间
                .approverTime(accessControlSpotApplicationRecord.getApproverTime())
                //分配状态
                .status(accessControlSpotApplicationRecord.getStatus().getName())
                //分配状态类型
                .statusType(accessControlSpotApplicationRecord.getStatus())
                //拒绝理由
                .deniedReason(accessControlSpotApplicationRecord.getDeniedReason())
                //门禁点权限下发状态
                .accessPermissionDistributionStatus(accessControlSpotApplicationRecord.getAccessPermissionDistributionStatus())
                .build();
    }
}
