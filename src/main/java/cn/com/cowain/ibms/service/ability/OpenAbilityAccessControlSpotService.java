package cn.com.cowain.ibms.service.ability;

import cn.com.cowain.ibms.rest.resp.space.ProjectSpaceTreeResp;

import java.util.List;

/**
 * OpenAbilityAccessControlSpotService
 *
 * @author: yanzy
 * @date: 2022/01/14/16:56
 * @projectName ibms
 */
public interface OpenAbilityAccessControlSpotService {

    /**
     * 获取能力下绑定的门禁点树
     *
     * @param openAbilityId 开发能力id
     * @author yanzy
     * @date 2022-01-14
     */
    List<ProjectSpaceTreeResp> getHasDeviceTree(String openAbilityId);

    /**
     * 获取能力下未绑定的门禁点树
     *
     * @param openAbilityId 开发能力id
     * @author yanzy
     * @date 2022-01-18
     */
    List<ProjectSpaceTreeResp> getNoBind(String openAbilityId);
}
