package cn.com.cowain.ibms.config;

import cn.com.cowain.ibms.component.DdlTool;
import org.fluttercode.datafactory.impl.DataFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManager;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/7/30 16:29
 */
@Configuration
public class ToolConfig {

    @Value("${spring.datasource.driver-class-name}")
    String driver;
    @Value("${spring.datasource.url}")
    String url;
    @Value("${spring.datasource.username}")
    String username;
    @Value("${spring.datasource.password}")
    String password;

    @Bean
    /**
     *  @Profile({"dev","test","local"})
     * */
    public DdlTool ddlTool(EntityManager entityManager) {
        return new DdlTool(driver, url, username, password, entityManager);
    }

    @Bean
    /**
     *  @Profile({"dev","test","local"})
     * */
    public DataFactory dataFactory() {
        return new DataFactory();
    }

}
