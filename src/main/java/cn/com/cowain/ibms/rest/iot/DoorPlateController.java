package cn.com.cowain.ibms.rest.iot;

import cn.com.cowain.ibms.rest.req.iot.DoorPlatePCUpdateReq;
import cn.com.cowain.ibms.rest.resp.iot.doorplate.DoorPlateStatusDetailResp;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.iot.DoorPlateReq;
import cn.com.cowain.ibms.rest.req.iot.DoorPlateSortReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.iot.DoorOpenResp;
import cn.com.cowain.ibms.rest.resp.iot.doorplate.DoorPlateSearchResp;
import cn.com.cowain.ibms.rest.resp.iot.doorplate.DoorPlateStatusResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.iot.DoorPlateService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import java.time.LocalDate;
import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author: cuiEnming
 * @title: DoorPlaterController
 * @date 2020/11/24  14:57
 * @since s2
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/doorplate")
@Api(tags = IConst.DOOR_PLATE)
public class DoorPlateController {


    private DoorPlateService doorPlateService;

    public DoorPlateController(DoorPlateService doorPlateService) {
        this.doorPlateService = doorPlateService;
    }

    @PostMapping
    @ApiOperation(value = "APP新增门牌", tags = IConst.DOOR_PLATE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "门牌名称"),
            @ApiImplicitParam(name = "roomId", value = "会议室id"),
            @ApiImplicitParam(name = "doorMagne", value = "门磁"),
    })
    public ResponseEntity<JsonResult<IdResp>> save(@RequestBody @Validated DoorPlateReq doorPlateReq, BindingResult bindingResult) {

        ServiceResult service = doorPlateService.save(doorPlateReq);
        JsonResult<IdResp> jsonResult;
        if (!service.isSuccess()) {
            jsonResult = JsonResult.error(String.valueOf(service.getObject()), null, ErrConst.E01);
            return ResponseEntity.ok(jsonResult);
        }
        jsonResult = JsonResult.ok("ok", (IdResp) service.getObject());

        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 更新门牌信息
     *
     * @param id           门牌ID
     * @param doorPlateReq 门牌信息
     * @return
     * @author Yang.Lee
     */
    @PutMapping("/{id}")
    @ApiOperation(value = "APP更新门牌信息", tags = IConst.DOOR_PLATE)
    public ResponseEntity<JsonResult<Object>> update(@PathVariable String id, @RequestBody @Validated DoorPlateReq doorPlateReq,
                                                     BindingResult bindingResult) {

        ServiceResult result = doorPlateService.update(id, doorPlateReq, false);
        JsonResult<Object> jsonResult = JsonResult.okNoData("OK");
        if (!result.isSuccess()) {
            jsonResult = JsonResult.error(String.valueOf(result.getObject()), result, ErrConst.E01);
        }

        return ResponseEntity.ok(jsonResult);
    }

    /**
     * PC更新门牌信息
     *
     * @param id:            门牌ID
     * @param doorPlateReq:  门牌信息
     * @param bindingResult: 参数校验对象
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/3/8 15:09
     **/
    @PutMapping("/{id}/pc")
    @ApiOperation(value = "PC更新门牌信息", tags = IConst.DOOR_PLATE)
    public ResponseEntity<JsonResult<Object>> updatePC(@PathVariable String id, @RequestBody @Validated DoorPlatePCUpdateReq doorPlateReq,
                                                       BindingResult bindingResult) {
        DoorPlateReq req = new DoorPlateReq();
        req.setDoorMagne(doorPlateReq.getDoorMagne());
        req.setName(doorPlateReq.getName());
        req.setPassword(doorPlateReq.getPassword());
        req.setRoomId(doorPlateReq.getRoomId());

        ServiceResult result = doorPlateService.update(id, req, true);
        JsonResult<Object> jsonResult = JsonResult.okNoData("OK");
        if (!result.isSuccess()) {
            jsonResult = JsonResult.error(String.valueOf(result.getObject()), result, ErrConst.E01);
        }

        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 查询门牌状态
     *
     * @param id
     * @return
     * @author Yang.Lee
     */
    @GetMapping("/{id}/status")
    @ApiOperation(value = "APP查询门牌状态", tags = IConst.DOOR_PLATE)
    public ResponseEntity<JsonResult<DoorPlateStatusResp>> searchStatus(@PathVariable String id) {

        ServiceResult result = doorPlateService.status(id);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.ok("ok", (DoorPlateStatusResp) result.getObject()));
    }

    /**
     * 根据硬件ID查询门牌数据
     *
     * @param hardwareId 硬件ID
     * @return 门牌信息
     * @author Yang.Lee
     * @date 2021/3/23 10:34
     **/
    @GetMapping("/hardware/{hardwareId}/status")
    @ApiOperation(value = "APP查询门牌状态(根据硬件id)", tags = IConst.DOOR_PLATE)
    public ResponseEntity<JsonResult<DoorPlateStatusResp>> searchStatusByHardwareId(@PathVariable String hardwareId) {

        ServiceResult result = doorPlateService.statusByHardwareId(hardwareId);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.ok("ok", (DoorPlateStatusResp) result.getObject()));
    }

    /**
     * 查询门牌分页数据
     *
     * @param doorPlateName 模糊查询关键字
     * @param page          分页页码
     * @param size          分页页长
     * @return
     */
    @GetMapping("/list")
    @ApiOperation(value = "PC查询门牌分页数据", tags = IConst.DOOR_PLATE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "门牌名称模糊查询关键字"),
            @ApiImplicitParam(name = "page", value = "查询页码"),
            @ApiImplicitParam(name = "size", value = "查询页长"),
    })
    public ResponseEntity<JsonResult<PageBean<DoorPlateSearchResp>>> search(
            @RequestParam(value = "name", defaultValue = "") String doorPlateName,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size) {

        PageBean<DoorPlateSearchResp> resultPageBean = doorPlateService.search(doorPlateName, page, size);
        JsonResult<PageBean<DoorPlateSearchResp>> jr = JsonResult.ok("OK", resultPageBean);
        return ResponseEntity.ok(jr);


    }

    /**
     * 排序
     *
     * @param doorPlateSortReq
     * @param bindingResult
     * @return
     * @author Yang.Lee
     */
    @PutMapping("/sort")
    @ApiOperation(value = "PC更新排序", tags = IConst.DOOR_PLATE)
    public ResponseEntity<JsonResult<Object>> updateSort(@RequestBody @Validated DoorPlateSortReq doorPlateSortReq, BindingResult bindingResult) {

        ServiceResult result = doorPlateService.sort(doorPlateSortReq);

        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error("操作失败", null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    /**
     * 删除门牌
     *
     * @param id 门牌ID
     * @return
     * @author Yang.Lee
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "PC删除门牌", tags = IConst.DOOR_PLATE)
    public ResponseEntity<JsonResult<Object>> delete(@PathVariable String id) {

        ServiceResult result = doorPlateService.delete(id);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    /**
     * 判断密码是否有效
     *
     * @param id       设备ID
     * @param password 设备密码
     * @return
     */
    @PostMapping("/password/{id}")
    @ApiOperation(value = "APP判断设备密码是否有效", tags = IConst.DOOR_PLATE)
    public ResponseEntity<JsonResult<ServiceResult>> checkPassword(@PathVariable String id, @RequestParam String password) {

        ServiceResult result = doorPlateService.checkDevicePassword(id, password);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.ok("ok", result));
    }


    /**
     * 更新所有门牌的门磁密码
     *
     * @param id
     * @param openPassword
     * @return
     */
    @PutMapping("/allOpenPassword")
    @ApiOperation(value = "PC更新所有门牌的门磁密码", tags = IConst.DOOR_PLATE)
    public ResponseEntity<JsonResult<String>> updateAllOpenPassword(@PathVariable String id, @ApiParam(value = "新的门磁密码", required = true) @RequestParam String openPassword) {

        return ResponseEntity.ok(JsonResult.ok("ok", null));
    }

    /**
     * 查询门牌状态
     *
     * @param id
     * @return
     * @author Yang.Lee
     */
    @GetMapping("/{id}/status/pc")
    @ApiOperation(value = "PC查询门牌状态", tags = IConst.DOOR_PLATE)
    public ResponseEntity<JsonResult<DoorPlateStatusResp>> searchStatusPC(@PathVariable String id) {

        ServiceResult result = doorPlateService.statusPC(id);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.ok("ok", (DoorPlateStatusResp) result.getObject()));
    }

    /**
     * 更新所有门牌密码
     */
    @PutMapping
    @ApiOperation(value = "PC更新门牌密码", tags = IConst.DOOR_PLATE)
    public ResponseEntity<JsonResult<Object>> updatePassword(@RequestParam String password) {

        String regex = "^[0-9A-Za-z]{6,12}$";
        if (password.matches(regex)) {
            ServiceResult result = doorPlateService.updatePassword(password);
            JsonResult<Object> jsonResult = JsonResult.okNoData("OK");
            if (!result.isSuccess()) {
                jsonResult = JsonResult.error(String.valueOf(result.getObject()), result, ErrConst.E01);
            }
            return ResponseEntity.ok(jsonResult);
        } else {
            JsonResult<Object> jsonResult = null;
            jsonResult = JsonResult.error("输入密码有误", null, ErrConst.E01);
            return ResponseEntity.ok(jsonResult);
        }
    }

    /**
     * 输入密码开门
     */
    @PostMapping("/openDoor")
    @ApiOperation(value = "输入密码开门", tags = IConst.DOOR_PLATE)
    public ResponseEntity<JsonResult<Object>> open(@RequestBody @Validated DoorOpenResp doorOpenResp, BindingResult bindingResult) {

        String regex = "^[0-9A-Za-z]{6,12}$";
        if (doorOpenResp.getPassword().matches(regex)) {
            ServiceResult result = doorPlateService.open(doorOpenResp);
            JsonResult<Object> jsonResult = JsonResult.okNoData("OK");
            if (!result.isSuccess()) {
                jsonResult = JsonResult.error(String.valueOf(result.getObject()), result, ErrConst.E01);
            }
            return ResponseEntity.ok(jsonResult);
        } else {
            JsonResult<Object> jsonResult = null;
            jsonResult = JsonResult.error("输入密码有误", null, ErrConst.E01);
            return ResponseEntity.ok(jsonResult);
        }
    }

    /**
     * 人脸开门
     */
    @PostMapping("/tql/open")
    public ResponseEntity<JsonResult<Object>> open(@RequestBody String data) {
        log.info("旷视云回调数据" + data);
        JsonResult<Object> jsonResult = JsonResult.okNoData("OK");
        // 解析数据
        JSONObject jsonObject = JSON.parseObject(data);
        JSONArray jsonIndex = jsonObject.getJSONArray("data");
        JSONObject partDaily = jsonIndex.getJSONObject(0);
        // 刷脸人员
        String personName = partDaily.getString("personName");
        // 刷脸设备
        String snCode = partDaily.getString("snCode");
        String personImageUrl = partDaily.getString("personImageUrl");
        String device = "M014200492008000050";
        if (!personName.isEmpty() && !personImageUrl.isEmpty() && snCode.equals(device)) {
            String id = "63661fe3-7a44-473a-b211-33b27760ae8a";
            ServiceResult result = doorPlateService.openDoor(id);
            if (!result.isSuccess()) {
                jsonResult = JsonResult.error("数据校验错误", result, ErrConst.E01);
            }
        } else {
            jsonResult = JsonResult.error("数据校验错误", null, ErrConst.E01);
        }
        return ResponseEntity.ok(jsonResult);
    }


    /**
     * 查询会议列表
     *
     * @param roomId 会议室ID
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/4/21 16:27
     **/
    @GetMapping("/search/list/status")
    @ApiOperation(value = "获取会议列表", tags = IConst.DOOR_PLATE)
    public ResponseEntity<JsonResult<List<DoorPlateStatusDetailResp>>> getRoomReservationRecordList(
            @RequestParam @ApiParam(value = "会议室ID") String roomId) {

        List<DoorPlateStatusDetailResp> result = doorPlateService.getStatusDetailList(roomId, LocalDate.now());

        return ResponseEntity.ok(JsonResult.ok("OK", result));
    }
}
