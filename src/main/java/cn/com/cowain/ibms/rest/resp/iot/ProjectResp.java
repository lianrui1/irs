package cn.com.cowain.ibms.rest.resp.iot;

import cn.com.cowain.ibms.rest.resp.space.SpaceFloorResp;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/3/29 15:29
 */
@Data
public class ProjectResp {

    @ApiModelProperty(value = "项目ID")
    private String projectId;

    @ApiModelProperty(value = "项目名称")
    private String projectName;

    @ApiModelProperty(value = "子空间信息")
    private List<SpaceFloorResp> spaceList;
}
