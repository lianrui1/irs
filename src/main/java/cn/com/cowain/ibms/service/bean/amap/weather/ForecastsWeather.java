package cn.com.cowain.ibms.service.bean.amap.weather;

import cn.com.cowain.ibms.service.bean.amap.AMapResp;
import lombok.Data;

import java.util.List;

/**
 * @author feng
 * @title: ForecastsWeather
 * @projectName ibms
 * @Date 2022/5/5 18:27
 */
@Data
public class ForecastsWeather  extends AMapResp {
    List<ForceCastsTempResult> forecasts;

}
