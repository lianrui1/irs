package cn.com.cowain.ibms.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.lang.NonNull;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;

/**
 * 文件操作通用工具类
 *
 * @author Yang.Lee
 * @date 2021/1/5 17:10
 */
@Slf4j
public class FileUtil {

    private FileUtil() {
    }

    /**
     * 将MultipartFile对象转为File对象
     *
     * @param source
     * @param target
     * @return
     * @throws Exception
     */
    public static File multipartFileToFile(MultipartFile source, File target) throws IOException {


        if (source == null || source.getSize() <= 0) {
            throw new NullPointerException("MultipartFile大小为0或名称为null");
        }
        String localFileName = source.getOriginalFilename();
        if (localFileName == null || localFileName.length() == 0) {
            throw new NullPointerException("MultipartFile大小为0或名称为null的文件名称为空");
        }

        InputStream ins = null;
        try {
            ins = source.getInputStream();
            inputStreamToFile(ins, target);
        } catch (Exception e) {
            log.error("multipartFileToFile 失败！！！！！", e);
        } finally {
            if (ins != null) {
                ins.close();
            }
        }

        return target;
    }

    /**
     * 将输入流转为文件
     *
     * @param ins
     * @param file
     * @throws IOException
     */
    public static void inputStreamToFile(InputStream ins, File file) throws IOException {
        OutputStream os = null;
        try {
            os = new FileOutputStream(file);
            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
                os.write(buffer, 0, bytesRead);
            }

        } catch (Exception e) {
            log.error("inputStreamToFile 失败！！！！！", e);
        } finally {
            if (os != null) {
                os.close();
            }
            if (ins != null) {
                ins.close();
            }
        }
    }

    /**
     * 将字符串写入文件
     *
     * @param context
     * @param targetFile
     * @throws IOException
     */
    public static void stringToFile(String context, File targetFile) throws IOException {
        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(targetFile);
            outputStream.write(context.getBytes(StandardCharsets.UTF_8));
        } catch (Exception e) {
            log.error("StringToFile 失败！！！！", e);
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }

    /**
     * 将byte[]写入文件
     *
     * @param context
     * @param targetFile
     * @throws IOException
     */
    public static void byteToFile(byte[] context, File targetFile) throws IOException {
        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(targetFile);
            outputStream.write(context);
        } catch (Exception e) {
            log.error("StringToFile 失败！！！！", e);
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }

    /**
     * 一行一行写入文件
     *
     * @param contentList 内容列表
     * @param targetFile  目标文件
     * @author Yang.Lee
     * @date 2022/3/10 16:19
     **/
    public static void writeToFileLineByLine(List<String> contentList, File targetFile) throws IOException {

        try (FileOutputStream out = new FileOutputStream(targetFile);
             OutputStreamWriter outWriter = new OutputStreamWriter(out, StandardCharsets.UTF_8);
             BufferedWriter bufWrite = new BufferedWriter(outWriter)) {

            for (String content : contentList) {
                bufWrite.write(content + "\r\n");
            }


        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * 删除文件
     *
     * @param files
     * @throws IOException
     */
    public static void deleteFiles(@NonNull File... files) throws IOException {

        for (File file : files) {
            if (file != null && file.exists()) {
                Files.delete(file.toPath());
            }
        }
    }

    /**
     * 获取文件扩展名
     *
     * @param fileName
     * @return
     */
    public static String getExt(String fileName) {

        if (fileName == null || fileName.trim().length() == 0) {
            throw new NullPointerException("文件名称为空");
        }

        int index = fileName.lastIndexOf(".");
        if (index < 0) {
            throw new NullPointerException("文件没有扩展名");
        } else {
            return fileName.substring(index + 1);
        }
    }

    /**
     * 创建文件
     *
     * @param filePath 文件地址
     * @param bytes    二进制数据
     * @return 文件对象
     */
    public static File create(String filePath, byte[] bytes) {

        File file = new File(filePath);

        try (InputStream in = new ByteArrayInputStream(bytes);
             FileOutputStream fos = new FileOutputStream(file)) {

            int len = 0;
            byte[] buf = new byte[1024];
            while ((len = in.read(buf)) != -1) {
                fos.write(buf, 0, len);
            }
            fos.flush();

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return file;
    }

    /**
     * 从项目resources目录下读取文件
     *
     * @param fileRelativePath 文件相对路径
     * @return File对象
     * @author Yang.Lee
     * @date 2021/4/26 15:40
     **/
    public static InputStream readFromResources(String fileRelativePath) throws IOException {

        ClassPathResource resource = new ClassPathResource(fileRelativePath);
        return resource.getInputStream();
    }
}
