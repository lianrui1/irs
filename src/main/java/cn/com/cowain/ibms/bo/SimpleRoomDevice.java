package cn.com.cowain.ibms.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author wei.cheng
 * @date 2022/10/10 14:53
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SimpleRoomDevice implements Serializable {
    /**
     * 会议室id
     */
    private String roomId;
    /**
     * 名称
     */
    private String name;
}
