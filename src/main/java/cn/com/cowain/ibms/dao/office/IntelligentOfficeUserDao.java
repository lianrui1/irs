package cn.com.cowain.ibms.dao.office;

import cn.com.cowain.ibms.bo.SimpleIntelligentOfficeUser;
import cn.com.cowain.ibms.entity.office.IntelligentOfficeUser;
import cn.com.cowain.ibms.enumeration.iot.IntelligentOfficeStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IntelligentOfficeUserDao extends JpaRepository<IntelligentOfficeUser, String>, JpaSpecificationExecutor<IntelligentOfficeUser> {


    // 根据会议室ID和人员工号获取用户权限信息
    Optional<IntelligentOfficeUser> findByIntelligentOfficeIdAndUserHrId(String officeId, String hrId);

    /**
     * 获取简单的数据
     *
     * @param officeId
     * @param hrId
     * @return
     */
    @Query(
            value = "select new cn.com.cowain.ibms.bo.SimpleIntelligentOfficeUser(invitedUserHrId) from IntelligentOfficeUser where intelligentOffice.id = ?1 and userHrId = ?2 and isDelete = 0"
    )
    SimpleIntelligentOfficeUser findSimpleByIntelligentOfficeIdAndUserHrId(String officeId, String hrId);

    List<IntelligentOfficeUser> findByUserHrId(String userHrId);

    /**
     * 根据员工id和办公室状态查询
     *
     * @param userHrId 用户工号
     * @param status   办公室状态
     * @return 有权限的办公室列表
     * @author Yang.Lee
     * @date 2021/12/20 18:24
     **/
    List<IntelligentOfficeUser> findByUserHrIdAndIntelligentOfficeStatus(String userHrId, IntelligentOfficeStatus status);

    // 根据办公室ID获取人员信息 倒叙排序
    List<IntelligentOfficeUser> findByIntelligentOfficeIdOrderByCreatedTimeDesc(String id);

    // 获取办公室下权限人员
    List<IntelligentOfficeUser> findByIntelligentOfficeId(String id);

    /**
     * 根据空间id查询
     *
     * @param spaceId 空间ID
     * @return 权限关系
     * @author Yang.Lee
     * @date 2022/1/6 15:23
     **/
    List<IntelligentOfficeUser> findByIntelligentOfficeSpaceId(String spaceId);
    // 根据会议室ID和人员工号获取用户权限信息
    List<IntelligentOfficeUser> findByIntelligentOfficeIdAndInvitedUserHrId(String officeId, String invitedUserHrId);
}
