package cn.com.cowain.ibms.utils;

import lombok.experimental.UtilityClass;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * @author Yang.Lee
 * @date 2021/4/25 13:26
 */
@UtilityClass
public class TokenUtils {

    /**
     * 获取token
     *
     * @return 请求头中的Authorization值
     * @author Yang.Lee
     * @date 2021/4/25 11:01
     **/
    public String getToken() {

        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        assert requestAttributes != null;

        String token = requestAttributes.getRequest().getHeader("Authorization");
        assert token != null;

        return token;
    }
}
