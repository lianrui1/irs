package cn.com.cowain.ibms.rest.resp.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/1/25 8:57
 */
@Data
@ApiModel("门牌密码开门")
public class DoorOpenResp {

    /**
     * 门牌ID
     */
    @NotBlank(message = "门牌ID不能为空")
    @ApiModelProperty(value = "门牌ID", required = true, position = 1)
    private String id;

    @NotNull
    @ApiModelProperty(value = "门牌密码", position = 3)
    private String password;
}
