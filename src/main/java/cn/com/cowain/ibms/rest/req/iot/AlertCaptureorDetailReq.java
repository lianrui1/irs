package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 * @author: yanzy
 * @date: 2022/4/15 14:42
 */
@Data
@ApiModel("查看详情请求参数")
public class AlertCaptureorDetailReq {

    @ApiModelProperty(value = "监控点id",required = true,position = 1)
    private String id;

    @ApiModelProperty(value = "是否设为已读",position = 2)
    private String isView;
}
