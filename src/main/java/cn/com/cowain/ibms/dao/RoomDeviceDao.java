package cn.com.cowain.ibms.dao;

import cn.com.cowain.ibms.bo.SimpleRoomDevice;
import cn.com.cowain.ibms.entity.RoomDevice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * Room Dao
 *
 * @author 张宇鑫
 * @version 1.0
 * @date 2020/8/3 19:36
 */
@Repository
public interface RoomDeviceDao extends JpaRepository<RoomDevice, String> {

    /**
     * 根据room_id 删除
     *
     * @param id
     * @return
     */
    void deleteByRoomId(String id);

    /**
     * 根据room_id 查找
     *
     * @param id
     * @return
     */
    Set<RoomDevice> findByRoomId(String id);

    /**
     * 根据 room_id 查找,按 name 排序
     *
     * @param roomId
     * @return
     */
    List<RoomDevice> findByRoomIdOrderByName(String roomId);

    /**
     * 查询会议室的设备列表
     *
     * @param roomIds
     * @return
     */
    @Query(
            value = "select new cn.com.cowain.ibms.bo.SimpleRoomDevice(room.id, name) from RoomDevice where room.id in (:roomIds) and isDelete = 0"
    )
    List<SimpleRoomDevice> findSimpleRoomDeviceByRoomIds(@Param("roomIds") List<String> roomIds);
}
