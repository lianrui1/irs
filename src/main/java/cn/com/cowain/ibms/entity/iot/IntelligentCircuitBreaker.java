package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * 智能断路器
 *
 * @author Yang.Lee
 * @date 2021/4/28 21:24
 */
@Data
@Entity
@Table(name = "iot_intelligent_circuit_breaker")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_intelligent_circuit_breaker", comment = "智能断路器数据表")
public class IntelligentCircuitBreaker extends BaseEntity {

    /**
     * iot 设备
     */
    @ManyToOne
    @JoinColumn(name = "device_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 设备ID'")
    private IotDevice device;

    /**
     * 用电量
     **/
    @Column(name = "electricity", nullable = false, columnDefinition = "decimal(12, 2) COMMENT '用电量'")
    private Double electricity;

    /**
     * 用电时间
     **/
    @Column(name = "data_time", nullable = false, columnDefinition = "datetime COMMENT '数据时间'")
    private LocalDate dataTime;
}
