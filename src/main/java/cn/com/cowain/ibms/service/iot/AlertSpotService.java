package cn.com.cowain.ibms.service.iot;

import cn.com.cowain.ibms.rest.req.iot.AlertSpotCreateReq;
import cn.com.cowain.ibms.rest.resp.iot.AlertSpotResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;

import java.util.List;

/**
 * 警戒点Service
 *
 * @author: yanzy
 * @date: 2022/4/13 10:53
 */
public interface AlertSpotService {

    /**
     * 查询所有警戒点
     *
     * @author: yanzy
     * @param: [spaceId]
     * @data: 2022/4/13 10:04:20
     */
    List<AlertSpotResp> getAlertSpot();

    /**
     * 创建警戒点
     *
     * @param req
     * @author wei.cheng
     * @date 2022/4/15 3:57 下午
     **/
    ServiceResult createAlertSpot(AlertSpotCreateReq req);
}
