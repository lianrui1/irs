package cn.com.cowain.ibms.feign.mqtt_connector.nt;

import cn.com.cowain.sfp.comm.JsonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Yang.Lee
 * @date 2021/6/9 21:50
 */
@Service
@RequestMapping({"/mqttConnector/api/v1/nt/magnetic"})
@FeignClient(value = "ms-mqtt-connector-nt", url = "${cowain.gateway.address}")
public interface MagneticNTApi{

    /**
     * 开门
     *
     * @param macAddress 门磁mac地址
     * @return 开门结果
     * @author Yang.Lee
     * @date 2021/5/5 15:46
     **/
    @PostMapping("/{macAddress}/open")
    JsonResult<String> open(@PathVariable String macAddress);
}
