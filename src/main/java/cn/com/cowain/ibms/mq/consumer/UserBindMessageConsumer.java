package cn.com.cowain.ibms.mq.consumer;

import cn.com.cowain.ibms.mq.Topic;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;


/**
 * 人员绑定返回topic
 *
 * @author: yanzy
 * @date: 2022/4/7 13:52
 */
@Slf4j
@RocketMQMessageListener(
        topic = "${rocketmq.env}-" + Topic.USER_BIND_RETURN,
        consumerGroup = "${rocketmq.env}-" + Topic.USER_BIND_RETURN
)
@Component
public class UserBindMessageConsumer implements RocketMQListener<String> {

//    @Resource
//    private AbilityService abilityService;


    @Override
    public void onMessage(@RequestBody String data) {

//        try{
//            // 解析数据
//            UserBindMessage userBindMessage = JSON.parseObject(data, UserBindMessage.class);
//            log.debug("mq返回结果{}",userBindMessage);
//
//            //修改人员导入状态
//            abilityService.changeUserImportStatus(userBindMessage);
//
//        } catch (Exception e){
//            log.error(e.getMessage(), e);
//        }
    }
}
