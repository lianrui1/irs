package cn.com.cowain.ibms.dao.distinguish;

import cn.com.cowain.ibms.entity.distinguish.AccessRulesToStaffGroup;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Set;

/**
 * 通行权限 - 用户组关系DAO对象
 *
 * @author Yang.Lee
 * @date 2021/3/10 10:44
 */
public interface AccessRulesToStaffGroupDao extends JpaRepository<AccessRulesToStaffGroup, String> {

    /**
     * 根据权限ID查询权限-人员组关系列表
     *
     * @param ruleId 权限ID
     * @return 权限-人员组关系列表
     * @author Yang.Lee
     * @date 2021/3/10 15:00
     **/
    Set<AccessRulesToStaffGroup> findByRuleId(String ruleId);

    /**
     * 根据权限ID查询权限-人员组关系列表
     *
     * @param groupId 人员组ID
     * @return 权限-人员组关系列表
     * @author Yang.Lee
     * @date 2021/3/10 15:00
     **/
    List<AccessRulesToStaffGroup> findByGroupId(String groupId);
}
