package cn.com.cowain.ibms.rest.resp.space;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/3/28 13:40
 */
@Data
public class SpaceResp {

    @ApiModelProperty(value = "空间信息")
    private List<SpaceFloorResp> spaceList;
}
