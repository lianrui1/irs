package cn.com.cowain.ibms.rest.resp.office;

import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.office.IntelligentOfficeDevice;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.rest.req.office.DeviceNameEditReq;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;

/**
 * 可控智能设备返回参数
 *
 * @author: yanzy
 * @date: 2022/1/27 14:37
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ControlledDeviceResp {

    //设备id
    @ApiModelProperty(value = "设备id", required = true)
    private String deviceId;

    //设备名称
    @ApiModelProperty(value = "设备类型名称", required = true)
    private String deviceName;

    //设备类型
    @ApiModelProperty(value = "设备类型", required = true)
    private DeviceType deviceType;

    //设备类型名称
    @ApiModelProperty(value = "设备类型名称", required = true)
    private String deviceTypeName;

    //创建时间
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createdTime;

    @ApiModelProperty(value = "附加子名称", required = true)
    private DeviceNameEditReq.AdditionalName additionalName;

    public static ControlledDeviceResp convert(IotDevice device, IntelligentOfficeDevice officeDevice) {
        ControlledDeviceResp resp = new ControlledDeviceResp();
        resp.setDeviceId(device.getId());
        resp.setDeviceType(device.getDeviceType());
        resp.setDeviceTypeName(device.getDeviceType().getName());
        resp.setCreatedTime(device.getCreatedTime());
        if (StringUtils.isNotBlank(officeDevice.getDeviceAlisa())){
            resp.setDeviceName(StringUtils.isEmpty(officeDevice.getDeviceAlisa()) ? device.getDeviceName()
                    : officeDevice.getDeviceAlisa());
        }else {
            resp.setDeviceName(officeDevice.getDevice().getDeviceName());
        }
        if (StringUtils.isNotBlank(officeDevice.getDeviceAlisaProperties())){
            resp.setAdditionalName(StringUtils.isEmpty(officeDevice.getDeviceAlisaProperties())
                    ? DeviceNameEditReq.AdditionalName.empty()
                    : JSON.parseObject(officeDevice.getDeviceAlisaProperties(), DeviceNameEditReq.AdditionalName.class));
        }
        return resp;
    }

}
