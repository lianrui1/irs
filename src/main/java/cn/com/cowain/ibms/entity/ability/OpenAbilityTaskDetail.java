package cn.com.cowain.ibms.entity.ability;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.enumeration.UserImportStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author Yang.Lee
 * @date 2022/1/10 17:02
 */
@Data
@Entity
@Table(name = "ibms_open_ability_task_detail")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_open_ability_task_detail", comment = "IBMS 开放能力任务详情")
@Where(clause = "is_del=0")
public class OpenAbilityTaskDetail extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "task_id", nullable = false, columnDefinition = "varchar(36) COMMENT '任务ID'")
    private OpenAbilityTask task;

    @Column(name = "user_hr_id", nullable = false, columnDefinition = "varchar(20) COMMENT '用户工号'")
    private String userHrId;

    @Column(name = "user_name", nullable = false, columnDefinition = "varchar(20) COMMENT '用户姓名'")
    private String userName;


    @ManyToOne
    @JoinColumn(name = "device_id", nullable = false, columnDefinition = "varchar(36) COMMENT '设备ID'")
    private IotDevice device;

    @Enumerated(EnumType.STRING)
    @Column(name = "user_import_status",columnDefinition = "varchar(50) COMMENT '人员导入状态'")
    private UserImportStatus userImportStatus;

    @Column(name = "fail_msg",columnDefinition = "varchar(50) COMMENT '人员失败原因'")
    private String failMsg;

    @Column(name = "result", columnDefinition = "text COMMENT '结果'")
    private String result;

    @Column(name = "status", columnDefinition = "varchar(200) COMMENT '结果状态'")
    private String status;

}
