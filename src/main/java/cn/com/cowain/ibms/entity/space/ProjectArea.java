package cn.com.cowain.ibms.entity.space;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 项目区域
 *
 * @author Yang.Lee
 * @date 2021/11/7 16:18
 */
@Data
@Entity
@Table(name = "ibms_project_area")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_project_area", comment = "项目区域表")
@Where(clause = "is_del=0")
public class ProjectArea extends BaseEntity {

    @Column(name = "name", nullable = false, columnDefinition = "varchar(10)  COMMENT '区域名称'")
    private String name;
}
