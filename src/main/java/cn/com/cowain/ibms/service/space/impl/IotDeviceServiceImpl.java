package cn.com.cowain.ibms.service.space.impl;

import cn.com.cowain.ibms.dao.SkillPersonDao;
import cn.com.cowain.ibms.dao.SysUserDao;
import cn.com.cowain.ibms.dao.ability.OpenAbilityAccessControlSpotDao;
import cn.com.cowain.ibms.dao.device.CircuitBreakerDao;
import cn.com.cowain.ibms.dao.device.DeviceControlLogDao;
import cn.com.cowain.ibms.dao.device.DeviceModeStatusDao;
import cn.com.cowain.ibms.dao.distinguish.DeviceCloudDao;
import cn.com.cowain.ibms.dao.distinguish.HkDeviceDao;
import cn.com.cowain.ibms.dao.iot.*;
import cn.com.cowain.ibms.dao.iotc.ApplicationDao;
import cn.com.cowain.ibms.dao.iotc.DeviceApplicationRelationDao;
import cn.com.cowain.ibms.dao.office.IntelligentOfficeDao;
import cn.com.cowain.ibms.dao.office.IntelligentOfficeDeviceDao;
import cn.com.cowain.ibms.dao.office.IntelligentOfficeRuleEngineDao;
import cn.com.cowain.ibms.dao.office.IntelligentOfficeUserDao;
import cn.com.cowain.ibms.dao.space.IotProductDao;
import cn.com.cowain.ibms.dao.space.ProjectDao;
import cn.com.cowain.ibms.dao.space.SpaceAdminDao;
import cn.com.cowain.ibms.dao.space.SpaceDeviceControlDao;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.ability.OpenAbilityAccessControlSpot;
import cn.com.cowain.ibms.entity.distinguish.DeviceCloud;
import cn.com.cowain.ibms.entity.iot.*;
import cn.com.cowain.ibms.entity.iotc.Application;
import cn.com.cowain.ibms.entity.iotc.DeviceApplicationRelation;
import cn.com.cowain.ibms.entity.office.*;
import cn.com.cowain.ibms.entity.space.IotProduct;
import cn.com.cowain.ibms.entity.space.Project;
import cn.com.cowain.ibms.entity.space.SpaceAdmin;
import cn.com.cowain.ibms.entity.space.SpaceDeviceControl;
import cn.com.cowain.ibms.enumeration.HkAddress;
import cn.com.cowain.ibms.enumeration.ability.Ability;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.enumeration.iot.*;
import cn.com.cowain.ibms.feign.bean.iotc.resp.DeviceResp;
import cn.com.cowain.ibms.feign.bean.iotc.resp.DeviceStatusChangeListResp;
import cn.com.cowain.ibms.feign.iotc.IOTCApi;
import cn.com.cowain.ibms.feign.iotc.bean.Telemetry;
import cn.com.cowain.ibms.feign.iotc.enumeration.MethodCode;
import cn.com.cowain.ibms.feign.iotc.enumeration.ModuleCode;
import cn.com.cowain.ibms.mq.bean.DeviceStatusChangeMsgPushMessage;
import cn.com.cowain.ibms.mq.producer.ErrorIotDeviceMessageProducer;
import cn.com.cowain.ibms.mq.producer.IotDeviceMessageProducer;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.IdAndSeqBean;
import cn.com.cowain.ibms.rest.req.Sorts;
import cn.com.cowain.ibms.rest.req.device.DeviceControlLogReq;
import cn.com.cowain.ibms.rest.req.iot.GroupBindingReq;
import cn.com.cowain.ibms.rest.req.iot.HIKFaceMachinePageReq;
import cn.com.cowain.ibms.rest.req.office.*;
import cn.com.cowain.ibms.rest.req.space.DevicePageReq;
import cn.com.cowain.ibms.rest.req.space.IotDeviceCreateReq;
import cn.com.cowain.ibms.rest.req.space.IotDeviceSortReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.StaffResp;
import cn.com.cowain.ibms.rest.resp.device.*;
import cn.com.cowain.ibms.rest.resp.distinguish.AccessRuleDetailResp;
import cn.com.cowain.ibms.rest.resp.iot.HIKAccessRulePageResp;
import cn.com.cowain.ibms.rest.resp.iot.IotDeviceNetworkChangeResp;
import cn.com.cowain.ibms.rest.resp.iot.IotDeviceResp;
import cn.com.cowain.ibms.rest.resp.office.RuleEngIneDetailActuatorResp;
import cn.com.cowain.ibms.rest.resp.office.RuleEngIneDetailTriggerResp;
import cn.com.cowain.ibms.rest.resp.qrcode.QrcodeAccessRuleDetailResp;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.WeChatService;
import cn.com.cowain.ibms.service.bean.*;
import cn.com.cowain.ibms.service.device.HwProductsService;
import cn.com.cowain.ibms.service.disthinguish.AccessRulesService;
import cn.com.cowain.ibms.service.iot.IotDeviceCacheService;
import cn.com.cowain.ibms.service.iotc.ApplicationService;
import cn.com.cowain.ibms.service.office.IntelligentOfficeRuleEngineService;
import cn.com.cowain.ibms.service.office.SpaceCacheService;
import cn.com.cowain.ibms.service.qrcode.QrcodeAccessRuleService;
import cn.com.cowain.ibms.service.space.IotDeviceService;
import cn.com.cowain.ibms.service.space.SpaceService;
import cn.com.cowain.ibms.utils.*;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.sfp.comm.utils.JwtUtil;
import cn.com.cowain.usercenter.common.req.PassportRoleEditReq;
import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Nullable;
import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Description
 *
 * @author yuchunlei
 * @version 1.0.0
 * @createTime 2020年12月22日 09:50:00
 */
@Slf4j
@Service
public class IotDeviceServiceImpl implements IotDeviceService {

    @Autowired
    private DeviceDao iotDeviceDao;

    @Autowired
    private IotProductDao iotProductDao;

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private SpaceDao spaceDao;

    @Resource
    private QrcodeAccessRuleService qrcodeAccessRuleService;

    @Resource
    private AccessRulesService accessRulesService;

    @Resource
    private IotDeviceMessageProducer iotDeviceMessageProducer;

    @Resource(name = "errorIotDeviceMessageProducer")
    private ErrorIotDeviceMessageProducer errorIotDeviceMessageProducer;

    @Autowired
    private HwProductsService productsService;

    @Resource
    private DeviceApplicationRelationDao deviceApplicationRelationDao;

    @Resource
    private ApplicationDao applicationDao;

    @Resource
    private ApplicationService applicationService;

    @Resource
    private IOTCApi iotcApi;

    @Resource
    private DoorMagneticDao doorMagneticDao;

    @Resource
    private CurtainDao curtainDao;

    @Resource
    private AirConditionerDao airConditionerDao;

    @Resource
    private CircuitBreakerDao circuitBreakerDao;

    @Resource
    private HkDeviceDao hkDeviceDao;

    @Resource
    private DeviceCloudDao deviceCloudDao;

    @Resource
    private SpaceDeviceControlDao spaceDeviceControlDao;

    @Resource
    private SysUserDao sysUserDao;

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserService;

    @Autowired
    private DeviceModeStatusDao modeStatusDao;

    @Resource
    private SpaceAdminDao spaceAdminDao;

    @Resource
    private SpaceService spaceService;

    @Resource
    private IntelligentOfficeDao intelligentOfficeDao;

    @Resource
    private AccessControlSpotDao accessControlSpotDao;

    @Resource
    private YSVideoCameraDao ysVideoCameraDao;

    @Resource
    private IntelligentOfficeDeviceDao intelligentOfficeDeviceDao;

    @Resource
    private IntelligentOfficeUserDao intelligentOfficeUserDao;

    @Autowired
    private SkillPersonDao skillPersonDao;

    @Autowired
    private WeChatService weChatService;

    @Resource
    private AlertCaptureorDao alertCaptureorDao;

    @Resource
    private AlertSpotDao alertSpotDao;

    @Resource
    private DeviceAccessRecordDao deviceAccessRecordDao;

    @Resource
    private DeviceControlLogDao deviceControlLogDao;

    @Resource
    private DeviceGroupItemDao deviceGroupItemDao;

    @Resource
    private DeviceLinkRelationDao deviceLinkRelationDao;

    @Resource
    private DeviceToSpaceDao deviceToSpaceDao;

    @Resource
    private DoorPlateDao doorPlateDao;

    @Resource
    private IlluminationSensingDao illuminationSensingDao;

    @Resource
    private IntelligentCircuitBreakerDao intelligentCircuitBreakerDao;

    @Resource
    private IotDeviceNetworkChangeDao iotDeviceNetworkChangeDao;

    @Resource
    private VideoMonitorSpotDao videoMonitorSpotDao;

    @Resource
    private OpenAbilityAccessControlSpotDao openAbilityAccessControlSpotDao;

    @Resource
    private ElevatorSpotDeviceRelationDao elevatorSpotDeviceRelationDao;

    @Value("${cowain.tsdb.url}")
    private String tsdbUrl;

    @Resource
    private RestTemplate restTemplate;
    @Resource
    private IntelligentOfficeRuleEngineDao intelligentOfficeRuleEngineDao;

    @Resource
    private IntelligentOfficeRuleEngineService intelligentOfficeRuleEngineService;

    @Autowired
    IotDeviceCacheService iotDeviceCacheService;

    @Autowired
    SpaceCacheService spaceCacheService;

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    DeviceDao deviceDao;

    public static void updateSpaceNum(IotDevice device, SpaceDao spaceDao) {
        if (!device.getSpace().getId().isEmpty()) {
            Optional<Space> optionalSpace = spaceDao.findById(device.getSpace().getId());
            if (optionalSpace.isPresent()) {
                Space space = optionalSpace.get();
                space.setDeviceNum(space.getDeviceNum() - 1);
                spaceDao.save(space);
            }
        }
    }

    @Override
    @Transactional
    public ServiceResult create(IotDeviceCreateReq dto) {
        IotDevice iotDevice = new IotDevice();
        //设备名称
        iotDevice.setDeviceName(dto.getDeviceName());

        if (Objects.nonNull(dto.getProductId())) {
            //查询产品
            Optional<IotProduct> iot = iotProductDao.findById(dto.getProductId());
            if (iot.isPresent()) {
                IotProduct iotProduct = iot.get();
                //所属产品
                iotDevice.setIotProduct(iotProduct);
            } else {
                return ServiceResult.error("未找到产品信息");
            }
        }


        Optional<Project> projectOptional = projectDao.findById(dto.getProjectId());
        if (!projectOptional.isPresent()) {
            return ServiceResult.error("未找到项目信息");
        }

        Project project = projectOptional.get();
        //所属项目
        iotDevice.setProject(project);

        // 如果已选择空间信息则验证空间数据
        if (dto.getSpaceId() != null && dto.getSpaceId().length() > 0) {
            Optional<Space> spaceOptional = spaceDao.findById(dto.getSpaceId());
            if (!spaceOptional.isPresent()) {
                return ServiceResult.error("未找到空间信息");
            }
            Space space = spaceOptional.get();
            // 新增空间设备数
            space.setDeviceNum(space.getDeviceNum() + 1);

            if (space.getProject() == null || !dto.getProjectId().equals(space.getProject().getId())) {
                return ServiceResult.error("空间数据与项目数据不匹配");
            }
            spaceDao.save(space);
            iotDevice.setSpace(space);
        }

        //设备编号
        Integer maxDeviceNo = iotDeviceDao.findMaxDeviceNo();
        if (maxDeviceNo != null) {
            maxDeviceNo = maxDeviceNo + 1;
        } else {
            maxDeviceNo = 0;
        }
        iotDevice.setDeviceNo(maxDeviceNo);

        // 添加默认设备类型
        iotDevice.setDeviceType(DeviceType.DEFAULT);

        // 查询设备排序信息
        Integer maxSort = iotDeviceDao.findMaxSeq();
        maxSort = maxSort == null ? 0 : maxSort + 1;
        iotDevice.setSeq(maxSort);

        iotDeviceDao.save(iotDevice);

        IdResp idResp = new IdResp();
        idResp.setId(iotDevice.getId());
        return ServiceResult.ok(idResp);
    }

    @Override
    @Transactional
    public PageBean<IotDeviceResp> search(DevicePageReq devicePageReq) {
        Sort sort = Sort.by(Sort.Direction.DESC, "seq");
        Pageable pageable = PageRequest.of(devicePageReq.getPage(), devicePageReq.getSize(), sort);
        //查询条件

        PageBean<IotDeviceResp> pageBean = new PageBean<>();
        //当前页
        pageBean.setPage(pageable.getPageNumber());
        //每页记录数
        pageBean.setSize(pageable.getPageSize());

        Specification<IotDevice> specification = (root, criteriaQuery, criteriaBuilder) -> {
            //集合 用于封装查询条件
            List<Predicate> list = new ArrayList<>();
            //项目id
            if (!StringUtils.isEmpty(devicePageReq.getProjectId())) {

                list.add(criteriaBuilder.equal(root.get("project").get("id"), devicePageReq.getProjectId()));
            }

            if (!StringUtils.isEmpty(devicePageReq.getSpaceId())) {

                list.add(criteriaBuilder.equal(root.get("space").get("id"), devicePageReq.getSpaceId()));
            }

            if (StringUtils.isNotBlank(devicePageReq.getProductId())) {
                list.add(criteriaBuilder.equal(root.get("iotProduct").get("id"), devicePageReq.getProductId()));
            }

            if (devicePageReq.getIotNodeType() != null) {
                list.add(criteriaBuilder.equal(root.get("iotNodeType"), devicePageReq.getIotNodeType()));
            }

            if (StringUtils.isNotEmpty(devicePageReq.getApplication())) {
                // 根据所属应用筛选
                List<DeviceApplicationRelation> relationList = deviceApplicationRelationDao.findByApplicationId(devicePageReq.getApplication());
                list.add(root.get("id").in(relationList.stream().map(DeviceApplicationRelation::getDevice).map(IotDevice::getId).collect(Collectors.toList())));
            }

            Predicate predicateSn;
            Predicate predicateDeviceName;
            Predicate predicateDeviceNameEN;
            //sn码
            if (!StringUtils.isEmpty(devicePageReq.getSnOrName())) {
                predicateSn = criteriaBuilder.like(root.get("sn").as(String.class), "%" + devicePageReq.getSnOrName() + "%");
                predicateDeviceName = criteriaBuilder.like(root.get("deviceName").as(String.class), "%" + devicePageReq.getSnOrName() + "%");
                predicateDeviceNameEN = criteriaBuilder.like(root.get("hwDeviceName").as(String.class), "%" + devicePageReq.getSnOrName() + "%");

                list.add(criteriaBuilder.or(predicateSn, predicateDeviceName, predicateDeviceNameEN));
            }

            // 网络状态
            if (devicePageReq.getDeviceStatus() != null) {
                list.add(criteriaBuilder.equal(root.get("hwStatus"), devicePageReq.getDeviceStatus()));
            }

            // 数据来源
            if (StringUtils.isNotEmpty(devicePageReq.getDataSource())) {
                list.add(criteriaBuilder.equal(root.get("dataSource").as(String.class), devicePageReq.getDataSource()));
            }

            if (devicePageReq.getDeviceType() != null) {
                list.add(criteriaBuilder.equal(root.get("deviceType"), devicePageReq.getDeviceType()));
            }

            if (!CollectionUtils.isEmpty(devicePageReq.getExcludedIdList())) {

                list.add(root.get("id").in(devicePageReq.getExcludedIdList()).not());
            }

            // 不查询门牌数据
            list.add(criteriaBuilder.notEqual(root.get("deviceType"), DeviceType.DOOR_PLATE));

            return criteriaBuilder.and(list.toArray(new Predicate[list.size()]));
        };

        Page<IotDevice> iotDevicePage = iotDeviceDao.findAll(specification, pageable);

        //总记录数
        pageBean.setTotalElements(iotDevicePage.getTotalElements());
        //总页数
        pageBean.setTotalPages(iotDevicePage.getTotalPages());
        List<IotDeviceResp> list = new ArrayList<>();
        //循环 封装为IotDeviceResp对象
        if (!CollectionUtils.isEmpty(iotDevicePage.getContent())) {
            //遍历
            iotDevicePage.getContent().forEach(iotDevice -> {
                IotDeviceResp deviceResp = new IotDeviceResp();
                //id
                deviceResp.setId(iotDevice.getId());
                //设备名字
                deviceResp.setDeviceName(iotDevice.getDeviceName());
                //sn码
                deviceResp.setSn(iotDevice.getSn());
                //所属产品
                deviceResp.setIotProductName(Optional.ofNullable(iotDevice.getIotProduct()).map(IotProduct::getName).orElse(""));
//                // 如果华为所属产品不为空则展示
//                if (Objects.nonNull(iotDevice.getHwProductName())) {
//                    deviceResp.setIotProductName(iotDevice.getHwProductName());
//                }
                //设备类型
                deviceResp.setIotNodeType(iotDevice.getIotNodeType());
                if (Objects.nonNull(iotDevice.getIotNodeType())) {
                    deviceResp.setIotNodeTypeName(iotDevice.getIotNodeType().getName());
                }
                //所属项目
                if (Objects.nonNull(iotDevice.getProject())) {
                    deviceResp.setProjectName(iotDevice.getProject().getProjectName());
                }
                //空间
                if (Objects.nonNull(iotDevice.getSpace())) {
                    deviceResp.setSpace(deviceResp.getProjectName() + " / " + SpaceService.getFullSpaceName(iotDevice.getSpace(), iotDevice.getSpace().getName()) + " / " + iotDevice.getAddress());
                }
                // 排序序号
                deviceResp.setSeq(iotDevice.getSeq());
                deviceResp.setDeviceStatus(Optional.ofNullable(iotDevice.getHwStatus()).orElse(DeviceStatus.UNKNOW));
                // 网络状态
                deviceResp.setDeviceStatusName(Optional.ofNullable(iotDevice.getHwStatus()).map(DeviceStatus::getName).orElse(DeviceStatus.UNKNOW.getName()));
                // 数据来源
                deviceResp.setDataSource(Optional.ofNullable(iotDevice.getDataSource()).map(DataSource::getName).orElse(""));
                // 使用华为设别名称在页面上显示设备英文名
                deviceResp.setDeviceNameEN(Optional.ofNullable(iotDevice.getHwDeviceName()).orElse(""));
                // 设备位置
                deviceResp.setAddress(iotDevice.getAddress());

                deviceResp.setLastStatusChangedTime(iotDevice.getLastHwStatusChangedDatetime());

                // 查询应用列表
                deviceResp.setApplicationList(applicationService.getListByDevice(iotDevice.getId()));
                list.add(deviceResp);
            });

        }
        pageBean.setList(list);
        log.info(String.valueOf(pageBean));
        return pageBean;
    }

    @Override
    @Transactional
    public IotDeviceResp getIotDeviceInfo(String id, String token) {
        Optional<IotDevice> iotDeviceOptional = iotDeviceDao.findById(id);
        IotDeviceResp iotDeviceResp = new IotDeviceResp();
        if (!iotDeviceOptional.isPresent()) {
            return iotDeviceResp;
        } else {
            IotDevice iotDevice = iotDeviceOptional.get();
            //设备名称
            iotDeviceResp.setDeviceName(iotDevice.getDeviceName());
            //IOT节点类型
            iotDeviceResp.setIotNodeType(iotDevice.getIotNodeType());
            //所属空间
            if (Objects.nonNull(iotDevice.getSpace())) {
                iotDeviceResp.setSpace(iotDevice.getSpace().getName());
                iotDeviceResp.setSpaceId(iotDevice.getSpace().getId());
            }
            //所属项目
            iotDeviceResp.setProjectId(Optional.ofNullable(iotDevice.getProject()).map(Project::getId).orElse(""));
            iotDeviceResp.setProjectName(Optional.ofNullable(iotDevice.getProject()).map(Project::getProjectName).orElse(""));
            //所属产品
            if (Objects.nonNull(iotDevice.getIotProduct())) {
                iotDeviceResp.setIotProductName(iotDevice.getIotProduct().getName());
                iotDeviceResp.setProductId(iotDevice.getIotProduct().getId());
            }
            //sn码
            iotDeviceResp.setSn(iotDevice.getSn());

            iotDeviceResp.setDeviceNameEN(iotDevice.getHwDeviceName());
            iotDeviceResp.setAddress(Optional.ofNullable(iotDevice.getAddress()).orElse(""));
            // 设备类型
            iotDeviceResp.setDeviceType(iotDevice.getDeviceType());

            iotDeviceResp.setSpaceControl(iotDevice.getSpaceControl() == null ? 0 : iotDevice.getSpaceControl());

            iotDeviceResp.setApplicationList(applicationService.getListByDevice(iotDevice.getId()));

            //设置用户对设备的别名
            setAlisa(id, token, iotDeviceResp);
        }
        return iotDeviceResp;
    }

    private void setAlisa(String deviceId, String token, IotDeviceResp iotDeviceResp) {
        if (StringUtils.isEmpty(token)) {
            return;
        }
        String hrId = JwtUtil.getHrId(token);
        Optional<IntelligentOffice> officeOp = intelligentOfficeDao.findBySpaceId(iotDeviceResp.getSpaceId());
        if(officeOp.isPresent()){
            Optional<IntelligentOfficeDevice> officeDeviceOptional = intelligentOfficeDeviceDao.findByIntelligentOfficeIdAndDeviceIdAndUserHrId(officeOp.get().getId(), deviceId, hrId);
            if (officeDeviceOptional.isPresent()) {
                IntelligentOfficeDevice officeDevice = officeDeviceOptional.get();
                if (StringUtils.isNotEmpty(officeDevice.getDeviceAlisa())) {
                    iotDeviceResp.setDeviceName(officeDevice.getDeviceAlisa());
                }
                iotDeviceResp.setAdditionalName(StringUtils.isEmpty(officeDevice.getDeviceAlisaProperties())
                        ? DeviceNameEditReq.AdditionalName.empty()
                        : JSON.parseObject(officeDevice.getDeviceAlisaProperties(), DeviceNameEditReq.AdditionalName.class));
            }
        }
    }

    @Override
    @Transactional
    public ServiceResult deleteById(String id) {
        Optional<IotDevice> iotDeviceOptional = iotDeviceDao.findById(id);
        if (!iotDeviceOptional.isPresent()) {
            return ServiceResult.error("删除失败，未找到id对应数据");
        }

        IotDevice device = iotDeviceOptional.get();

        // 如果是门磁或者旷世刷脸设备，需要判断是否有通行权限
        if (device.getDeviceType().equals(DeviceType.DOOR_MAGNETIC)) {
            QrcodeAccessRuleDetailResp qrcodeAccessRuleDetailResp = qrcodeAccessRuleService.getByDeviceId(id);
            if (qrcodeAccessRuleDetailResp != null) {
                return ServiceResult.error("删除失败，该设备存在通信权限");
            }
        } else if (device.getDeviceType().equals(DeviceType.FACE_RECOGNITION)) {
            AccessRuleDetailResp accessRuleDetailResp = accessRulesService.getByDeviceId(id);
            if (accessRuleDetailResp != null) {
                return ServiceResult.error("删除失败，该设备存在通信权限");
            }
        }

        device.setIsDelete(1);
        device.setUpdatedTime(LocalDateTime.now());
        iotDeviceDao.save(device);

        // 更改空间设备数
        if (device.getSpace() != null) {
            updateSpaceNum(device, spaceDao);
        }
        return ServiceResult.ok();
    }

    @Override
    @Transactional
    public ServiceResult update(String id, IotDeviceCreateReq dto) {
        //根据设备id查询iot设备
        Optional<IotDevice> optionalIotDevice = iotDeviceDao.findById(id);

        if (!optionalIotDevice.isPresent()) {
            return ServiceResult.error("未找到设备信息");
        }

        IotDevice iotDevice = optionalIotDevice.get();

        //iot设备名称
        iotDevice.setDeviceName(dto.getDeviceName());
        //iot设备节点类型
//        iotDevice.setIotNodeType(dto.getIotNodeType());

//        iotDevice.setDeviceType(dto.getDeviceType());

        // 项目信息
        Optional<Project> projectOptional = projectDao.findById(dto.getProjectId());
        if (!projectOptional.isPresent()) {
            return ServiceResult.error("未找到项目信息");
        }
        iotDevice.setProject(projectOptional.get());

        // 验证空间
        Space space = null;
        // 更换空间为空 原空间-1
        if ((dto.getSpaceId() == null || dto.getSpaceId().isEmpty()) && iotDevice.getSpace() != null) {
            Optional<Space> spaceOp = spaceDao.findById(iotDevice.getSpace().getId());
            if (spaceOp.isPresent()) {
                Space spaceOption = spaceOp.get();
                spaceOption.setDeviceNum(spaceOption.getDeviceNum() - 1);
                spaceDao.save(spaceOption);
            }
        }
        if (dto.getSpaceId() != null && dto.getSpaceId().length() > 0) {
            Optional<Space> optionalSpace = spaceDao.findById(dto.getSpaceId());
            if (!optionalSpace.isPresent()) {
                return ServiceResult.error("未找到空间信息");
            }
            space = optionalSpace.get();
            // 更改空间设备数
            // 更换空间 原空间-1 现空间+1
            if (iotDevice.getSpace() != null) {
                if (!dto.getSpaceId().equals(iotDevice.getSpace().getId())) {
                    // update device space
                    deviceSpaceChange(iotDevice, iotDevice.getSpace(), space);

//                    // 原空间模式下设备删除
//                    List<DeviceModeStatus> modeStatusList = modeStatusDao.findByModeSpaceIdAndDeviceId(iotDevice.getSpace().getId(), iotDevice.getId());
//                    modeStatusList.forEach(it -> {
//                        it.setIsDelete(1);
//                        modeStatusDao.save(it);
//                    });

                    space.setDeviceNum(space.getDeviceNum() + 1);
                    spaceDao.save(space);
                    Optional<Space> spaceOp = spaceDao.findById(iotDevice.getSpace().getId());
                    if (spaceOp.isPresent()) {
                        Space spaceOption = spaceOp.get();
                        spaceOption.setDeviceNum(spaceOption.getDeviceNum() - 1);
                        spaceDao.save(spaceOption);
                    }
                }
            } else {
                space.setDeviceNum(space.getDeviceNum() + 1);
                spaceDao.save(space);
            }
            if (space.getProject() == null || !space.getProject().getId().equals(dto.getProjectId())) {
                return ServiceResult.error("空间数据与项目数据不匹配");
            }
            spaceCacheService.setSpaceInRedis(space);
        }

//        if (null != iotDevice.getSpace() && !dto.getSpaceId().equals(iotDevice.getSpace().getId())) {
//            // 如果修改了设备所属空间，则将原设备空间下的所有用户权限删除
//            removeDeviceAllControlUsers(id);
//        }

        iotDevice.setSpace(space);

        if (ObjectUtils.isNotEmpty(iotDevice.getSpace())&&ObjectUtils.isNotEmpty(iotDevice.getSpace().getId())){
            //原设备所属空间id
            String spaceId = iotDevice.getSpace().getId();

            //校验规则
            //根据空间id查询所以关联空间
            List<Space> spaceList = spaceDao.findAllByIdOrParentId(spaceId,spaceId);

            //所有空间id
            List<String> spaceIds = new ArrayList<>();
            spaceList.forEach(it ->
                    spaceIds.add(it.getId())
            );

            //根据所有空间id查询所有办公室
            List<IntelligentOffice> officeList = intelligentOfficeDao.findAllBySpaceIdIn(spaceIds);

            //officeIds
            List<String> officeIdList = new ArrayList<>();
            for (IntelligentOffice intelligentOffice :officeList){
                officeIdList.add(intelligentOffice.getId());
            }

            //查询所有规则
            List<IntelligentOfficeRuleEngine> intelligentOfficeRuleEngineList = intelligentOfficeRuleEngineDao.findAllByIntelligentOfficeIdIn(officeIdList);

            for (IntelligentOfficeRuleEngine it : intelligentOfficeRuleEngineList){
                //判断设备是否可控
                if(ObjectUtils.isNotEmpty(iotDevice.getSpaceControl()) && iotDevice.getSpaceControl() == IConst.SPACE_CONTROL){

                    //可控
                    //将json格式的执行设备转换
                    List<RuleEngIneDetailActuatorResp> detailActuatorResps = JSON.parseArray(it.getActuator(), RuleEngIneDetailActuatorResp.class);

                    for(int i=0;i<detailActuatorResps.size();i++){
                        RuleEngIneDetailActuatorResp actuator = detailActuatorResps.get(i);
                        //设备id不为空, 执行设备中id与变更id设备相同
                        if (actuator.getDeviceId() != null && iotDevice.getId().equals(actuator.getDeviceId())){
                            detailActuatorResps.remove(actuator);
                            it.setActuator(JSON.toJSONString(detailActuatorResps));
                            log.info("执行设备修改{}",detailActuatorResps);
                            intelligentOfficeRuleEngineDao.save(it);
                        }
                    }

                    //如果规则不成立删除规则
                    //可控设备为0
                    if(CollUtil.isEmpty(detailActuatorResps)){
                        it.setIsDelete(1);
                        intelligentOfficeRuleEngineDao.save(it);

                        //删除规则
                        Telemetry telemetry = new Telemetry();
                        telemetry.setRuleChainId(it.getId());

                        GroupBindingReq groupBindingReq = new GroupBindingReq();
                        groupBindingReq.setChainSite("cloud");
                        groupBindingReq.setChannelType("3");
                        groupBindingReq.setJsonArg(JSON.toJSONString(telemetry));
                        groupBindingReq.setMethodCode(MethodCode.DELETE_RULECHAIN.getName());
                        groupBindingReq.setModuleCode(ModuleCode.HW.getName());
                        JsonResult<Object> objectJsonResult = iotcApi.postToConn(groupBindingReq);

                        if (objectJsonResult.getStatus() == 0) {
                            log.error("规则删除失败原因：" + objectJsonResult.getMsg() + "code" + objectJsonResult.getCode());
                            //  联动规则删除失败，手动回滚事务,返回异常
                            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                            return ServiceResult.error(objectJsonResult.getMsg());
                        }
                    }

                }else {
                    //不可控
                    //将json格式的触发条件设备转换
                    List<RuleEngIneDetailTriggerResp> detailTriggerResps = JSON.parseArray(it.getTrigger(),RuleEngIneDetailTriggerResp.class);

                    for(int i=0;i<detailTriggerResps.size();i++){
                        RuleEngIneDetailTriggerResp trigger = detailTriggerResps.get(i);
                        log.info("触发条件{}",trigger);
                        //设备id不为空, 执行设备中id与变更id设备相同
                        if (trigger.getDeviceId() != null && iotDevice.getId().equals(trigger.getDeviceId())){
                            detailTriggerResps.remove(trigger);
                            it.setActuator(JSON.toJSONString(detailTriggerResps));
                            log.info("触发条件修改{}",detailTriggerResps);
                            intelligentOfficeRuleEngineDao.save(it);
                        }
                    }

                    //如果规则不成立删除规则
                    //可控设备为0
                    if(CollUtil.isEmpty(detailTriggerResps)){
                        it.setIsDelete(1);
                        intelligentOfficeRuleEngineDao.save(it);

                        //删除规则
                        Telemetry telemetry = new Telemetry();
                        telemetry.setRuleChainId(it.getId());

                        GroupBindingReq groupBindingReq = new GroupBindingReq();
                        groupBindingReq.setChainSite("cloud");
                        groupBindingReq.setChannelType("3");
                        groupBindingReq.setJsonArg(JSON.toJSONString(telemetry));
                        groupBindingReq.setMethodCode(MethodCode.DELETE_RULECHAIN.getName());
                        groupBindingReq.setModuleCode(ModuleCode.HW.getName());
                        JsonResult<Object> objectJsonResult = iotcApi.postToConn(groupBindingReq);

                        if (objectJsonResult.getStatus() == 0) {

                            log.error("规则删除失败原因：" + objectJsonResult.getMsg() + "code" + objectJsonResult.getCode());
                            //  联动规则删除失败，手动回滚事务,返回异常
                            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                            return ServiceResult.error(objectJsonResult.getMsg());
                        }
                    } else {
                        //修改规则
                        //规则链创建接收对象
                        RuleEngineDetailReq detailReq = new RuleEngineDetailReq();
                        detailReq.setOfficeId(it.getIntelligentOffice().getId());
                        detailReq.setImgUrl(it.getLogo());
                        detailReq.setName(it.getName());
                        detailReq.setPurpose(it.getDescribe());
                        detailReq.setTriggerCondition(it.getTriggerCondition());
                        detailReq.setTriggerReqs(JSON.parseArray(it.getTrigger(), RuleEngIneDetailTriggerReq.class));
                        detailReq.setActuatorReqs(JSON.parseArray(it.getActuator(), RuleEngIneDetailActuatorReq.class));
                        detailReq.setUsableStatus(it.getUsableStatus());
                        detailReq.setEffectiveTypeReq(JSON.parseObject(it.getEffectiveType(), RuleEngineDetailEffectiveTypeReq.class));
                        //调用修改规则接口
                        log.info("规则id{}",it.getId());
                        log.info("调用修改规则接口，修改参数{}",detailReq);
                        intelligentOfficeRuleEngineService.updateRileEngine(it.getId(),detailReq);
                    }
                }
            }
        }

        // 验证设备
        IotProduct product = null;
        if (dto.getProductId() != null) {
            Optional<IotProduct> optionalIotProduct = iotProductDao.findById(dto.getProductId());
            if (!optionalIotProduct.isPresent()) {
                return ServiceResult.error("未找到产品信息");
            }
            product = optionalIotProduct.get();
        }
        iotDevice.setIotProduct(product);

        // 设备位置
        iotDevice.setAddress(dto.getAddress());

        iotDevice.setSpaceControl(dto.getSpaceControl());

        //编辑
        iotDeviceDao.save(iotDevice);

        //更新设备应用信息
        if (!CollectionUtils.isEmpty(dto.getApplicationList())) {

            List<DeviceApplicationRelation> deviceApplicationRelationList = deviceApplicationRelationDao.findByDeviceId(iotDevice.getId());
            Set<String> dbApplicationIdSet = deviceApplicationRelationList.stream().map(DeviceApplicationRelation::getApplication).map(Application::getId).collect(Collectors.toSet());

            // 计算出需要被删除和需要被添加的数据
            Set<String> addSet = new HashSet<>(dto.getApplicationList());
            addSet.removeAll(dbApplicationIdSet);

            Set<String> delSet = new HashSet<>(dbApplicationIdSet);
            delSet.removeAll(dto.getApplicationList());

            List<DeviceApplicationRelation> list = deviceApplicationRelationList.stream()
                    .filter(obj -> delSet.contains(obj.getApplication().getId()))
                    .map(obj -> {
                        obj.setIsDelete(1);
                        return obj;
                    }).collect(Collectors.toList());

            deviceApplicationRelationDao.saveAll(list);
            list = new ArrayList<>();
            for (String applicationId : addSet) {
                Optional<Application> applicationOptional = applicationDao.findById(applicationId);
                if (applicationOptional.isEmpty()) {
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return ServiceResult.error(ServiceMessage.getMessageWithColon("未找到应用数据", applicationId));
                }

                DeviceApplicationRelation relation = new DeviceApplicationRelation();
                relation.setDevice(iotDevice);
                relation.setApplication(applicationOptional.get());
                list.add(relation);
            }

            deviceApplicationRelationDao.saveAll(list);
        } else {
            List<DeviceApplicationRelation> deviceApplicationRelationList = deviceApplicationRelationDao.findByDeviceId(iotDevice.getId());
            deviceApplicationRelationList.forEach(obj -> obj.setIsDelete(1));
            deviceApplicationRelationDao.saveAll(deviceApplicationRelationList);
        }
        return ServiceResult.ok();
    }

    /**
     * 对设备数据进行排序
     *
     * @param sort
     * @return
     */
    @Override
    @Transactional
    public ServiceResult sort(IotDeviceSortReq sort) {

        // 需排队的list对象
        List<IotDevice> iotDevices = new ArrayList<>();
        List<IdAndSeqBean> list = sort.getData();
        // 循环
        list.forEach(it -> {
            // 根据id查询排序数据是否存在
            Optional<IotDevice> iotDeviceOptional = iotDeviceDao.findById(it.getId());
            // 获取信息
            if (iotDeviceOptional.isPresent()) {
                IotDevice iotDevice = iotDeviceOptional.get();
                // 设置排序
                iotDevice.setSeq(it.getSeq());
                // 添加空列表内
                iotDevices.add(iotDevice);
            }
        });

        // 保存排序结果
        iotDeviceDao.saveAll(iotDevices);
        return ServiceResult.ok();
    }

    /**
     * 获取设备网络变化分页数据
     *
     * @param deviceId     设备ID
     * @param deviceStatus 设备状态
     * @param page         页码
     * @param size         页长
     * @return 分页数据
     * @author Yang.Lee
     * @date 2021/5/7 10:40
     **/
    @Override
    @Transactional
    public PageBean<IotDeviceNetworkChangeResp> getDeviceNetworkChangedPage(String deviceId, String deviceStatus, int page, int size) {

//        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "dataTime"));
//        Page<IotDeviceNetworkChange> IotDeviceNetworkChangePage = iotDeviceNetworkChangeDao.findAllByDeviceId(deviceId, pageable);

        // 根据设备Id查询SN

        Optional<IotDevice> iotDeviceOptional = iotDeviceDao.findById(deviceId);
        if (iotDeviceOptional.isEmpty()) {
            return new PageBean<>();
        }

        JsonResult<PageBean<DeviceStatusChangeListResp>> jsonResult = iotcApi.getStatusList(page, size, iotDeviceOptional.get().getSn(), deviceStatus);

        if (jsonResult.getStatus() == 0) {
            return new PageBean<>();
        }
        PageBean<DeviceStatusChangeListResp> iotcRespPage = jsonResult.getData();
        List<DeviceStatusChangeListResp> iotcRespList = iotcRespPage.getList();
        PageBean<IotDeviceNetworkChangeResp> result = new PageBean<>();
        result.setPage(page);
        result.setSize(size);
        result.setTotalPages(iotcRespPage.getTotalPages());
        result.setTotalElements(iotcRespPage.getTotalElements());
        result.setList(iotcRespList.stream().map(obj -> IotDeviceNetworkChangeResp.builder()
                .deviceId(obj.getDevice().getId())
                .deviceName(obj.getDevice().getChName())
                .deviceStatus(obj.getDeviceStatus())
                .deviceStatusName(obj.getDeviceStatus().getName())
                .dataTime(obj.getDataTime())
                .build()).collect(Collectors.toList()));

        return result;
    }

    /**
     * 控制设备
     *
     * @param deviceId 设备ID
     * @param cmd      控制指令
     * @return 控制结果
     * @author Yang.Lee
     * @date 2021/6/16 15:41
     **/
    @Override
    @Transactional
    public ServiceResult control(String deviceId, String cmd) {

        log.debug("控制设备 : " + deviceId, cmd);
        Optional<IotDevice> byId = iotDeviceDao.findById(deviceId);
        if (byId.isPresent()) {
            IotDevice iotDevice = byId.get();
            log.debug("控制设备 : " + iotDevice.getHwDeviceName(), cmd);
            // 判断设备来源 设备是否在线
            if (DataSource.HW_CLOUD.equals(iotDevice.getDataSource()) && DeviceStatus.ONLINE.equals(iotDevice.getHwStatus())) {
                // 获取设备所属产品的服务ID和命令名称
                ServiceResult serviceResult = productsService.findProductProperties(iotDevice);
                if(!serviceResult.isSuccess()){
                    log.error("设备控制失败,重新控制：" + deviceId);
                    errorIotDeviceMessageProducer.deviceControl(deviceId, cmd);
                }
                DoorMagneticHWReq hwReq = (DoorMagneticHWReq) serviceResult.getObject();
                // 如果是断路器及四路开关通过设备属性控制
                if (DeviceType.CIRCUIT_BREAKER.equals(iotDevice.getDeviceType())) {
                    // 下发控制设备
                    DoorMagneticHWParasReq paras = new DoorMagneticHWParasReq();
                    // 断路器控制命令
                    paras.setIsSwitch(cmd);
                    hwReq.setParas(paras);
                    hwReq.setDeviceId(iotDevice.getId());
                    hwReq.setDevice_id(iotDevice.getHwDeviceId());
                    log.info("毫米波控制设备" + iotDevice.getHwDeviceId());
                    log.info("毫米波控制设备命令" + JSON.toJSONString(paras));
                    ServiceResult result = productsService.updateSwitch(hwReq, "", "毫米波");
                    if (!result.isSuccess()) {
                        log.error("设备控制失败,重新控制：" + deviceId);
                        errorIotDeviceMessageProducer.deviceControl(deviceId, cmd);
                    } else {
                        log.info("设备控制成功,：" + deviceId);
                    }
                } else if (DeviceType.FOUR_WAY_SWITCH.equals(iotDevice.getDeviceType())) {
                    // 下发控制设备
                    DoorMagneticHWParasReq paras = new DoorMagneticHWParasReq();
                    // 四路开关控制命令
                    paras.setLight1(cmd);
                    paras.setLight2(cmd);
                    paras.setLight3(cmd);
                    paras.setLight4(cmd);
                    hwReq.setParas(paras);
                    hwReq.setDeviceId(iotDevice.getId());
                    hwReq.setDevice_id(iotDevice.getHwDeviceId());
                    log.info("毫米波控制设备" + iotDevice.getHwDeviceId());
                    log.info("毫米波控制设备命令" + JSON.toJSONString(paras));
                    ServiceResult result = productsService.updateSwitch(hwReq, "", "毫米波");
                    if (!result.isSuccess()) {
                        log.error("设备控制失败,重新控制：" + deviceId);
                        errorIotDeviceMessageProducer.deviceControl(deviceId, cmd);
                    } else {
                        log.info("设备控制成功,：" + deviceId);
                    }
                } else if (DeviceType.CURTAINS.equals(iotDevice.getDeviceType())) {
                    DoorMagneticHWParasReq paras = new DoorMagneticHWParasReq();
                    // 控制窗帘
                    paras.setAction("off".equals(cmd) ? "close" : "open");
                    hwReq.setParas(paras);
                    hwReq.setDevice_id(iotDevice.getHwDeviceId());
                    log.info("毫米波控制设备" + iotDevice.getHwDeviceId());
                    log.info("毫米波控制设备命令" + JSON.toJSONString(paras));
                    hwReq.setDeviceId(iotDevice.getId());
                    ServiceResult result = productsService.command(hwReq, "", "毫米波");
                    if (!result.isSuccess()) {
                        log.error("设备控制失败,重新控制：" + deviceId);
                        errorIotDeviceMessageProducer.deviceControl(deviceId, cmd);
                    } else {
                        log.info("设备控制成功,：" + deviceId);
                    }
                }
            }
        }

        return ServiceResult.ok();
    }

    /**
     * 打开空间下的所有可控设备
     *
     * @param space 空间
     * @return 控制结果
     * @author Yang.Lee
     * @date 2021/6/16 15:43
     **/
    @Override
    @Transactional
    public ServiceResult openSpaceDevices(Space space) {

        // 查找该空间下所有可被控制的设备列表
        List<IotDevice> controlList = iotDeviceDao.findBySpaceIdAndSpaceControl(space.getId(), 1);
        if (controlList.isEmpty()) {
            return ServiceResult.ok("该空间下无可被控制的设备");
        }

        // 找到继电器，如果有则先打开开关
        List<IotDevice> breakerList = controlList.stream().filter(obj -> DeviceType.CIRCUIT_BREAKER.equals(obj.getDeviceType())).collect(Collectors.toList());
        if (!breakerList.isEmpty()) {
            breakerList.forEach(obj -> {
                ServiceResult serviceResult = iotDeviceMessageProducer.deviceControl(obj.getId(), "on");
                if (!serviceResult.isSuccess()) {
                    log.error("开启断路器 {} 失败，{}", obj.getId(), serviceResult.getObject());
                }
            });
        }

        // 控制除开关外的其他设备
        controlList.removeAll(breakerList);
        String deviceId = "";
        // 控制除开关外的其他设备
        controlList.removeAll(breakerList);
        for (IotDevice device : controlList) {
            deviceId += device.getId() + ",";
        }

        ServiceResult serviceResult = iotDeviceMessageProducer.deviceControl(deviceId, "on");
        if (!serviceResult.isSuccess()) {
            log.error("开启设备 {} 失败，{}", deviceId, serviceResult.getObject());
        }

        return ServiceResult.ok();
    }

    /**
     * 关闭空间下所有设备
     *
     * @param space space 空间
     * @return 控制结果
     * @author Yang.Lee
     * @date 2022/1/19 15:45
     **/
    @Override
    @Transactional
    public ServiceResult closeSpaceDevices(Space space) {

        List<IotDevice> deviceList = iotDeviceDao.findBySpaceId(space.getId());
        deviceList.forEach(obj -> control(obj.getId(), "off"));

        return null;
    }

    /**
     * 获取海康面板机分页数据
     *
     * @param req 请求参数
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/7/20 15:19
     **/
    @Override
    @Transactional
    public PageBean<HIKAccessRulePageResp> getHIKFaceMachinePage(HIKFaceMachinePageReq req) {

        // 根据项目，名称，空间查询出ibms数据库中的数据

        List<IotDevice> hkDeviceList = iotDeviceDao.findAll((root, query, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();

            // 项目ID
            if (StringUtils.isNotBlank(req.getProjectId())) {
                list.add(criteriaBuilder.equal(root.get("project").get("id"), req.getProjectId()));
            }
            // 空间ID
            if (StringUtils.isNotBlank(req.getSpaceId())) {
                list.add(criteriaBuilder.equal(root.get("space").get("id"), req.getSpaceId()));
            }

            if (StringUtils.isNotEmpty(req.getKeyword1())) {
                // 设备ID
                String like = "%" + req.getKeyword1() + "%";
                list.add(criteriaBuilder.or(criteriaBuilder.like(root.get("sn"), like), criteriaBuilder.like(root.get("deviceName"), like)));
            }

            list.add(criteriaBuilder.equal(root.get("deviceType"), DeviceType.HIK_FACE_MACHINE));

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, Sort.by(req.getOrders()));

        // TODO 如果keyword2有值，则查询HK-Connector 找出所有人员相关设备，然后区两个list的并集


        List<HIKAccessRulePageResp> resultList = new ArrayList<>();

        hkDeviceList.forEach(obj -> {
            HIKAccessRulePageResp resp = new HIKAccessRulePageResp();
            resp.setDeviceName(obj.getDeviceName());
            resp.setDeviceAddress(Optional.ofNullable(obj.getAddress()).orElse(""));
            resp.setSn(Optional.ofNullable(obj.getSn()).orElse(""));
            resp.setProjectName(Optional.ofNullable(obj.getProject()).map(Project::getProjectName).orElse(""));
            resp.setSpaceName(Optional.ofNullable(obj.getSpace()).map(space -> SpaceService.getFullSpaceName(space, space.getName())).orElse(""));
            resp.setDeviceSourceName(Optional.ofNullable(obj.getDataSource()).map(DataSource::getName).orElse(""));
            resp.setDeviceId(obj.getId());
            resultList.add(resp);
        });

        PageBean<HIKAccessRulePageResp> result = new PageBean<>();
        result.setPage(req.getPage());
        result.setSize(req.getSize());
        result.setTotalElements(resultList.size());


        result.setList(resultList);
        int totalPages = 1;
        if (req.getSize() * req.getPage() > resultList.size()) {
            result.setList(new ArrayList<>());
        } else {
            if (resultList.size() > req.getSize()) {
                int left = resultList.size() % req.getSize();
                int right = resultList.size() / req.getSize();
                totalPages = left == 0 ? right : right + 1;

                int subStartIndex = req.getPage() * req.getSize();
                int subEndIndex = (req.getPage() + 1) * req.getSize() - 1;

                if (subEndIndex > resultList.size()) {
                    subEndIndex = resultList.size();
                }

                List<HIKAccessRulePageResp> pageList = resultList.subList(subStartIndex, subEndIndex);
                result.setList(pageList);

            }
        }

        result.setTotalPages(totalPages);

        return result;
    }

    /**
     * 从iotc获取设备列表数据
     *
     * @return 设备列表
     * @author Yang.Lee
     * @date 2021/10/27 16:12
     **/
    @Override
    public List<DeviceResp> getDeviceListFromIOTC() {

        // 直接获取99999条数据，不用多次请求
        JsonResult<PageBean<DeviceResp>> jsonResult = iotcApi.getDeviceList(0, 99999);
        if (jsonResult.getStatus() == 0) {
            return new ArrayList<>();
        }

        return jsonResult.getData().getList();
    }

    /**
     * 保存设备数据（从IOTC同步的数据）
     *
     * @param deviceList 设备数据
     * @return 保存结果
     * @author Yang.Lee
     * @date 2021/10/27 16:45
     **/
    @Override
    @Transactional
    public ServiceResult saveDeviceList(List<DeviceResp> deviceList) {

        // 循环所有设备，判断哪些设备是新加的，哪些设备是已经存在的

        List<IotDevice> updateList = new ArrayList<>();
        List<DeviceResp> addList = new ArrayList<>();

        deviceList.forEach(obj -> {
            if (Objects.isNull(obj.getDeviceProperties())) {
                obj.setDeviceProperties(new DeviceResp.DeviceProperties());
            }
            try {
                Optional<IotDevice> deviceOptional = iotDeviceDao.findBySn(obj.getCode());
                if (deviceOptional.isPresent()) {

                    IotDevice iotDevice = deviceOptional.get();
                    iotDevice.setDeviceName(obj.getDeviceName());
                    iotDevice.setDataSource(getDataSourceFromIotc(obj.getDeviceChannelVo().getChannelType()));
                    iotDevice.setIotNodeType(obj.getIotNodeType());
                    iotDevice.setStatus(1);
                    iotDevice.setHwStatus(obj.getWorkStatus());
                    iotDevice.setLastHwStatusChangedDatetime(obj.getChangeTime());
                    iotDevice.setHwGatewayId(obj.getDeviceProperties().getHwGatewayId());
                    iotDevice.setHwDeviceId(obj.getDeviceProperties().getHwDeviceId());
                    iotDevice.setHwDeviceName(obj.getDeviceProperties().getHwDeviceName());
                    iotDevice.setHwNodeType(obj.getDeviceProperties().getHwNodeType());
                    iotDevice.setHwProductId(obj.getDeviceProperties().getHwProductId());
                    iotDevice.setHwProductName(obj.getDeviceProperties().getHwProductName());
                    iotDevice.setHwNodeType(obj.getDeviceProperties().getHwNodeType());

                    updateList.add(iotDevice);

                } else {
                    addList.add(obj);
                }
            } catch (Exception e) {
                log.error("saveDeviceList error sn : {}", obj.getCode());
            }
        });

        // 保存所有的修改数据
        iotDeviceDao.saveAll(updateList);

        addList.forEach(obj -> {
            if (Objects.isNull(obj.getDeviceProperties())) {
                obj.setDeviceProperties(new DeviceResp.DeviceProperties());
            }

            IotDevice iotDevice = new IotDevice();

            iotDevice.setDeviceType(obj.getDeviceType());
            iotDevice.setDeviceName(obj.getDeviceName());
            iotDevice.setDataSource(getDataSourceFromIotc(obj.getDeviceChannelVo().getChannelType()));
            iotDevice.setIotNodeType(obj.getIotNodeType());
            iotDevice.setStatus(1);
            iotDevice.setHwDeviceId(obj.getDeviceProperties().getHwDeviceId());
            iotDevice.setHwStatus(obj.getWorkStatus());
            iotDevice.setLastHwStatusChangedDatetime(obj.getChangeTime());
            iotDevice.setHwGatewayId(obj.getDeviceProperties().getHwGatewayId());
            iotDevice.setHwDeviceName(obj.getDeviceProperties().getHwDeviceName());
            iotDevice.setHwNodeType(obj.getDeviceProperties().getHwNodeType());
            iotDevice.setHwProductId(obj.getDeviceProperties().getHwProductId());
            iotDevice.setHwProductName(obj.getDeviceProperties().getHwProductName());
            iotDevice.setHwNodeType(obj.getDeviceProperties().getHwNodeType());
            iotDevice.setSn(obj.getCode());

            iotDeviceDao.save(iotDevice);

            if (Optional.ofNullable(iotDevice.getDeviceType()).isPresent()) {
                // 根据设备类型创建数据
                switch (iotDevice.getDeviceType()) {
                    case DOOR_MAGNETIC:

                        DoorMagnetic doorMagnetic = new DoorMagnetic();

                        doorMagnetic.setDevice(iotDevice);
                        doorMagnetic.setHuaweiId(iotDevice.getHwDeviceId());
                        doorMagnetic.setName(iotDevice.getDeviceName());
                        doorMagnetic.setNumber(RandomCode.getRandom(8));
                        doorMagnetic.setDoorMagneticSource(DoorMagneticSource.HW_CLOUD);
                        doorMagneticDao.save(doorMagnetic);

                        break;
                    case AIR_CONDITIONER:

                        AirConditioner airConditioner = new AirConditioner();
                        airConditioner.setDevice(iotDevice);
                        airConditionerDao.save(airConditioner);

                        break;
                    case CURTAINS:

                        Curtain curtain = new Curtain();
                        curtain.setDevice(iotDevice);

                        curtainDao.save(curtain);
                        break;
                    case CIRCUIT_BREAKER:

                        CircuitBreaker circuitBreaker = new CircuitBreaker();
                        circuitBreaker.setIotDevice(iotDevice);
                        circuitBreakerDao.save(circuitBreaker);

                        break;
                    case FACE_RECOGNITION:

                        DeviceCloud deviceCloud = new DeviceCloud();
                        deviceCloud.setIotDevice(iotDevice);
                        deviceCloud.setKsDeviceId(obj.getDeviceProperties().getUuid());
                        deviceCloud.setCloudDeviceId(obj.getDeviceProperties().getSnCode());
                        deviceCloud.setAddress("");
                        deviceCloud.setName(obj.getDeviceName());
                        deviceCloudDao.save(deviceCloud);
                        break;
                    case HIK_FACE_MACHINE:

                        HkDevice hkDevice = new HkDevice();
                        hkDevice.setDevice(iotDevice);
                        hkDevice.setResourceIndexCode(obj.getCode());
                        hkDevice.setIp(obj.getDeviceProperties().getIp());
                        if (Objects.nonNull(obj.getDeviceProperties().getCheckinSite())) {
                            hkDevice.setAddress(HkAddress.valueOf(obj.getDeviceProperties().getCheckinSite().toUpperCase(Locale.ROOT)));
                        }
                        hkDeviceDao.save(hkDevice);
                        break;
                    case CAMERA:

                        YSVideoCamera ysVideoCamera = new YSVideoCamera();
                        ysVideoCamera.setSerial(obj.getCode());

                        ysVideoCamera.setDevice(iotDevice);
                        ysVideoCamera.setCategory(obj.getDeviceProperties().getParentCategory());
                        ysVideoCamera.setDeviceVersion(obj.getDeviceProperties().getDeviceVersion());
                        ysVideoCamera.setDefence(obj.getDeviceProperties().getDefence());
                        ysVideoCamera.setAlarmSoundMode(obj.getDeviceProperties().getAlarmSoundMode());
                        ysVideoCamera.setIsEncrypt(obj.getDeviceProperties().getIsEncrypt());
                        ysVideoCamera.setModel(obj.getDeviceProperties().getModel());
                        ysVideoCamera.setNetType(obj.getDeviceProperties().getNetType());
                        ysVideoCamera.setOfflineNotify(obj.getDeviceProperties().getOfflineNotify());
                        ysVideoCamera.setSignal(obj.getDeviceProperties().getSignal());
                        ysVideoCamera.setYsDeviceType(obj.getDeviceProperties().getDeviceType());

                        ysVideoCameraDao.save(ysVideoCamera);
                        break;
                    default:
                        break;
                }
            }

        });

        return ServiceResult.ok();
    }


    @Override
    public ServiceResult fullView() {
        List<IotDevice> devices = iotDeviceDao.findByDeviceTypeNot(DeviceType.DOOR_PLATE);
        List<Project> projects = projectDao.findAll();
        List<FactoryDeviceMsgResp> list = new ArrayList<>();
        List<FullViewRadioResp> radioList = new ArrayList<>();
        List<FullViewRoundResp> roundList = new ArrayList<>();

        // 在线数
        long onlineDevices = devices.stream().filter(e -> DeviceStatus.ONLINE.equals(e.getHwStatus())).count();
        // 离线数
        long offlineDevices = devices.stream().filter(e -> DeviceStatus.OFFLINE.equals(e.getHwStatus())).count();
        // 其他数
        long otherDevices = devices.size() - onlineDevices - offlineDevices;

        // 在线
        roundList.add(FullViewRoundResp.builder().type(DeviceStatus.ONLINE.getName()).value(Integer.valueOf(onlineDevices + "")).build());
        // 离线
        roundList.add(FullViewRoundResp.builder().type(DeviceStatus.OFFLINE.getName()).value(Integer.valueOf(offlineDevices + "")).build());
        // 其他
        roundList.add(FullViewRoundResp.builder().type(DeviceStatus.OTHER.getName()).value(Integer.valueOf(otherDevices + "")).build());

        projects.forEach(e -> {
            // 项目设备数
            long totalProjectDevices = devices.stream().filter(on -> e.getId().equals(Optional.ofNullable(on.getProject()).map(Project::getId).orElse(""))).count();
            // 项目在线数
            long onlineProjectDevices = devices.stream().filter(on -> DeviceStatus.ONLINE.equals(on.getHwStatus()) &&
                    e.getId().equals(Optional.ofNullable(on.getProject()).map(Project::getId).orElse(""))).count();
            // 项目离线数
            long offlineProjectDevices = devices.stream().filter(off -> DeviceStatus.OFFLINE.equals(off.getHwStatus()) &&
                    e.getId().equals(Optional.ofNullable(off.getProject()).map(Project::getId).orElse(""))).count();
            // 项目设备数
            radioList.add(FullViewRadioResp.builder()
                    .type(e.getProjectName())
                    .value(Integer.valueOf(totalProjectDevices + ""))
                    .build());
            // 在线
            list.add(FactoryDeviceMsgResp.builder().name(e.getProjectName())
                    .type(DeviceStatus.OFFLINE.getName())
                    .value(Integer.valueOf(offlineProjectDevices + ""))
                    .build());
            // 离线
            list.add(FactoryDeviceMsgResp.builder().name(e.getProjectName())
                    .type(DeviceStatus.ONLINE.getName())
                    .value(Integer.valueOf(onlineProjectDevices + ""))
                    .build());
        });

        FullViewResp build = FullViewResp.builder()
                // 设备圆形
                .fullViewRound(roundList)
                // 项目信息列表
                .factoryDeviceMsgRespList(list)
                // 比率图
                .radio(radioList)
                .build();
        return ServiceResult.ok(build);
    }

    /**
     * 楼栋总览
     *
     * @param projectId
     * @param spaceId
     * @return
     */
    @Override
    public ServiceResult buildingView(String projectId, String spaceId) {
        //顶级空间
        Set<Space> spaces = spaceDao.findByProjectIdAndIsShowAndLevelNot(projectId, 1, 0);
        List<Space> spaceList = null;
        List<Space> spaceList1 = null;
        List<FactoryDeviceMsgResp> list = new ArrayList<>();
        List<FullViewRadioResp> radioList = new ArrayList<>();
        List<FullViewRoundResp> roundList = new ArrayList<>();
        if ("0".equals(spaceId)) {
            // 顶级
            spaceList = spaces.stream().filter(e -> e.getParent() == null).collect(Collectors.toList());
            spaceList1 = new ArrayList<>(spaces);

        } else {
            // 子集
            spaceList = spaces.stream().filter(e -> spaceId.equals(Optional.ofNullable(e.getParent()).map(Space::getId).orElse(""))).collect(Collectors.toList());

            Optional<Space> daoById = spaceDao.findById(spaceId);
            ArrayList<Space> children = new ArrayList<>();
            spaceList1 = treeMenuList(new ArrayList<>(spaces), spaceId, children);
            if (daoById.isPresent()) {
                spaceList1.add(daoById.get());
            }

        }

        List<IotDevice> spaceIdIn = iotDeviceDao.findAllBySpaceIdInAndDeviceTypeNot(spaceList1.stream().map(Space::getId).collect(Collectors.toList()), DeviceType.DOOR_PLATE);

        // 在线数
        long onlineDevices = spaceIdIn.stream().filter(e -> DeviceStatus.ONLINE.equals(e.getHwStatus())).count();
        // 离线数
        long offlineDevices = spaceIdIn.stream().filter(e -> DeviceStatus.OFFLINE.equals(e.getHwStatus())).count();
        // 其他数
        long otherDevices = spaceIdIn.size() - onlineDevices - offlineDevices;

        // 在线
        roundList.add(FullViewRoundResp.builder().type(DeviceStatus.ONLINE.getName()).value(Integer.valueOf(onlineDevices + "")).build());
        // 离线
        roundList.add(FullViewRoundResp.builder().type(DeviceStatus.OFFLINE.getName()).value(Integer.valueOf(offlineDevices + "")).build());
        // 其他
        roundList.add(FullViewRoundResp.builder().type(DeviceStatus.OTHER.getName()).value(Integer.valueOf(otherDevices + "")).build());

        spaceList.forEach(e -> {

            Optional<Space> daoById = spaceDao.findById(e.getId());
            ArrayList<Space> children = new ArrayList<>();
            List<Space> menuList = treeMenuList(new ArrayList<>(spaces), e.getId(), children);
            if (daoById.isPresent()) {
                menuList.add(daoById.get());
            }
            List<IotDevice> iotDevices = iotDeviceDao.findAllBySpaceIdInAndDeviceTypeNot(menuList.stream().map(Space::getId).collect(Collectors.toList()), DeviceType.DOOR_PLATE);


            // 项目设备数
            long totalProjectDevices = iotDevices.size();

            // 项目在线数
            long onlineProjectDevices = iotDevices.stream().filter(on -> DeviceStatus.ONLINE.equals(on.getHwStatus())).count();
            // 项目离线数
            long offlineProjectDevices = iotDevices.stream().filter(off -> DeviceStatus.OFFLINE.equals(off.getHwStatus())).count();

            // 项目设备数
            radioList.add(FullViewRadioResp.builder()
                    .type(e.getName())
                    .value(Integer.valueOf(totalProjectDevices + ""))
                    .build());
            // 在线
            list.add(FactoryDeviceMsgResp.builder().name(e.getName())
                    .type(DeviceStatus.OFFLINE.getName())
                    .value(Integer.valueOf(offlineProjectDevices + ""))
                    .build());
            // 离线
            list.add(FactoryDeviceMsgResp.builder().name(e.getName())
                    .type(DeviceStatus.ONLINE.getName())
                    .value(Integer.valueOf(onlineProjectDevices + ""))
                    .build());
        });

        FullViewResp build = FullViewResp.builder()
                // 设备圆形
                .fullViewRound(roundList)
                // 项目信息列表
                .factoryDeviceMsgRespList(list)
                // 比率图
                .radio(radioList)
                .build();
        return ServiceResult.ok(build);
    }

    /**
     * @param spaceId
     * @return
     * @description 楼层视图
     * @author tql
     * @date 21-11-15
     */
    @Override
    public ServiceResult floorView(String spaceId) {
        // 获取空间下子空间
        List<Space> spaceList = spaceDao.findAllByParentId(spaceId);
        if (spaceList.isEmpty()) {
            return ServiceResult.error("未找到数据信息");
        }
        List<SpaceListViewResp> list = new ArrayList<>();
        spaceList.forEach(e -> {
            List<SpaceDeviceListViewResp> deviceList = new ArrayList<>();
            // 获取空间下设备信息
            List<IotDevice> iotDevices = iotDeviceDao.findBySpaceId(e.getId());

            iotDevices.forEach(device ->
                deviceList.add(SpaceDeviceListViewResp.builder()
                        // id
                        .id(device.getId())
                        // 设备名
                        .name(device.getDeviceName())
                        // 设备类型
                        .deviceType(device.getDeviceType())
                        // 设备类型描述
                        .deviceTypeDesc(device.getDeviceType().getName())
                        // 设备状态
                        .hwStatus(device.getHwStatus())
                        .build())
            );
            SpaceListViewResp listViewResp = SpaceListViewResp.builder()
                    // id
                    .id(e.getId())
                    // 空间名
                    .name(e.getName())
                    // 位置
                    .position(Optional.ofNullable(e.getPosition()).orElse(""))
                    // 描述
                    .desc(Optional.ofNullable(e.getRemark()).orElse(""))
                    // 时间
                    .date(e.getCreatedTime().toLocalDate())
                    // 设备列表
                    .spaceDeviceListViewRespList(deviceList)
                    .build();
            list.add(listViewResp);

        });

        return ServiceResult.ok(FloorViewResp.builder().spaceListViewResps(list).build());
    }

    /**
     * 查询可以控制设备的员工列表
     *
     * @param deviceId 设备ID
     * @param keyword  查询关键字
     * @return 员工列表
     * @author Yang.Lee
     * @date 2021/11/15 22:07
     **/
    @Override
    @Transactional
    public List<StaffResp> getDeviceStaff(String deviceId, String keyword) {

        List<SpaceDeviceControl> deviceControlList = spaceDeviceControlDao.findAll((root, query, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();

            list.add(criteriaBuilder.equal(root.get("device").get("id"), deviceId));

            if (StringUtils.isNotBlank(keyword)) {
                List<SysUser> userList = sysUserDao.findAll((root1, query1, criteriaBuilder1) -> {

                    String like = "%" + keyword + "%";
                    return criteriaBuilder1.or(criteriaBuilder1.like(root1.get("nameZh"), like),
                            criteriaBuilder1.like(root1.get("empNo"), like),
                            criteriaBuilder1.like(root1.get("fullDepartmentName"), like));
                });

                list.add(root.get("userEmpNo").in(userList.stream().map(SysUser::getEmpNo).collect(Collectors.toList())));
            }

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        });

        return deviceControlList.stream().map(obj ->
                StaffResp.convert(sysUserService.getUserByEmpNo(obj.getUserEmpNo()))
        ).collect(Collectors.toList());
    }

    /**
     * 移除用户的设备控制权限
     *
     * @param deviceId 设备id
     * @param userHrId 用户工号
     * @return 操作结果
     * @author Yang.Lee
     * @date 2021/11/22 13:19
     **/
    @Override
    @Transactional
    public ServiceResult removeDeviceControlAccess(String deviceId, String userHrId) {

        Optional<SpaceDeviceControl> spaceDeviceControlOptional = spaceDeviceControlDao.findByUserEmpNoAndDeviceId(userHrId, deviceId);

        if (spaceDeviceControlOptional.isEmpty()) {
            return ServiceResult.error("操作失败，用户无设备操作权限");
        }

        SpaceDeviceControl spaceDeviceControl = spaceDeviceControlOptional.get();

        IotDevice device = spaceDeviceControl.getDevice();
        if (device.getSpace() != null) {

            // 判断用户是否是该空间下的管理员，管理员权限不可删除
            Optional<SpaceAdmin> spaceAdminOptional = spaceAdminDao.findBySpaceIdAndAdminEmpNo(device.getSpace().getId(), userHrId);
            if (spaceAdminOptional.isPresent()) {
                return ServiceResult.error("操作失败，该用户为空间管理员，无法删除权限");
            }
        }

        spaceDeviceControl.setIsDelete(1);
        spaceDeviceControlDao.save(spaceDeviceControl);

        if (device.getSpace() != null) {
            // 判断空间是否为智能办公室
            Optional<IntelligentOffice> intelligentOfficeOptional = intelligentOfficeDao.findBySpaceId(device.getSpace().getId());
            if (intelligentOfficeOptional.isPresent()) {
                // 判断该用户是否对办公室下其他设备有操作权限

                List<SpaceDeviceControl> spaceDeviceControlList = spaceDeviceControlDao.findByDeviceSpaceIdOrderByCreatedTimeDesc(device.getSpace().getId());
                if (spaceDeviceControlList.isEmpty()) {

                    // 判断该用户是否有其他智能会议室的权限
                    List<SpaceAdmin> spaceAdminList = spaceAdminDao.findByAdminEmpNo(userHrId);
                    Page<IntelligentOffice> intelligentOfficePage = intelligentOfficeDao.findBySpaceIdIn(spaceAdminList.stream()
                                    .map(SpaceAdmin::getSpace)
                                    .map(Space::getId)
                                    .collect(Collectors.toList()),
                            PageRequest.of(0, 9999));


                    if (intelligentOfficePage.getTotalElements() == 0) {
                        PassportRoleEditReq passportRoleEditReq = new PassportRoleEditReq();
                        passportRoleEditReq.setAddUserHrIdList(new ArrayList<>());
                        passportRoleEditReq.setRmUserHrIdList(Arrays.asList(userHrId));

                        ServiceResult serviceResult = spaceService.editUCIntelligentOfficeRole(device.getSpace().getId(), passportRoleEditReq);
                        if (!serviceResult.isSuccess()) {
                            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                            return serviceResult;
                        }
                    }
                }
            }
        }

        return ServiceResult.ok();
    }

    @Override
    public ServiceResult iotcToIbmsUpdDevice(DeviceResp deviceResp) {

        if (ObjectUtils.isEmpty(deviceResp)){
            return ServiceResult.error("设备锁定信息为空");
        }

        IotDevice iotDevice = new IotDevice();
        iotDevice.setDeviceType(deviceResp.getDeviceType());
        iotDevice.setDeviceName(deviceResp.getDeviceName());
        iotDevice.setDataSource(DataSource.HW_CLOUD);
        iotDevice.setIotNodeType(deviceResp.getIotNodeType());
        iotDevice.setStatus(1);
        iotDevice.setHwDeviceId(deviceResp.getHwDeviceId());
        iotDevice.setHwStatus(deviceResp.getWorkStatus());
        iotDevice.setLastHwStatusChangedDatetime(deviceResp.getChangeTime());

        if (ObjectUtils.isNotEmpty(deviceResp.getDeviceProperties())){
            iotDevice.setHwGatewayId(deviceResp.getDeviceProperties().getHwGatewayId());
            iotDevice.setHwDeviceName(deviceResp.getDeviceProperties().getHwDeviceName());
            iotDevice.setHwNodeType(deviceResp.getDeviceProperties().getHwNodeType());
            iotDevice.setHwProductId(deviceResp.getDeviceProperties().getHwProductId());
            iotDevice.setHwProductName(deviceResp.getDeviceProperties().getHwProductName());
        } else {
            return ServiceResult.error("设备属性为空");
        }

        iotDevice.setSn(deviceResp.getCode());

        //存入数据库
        iotDeviceDao.save(iotDevice);

        if (Optional.ofNullable(iotDevice.getDeviceType()).isPresent()) {
            // 根据设备类型创建数据
            switch (iotDevice.getDeviceType()) {
                case DOOR_MAGNETIC:

                    DoorMagnetic doorMagnetic = new DoorMagnetic();
                    doorMagnetic.setDevice(iotDevice);
                    doorMagnetic.setHuaweiId(iotDevice.getHwDeviceId());
                    doorMagnetic.setName(iotDevice.getDeviceName());
                    doorMagnetic.setNumber(RandomCode.getRandom(8));
                    doorMagnetic.setDoorMagneticSource(DoorMagneticSource.HW_CLOUD);
                    doorMagneticDao.save(doorMagnetic);
                    break;
                case AIR_CONDITIONER:

                    AirConditioner airConditioner = new AirConditioner();
                    airConditioner.setDevice(iotDevice);
                    airConditionerDao.save(airConditioner);
                    break;
                case CURTAINS:

                    Curtain curtain = new Curtain();
                    curtain.setDevice(iotDevice);
                    curtainDao.save(curtain);
                    break;
                default:
                    break;
            }
        }

        //判断设备是否是门磁
        if (DeviceType.DOOR_MAGNETIC.equals(deviceResp.getDeviceType())){

            //门磁设备存入缓存
            iotDeviceCacheService.doorMagneticRedis(iotDevice);
        }

        //判断设备是否是门牌
        if (DeviceType.DOOR_PLATE.equals(deviceResp.getDeviceType())){

            //门牌设备存入缓存
            iotDeviceCacheService.doorPlateRedis(iotDevice);
        }

        /*
         *  判断Redis中是否存在一级节点设备类型的list的key
         *  数据结构的设计为：
         *  key是env:device-iot
         *  value是一个List list中放 DeviceType对象
         */
        if (ObjectUtils.isNotEmpty(iotDevice.getDeviceType())){
            iotDeviceCacheService.redisDeviceTypeAdd(iotDevice);
        }

        /*
         *  判断Redis中是否存在二级节点设备ID的list的key
         *  数据结构的设计为：
         *  key是env:device-iot:AIR_CONDITIONER
         *  value是一个List list中放 DeviceDataInfo 对象
         *  DeviceDataInfo有 hwDeviceId 和 serviceId的关键字
         */
        if (ObjectUtils.isNotEmpty(iotDevice.getDeviceType()) && StringUtils.isNotEmpty(iotDevice.getHwProductId())) {
            iotDeviceCacheService.redisDeviceDataInfo(iotDevice);
        }


        return ServiceResult.ok();
    }

    /**
     * 移除设备的所有控制权限人员
     *
     * @param deviceId 设备ID
     * @return 操作结果
     * @author Yang.Lee
     * @date 2021/11/26 10:46
     **/
    @Override
    @Transactional
    public ServiceResult removeDeviceAllControlUsers(String deviceId) {

        List<SpaceDeviceControl> spaceDeviceControlList = spaceDeviceControlDao.findByDeviceId(deviceId);
        if (!spaceDeviceControlList.isEmpty()) {
            spaceDeviceControlList.forEach(obj -> obj.setIsDelete(1));
            spaceDeviceControlDao.saveAll(spaceDeviceControlList);
        }

        return ServiceResult.ok();
    }


    @Override
    public List<Space> getChildSpacesBySpaceId(String spaceId) {
        List<Space> spaceList = null;
        Optional<Space> byId = spaceDao.findById(spaceId);

        if (byId.isPresent()) {
            Space space = byId.get();
            Set<Space> spaces = spaceDao.findByProjectIdAndIsShowAndLevelNot(space.getProject().getId(), 1, 0);

            // 子集空间
            Optional<Space> daoById = spaceDao.findById(spaceId);
            ArrayList<Space> children = new ArrayList<>();
            spaceList = treeMenuList(new ArrayList<>(spaces), spaceId, children);
            if (daoById.isPresent()) {
                spaceList.add(daoById.get());
            }

        }
        return spaceList;
    }

    @Override
    @Transactional
    public void deviceStatusChangeMsgPush(DeviceStatusChangeMsgPushMessage parseObject) {

        log.error("设备状态变更消息推送");
        Optional<IotDevice> deviceOp = iotDeviceDao.findByHwDeviceId(parseObject.getDeviceHwId());
        if (deviceOp.isPresent()) {
            IotDevice device = deviceOp.get();
            skillPersonDao.findAll().forEach(skillPerson -> {

                WeChatMsg weChatMsg = new WeChatMsg();
                weChatMsg.setKeyword1(device.getHwDeviceName() + "状态变化");
                weChatMsg.setKeyword2(parseObject.getStatus().getName());
                weChatMsg.setKeyword3(DateUtils.formatLocalDateTime(LocalDateTime.now(), DateUtils.PATTERN_DATETIME));
                if (null != device.getSpace()) {
                    weChatMsg.setRemark(SpaceService.getFullSpaceName(device.getSpace(), device.getSpace().getName()));
                }
                log.info("设备状态发送变化消息推送" + device.getHwDeviceId());
                log.info("接收人: " + device.getHwDeviceName());

                // 设备状态变更消息推送
                weChatService.deviceStatusChangeMsgPush(skillPerson.getOpenId(), weChatMsg);

                /*SendMsgRequestVo sendMsgRequestVo = new SendMsgRequestVo();
                sendMsgRequestVo.setOpenId(skillPerson.getOpenId());
                sendMsgRequestVo.setTemplate(template);

                List<SendParams> sendParamsList = new ArrayList<>();
                SendParams sendParams = new SendParams();
                sendParams.setColor("#FF000000");
                sendParams.setVal(device.getHwDeviceName() + "状态变化");
                sendParams.setKey("keyword1");
                sendParamsList.add(sendParams);

                SendParams sendParams1 = new SendParams();
                if(DeviceStatus.ACTIVE.equals(parseObject.getStatus()) || DeviceStatus.ONLINE.equals(parseObject.getStatus())){
                    sendParams1.setColor("#FF00FF00");
                }else{
                    sendParams1.setColor("#FF0000");
                }
                sendParams1.setVal(parseObject.getStatus().getName());
                sendParams1.setKey("keyword2");
                sendParamsList.add(sendParams1);

                SendParams sendParams2 = new SendParams();
                sendParams2.setColor("#FF000000");
                sendParams2.setVal(DateUtils.formatLocalDateTime(LocalDateTime.now(), DateUtils.PATTERN_DATETIME));
                sendParams2.setKey("keyword3");
                sendParamsList.add(sendParams2);


                sendMsgRequestVo.setParams(sendParamsList);
                try {
                    restTemplate.postForEntity(sendMsgUrl, sendMsgRequestVo, String.class);
                    log.error("设备状态变更消息推送成功:" + device.getHwDeviceId());
                }catch (Exception e){
                    log.error("设备状态变更消息推送失败:" + device.getHwDeviceId());
                }*/
            });
        } else {
            log.error("设备不存在" + parseObject.getDeviceHwId());
        }
    }

    @Override
    public PageBean<HIKAccessRulePageResp> getFaceMachinePage(HIKFaceMachinePageReq req) {

        // 获取所有已绑定门禁的设备ID
        List<String> deviceIds = new ArrayList<>();
        accessControlSpotDao.findAll().forEach(it -> {
            if (null != it.getDevice()) {
                deviceIds.add(it.getDevice().getId());
            }
        });

        // 创建分页查询对象
        req.addOrderDesc(Sorts.CREATED_TIME);
        Sort sort = Sort.by(req.getOrders());
        Pageable pageable = PageRequest.of(req.getPage(), req.getSize(), sort);

        // 获取所有门磁 HK KS面板机设备
        // Page<IotDevice> devicePage = iotDeviceDao.findByDeviceTypeOrDeviceTypeOrDeviceType(DeviceType.DOOR_MAGNETIC, DeviceType.FACE_RECOGNITION, DeviceType.HIK_FACE_MACHINE, pageable);

        Page<IotDevice> devicePage = iotDeviceDao.findAll((root, criteriaQuery, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();
            // 根据空间ID
            if (StringUtils.isNotBlank(req.getSpaceId())) {
                list.add(criteriaBuilder.equal(root.get("space").get("id"), req.getSpaceId()));
            }

            // 面板机和门磁
            list.add(criteriaBuilder.or(
                    criteriaBuilder.equal(root.get("deviceType"), DeviceType.HIK_FACE_MACHINE),
                    criteriaBuilder.equal(root.get("deviceType"), DeviceType.FACE_RECOGNITION),
                    criteriaBuilder.equal(root.get("deviceType"), DeviceType.DOOR_MAGNETIC)
            ));

            if (!deviceIds.isEmpty()) {
                // 绑定的设备ID
                list.add(criteriaBuilder.not(criteriaBuilder.in(root.get("id")).value(deviceIds)));
            }

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, pageable);
        List<HIKAccessRulePageResp> hikAccessRulePageResps = new ArrayList<>();
        devicePage.getContent().forEach(obj -> hikAccessRulePageResps.add(convert(obj)));

        PageBean<HIKAccessRulePageResp> pageBean = new PageBean<>();
        pageBean.setList(hikAccessRulePageResps);
        pageBean.setPage(req.getPage());
        pageBean.setSize(req.getSize());
        pageBean.setTotalPages(devicePage.getTotalPages());
        pageBean.setTotalElements(devicePage.getTotalElements());
        return pageBean;
    }

    private DataSource getDataSourceFromIotc(String channel) {
        DataSource dataSource;
        switch (channel) {
            case "1":
                dataSource = DataSource.HK_CONNECTOR;
                break;
            case "2":
                dataSource = DataSource.MEGVII_CLOUD;
                break;
            case "3":
                dataSource = DataSource.HW_CLOUD;
                break;
            default:
                dataSource = DataSource.OTHIER;
        }
        return dataSource;
    }

//    /**
//     * 对象转换
//     *
//     * @param source 待转对象
//     * @return 转换结果
//     * @author Yang.Lee
//     * @date 2021/5/7 10:50
//     **/
//    public PageBean<IotDeviceNetworkChangeResp> convertIotDeviceNetworkChangeRespPage(Page<IotDeviceNetworkChange> source) {
//
//        PageBean<IotDeviceNetworkChangeResp> resultPage = new PageBean<>();
//
//        resultPage.setPage(source.getPageable().getPageNumber());
//        resultPage.setSize(source.getPageable().getPageSize());
//        resultPage.setTotalPages(source.getTotalPages());
//        resultPage.setTotalElements(source.getTotalElements());
//        resultPage.setList(convertIotDeviceNetworkChangeRespList(source.getContent()));
//
//        return resultPage;
//    }

    /**
     * 对象转换
     *
     * @param source 待转对象
     * @return 转换结果
     * @author Yang.Lee
     * @date 2021/5/7 10:50
     **/
    public List<IotDeviceNetworkChangeResp> convertIotDeviceNetworkChangeRespList(List<IotDeviceNetworkChange> source) {

        List<IotDeviceNetworkChangeResp> result = new ArrayList<>();
        if (source == null || source.isEmpty()) {
            return result;
        }

        source.forEach(obj -> result.add(convertIotDeviceNetworkChangeResp(obj)));

        return result;
    }

    /**
     * 对象转换
     *
     * @param source 待转对象
     * @return 转换结果
     * @author Yang.Lee
     * @date 2021/5/7 10:50
     **/
    public IotDeviceNetworkChangeResp convertIotDeviceNetworkChangeResp(IotDeviceNetworkChange source) {

        return IotDeviceNetworkChangeResp.builder()
                .deviceId(source.getDevice().getId())
                .deviceName(source.getDevice().getDeviceName())
                .deviceStatusName(source.getDeviceStatus().getName())
                .deviceStatus(source.getDeviceStatus())
                .sn(source.getDevice().getSn())
                .dataTime(source.getDataTime())
                .build();
    }

//
//    /**
//     * 生成设备sn码
//     *
//     * @param projectNo
//     * @param iotNodeType
//     * @param productNo
//     * @param maxDeviceNo
//     * @return
//     */
//    private String generateSnCode(String projectNo, IotNodeType iotNodeType, String productNo, Integer maxDeviceNo) {
//        String typeCode = "";
//        if (iotNodeType == IotNodeType.GATEWAY) {
//            typeCode = "01";
//        } else if (iotNodeType == IotNodeType.DIRECT_DEVICE) {
//            typeCode = "02";
//        } else if (iotNodeType == IotNodeType.GATEWAY_SUB_DEVICE) {
//            typeCode = "03";
//        }
//
//        return projectNo + typeCode + productNo + String.format("%04d", maxDeviceNo);
//    }

    /**
     * 获取某个父节点下面的所有子节点
     *
     * @param menuList
     * @param pid
     * @return
     */
    private List<Space> treeMenuList(List<Space> menuList, String pid, List<Space> childMenu) {
        for (Space mu : menuList) {
            // 遍历出父id等于参数的id，add进子节点集合
            if (mu.getParent() != null && pid.equals(mu.getParent().getId())) {
                // 递归遍历下一级
                childMenu.add(mu);
                treeMenuList(menuList, mu.getId(), childMenu);
            }
        }
        return childMenu;
    }

    /**
     * 设备空间变化后执行方法
     *
     * @param device   设备
     * @param oldSpace 旧空间
     * @param newSpace 新空间
     * @author Yang.Lee
     * @date 2022/1/4 18:14
     **/
    @Transactional
    public void deviceSpaceChange(IotDevice device, @Nullable Space oldSpace, Space newSpace) {

        if (oldSpace != null) {
            // 设备空间发生变化修改以下内容：
            // 1. 若设备所属旧空间为智能办公室，则删除设备映射表的数据，清除办公室模式中的该设备
            // 2. 旧空间中的设备控制权限清空
            Optional<IntelligentOffice> intelligentOfficeOptional = intelligentOfficeDao.findBySpaceId(oldSpace.getId());
            if (intelligentOfficeOptional.isPresent()) {

                IntelligentOffice office = intelligentOfficeOptional.get();

                List<IntelligentOfficeDevice> intelligentOfficeDeviceList =
                        intelligentOfficeDeviceDao.findByDeviceId(device.getId());

                // 删除设备映射表数据
                intelligentOfficeDeviceList.forEach(obj -> obj.setIsDelete(1));
                intelligentOfficeDeviceDao.saveAll(intelligentOfficeDeviceList);

                // 删除模式数据
                List<DeviceModeStatus> modeStatusList =
                        modeStatusDao.findByModeIntelligentOfficeIdAndDeviceId(office.getId(), device.getId());

                modeStatusList.forEach(obj -> obj.setIsDelete(1));
                modeStatusDao.saveAll(modeStatusList);

                // 删除权限数据
                List<SpaceDeviceControl> spaceDeviceControlList = spaceDeviceControlDao.findByDeviceId(device.getId());

                spaceDeviceControlList.forEach(obj -> obj.setIsDelete(1));
                spaceDeviceControlDao.saveAll(spaceDeviceControlList);
            }
        }

        // 1. 找到所有新空间有权限的用户
        List<IntelligentOfficeUser> intelligentOfficeUserList = intelligentOfficeUserDao.findByIntelligentOfficeSpaceId(newSpace.getId());

        // 向新空间中的用户添加 设备控制权限
        List<SpaceDeviceControl> spaceDeviceControlList = intelligentOfficeUserList.stream().map(obj -> {
            SpaceDeviceControl spaceDeviceControl = new SpaceDeviceControl();
            spaceDeviceControl.setDevice(device);
            spaceDeviceControl.setUserEmpNo(obj.getUserHrId());
            spaceDeviceControl.setUserName(obj.getUserName());
            spaceDeviceControl.setIsDelete(0);
            return spaceDeviceControl;
        }).collect(Collectors.toList());

        spaceDeviceControlDao.saveAll(spaceDeviceControlList);
    }

    @Override
    @Transactional
    public ServiceResult deleteBySn(String deviceSn) {
        log.info("start to delete device by sn, sn:{}", deviceSn);
        List<IotDevice> iotDeviceList = iotDeviceDao.findAllBySn(deviceSn);
        if (CollectionUtils.isEmpty(iotDeviceList)) {
            log.warn("device is not found by sn:{}", deviceSn);
            return ServiceResult.error("设备未找到");
        }
        iotDeviceList.forEach(device -> {
            //门禁点解绑
            Optional<AccessControlSpot> spotOptional = accessControlSpotDao.findByDeviceId(device.getId());
            if (spotOptional.isPresent()) {
                AccessControlSpot spot = spotOptional.get();
                spot.setDevice(null);
                spot.setAccessControlSpotType(null);
                accessControlSpotDao.save(spot);
            }
            //更改空间设备数
            if (device.getSpace() != null) {
                updateSpaceNum(device, spaceDao);
            }

            //设备-to-空间映射关系删除
            Optional<DeviceToSpace> deviceToSpaceOptional = deviceToSpaceDao.findByDeviceId(device.getId());
            if (deviceToSpaceOptional.isPresent()) {
                DeviceToSpace deviceToSpace = deviceToSpaceOptional.get();
                deviceToSpace.setIsDelete(1);
                deviceToSpaceDao.save(deviceToSpace);
            }

            //空调数据删除
            if (DeviceType.AIR_CONDITIONER.equals(device.getDeviceType())) {
                Optional<AirConditioner> airConditionerOptional = airConditionerDao.findByDevice(device);
                if (airConditionerOptional.isPresent()) {
                    AirConditioner airConditioner = airConditionerOptional.get();
                    airConditioner.setIsDelete(1);
                    airConditionerDao.save(airConditioner);
                }
            } else if (DeviceType.CURTAINS.equals(device.getDeviceType())) {
                //窗帘数据删除
                Optional<Curtain> curtainOptional = curtainDao.findByDevice(device);
                if (curtainOptional.isPresent()) {
                    Curtain curtain = curtainOptional.get();
                    curtain.setIsDelete(1);
                    curtainDao.save(curtain);
                }
            } else if (DeviceType.DOOR_MAGNETIC.equals(device.getDeviceType())) {
                //门磁删除
                Optional<DoorMagnetic> doorMagneticOptional = doorMagneticDao.findByDeviceId(device.getId());
                if (doorMagneticOptional.isPresent()) {
                    DoorMagnetic doorMagnetic = doorMagneticOptional.get();
                    doorMagnetic.setIsDelete(1);
                    doorMagneticDao.save(doorMagnetic);
                }
            } else if (DeviceType.DOOR_PLATE.equals(device.getDeviceType())) {
                //门牌删除
                Optional<DoorPlate> doorPlateOptional = doorPlateDao.findByDeviceId(device.getId());
                if (doorPlateOptional.isPresent()) {
                    DoorPlate doorPlate = doorPlateOptional.get();
                    doorPlate.setIsDelete(1);
                    doorPlateDao.save(doorPlate);
                }
            } else if (DeviceType.ILLUMINATION_SENSING.equals(device.getDeviceType())) {
                //光照度传感器设备删除
                Optional<IlluminationSensing> illuminationSensingOptional = illuminationSensingDao.findByDeviceId(device.getId());
                if (illuminationSensingOptional.isPresent()) {
                    IlluminationSensing illuminationSensing = illuminationSensingOptional.get();
                    illuminationSensing.setIsDelete(1);
                    illuminationSensingDao.save(illuminationSensing);
                }
            } else if (DeviceType.CAMERA.equals(device.getDeviceType())) {
                //萤石云视频摄像头删除
                YSVideoCamera ysVideoCamera = ysVideoCameraDao.findByDeviceId(device.getId());
                if (Objects.nonNull(ysVideoCamera)) {
                    ysVideoCamera.setIsDelete(1);
                    ysVideoCameraDao.save(ysVideoCamera);
                }
            }
            //智能断路器删除
            Optional<IntelligentCircuitBreaker> intelligentCircuitBreakerOptional = intelligentCircuitBreakerDao.findByDeviceId(device.getId());
            if (intelligentCircuitBreakerOptional.isPresent()) {
                IntelligentCircuitBreaker intelligentCircuitBreaker = intelligentCircuitBreakerOptional.get();
                intelligentCircuitBreaker.setIsDelete(1);
                intelligentCircuitBreakerDao.save(intelligentCircuitBreaker);
            }

            //设备上下线信息删除
            List<IotDeviceNetworkChange> iotDeviceNetworkChangeList = iotDeviceNetworkChangeDao.findAllByDeviceId(device.getId());
            if (!CollectionUtils.isEmpty(iotDeviceNetworkChangeList)) {
                iotDeviceNetworkChangeList.forEach(iotDeviceNetworkChange -> iotDeviceNetworkChange.setIsDelete(1));
                iotDeviceNetworkChangeDao.saveAll(iotDeviceNetworkChangeList);
            }
            //视频监控点表删除
            VideoMonitorSpot videoMonitorSpot = videoMonitorSpotDao.getByDeviceId(device.getId());
            if (Objects.nonNull(videoMonitorSpot)) {
                videoMonitorSpot.setIsDelete(1);
                videoMonitorSpotDao.save(videoMonitorSpot);
            }
            //警戒抓拍解绑设备
            List<AlertCaptureor> alertCaptureorList = alertCaptureorDao.findAllByDeviceId(device.getId());
            if (!CollectionUtils.isEmpty(alertCaptureorList)) {
                alertCaptureorList.forEach(alertCaptureor -> alertCaptureor.setDevice(null));
                alertCaptureorDao.saveAll(alertCaptureorList);
            }
            //警戒点解绑设备
            List<AlertSpot> alertSpotList = alertSpotDao.findAllByDeviceId(device.getId());
            if (!CollectionUtils.isEmpty(alertSpotList)) {
                alertSpotList.forEach(alertSpot -> alertSpot.setDevice(null));
                alertSpotDao.saveAll(alertSpotList);
            }

            //设备通行记录删除
            List<DeviceAccessRecord> deviceAccessRecordList = deviceAccessRecordDao.findAllByIotDeviceId(device.getId());
            if (!CollectionUtils.isEmpty(deviceAccessRecordList)) {
                deviceAccessRecordList.forEach(deviceAccessRecord -> deviceAccessRecord.setIsDelete(1));
                deviceAccessRecordDao.saveAll(deviceAccessRecordList);
            }
            //设备控制记录删除
            List<DeviceControlLog> deviceControlLogList = deviceControlLogDao.findAllByDeviceId(device.getId());
            if (!CollectionUtils.isEmpty(deviceControlLogList)) {
                deviceControlLogList.forEach(deviceControlLog -> deviceControlLog.setIsDelete(1));
                deviceControlLogDao.saveAll(deviceControlLogList);
            }
            //设备群组删除
            List<DeviceGroupItem> deviceGroupItemList = deviceGroupItemDao.findAllByIotDeviceId(device.getId());
            if (!CollectionUtils.isEmpty(deviceGroupItemList)) {
                deviceGroupItemList.forEach(deviceGroupItem -> deviceGroupItem.setIsDelete(1));
                deviceGroupItemDao.saveAll(deviceGroupItemList);
            }
            //设备联动关系删除
            List<DeviceLinkRelation> deviceLinkRelationList = deviceLinkRelationDao.findAllByIotDeviceId(device.getId());
            if (!CollectionUtils.isEmpty(deviceLinkRelationList)) {
                deviceLinkRelationList.forEach(deviceLinkRelation -> deviceLinkRelation.setIsDelete(1));
                deviceLinkRelationDao.saveAll(deviceLinkRelationList);
            }
            //hk海康设备删除
            Optional<HkDevice> hkDeviceOptional = hkDeviceDao.findByDeviceId(device.getId());
            if (hkDeviceOptional.isPresent()) {
                HkDevice hkDevice = hkDeviceOptional.get();
                hkDevice.setIsDelete(1);
                hkDeviceDao.save(hkDevice);
            }
            device.setIsDelete(1);
            iotDeviceDao.save(device);
        });
        return ServiceResult.ok("删除设备成功");
    }

    @Override
    public PageBean<HIKAccessRulePageResp> faceDevice(HIKFaceMachinePageReq req) {

        // 获取所有已绑定楼梯点的设备ID
        List<String> deviceIds = new ArrayList<>();
        elevatorSpotDeviceRelationDao.findAll().forEach(it -> {
            if (!deviceIds.contains(it.getDevice().getId())) {
                deviceIds.add(it.getDevice().getId());
            }
        });

        // 创建分页查询对象
        req.addOrderDesc(Sorts.CREATED_TIME);
        Sort sort = Sort.by(req.getOrders());
        Pageable pageable = PageRequest.of(req.getPage(), req.getSize(), sort);

        Page<IotDevice> devicePage = iotDeviceDao.findAll((root, criteriaQuery, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();

            // 项目ID
            if (StringUtils.isNotBlank(req.getProjectId())) {
                list.add(criteriaBuilder.equal(root.get("project").get("id"), req.getProjectId()));
            }

            // 根据空间ID
            if (StringUtils.isNotBlank(req.getSpaceId())) {
                list.add(criteriaBuilder.equal(root.get("space").get("id"), req.getSpaceId()));
            }

            // 面板机
            list.add(criteriaBuilder.or(
                    criteriaBuilder.equal(root.get("deviceType"), DeviceType.HIK_FACE_MACHINE),
                    criteriaBuilder.equal(root.get("deviceType"), DeviceType.FACE_RECOGNITION)
            ));

            // 搜索关键字， SN码 / 设备名称
            if (StringUtils.isNotEmpty(req.getKeyword1())) {
                // 设备ID
                String like = "%" + req.getKeyword1() + "%";
                list.add(criteriaBuilder.or(criteriaBuilder.like(root.get("sn"), like), criteriaBuilder.like(root.get("deviceName"), like)));
            }

            if (!deviceIds.isEmpty()) {
                // 绑定的设备ID
                list.add(criteriaBuilder.not(criteriaBuilder.in(root.get("id")).value(deviceIds)));
            }
            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, pageable);
        List<HIKAccessRulePageResp> hikAccessRulePageResps = new ArrayList<>();
        devicePage.getContent().forEach(obj -> hikAccessRulePageResps.add(convert(obj)));
        PageBean<HIKAccessRulePageResp> pageBean = new PageBean<>();
        pageBean.setList(hikAccessRulePageResps);
        pageBean.setPage(req.getPage());
        pageBean.setSize(req.getSize());
        pageBean.setTotalPages(devicePage.getTotalPages());
        pageBean.setTotalElements(devicePage.getTotalElements());
        return pageBean;
    }

    /**
     * @param place 根据地区获取旷视鸿图报餐设备id
     * @return
     * @description
     * @author tql
     * @date 22-5-19
     */
    @Override
    public List<String> getKsHtPlaceDevice(AccessControlSpotPlace place) {
        List<String> list = new ArrayList<>();
        List<OpenAbilityAccessControlSpot> ability = openAbilityAccessControlSpotDao.findByOpenAbilityAbilityAndSpotAccessControlSpotPlace(Ability.MEAL, place);
        if (!CollectionUtils.isEmpty(ability)) {
            ability.forEach(obj -> {
                if (obj.getSpot() != null && obj.getSpot().getDevice() != null && IotDeviceService.isKSHTDevice(obj.getSpot().getDevice())) {
                    list.add(obj.getSpot().getDevice().getSn());
                }
            });
        }
        return list;
    }

    @Override
    public PageBean<DeviceControlLogResp> getDeviceControlLogPage(DeviceControlLogReq req) {

        PageBean<DeviceControlLogResp> pageBean = new PageBean<>();
        List<DeviceControlLogResp> list = new ArrayList<>();

        // 发送Http请求
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<IotDevice> requestGet = new HttpEntity<>(headers);

        StringBuilder url = new StringBuilder();
        url.append(tsdbUrl).append("?page=").append(req.getPage() + 1).append("&size=").append(req.getSize());
        if(StringUtils.isNotEmpty(req.getKeyword())){
            url.append("&keyword=").append(req.getKeyword());
        }
        if(null != req.getDeviceControlStatus()){
            url.append("&deviceControlStatus=").append(req.getDeviceControlStatus());
        }
        if(null != req.getStartTime()){
            url.append("&startTime=").append(req.getStartTime().toLocalDate() + " " + req.getStartTime().toLocalTime());
        }
        if(null != req.getEndTime()){
            url.append("&endTime=").append(req.getEndTime().toLocalDate() + " " + req.getEndTime().toLocalTime());
        }

        ResponseEntity<String> response;
        try {
            response = restTemplate.exchange(url.toString(), HttpMethod.GET, requestGet, String.class);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            pageBean.setList(list);
            return pageBean;
        }
        String body = response.getBody();
        // 解析数据
        JSONObject jsonObject = JSON.parseObject(body);
        JSONObject jsonData = jsonObject.getJSONObject("data");
        pageBean.setPage(jsonData.getInteger("currentPage"));
        pageBean.setSize(jsonData.getInteger("pageSize"));
        pageBean.setTotalPages(jsonData.getInteger("totalPages"));
        pageBean.setTotalElements(jsonData.getInteger("totalCount"));
        list = JSON.parseArray(jsonData.getJSONArray("list").toJSONString(), DeviceControlLogResp.class);
        pageBean.setList(list);

        return pageBean;
    }

    /**
     * device转换为HIKAccessRulePageResp
     *
     * @param device
     * @return
     */
    private HIKAccessRulePageResp convert(IotDevice device) {
        HIKAccessRulePageResp resp = new HIKAccessRulePageResp();
        resp.setDeviceName(device.getDeviceName());
        resp.setDeviceAddress(Optional.ofNullable(device.getAddress()).orElse(""));
        resp.setSn(Optional.ofNullable(device.getSn()).orElse(""));
        resp.setProjectName(Optional.ofNullable(device.getProject()).map(Project::getProjectName).orElse(""));
        resp.setSpaceName(Optional.ofNullable(device.getSpace()).map(space -> SpaceService.getFullSpaceName(space, space.getName())).orElse(""));
        resp.setDeviceSourceName(Optional.ofNullable(device.getDataSource()).map(DataSource::getName).orElse(""));
        resp.setDeviceId(device.getId());
        resp.setDeviceTypeName(Optional.ofNullable(device.getDeviceType()).map(DeviceType::getName).orElse(""));
        return resp;
    }

    @Override
    public IotDevice getIotDeviceFirstFromRedis(String id) {
        IotDevice iotDevice;
        String deviceKey = redisUtil.createDeviceIdKey(IConst.HW_DEVICE_MODULE, id, RedisUtil.Separator.COLON);
        if (redisUtil.hasKey(deviceKey)) {
            Map<Object, Object> map = redisUtil.hmget(deviceKey);
            iotDevice = DataUtils.mapCastTargetObject(map, IotDevice.class);
            if (Objects.nonNull(iotDevice)) {
                iotDevice.setId(id);
            }
            log.info("从Redis中获取数据{}", iotDevice);
        } else {
            Optional<IotDevice> iotDeviceOptional = deviceDao.findById(id);
            iotDevice = iotDeviceOptional.orElse(null);
        }
        return iotDevice;
    }
}
