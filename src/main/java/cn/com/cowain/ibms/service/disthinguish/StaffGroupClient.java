package cn.com.cowain.ibms.service.disthinguish;


import cn.com.cowain.ibms.rest.req.distinguish.GroupExistReq;
import cn.com.cowain.ibms.rest.req.distinguish.UserGroupReq;
import cn.com.cowain.ibms.rest.req.distinguish.UserReq;

import java.util.ArrayList;

public interface StaffGroupClient {


    // 删除旷世人员组
    String deleteStaffGroup(String toString, String id);

    // 新增旷世人员组
    String save(String ks, UserGroupReq userGroupReq);

    // 修改人员
    String updateUser(String toString, ArrayList<UserReq> userReq);

    // 修改人员组
    String update(String toString, ArrayList<UserGroupReq> userGroupReq);

    // 判断该人员组是否存在
    String exist(GroupExistReq groupExistReq);
}
