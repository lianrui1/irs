package cn.com.cowain.ibms.feign.iotc.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2022/2/14 19:24
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateHTMemberGroupReq {

    private List<Param> groupList;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Param {
        private String id;

        private String name;

        private int type;
    }


}
