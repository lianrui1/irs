package cn.com.cowain.ibms.mq.bean;

import cn.com.cowain.ibms.enumeration.LogStatus;
import lombok.Data;

@Data
public class InterFaceLogMessage {


    private String type;
    /**
     * 入参
     */
    private String arg;


    /**
     * 日志编码
     */
    private String returnCode;


    /**
     * 返回
     */
    private String returnMsg;

    /**
     * 路径
     */
    private String url;


    /**
     * 消息时间
     */
    private String msgTime;

    /**
     * 日志信息来源
     */
    private String source;

    private LogStatus logStatus;

    private String returnData;

}

