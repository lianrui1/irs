package cn.com.cowain.ibms.exceptions;

/**
 * 远程服务不可用异常
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/7/20
 */
public class RemoteServiceUnavailableException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public RemoteServiceUnavailableException(String message) {
        super(message);
    }
}
