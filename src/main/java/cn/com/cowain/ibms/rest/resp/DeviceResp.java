package cn.com.cowain.ibms.rest.resp;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/6/22 10:40
 */
@Data
public class DeviceResp {


    @ApiModelProperty(value = "空调温度", position = 0)
    private Integer setTemp;

    @ApiModelProperty(value = "四路开关 on off", position = 1)
    private String light1;

    @ApiModelProperty(value = "四路开关 on off", position = 2)
    private String light2;

    @ApiModelProperty(value = "四路开关 on off", position = 3)
    private String light3;

    @ApiModelProperty(value = "四路开关 on off", position = 4)
    private String light4;

    @ApiModelProperty(value = "亮度", position = 5)
    private String luminance;

    @ApiModelProperty(value = "温度", position = 6)
    private String temperature;

    @ApiModelProperty(value = "湿度", position = 7)
    private String humidity;

    @ApiModelProperty(value = "噪音", position = 8)
    private String noise;

    @ApiModelProperty(value = "空气质量", position = 9)
    private String pm10;

    @ApiModelProperty(value = "空气质量", position = 10)
    private String pm2_5;

    @ApiModelProperty(value = "窗帘状态", position = 11)
    private String action;
}
