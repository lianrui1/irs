package cn.com.cowain.ibms.rest.resp.audio;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/1/6 10:16
 */
@Data
@ApiModel("会议音频记录详情")
public class AudioRecordRespPage {

    /**
     * 会议预约记录ID，外键
     */
    @ApiModelProperty(value = "会议id", required = true, position = 0)
    private String reservationRecordId;

    /**
     * 会议主题
     */
    @ApiModelProperty(value = "会议主题", position = 1)
    private String topic;

    /**
     * 预定日期
     */
    @ApiModelProperty(value = "预定日期", position = 2)
    private LocalDate date;

    /**
     * 从时间
     */
    @JsonFormat(pattern = "HH:mm")
    @ApiModelProperty(value = "从时间", position = 3)
    private LocalDateTime from;

    /**
     * 到时间
     */
    @JsonFormat(pattern = "HH:mm")
    @ApiModelProperty(value = "到时间", position = 4)
    private LocalDateTime to;

    /**
     * 名称
     */
    @ApiModelProperty(value = "会议室名称", position = 5)
    private String name;

    /**
     * 语音开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "语音开始时间", position = 6)
    private LocalDateTime startTime;

    /**
     * 语音结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "语音结束时间", position = 7)
    private LocalDateTime endTime;

    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键ID", required = true, position = 8)
    private String id;
}
