package cn.com.cowain.ibms.rest.resp.office;

import cn.com.cowain.ibms.enumeration.RuleEngineDetailEffectiveType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 生效方式
 *
 * @author: yanzy
 * @date: 2022/2/11 10:41
 */
@Data
@ApiModel("生效方式返回对象")
public class RuleEngineDetailEffectiveTypeResp {

    @ApiModelProperty(value = "生效方式")
    private RuleEngineDetailEffectiveType effectiveType;

    @ApiModelProperty(value = "定时时间")
    private RuleEngineDetailEffectiveTypeTimingTimeResp effectiveTypeTimingTimeReq;
}
