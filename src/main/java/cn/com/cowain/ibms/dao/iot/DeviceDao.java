package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.bo.SimpleDevice;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.enumeration.iot.DataSource;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * @author Yang.Lee
 * @date 2020/11/27 15:17
 */
@Repository
public interface DeviceDao extends JpaRepository<IotDevice, String>, JpaSpecificationExecutor<IotDevice> {

    /**
     * 根据空间ID查询该空间下的设备列表
     *
     * @param spaceId
     * @return
     */
    List<IotDevice> findAllBySpaceId(String spaceId);

    List<IotDevice> findAllBySpaceIdIn(List<String> spaceIds);

    @Query(
            value = "select new cn.com.cowain.ibms.bo.SimpleDevice(id, spaceControl) from IotDevice where space.id in (:spaceIds) and isDelete = 0"
    )
    List<SimpleDevice> findIdBySpaceIdIn(@Param("spaceIds") List<String> spaceIds);

    List<IotDevice> findBySpaceIdInAndStatus(List<String> spaceIds,Integer status);

    /**
     * 根据空间IDs查询该空间下的设备列表
     *
     * @param spaceIds
     * @return
     */
    List<IotDevice> findAllBySpaceIdInAndDeviceTypeNot(List<String> spaceIds,DeviceType deviceType);

    /**
     * 根据空间IDs查询该空间下对应类型的设备列表
     *
     * @param spaceIds
     * @param deviceTypes
     * @return
     */
    @Query(
            value = "select id, device_type from iot_device where space_id in (:spaceIds) and device_type in (:deviceTypes) and is_del = 0 order by created_time desc",
            nativeQuery = true
    )
    List<Object[]> findAllBySpaceIdInAndDeviceTypeIn(@Param("spaceIds") List<String> spaceIds,@Param("deviceTypes") List<String> deviceTypes);


    /**
     * 查询设备最大编号
     *
     * @return
     */
    @Query("SELECT MAX(iotDevice.deviceNo) FROM IotDevice AS iotDevice")
    Integer findMaxDeviceNo();

    /**
     * 查询最大排序
     *
     * @return
     */
    @Query("select max(iotDevice.seq) from IotDevice AS iotDevice")
    Integer findMaxSeq();

    /**
     * 根据空间ID查询设备列表
     *
     * @param spaceId 空间ID
     * @return 设备列表
     */
    List<IotDevice> findBySpaceId(String spaceId);

    /**
     * 根据华为云设备ID查询设备列表
     *
     * @return 设备
     */
    Optional<IotDevice> findByHwDeviceId(String id);

    // 根据华为云设备名称查询设备
    Optional<IotDevice> findByHwDeviceName(String hwDeviceName);

    /**
     * 根据项目ID查询设别列表
     *
     * @param projectId 项目ID
     * @return 设备列表
     * @author Yang.Lee
     * @date 2021/3/27 13:58
     **/
    List<IotDevice> findByProjectId(String projectId);


    /**
     * 根据产品ID查询设别列表
     *
     * @param productId 产品ID
     * @return 设备列表
     * @author Yang.Lee
     * @date 2021/3/27 14:23
     **/
    List<IotDevice> findByIotProductId(String productId);

    // 获取所有非直连设备的网关ID
    @Query("SELECT d.hwGatewayId FROM IotDevice AS d where d.hwNodeType = ?1  GROUP BY d.hwGatewayId")
    List<String> findByHwNodeTypeGroupByHwGatewayId(String endpoint);

    // 根据网关设备ID获取子设备信息
    List<IotDevice> findByHwNodeTypeAndHwGatewayId(String endpoint, String endpoint1);

    // 根据设备节点类型和网关id获取网关设备信息
    Optional<IotDevice> findByHwGatewayIdAndHwNodeType(String endpoint, String gateway);

    // 根据华为云产品id获取其下设备
    List<IotDevice> findByHwProductId(String id);

    // 根据华为云产品id获取其直连设备
    Optional<IotDevice> findByHwDeviceIdAndHwNodeType(String hwGatewayId, String gateway);

    /**
     * 查询空间下是否可以被用户手动控制的设别列表
     *
     * @param spaceId      空间ID
     * @param spaceControl 1；可以被控制的设备，0不可以被控制的设备
     * @return 设备列表
     * @author Yang.Lee
     * @date 2021/6/17 11:05
     **/
    List<IotDevice> findBySpaceIdAndSpaceControl(String spaceId, Integer spaceControl);

    List<IotDevice> findBySpaceIdInAndSpaceControl(List<String> spaceIds, Integer spaceControl);

    /**
     *
     * @author: yanzy
     * @param: [spaceId, spaceControl, status]
     * @data: 2022/2/7 10:02:26
     */
    List<IotDevice> findBySpaceIdAndSpaceControlAndStatusOrderByCreatedTimeDesc(String spaceId, Integer spaceControl,Integer status);

    List<IotDevice> findBySpaceIdInAndSpaceControlAndStatusOrderByCreatedTimeDesc(List<String> spaceIds, Integer spaceControl, Integer status);
    /**
     * 查询该空间下传感器设备
     *
     * @author: yanzy
     * @param: deviceType 设备属性（传感器属性）
     * @data: 2022/2/8 15:02:57
     */
    List<IotDevice> findBySpaceIdAndStatusAndDeviceTypeInOrderByCreatedTimeDesc(String spaceId,Integer status,List<DeviceType> deviceType);

    List<IotDevice> findBySpaceIdInAndStatusAndDeviceTypeInOrderByCreatedTimeDesc(List<String> spaceIds,Integer status,List<DeviceType> deviceTypes);

    List<IotDevice> findByDeviceType(DeviceType circuitBreaker);

    /**
     * @param circuitBreaker 设备类型
     * @return
     * @description 查询设备类型不等于
     * @author tql
     * @date 21-11-19
     */
    List<IotDevice> findByDeviceTypeNot(DeviceType circuitBreaker);

    // 根据空间和华为云ID获取设备信息
    Optional<IotDevice> findBySpaceIdAndHwDeviceId(String id, String id1);

    /**
     * 根据sn码查询
     *
     * @param sn sn码
     * @return 设备信息
     * @author Yang.Lee
     * @date 2021/10/27 17:06
     **/
    Optional<IotDevice> findBySn(String sn);

    // 获取对应空间下的人体传感器
    List<IotDevice> findBySpaceIdAndDeviceType(String id, DeviceType sensorBodySensing);

    List<IotDevice> findByDeviceTypeAndSpaceNotNull(DeviceType sensorBodySensing);

    // 获取所有门磁和面板机设备
    Page<IotDevice> findByDeviceTypeOrDeviceTypeOrDeviceType(DeviceType doorMagnetic, DeviceType faceRecognition, DeviceType hikFaceMachine, Pageable pageable);

    // 获取包含ID的设备
    List<IotDevice> findByIdIn(List<String> deviceIds);

    List<IotDevice> findBySpaceIdIn(Collection<String> spaceId);

    /**
     * 查询sn = {@code sn}的iotDevice
     * @param sn
     * @return
     */
    List<IotDevice> findAllBySn(String sn);

    /**
     * 根据来源查询设备列表
     *
     * @return 设备
     */
    List<IotDevice> findAllByDataSource(DataSource dataSource);

    /**
     * 查询集控码关联的设备
     *
     * @param centralizedControlCodeId
     * @return
     */
    @Query(
            value = "select i.id, i.device_type from iot_device i, ibms_centralized_code_device_relation c where i.id = c.device_id and " +
                    "c.centralized_control_code_id = :centralizedControlCodeId and i.is_del = 0 and c.is_del = 0 order by c.created_time desc",
            nativeQuery = true
    )
    List<Object[]> findAllByCentralizedControlCodeId(@Param("centralizedControlCodeId")String centralizedControlCodeId);

    /**
     * 分解设备id和设备类型查询
     *
     * @author: yanzy
     * @data: 2022/9/21 16:09:01
     */
    Optional<IotDevice> findByIdAndDeviceType(String id, DeviceType deviceType);
}
