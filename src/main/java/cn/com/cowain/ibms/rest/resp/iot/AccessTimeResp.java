package cn.com.cowain.ibms.rest.resp.iot;

import cn.com.cowain.ibms.entity.iot.AccessControlSpotUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author wei.cheng
 * @date 2022/03/17 17:25
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("用户对门禁点通行时间段")
public class AccessTimeResp {

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "分配通行开始时间", position = 1)
    private LocalDateTime accessStartTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "分配通行结束时间", position = 2)
    private LocalDateTime accessEndTime;

    public static AccessTimeResp convert(AccessControlSpotUser accessControlSpotUser){
        return AccessTimeResp.builder()
                .accessStartTime(accessControlSpotUser.getAccessStartTime())
                .accessEndTime(accessControlSpotUser.getAccessEndTime())
                .build();
    }
}
