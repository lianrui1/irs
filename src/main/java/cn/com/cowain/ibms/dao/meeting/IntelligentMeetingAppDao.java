package cn.com.cowain.ibms.dao.meeting;/**
 * Created with IntelliJ IDEA.
 *
 * @author: yanzy
 * @date: 2021/12/29/18:00
 * @version:
 */

import cn.com.cowain.ibms.entity.meeting.IntelligentMeetingApp;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * @author yanzy
 * @date 2021/12/29
 */
public interface IntelligentMeetingAppDao extends JpaRepository<IntelligentMeetingApp, String> {

    /**
     * @Param:
     */
    Optional<IntelligentMeetingApp> findBySerialNumber(String id);

    // 根据会议室ID获取会议大屏
    List<IntelligentMeetingApp> findByRoomId(String id);

    /**
     * 判断是否存在roomId = {@code roomId}的会议大屏
     *
     * @param roomId
     * @return
     */
    Boolean existsByRoomId(String roomId);
}
