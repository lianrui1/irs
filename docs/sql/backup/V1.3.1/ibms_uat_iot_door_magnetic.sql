-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 10.0.1.232    Database: ibms_uat
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `iot_door_magnetic`
--

DROP TABLE IF EXISTS `iot_door_magnetic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `iot_door_magnetic` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `created_by` varchar(36) DEFAULT NULL COMMENT '创建人',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(36) DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int(11) DEFAULT NULL COMMENT '版本号',
  `door_magnetic_mac` varchar(20) DEFAULT NULL COMMENT '门磁设备mac地址',
  `entity_id` varchar(36) NOT NULL COMMENT 'iot编号',
  `name` varchar(30) DEFAULT NULL COMMENT '名称',
  `number` varchar(50) NOT NULL COMMENT '编号',
  `device_id` varchar(36) NOT NULL COMMENT 'FK',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK9pmrehaunftbjp323iafk0npf` (`device_id`) USING BTREE,
  CONSTRAINT `FK9pmrehaunftbjp323iafk0npf` FOREIGN KEY (`device_id`) REFERENCES `iot_device` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='门磁';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `iot_door_magnetic`
--

LOCK TABLES `iot_door_magnetic` WRITE;
/*!40000 ALTER TABLE `iot_door_magnetic` DISABLE KEYS */;
INSERT INTO `iot_door_magnetic` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `door_magnetic_mac`, `entity_id`, `name`, `number`, `device_id`) VALUES ('1',NULL,'2020-12-30 20:06:47',0,NULL,'2020-12-30 20:06:52',1,'FCF5C48BA0F7','2d6c5330-3921-11eb-a2af-6ff688968db5','花桥','321','123');
INSERT INTO `iot_door_magnetic` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `door_magnetic_mac`, `entity_id`, `name`, `number`, `device_id`) VALUES ('2',NULL,'2020-12-30 20:07:05',0,NULL,'2020-12-30 20:07:07',1,'FCF5C48B643D','505133d0-3b55-11eb-a2af-6ff688968db5','昆山','fm21zu8v','ff566fe6-12ed-4eed-816f-37d664602dcf');
/*!40000 ALTER TABLE `iot_door_magnetic` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-25 15:53:41
