package cn.com.cowain.ibms.service.ability.impl;

import cn.com.cowain.ibms.dao.ability.OpenAbilityAccessControlSpotDao;
import cn.com.cowain.ibms.dao.ability.OpenAbilityDao;
import cn.com.cowain.ibms.dao.ability.OpenAbilityTaskDao;
import cn.com.cowain.ibms.dao.ability.OpenAbilityTaskDetailDao;
import cn.com.cowain.ibms.dao.iot.AccessControlSpotDao;
import cn.com.cowain.ibms.dao.iot.AccessControlSpotKSHTMemberGroupDao;
import cn.com.cowain.ibms.dao.iot.AccessControlSpotUserDao;
import cn.com.cowain.ibms.dao.meal.KshtTimePlanDao;
import cn.com.cowain.ibms.dao.meal.MealGroupDao;
import cn.com.cowain.ibms.dao.meal.MealPersonDao;
import cn.com.cowain.ibms.entity.ability.OpenAbility;
import cn.com.cowain.ibms.entity.ability.OpenAbilityAccessControlSpot;
import cn.com.cowain.ibms.entity.ability.OpenAbilityTask;
import cn.com.cowain.ibms.entity.ability.OpenAbilityTaskDetail;
import cn.com.cowain.ibms.entity.iot.AccessControlSpot;
import cn.com.cowain.ibms.entity.iot.AccessControlSpotKSHTMemberGroup;
import cn.com.cowain.ibms.entity.iot.AccessControlSpotUser;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.meal.KsHtTimePlan;
import cn.com.cowain.ibms.entity.meal.MealGroup;
import cn.com.cowain.ibms.entity.meal.MealPerson;
import cn.com.cowain.ibms.enumeration.UserImportStatus;
import cn.com.cowain.ibms.enumeration.ability.Ability;
import cn.com.cowain.ibms.enumeration.ability.AbilityTaskStatus;
import cn.com.cowain.ibms.enumeration.ability.MethodType;
import cn.com.cowain.ibms.enumeration.ability.TaskType;
import cn.com.cowain.ibms.enumeration.iot.AccessControlInOut;
import cn.com.cowain.ibms.enumeration.iot.AccessControlSpotPlace;
import cn.com.cowain.ibms.enumeration.iot.KSHTMemberGroupType;
import cn.com.cowain.ibms.enumeration.iot.PersonType;
import cn.com.cowain.ibms.enumeration.meal.MealStatus;
import cn.com.cowain.ibms.exceptions.BizLogicNotMatchException;
import cn.com.cowain.ibms.feign.iotc.bean.CreateHTMemberGroupReq;
import cn.com.cowain.ibms.mq.Topic;
import cn.com.cowain.ibms.mq.bean.AccessPermissions;
import cn.com.cowain.ibms.mq.bean.BizReturnMessage;
import cn.com.cowain.ibms.mq.bean.OpenAbilityMessage;
import cn.com.cowain.ibms.mq.bean.UserBindMessage;
import cn.com.cowain.ibms.mq.producer.VisitorMessageReturnProducer;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.Sorts;
import cn.com.cowain.ibms.rest.req.ability.AbilityReq;
import cn.com.cowain.ibms.rest.req.iot.AccessControlSpotUserReq;
import cn.com.cowain.ibms.rest.req.iot.PermisssionReq;
import cn.com.cowain.ibms.rest.resp.EquipmentDataResp;
import cn.com.cowain.ibms.rest.resp.UploadResp;
import cn.com.cowain.ibms.rest.resp.ability.AbilityDetailResp;
import cn.com.cowain.ibms.rest.resp.ability.AbilityNodeResp;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.UploadService;
import cn.com.cowain.ibms.service.ability.AbilityService;
import cn.com.cowain.ibms.service.ability.IotcConnectService;
import cn.com.cowain.ibms.service.bean.ServiceMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.iot.AccessControlSpotService;
import cn.com.cowain.ibms.service.iot.AccessControlSpotUserService;
import cn.com.cowain.ibms.service.iotc.IOTCService;
import cn.com.cowain.ibms.service.meal.MealService;
import cn.com.cowain.ibms.service.space.IotDeviceService;
import cn.com.cowain.ibms.service.space.SpaceService;
import cn.com.cowain.ibms.utils.*;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.annotation.Nullable;
import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 开放能力相关Service实现类
 *
 * @author Yang.Lee
 * @date 2022/2/21 14:45
 **/
@Slf4j
@Service
public class AbilityServiceImpl implements AbilityService {

    @Resource
    private IotcConnectService iotcConnectService;
    @Resource
    private OpenAbilityDao openAbilityDao;
    @Resource
    private AccessControlSpotDao accessControlSpotDao;
    @Resource
    private OpenAbilityAccessControlSpotDao openAbilityAccessControlSpotDao;

    @Value("${iotc.timeplan:e62817832b764a088f6d52033f9c9db1}")
    private String timePlan;

    @Resource
    private IOTCService iotcService;

    @Resource
    private OpenAbilityTaskDao openAbilityTaskDao;

    @Resource
    private OpenAbilityTaskDetailDao openAbilityTaskDetailDao;

    @Resource
    private AccessControlSpotService accessControlSpotService;

    @Resource
    private AccessControlSpotKSHTMemberGroupDao accessControlSpotKSHTMemberGroupDao;

    @Resource
    private VisitorMessageReturnProducer visitorMessageReturnProducer;

    @Resource
    private AccessControlSpotUserDao accessControlSpotUserDao;

    @Resource
    private RedisUtil redisUtil;

    @Resource
    private MealGroupDao mealGroupDao;

    @Resource
    private MealPersonDao mealPersonDao;

    @Resource
    private KshtTimePlanDao kshtTimePlanDao;

    @Resource
    private AccessControlSpotUserService accessControlSpotUserService;

    @Resource
    private MealService mealService;

    @Resource
    private RestTemplateUtil restTemplateUtil;

    @Resource
    private UploadService uploadService;

    @Override
    public ServiceResult deleteBindAbility(String id) {
        //1查出所有绑定信息 2剔除请求一致的绑定信息 3保存本地 4 发送给iotc
        Optional<OpenAbilityAccessControlSpot> accSpot = openAbilityAccessControlSpotDao.findById(id);
        if (accSpot.isEmpty()) {
            return ServiceResult.error("未找到当前分组与绑定信息");
        }

        OpenAbilityAccessControlSpot openAbilityAccessControlSpot = accSpot.get();

        openAbilityAccessControlSpot.setIsDelete(1);
        openAbilityAccessControlSpotDao.save(openAbilityAccessControlSpot);

        AccessControlSpot spot = openAbilityAccessControlSpot.getSpot();
        if (!Ability.ATTENDANCE.equals(openAbilityAccessControlSpot.getOpenAbility().getAbility()) && IotDeviceService.isKSHTDevice(spot.getDevice())) {
            // 找到员工组列表，删除所有员工组

            List<AccessControlSpotKSHTMemberGroup> memberGroupList = accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotIdAndOpenAbilityAbility(spot.getId(), openAbilityAccessControlSpot.getOpenAbility().getAbility());
            memberGroupList.forEach(obj -> {
                iotcService.removeStaffHTMemberGroup(obj.getId());
                obj.setIsDelete(1);
            });

            accessControlSpotKSHTMemberGroupDao.saveAll(memberGroupList);
        }
        return ServiceResult.ok();
    }

    @Override
    public PageBean<AbilityNodeResp> getAccessSpotController(AbilityReq req, Boolean hasBinding) {
        //封装and语句
        List<Predicate> listAnd = new ArrayList<>();
        //封装or语句
        List<Predicate> listOr = new ArrayList<>();
        // 查询智能办公列表分页
        Sort sort = Sort.by(Sort.Direction.DESC, Sorts.CREATED_TIME);
        Pageable pageable = PageRequest.of(req.getPage(), req.getSize(), sort);
        Page<AccessControlSpot> officePage = accessControlSpotDao.findAll((root, criteriaQuery, criteriaBuilder) -> {
            if (StringUtils.isNotEmpty(req.getBasicId())) {
                List<OpenAbilityAccessControlSpot> accessControlSpots = openAbilityAccessControlSpotDao.findByOpenAbilityId(req.getBasicId());
                if (!CollectionUtils.isEmpty(accessControlSpots)) {
                    List<String> ids = accessControlSpots.stream().map(OpenAbilityAccessControlSpot::getSpot).map(AccessControlSpot::getId).collect(Collectors.toList());
                    // ids查询
                    CriteriaBuilder.In<Object> inInitiator = criteriaBuilder.in(root.get("id"));
                    ids.forEach(inInitiator::value);
                    if (!hasBinding) {
                        //查询未绑定当前能力的设备
                        listAnd.add(criteriaBuilder.not(criteriaBuilder.or(criteriaBuilder.and(inInitiator))));
                    } else {
                        listAnd.add(criteriaBuilder.or(criteriaBuilder.and(inInitiator)));
                    }
                }
            }
            // 根据名字获取
            if (StringUtils.isNotBlank(req.getAccessControlName())) {
                String like = "%" + req.getAccessControlName() + "%";
                listAnd.add(criteriaBuilder.like(root.get("name"), like));
            }
            //设备不能为空
            listAnd.add(criteriaBuilder.isNotNull(root.get("device")));
            // 根据左侧树 分页查询
            if (StringUtils.isNotBlank(req.getSpaceId())) {
                listOr.add(criteriaBuilder.equal(root.get("space").get("id"), req.getSpaceId()));
                listOr.add(criteriaBuilder.equal(root.get("space").get("parent").get("id"), req.getSpaceId()));
            }
            // 根据左侧树 分页查询
            if (StringUtils.isNotBlank(req.getProjectId())) {
                listOr.add(criteriaBuilder.equal(root.get("space").get("project").get("id"), req.getProjectId()));
            }
            Predicate[] array_and = new Predicate[listAnd.size()];
            Predicate Pre_And = criteriaBuilder.and(listAnd.toArray(array_and));
            Predicate[] array_or = new Predicate[listOr.size()];
            Predicate Pre_Or = criteriaBuilder.or(listOr.toArray(array_or));
            if (!CollectionUtils.isEmpty(listOr)) {
                return criteriaQuery.where(Pre_And, Pre_Or).getRestriction();
            } else {
                return criteriaQuery.where(Pre_And).getRestriction();
            }
        }, pageable);
        List<AbilityNodeResp> result = new ArrayList();
        officePage.getContent();
        if (!CollectionUtils.isEmpty(officePage.getContent())) {
            for (AccessControlSpot resp : officePage.getContent()) {
                AbilityNodeResp resultEm = new AbilityNodeResp(resp);
                if (hasBinding) {
                    Optional<OpenAbilityAccessControlSpot> openAbilityAccessControlSpot = openAbilityAccessControlSpotDao.findByOpenAbilityIdAndSpotId(req.getBasicId(), resultEm.getId());
                    if (openAbilityAccessControlSpot.isEmpty()) {
                        continue;
                    }
                    OpenAbilityAccessControlSpot spot = openAbilityAccessControlSpot.get();
                    resultEm.setId(openAbilityAccessControlSpot.get().getId());
                    resultEm.setAddres(spot.getSpot().getSpace().getProject().getProjectName() + " / " + SpaceService.getFullSpaceName(spot.getSpot().getSpace(), spot.getSpot().getSpace().getName()) + " / " + resultEm.getAddres());
                }
                result.add(resultEm);
            }
        }
        PageBean<AbilityNodeResp> officeRespPage = new PageBean<>();
        officeRespPage.setList(result);
        officeRespPage.setTotalPages(officePage.getTotalPages());
        officeRespPage.setTotalElements(officePage.getTotalElements());
        officeRespPage.setPage(pageable.getPageNumber());
        officeRespPage.setSize(pageable.getPageSize());
        return officeRespPage;
    }

    @Override
    public ServiceResult findDetail(String id) {
        AbilityDetailResp resp = new AbilityDetailResp();
        Optional<OpenAbility> result = openAbilityDao.findById(id);
        if (result.isPresent()) {
            resp.setId(result.get().getId());
            resp.setDescribe(result.get().getDescribe());
            resp.setName(result.get().getAbility().getName());
            return ServiceResult.ok(resp);
        }
        return ServiceResult.error("未找到对应的能力");
    }

    /**
     * 处理开放能力消息
     *
     * @param message 消息
     * @return 处理结果
     * @author Yang.Lee
     * @date 2022/1/26 16:23
     **/
    @Override
    @Transactional
    public ServiceResult process(OpenAbilityMessage message) {

        ServiceResult result;
        switch (message.getAbility()) {
            case MEAL:
                result = meal(message);
                break;
            case LEAVE:
                result = leave(message);
                break;
            case VISITOR:
                result = visitor(message);
                break;
            case ON_BOARDING:
                result = onBoarding(message);
                break;
            case UPDATE_FACE:
                result = updateFace(message);
                break;
            default:
                log.error("开放能力未进行数据处理，{}", message);
                result = ServiceResult.error("未找到开放能力");
        }

        return result;
    }

    /**
     * 更新人脸
     *
     * @param abilityMessage 消息
     * @return 处理结果
     * @author Yang.Lee
     * @date 2022/3/7 13:53
     **/
    @Transactional
    public ServiceResult updateFace(OpenAbilityMessage abilityMessage) {
        log.debug("开始执行更新人脸逻辑");

        // 将入职用户分类，查出那些是需要根据部门和职级筛选的用户，哪些

//        // 找出入职该能力下所有对应的设备列表
//        List<OpenAbilityAccessControlSpot> spotList = openAbilityAccessControlSpotDao.findByOpenAbilityAbility(Ability.ON_BOARDING);

        // 循环所有下发的人，判断这个人可以下发哪些设备
        abilityMessage.getUserList().forEach(obj -> {


//            List<OpenAbilityAccessControlSpot> userAvailableSpotList = spotList.stream().filter(spot -> {
//
//                // 先根据入职地点筛选
//                // 非上班地点的门禁点不下发
//                if (obj.getPlace() != null && spot.getSpot().getAccessControlSpotPlace() != null && !spot.getSpot().getAccessControlSpotPlace().equals(obj.getPlace())) {
//                    return false;
//                }
//
//                String properties = spot.getSpot().getProperties();
//
//                if (StringUtils.isBlank(properties)) {
//                    return true;
//                }
//
//                JSONArray propertyArray = JSON.parseArray(properties);
//                Map<String, AccessControlSpot.Property> propertyMap = new HashMap<>();
//                for (int i = 0; i < propertyArray.size(); i++) {
//                    AccessControlSpot.Property property = propertyArray.getObject(i, AccessControlSpot.Property.class);
//
//                    // 入职需要获取到部门和职级，南通大厅的数据根据部门和职级做筛选
//                    if ("departmentCode".equals(property.getName()) || "positionLevel".equals(property.getName())) {
//                        propertyMap.put(property.getName(), property);
//                    }
//                }
//
//                if (!CollectionUtils.isEmpty(propertyMap)) {
//                    // 获取用户的职级和部门，用户职级大于门禁点职级或者用户部门不在门禁点部门范围内的数据放行
//                    AccessControlSpot.Property deptProperty = propertyMap.get("departmentCode");
//                    List<String> departmentCodeList = Arrays.asList(String.valueOf(deptProperty.getVal()).split(","));
//
//                    AccessControlSpot.Property positionProperty = propertyMap.get("positionLevel");
//                    int positionLevel = (int) positionProperty.getVal();
//                    if (departmentCodeList.contains(obj.getCode()) && obj.getPositionLevel() < positionLevel) {
//                        return false;
//                    }
//
//                    return true;
//                }
//
//                return true;
//            }).collect(Collectors.toList());

            // 这个人已下发数据的设备列表
            List<AccessControlSpotUser> accessControlSpotUserList = accessControlSpotUserDao.findByUserHrId(obj.getHrId());
            // 获取所有门禁点数据，可能有重复数据
            List<AccessControlSpot> accessControlSpotAllList = accessControlSpotUserList.stream()
                    .map(AccessControlSpotUser::getAccessControlSpot)
                    .collect(Collectors.toList());

            // 门禁点去重
            List<AccessControlSpot> accessControlSpotList = accessControlSpotAllList.stream()
                    .collect(Collectors.collectingAndThen(Collectors.toCollection(
                            () -> new TreeSet<>(Comparator.comparing(AccessControlSpot::getId))), ArrayList::new));

            // 获取所有的设备集合
            List<IotDevice> deviceList = accessControlSpotList.stream()
                    .map(AccessControlSpot::getDevice)
                    .filter(device -> device != null)
                    .collect(Collectors.toList());

            // 获取所有的m门禁点ID集合
            Set<String> spotIdSet = accessControlSpotList.stream()
                    .map(AccessControlSpot::getId)
                    .collect(Collectors.toSet());

            // 查询所有的旷视人员组数据
            List<AccessControlSpotKSHTMemberGroup> coverGroupList = accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotIdIn(spotIdSet);

            if (SysUserService.isVisitor(obj.getHrId())) {
                coverGroupList = coverGroupList.stream()
                        .filter(htGroup ->
                                KSHTMemberGroupType.MEAL_VISITOR.equals(htGroup.getGroupType())
                                        || KSHTMemberGroupType.VISITOR.equals(htGroup.getGroupType()))
                        .collect(Collectors.toList());
            } else {
                coverGroupList = coverGroupList.stream()
                        .filter(htGroup ->
                                KSHTMemberGroupType.MEAL_STAFF.equals(htGroup.getGroupType())
                                        || KSHTMemberGroupType.STAFF.equals(htGroup.getGroupType()))
                        .collect(Collectors.toList());
            }

            AccessPermissions.User user = AccessPermissions.User.convert(obj, coverGroupList.stream()
                    .map(AccessControlSpotKSHTMemberGroup::getId)
                    .collect(Collectors.toList())
            );

            if (SysUserService.isVisitor(user.getJobNo())) {
                user.setPersonType(AccessPermissions.PersonType.VISITOR);
            } else {
                user.setPersonType(AccessPermissions.PersonType.EMPLOYEE);
            }

            // 创建任务
            OpenAbilityTask task = createTask(abilityMessage.getAbility(), abilityMessage.getUserList(), deviceList, TaskType.MQ, abilityMessage.getType());
            // 获取设备的code集合
            List<String> deviceCodeList = deviceList.stream().map(IotDevice::getSn).collect(Collectors.toList());

            // 创建需要发送mq的消息体
            AccessPermissions message = new AccessPermissions();
            message.setDeviceCodeList(deviceCodeList);
            message.setResourceId(task.getId());
            message.setReturnTopic(Topic.get(Topic.BIZ_TO_IOTC_RETURN));
            message.setUserList(Arrays.asList(user));
            message.setSourceName("IBMS");
            message.setMethodName(AccessPermissions.Method.UPDATE.getName());
            message.setModuleName(AccessPermissions.Model.PERSON.getName());

            //将数据写入门禁点权限
            AccessControlSpotUserReq req = new AccessControlSpotUserReq();
            req.setEmpNo(obj.getHrId());
            req.setName(obj.getName());
            req.setVip(false);
            req.setStartTime(obj.getStartTime());
            req.setEndTime(obj.getEndTime());

            List<AccessControlSpotUserReq> userList = new ArrayList<>();
            userList.add(req);
            accessControlSpotService.addUsers(userList, Ability.UPDATE_FACE, accessControlSpotList.toArray(new AccessControlSpot[0]));

            // 向iotc发送消息
            iotcService.updateFace(message);
        });

        return ServiceResult.ok();
    }

    /**
     * 开放能力绑定门禁点
     *
     * @param req 请求参数
     * @return 绑定门禁点
     * @author Yang.Lee
     * @date 2022/2/14 9:46
     **/
    @Override
    @Transactional
    public ServiceResult bindingSpot(List<AbilityReq> req) {

        //  将数据根据开放能力分组
        Map<String, List<AbilityReq>> abilityListMap = req.stream().collect(Collectors.toMap(AbilityReq::getBasicId,
                e -> new ArrayList<>(Arrays.asList(e)), (oldList, newList) -> {
                    oldList.addAll(newList);
                    return oldList;
                }));

        ServiceResult result = null;

        for (Map.Entry<String, List<AbilityReq>> entry : abilityListMap.entrySet()) {
            String abilityId = entry.getKey();
            // 判断开放能力是否存在
            Optional<OpenAbility> openAbilityOptional = openAbilityDao.findById(abilityId);
            if (openAbilityOptional.isEmpty()) {
                return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_OPEN_ABILITY, abilityId), ErrConst.E01);
            }

            OpenAbility openAbility = openAbilityOptional.get();
            result = bindingSpot(openAbility.getAbility(),
                    abilityListMap.get(abilityId)
                            .stream()
                            .map(AbilityReq::getAccessControlId)
                            .collect(Collectors.toList()));

            if (!result.isSuccess()) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return result;
            }
        }
        return result;
    }

    /**
     * 开放能力绑定门禁点
     *
     * @param ability    开放能力
     * @param spotIdList 门禁点id集合
     * @return 结果
     * @author Yang.Lee
     * @date 2022/2/14 9:59
     **/
    @Override
    @Transactional
    public ServiceResult bindingSpot(Ability ability, List<String> spotIdList) {

        Optional<OpenAbility> openAbilityOptional = openAbilityDao.findByAbility(ability);

        if (openAbilityOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_OPEN_ABILITY, ability.getName()), ErrConst.E01);
        }
        OpenAbility openAbility = openAbilityOptional.get();

        // 查询门禁点信息
        List<AccessControlSpot> spotList = accessControlSpotDao.findAllById(spotIdList);

        if (spotList.isEmpty()) {
            return ServiceResult.error("未查询到对应门禁点列表", ErrConst.E01);
        }

        // 将 能力-门禁点 关系保存到数据库
        List<OpenAbilityAccessControlSpot> openAbilityAccessControlSpotList = spotList.stream()
                .map(obj -> new OpenAbilityAccessControlSpot(openAbility, obj))
                .collect(Collectors.toList());
        openAbilityAccessControlSpotDao.saveAll(openAbilityAccessControlSpotList);

        // 入职，访客，报餐 时，创建鸿图员工组
        if (Ability.ON_BOARDING.equals(ability) || Ability.VISITOR.equals(ability) || Ability.MEAL.equals(ability)) {
            // 将绑定旷视 - 鸿图设备的门禁点找出来， 调用iotc接口创建鸿图的员工组
            List<AccessControlSpot> kshtSpotList = spotList.stream().filter(obj ->
                    Optional.ofNullable(obj.getDevice()).isPresent()
                            && IotDeviceService.isKSHTDevice(obj.getDevice())
            ).collect(Collectors.toList());

            if (!kshtSpotList.isEmpty()) {

                // 判断应创建什么类型的人员组
                List<KSHTMemberGroupType> groupTypeList = new ArrayList<>();
                switch (ability) {
                    case ON_BOARDING:
                        groupTypeList.add(KSHTMemberGroupType.STAFF);
                        break;
                    case VISITOR:
                        groupTypeList.add(KSHTMemberGroupType.VISITOR);
                        break;
                    case MEAL:
//                        groupTypeList.add(KSHTMemberGroupType.MEAL_VISITOR);
//                        groupTypeList.add(KSHTMemberGroupType.MEAL_STAFF);
                        break;
                    default:
                        groupTypeList.add(KSHTMemberGroupType.OTHER);
                }

                //能力为报餐，无需创建报餐组
                if (groupTypeList.isEmpty()) {
                    return ServiceResult.ok();
                }

                // 创建鸿图员工组对象
                List<AccessControlSpotKSHTMemberGroup> kshtMemberGroupList = new ArrayList<>();
                kshtSpotList.forEach(obj -> groupTypeList.stream().filter(type -> {

                    // 判断门禁点是否已存在员工组，已存在则不需要再次创建
                    return accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotIdAndGroupTypeAndOpenAbilityAbility(obj.getId(), type, ability).isEmpty();
                }).forEach(type -> {
                    AccessControlSpotKSHTMemberGroup memberGroup = new AccessControlSpotKSHTMemberGroup();
                    memberGroup.setGroupType(type);
                    memberGroup.setGroupName(obj.getName() + "_" + type.getName());
                    memberGroup.setAccessControlSpot(obj);
                    memberGroup.setOpenAbility(openAbility);
                    kshtMemberGroupList.add(memberGroup);
                }));

                accessControlSpotKSHTMemberGroupDao.saveAll(kshtMemberGroupList);

                List<CreateHTMemberGroupReq.Param> groupReqParamList = kshtMemberGroupList.stream()
                        .map(obj -> CreateHTMemberGroupReq.Param.builder()
                                .id(obj.getId())
                                .type(obj.getGroupType().getCode())
                                .name(obj.getGroupName())
                                .build())
                        .collect(Collectors.toList());

                // 向iotc发送创建员工组消息
                ServiceResult result = iotcService.createStaffHTMemberGroup(CreateHTMemberGroupReq.builder().groupList(groupReqParamList).build());
                if (!result.isSuccess()) {
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return result;
                }

                // 像iotc发送创建员工组消息
                for (AccessControlSpotKSHTMemberGroup group : kshtMemberGroupList) {
                    PermisssionReq permisssionReq = new PermisssionReq();
                    String[] groupUuidList = new String[]{group.getId()};
                    String[] deviceUuidList = new String[]{group.getAccessControlSpot().getDevice().getSn()};
                    permisssionReq.setDeviceUuidList(deviceUuidList);
                    permisssionReq.setGroupUuidList(groupUuidList);
                    permisssionReq.setTimePlanId(timePlan);
                    result = iotcConnectService.bindingDevice(permisssionReq, "2", "KS");
                    //失败
                    if (!result.isSuccess()) {
                        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                        //this.delete(access.get(), newAbility.get());
                        return result;
                    }
                }
            }
        }

        return ServiceResult.ok();
    }

    /**
     * 将用户从鸿图的员工组中移除
     *
     * @param userHrId
     * @param kshtGroupId
     * @return 结果
     * @author Yang.Lee
     * @date 2022/2/17 13:21
     **/
    @Override
    public ServiceResult unbindKSHT(String kshtGroupId, List<String> userHrId) {

        return iotcService.removeUserFromKSHTGroup(kshtGroupId, userHrId);
    }

    /**
     * 保存返回的信息
     *
     * @param message 返回消息
     * @return 操作结果
     * @author Yang.Lee
     * @date 2022/3/3 10:03
     **/
    @Override
    @Transactional
    public ServiceResult saveReturnMessage(BizReturnMessage message) {

        Optional<OpenAbilityTask> taskOptional = openAbilityTaskDao.findById(message.getResourceId());
        if (taskOptional.isEmpty()) {
            log.error("未找到任务信息，返回结果：{}", JSON.toJSONString(message));
            return ServiceResult.error("未找到任务信息，返回结果：" + JSON.toJSONString(message), ErrConst.E01);
        }
        OpenAbilityTask task = taskOptional.get();
        task.setResult(JSON.toJSONString(message));

        openAbilityTaskDao.save(task);

        List<OpenAbilityTaskDetail> detailList = openAbilityTaskDetailDao.findByTaskId(task.getId());

        List<OpenAbilityTaskDetail> aimedDetailList = detailList.stream()
                .filter(obj -> obj.getUserHrId().equals(message.getJobNo()))
                .collect(Collectors.toList());

        String failCode = message.getFailCode();
        aimedDetailList.forEach(obj -> {
            obj.setStatus("0".equals(failCode) ? "success" : "failed");
            obj.setResult(JSON.toJSONString(message));
        });

        // 保存结果
        openAbilityTaskDetailDao.saveAll(aimedDetailList);

        if (Ability.VISITOR.equals(task.getAbility())) {

            //  从redis里查询是否已过数据
            String key = "visitor-add-return-" + message.getResourceId();
            String realKey = redisUtil.createRealKey(key);

            // redis没有，表示已发过了，不需要再次发
            if (redisUtil.hasKey(realKey)) {
                return ServiceResult.ok();
            }

            // 判断是否向访客发送数据
            // 将访客数据返回给ehr
            String tag = null;
            switch (task.getMethodType()) {
                case ADD:
                    tag = "issue-face";
                    break;
                case UPDATE:
                    tag = "update-face";
                    break;
                case DELETE:
                    tag = "delete-face";
                    break;
                default:
            }
            if ("0".equals(failCode)) {

                BizReturnMessage ehrMessage = new BizReturnMessage();
                ehrMessage.setResourceId(message.getJobNo());
                ehrMessage.setFailCode(message.getFailCode());
                ehrMessage.setFailMsg(message.getFailMsg());
                visitorMessageReturnProducer.push(ehrMessage, tag);
                log.debug("向访客发送下发回调 1， 原始数据：{}", JSON.toJSONString(message));
                // 将发送结果存入redis
                redisUtil.set(realKey, 1, RedisUtil.FIFTEEN_MIN_SECOND);
            } else {
                List<OpenAbilityTaskDetail> restDetail = detailList.stream()
                        .filter(obj -> !obj.getUserHrId().equals(message.getJobNo()))
                        .filter(obj -> StringUtils.isBlank(obj.getResult()))
                        .collect(Collectors.toList());

                // 如果没有剩余的任务了，则发送消息给ehr
                if (restDetail.isEmpty()) {
                    BizReturnMessage ehrMessage = new BizReturnMessage();
                    ehrMessage.setResourceId(message.getJobNo());
                    ehrMessage.setFailCode(message.getFailCode());
                    ehrMessage.setFailMsg(message.getFailMsg());
                    visitorMessageReturnProducer.push(ehrMessage, tag);
                    log.debug("向访客发送下发回调 2， 原始数据：{}", JSON.toJSONString(message));
                    // 将发送结果存入redis
                    redisUtil.set(realKey, 1, RedisUtil.FIFTEEN_MIN_SECOND);
                }
            }

        } else if (Ability.MEAL.equals(task.getAbility())) {
            // 报餐逻辑处理
            log.info("报餐异常人员工号:{},异常信息:{}", message.getJobNo(), message.getFailMsg());
            // 根据工号查询出最新的一条数据打上异常标签
            Optional<MealPerson> first = mealPersonDao.findFirstByWorkNoOrderByCreatedTimeDesc(message.getJobNo());
            if (first.isPresent()) {
                MealPerson mealPerson = first.get();
                mealPerson.setFailMsg(message.getFailMsg());
                if ("0".equals(failCode)) {
                    mealPerson.setStatus(MealStatus.SUCCESS);
                } else {
                    mealPerson.setStatus(MealStatus.FAILED);
                }
                mealPersonDao.save(mealPerson);
            }
        }

        return ServiceResult.ok();
    }

    @Override
    @Transactional
    public ServiceResult changeUserImportStatus(UserBindMessage userBindMessage) {
        List<OpenAbilityTaskDetail> taskDetail = openAbilityTaskDetailDao.findByTaskIdAndUserHrId(userBindMessage.getResourceId(), userBindMessage.getJobNo());
        taskDetail.forEach(obj -> {
            if ("0".equals(userBindMessage.getFailCode()) || "200".equals(userBindMessage.getFailCode())) {
                obj.setUserImportStatus(UserImportStatus.SUCCESS);
                openAbilityTaskDetailDao.save(obj);
                log.debug("人员下发成功状态修改{}", userBindMessage);
            } else {
                obj.setUserImportStatus(UserImportStatus.FAILED);
                obj.setFailMsg(userBindMessage.getFailMsg());
                openAbilityTaskDetailDao.save(obj);
                log.debug("人员下发失败状态修改{}", userBindMessage);
            }
        });
        return ServiceResult.ok();
    }

    @Override
    public ServiceResult changeUserUnBindImportStatus(UserBindMessage userBindMessage) {
        List<OpenAbilityTaskDetail> taskDetail = openAbilityTaskDetailDao.findByTaskIdAndUserHrId(userBindMessage.getResourceId(), userBindMessage.getJobNo());
        taskDetail.forEach(obj -> {
            if ("0".equals(userBindMessage.getFailCode()) || "200".equals(userBindMessage.getFailCode())) {
                obj.setUserImportStatus(UserImportStatus.DEL_SUCCESS);
                openAbilityTaskDetailDao.save(obj);
                log.debug("异常人员删除成功{}", userBindMessage);
            } else {
                obj.setUserImportStatus(UserImportStatus.DEL_FAILED);
                openAbilityTaskDetailDao.save(obj);
                log.debug("异常人员删除失败{}", userBindMessage);
            }
        });
        return ServiceResult.ok();
    }

    @Override
    public ServiceResult changeUserBatchUnBindImportStatus(UserBindMessage userBindMessage) {

        List<AccessControlSpotUser> all = new ArrayList<>();
        List<OpenAbilityTaskDetail> taskDetail = openAbilityTaskDetailDao.findByTaskIdAndUserHrId(userBindMessage.getResourceId(), userBindMessage.getJobNo());

        List<String[]> errorList = new ArrayList<>();

        taskDetail.forEach(obj -> {
            if ("0".equals(userBindMessage.getFailCode()) || "200".equals(userBindMessage.getFailCode())) {
                obj.setUserImportStatus(UserImportStatus.DEL_SUCCESS);
                openAbilityTaskDetailDao.save(obj);
                log.debug("人员删除成功{}", userBindMessage);

                //移除用户在ibms的部分权限
                Optional<AccessControlSpot> accessControlSpot = accessControlSpotDao.findByDeviceId(obj.getDevice().getId());
                if (!accessControlSpot.isEmpty()) {
                    List<AccessControlSpotUser> spotUserList = accessControlSpotUserDao.findByAccessControlSpotIdAndUserHrId(accessControlSpot.get().getId(), userBindMessage.getJobNo());
                    all.addAll(spotUserList);
                }
                all.forEach(it -> it.setIsDelete(1));
                accessControlSpotUserDao.saveAll(all);

            } else {
                obj.setUserImportStatus(UserImportStatus.DEL_FAILED);
                openAbilityTaskDetailDao.save(obj);
                log.debug("人员删除失败{}", userBindMessage);

                //将删除数据存入缓存
                String[] errorData = new String[3];
                errorData[0] = obj.getUserName();
                errorData[1] = obj.getUserHrId();
                errorData[2] = userBindMessage.getFailMsg();
                errorList.add(errorData);
            }
        });

        if (!errorList.isEmpty()) {
            // 将失败数据存入缓存，供用户下载
            String redisKey = redisUtil.createRealKey("AccessRulesImportError-" + userBindMessage.getResourceId());
            redisUtil.set(redisKey, errorList, RedisUtil.ONE_DAY_SECOND);
        }
        return ServiceResult.ok();
    }

    /**
     * 获取某能力下的设备列表
     *
     * @param ability 能力
     * @return 设备列表
     * @author Yang.Lee
     * @date 2022/5/4 17:18
     **/
    @Override
    public List<EquipmentDataResp> getAbilityEquipmentList(Ability ability) {

        List<OpenAbilityAccessControlSpot> spotList = openAbilityAccessControlSpotDao.findByOpenAbilityAbility(Ability.ATTENDANCE);

        return spotList.stream().filter(obj -> obj.getSpot().getDevice() != null).map(obj -> {

            EquipmentDataResp resp = new EquipmentDataResp();

            int faceType = 0;
            if (AccessControlInOut.IN.equals(obj.getSpot().getInOut())) {
                faceType = 2;
            } else if (AccessControlInOut.OUT.equals(obj.getSpot().getInOut())) {
                faceType = 1;
            }
            resp.setDeviceType("0");
            // 由原来的是设备id改为门禁点id
            resp.setCcDeviceCode(obj.getSpot().getId());
            resp.setFaceType(String.valueOf(faceType));
            resp.setName(obj.getSpot().getName());
            resp.setIp("");
            resp.setOptionType("0");

            return resp;
        }).collect(Collectors.toList());
    }

    /**
     * 处理报餐逻辑
     *
     * @param abilityMessage 消息
     * @return 处理结果
     * @author Yang.Lee
     * @date 2022/1/27 15:11
     **/
    @Transactional
    public ServiceResult meal(OpenAbilityMessage abilityMessage) {
        log.debug("开始执行报餐逻辑");
        // 报餐数据入库
        abilityMessage.getUserList().forEach(obj -> {
            MealPerson mealPerson = new MealPerson();
            mealPerson.setEndTime(obj.getEndTime());
            mealPerson.setStartTime(obj.getStartTime());
            mealPerson.setPlace(obj.getPlace());
            mealPerson.setStatus(MealStatus.DOING);
            mealPerson.setWorkNo(obj.getHrId());
            mealPersonDao.save(mealPerson);

        });
        if (IConst.MEAL_OVER_END_VALUE.equals(abilityMessage.getOver())) {
            // 开始检验权限配置
            ServiceResult serviceResult = mealService.permissionConfig(abilityMessage.getMealTimeList());
            // 为防止其他mq消息消费生成数据还未完全插入数据库，接受到下发指令时，延迟30s执行查询需要下发的人员列表进行下发
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                log.error("线程睡眠失败", e);
                Thread.currentThread().interrupt();
            }
            List<MealPerson> personDaoAll = mealPersonDao.findByStatus(MealStatus.DOING);

            if (serviceResult.isSuccess()) {
                // 报餐人员数据下发
                mealUserDownload(abilityMessage.getType(), personDaoAll);

                // 报餐数据状态更新
                personDaoAll.forEach(val -> {
                    val.setStatus(MealStatus.SUCCESS);
                    mealPersonDao.save(val);
                });
            } else {
                log.warn("配置权限请求失败:{}", serviceResult.getObject());
                // 报餐数据状态更新
                personDaoAll.forEach(val -> {
                    val.setStatus(MealStatus.FAILED);
                    val.setFailMsg("配置权限请求失败");
                    mealPersonDao.save(val);
                });
            }
        }


        return ServiceResult.ok();
    }

    /**
     * 报餐人员数据下发
     *
     * @param type
     * @param personDaoAll
     */
    @Override
    @Transactional
    public void mealUserDownload(MethodType type, List<MealPerson> personDaoAll) {
        if (CollectionUtils.isEmpty(personDaoAll)) {
            return;
        }
        // 获取报餐中数据
        // 发送报餐信息
        Map<AccessControlSpotPlace, List<OpenAbilityMessage.User>> placeUserMap = new HashMap<>();
        personDaoAll.forEach(obj -> {
            if (!placeUserMap.containsKey(obj.getPlace())) {
                placeUserMap.put(obj.getPlace(), new ArrayList<>());
            }
            // 参数对象转换
            OpenAbilityMessage.User user = new OpenAbilityMessage.User();
            user.setHrId(obj.getWorkNo());
            user.setStartTime(obj.getStartTime());
            user.setEndTime(obj.getEndTime());
            user.setPlace(obj.getPlace());
            placeUserMap.get(obj.getPlace()).add(user);
        });

        // 由于一个人可以同时对多个地点进行报餐，并且旷世bind对于人员组的更新是全量的，为防止对上一个地点绑定人员组覆盖，多个地点报餐的人员组需要进行累加
        MultiValueMap<String, String> hrIdToGroupIdListMap = new LinkedMultiValueMap<>();

        // 创建发送格式的数据
        placeUserMap.forEach((key, val) -> {

            List<OpenAbilityAccessControlSpot> abilitySpotList =
                    openAbilityAccessControlSpotDao.findByOpenAbilityAbilityAndSpotAccessControlSpotPlace(Ability.MEAL, key);
            List<AccessControlSpot> spotList = abilitySpotList.stream().map(OpenAbilityAccessControlSpot::getSpot).collect(Collectors.toList());
            List<AccessControlSpotUserReq> userList = val
                    .stream()
                    .map(obj -> {
                        AccessControlSpotUserReq req = new AccessControlSpotUserReq();
                        req.setEmpNo(obj.getHrId());
                        req.setName(obj.getName());
                        req.setVip(false);
                        req.setStartTime(obj.getStartTime());
                        req.setEndTime(obj.getEndTime());
                        return req;
                    })
                    .collect(Collectors.toList());
            // 将权限数据写入
            accessControlSpotService.addUsers(userList, Ability.MEAL, spotList.toArray(new AccessControlSpot[0]));

            AccessPermissions message = createBaseMessage(Ability.MEAL, val, key, type, hrIdToGroupIdListMap);
            message.setMethodName(AccessPermissions.Method.BIND.getName());
            message.setModuleName(AccessPermissions.Model.PERSON.getName());
            message.setOver("1");

            iotcService.addUser(message);
        });
    }

    /**
     * 处理离职逻辑
     *
     * @param abilityMessage 消息
     * @return 处理结果
     * @author Yang.Lee
     * @date 2022/1/27 15:11
     **/
    @Transactional
    public ServiceResult leave(OpenAbilityMessage abilityMessage) {
        log.debug("开始执行离职逻辑");

        Map<Integer, List<OpenAbilityMessage.User>> leaveTypeMap = abilityMessage.getUserList()
                .stream()
                .collect(Collectors.toMap(OpenAbilityMessage.User::getLeaveType,
                        e -> new ArrayList<>(Arrays.asList(e)),
                        (oldList, newList) -> {
                            oldList.addAll(newList);
                            return oldList;
                        }));

        leaveTypeMap.forEach((key, val) -> {
            if (key == 0) {
                // 走离职逻辑
                createLevelMessage(abilityMessage);

                List<String> userHrIdList = abilityMessage.getUserList()
                        .stream()
                        .map(OpenAbilityMessage.User::getHrId)
                        .collect(Collectors.toList());
                // 移除用户在ibms的所有权限
                accessControlSpotService.removeUserAllAccess(userHrIdList);
            } else {
                // 走解绑逻辑
                OpenAbilityMessage abilityMessageNew = new OpenAbilityMessage();
                BeanUtils.copyProperties(abilityMessage, abilityMessageNew);
                abilityMessageNew.setUserList(val);
                AccessPermissions message = createLevelUnbindMessage(key, abilityMessageNew);

                List<String> userHrIdList = abilityMessageNew.getUserList()
                        .stream()
                        .map(OpenAbilityMessage.User::getHrId)
                        .collect(Collectors.toList());
                // 移除用户在ibms的部分权限

                List<String> deviceSNList = message.getDeviceCodeList();
                List<AccessControlSpot> spotList = accessControlSpotDao.findByDeviceSnIn(deviceSNList);
                accessControlSpotService.removeUserAccess(userHrIdList, spotList.toArray(new AccessControlSpot[0]));
                // 向iotc发送数据
                iotcService.unBindUserWithDevices(message);

            }
        });

        return ServiceResult.ok();
    }

    @Transactional
    public AccessPermissions createLevelUnbindMessage(Integer levelType, OpenAbilityMessage abilityMessage) {

        List<OpenAbilityAccessControlSpot> spotList;

        if (levelType == 1) {
            spotList = openAbilityAccessControlSpotDao.findByOpenAbilityAbilityAndSpotInOut(abilityMessage.getAbility(), AccessControlInOut.IN);
        } else {
            spotList = new ArrayList<>();
        }

        List<IotDevice> deviceList = spotList.stream()
                .map(OpenAbilityAccessControlSpot::getSpot)
                .map(AccessControlSpot::getDevice)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        // 创建任务
        OpenAbilityTask task = createTask(abilityMessage.getAbility(), abilityMessage.getUserList(), deviceList, TaskType.MQ, abilityMessage.getType());

        // 获取设备的code集合
        List<String> deviceCodeList = deviceList.stream().map(IotDevice::getSn).collect(Collectors.toList());

        // 找到对应员工组的id集合
        List<AccessControlSpotKSHTMemberGroup> memberGroupList =
                accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotDeviceSnIn(deviceCodeList);
        List<String> memberGroupIdList = memberGroupList.stream()
                .map(AccessControlSpotKSHTMemberGroup::getId)
                .collect(Collectors.toList());


        // 创建需要发送mq的消息体
        AccessPermissions message = new AccessPermissions();
        message.setDeviceCodeList(deviceCodeList);
        message.setResourceId(task.getId());
        message.setReturnTopic(Topic.get(Topic.BIZ_TO_IOTC_RETURN));
        message.setUserList(abilityMessage.getUserList()
                .stream()
                .map(obj ->
                        AccessPermissions.User.convert(obj, memberGroupIdList))
                .collect(Collectors.toList())
        );
        message.setSourceName("IBMS");
        message.setMethodName(AccessPermissions.Method.UNBIND.getName());
        message.setModuleName(AccessPermissions.Model.PERSON.getName());
        message.setOver(abilityMessage.getOver());

        return message;
    }

    /**
     * 创建离职消息
     *
     * @param abilityMessage 离职数据
     * @return 结果
     * @author Yang.Lee
     * @date 2022/3/1 14:52
     **/
    @Transactional
    public void createLevelMessage(OpenAbilityMessage abilityMessage) {

        List<OpenAbilityMessage.User> user1List = new ArrayList<>();
        List<OpenAbilityMessage.User> user0List = new ArrayList<>();
        for (OpenAbilityMessage.User user : abilityMessage.getUserList()) {
            if (1 == user.getLeaveType()) {
                user1List.add(user);
            } else if (0 == user.getLeaveType()) {
                user0List.add(user);
            }
        }


        if (!CollectionUtils.isEmpty(user1List)) {
            List<OpenAbilityAccessControlSpot> spotList = openAbilityAccessControlSpotDao.findByOpenAbilityAbilityAndSpotInOut(abilityMessage.getAbility(), AccessControlInOut.IN);
            List<IotDevice> deviceList = spotList.stream()
                    .map(OpenAbilityAccessControlSpot::getSpot)
                    .map(AccessControlSpot::getDevice)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());

            // 获取设备的code集合
            List<String> deviceCodeList = deviceList.stream().map(IotDevice::getSn).collect(Collectors.toList());
            AccessPermissions message = levelUser(abilityMessage, user1List, deviceList, deviceCodeList);
            // 向iotc发送数据
            iotcService.delUser(message);
        } else if (!CollectionUtils.isEmpty(user0List)) {
            List<OpenAbilityAccessControlSpot> spotList = openAbilityAccessControlSpotDao.findByOpenAbilityAbility(abilityMessage.getAbility());
            List<IotDevice> deviceList = spotList.stream()
                    .map(OpenAbilityAccessControlSpot::getSpot)
                    .map(AccessControlSpot::getDevice)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());

            // 获取设备的code集合
            List<String> deviceCodeList = deviceList.stream().map(IotDevice::getSn).collect(Collectors.toList());
            AccessPermissions message = levelUser(abilityMessage, user1List, deviceList, deviceCodeList);
            // 向iotc发送数据
            iotcService.delUser(message);
        }

    }

    private AccessPermissions levelUser(OpenAbilityMessage abilityMessage, List<OpenAbilityMessage.User> userList, List<IotDevice> deviceList, List<String> deviceCodeList) {

        // 创建任务
        OpenAbilityTask task = createTask(abilityMessage.getAbility(), userList, deviceList, TaskType.MQ, abilityMessage.getType());

        // 创建需要发送mq的消息体
        AccessPermissions message = new AccessPermissions();
        message.setDeviceCodeList(deviceCodeList);
        message.setResourceId(task.getId());
        message.setReturnTopic(Topic.get(Topic.BIZ_TO_IOTC_RETURN));
        message.setUserList(abilityMessage.getUserList()
                .stream()
                .map(obj ->
                        AccessPermissions.User.convert(obj, new ArrayList<>()))
                .collect(Collectors.toList())
        );
        message.setSourceName("IBMS");
        message.setMethodName(AccessPermissions.Method.DELETE.getName());
        message.setModuleName(AccessPermissions.Model.PERSON.getName());
        message.setOver(abilityMessage.getOver());
        return message;
    }

    /**
     * 构建访客消息
     *
     * @param user  访客数据
     * @param place 下发地点
     * @return 消息内容
     * @author Yang.Lee
     * @date 2022/3/2 17:01
     **/
    @Transactional
    public AccessPermissions createVisitorMessage(OpenAbilityMessage.User user, @Nullable AccessControlSpotPlace place, MethodType methodType) {

        // 找出该能力下所有对应的设备列表
        List<OpenAbilityAccessControlSpot> spotList = openAbilityAccessControlSpotDao.findByOpenAbilityAbilityAndSpotAccessControlSpotPlace(Ability.VISITOR, place);

        // 根据访客类型进行筛选
        List<AccessControlSpot> availableSpotList = spotList.stream()
                .filter(obj -> {

                    String propertiesStr = obj.getSpot().getProperties();
                    // 如果门禁点无特殊权限要求，数据放行
                    if (StringUtils.isBlank(propertiesStr)) {
                        return true;
                    }

                    // 按照条件筛选
                    JSONArray propertyArray = JSON.parseArray(propertiesStr);
                    // 找到访客类型要求
                    AccessControlSpot.Property visitorCode = null;
                    for (int i = 0; i < propertyArray.size(); i++) {
                        AccessControlSpot.Property property = propertyArray.getObject(i, AccessControlSpot.Property.class);

                        if ("visitorCode".equals(property.getName())) {
                            visitorCode = property;
                            break;
                        }
                    }
                    // 如果门禁点无访客类型的要求，则数据放行
                    if (visitorCode == null) {
                        return true;
                    }
                    // 根据门禁点的访客类型要求，如果用户的访客类型不在要求内的，则筛掉
                    List<String> departmentCodeList = Arrays.asList(String.valueOf(visitorCode.getVal()).split(","));

                    if (departmentCodeList.contains(user.getVisitorType())) {
                        return false;
                    }

                    return true;
                })
                .map(OpenAbilityAccessControlSpot::getSpot)
                .collect(Collectors.toList());

        List<IotDevice> deviceList = availableSpotList.stream()
                .filter(obj -> Optional.ofNullable(obj.getDevice()).isPresent())
                .map(AccessControlSpot::getDevice).collect(Collectors.toList());

        List<String> deviceCodeList = deviceList.stream()
                .map(IotDevice::getSn)
                .collect(Collectors.toList());

        // 创建任务
        OpenAbilityTask task = createTask(Ability.VISITOR, Arrays.asList(user), deviceList, TaskType.MQ, methodType);

        // 对象转换
        List<AccessPermissions.User> userList = new ArrayList<>();
        Set<String> availableSpotIdSet = availableSpotList.stream().map(AccessControlSpot::getId).collect(Collectors.toSet());
        List<AccessControlSpotKSHTMemberGroup> groupList = accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotIdInAndGroupTypeAndOpenAbilityAbility(availableSpotIdSet, KSHTMemberGroupType.VISITOR, Ability.VISITOR);

        AccessPermissions.User messageUser = AccessPermissions.User.convert(user,
                groupList.stream()
                        .map(AccessControlSpotKSHTMemberGroup::getId)
                        .collect(Collectors.toList()));
        messageUser.setPersonType(AccessPermissions.PersonType.VISITOR);
        userList.add(messageUser);
        // 创建需要发送mq的消息体
        AccessPermissions message = new AccessPermissions();

        message.setDeviceCodeList(deviceCodeList);
        message.setResourceId(task.getId());
        message.setReturnTopic(Topic.get(Topic.BIZ_TO_IOTC_RETURN));
        message.setUserList(userList);
        message.setSourceName("IBMS");
        message.setModuleName(AccessPermissions.Model.PERSON.getName());

        return message;
    }


    @Transactional
    public AccessPermissions createBaseMessage(Ability ability, List<OpenAbilityMessage.User> messageUserList, AccessControlSpotPlace place,
                                               MethodType methodType, MultiValueMap<String, String> hrIdToGroupIdListMap) {

        // 找出该能力下所有对应的设备列表
        List<OpenAbilityAccessControlSpot> spotList = openAbilityAccessControlSpotDao.findByOpenAbilityAbility(ability);

        Set<String> spotIdSet = spotList.stream()
                .map(OpenAbilityAccessControlSpot::getSpot)
                .map(AccessControlSpot::getId)
                .collect(Collectors.toSet());

        // 获取能力对应的人员组列表
        List<AccessPermissions.User> userList;
        switch (ability) {
            case MEAL:
                userList = messageUserList
                        .stream()
                        .map(user -> {
                            // 获取人员所有组
                            List<String> userGroupIds = accessControlSpotUserService.findKsHtUserGroupByHrId(user.getHrId()).stream()
                                    .map(AccessControlSpotKSHTMemberGroup::getId).collect(Collectors.toList());

                            List<String> needBindGroupIds;
                            if (SysUserService.isVisitor(user.getHrId())) {
                                // 加上访客报餐组id
                                needBindGroupIds = transformData(user, userGroupIds, PersonType.VISITOR);
                            } else {
                                // 加上员工报餐组id
                                needBindGroupIds = transformData(user, userGroupIds, PersonType.EMPLOYEE);
                            }

                            List<String> groupIds = hrIdToGroupIdListMap.get(user.getHrId());
                            if (!CollectionUtils.isEmpty(groupIds)) {
                                needBindGroupIds.addAll(groupIds);
                            }
                            needBindGroupIds = needBindGroupIds.stream().distinct().collect(Collectors.toList());
                            hrIdToGroupIdListMap.put(user.getHrId(), needBindGroupIds);
                            return AccessPermissions.User.convert(user, needBindGroupIds);
                        })
                        .collect(Collectors.toList());

//            userList = messageUserList
//                        .stream()
//                        .map(user -> {
//                            // 获取人员所有组
//                            List<String> userGroupIds = accessControlSpotUserService.findKsHtUserGroupByHrId(user.getHrId()).stream().map(AccessControlSpotKSHTMemberGroup::getId).collect(Collectors.toList());
//                            if (SysUserService.isVisitor(user.getHrId())) {
//                                // 加上访客报餐组id
//                                return AccessPermissions.User.convert(user, transformData(user, userGroupIds, PersonType.VISITOR));
//                            } else {
//                                // 加上员工报餐组id
//                                return AccessPermissions.User.convert(user, transformData(user, userGroupIds, PersonType.EMPLOYEE));
//                            }
//                        })
//                        .collect(Collectors.toList());
                break;
            default:
                List<AccessControlSpotKSHTMemberGroup> groupList = accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotIdIn(spotIdSet);

                userList = messageUserList
                        .stream()
                        .map(obj -> {

                            List<AccessControlSpotKSHTMemberGroup> coverGroupList;
                            if (Ability.MEAL.equals(ability) || Ability.VISITOR.equals(ability)) {
                                coverGroupList = groupList.stream().filter(group -> obj.getPlace().equals(group.getAccessControlSpot().getAccessControlSpotPlace())).collect(Collectors.toList());
                            } else {
                                coverGroupList = groupList;
                            }
                            return AccessPermissions.User.convert(obj,
                                    coverGroupList.stream()
                                            .map(AccessControlSpotKSHTMemberGroup::getId)
                                            .collect(Collectors.toList()));
                        })
                        .collect(Collectors.toList());
        }

        // 获取设备的code集合
        List<IotDevice> deviceList;
        if (Ability.MEAL.equals(ability) || Ability.VISITOR.equals(ability)) {
            deviceList = spotList.stream()
                    .map(OpenAbilityAccessControlSpot::getSpot)
                    .filter(obj -> obj.getDevice() != null && place.equals(obj.getAccessControlSpotPlace()))
                    .map(AccessControlSpot::getDevice)
                    .collect(Collectors.toList());
        } else {
            deviceList = spotList.stream()
                    .map(OpenAbilityAccessControlSpot::getSpot)
                    .filter(obj -> obj.getDevice() != null)
                    .map(AccessControlSpot::getDevice)
                    .collect(Collectors.toList());

        }
        List<String> deviceCodeList = deviceList.stream().map(IotDevice::getSn).collect(Collectors.toList());

        // 创建任务
        OpenAbilityTask task = createTask(ability, messageUserList, deviceList, TaskType.MQ, methodType);

        // 创建需要发送mq的消息体
        AccessPermissions message = new AccessPermissions();
        message.setDeviceCodeList(deviceCodeList);
        message.setResourceId(task.getId());
        message.setReturnTopic(Topic.get(Topic.BIZ_TO_IOTC_RETURN));
        message.setUserList(userList);
        message.setSourceName("IBMS");
        return message;
    }


    /**
     * 处理访客逻辑
     *
     * @param abilityMessage 消息
     * @return 处理结果
     * @author Yang.Lee
     * @date 2022/1/27 15:11
     **/
    @Transactional
    public ServiceResult visitor(OpenAbilityMessage abilityMessage) {
        log.debug("开始执行访客逻辑");

        String methodName;
        switch (abilityMessage.getType()) {
            case ADD:
                methodName = AccessPermissions.Method.ADD.getName();
                checkUserFacePicAndChangeIfNotGood(abilityMessage.getUserList());
                break;
            case DELETE:
                abilityMessage.setAbility(Ability.LEAVE);
                // abilityMessage.getUserList().forEach(obj -> obj.setLeaveType(0));
                return leave(abilityMessage);
            case UPDATE:
                // 功能同修改人脸
                abilityMessage.setAbility(Ability.UPDATE_FACE);
                return updateFace(abilityMessage);
            default:
                throw new BizLogicNotMatchException("访客执行类型未知");
        }

        Map<AccessControlSpotPlace, List<OpenAbilityMessage.User>> placeUserMap = new HashMap<>();

        abilityMessage.getUserList().forEach(obj -> {
            if (!placeUserMap.containsKey(obj.getPlace())) {
                placeUserMap.put(obj.getPlace(), new ArrayList<>());
            }
            placeUserMap.get(obj.getPlace()).add(obj);
        });
        // 查询能力对应的门禁点列表

        // 创建发送格式的数据
        placeUserMap.forEach((place, val) -> {

            List<OpenAbilityAccessControlSpot> abilitySpotList =
                    openAbilityAccessControlSpotDao.findByOpenAbilityAbilityAndSpotAccessControlSpotPlace(abilityMessage.getAbility(), place);

            List<AccessControlSpot> spotList = abilitySpotList.stream().map(OpenAbilityAccessControlSpot::getSpot).collect(Collectors.toList());
            List<AccessControlSpotUserReq> userList = val
                    .stream()
                    .map(obj -> {
                        AccessControlSpotUserReq req = new AccessControlSpotUserReq();
                        req.setEmpNo(obj.getHrId());
                        req.setName(obj.getName());
                        req.setVip(false);
                        req.setStartTime(obj.getStartTime());
                        req.setEndTime(obj.getEndTime());
                        return req;
                    })
                    .collect(Collectors.toList());

            // 将权限数据写入
            accessControlSpotService.addUsers(userList, Ability.VISITOR, spotList.toArray(new AccessControlSpot[0]));

            val.forEach(obj -> {
                AccessPermissions message = createVisitorMessage(obj, place, abilityMessage.getType());
                message.setMethodName(methodName);
                message.setOver(abilityMessage.getOver());

                iotcService.addUser(message);
            });
        });

        return ServiceResult.ok();
    }

    /**
     * 创建任务
     *
     * @param ability    开放能力
     * @param userList   用户列表
     * @param deviceList 设备列表
     * @param type       消息类型
     * @param methodType 执行类型
     * @return
     * @author Yang.Lee
     * @date 2022/1/27 17:42
     **/
    @Transactional
    public OpenAbilityTask createTask(Ability ability, List<OpenAbilityMessage.User> userList, List<IotDevice> deviceList, TaskType type, MethodType methodType) {

        // 创建 task
        OpenAbilityTask task = new OpenAbilityTask();

        task.setAbility(ability);
        task.setStartTime(LocalDateTime.now());
        task.setStatus(AbilityTaskStatus.DOING);
        task.setTopic(Topic.BIZ_TO_IOTC);
        task.setType(type);
        task.setMethodType(methodType);
        openAbilityTaskDao.save(task);

        // 创建 task detail
        List<OpenAbilityTaskDetail> allDetail = new ArrayList<>();
        deviceList.forEach(device -> {

            List<OpenAbilityTaskDetail> detailList = userList.stream().map(obj -> {
                OpenAbilityTaskDetail openAbilityTaskDetail = new OpenAbilityTaskDetail();

                openAbilityTaskDetail.setTask(task);
                openAbilityTaskDetail.setDevice(device);
                openAbilityTaskDetail.setUserHrId(obj.getHrId());
                openAbilityTaskDetail.setUserName(obj.getName());

                return openAbilityTaskDetail;
            }).collect(Collectors.toList());

            allDetail.addAll(detailList);
        });

        openAbilityTaskDetailDao.saveAll(allDetail);

        return task;
    }

    /**
     * 处理入职逻辑
     *
     * @param abilityMessage 消息
     * @return 处理结果
     * @author Yang.Lee
     * @date 2022/1/27 15:13
     **/
    @Transactional
    public ServiceResult onBoarding(OpenAbilityMessage abilityMessage) {
        log.debug("开始执行入职逻辑");

        // 将入职用户分类，查出那些是需要根据部门和职级筛选的用户，哪些

        // 找出该能力下所有对应的设备列表
        List<OpenAbilityAccessControlSpot> spotList = openAbilityAccessControlSpotDao.findByOpenAbilityAbility(abilityMessage.getAbility());

        // 循环所有下发的人，判断这个人可以下发哪些设备
        abilityMessage.getUserList().forEach(obj -> {

            // 这个人以下发数据的设备列表
            List<AccessControlSpot> userAvailableSpotList = spotList.stream().filter(spot -> {

                // 先根据入职地点筛选
                // 非上班地点的门禁点不下发
                if (obj.getPlace() != null && spot.getSpot().getAccessControlSpotPlace() != null && !spot.getSpot().getAccessControlSpotPlace().equals(obj.getPlace())) {
                    return false;
                }

                String properties = spot.getSpot().getProperties();

                if (StringUtils.isBlank(properties)) {
                    return true;
                }

                JSONArray propertyArray = JSON.parseArray(properties);
                Map<String, AccessControlSpot.Property> propertyMap = new HashMap<>();
                for (int i = 0; i < propertyArray.size(); i++) {
                    AccessControlSpot.Property property = propertyArray.getObject(i, AccessControlSpot.Property.class);

                    // 入职需要获取到部门和职级，南通大厅的数据根据部门和职级做筛选
                    if ("departmentCode".equals(property.getName()) || "positionLevel".equals(property.getName())) {
                        propertyMap.put(property.getName(), property);
                    }
                }

                if (!CollectionUtils.isEmpty(propertyMap)) {
                    // 获取用户的职级和部门，用户职级大于门禁点职级或者用户部门不在门禁点部门范围内的数据放行
                    AccessControlSpot.Property deptProperty = propertyMap.get("departmentCode");
                    List<String> departmentCodeList = Arrays.asList(String.valueOf(deptProperty.getVal()).split(","));

                    AccessControlSpot.Property positionProperty = propertyMap.get("positionLevel");
                    int positionLevel = (int) positionProperty.getVal();
                    if (departmentCodeList.contains(obj.getCode()) && obj.getPositionLevel() < positionLevel) {
                        return false;
                    }

                    return true;
                }

                return true;
            }).map(OpenAbilityAccessControlSpot::getSpot).collect(Collectors.toList());

            // 获取所有的设备集合
            List<IotDevice> deviceList = userAvailableSpotList.stream()
                    .filter(spot -> spot.getDevice() != null)
                    .map(AccessControlSpot::getDevice)
                    .collect(Collectors.toList());

            // 获取所有的m门禁点ID集合
            Set<String> spotIdSet = userAvailableSpotList.stream()
                    .map(AccessControlSpot::getId)
                    .collect(Collectors.toSet());

            // 查询所有的旷视人员组数据
            List<AccessControlSpotKSHTMemberGroup> coverGroupList =
                    accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotIdInAndGroupTypeAndOpenAbilityAbility(spotIdSet,
                            KSHTMemberGroupType.STAFF,
                            Ability.ON_BOARDING);

            AccessPermissions.User user = AccessPermissions.User.convert(obj, coverGroupList.stream()
                    .map(AccessControlSpotKSHTMemberGroup::getId)
                    .collect(Collectors.toList())
            );

            user.setPersonType(AccessPermissions.PersonType.EMPLOYEE);

            // 创建任务
            OpenAbilityTask task = createTask(abilityMessage.getAbility(), abilityMessage.getUserList(), deviceList, TaskType.MQ, abilityMessage.getType());
            // 获取设备的code集合
            List<String> deviceCodeList = deviceList.stream().map(IotDevice::getSn).collect(Collectors.toList());

            //将数据写入门禁点权限
            AccessControlSpotUserReq req = new AccessControlSpotUserReq();
            req.setEmpNo(obj.getHrId());
            req.setName(obj.getName());
            req.setVip(false);
            req.setStartTime(obj.getStartTime());
            req.setEndTime(obj.getEndTime());

            accessControlSpotService.addUsers(Lists.newArrayList(req), Ability.ON_BOARDING, userAvailableSpotList.stream().toArray(AccessControlSpot[]::new));

            // 创建需要发送mq的消息体
            AccessPermissions message = new AccessPermissions();
            message.setDeviceCodeList(deviceCodeList);
            message.setResourceId(task.getId());
            message.setReturnTopic(Topic.get(Topic.BIZ_TO_IOTC_RETURN));
            message.setUserList(Arrays.asList(user));
            message.setSourceName("IBMS");
            message.setMethodName(AccessPermissions.Method.ADD.getName());
            message.setModuleName(AccessPermissions.Model.PERSON.getName());

            // 向iotc发送消息
            iotcService.addUser(message);
        });

        return ServiceResult.ok();
    }

    /**
     * 获取开放能力列表
     *
     * @return 能力列表
     * @author Yang.Lee
     * @date 2022/1/14 10:52
     **/
    @Override
    public List<AbilityDetailResp> getList() {

        List<OpenAbility> abilityList = openAbilityDao.findAll();
        return abilityList.stream().map(AbilityDetailResp::convert).peek(obj -> {

            obj.setSpotCount(openAbilityAccessControlSpotDao.countByOpenAbilityId(obj.getId()));
            obj.setApplication("EHR");

        }).collect(Collectors.toList());
    }


    /**
     * @param user
     * @param groupList
     * @param personType
     * @return
     * @description 处理返回给旷视的用户组id
     * @author tql
     * @date 22-5-19
     */
    private List<String> transformData(OpenAbilityMessage.User user, List<String> groupList, PersonType personType) {
        List<String> list = new ArrayList<>();
        list.addAll(groupList);

        // 获取报餐的时间计划
        Optional<KsHtTimePlan> timePlane = kshtTimePlanDao.findByStartTimeAndEndTimeAndPlace(
                DateUtils.formatLocalDateTime(user.getStartTime(), DateUtils.PATTERN_TIME1),
                DateUtils.formatLocalDateTime(user.getEndTime(), DateUtils.PATTERN_TIME1),
                user.getPlace());
        if (timePlane.isPresent()) {
            KsHtTimePlan ksHtTimePlan = timePlane.get();
            // 获取报餐的组
            List<MealGroup> mealGroups = mealGroupDao.findByKsHtTimePlan(ksHtTimePlan);
            mealGroups.forEach(obj -> {
                // 字段包含判断
                if (obj.getName().contains(personType.getName())) {
                    list.add(obj.getId());
                }
            });
        }

        log.info("绑定人员组信息:{}", list);
        return list;
    }

    /**
     * 检查用户图片。如果用户图片不合格则修改图片
     *
     * @param userList 用户列表
     * @return
     * @author Yang.Lee
     * @date 2022/7/11 18:04
     **/
    public void checkUserFacePicAndChangeIfNotGood(List<OpenAbilityMessage.User> userList) {

        userList.forEach(this::checkUserFacePicAndChangeIfNotGood);
    }

    /**
     * 检查用户图片。如果用户图片不合格则修改图片
     *
     * @param user 用户信息
     * @return
     * @author Yang.Lee
     * @date 2022/7/11 18:06
     **/
    private void checkUserFacePicAndChangeIfNotGood(OpenAbilityMessage.User user) {

        String photoUrl = user.getPhoto();
        if (StringUtils.isEmpty(photoUrl)) {
            throw new NullPointerException();
        }

        File tmpFile;
        try {
            tmpFile = restTemplateUtil.getFile(user.getPhoto());

            log.debug("文件大小 {}", tmpFile.length());
            if (tmpFile.length() >= 200000) {
                // 需要进行压缩
                // 压缩后的结果对象
                File picCompressTarget = File.createTempFile(UUID.randomUUID().toString(), ".jpg");
                // 压缩质量默认为0.8
                PicUtils.compressQuality(tmpFile, picCompressTarget, 0.8f);

                log.debug("图片压缩，压缩前大小：{}， 压缩后大小： {}", tmpFile.length(), picCompressTarget.length());

                // 将压缩后的图片上传，获取图片连接
                ServiceResult uploadResult = uploadService.upload(picCompressTarget);
                if (uploadResult.isSuccess()) {

                    UploadResp uploadResp = (UploadResp) uploadResult.getObject();
                    user.setPhoto(uploadResp.getUrl());
                } else {
                    log.error("文件上传失败，{}", uploadResult.getObject());
                }

                // 处理完成后删除临时文件
                FileUtil.deleteFiles(picCompressTarget, tmpFile);
            }

        } catch (IOException e) {
            log.error(e.getMessage(), e);
            return;
        }
    }
}
