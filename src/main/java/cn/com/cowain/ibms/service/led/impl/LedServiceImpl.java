package cn.com.cowain.ibms.service.led.impl;

import cn.com.cowain.ibms.dao.ability.OpenAbilityAccessControlSpotDao;
import cn.com.cowain.ibms.dao.iot.AccessControlSpotAccessRecordDao;
import cn.com.cowain.ibms.entity.ability.OpenAbilityAccessControlSpot;
import cn.com.cowain.ibms.entity.iot.AccessControlSpotAccessRecord;
import cn.com.cowain.ibms.enumeration.ability.Ability;
import cn.com.cowain.ibms.enumeration.iot.AccessControlInOut;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.rest.req.iot.AttendanceInOutResp;
import cn.com.cowain.ibms.rest.req.iot.AttendanceStatisticsResp;
import cn.com.cowain.ibms.rest.resp.ChartDataResp;
import cn.com.cowain.ibms.service.led.LedService;
import cn.com.cowain.ibms.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Yang.Lee
 * @date 2022/5/4 10:33
 */
@Service
@Slf4j
public class LedServiceImpl implements LedService {

    @Resource
    private OpenAbilityAccessControlSpotDao openAbilityAccessControlSpotDao;

    @Resource
    private AccessControlSpotAccessRecordDao accessControlSpotAccessRecordDao;

    /**
     * 获取考勤数据统计
     *
     * @param projectId 项目ID，用于筛选
     * @return 考勤数据统计
     * @author Yang.Lee
     * @date 2022/5/4 10:33
     **/
    @Override
    @Transactional
    public AttendanceStatisticsResp getAttendanceStatistic(String projectId) {

        // 查询所有考勤设备
        List<OpenAbilityAccessControlSpot> accessControlSpotList = openAbilityAccessControlSpotDao.findAll((root, query, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();

            // 筛选地点
            if (StringUtils.isNotBlank(projectId)) {
                list.add(criteriaBuilder.equal(root.get("spot").get("space").get("project").get("id"), projectId));
            }

            // 筛选考勤设备
            list.add(criteriaBuilder.equal(root.get("openAbility").get("ability"), Ability.ATTENDANCE));

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        });

        // 获取设备总数
        long deviceCount = accessControlSpotList.stream().filter(obj -> obj.getSpot().getDevice() != null).count();

        // 获取未通行记录总数
        long unrecognizedNumber = accessControlSpotAccessRecordDao.count((root, query, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();

            // 筛选地点
            if (StringUtils.isNotBlank(projectId)) {
                list.add(criteriaBuilder.equal(root.get("accessControlSpot").get("space").get("project").get("id"), projectId));
            }

            // 筛选时间，只查询当天数据
            list.add(criteriaBuilder.between(root.get("deviceAccessRecord").get("dataTime"), LocalDateTime.of(LocalDate.now(), LocalTime.MIN), LocalDateTime.of(LocalDate.now(), LocalTime.MAX)));

            // 筛选未通行的数据
            list.add(criteriaBuilder.notEqual(root.get("deviceAccessRecord").get("accessStatus"), "通行"));

            // 只查询面板机数据
            list.add(criteriaBuilder.notEqual(root.get("accessControlSpot").get("device").get("deviceType"), DeviceType.DOOR_MAGNETIC));

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        });

        return AttendanceStatisticsResp.builder()
                .deviceNumber(deviceCount)
                .unrecognizedNumber(unrecognizedNumber)
                .build();
    }

    /**
     * 查询考勤设备进出的数据（最近12小时数据）
     *
     * @param projectId 项目ID
     * @return 数据对象
     * @author Yang.Lee
     * @date 2022/5/4 14:30
     **/
    @Override
    public AttendanceInOutResp getAttendanceInOut(String projectId) {

        // 查询12个小时的数据
        int hour = 12;
        // 30分钟间隔
        int xTimeInterval = 30;

        // 12个小时里的所有x轴数据段个数
        int xLength = 60 / xTimeInterval * hour;

        LocalDateTime[] xArr = new LocalDateTime[xLength];
        LocalDateTime endTime = LocalDateTime.now();
        xArr[xLength - 1] = endTime;
        for (int i = xArr.length - 2; i >= 0; i--) {
            xArr[i] = xArr[i + 1].minusMinutes(xTimeInterval);
        }

        LocalDateTime startTime = xArr[0];
        DateTimeFormatter formatter= DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

       List<String> ids;
        if(StringUtils.isEmpty(projectId)) {
            ids = accessControlSpotAccessRecordDao.findByDataTimeSql(startTime.format(formatter), endTime.format(formatter));
        }else {
            ids = accessControlSpotAccessRecordDao.findByDataTimeProjectNotNullSql(startTime.format(formatter), endTime.format(formatter), projectId,"通行");
        }
        // 查询最近12个小时的数据

        List<AccessControlSpotAccessRecord> tempList = accessControlSpotAccessRecordDao.findAllById(ids);
        List<AccessControlSpotAccessRecord> accessRecordList=new ArrayList<>();
        if(!CollectionUtils.isEmpty(tempList)){
            for(AccessControlSpotAccessRecord record:tempList){
                if(!record.getAccessControlSpot().getDevice().getDeviceType().equals(DeviceType.DOOR_MAGNETIC)){
                    accessRecordList.add(record);
                }
            }
        }

        // 区分进出数据
        // 进数据列表
        List<AccessControlSpotAccessRecord> inList = new ArrayList<>();
        // 出数据列表
        List<AccessControlSpotAccessRecord> outList = new ArrayList<>();

        accessRecordList.forEach(obj -> {
            if (AccessControlInOut.IN.equals(obj.getAccessControlSpot().getInOut())) {
                inList.add(obj);
            } else if (AccessControlInOut.OUT.equals(obj.getAccessControlSpot().getInOut())) {
                outList.add(obj);
            }
        });

        List<ChartDataResp> inChartDataList = new ArrayList<>();
        List<ChartDataResp> outChartDataList = new ArrayList<>();

        for (LocalDateTime x : xArr) {

            inChartDataList.add(ChartDataResp.builder()
                    .x(DateUtils.formatLocalDateTime(x, DateUtils.PATTERN_DATETIME))
                    .y(0)
                    .build());

            outChartDataList.add(ChartDataResp.builder()
                    .x(DateUtils.formatLocalDateTime(x, DateUtils.PATTERN_DATETIME))
                    .y(0)
                    .build());
        }

        // 统计进数据
        for(AccessControlSpotAccessRecord inData : inList){
            b : for (int i = 0; i < inChartDataList.size() - 1; i++){
                LocalDateTime current = DateUtils.parseLocalDateTime(inChartDataList.get(i).getX(), DateUtils.PATTERN_DATETIME);
                LocalDateTime next = DateUtils.parseLocalDateTime(inChartDataList.get(i + 1).getX(), DateUtils.PATTERN_DATETIME);

                if (inData.getDeviceAccessRecord().getDataTime().isAfter(current) &&
                        inData.getDeviceAccessRecord().getDataTime().isBefore(next)) {
                    double num = inChartDataList.get(i).getY() + 1;
                    inChartDataList.get(i).setY(num);
                    break b;
                }
            }
        }


        // 统计出数据
        for(AccessControlSpotAccessRecord outData : outList){
            b : for (int i = 0; i < outChartDataList.size() - 1; i++){
                LocalDateTime current = DateUtils.parseLocalDateTime(outChartDataList.get(i).getX(), DateUtils.PATTERN_DATETIME);
                LocalDateTime next = DateUtils.parseLocalDateTime(outChartDataList.get(i + 1).getX(), DateUtils.PATTERN_DATETIME);

                if (outData.getDeviceAccessRecord().getDataTime().isAfter(current) &&
                        outData.getDeviceAccessRecord().getDataTime().isBefore(next)) {
                    double num = outChartDataList.get(i).getY() + 1;
                    outChartDataList.get(i).setY(num);
                    break b;
                }
            }
        }


        AttendanceInOutResp inOutResp = new AttendanceInOutResp();
        inOutResp.setIn(inChartDataList);
        inOutResp.setOut(outChartDataList);

        return inOutResp;
    }
}
