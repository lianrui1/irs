package cn.com.cowain.ibms.entity.space;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.enumeration.space.ProjectPropertyRightType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 项目记录表
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2020/12/17 10:08
 */
@Data
@Entity
@Table(name = "ibms_project")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_project", comment = "项目记录表")
@Where(clause = "is_del=0")
public class Project extends BaseEntity {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 项目编号
     */
    @Column(name = "project_no", nullable = false, columnDefinition = "varchar(8)  COMMENT '项目编号'")
    private String no;

    /**
     * 项目名称
     */
    @Column(name = "project_name", nullable = false, columnDefinition = "varchar(25)  COMMENT '项目名称'")
    private String projectName;

    /**
     * 省级编号
     */
    @Column(name = "province_id", nullable = false, columnDefinition = "varchar(6) COMMENT '省级编号'")
    private String provinceId;

    /**
     * 省级名称
     */
    @Column(name = "province_name", nullable = false, columnDefinition = "varchar(20) COMMENT '省级名称'")
    private String provinceName;

    /**
     * 市级编号
     */
    @Column(name = "city_id", nullable = false, columnDefinition = "varchar(6) COMMENT '市级编号'")
    private String cityId;

    /**
     * 市级编号
     */
    @Column(name = "city_name", nullable = false, columnDefinition = "varchar(20) COMMENT '市级名称'")
    private String cityName;

    /**
     * 区级编号
     */
    @Column(name = "district_id", nullable = false, columnDefinition = "varchar(6) COMMENT '区级编号'")
    private String districtId;

    /**
     * 区级名称
     */
    @Column(name = "district_name", nullable = false, columnDefinition = "varchar(20) COMMENT '区级名称'")
    private String districtName;

    /**
     * 项目地址
     */
    @Column(name = "address", nullable = false, columnDefinition = "varchar(50)  COMMENT '项目详细地址'")
    private String address;

    /**
     * 项目区域
     */
    @ManyToOne
    @JoinColumn(name = "project_area_id", nullable = false, columnDefinition = "varchar(50)  COMMENT '项目区域'")
    private ProjectArea projectArea;

    /**
     * 单位
     */
    @Column(name = "visit_company", columnDefinition = "varchar(50)  COMMENT '单位'")
    private String visitCompany;

    /**
     * 是否可邀请访客
     */
    @Column(name = "is_visitor", columnDefinition = "varchar(10) COMMENT '是否可邀请访客'")
    private String visitor;

    @Enumerated(EnumType.STRING)
    @Column(name = "property_right_type", columnDefinition = "varchar(50) COMMENT '产权属性'")
    private ProjectPropertyRightType propertyRightType;

    @Column(name = "area_m2", columnDefinition = "varchar(50) COMMENT '面积'")
    private String areaM2;

    @Column(name = "introduction", columnDefinition = "varchar(200) COMMENT '简介'")
    private String introduction;

    @Column(name = "picture", columnDefinition = "varchar(200) COMMENT '图片'")
    private String picture;
}
