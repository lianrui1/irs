package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 设备上下线对象
 *
 * @author Yang.Lee
 * @date 2021/4/28 21:08
 */
@Data
@Entity
@Table(name = "iot_device_network_change")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_device_network_change", comment = "设备上下线信息")
public class IotDeviceNetworkChange extends BaseEntity {

    /**
     * iot 设备
     */
    @ManyToOne
    @JoinColumn(name = "device_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 设备ID'")
    private IotDevice device;

    /**
     * 设备的状态。
     *
     * ONLINE：设备在线。
     * OFFLINE：设备离线。
     * ABNORMAL：设备异常。
     * INACTIVE：设备未激活。
     * FROZEN：设备冻结
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "device_status", nullable = false, columnDefinition = "varchar(256) COMMENT '华为云中的设备的状态。ONLINE：设备在线。OFFLINE：设备离线。ABNORMAL：设备异常。INACTIVE：设备未激活。FROZEN：设备冻结'")
    private DeviceStatus deviceStatus;

    /**
     * 数据时间
     **/
    @Column(name = "data_time", nullable = false, columnDefinition = "datetime COMMENT '数据时间'")
    private LocalDateTime dataTime;
}
