package cn.com.cowain.ibms.config;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.RedisConnectionFactory;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/10/20
 */
@SpringBootTest
public class RedisConnectionFactoryTest {

    @Autowired
    private RedisConnectionFactory redisConnectionFactory;

    @Test
    public void test1() {
        Assertions.assertNotNull(redisConnectionFactory);
    }

}