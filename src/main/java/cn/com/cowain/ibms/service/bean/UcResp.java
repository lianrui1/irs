package cn.com.cowain.ibms.service.bean;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/17 16:53
 */
@Data
public class UcResp {
    private String workPhoto;
    private String realName;
}
