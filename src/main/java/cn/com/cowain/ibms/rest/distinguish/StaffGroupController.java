package cn.com.cowain.ibms.rest.distinguish;

import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.PageReq;
import cn.com.cowain.ibms.rest.req.Sorts;
import cn.com.cowain.ibms.rest.req.distinguish.StaffGroupPageReq;
import cn.com.cowain.ibms.rest.req.distinguish.StaffGroupReq;
import cn.com.cowain.ibms.rest.resp.distinguish.AccessControlSpotResp;
import cn.com.cowain.ibms.rest.resp.distinguish.StaffGroupResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.disthinguish.StaffGroupService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * 人员组controller
 *
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/5 17:32
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/staff")
@Api(tags = IConst.DISTINGUISH)
public class StaffGroupController {

    @Autowired
    private StaffGroupService staffGroupService;

    /**
     * 获取人员组列表
     *
     * @return
     */
    @GetMapping
    @ApiOperation(value = "PC查询人员组列表", tags = {IConst.V20,IConst.V23,IConst.DISTINGUISH})
    public ResponseEntity<JsonResult<PageBean<StaffGroupResp>>> getStaffGroupList(StaffGroupPageReq req) {
        // 查询所有人员组列表,根据创建时间倒叙排列
        req.addOrderDesc(Sorts.CREATED_TIME);
        return ResponseEntity.ok(JsonResult.ok(staffGroupService.search(req)));
    }

    /**
     * 新增人员组
     *
     * @return
     */
    @PostMapping
    @ApiOperation(value = "PC新增人员组", tags = IConst.DISTINGUISH)
    public ResponseEntity<JsonResult<ServiceResult>> save(@RequestBody @Validated @ApiParam("人员组信息") StaffGroupReq staffGroupReq, HttpServletRequest request,
                                                          BindingResult bindingResult) {

        log.debug("PC新增人员组");
        // 保存
        ServiceResult service = staffGroupService.save(staffGroupReq);
        JsonResult<ServiceResult> jsonResult = JsonResult.okNoData("OK");
        if (!service.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(service.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 更新人员组信息
     *
     * @param id            人员组ID
     * @param bindingResult
     * @return
     */
    @PutMapping("/{id}")
    @ApiOperation(value = "PC更新人员组信息", tags = IConst.DISTINGUISH)
    public ResponseEntity<JsonResult<ServiceResult>> update(@PathVariable @ApiParam("人员组ID") String id,
                                                            @RequestBody @Validated @ApiParam("人员组信息") StaffGroupReq staffGroupReq, HttpServletRequest request,
                                                            BindingResult bindingResult) {
        // 变更
        ServiceResult service = staffGroupService.update(id, staffGroupReq);
        JsonResult<ServiceResult> jsonResult = JsonResult.okNoData("OK");
        if (!service.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(service.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 根据ID查询人员组信息
     *
     * @param id 人员组ID
     * @return
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "PC查询人员组信息", tags = IConst.DISTINGUISH)
    public ResponseEntity<JsonResult<StaffGroupResp>> search(@PathVariable @ApiParam("人员组ID") String id) {

        ServiceResult result = staffGroupService.get(id);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.ok("ok", (StaffGroupResp) result.getObject()));
    }

    /**
     * 根据ID删除人员组信息
     *
     * @param id 人员组ID
     * @return
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "PC删除人员组信息", tags = IConst.DISTINGUISH)
    public ResponseEntity<JsonResult<ServiceResult>> delete(@PathVariable @ApiParam("人员组ID") String id) {
        JsonResult<ServiceResult> jsonResult = JsonResult.okNoData("OK");
        ServiceResult result = staffGroupService.delete(id);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 获取人员组列表
     *
     * @return
     */
    @GetMapping("/page")
    @ApiOperation(value = "PC查询人员组列表分页", tags = IConst.DISTINGUISH)
    public ResponseEntity<JsonResult<PageBean<StaffGroupResp>>> getStaffGroupPageList(PageReq pageReq, @ApiParam(name ="accessControlSpotId", value = "门禁点ID") String accessControlSpotId) {
        // 查询所有人员组列表分页
        PageBean<StaffGroupResp> pageList = staffGroupService.searchPage(pageReq, accessControlSpotId);
        return ResponseEntity.ok(JsonResult.ok("OK", pageList));
    }


    @PostMapping("/upload/createGroups")
    @ApiOperation(value = "上传员工组数据", tags = "${project.sprint}")
    public ResponseEntity<JsonResult<Object>> uploadStaffGroup(MultipartFile file) {

        ServiceResult result = staffGroupService.addStaffGroupBatch(file);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(result.getObject().toString(), null, result.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.ok(result.getObject()));
    }

    @GetMapping("/download/importFailed")
    @ApiOperation(value = "下载导入失败的数据", tags = "${project.sprint}")
    public void getUploadStaffGroupErrorFile(HttpServletResponse response, @RequestParam String errorDataId) {

        byte[] bytes = staffGroupService.getImportErrorDataFileByte(errorDataId);

        response.addHeader(IConst.CONTENT_TYPE, IConst.APPLICATION);
        response.addHeader(IConst.CONTENT_DISPOSITION, "attachment;filename=" + errorDataId + ".xlsx");
        response.addHeader(IConst.CONTENT_LENGTH, String.valueOf(bytes.length));
        OutputStream stream = null ;
        try {
            stream = response.getOutputStream();
            stream.write(bytes);
            stream.flush();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } finally {
            if(stream != null){
                try {
                    stream.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }

    /**
     * 获取门禁点详情列表
     *
     * @author: yanzy
     * @param: id 人员组ID
     * @return:
     */
    @GetMapping("/access/{id}")
    @ApiOperation(value = "查询门禁点详情列表", tags = IConst.V23)
    public ResponseEntity<JsonResult<PageBean<AccessControlSpotResp>>> getAccessControlSpotRespList(@PathVariable @ApiParam(name = "id", value = "人员组ID", required = true) String id,PageReq req) {

        return ResponseEntity.ok(JsonResult.ok("OK",staffGroupService.getAccessControlSpotRespList(id, req)));
    }
}
