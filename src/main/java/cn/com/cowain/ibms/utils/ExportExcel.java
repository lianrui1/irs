
package cn.com.cowain.ibms.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.util.List;

/**
 * 导出集合数据  到excel文件
 * excel文件：
 *      文件名，表头名字， 实体数据。
 *  1.需要设置表头的文件名，=。
 *  2.需要设置表头的字段，这里就是fields[] 数组，attributes[] 就是存放个表头对应对象的字段
 *    导出集合到excel中，通常集合中对象的所有属性不全部导出到excel，例如id字段。
 *  3.需要设置实体数据。
 *  注意： fields [] 和 attributes[] 遵循一一对应，即表头的一个格子对应对象的一个字段。
 *  另外，这里使用的是反射做处理的，所以约定优于配置，即集合中的实体对象需要和attributes[]一样的顺序(只许多，不许少)
 *  例如：
 *      fields={"序号","姓名","年龄"}
 *      attributes={"id","name","age"}
 * @ClassName projectExportExcel
 * @Description
 * @Author cfl
 * @Date 2019/8/6 19:35
 * @Version 1.0 : 利用反射结合POI.jar 中HSSFWorkbook对象导出Excel。
 * @Version 1.1 （19/10/2）: 一、新增 成员变量 suffix（文件后缀）、SIZE（判断大小，选择不同的Workbook对象）、MEMORY_NUM（内存中保留数据条数，其余写入硬盘临时文件）、
 *                  SHEET_SIZE（一个工作薄sheet多少条数据）；
 *              二、根据接口编程，根据里氏代换原则动态选择 HSSFWorkbook，XSSFWorkbook, SXSSFWorkbook对象；
 *              三、完善代码，使其不再依赖pojo的声明字段的顺序；
 *              四、删除 void setFilesAndAttributes(Class clazz) 方法；
 *              五、新增 void setSHEET_SIZE(int sheet_size)方法；
 */
@Slf4j
public class ExportExcel {

    // 表头
    private static String[] fields;
    // 表头对应的 字段
    private static String[] attributes;
    // excel文件格式（.xls,.xlsx）
    private static String suffix = ".xlsx";
    // 导出时选择不同的工作博对象，小于等于SIZE 创建HSSFWorkbook，大于SIZE创建SXSSFWorkbook
    private final static  int SIZE = 10000;
    // 创建SXSSFWorkbook对象对少条flush
    private final static  int MEMORY_NUM = 1000;
    // 一个工作表有 SHEET_SIZE 数据（加一是因为表头占一行）
    private static  Integer sheetSize = 100001;


    /**
     * 设置成员变量 fields 和attributes
     * @param fileds 表头
     * @param attributes    表头对应的字段
     */
    public static void setFilesAndAttributes(String[] fileds, String[] attributes) {

        // 深层复制。开辟一块新内存空间
        // 先初始化数组
        int length = fileds.length;
        ExportExcel.fields = new String[fileds.length];
        ExportExcel.attributes = new String[attributes.length];
        // 因 fileds，attributes一一对应。
        for(int i = 0; i < length; i++) {
            ExportExcel.fields[i] = fileds[i];
            ExportExcel.attributes[i] = attributes[i];
        }
    }


    /**
     * 写出Excel文件
     * 	fileName:文件下载到客户端的文件名
     * @param fileName  浏览器下载的excel文件名字
     * @param list  需要写出的数据
     * @param <T>
     */
    public static <T> void writeExcel(String fileName, List<T> list) {
        // 1.判断属性 files 和 attributes 属性是否满足导出excel的基本要求。
        if(!checkFieldsAndAttr()){
            return;
        }
        // 获取请求对象和响应对象
        ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        RequestContextHolder.setRequestAttributes(sra, true);
        HttpServletRequest request = null;
        HttpServletResponse response = null;
        if(sra!=null){
            request = sra.getRequest();
            response = sra.getResponse();
        }

        // 2.创建工作薄对象
        Workbook wb = ExportExcel.createWorkbook(list);
        // 查看内存使用
        //响应对象获取输出流，通过输出流写出excel文件
        OutputStream out = null;
        // 3.设置编码格式，响应头
        try {
            if(request!=null){
                request.setCharacterEncoding("UTF-8");
            }
            // 获取excel名字
            fileName = setFileName(fileName);
            fileName = URLEncoder.encode(fileName, "UTF-8");
            if(response!=null){
                response.setCharacterEncoding("UTF-8");
                response.setContentType("application/x-download");
                response.addHeader("Content-Disposition", "attachment;filename=" + fileName);
                out = response.getOutputStream();
            }
            // 写出到浏览器
            if(wb!=null){
                wb.write(out);
            }
        }catch (RuntimeException e){
            log.error(e.getMessage(), e);
        } catch (UnsupportedEncodingException e) {
            log.error(e.getMessage(), e);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } finally{
            try {
                if(out!=null){
                    out.close();
                }
                if(wb!=null){
                    wb.close();
                }
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    /**
     * 检查属性Fields 和 Attributes 是否满足导出excel的要求
     * @return  满足导出要求，返回true，不满足返回false。
     */
    private static boolean checkFieldsAndAttr(){
        Boolean flag = true;

        // Fields 和 Attributes不一一对应！
        if(ExportExcel.fields == null || ExportExcel.attributes == null){
            flag = false;
            log.error("ExportException: 导出excel时的 Fields 和 Attributes 未定义");
        }else if(ExportExcel.fields.length != ExportExcel.attributes.length){
            flag = false;
            log.error("ExportException: 导出excel时的Fields 和 Attributes 的长度不一致错误");
        }
        return flag;
    }

    /**
     * 返回 HSSFWorkbook/ XSSFWorkbook/SXSSFWorkbook 对象
     * 创建Excel文件对象
     * @Param [list 表格数据, fields, attributes]
     * @Return org.apache.poi.hssf.usermodel.HSSFWorkbook
     */
    private static <T> Workbook createWorkbook(List<T> list) {
        //创建Workbook对象
        Workbook wb = null;
        //创建HSSFSheet对象

        if(list.size() <= SIZE){
            // 2003
            wb = new HSSFWorkbook();
            // 修改excel文件格式。
            suffix = ".xls";
        } else if(list.size() > SIZE){
            // 进行大批量写入操作解决了这个问题。防止内存溢出，构造参数是指内存中多少条flush到磁盘上
            try {
                //ExportExcel.createTempXSSFWorkbook(),
                wb = new SXSSFWorkbook(MEMORY_NUM);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
            log.error("创建了SXSSFWorkbook");
        } else {
            // 2007以上，先将其保留
            wb = new XSSFWorkbook();
        }
        // 创建内容实体
        createBody(wb, list);
        // 写出excel到硬盘

        return wb;
    }


    /**
     * 根据反射创建excel 的实体
     * 注意这里的list集合的成员的属性，需要根据attributes顺序写。
     * @param wb    excel工作博对象
     * @param list  需要写出到excel的集合
     * @param <T>   集合泛型
     */
    private static <T> void createBody(Workbook wb, List<T> list) {
        // excel工作表对象
        Sheet sheet = null;
        // excel行对象
        Row row = null;
        // 单元格对象
        Cell c = null;
        // 使用泛型声明一个集合中的元素类型对象 P
        T p = null;
        // P对象的所有字段
        Field[] fields = null;
        // 新建的sheet工作表 从第一行创建起
        int row_index = 0;
        // 创建行
        for (int i = 0; i < list.size(); i++) {
            // SHEET_SIZE 条数据 换一个工作表
            if(i % sheetSize == 0){
                row_index = 0;
                sheet = wb.createSheet("sheet"+ (i / sheetSize));
                // 每一个工作薄都要创建表头
                row = sheet.createRow(row_index);
                // 创建表头
                createHead(row, ExportExcel.fields);
            }
            ++row_index;
            row = sheet.createRow(row_index);
            // 从集合中获取需要导出的一条数据
            p = list.get(i);
            // 获取对象所有属性
            fields = p.getClass().getDeclaredFields();

            // 创建一行中的所有列
            try {
                for (int j = 0; j < fields.length; j++) {
                    for (int k = 0; k < ExportExcel.attributes.length; k++) {
                        // 配置文件字段和对象字段相比较，相同就执行方法
                        if (fields[j].getName().equalsIgnoreCase(ExportExcel.attributes[k])) {
                            // 创建HSSFCell对象 表头
                            c = row.createCell(k);
                            fields[j].setAccessible(true);
                            if (fields[j].get(p) == null) {
                                c.setCellValue("");
                            } else {
                                c.setCellValue(fields[j].get(p).toString());
                            }
                            break;
                        }
                    }
                }
            } catch (IllegalAccessException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    /**
     * 创建 Excel表头
     * @param row   表头行对象
     * @param fields   表头列字段
     */
    private static void createHead(Row row, String[] fields) {
        // 创建行头
        for (int i = 0; i < fields.length; i++) {
            //创建HSSFCell对象 表头
            Cell cell = row.createCell(i);
            //设置表头值
            cell.setCellValue(fields[i]);
        }
    }

    /**
     * 设置导出文件名
     *
     * @Param [fileName] 文件名
     * @Return java.lang.String
     */
    private static String setFileName(String fileName) {
        // 文件名包含了.xls,和.xlsx;直接使用
        if(fileName.endsWith(".xls") || fileName.endsWith(".xlsx")){
            return fileName;
        }
        // 默认 xlsx 格式
        if (fileName.indexOf(".") == -1) {
            fileName += suffix;
        } else if(fileName.endsWith(".")){
            // 截取小数点前面的字符串。
            fileName = fileName.substring(0, fileName.lastIndexOf("."));
            // 追加文件格式
            fileName += suffix;
        }else{
            // 抛出异常
            throw new RuntimeException("警告：导出文件错误: " + fileName +"不是正确的excel格式：.xls, .xlsx");
        }

        return fileName;
    }

}