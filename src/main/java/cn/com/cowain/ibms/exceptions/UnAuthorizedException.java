package cn.com.cowain.ibms.exceptions;

/**
 * @author Yang.Lee
 * @date 2021/4/14 14:10
 */
public class UnAuthorizedException extends RuntimeException{
    public UnAuthorizedException(String message) {
        super(message);
    }
}
