package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/26 9:47
 */
@Data
@ApiModel("hk添加人员门禁点权限请求对象人员信息")
public class UserReq {

    @ApiModelProperty(value = "人员工号")
    private String empNo;

    @ApiModelProperty(value = "人员姓名")
    private String name;
}
