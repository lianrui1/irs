package cn.com.cowain.ibms.rest.req.device.centralized;

import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wei.cheng
 * @date 2022/09/19 10:04
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "查询集控码列表请求参数")
public class CentralizedControlCodeSearchReq extends PageReq {

    @ApiModelProperty(value = "根据项目id进行筛选")
    private String projectId;

    @ApiModelProperty(value = "根据空间id进行筛选")
    private String spaceId;

    @ApiModelProperty(value = "根据集控码名称模糊查询")
    private String name;
}
