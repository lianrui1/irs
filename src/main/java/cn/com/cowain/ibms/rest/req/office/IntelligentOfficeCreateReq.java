package cn.com.cowain.ibms.rest.req.office;

import cn.com.cowain.ibms.enumeration.iot.IntelligentOfficeStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/8 15:30
 */
@Data
@ApiModel("创建智能办公室请求对象")
public class IntelligentOfficeCreateReq {

    @NotBlank
    @Length(max = 10, min = 1)
    //@Pattern(regexp = "^[\u4e00-\u9fa5]{1,10}$", message = "名称校验失败，只允许输入中文，且长度不超过10")
    @ApiModelProperty(value = "办公室名称", required = true)
    private String name;

    @NotBlank
    @ApiModelProperty(value = "空间ID", required = true)
    private String spaceId;

    @ApiModelProperty(value = "状态", required = true)
    private IntelligentOfficeStatus status = IntelligentOfficeStatus.ENABLE;
}
