package cn.com.cowain.ibms.websocket.server;

import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.meeting.IntelligentMeetingAppService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.websocket.bean.MeetingMessage;
import cn.com.cowain.ibms.websocket.bean.MessageResult;
import cn.com.cowain.ibms.websocket.bean.resp.MeetingAppResp;
import cn.com.cowain.ibms.websocket.enumeration.MessageType;
import cn.com.cowain.ibms.websocket.enumeration.WSActionType;
import cn.com.cowain.ibms.websocket.service.RTASRService;
import cn.com.cowain.sfp.comm.JsonResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.stream.Collectors;

/**
 * websocket服务
 *
 * @author Yang.Lee
 * @date 2021/1/12 9:52
 */
@Slf4j
@ServerEndpoint("/ibms/ws/{action}/{id}")
@Component
@Data
public class WsServer {

    /**
     * 正在连接的实时语音请求列表
     */
    public static final CopyOnWriteArraySet<WsServer> webSocketSet = new CopyOnWriteArraySet<>();

    //要注入的service或者dao
    private static RTASRService rtasrService;

    @Autowired
    public void setRTASRService(RTASRService rtasrService) {
        setRtasrServiceValue(rtasrService);
    }

    public static void setRtasrServiceValue(RTASRService rtasrService) {
        WsServer.rtasrService = rtasrService;
    }


    private static IntelligentMeetingAppService intelligentMeetingAppService;

    @Autowired
    public void setIntelligentMeetingAppService(IntelligentMeetingAppService intelligentMeetingAppService) {
        setIntelligentMeetingAppServiceValue(intelligentMeetingAppService);
    }

    public static void setIntelligentMeetingAppServiceValue(IntelligentMeetingAppService intelligentMeetingAppService) {
        WsServer.intelligentMeetingAppService = intelligentMeetingAppService;
    }

    private static final String UNKNOWN_MESSAGE_TYPE = "websocket接收到未知消息类型。";

    /**
     * server id， 当server type 为 meeting时为会议室 room id
     **/
    private String id;

    private WSActionType type;

    private String sessionId;

    private Session session;

    /**
     * 连接成功
     *
     * @param session
     * @param action
     * @param id
     */
    @OnOpen
    public void onOpen(Session session, @PathParam("action") String action, @PathParam("id") String id) {
        log.info("链接建立， id = " + id + " , action = " + action + ",  session id = " + session.getId());
        WSActionType wsActionType;
        try {
            wsActionType = WSActionType.valueOf(action.toUpperCase());
        } catch (IllegalArgumentException e) {
            log.info(UNKNOWN_MESSAGE_TYPE + e.getMessage());
            return;
        }

        this.id = id;
        this.type = wsActionType;
        this.sessionId = session.getId();
        this.session = session;
        // 加入set中
        webSocketSet.add(this);

        // 根据 action执行业务
        switch (wsActionType) {
            case RTASR:
                try {
                    ServiceResult result = rtasrService.open(session);
                    if (!result.isSuccess()) {
                        String message = JSON.toJSONString(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E15));
                        this.sendMessage(message, session);
                    }
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                    // 从set中删除
                    webSocketSet.remove(this);
                    try {
                        session.close();
                    } catch (IOException ioException) {
                        log.error("session 关闭失败！！！", e);
                    }
                    Thread.currentThread().interrupt();
                }
                break;
            case MEETING:
                // 当会议app链接过来时，  id表示会议室id

                ServiceResult result = intelligentMeetingAppService.getMeetingAppDetail(id);
                if (!result.isSuccess()) {
                    sendMessage(JSON.toJSONString(MessageResult.error((String) result.getObject(), null, MessageType.MEETING_DETAIL)), session);
                } else {

                    sendMessage(JSON.toJSONString(MessageResult.ok((MeetingAppResp) result.getObject(), MessageType.MEETING_DETAIL)), session);
                }

                break;
            default:
                sendMessage(JSON.toJSONString(MessageResult.ok(null, MessageType.GENERAL)), session);
                break;
        }

    }

    /**
     * 连接关闭
     *
     * @param session
     */
    @OnClose
    public void onClose(Session session, @PathParam("action") String action, @PathParam("id") String id) throws IOException {
        log.info("链接关闭， session id = " + session.getId());
        // 从set中删除
        webSocketSet.remove(this);

        WSActionType wsActionType;
        try {
            String upper = action.toUpperCase();
            wsActionType = WSActionType.valueOf(upper);
        } catch (IllegalArgumentException e) {
            log.info(UNKNOWN_MESSAGE_TYPE + e.getMessage());
            return;
        }

        // 根据 action执行业务
        switch (wsActionType) {
            case RTASR:
                rtasrService.close(session);
                break;
            default:
                close(session.getId());
        }
    }

    /**
     * 接收到消息
     *
     * @param message
     * @param session
     * @param action
     */
    @OnMessage
    public void onMsg(String message, Session session, @PathParam("action") String action, @PathParam("id") String id) {
        log.info("get message from sessionid = " + session.getId() + " , action :" + action);
        log.info(message);
        WSActionType wsActionType = getActionType(action);

        try {
            // json格式的新数据
            JSONObject jsonObject = JSON.parseObject(message);

            WsServer wsServer = get(session.getId());

            if(Optional.ofNullable(wsServer).isEmpty()){
                this.sendMessage(JSON.toJSONString(MessageResult.error("客户端注册异常，未找到注册数据，请重新注册", null, MessageType.ERROR)), session);
                return ;
            }

            switch (wsServer.getType()) {
                case MEETING_RESERVATION:

                    MeetingMessage meetingMessage = jsonObject.toJavaObject(MeetingMessage.class);

                    this.sendMessage(JSON.toJSONString(MessageResult.ok(null, meetingMessage.getCmd())), session);
                    break;
                default:
                    this.sendMessage(JSON.toJSONString(MessageResult.ok(null, MessageType.GENERAL)), session);

            }


        } catch (Exception e) {

            // 字符串格式的旧数据
            String[] messageArr = message.split("\\|");

            WsServer wsServer = get(session.getId());
            if (wsServer == null) {
                this.sendMessage(JSON.toJSONString(MessageResult.error("未找到会话，请重新连接服务", null, MessageType.GENERAL)), session);
                return;
            }

            log.info("服务器端找到会话，id {}, sessionId {}, type {}", wsServer.getId(), wsServer.getSessionId(), wsServer.getType());

            switch (wsActionType) {
                case MEETING:
                    MessageType messageType = getMessageResultType(messageArr[1]);
                    ServiceResult result = intelligentMeetingAppService.meetingMessage(messageType, messageArr);

                    if (result.isSuccess()) {

                        if (MessageType.EXTEND.equals(messageType)) {
                            this.sendMessage(JSON.toJSONString(MessageResult.ok((MeetingAppResp) result.getObject(), messageType)), session);
                        } else {
                            this.sendMessage(JSON.toJSONString(MessageResult.ok(null, messageType)), session);
                        }


                    } else {
                        this.sendMessage(JSON.toJSONString(MessageResult.error((String) result.getObject(), null, messageType)), session);
                    }
                    break;
                default:
                    this.sendMessage(JSON.toJSONString(MessageResult.ok(null, MessageType.GENERAL)), session);

            }
        }


    }

    @OnMessage
    public void onMessage(byte[] message, Session session, @PathParam("action") String action, @PathParam("id") String id) throws URISyntaxException, InterruptedException {
        log.debug("get byte data, from sessionid = " + session.getId() + ", action :" + action);
        log.debug("byte length:" + message.length);

        WSActionType wsActionType;
        try {
            wsActionType = WSActionType.valueOf(action.toUpperCase());
        } catch (IllegalArgumentException e) {
            log.info(UNKNOWN_MESSAGE_TYPE + e.getMessage());
            return;
        }

        // 根据 action执行业务
        switch (wsActionType) {
            case RTASR:
                rtasrService.translate(session, message);
                break;
            default:
                break;
        }
        this.sendMessage("ok", session);
    }

    /**
     * 服务端发送消息给客户端
     */
    public void sendMessage(String message, Session toSession) {
        try {
            log.info("服务端给客户端[{}]发送消息{}", toSession.getId(), message);
            toSession.getBasicRemote().sendText(message);
        } catch (IOException e) {
            log.error("服务端给客户端[{}]发送消息失败,消息内容：{}", message);
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 根据sessionID 获取 链接对象
     *
     * @param sessionId sessionId
     * @return 链接对象
     * @author Yang.Lee
     * @date 2022/1/19 9:45
     **/
    @Nullable
    public static synchronized WsServer get(String sessionId) {
        return webSocketSet.stream().filter(obj -> sessionId.equals(obj.sessionId)).findFirst().orElse(null);
    }

    private WSActionType getActionType(String action) {
        return WSActionType.valueOf(action.toUpperCase());
    }

    private MessageType getMessageResultType(String message) {
        return MessageType.valueOf(message.toUpperCase());
    }

    public synchronized void close(String sessionId) {
        WsServer wsServer = get(sessionId);
        if (wsServer == null) {
            return;
        }
        webSocketSet.remove(wsServer);
    }

    /**
     * 获取指定id对应的Server集合
     *
     * @param id server id
     * @return server集合
     * @author Yang.Lee
     * @date 2022/1/19 9:43
     **/
    public static synchronized List<WsServer> getServerList(String id) {

        return WsServer.webSocketSet.stream().filter(obj -> obj.getId().equals(id)).collect(Collectors.toList());
    }

}
