package cn.com.cowain.ibms.rest.resp.space;

import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.space.IotProduct;
import cn.com.cowain.ibms.entity.space.Project;
import cn.com.cowain.ibms.enumeration.IotNodeType;
import cn.com.cowain.ibms.enumeration.iot.DataSource;
import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.beans.BeanUtils;

import java.time.LocalDateTime;

/**
 *  @title
 *  @Description 描述
 *  @author jf.sui
 *  @Date
 */
@Data
@ApiModel("展厅项目获取设备信息")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DeviceInfoShowRoomResp   {
    @ApiModelProperty(value = "设备ID", required = true)
    private String id;
    /**
     * 设备状态 0:禁用;1:启用
     */

    private Integer status = 1;

    /**
     * 设备类型类型
     */
    private DeviceType deviceType;

    /**
     * IOT网络节点 类型
     */
    private IotNodeType iotNodeType;

    /**
     * 设备密码
     */
    private String password;


    /**
     * SN码
     */
    private String sn;

    /**
     * 设备名称
     */
    private String deviceName;
    /**
     * 所属产品
     */
    private IotProduct iotProduct;

    /**
     * 所属项目
     */
    private Project project;

    /**
     * 所属空间
     */
    private Space space;

    /**
     * 设备编号
     */
    private int deviceNo;

    /**
     * 排序字段
     */
    private int seq;

    /**
     * 华为云中的设备ID
     */
    private String hwDeviceId;

    /**
     * 华为云中的设备名称
     */
    private String hwDeviceName;

    /**
     * 华为云中的设备节点类型
     *
     * ENDPOINT：非直连设备。
     * GATEWAY：直连设备或网关。
     * UNKNOWN：未知。
     */
    private String hwNodeType;


    /**
     * 华为云中的设备关联的产品ID
     */
    private String hwProductId;

    /**
     * 华为云中的设备关联的产品名称
     */
    private String hwProductName;

    /**
     * 华为云中的设备关联的网关ID，用于标识设备所属的父设备，即父设备的设备ID
     */
    private String hwGatewayId;

    /**
     * 华为云中的设备的状态。
     *
     * ONLINE：设备在线。
     * OFFLINE：设备离线。
     * ABNORMAL：设备异常。
     * INACTIVE：设备未激活。
     * FROZEN：设备冻结
     */
   private DeviceStatus hwStatus;

    /**
     * 数据来源
     **/
    private DataSource dataSource;

    /**
     * 设备位置
     **/
    private String address;

    /**
     * 华为云最近一次状态时间
     **/
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime lastHwStatusChangedDatetime;

    /**
     * 设备是否纳入空间控制
     **/
    private Integer spaceControl;
    public static  DeviceInfoShowRoomResp convert(IotDevice device){
        DeviceInfoShowRoomResp resp=new DeviceInfoShowRoomResp();
        BeanUtils.copyProperties(device, resp);
        return resp;
    }
}
