package cn.com.cowain.ibms.rest.req.meeting;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/12 18:33
 */
@Data
public class CustomUserReq {

    private String userGroupId;
}
