package cn.com.cowain.ibms.dao.task;

import cn.com.cowain.ibms.entity.task.TaskCenterTask;
import cn.com.cowain.ibms.enumeration.task.TaskOperateResult;
import cn.com.cowain.ibms.enumeration.task.TaskStatus;
import cn.com.cowain.ibms.enumeration.task.TaskType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author wei.cheng
 * @date 2022/03/10 16:50
 */
@Repository
public interface TaskCenterTaskDao extends JpaRepository<TaskCenterTask, String>, JpaSpecificationExecutor<TaskCenterTask> {
    /**
     * 查询type = {@code type} & status = {@code status} & taskId = {@code taskId}的TaskCenterTask
     *
     * @param type
     * @param status
     * @param taskId
     * @return
     */
    List<TaskCenterTask> findAllByTypeAndStatusAndTaskId(TaskType type, TaskStatus status, String taskId);

    /**
     * 查询type = {@code type} & taskId = {@taskId}的TaskCenterTask
     *
     * @param type
     * @param taskId
     * @return
     */
    List<TaskCenterTask> findAllByTypeAndTaskId(TaskType type, String taskId);

    List<TaskCenterTask> findAllByOperateResultAndUcHrId(TaskOperateResult pending, String userHrId);
}
