package cn.com.cowain.ibms.rest.resp.device.centralized;

import cn.com.cowain.ibms.rest.bean.PageBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author wei.cheng
 * @date 2022/09/20 16:54
 */
@Data
@ApiModel(value = "h5端集控码控制设备列表")
public class CentralizedCodeControlDeviceListH5Resp {

    @ApiModelProperty(value = "控制设备个数")
    private Integer count;

    @ApiModelProperty(value = "控制设备中已开启的数量")
    private Integer countOpen;

    @ApiModelProperty(value = "设备列表(分页)")
    private PageBean<CentralizedCodeControlDeviceH5Resp> deviceList;
}
