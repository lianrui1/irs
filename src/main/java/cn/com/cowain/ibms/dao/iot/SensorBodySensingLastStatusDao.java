package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.SensorBodySensingLastStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SensorBodySensingLastStatusDao extends JpaRepository<SensorBodySensingLastStatus, String>, JpaSpecificationExecutor<SensorBodySensingLastStatus> {


    Optional<SensorBodySensingLastStatus> findByDeviceId(String id);
}
