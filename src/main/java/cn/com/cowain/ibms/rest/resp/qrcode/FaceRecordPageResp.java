package cn.com.cowain.ibms.rest.resp.qrcode;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/22 14:13
 */
@Builder
@Data
public class FaceRecordPageResp {

    @ApiModelProperty(value = "排序")
    private Integer seq;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "工号")
    private String jobNum;

    @ApiModelProperty(value = "部门")
    private String dept;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "抓拍时间")
    private LocalDateTime happenTime;

    @ApiModelProperty(value = "状态 全部通行")
    private String status;

}
