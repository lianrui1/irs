package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 萤石云视频摄像头
 *
 * @author Yang.Lee
 * @date 2021/11/17 15:59
 */
@Data
@Entity
@Table(name = "iot_ys_video_camera")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_ys_video_camera", comment = "萤石云视频摄像头表")
public class YSVideoCamera extends BaseEntity {

    @Column(name = "serial", columnDefinition = "varchar(100) COMMENT '设备序列号'")
    private String serial;

    @Column(name = "ys_device_type", columnDefinition = "varchar(100) COMMENT '萤石云设备类型'")
    private String ysDeviceType;

    @Column(name = "defence", columnDefinition = "int(2) COMMENT '具有防护能力的设备布撤防状态：0-睡眠，8-在家，16-外出，普通IPC布撤防状态：0-撤防，1-布防'")
    private int defence;

    @Column(name = "model", columnDefinition = "varchar(100) COMMENT '设备型号'")
    private String model;

    @Column(name = "is_encrypt", columnDefinition = "tinyint(1) COMMENT '是否加密：0-不加密，1-加密'")
    private int isEncrypt;

    @Column(name = "alarm_sound_mode", columnDefinition = "tinyint(1) COMMENT '告警声音模式：0-短叫，1-长叫，2-静音'")
    private int alarmSoundMode;

    @Column(name = "offline_notify", columnDefinition = "tinyint(1) COMMENT '设备下线是否通知：0-不通知 1-通知'")
    private int offlineNotify;

    @Column(name = "category", columnDefinition = "varchar(100) COMMENT '设备大类'")
    private String category;

    @Column(name = "net_type", columnDefinition = "varchar(100) COMMENT '网络类型，如有线连接wire'")
    private String netType;

    @Column(name = "[signal]", columnDefinition = "varchar(100) COMMENT '信号强度(%)'")
    private String signal;

    @Column(name = "device_version", columnDefinition = "varchar(50) COMMENT '设备版本号'")
    private String deviceVersion;

    @ManyToOne
    @JoinColumn(name = "device_id", nullable = false, columnDefinition = "varchar(36) COMMENT '视频监控点关联设备ID FK'")
    private IotDevice device;


}
