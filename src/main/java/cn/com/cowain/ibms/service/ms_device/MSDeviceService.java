package cn.com.cowain.ibms.service.ms_device;

import cn.com.cowain.ibms.rest.req.ms_device.AccessRuleCreateReq;
import cn.com.cowain.ibms.rest.req.ms_device.AccessRuleUpdateReq;
import cn.com.cowain.ibms.service.bean.ServiceResult;

/**
 * ms-device service
 *
 * @author Yang.Lee
 * @date 2021/3/19 19:54
 */
public interface MSDeviceService {

    /**
     * 创建通行权限
     *
     * @param req 请求参数
     * @return 请求结果
     * @author Yang.Lee
     * @date 2021/3/19 20:33
     **/
    ServiceResult createAccessRule(AccessRuleCreateReq req);

    /**
     * 编辑通行权限
     *
     * @param req 请求参数
     * @return 请求结果
     * @author Yang.Lee
     * @date 2021/3/19 20:33
     **/
    ServiceResult updateAccessRule(AccessRuleUpdateReq req);

    /**
     * 删除通行权限
     * @param ksDeviceId 旷世设备ID
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/3/20 18:48
     **/
    ServiceResult deleteAccessRule(String ksDeviceId);
}
