package cn.com.cowain.ibms.rest.resp.office;

import cn.com.cowain.ibms.enumeration.RuleEngIneDetailTriggerDeviceCondition;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 传感器设备返回参数
 *
 * @author: yanzy
 * @date: 2022/2/14 20:19
 */
@Data
@ApiModel("传感器设备返回参数")
public class RuleEngIneDetailTriggerSensorResp {

    @ApiModelProperty(value = "属性")
    private String key;

    @ApiModelProperty(value = "值  或 (差:4 中:3 良:2 优:1) 或 true or false")
    private String value;

    @ApiModelProperty(value = "条件")
    private RuleEngIneDetailTriggerDeviceCondition deviceCondition;

}
