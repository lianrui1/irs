package cn.com.cowain.ibms.service.iot.impl;

import cn.com.cowain.ibms.dao.iot.AlertSpotDao;
import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.dao.iot.SpaceDao;
import cn.com.cowain.ibms.entity.iot.AlertSpot;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.rest.req.iot.AlertSpotCreateReq;
import cn.com.cowain.ibms.rest.resp.iot.AlertSpotResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.iot.AlertSpotService;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author: yanzy
 * @date: 2022/4/13 10:55
 */
@Service
@Slf4j
public class AlertSpotServiceImpl implements AlertSpotService {

    @Resource
    private AlertSpotDao alertSpotDao;
    @Resource
    private DeviceDao deviceDao;
    @Resource
    private SpaceDao spaceDao;

    /**
     * 查询所有警戒点
     *
     * @author: yanzy
     * @param: [spaceId]
     * @data: 2022/4/13 10:04:20
     */
    @Override
    public List<AlertSpotResp> getAlertSpot() {

        List<AlertSpot> alertSpotList = alertSpotDao.findAll();

        return alertSpotList.stream().map(AlertSpotResp::convert).collect(Collectors.toList());
    }

    @Override
    public ServiceResult createAlertSpot(AlertSpotCreateReq req) {
        log.info("start to create alertSpot, req:{}", JSONObject.toJSONString(req));
        Optional<IotDevice> iotDeviceOptional = deviceDao.findById(req.getDeviceId());
        if (iotDeviceOptional.isEmpty()) {
            return ServiceResult.error("设备不存在");
        }
        IotDevice device = iotDeviceOptional.get();
        Optional<Space> spaceOptional = spaceDao.findById(req.getSpaceId());
        if (spaceOptional.isEmpty()) {
            return ServiceResult.error("空间不存在");
        }
        List<AlertSpot> alertSpotList = alertSpotDao.findAllByDeviceId(device.getId());
        if (CollectionUtils.isNotEmpty(alertSpotList)) {
            return ServiceResult.error("设备已绑定监控点，一个设备只能绑定一个监控点");
        }
        AlertSpot alertSpot = new AlertSpot();
        alertSpot.setName(req.getName().trim());
        alertSpot.setAddress(req.getAddress());
        alertSpot.setSpace(spaceOptional.get());
        alertSpot.setDevice(device);
        alertSpot.setMsUrl(req.getMsUrl());
        alertSpotDao.save(alertSpot);
        return ServiceResult.ok(AlertSpotResp.convert(alertSpot));
    }
}
