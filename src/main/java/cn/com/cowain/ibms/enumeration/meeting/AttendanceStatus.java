package cn.com.cowain.ibms.enumeration.meeting;

/**
 * @author Yang.Lee
 * @date 2021/7/16 15:39
 */
public enum AttendanceStatus {

    SIGNED("已签到"),

    NOT_SIGN("未签到");

    private String name;

    AttendanceStatus(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
}
