package cn.com.cowain.ibms.feign.iotc.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 * @author: yanzy
 * @date: 2022/2/17 20:24
 */
@Data
@ApiModel("规则判断调节接收值")
public class Conditions {

    @ApiModelProperty(value = "参数名", example = "temperature1/temperature2")
    private String key;

    @ApiModelProperty(value = "条件判断值", example = "20")
    private String value;

    @ApiModelProperty(value = "大于小于", example = ">/<")
    private String compare;
}
