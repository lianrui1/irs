package cn.com.cowain.ibms.rest.req.iot;

import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2021/7/20 14:12
 */
@Data
@ApiModel("海康面板机分页查询参数")
public class HIKFaceMachinePageReq extends PageReq {

    @ApiModelProperty(value = "项目ID", position = 1)
    private String projectId;

    @ApiModelProperty(value = "空间id", position = 2)
    private String spaceId;

    @ApiModelProperty(value = "搜索关键字， SN码 / 设备名称", position = 3)
    private String keyword1;

    @ApiModelProperty(value = "搜索关键字， 员工姓名 / 工号", position = 3)
    private String keyword2;
}
