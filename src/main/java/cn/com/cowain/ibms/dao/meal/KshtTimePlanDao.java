package cn.com.cowain.ibms.dao.meal;

import cn.com.cowain.ibms.entity.meal.KsHtTimePlan;
import cn.com.cowain.ibms.enumeration.iot.AccessControlSpotPlace;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

/**
 * @author Yang.Lee
 * @date 2022/1/14 10:54
 */
public interface KshtTimePlanDao extends JpaRepository<KsHtTimePlan, String>, JpaSpecificationExecutor<KsHtTimePlan> {

    /**
     * @param startTime
     * @param endTime
     * @param place
     * @return
     * @description 根据报餐时间和地点找到报餐组
     * @author tql
     * @date 22-5-19
     */
    Optional<KsHtTimePlan> findByStartTimeAndEndTimeAndPlace(String startTime, String endTime, AccessControlSpotPlace place);
}
