package cn.com.cowain.ibms.entity.ability;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.enumeration.ability.Ability;
import cn.com.cowain.ibms.enumeration.ability.AbilityTaskStatus;
import cn.com.cowain.ibms.enumeration.ability.MethodType;
import cn.com.cowain.ibms.enumeration.ability.TaskType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author Yang.Lee
 * @date 2022/1/10 16:55
 */
@Data
@Entity
@Table(name = "ibms_open_ability_task")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_open_ability_task", comment = "IBMS 开放能力任务表")
@Where(clause = "is_del=0")
public class OpenAbilityTask extends BaseEntity {

    @Enumerated(EnumType.STRING)
    @Column(name = "ability_id", nullable = false, columnDefinition = "varchar(36) COMMENT '开放能力'")
    private Ability ability;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false, columnDefinition = "varchar(50) COMMENT '任务状态'")
    private AbilityTaskStatus status;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false, columnDefinition = "varchar(50) COMMENT '任务类型：MQ/API'")
    private TaskType type;

    @Column(name = "topic", nullable = false, columnDefinition = "varchar(100) COMMENT 'mq topic'")
    private String topic;

    @Column(name = "start_time", columnDefinition = "datetime COMMENT '任务开始时间'")
    private LocalDateTime startTime;

    @Column(name = "end_time", columnDefinition = "datetime COMMENT '任务结束时间'")
    private LocalDateTime endTime;

    @Column(name = "result", columnDefinition = "text COMMENT '结果'")
    private String result;

    @Enumerated(EnumType.STRING)
    @Column(name = "methdo_type", nullable = false, columnDefinition = "varchar(50) COMMENT '执行类型'")
    private MethodType methodType;
}
