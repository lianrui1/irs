package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.DeviceGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/10/21 13:29
 */
public interface DeviceGroupDao extends JpaRepository<DeviceGroup, String>, JpaSpecificationExecutor<DeviceGroup> {

    /**
     * 查询父节点查询
     *
     * @param parentId 父ID
     * @return 群组列表
     * @author Yang.Lee
     * @date 2021/10/27 14:30
     **/
    List<DeviceGroup> findByParentGroupId(String parentId);
}
