package cn.com.cowain.ibms.rest.resp;

import cn.com.cowain.ibms.enumeration.ReservationRecordItemStatus;
import cn.com.cowain.ibms.enumeration.ReservationRecordStatus;
import cn.com.cowain.ibms.rest.bean.ParticipantBean;
import cn.com.cowain.ibms.rest.resp.meeting.CustomUserGroupResp2;
import cn.com.cowain.ibms.rest.resp.meeting.InvitationCardResp;
import cn.com.cowain.ibms.rest.resp.meeting.MeetingFileResp;
import cn.com.cowain.ibms.rest.resp.meeting.MeetingFileUrlResp;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Description
 *
 * @author 张宇鑫
 * @version 1.0
 * @date 2020/8/7 16:22
 */
@Data
@ApiModel("会议详情")
public class ReservationRecordDetailsResp{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 会议id
     */
    @ApiModelProperty(value = "会议id", required = true, position = 0)
    private String id;

    /**
     * 会议主题
     */
    @ApiModelProperty(value = "会议主题", required = true, position = 1)
    private String topic;

    /**
     * 开始时间
     */
    @ApiModelProperty(value = "开始时间", required = true, position = 2)
    private LocalDateTime timeFrom;

    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间", required = true, position = 3)
    private LocalDateTime timeTo;

    /**
     * 楼栋
     */
    @ApiModelProperty(value = "楼栋", required = true, position = 4)
    private String building;

    /**
     * 楼层
     */
    @ApiModelProperty(value = "楼层", required = true, position = 5)
    private String floor;

    /**
     * 参会人以及状态
     */
    @ApiModelProperty(value = "参会人以及状态", required = true, position = 6)
    private List<ParticipantBean> participant;


    /**
     * 会议室id
     */
    @ApiModelProperty(value = "会议室id", required = true, position = 7)
    private String roomId;

    /**
     * 是否可编辑
     */
    @ApiModelProperty(value = "是否可编辑,true:可编辑; false:不可编辑", required = true, position = 8)
    private Boolean editable;

    /**
     * 备注说明
     */
    @ApiModelProperty(value = "备注说明", required = true, position = 9)
    private String remark;

    /**
     * 需要的服务
     */
    @ApiModelProperty(value = "需要的服务", required = true, position = 10)
    private String service;

    /**
     * 会议发起人
     */
    @ApiModelProperty(value = "会议发起人", required = true, position = 11)
    private String initiatorName;

    /**
     * 预定记录状态
     */
    @ApiModelProperty(value = "预定记录状态", required = true, position = 12)
    private ReservationRecordStatus reservationRecordStatus;

    /**
     * 是否本人发起 0否 1是
     */
    @ApiModelProperty(value = "是否本人发起 0否 1是", required = true, position = 13)
    private Integer isOwn = 0;

    /**
     * 会议室名称
     */
    @ApiModelProperty(value = "会议室名称", required = true, position = 14)
    private String roomName;

    /**
     * 参会人状态
     */
    @ApiModelProperty(value = "参会人状态", required = true, position = 15)
    private ReservationRecordItemStatus userStatus;

    /**
     * 会议室是否被删除状态 0存在  1被删
     */
    @ApiModelProperty(value = "会议室状态", required = true, position = 16)
    private int remove;

    @ApiModelProperty(value = "会议发起人工号", required = true, position = 17)
    private String initiatorEmpNo;

    @ApiModelProperty(value = "免打扰.1:免打扰;0:可以打扰", required = true, position = 18, example = "1")
    private boolean noDisturb;

    @ApiModelProperty(value = "会议状态", position = 19)
    private ReservationRecordStatus status;

    /**
     * 位置
     */
    @ApiModelProperty(value = "空间地址", required = true, position = 20)
    private String location;

    /**
     * 项目ID
     */
    @ApiModelProperty(value = "项目ID", required = true, position = 21)
    private String projectId;

    /**
     * 项目名称
     */
    @ApiModelProperty(value = "项目名称", required = true, position = 22)
    private String projectName;

    /**
     * 空间id
     */
    @ApiModelProperty(value = "空间id", required = true, position = 23)
    private String spaceId;

    /**
     * 空间名称
     */
    @ApiModelProperty(value = "空间名称", required = true, position = 24)
    private String spaceName;

    /**
     * 邀请卡
     */
    @ApiModelProperty(value = "邀请卡", required = true, position = 25)
    private InvitationCardResp cardResp;

    /**
     * 会议组列表
     */
    @ApiModelProperty(value = "会议组列表", position = 26)
    private List<CustomUserGroupResp2> userGroupList;

    /**
     * 会议文档
     *
     * @author: yanzy
     */
    @ApiModelProperty(value = "会议文档", position = 27)
    private List<MeetingFileResp> meetingFileList;

    /**
     * 会议文档
     *
     * @author: yanzy
     */
    @ApiModelProperty(value = "会议文档链接")
    private List<MeetingFileUrlResp> meetingFileUrlList;

    /**
     * 是否有会议大屏
     *
     * @author: yanzy
     */
    @ApiModelProperty(value = "是否有会议大屏 1:是； 0：否", position = 28)
    private Integer isScreen;

    /**
     * 欢迎语
     *
     * @author: yanzy
     */
    @ApiModelProperty(value = "欢迎语设置", position = 29)
    private String welcomeWords;

    @ApiModelProperty(value = "会议大屏状态 1:是； 2：否", position = 30)
    private Integer screenStatus;

    @ApiModelProperty(value = "智能设备控制 1:是； 2：否", position = 31)
    private Integer deviceControl;

}
