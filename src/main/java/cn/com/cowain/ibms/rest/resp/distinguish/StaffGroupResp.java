package cn.com.cowain.ibms.rest.resp.distinguish;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/5 18:57
 */
@Data
@ApiModel("人员组响应对象")
public class StaffGroupResp {

    @ApiModelProperty(value = "人员组名称", required = true, position = 0)
    private String name;

    @ApiModelProperty(value = "组内人数", position = 1)
    private int count;

    @ApiModelProperty(value = "排序", position = 2)
    private int seq;

    @ApiModelProperty(value = "参与人列表", position = 3)
    private List<StaffSysUserResp> participantList;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间", position = 4)
    private LocalDateTime createdTime;

    @ApiModelProperty(value = "人员组ID", required = true, position = 5)
    private String id;

    @ApiModelProperty(value = "是否门禁点默认人员组，1-是，0-否", required = true, position = 6)
    private int isDefault;

    /**
     * 添加关联门禁点个数字段
     *
     * @author yanzy
     * @date 2022/01/11
     */
    @ApiModelProperty(value = "关联门禁点个数", required = true, position = 7)
    private int accessControlSpotCount;
}
