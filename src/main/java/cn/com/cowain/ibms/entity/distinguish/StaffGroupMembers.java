package cn.com.cowain.ibms.entity.distinguish;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 人员组成员
 *
 * @author Yang.Lee
 * @date 2021/3/4 20:23
 */
@Data
@Entity
@Table(name = "ibms_distinguish_staff_group_members")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_distinguish_staff_group_members", comment = "人脸识别人员组成员表")
@Where(clause = "is_del=0")
public class StaffGroupMembers extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "staff_group_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 人员组ID'")
    private StaffGroup groupId;

    @Column(name = "sys_user_emp_no", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 员工工号'")
    private String empNo;

    @Column(name = "sys_user_name", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 员工工号'")
    private String realName;
}
