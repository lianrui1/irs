package cn.com.cowain.ibms.feign.task_center;

import cn.com.cowain.ibms.service.bean.schedule.req.DataSyncReq;
import cn.com.cowain.sfp.comm.JsonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

/**
 * @author Yang.Lee
 * @date 2021/6/21 17:02
 */
@Service
@RequestMapping({"/ms-task-center/v1"})
@FeignClient(value = "ms-task-center-schedule", url = "${cowain.gateway.address}")
public interface ScheduleApi {

    /**
     * 新建日程
     *
     * @param req 请求参数
     * @return 请求结果，返回的data为日程中心主键
     * @author Yang.Lee
     * @date 2021/6/21 17:17
     **/
    @PostMapping("/oa_schedule/add")
    JsonResult<String> create(@RequestBody DataSyncReq req);

    /**
     * 编辑日程
     *
     * @param req 请求参数
     * @return 请求结果
     * @author Yang.Lee
     * @date 2021/6/21 17:17
     **/
    @PostMapping("/oa_schedule/edit")
    JsonResult<String> update(@RequestBody DataSyncReq req);

    /**
     * 取消日程
     *
     * @param req 日程id
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/6/21 17:18
     **/
    @PostMapping("/oa_schedule/disable")
    JsonResult<String> delete(@RequestBody DataSyncReq req);
}
