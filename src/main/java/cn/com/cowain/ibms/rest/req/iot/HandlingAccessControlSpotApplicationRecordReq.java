package cn.com.cowain.ibms.rest.req.iot;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;

/**
 * @author wei.cheng
 * @date 2022/03/08 16:23
 */
@Data
@ApiModel("门禁点权限申请处理")
public class HandlingAccessControlSpotApplicationRecordReq {

    @ApiModelProperty(value = "审批状态，APPROVED:通过;DENIED:拒绝")
    @NotBlank
    @Pattern(regexp = "APPROVED|DENIED", message = "审批状态异常")
    private String status;

    @ApiModelProperty(value = "拒绝原因")
    @Length(max = 500, message = "拒绝原因不能超过500字")
    private String deniedReason;

    @ApiModelProperty(value = "权限开始时间", example = "2022-12-31")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate accessStartTime;

    @ApiModelProperty(value = "权限结束时间", example = "2024-12-31")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate accessEndTime;
}
