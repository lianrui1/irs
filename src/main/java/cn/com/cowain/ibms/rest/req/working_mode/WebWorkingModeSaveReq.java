package cn.com.cowain.ibms.rest.req.working_mode;

import cn.com.cowain.ibms.enumeration.iot.UsableStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 *  @title 
 *  @Description 描述
 *  @author jf.sui
 *  @Date  
 */
@Data
@ApiModel("新建工作模式对象")
public class WebWorkingModeSaveReq {


    @NotBlank
    @ApiModelProperty(value = "小图标url", required = true)
    private String imgUrl;

    @NotBlank
    @Length(max = 6,message = "名称最长为6")
    @ApiModelProperty(value = "场景名称", required = true)
    private String name;

    @ApiModelProperty(value = "场景介绍")
    private String purpose;



    @ApiModelProperty(value = "是否启用 DISABLED禁用 ENABLE启用")
    private UsableStatus usableStatus = UsableStatus.ENABLE;

    //@Size(min = 1, message = "设备不能为空")
    @NotEmpty
    @ApiModelProperty(value = "设备状态设置对象", required = true)
    private List<WebDeviceModeStatusSaveReq> saveReqs;
}
