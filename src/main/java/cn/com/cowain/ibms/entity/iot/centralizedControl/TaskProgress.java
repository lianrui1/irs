package cn.com.cowain.ibms.entity.iot.centralizedControl;

import lombok.Data;

@Data
public class TaskProgress {


    // 执行失败
    private int fail;

    // 执行成功
    private int success;

    // 执行总数
    private int total;

    // 已执行
    private int executed;

}
