package cn.com.cowain.ibms.rest.resp.iot.doorplate;

import cn.com.cowain.ibms.enumeration.iot.DoorPlateStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

/**
 * 会议详情列表数据（只显示部分会议信息）
 *
 * @author Yang.Lee
 * @date 2021/4/21 15:46
 */
@Data
@Builder
@ApiModel("会议详情列表数据")
public class DoorPlateStatusDetailResp {

    /**
     * 会议发起人姓名
     **/
    @ApiModelProperty(value = "会议发起人姓名", example = "张三", position = 1)
    private String initiator;

    /**
     * 会议发起人部门
     **/
    @ApiModelProperty(value = "会议发起人部门", example = "POD", position = 2)
    private String initiatorDepartment;

    /**
     * 头像
     **/
    @ApiModelProperty(value = "头像", example = "http://img.cowain.cn/avatar.jpg", position = 3)
    private String headImg;

    /**
     * 会议室名称
     **/
    @ApiModelProperty(value = "会议室名称", example = "天权", position = 4)
    private String roomName;

    /**
     * 开始时间
     */
    @ApiModelProperty(value = "开始时间", example = "07:00", position = 5)
    private String from;

    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间", example = "11:00", position = 6)
    private String to;

    /**
     * 会议主题
     */
    @ApiModelProperty(value = "会议主题", example = "需求评审", position = 7)
    private String topic;

    /**
     * 门牌状态
     **/
    @ApiModelProperty(name = "门牌状态", example = "FREE: 空闲; IN_PROCESS: 进行中; ABOUT_TO_BEGIN: 即将开始; END: 已结束", position = 8)
    private DoorPlateStatus status;

    @ApiModelProperty(name = "门牌状态名称描述", example = "已结束", position = 9)
    private String statusDescribe;
}
