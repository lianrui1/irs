package cn.com.cowain.ibms.dao;

import cn.com.cowain.ibms.entity.RoomService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Room Dao
 *
 * @author 张宇鑫
 * @version 1.0
 * @date 2020/8/3 19:36
 */
@Repository
public interface RoomServiceDao extends JpaRepository<RoomService, String> {

    /**
     * 根据Id查询 设备名称
     *
     * @param id
     * @return
     */
    Optional<RoomService> findById(String id);

}
