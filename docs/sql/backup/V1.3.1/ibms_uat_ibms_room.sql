-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 10.0.1.232    Database: ibms_uat
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ibms_room`
--

DROP TABLE IF EXISTS `ibms_room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ibms_room` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `created_by` varchar(36) DEFAULT NULL COMMENT '创建人',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(36) DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int(11) DEFAULT NULL COMMENT '版本号',
  `building` varchar(200) NOT NULL COMMENT '楼栋',
  `room_cap` int(11) NOT NULL COMMENT '人数容量',
  `devices` varchar(200) DEFAULT NULL COMMENT '包含设备',
  `floor` varchar(200) NOT NULL COMMENT '楼层',
  `is_open` int(11) DEFAULT NULL COMMENT '是否开放 0:否;1:是',
  `room_name` varchar(255) NOT NULL COMMENT '名称',
  `seq` int(11) DEFAULT NULL COMMENT '排序',
  `room_type` varchar(50) DEFAULT NULL COMMENT '房间类型',
  `space_id` varchar(36) DEFAULT NULL COMMENT 'space_id FK',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK64qgoeoomviu28r3dcdu13u4l` (`space_id`) USING BTREE,
  CONSTRAINT `FK64qgoeoomviu28r3dcdu13u4l` FOREIGN KEY (`space_id`) REFERENCES `iot_space` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='会议室';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ibms_room`
--

LOCK TABLES `ibms_room` WRITE;
/*!40000 ALTER TABLE `ibms_room` DISABLE KEYS */;
INSERT INTO `ibms_room` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `building`, `room_cap`, `devices`, `floor`, `is_open`, `room_name`, `seq`, `room_type`, `space_id`) VALUES ('11de355e-dd8e-4b3a-b6c7-483874d2cac2','5337','2020-12-30 20:23:57',0,NULL,'2020-12-30 20:23:57',1,'汇金大厦',20,NULL,'16',1,'长庚',2,NULL,'57452c93-af90-4ba7-bd02-4267eb20c417');
INSERT INTO `ibms_room` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `building`, `room_cap`, `devices`, `floor`, `is_open`, `room_name`, `seq`, `room_type`, `space_id`) VALUES ('4af6f5da-98f0-4fcc-9120-2b7f12b74426','5337','2020-12-31 08:10:19',0,NULL,'2020-12-31 08:10:19',1,'A',20,NULL,'3',1,'天玑',0,NULL,'17007a67-0272-4f83-bc76-fac755a106b5');
INSERT INTO `ibms_room` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `building`, `room_cap`, `devices`, `floor`, `is_open`, `room_name`, `seq`, `room_type`, `space_id`) VALUES ('6d9faaf8-9215-4402-aaaf-b2282a63bb87','5337','2020-12-31 09:47:19',1,NULL,'2020-12-31 09:47:19',1,'C',5,NULL,'3',1,'临时会议室',4,NULL,'15c5ad65-ab30-43b8-a5dc-38446e31a847');
INSERT INTO `ibms_room` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `building`, `room_cap`, `devices`, `floor`, `is_open`, `room_name`, `seq`, `room_type`, `space_id`) VALUES ('e8e1e58b-6db8-4ca4-8133-7146d70a2177','5337','2020-12-30 20:24:26',0,NULL,'2020-12-30 20:24:26',1,'汇金大厦',20,NULL,'16',1,'启明',3,NULL,'839b114d-87bc-4e3a-a8be-f274916d0416');
INSERT INTO `ibms_room` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `building`, `room_cap`, `devices`, `floor`, `is_open`, `room_name`, `seq`, `room_type`, `space_id`) VALUES ('ed2dc26f-cd02-4142-86e1-eee8e0753d63','5337','2020-12-30 20:23:18',0,NULL,'2020-12-30 20:23:18',1,'C',30,NULL,'3',1,'产品研发中心',1,NULL,'1f53db55-dc98-444b-8cef-74762900930b');
/*!40000 ALTER TABLE `ibms_room` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-25 15:53:40
