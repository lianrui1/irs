package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @author: cuiEnming
 * @title: DoorPlateReq
 * @date 2020/11/24  15:07
 * @since s2
 */
@Data
@ApiModel("门牌信息")
public class DoorPlateReq {

    /**
     * 门牌名称
     */
    @Pattern(regexp = "^[a-z0-9A-Z\u4e00-\u9fa5]{1,20}$", message = "门牌名称检验失败")
    @ApiModelProperty(value = "门牌名称", required = true)
    private String name;

    /**
     * 门派绑定会议室ID
     */
    @NotBlank(message = "会议室ID不能为空")
    @ApiModelProperty(value = "会议室ID", required = true, position = 1)
    private String roomId;

    @ApiModelProperty(value = "门磁编号", position = 2)
    private String doorMagne;

    @NotNull
    @ApiModelProperty(value = "设备密码", position = 3)
    private String password;

    @NotNull
    @ApiModelProperty(value = "硬件编号", position = 4, required = true)
    private String hardwareId;
}
