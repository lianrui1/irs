package cn.com.cowain.ibms.feign.iotc.bean;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * @author wei.cheng
 * @date 2022/03/25 13:49
 */
@Data
@Builder
public class FindPersonPicReq {

    @ApiModelProperty(value = "查询人员工号列表")
    private List<String> jobNos;
}
