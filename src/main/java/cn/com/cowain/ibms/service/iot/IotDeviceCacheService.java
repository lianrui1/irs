package cn.com.cowain.ibms.service.iot;

import cn.com.cowain.ibms.entity.iot.DoorMagnetic;
import cn.com.cowain.ibms.entity.iot.DoorPlate;
import cn.com.cowain.ibms.entity.iot.IotDevice;

import java.util.Optional;

public interface IotDeviceCacheService {
    void doorMagneticRedis(IotDevice iotDevice);

    void doorPlateRedis(IotDevice iotDevice);

    void deviceRedis(IotDevice iotDevice);

    Optional<DoorPlate> getDoorPlateToRedis(String doorPlateId);

    Optional<DoorMagnetic> getDoorMagneticToRedis(String dm);

    void redisDeviceTypeAdd(IotDevice iotDevice);

    void redisDeviceDataInfo(IotDevice iotDevice);
}
