package cn.com.cowain.ibms.dao.meetingfile;

import cn.com.cowain.ibms.entity.meeting.MeetingFile;
import cn.com.cowain.ibms.enumeration.meeting.FileType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author wei.cheng
 * @date 2022/02/23 14:44
 */
@Repository
public interface MeetingFileDao extends JpaRepository<MeetingFile, String> {
    /**
     * 查询reservationRecordId = {@code reservationRecordId}的meetingFile列表
     *
     * @param reservationRecordId
     * @return
     */
    List<MeetingFile> findAllByReservationRecordIdOrderByCreatedTimeAsc(String reservationRecordId);

    // 根据ID获取会议上传文件
    List<MeetingFile> findByReservationRecordIdAndFileType(String id, FileType url);

    List<MeetingFile> findAllByReservationRecordIdAndFileTypeOrderByCreatedTimeAsc(String reservationRecordId, FileType fileType);

    /**
     * 查询meetingFile列表
     *
     * @param reservationRecordId
     * @param creatorHrId
     * @param fileName
     * @param fileExt
     * @return
     */
    List<MeetingFile> findAllByReservationRecordIdAndCreatorHrIdAndFileNameAndFileExt(String reservationRecordId, String creatorHrId, String fileName,
                                                                                      String fileExt);
}
