package cn.com.cowain.ibms.rest.resp;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;


/**
 * Description
 *
 * @author 张宇鑫
 * @version 1.0
 * @since 2020-8-28 13:25:30
 */
@Data
@ApiModel("当天可预约最合适的时间")
public class AppointmentsTimeCanBeMdeResp implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 从时间
     */
    @ApiModelProperty(value = "检测开始时间", required = true, position = 0)
    private LocalTime from;

    @ApiModelProperty(value = "检测结束时间", required = true, position = 1)
    private LocalTime to;

    @ApiModelProperty(value = "年月日", required = true, position = 2)
    private LocalDate ytd;

}


