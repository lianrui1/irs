package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.enumeration.Condition;
import cn.com.cowain.ibms.enumeration.iot.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 门禁点
 *
 * @author Yang.Lee
 * @date 2021/11/7 15:07
 */
@Data
@Entity
@Table(name = "iot_access_control_spot")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_access_control_spot", comment = "门禁点")
public class AccessControlSpot extends BaseEntity {

    @Column(name = "name", nullable = false, columnDefinition = "varchar(30) COMMENT '门禁点名称'")
    private String name;

    @Column(name = "address", nullable = false, columnDefinition = "varchar(100) COMMENT '具体位置'")
    private String address;

    @ManyToOne
    @JoinColumn(name = "space_id", nullable = false, columnDefinition = "varchar(36) COMMENT '门禁点所属空间ID FK'")
    private Space space;

    @ManyToOne
    @JoinColumn(name = "device_id", columnDefinition = "varchar(36) COMMENT '门禁点关联设备ID FK'")
    private IotDevice device;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", columnDefinition = "varchar(20) COMMENT '门禁点类型'")
    private AccessControlSpotType accessControlSpotType;

    @Enumerated(EnumType.STRING)
    @Column(name = "spot_level", nullable = false, columnDefinition = "varchar(20) COMMENT '门禁点等级'")
    private AccessControlSpotLevel accessControlSpotLevel;

    @Enumerated(EnumType.STRING)
    @Column(name = "spot_place",  columnDefinition = "varchar(20) COMMENT '门禁点位置'")
    private AccessControlSpotPlace accessControlSpotPlace;

    @Enumerated(EnumType.STRING)
    @Column(name = "spot_in_out", columnDefinition = "varchar(20) COMMENT '门禁点进出类型'")
    private AccessControlInOut inOut;

    @Column(name = "properties", columnDefinition = "varchar(5000) COMMENT '门禁点属性'")
    private String properties;

    @Enumerated(EnumType.STRING)
    @Column(name = "auth_type", columnDefinition = "varchar(100) COMMENT '门禁点权限类型'")
    private AuthType authType;

    @Data
    public static class Property{


        /**
         * departmentCode, positionLevel
         **/
        private String name;

        private Object val;

        private Condition condition;
    }
}
