package cn.com.cowain.ibms.dao.distinguish;

import cn.com.cowain.ibms.entity.distinguish.AccessRules;
import cn.com.cowain.ibms.entity.distinguish.DeviceCloud;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

/**
 * @author Yang.Lee
 * @date 2021/3/10 10:34
 */
public interface AccessRulesDao extends JpaRepository<AccessRules, String>, JpaSpecificationExecutor<AccessRules> {

    /**
     * 根据设备查询通行权限
     *
     * @param deviceCloud 设备信息
     * @return 通行权限
     * @author Yang.Lee
     * @date 2021/3/20 10:49
     **/
    Optional<AccessRules> findByDeviceCloud(DeviceCloud deviceCloud);
}
