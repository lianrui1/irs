package cn.com.cowain.ibms.mq.bean;

import lombok.Data;

/**
 *
 * @author: yanzy
 * @date: 2022/4/7 16:34
 */
@Data
public class UserBindMessage {

    private String resourceId;

    private String failCode;

    private String failMsg;

    private String jobNo;
}
