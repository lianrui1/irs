package cn.com.cowain.ibms.enumeration;

/**
 * 房间类型
 *
 * @author 胡荆陵
 * @since 2020-07-30
 */
public enum RoomType {

    /**
     * 会议室(小型)
     */
    MEETING,
    /**
     * 会议室(大型),预留
     */
    CONFERENCE

}
