package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/7/12 13:30
 */
@Data
@Entity
@Table(name = "sensor_body_sensing_last_status")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "sensor_body_sensing_last_status", comment = "人体传感器最后一次状态记录表")
public class SensorBodySensingLastStatus extends BaseEntity {

    @Column(name = "is_people_there", nullable = false, columnDefinition = "varchar(10) COMMENT '是否有人 true有人  false无人'")
    private String isPeopleThere;

    @OneToOne
    @JoinColumn(name = "device_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK。设备表主键id'")
    private IotDevice device;

}
