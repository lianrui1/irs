package cn.com.cowain.ibms.feign.task_center.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wei.cheng
 * @date 2022/03/11 16:29
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccessControlSpotApplicationTaskData {
    /**
     * 门禁名称
     */
    private String spotName;
    /**
     * 门禁位置
     */
    private String spotAddress;
    /**
     * 申请时间
     */
    private String applicationTime;
}
