package cn.com.cowain.ibms.aop;

import com.alibaba.fastjson.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/21/20
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ValidationAdviceTest {

    @LocalServerPort
    private int port;

    @Autowired
    private RestTemplate restTemplate;

    private String getPath() {
        return "http://localhost:" + port + API_BASE;
    }

    @Test
    void handler() {
        // header
        String url = getPath() + "/reservation";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        // json
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("topic", "测试主题");
        jsonObj.put("date", "2000-12-31");
        jsonObj.put("from", "2000-12-31 08:00");
        jsonObj.put("to", "2000-12-31 09:00");
        jsonObj.put("roomId", "111223");
        // httpEntity
        HttpEntity<Object> httpEntity = new HttpEntity<>(jsonObj.toString(), headers);
        // 断言
        Assertions.assertThrows(HttpClientErrorException.class,
                () -> restTemplate.exchange(url, HttpMethod.POST, httpEntity, JSONObject.class));
    }

}
