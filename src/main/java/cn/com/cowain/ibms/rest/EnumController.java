package cn.com.cowain.ibms.rest;

import cn.com.cowain.ibms.enumeration.AlertCaptureorStatus;
import cn.com.cowain.ibms.enumeration.AlertSubType;
import cn.com.cowain.ibms.enumeration.AlertType;
import cn.com.cowain.ibms.enumeration.ApprovalStatus;
import cn.com.cowain.ibms.enumeration.integration_control.SelectDeviceType;
import cn.com.cowain.ibms.enumeration.iot.*;
import cn.com.cowain.ibms.enumeration.space.ProjectPropertyRightType;
import cn.com.cowain.ibms.rest.resp.BaseListResp;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author Yang.Lee
 * @date 2021/11/10 13:31
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/enum")
@Api(tags = IConst.MODULE_ENUM)
public class EnumController {

    @GetMapping("/list/{type}")
    @ApiOperation(value = "获取枚举列表", tags = {IConst.MODULE_ENUM, IConst.V20})
    public ResponseEntity<JsonResult<List<BaseListResp>>> getList(@PathVariable @ApiParam(value = "枚举类型.ProjectPropertyRightType:产权属性;SpaceType:空间类型;" +
            "AccessControlSpotType:门禁点类型;AccessControlSpotLevel:门禁点等级" +
            "PersonType:员工类型;EMPLOYEE,VISITOR； AccessControlInOut：门禁点进出类型； AccessControlSpotPlace： 门禁点工厂位置; ApprovalStatus: 审批状态 ; AlertType: 警戒抓怕类型" +
            "AlertCaptureorStatus:警戒抓拍状态；AlertSubType: 警戒抓拍子类型; SelectDeviceType: 选择设备类型") String type) {

        List<BaseListResp> enumList = new ArrayList<>();
        switch (type) {
            case "ProjectPropertyRightType":
                for (ProjectPropertyRightType obj : ProjectPropertyRightType.values()) {
                    enumList.add(new BaseListResp(obj.toString(), obj.getName()));
                }
                break;
            case "IntelligentOfficeStatus":
                for (IntelligentOfficeStatus obj : IntelligentOfficeStatus.values()) {
                    enumList.add(new BaseListResp(obj.toString(), obj.getName()));
                }
                break;
            case "AccessControlSpotType":
                for (AccessControlSpotType obj : AccessControlSpotType.values()) {
                    enumList.add(new BaseListResp(obj.toString(), obj.getName()));
                }
                break;
            case "AccessControlSpotLevel":
                for (AccessControlSpotLevel obj : AccessControlSpotLevel.values()) {
                    enumList.add(new BaseListResp(obj.toString(), obj.getName()));
                }
                break;
            case "SpaceType":
                for (SpaceType obj : SpaceType.values()) {
                    enumList.add(new BaseListResp(obj.toString(), obj.getName()));
                }
                break;
            case "PersonType":
                for (PersonType obj : PersonType.values()) {
                    enumList.add(new BaseListResp(obj.toString(), obj.getName()));
                }
                break;

            case "AccessControlInOut":
                for (AccessControlInOut obj : AccessControlInOut.values()) {
                    enumList.add(new BaseListResp(obj.toString(), obj.getName()));
                }
                break;

            case "AccessControlSpotPlace":
                for (AccessControlSpotPlace obj : AccessControlSpotPlace.values()) {
                    enumList.add(new BaseListResp(obj.toString(), obj.getName()));
                }
                break;

            case "ApprovalStatus":
                for (ApprovalStatus obj : ApprovalStatus.values()) {
                    enumList.add(new BaseListResp(obj.toString(), obj.getName()));
                }
                break;
            case "AlertType":
                for (AlertType obj : AlertType.values()) {
                    enumList.add(new BaseListResp(obj.toString(), obj.getName()));
                }
                break;

            case "AlertCaptureorStatus":
                for (AlertCaptureorStatus obj : AlertCaptureorStatus.values()) {
                    enumList.add(new BaseListResp(obj.toString(), obj.getName()));
                }
                break;

            case "AlertSubType":
                for (AlertSubType obj : AlertSubType.values()) {
                    enumList.add(new BaseListResp(obj.toString(), obj.getName()));
                }
                break;

            case "SelectDeviceType":
                for (SelectDeviceType obj : SelectDeviceType.values()) {
                    enumList.add(new BaseListResp(obj.toString(), obj.getName()));
                }
                break;

            default:

        }

        return ResponseEntity.ok(JsonResult.ok(enumList));
    }
}
