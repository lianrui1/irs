package cn.com.cowain.ibms.rest;

import cn.com.cowain.ibms.service.SMSService;
import cn.com.cowain.ibms.service.bean.ServiceMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.ibms.utils.RegexUtil;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * 短消息Controller
 *
 * @author Yang.Lee
 * @date 2021/7/5 13:44
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/sms")
@Api(tags = IConst.MODULE_SMS)
public class SMSController {

    @Resource
    private SMSService smsService;

    /**
     * 发送短信验证码
     *
     * @param mobile 手机号码
     * @return 发送结果
     * @author Yang.Lee
     * @date 2021/7/5 17:14
     **/
    @GetMapping("/{mobile}/pinNumber")
    @ApiOperation(value = "发送短信验证码", tags = IConst.MODULE_SMS)
    public ResponseEntity<JsonResult<String>> getPinNumber(@PathVariable String mobile) {

        if (!RegexUtil.match(mobile, RegexUtil.MOBILE_PHONE_NUMBER)) {
            return ResponseEntity.ok(JsonResult.error(ServiceMessage.MOBILE_FORMAT_ERROR, null, ErrConst.E01));
        }

        ServiceResult result = smsService.syncSendPinNumber(mobile);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(result.getObject().toString(), null, ErrConst.E21));
        }

        return ResponseEntity.ok(JsonResult.okNoData("发送成功"));
    }
}
