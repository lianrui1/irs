package cn.com.cowain.ibms.utils.websocket;

import org.java_websocket.drafts.Draft;
import org.java_websocket.drafts.Draft_17;
import org.java_websocket.handshake.ClientHandshakeBuilder;

/**
 * @author Yang.Lee
 * @date 2021/1/11 15:11
 */
@SuppressWarnings("deprecation")
public class DraftWithOrigin extends Draft_17 {
    private String originUrl;

    public DraftWithOrigin(String originUrl) {
        this.originUrl = originUrl;
    }

    @Override
    public Draft copyInstance() {
        return new DraftWithOrigin(originUrl);
    }


    @Override
    public ClientHandshakeBuilder postProcessHandshakeRequestAsClient(ClientHandshakeBuilder request) {
        super.postProcessHandshakeRequestAsClient(request);
        request.put("Origin", originUrl);
        return request;
    }
}
