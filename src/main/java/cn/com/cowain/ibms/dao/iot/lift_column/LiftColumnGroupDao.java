package cn.com.cowain.ibms.dao.iot.lift_column;

import cn.com.cowain.ibms.entity.iot.LiftColumnGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * 升降柱组DAO
 **/
@Repository
public interface LiftColumnGroupDao extends JpaRepository<LiftColumnGroup, String>, JpaSpecificationExecutor<LiftColumnGroup> {


}
