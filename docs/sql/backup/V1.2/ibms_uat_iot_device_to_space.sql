-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 10.0.1.232    Database: ibms_uat
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `iot_device_to_space`
--

DROP TABLE IF EXISTS `iot_device_to_space`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `iot_device_to_space` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `created_by` varchar(36) DEFAULT NULL COMMENT '创建人',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(36) DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int(11) DEFAULT NULL COMMENT '版本号',
  `device_id` varchar(36) NOT NULL COMMENT 'iot_device FK',
  `space_id` varchar(36) NOT NULL COMMENT 'iot_space FK',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FKf0qgdk39j72vng4wyqhed0ju8` (`device_id`) USING BTREE,
  KEY `FKbh9we3a8hfvj3e1tvblhy4hoi` (`space_id`) USING BTREE,
  CONSTRAINT `FKbh9we3a8hfvj3e1tvblhy4hoi` FOREIGN KEY (`space_id`) REFERENCES `iot_space` (`id`),
  CONSTRAINT `FKf0qgdk39j72vng4wyqhed0ju8` FOREIGN KEY (`device_id`) REFERENCES `iot_device` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='设备-to-空间映射表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `iot_device_to_space`
--

LOCK TABLES `iot_device_to_space` WRITE;
/*!40000 ALTER TABLE `iot_device_to_space` DISABLE KEYS */;
INSERT INTO `iot_device_to_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_id`, `space_id`) VALUES ('07a68c91-ae9c-4f91-b2ca-1cd1efeb964b',NULL,'2021-01-12 15:45:37',0,NULL,'2021-01-12 15:45:37',0,'1fc7e9ac-611f-493d-946d-a7fbbc636266','17007a67-0272-4f83-bc76-fac755a106b5');
INSERT INTO `iot_device_to_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_id`, `space_id`) VALUES ('0ffdd572-e86a-41cf-9b48-d7b5441d862a',NULL,'2021-01-12 15:45:47',0,NULL,'2021-01-12 15:45:47',0,'72aa01a1-7b55-48df-9f7c-c7fc4ccfd3fe','1f53db55-dc98-444b-8cef-74762900930b');
INSERT INTO `iot_device_to_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_id`, `space_id`) VALUES ('23f10242-75be-45d3-ba31-13c76c670b33',NULL,'2021-01-05 15:23:51',1,NULL,'2021-01-05 15:23:51',1,'511a8015-3993-4fa1-8151-f6543bb48d23','1f53db55-dc98-444b-8cef-74762900930b');
INSERT INTO `iot_device_to_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_id`, `space_id`) VALUES ('306b547a-d7f5-4813-8206-641d686c72b3',NULL,'2021-01-12 15:45:05',0,NULL,'2021-01-12 15:45:05',1,'6a1cd0d1-55f9-4986-92e8-d7458d9847da','17007a67-0272-4f83-bc76-fac755a106b5');
INSERT INTO `iot_device_to_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_id`, `space_id`) VALUES ('62cc44fc-f044-442d-872f-d9d67862586e',NULL,'2021-01-06 18:08:46',0,NULL,'2021-01-06 18:08:46',0,'4150c508-fc0d-407e-8fe4-4a34a8947cd1','1f53db55-dc98-444b-8cef-74762900930b');
INSERT INTO `iot_device_to_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_id`, `space_id`) VALUES ('697f011b-a516-4d13-acca-28a17350b03c',NULL,'2020-12-30 20:16:22',1,NULL,'2020-12-30 20:16:22',1,'e1068166-8389-4d87-b67b-4fde4f4f467a','877df00f-f3cc-4fd7-9e71-37a1fcc3d40f');
INSERT INTO `iot_device_to_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_id`, `space_id`) VALUES ('6bfc0761-5ada-4e8a-bbde-acf674512135',NULL,'2021-01-12 15:42:51',0,NULL,'2021-01-12 15:42:51',0,'6288bd48-cd0f-4da0-9a96-b53527289363','17007a67-0272-4f83-bc76-fac755a106b5');
INSERT INTO `iot_device_to_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_id`, `space_id`) VALUES ('7c94e671-ba04-4b60-b505-ccceb7ab6277',NULL,'2020-12-31 11:22:46',0,NULL,'2020-12-31 11:22:46',5,'5646bc13-31db-4f7a-85bb-f46dd148873a','17007a67-0272-4f83-bc76-fac755a106b5');
INSERT INTO `iot_device_to_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_id`, `space_id`) VALUES ('8ac60da5-720a-41cf-9420-03a05085b8a0',NULL,'2021-01-14 15:59:13',0,NULL,'2021-01-14 15:59:13',0,'3047883a-43de-4364-880b-320e8f5d50b9','17007a67-0272-4f83-bc76-fac755a106b5');
INSERT INTO `iot_device_to_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_id`, `space_id`) VALUES ('8d8080ed-38f1-496b-b38e-4539755d1ea9',NULL,'2021-01-06 18:41:36',0,NULL,'2021-01-06 18:41:36',0,'2f646c72-111f-4610-9b32-61fe8182de3b','1f53db55-dc98-444b-8cef-74762900930b');
INSERT INTO `iot_device_to_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_id`, `space_id`) VALUES ('8fb0bc0f-b466-426b-b9b7-383fd20f4e25',NULL,'2021-01-13 15:14:10',0,NULL,'2021-01-13 15:14:10',0,'716c30e4-4d09-48ed-b986-c1a922a323e4','17007a67-0272-4f83-bc76-fac755a106b5');
INSERT INTO `iot_device_to_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_id`, `space_id`) VALUES ('a9339d5d-ef49-4352-9ccd-c88ab5040a9f',NULL,'2021-01-21 16:23:16',0,NULL,'2021-01-21 16:23:16',0,'e36cbbbe-665b-4616-a221-8b94a41a855a','1f53db55-dc98-444b-8cef-74762900930b');
INSERT INTO `iot_device_to_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_id`, `space_id`) VALUES ('b7c231e3-8c07-4d2f-bb02-638088957fa8',NULL,'2021-01-13 09:37:50',1,NULL,'2021-01-13 09:37:50',0,'e7065df1-100a-4864-934b-b6e026d3c223','57452c93-af90-4ba7-bd02-4267eb20c417');
INSERT INTO `iot_device_to_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_id`, `space_id`) VALUES ('b991c02c-12fd-4b64-8b0e-af536926f1c4',NULL,'2020-12-30 20:29:00',0,NULL,'2020-12-30 20:29:00',1,'08ce2a99-c807-4ada-8976-9dec5ace9e88','839b114d-87bc-4e3a-a8be-f274916d0416');
INSERT INTO `iot_device_to_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_id`, `space_id`) VALUES ('c0a82cd4-002d-44eb-95d9-e6e590fdf647',NULL,'2021-01-08 09:23:18',0,NULL,'2021-01-08 09:23:18',0,'45741b54-c080-40b2-a804-13a6024a0aa2','17007a67-0272-4f83-bc76-fac755a106b5');
INSERT INTO `iot_device_to_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_id`, `space_id`) VALUES ('c6e349a9-0ac1-47e3-99c2-2379bf650ad1',NULL,'2021-01-06 17:45:31',0,NULL,'2021-01-06 17:45:31',0,'8ac1b16d-c38e-4317-8780-702234f8c0d6','1f53db55-dc98-444b-8cef-74762900930b');
INSERT INTO `iot_device_to_space` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `device_id`, `space_id`) VALUES ('e6f49c84-5f16-4791-9ddd-9be2fbff23c0',NULL,'2021-01-21 16:23:18',0,NULL,'2021-01-21 16:23:18',0,'e28739ed-358c-4770-aadd-13095490e5eb','1f53db55-dc98-444b-8cef-74762900930b');
/*!40000 ALTER TABLE `iot_device_to_space` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-21 19:00:37
