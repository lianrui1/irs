package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.space.Project;
import cn.com.cowain.ibms.entity.space.SpacePurpose;
import cn.com.cowain.ibms.enumeration.iot.SpaceType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * iot 空间表
 *
 * @author cuiEnming
 * @date 2020/11/19  18:51
 * @since s2
 */
@ToString(callSuper = true)
@Data
@Entity
@Table(name = "iot_space")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_space", comment = "空间表")
public class Space extends BaseEntity {

    /**
     * 空间名称
     */
    @Column(name = "s_name", nullable = false, columnDefinition = "varchar(36) COMMENT '空间名称'")
    private String name;

    /**
     * 空间编号
     */
    @Column(name = "s_no", nullable = false, columnDefinition = "varchar(8) COMMENT '空间编号'")
    private String number;

    /**
     * 空间层级
     */
    @Column(name = "s_level", nullable = false, columnDefinition = "int(2) COMMENT '空间层级'")
    private int level;

    @Column(name = "floor",  columnDefinition = "varchar(30) COMMENT '空间楼层'")
    private String floor;

    /**
     * 空间设备数
     */
    @Column(name = "deviceNum", nullable = false, columnDefinition = "int(2) COMMENT '空间设备数'")
    private int deviceNum = 0;

    /**
     * 备注信息
     */
    @Column(name = "s_remark", columnDefinition = "varchar(30) COMMENT '备注信息'")
    private String remark;
    /**
     *
     */
    @Column(name = "s_type", nullable = false, columnDefinition = "varchar(20) COMMENT '空间类型'")
    @Enumerated(EnumType.STRING)
    private SpaceType spaceType;

    /**
     *
     */
    @ManyToOne
    @JoinColumn(name = "parent_id", columnDefinition = "varchar(36) COMMENT 'FK'")
    private Space parent;

    @ManyToOne
    @JoinColumn(name = "project_id", columnDefinition = "varchar(36) COMMENT '项目编号 FK'")
    private Project project;

    @Column(name = "position", columnDefinition = "varchar(20) COMMENT '图纸位置'")
    private String position;

    @Column(name = "properties", columnDefinition = "text COMMENT '空间属性，因不同类型的空间属性值有差异，因此保存的是json字符'")
    private String properties;

    @ManyToOne
    @JoinColumn(name = "space_purpose_id", columnDefinition = "varchar(36) COMMENT '空间用途id FK'")
    private SpacePurpose spacePurpose;

    @Column(name = "introduction", columnDefinition = "varchar(200) COMMENT '简介'")
    private String introduction;

    @Column(name = "picture", columnDefinition = "varchar(200) COMMENT '图片'")
    private String picture;

    @Column(name = "is_show", columnDefinition = "tinyint(1) default 1 COMMENT '是否前端展示，1-是，0-否'")
    private int isShow;
}
