package cn.com.cowain.ibms.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 系统用户
 * 后面要从用户中心读取或同步
 *
 * @author 胡荆陵
 * @since 2020-7-30
 */
@Data
@Entity
@Table(name = "ibms_sys_user")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_sys_user", comment = "系统用户冗余表")
public class SysUser extends BaseEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 用户中心的全局唯一id
     */
    @Column(name = "sys_id", nullable = false, columnDefinition = "varchar(100) COMMENT 'EHR 的 stId'")
    private String sysId;

    /**
     * 员工编号
     */
    @Column(name = "emp_no", nullable = false, columnDefinition = "varchar(255) COMMENT '员工编号'")
    private String empNo;

    /**
     * 员工姓名 中文
     */
    @Column(name = "emp_name_zh", nullable = false, columnDefinition = "varchar(20) COMMENT '员工姓名中文'")
    private String nameZh;

    /**
     * 员工姓名 英文
     */
    @Column(name = "emp_name_en", columnDefinition = "varchar(30) COMMENT '员工姓名英文'")
    private String nameEn;

    /**
     * 部门ID，预留
     */
    @Column(name = "dept_id", nullable = false, columnDefinition = "varchar(100) COMMENT '部门ID'")
    private String deptId;

    /**
     * 部门名称
     */
    @Column(name = "dept_name", nullable = false, columnDefinition = "varchar(100) COMMENT '部门名称'")
    private String deptName;

    /**
     * WX open ID
     */
    @Column(name = "wx_open_id", columnDefinition = "varchar(100) COMMENT '微信 openId'")
    private String wxOpenId;

    /**
     * 工作等级名
     */
    @Column(name = "job_rank_name", columnDefinition = "varchar(100) COMMENT '工作等级名'")
    private String jobRankName;

    @Column(name = "uc_user_id", columnDefinition = "varchar(100) COMMENT 'UC中的用户id'")
    private String ucUserId;

    @Column(name = "wx_headImgUrl", columnDefinition = "varchar(256) COMMENT '微信头像 headImgUrl'")
    private String headImgUrl;

    @Column(name = "full_department_name", columnDefinition = "varchar(256) COMMENT '用户部门全称'")
    private String fullDepartmentName;

    @Column(name = "status", columnDefinition = "varchar(25) COMMENT '用户是否离职  leave离职'")
    private String status;

    @Column(name = "mobile", columnDefinition = "varchar(20) COMMENT '手机号码'")
    private String mobile;

    @Column(name = "work_photo", columnDefinition = "varchar(300) COMMENT '工作照图片'")
    private String workPhoto;
}
