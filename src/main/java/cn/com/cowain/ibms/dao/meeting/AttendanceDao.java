package cn.com.cowain.ibms.dao.meeting;

import cn.com.cowain.ibms.entity.meeting.Attendance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * 会议签到Dao
 *
 * @author Yang.Lee
 * @date 2021/6/15 11:14
 */
public interface AttendanceDao extends JpaRepository<Attendance, String> {

    /**
     * 根据会议ID查询会议签到数据
     *
     * @param reservationId 会议ID
     * @return 当前会议的签到数据
     * @author Yang.Lee
     * @date 2021/6/15 13:07
     **/
    List<Attendance> findByReservationRecordId(String reservationId);

    /**
     * 根据员工编号与会议id查询签到数据
     *
     * @param empNo         员工编号
     * @param reservationId 会议id
     * @return 签到记录
     * @author Yang.Lee
     * @date 2021/6/16 13:41
     **/
    Optional<Attendance> findByUserEmpNoAndReservationRecordId(String empNo, String reservationId);

//    String pageSql = "SELECT " +
//            "su.emp_no empNo, " +
//            "su.emp_name_zh name, " +
//            "su.full_department_name department, " +
//            "r.id as roomID, " +
//            "r.room_name roomName, " +
//            "p.id as projectID, " +
//            "p.project_name projectName, " +
//            "s.id as spaceID, " +
//            "s.s_name spaceName, " +
//            "rr.r_date meetingDate, " +
//            "rr.r_from startTime, " +
//            "rr.r_to endTime, " +
//            "rr.r_topic topic, " +
//            "ma.created_time AS signDatetime " +
//            "FROM " +
//            "( SELECT * FROM ibms_reservation_record_item rri WHERE ri_status <> 'CANCEL') t1 " +
//            "LEFT JOIN ibms_sys_user su ON t1.ri_owner_emp_no = su.emp_no " +
//            "LEFT JOIN ibms_reservation_record rr ON t1.record_id = rr.id " +
//            "LEFT JOIN ibms_room r ON r.id = rr.room_id " +
//            "LEFT JOIN iot_space s ON s.id = r.space_id " +
//            "LEFT JOIN ibms_project p ON p.id = s.project_id " +
//            "LEFT JOIN ibms_meeting_attendance ma ON ma.record_id = rr.id AND ma.user_emp_no = t1.ri_owner_emp_no " +
//            "WHERE " +
//            "1 = 1 " +
//            "AND rr.r_status <> 'CANCEL' " +
//            "AND if(:projectId != '', p.id = :projectId, 1=1) " +
//            "AND if(:roomId != '', r.id = :roomId, 1=1) " +
//            "AND if(:signState = 'NOT_SIGN', ma.created_time is null,  1=1) " +
//            "AND if(:signState = 'SIGNED', ma.created_time is not null, 1=1)" +
//            "AND if(:keyWord = '', 1=1, ( emp_no LIKE CONCAT('%',:keyWord,'%') OR emp_name_zh LIKE CONCAT('%',:keyWord,'%') OR full_department_name LIKE CONCAT('%',:keyWord,'%') OR r_topic LIKE CONCAT('%',:keyWord,'%') )) " +
//            "ORDER BY meetingDate DESC, startTime DESC "
//            ;
//
//    String pageCountSql = "SELECT " +
//            "count(*) " +
//            "FROM " +
//            "( SELECT * FROM ibms_reservation_record_item rri WHERE ri_status <> 'CANCEL') t1 " +
//            "LEFT JOIN ibms_sys_user su ON t1.ri_owner_emp_no = su.emp_no " +
//            "LEFT JOIN ibms_reservation_record rr ON t1.record_id = rr.id " +
//            "LEFT JOIN ibms_room r ON r.id = rr.room_id " +
//            "LEFT JOIN iot_space s ON s.id = r.space_id " +
//            "LEFT JOIN ibms_project p ON p.id = s.project_id " +
//            "LEFT JOIN ibms_meeting_attendance ma ON ma.record_id = rr.id AND ma.user_emp_no = t1.ri_owner_emp_no " +
//            "WHERE " +
//            "1 = 1 " +
//            "AND rr.r_status <> 'CANCEL' " +
//            "AND if(:projectId != '', p.id = :projectId, 1=1) " +
//            "AND if(:roomId != '', r.id = :roomId, 1=1) " +
//            "AND if(:signState = 'NOT_SIGN', ma.created_time is null,  1=1) " +
//            "AND if(:signState = 'SIGNED', ma.created_time is not null, 1=1)" +
//            "AND if(:keyWord = '', 1=1, ( emp_no LIKE CONCAT('%',:keyWord,'%') OR emp_name_zh LIKE CONCAT('%',:keyWord,'%') OR full_department_name LIKE CONCAT('%',:keyWord,'%') OR r_topic LIKE CONCAT('%',:keyWord,'%') )) "
//            ;

    String PAGESQL2 = "SELECT " +
            " t3.id record_id, " +
            " t3.room_id, " +
            " t3.emp_no empNo, " +
            " t3.`name`, " +
            " t3.meetingDate, " +
            " t3.startTime, " +
            " t3.endTime, " +
            " t3.topic, " +
            " t3.signDatetime, " +
            " r.id roomID, " +
            " r.`room_name` roomName, " +
            " s.id spaceID, " +
            " s.`s_name` spaceName, " +
            " p.id projectID, " +
            " p.`project_name` projectName, " +
            " t3.full_department_name  " +
            "FROM " +
            " ( " +
            " SELECT " +
            "  *  " +
            " FROM " +
            "  ( " +
            "  SELECT " +
            "   rr.id, " +
            "   rr.room_id, " +
            "   mgd.job_num emp_no, " +
            "   mgd.`name` `name`, " +
            "   '' AS full_department_name, " +
            "   rr.r_date meetingDate, " +
            "   rr.r_from startTime, " +
            "   rr.r_to endTime, " +
            "   rr.r_topic topic, " +
            "   ma.created_time AS signDatetime  " +
            "  FROM " +
            "   ibms_reservation_record rr " +
            "   LEFT JOIN ibms_meeting_invitation_card mic ON rr.id = mic.record_id " +
            "   LEFT JOIN ibms_meeting_guest_invitation_record mgir ON mgir.invitation_card_id = mic.id " +
            "   LEFT JOIN ibms_meeting_guest_detail mgd ON mgd.invitaion_id = mic.id " +
            "   LEFT JOIN ibms_meeting_attendance ma ON ma.record_id = rr.id  " +
            "   AND ma.user_emp_no = mgd.job_num  " +
            "  WHERE " +
            "   rr.record_type = 1  " +
            "   AND rr.r_status <> 'CANCEL'  " +
            "   AND mgir.invitation_status = 'ACCEPTED'  " +
            "   AND rr.is_del = 0  " +
            "   AND mic.is_del = 0  " +
            "   AND mgir.is_del = 0  " +
            "   AND mgd.is_del = 0  " +
            "  ) t1 UNION ALL " +
            " SELECT " +
            "  *  " +
            " FROM " +
            "  ( " +
            "  SELECT " +
            "   rr.id, " +
            "   rr.room_id, " +
            "   su.emp_no, " +
            "   su.emp_name_zh `name`, " +
            "   su.full_department_name, " +
            "   rr.r_date meetingDate, " +
            "   rr.r_from startTime, " +
            "   rr.r_to endTime, " +
            "   rr.r_topic topic, " +
            "   ma.created_time AS signDatetime  " +
            "  FROM " +
            "   ibms_reservation_record rr " +
            "   LEFT JOIN ibms_reservation_record_item rri ON rr.id = rri.record_id " +
            "   LEFT JOIN ibms_sys_user su ON su.emp_no = rri.ri_owner_emp_no " +
            "   LEFT JOIN ibms_meeting_attendance ma ON ma.record_id = rr.id  " +
            "   AND ma.user_emp_no = su.emp_no  " +
            "  WHERE " +
            "   1=1  " +
            "   AND rr.r_status <> 'CANCEL'  " +
            "   AND rri.ri_status <> 'CANCEL'  " +
            "   AND rr.is_del = 0  " +
            "   AND rri.is_del = 0  " +
            "  ) t2  " +
            " ) t3 " +
            " LEFT JOIN ibms_room r ON r.id = t3.room_id " +
            " LEFT JOIN iot_space s ON s.id = r.space_id " +
            " LEFT JOIN ibms_project p ON p.id = s.project_id  " +
            "WHERE " +
            " 1 = 1  " +
            "AND if(:projectId != '', p.id = :projectId, 1=1) " +
            "AND if(:roomId != '', r.id = :roomId, 1=1) " +
            "AND if(:signState = 'NOT_SIGN', signDatetime is null,  1=1) " +
            "AND if(:signState = 'SIGNED',signDatetime is not null, 1=1) " +
            "AND if(:keyWord = '', 1=1, ( emp_no LIKE CONCAT('%',:keyWord,'%') OR `name` LIKE CONCAT('%',:keyWord,'%') OR full_department_name LIKE CONCAT('%',:keyWord,'%') OR topic LIKE CONCAT('%',:keyWord,'%') )) " +
            "ORDER BY " +
            " meetingDate DESC, " +
            " startTime DESC";

    String PAGECOUNTSQL2 = "SELECT " +
            "count(*) " +
            "FROM " +
            " ( " +
            " SELECT " +
            "  *  " +
            " FROM " +
            "  ( " +
            "  SELECT " +
            "   rr.id, " +
            "   rr.room_id, " +
            "   mgd.job_num emp_no, " +
            "   mgd.`name` `name`, " +
            "   '' AS full_department_name, " +
            "   rr.r_date meetingDate, " +
            "   rr.r_from startTime, " +
            "   rr.r_to endTime, " +
            "   rr.r_topic topic, " +
            "   ma.created_time AS signDatetime  " +
            "  FROM " +
            "   ibms_reservation_record rr " +
            "   LEFT JOIN ibms_meeting_invitation_card mic ON rr.id = mic.record_id " +
            "   LEFT JOIN ibms_meeting_guest_invitation_record mgir ON mgir.invitation_card_id = mic.id " +
            "   LEFT JOIN ibms_meeting_guest_detail mgd ON mgd.invitaion_id = mic.id " +
            "   LEFT JOIN ibms_meeting_attendance ma ON ma.record_id = rr.id  " +
            "   AND ma.user_emp_no = mgd.job_num  " +
            "  WHERE " +
            "   rr.record_type = 1  " +
            "   AND rr.r_status <> 'CANCEL'  " +
            "   AND mgir.invitation_status = 'ACCEPTED'  " +
            "   AND rr.is_del = 0  " +
            "   AND mic.is_del = 0  " +
            "   AND mgir.is_del = 0  " +
            "   AND mgd.is_del = 0  " +
            "  ) t1 UNION ALL " +
            " SELECT " +
            "  *  " +
            " FROM " +
            "  ( " +
            "  SELECT " +
            "   rr.id, " +
            "   rr.room_id, " +
            "   su.emp_no, " +
            "   su.emp_name_zh `name`, " +
            "   su.full_department_name, " +
            "   rr.r_date meetingDate, " +
            "   rr.r_from startTime, " +
            "   rr.r_to endTime, " +
            "   rr.r_topic topic, " +
            "   ma.created_time AS signDatetime  " +
            "  FROM " +
            "   ibms_reservation_record rr " +
            "   LEFT JOIN ibms_reservation_record_item rri ON rr.id = rri.record_id " +
            "   LEFT JOIN ibms_sys_user su ON su.emp_no = rri.ri_owner_emp_no " +
            "   LEFT JOIN ibms_meeting_attendance ma ON ma.record_id = rr.id  " +
            "   AND ma.user_emp_no = su.emp_no  " +
            "  WHERE " +
            "   1=1  " +
            "   AND rr.r_status <> 'CANCEL'  " +
            "   AND rri.ri_status <> 'CANCEL'  " +
            "   AND rr.is_del = 0  " +
            "   AND rri.is_del = 0  " +
            "  ) t2  " +
            " ) t3 " +
            " LEFT JOIN ibms_room r ON r.id = t3.room_id " +
            " LEFT JOIN iot_space s ON s.id = r.space_id " +
            " LEFT JOIN ibms_project p ON p.id = s.project_id  " +
            "WHERE " +
            " 1 = 1  " +
            "AND if(:projectId != '', p.id = :projectId, 1=1) " +
            "AND if(:roomId != '', r.id = :roomId, 1=1) " +
            "AND if(:signState = 'NOT_SIGN', signDatetime is null,  1=1) " +
            "AND if(:signState = 'SIGNED',signDatetime is not null, 1=1) " +
            "AND if(:keyWord = '', 1=1, ( emp_no LIKE CONCAT('%',:keyWord,'%') OR `name` LIKE CONCAT('%',:keyWord,'%') OR full_department_name LIKE CONCAT('%',:keyWord,'%') OR topic LIKE CONCAT('%',:keyWord,'%') )) " +
            "ORDER BY " +
            " meetingDate DESC, " +
            " startTime DESC";

    @Query(value = PAGESQL2, nativeQuery = true, countQuery = PAGECOUNTSQL2)
    Page<Map<String,Object>> selectAttendancePage(@Param("projectId") String projectId,
                                                        @Param("roomId") String roomId,
                                                        @Param("signState") String signState,
                                                        @Param("keyWord") String keyWord,
                                                        Pageable pageable);
}
