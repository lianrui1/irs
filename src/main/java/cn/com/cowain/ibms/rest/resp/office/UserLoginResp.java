package cn.com.cowain.ibms.rest.resp.office;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/12/20 17:00
 */
@Data
@ApiModel("智能会议室小程序登录返回对象")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserLoginResp {

    @ApiModelProperty(value = "用户工号", example = "CWA8888", position = 1)
    private String userHrId;

    @ApiModelProperty(value = "用户姓名", example = "张三", position = 2)
    private String userName;

    @ApiModelProperty(value = "用户头像", example = "https://1/2/3.jpg", position = 3)
    private String headImgUrl;

    @ApiModelProperty(value = "部门名称", example = "PD / ROT", position = 4)
    private String departmentName;

    @ApiModelProperty(value = "用户所拥有的智能办公室列表", position = 5)
    private List<UserOfficeInfo> officeList;

    @ApiModelProperty(value = "身份令牌", position = 6)
    private String authorization;

    @ApiModelProperty(value = "是否已执行过引导操作。1：是；0否", position = 7)
    private int isGuide;

    @Data
    public static class UserOfficeInfo{

        @ApiModelProperty(value = "办公室ID", example = "123456", position = 1)
        private String id;

        @ApiModelProperty(value = "办公室名称", example = "奥林匹斯", position = 2)
        private String name;

        @ApiModelProperty(value = "用户是否是admin。1：是；0否", example = "1", position = 3)
        private int isAdmin;

        @ApiModelProperty(value = "办公室是否已初始化。1：是；0否", example = "1", position = 4)
        private int isInit;

        @ApiModelProperty(value = "办公室所属空间ID", example = "10", position = 5)
        private String spaceId;
    }
}
