package cn.com.cowain.ibms.service.office.impl;

import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.rest.resp.device.centralized.CentralizedControlDeviceFilter;
import cn.com.cowain.ibms.service.bean.DoorMagneticHWReq;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.device.HwProductsService;
import cn.com.cowain.ibms.service.office.DevicePropertiesService;
import cn.com.cowain.ibms.utils.DataUtils;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.ibms.utils.RedisUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service
public class DevicePropertiesServiceImpl implements DevicePropertiesService {
    @Autowired
    private DeviceDao deviceDao;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private HwProductsService productsService;

    /**
     * 从Redis获取设备属性
     * @param hwDeviceId
     * @param tClass
     * @param <T>
     * @return
     */
    @Override
    public <T>T getPropertiesFromDeviceRedis(String hwDeviceId, Class<T> tClass) {
        Optional<IotDevice> iotDeviceOptional = deviceDao.findByHwDeviceId(hwDeviceId);
        if (iotDeviceOptional.isPresent()) {
            String iotDeviceKey = redisUtil.createDeviceRealKey(IConst.DEVICE_MODULE, String.valueOf(iotDeviceOptional.get().getDeviceType()), hwDeviceId, RedisUtil.Separator.COLON);
            if (redisUtil.hasKey(iotDeviceKey)) {
                Map<Object, Object> deviceMap = redisUtil.hmget(iotDeviceKey);
                T deviceDetailResp = DataUtils.mapCastTargetObject(deviceMap, tClass);
                if (deviceDetailResp == null) {
                    log.info("conditionerStatusDetailResp对象转换异常，从api获取！");
                    return null;
                }
                return deviceDetailResp;
            } else {
                return null;

            }
        } else {
            return null;
        }
    }

    @Override
    public <T> T getPropertiesFromDeviceRedis(String hwDeviceId, DeviceType deviceType, Class<T> tClass) {
        if(StrUtil.isBlank(hwDeviceId)){
            return null;
        }
        if(Objects.isNull(deviceType)){
            return getPropertiesFromDeviceRedis(hwDeviceId, tClass);
        }
        String iotDeviceKey = redisUtil.createDeviceRealKey(IConst.DEVICE_MODULE, deviceType.name(), hwDeviceId, RedisUtil.Separator.COLON);
        if (redisUtil.hasKey(iotDeviceKey)) {
            Map<Object, Object> deviceMap = redisUtil.hmget(iotDeviceKey);
            T deviceDetailResp = DataUtils.mapCastTargetObject(deviceMap, tClass);
            if (deviceDetailResp == null) {
                log.info("conditionerStatusDetailResp对象转换异常，从api获取！");
                return null;
            }
            return deviceDetailResp;
        } else {
            return null;
        }
    }

    @Override
    public <T> T getPropertiesFromDeviceRedis(IotDevice iotDevice, Class<T> tClass) {
        if(StrUtil.isBlank(iotDevice.getHwDeviceId())|| Objects.isNull(iotDevice.getDeviceType())){
            return null;
        }
        String iotDeviceKey = redisUtil.createDeviceRealKey(IConst.DEVICE_MODULE, iotDevice.getDeviceType().name(), iotDevice.getHwDeviceId(), RedisUtil.Separator.COLON);
        if (redisUtil.hasKey(iotDeviceKey)) {
            Map<Object, Object> deviceMap = redisUtil.hmget(iotDeviceKey);
            T deviceDetailResp = DataUtils.mapCastTargetObject(deviceMap, tClass);
            if (deviceDetailResp == null) {
                log.info("conditionerStatusDetailResp对象转换异常，从api获取！");
                return null;
            }
            return deviceDetailResp;
        } else {
            return null;
        }
    }

    /**
     * 从api中获取后将属性加入Redis
     * 该方法中包含查询api的过程，只适用于部分接口
     * @param iotDevice
     * @param respClass
     * @param <T>
     * @return
     */
    @Override
    public <T>T addPropertiesToDeviceRedis(IotDevice iotDevice, Class<T> respClass) {
        String iotDeviceKey = redisUtil.createDeviceRealKey(IConst.DEVICE_MODULE, String.valueOf(iotDevice.getDeviceType()), iotDevice.getHwDeviceId(), RedisUtil.Separator.COLON);

        log.info("Redis中设备属性不存在，从api获取！");
        T deviceDetailResp=findPropertiesFromApi(iotDevice);
        if(deviceDetailResp==null){
            log.info("设备{}需要转换的对象为空",iotDevice.getDeviceName());
            return null;
        }
        Map<String,Object> map=DataUtils.targetObjectCastMap(deviceDetailResp,respClass);

        map.put("hwDeviceId",iotDevice.getHwDeviceId());

        log.info("将{}存入Redis",deviceDetailResp);
        redisUtil.hmset(iotDeviceKey,map,true);
        return deviceDetailResp;
    }
    /**
     * 将查询结果属性加入Redis
     * 此方法中没有调用查询的过程，而是在参数中传入已经查询好的设备信息
     * @param iotDevice
     * @param respClass
     * @param <T>
     * @return
     */
    @Override
    public <T>T addPropertiesToDeviceRedis(IotDevice iotDevice, T deviceDetailResp,Class<T> respClass) {
        String iotDeviceKey = redisUtil.createDeviceRealKey(IConst.DEVICE_MODULE, String.valueOf(iotDevice.getDeviceType()), iotDevice.getHwDeviceId(), RedisUtil.Separator.COLON);

        log.info("Redis中设备属性不存在，从api获取！");
        //T deviceDetailResp=findPropertiesFromApi(iotDevice);
        if(deviceDetailResp==null){
            log.info("设备{},对象为空",iotDevice.getDeviceName());
            return null;
        }
        Map<String,Object> map=DataUtils.targetObjectCastMap(deviceDetailResp,respClass);
        if (map.isEmpty()){
            map.put("hwDeviceId",iotDevice.getHwDeviceId());
        }
        redisUtil.hmset(iotDeviceKey,map,true);
        return deviceDetailResp;
    }

    /**
     * 筛选设备：离线设备，无效操作设备，和有效设备
     * @param iotDevice
     * @param paras
     * @param controlDeviceFilter
     */
    @Override
    public void filterDevice(IotDevice iotDevice, Map<String, String> paras, CentralizedControlDeviceFilter controlDeviceFilter) {
        if(StringUtils.isEmpty(paras.get(IConst.DICT_STATUS))||StringUtils.equals(paras.get(IConst.DICT_STATUS), IConst.STATUS_OFFLINE)){
            List<IotDevice> iotDevices=controlDeviceFilter.getOffLineDevices();
            if(CollUtil.isEmpty(iotDevices)){
                iotDevices=new LinkedList<>();
            }
            iotDevices.add(iotDevice);
            controlDeviceFilter.setOffLineDevices(iotDevices);
        }
        else if (StringUtils.isEmpty(paras.get(IConst.DICT_POWER))||StringUtils.equals(paras.get(IConst.DICT_POWER), paras.get(IConst.DICT_TARGET_POWER))){
            List<IotDevice> iotDevices=controlDeviceFilter.getInvalidDevices();
            if(CollUtil.isEmpty(iotDevices)){
                iotDevices=new LinkedList<>();
            }
            iotDevices.add(iotDevice);
            controlDeviceFilter.setInvalidDevices(iotDevices);
        }else {
            List<IotDevice> iotDevices=controlDeviceFilter.getValidDevices();
            if(CollUtil.isEmpty(iotDevices)){
                iotDevices=new LinkedList<>();
            }
            iotDevices.add(iotDevice);
            controlDeviceFilter.setValidDevices(iotDevices);
        }
    }

    /**
     * 从接口中获取属性
     * @param iotDevice
     * @param <T>
     * @return
     */
    private <T>T findPropertiesFromApi(IotDevice iotDevice) {
        T devicePropertiesResp;
        ServiceResult serviceResult=productsService.findProductProperties(iotDevice);
        if (serviceResult.isSuccess()){
            DoorMagneticHWReq req = (DoorMagneticHWReq) serviceResult.getObject();
            ServiceResult result = productsService.findByDeviceAndServiceId(iotDevice, req.getService_id());
            if (result.isSuccess()){
                 devicePropertiesResp = (T) result.getObject();
            }else {
                return null;
            }
            return devicePropertiesResp;
        }else {
            return null;
        }
    }


}
