package cn.com.cowain.ibms.service.meal.impl;

import cn.com.cowain.ibms.dao.meal.KshtTimePlanDao;
import cn.com.cowain.ibms.dao.meal.MealGroupDao;
import cn.com.cowain.ibms.entity.MessageBody;
import cn.com.cowain.ibms.entity.MessagePoolRecord;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.meal.KsHtTimePlan;
import cn.com.cowain.ibms.entity.meal.MealGroup;
import cn.com.cowain.ibms.enumeration.iot.PersonType;
import cn.com.cowain.ibms.feign.ks_conn.KSConnApi;
import cn.com.cowain.ibms.mq.bean.OpenAbilityMessage;
import cn.com.cowain.ibms.rest.resp.meal.HtAcrossPermissionConfigReq;
import cn.com.cowain.ibms.service.MessageSendService;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.meal.MealService;
import cn.com.cowain.ibms.service.space.IotDeviceService;
import cn.com.cowain.ibms.utils.DateUtils;
import cn.com.cowain.ibms.utils.ErrConst;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Stopwatch;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author wei.cheng
 * @date 2022/06/06 18:22
 */
@Slf4j
@Service
public class MealServiceImpl implements MealService {
    @Resource
    private IotDeviceService iotDeviceService;

    @Resource
    private MealGroupDao mealGroupDao;

    @Resource
    private KshtTimePlanDao kshtTimePlanDao;

    @Value("#{'${meal.fail-alert}'.split(',')}")
    private List<String> alertHrIds;

    @Resource
    private MessageSendService messageSendService;
    @Value("${cowain.send.message.vx.msgTemplateId}")
    private String vxMsgTemplateId;

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserServiceUC;

    @Resource
    private KSConnApi ksConnApi;

    /**
     * @param mealList
     * @return
     * @description 旷视鸿图权限配置
     * @author tql
     * @date 22-5-19
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public synchronized ServiceResult permissionConfig(List<OpenAbilityMessage.Meal> mealList) {
        HtAcrossPermissionConfigReq configReq = new HtAcrossPermissionConfigReq();
        HtAcrossPermissionConfigReq.DeleteConfig deleteConfig = new HtAcrossPermissionConfigReq.DeleteConfig();
        List<HtAcrossPermissionConfigReq.AddConfig> addConfigsList = new ArrayList();
        List<String> deletedTimePlanIdList = new ArrayList();
        List<String> deletedGroupIdList = new ArrayList();

        List<KsHtTimePlan> ksHtTimePlanDaoAll = kshtTimePlanDao.findAll();
        List<MealGroup> mealGroups = mealGroupDao.findAll();
        List<OpenAbilityMessage.Meal> addList = new ArrayList<>();
        List<KsHtTimePlan> delList = new ArrayList<>();
        // 判断出需要创建的时间计划
        mealList.forEach(obj -> {
            if (CollectionUtils.isEmpty(ksHtTimePlanDaoAll)) {
                addList.add(obj);
            } else {
                // 判断出不一模一样的数据
                if (!checkMealData(obj, ksHtTimePlanDaoAll)) {
                    addList.add(obj);
                }
            }
        });

        if (!CollectionUtils.isEmpty(ksHtTimePlanDaoAll)) {
            // 判断出需要删除的数据
            ksHtTimePlanDaoAll.forEach(obj -> {
                // 判断出不一模一样的数据
                if (!checkTimePlaneData(obj, mealList)) {
                    delList.add(obj);
                }
            });
        }

        // 删除人员组和时间计划添加
        delList.forEach(obj -> {
            deletedTimePlanIdList.add(obj.getId());
            List<String> list = mealGroups.stream().filter(e -> e.getKsHtTimePlan().getId().equals(obj.getId())).map(MealGroup::getId).collect(Collectors.toList());
            deletedGroupIdList.addAll(list);
        });
        // 删除组
        deleteConfig.setGroupBizIds(deletedGroupIdList);
        // 删除时间计划
        deleteConfig.setTimePlanBizIds(deletedTimePlanIdList);
        // 添加时间计划
        for (OpenAbilityMessage.Meal mealPerson : addList) {
            List<String> devices = iotDeviceService.getKsHtPlaceDevice(mealPerson.getPlace());
            if (!devices.isEmpty()) {
                // 创建访客/员工权限配置对象
                HtAcrossPermissionConfigReq.AddConfig addConfig = new HtAcrossPermissionConfigReq.AddConfig();
                HtAcrossPermissionConfigReq.GroupConfig visitorConfig = new HtAcrossPermissionConfigReq.GroupConfig();
                HtAcrossPermissionConfigReq.GroupConfig employeeConfig = new HtAcrossPermissionConfigReq.GroupConfig();
                KsHtTimePlan KsHtTimePlan = new KsHtTimePlan();
                // 创建访客报餐组,员工报餐组
                MealGroup visitorGroup = new MealGroup();
                MealGroup employeeGroup = new MealGroup();
                List<HtAcrossPermissionConfigReq.GroupConfig> arrayList = new ArrayList<>();
                // 新增计划配置
                String timePlanName = mealPerson.getPlace() + "_" + mealPerson.getStartTime() + "_" + mealPerson.getEndTime();

                // copy时间计划属性
                BeanUtils.copyProperties(mealPerson, KsHtTimePlan);
                KsHtTimePlan.setName(timePlanName);
                kshtTimePlanDao.save(KsHtTimePlan);

                String visitorGroupName = mealPerson.getPlace() + "_" + mealPerson.getStartTime() + "_" + mealPerson.getEndTime() + "_报餐" + PersonType.VISITOR
                        .getName() + "组";
                String employeeGroupName = mealPerson.getPlace() + "_" + mealPerson.getStartTime() + "_" + mealPerson.getEndTime() + "_报餐" + PersonType.EMPLOYEE
                        .getName() + "组";
                // 访客报餐组
                visitorGroup.setName(visitorGroupName);
                visitorGroup.setKsHtTimePlan(KsHtTimePlan);
                // 员工报餐组
                employeeGroup.setName(employeeGroupName);
                employeeGroup.setKsHtTimePlan(KsHtTimePlan);
                mealGroupDao.save(visitorGroup);
                mealGroupDao.save(employeeGroup);

                // 访客业务id
                visitorConfig.setGroupBizId(visitorGroup.getId());
                // 访客组名称
                visitorConfig.setGroupName(visitorGroupName);
                // 访客业务id
                employeeConfig.setGroupBizId(employeeGroup.getId());
                // 访客组名称
                employeeConfig.setGroupName(employeeGroupName);
                // 添加访客报餐组配置
                arrayList.add(visitorConfig);
                // 添加员工报餐组配置
                arrayList.add(employeeConfig);
                // 时间计划id
                addConfig.setTimePlanBizId(KsHtTimePlan.getId());
                // 时间计划名
                addConfig.setTimePlanName(timePlanName);
                // 开始时间
                addConfig.setStartTime(mealPerson.getStartTime());
                // 结束时间
                addConfig.setEndTime(mealPerson.getEndTime());
                // 关联的设备id
                // 获取地区的报餐设备
                addConfig.setDeviceIds(devices);
                addConfig.setGroupConfigs(arrayList);
                // 添加配置
                addConfigsList.add(addConfig);
            }
        }

        configReq.setDeleteConfigs(deleteConfig);
        // 添加权限组
        configReq.setAddConfigs(addConfigsList);
        if (CollectionUtils.isEmpty(configReq.getAddConfigs()) && CollectionUtils.isEmpty(deleteConfig.getGroupBizIds()) &&
                CollectionUtils.isEmpty(deleteConfig.getTimePlanBizIds())) {
            log.info("旷世报餐权限未发生变更");
            return ServiceResult.ok("旷世报餐权限未发生变更");
        }
        log.info("旷视鸿图权限配置请求参数:{}", JSON.toJSONString(configReq));

//        // 旷视鸿图参数配置
//        GroupBindingReq param = GroupBindingReq.builder()
//                .chainSite("KS")
//                .methodCode(MethodCode.CONFIG_PERMISSION.getName())
//                .moduleCode(ModuleCode.KSHT.getName())
//                .jsonArg(JSON.toJSONString(configReq))
//                .channelType("2")
//                .build();
//        log.info("IOTC请求参数:{}", JSON.toJSONString(param));
        // 发送IotC接口请求
       cn.com.cowain.ibms.rest.bean.JsonResult<Object> result;
        Stopwatch stopwatch = Stopwatch.createStarted();
        try {
            result = ksConnApi.configPermission(configReq);
            log.info("旷视鸿图接口返回数据:{}, 耗时：{} ms", JSONObject.toJSONString(result), stopwatch.elapsed(TimeUnit.MILLISECONDS));
            if (result.getStatus() == 0) {
                // 数据回滚
                rollbackData(addConfigsList);
                sendMessage("旷世报餐权限配置失败", result.getMsg());
                return ServiceResult.error(result.getMsg(), result.getCode());
            } else {
                // 数据处理
                checkData(delList);
            }
            return ServiceResult.ok();
        } catch (Exception e) {
            log.error("调用权限配置接口异常，msg:{}", e.getMessage(), e);
            log.info("耗时：{} ms", stopwatch.elapsed(TimeUnit.MILLISECONDS));
            // 数据回滚
            rollbackData(addConfigsList);
            sendMessage("旷世报餐权限配置失败", e.getMessage());
            return ServiceResult.error("权限配置失败", ErrConst.E13);
        }
    }


    /**
     * @param meal
     * @param ksHtTimePlan 检查旷视鸿图报餐数据
     * @return
     * @description
     * @author tql
     * @date 22-5-19
     */
    private boolean checkMealData(OpenAbilityMessage.Meal meal, List<KsHtTimePlan> ksHtTimePlan) {
        boolean dataStatus = false;
        for (KsHtTimePlan value : ksHtTimePlan) {
            if (meal.getPlace() == value.getPlace() && meal.getStartTime().equals(value.getStartTime()) && meal.getEndTime().equals(value.getEndTime())) {
                // 需要新增的时间计划
                dataStatus = true;
            }
        }
        return dataStatus;
    }


    /**
     * @param timePlan
     * @param mealList
     * @return
     * @description 检查本地报餐数据是否减少
     * @author tql
     * @date 22-5-19
     */
    private boolean checkTimePlaneData(KsHtTimePlan timePlan, List<OpenAbilityMessage.Meal> mealList) {
        boolean dataStatus = false;
        for (OpenAbilityMessage.Meal value : mealList) {
            if (timePlan.getPlace() == value.getPlace() && timePlan.getStartTime().equals(value.getStartTime()) && timePlan.getEndTime().equals(value.getEndTime())) {
                dataStatus = true;
            }
        }
        return dataStatus;
    }

    /**
     * @param delList
     * @return
     * @description 成功返回数据处理
     * @author tql
     * @date 22-5-19
     */
    private void checkData(List<KsHtTimePlan> delList) {
        // 删除数据不为空
        if (!CollectionUtils.isEmpty(delList)) {
            delList.forEach(obj -> {
                List<MealGroup> mealGroups = mealGroupDao.findByKsHtTimePlan(obj);
                mealGroups.forEach(value -> {
                    value.setIsDelete(1);
                    mealGroupDao.save(value);
                });
                obj.setIsDelete(1);
                kshtTimePlanDao.save(obj);
            });
        }
    }

    /**
     * @param addConfigsList
     * @return
     * @description 请求失败数据回滚
     * @author tql
     * @date 22-5-24
     */
    private void rollbackData(List<HtAcrossPermissionConfigReq.AddConfig> addConfigsList) {
        // 删除数据不为空
        if (!CollectionUtils.isEmpty(addConfigsList)) {
            addConfigsList.forEach(obj -> {
                Optional<KsHtTimePlan> ksHtTimePlan = kshtTimePlanDao.findById(obj.getTimePlanBizId());
                if (ksHtTimePlan.isPresent()) {
                    KsHtTimePlan timePlan = ksHtTimePlan.get();
                    List<MealGroup> mealGroups = mealGroupDao.findByKsHtTimePlan(timePlan);
                    mealGroups.forEach(value -> {
                        value.setIsDelete(1);
                        mealGroupDao.save(value);
                    });
                    timePlan.setIsDelete(1);
                    kshtTimePlanDao.save(timePlan);
                }
            });
        }
    }

    /**
     * 报餐失败后发送消息通知给处理人进行紧急处理
     *
     * @param title
     * @param errorMsg
     */
    private void sendMessage(String title, String errorMsg) {
        if (CollectionUtils.isEmpty(alertHrIds)) {
            log.warn("未配置报餐异常通知人");
            return;
        }
        try {
            List<SysUser> users = alertHrIds.stream().map(hrId -> sysUserServiceUC.getUserByEmpNo(hrId))
                    .filter(s -> Objects.nonNull(s) && StringUtils.isNotBlank(s.getWxOpenId())).collect(Collectors.toList());
            if (CollectionUtils.isEmpty(users)) {
                log.warn("根据工号无法找到对应人员的wxOpenId");
                return;
            }
            users.forEach(user -> {
                MessagePoolRecord messagePoolRecord = new MessagePoolRecord();
                messagePoolRecord.setMsgFromSys(8);
                messagePoolRecord.setMsgType(3);
                messagePoolRecord.setPriority(0);
                messagePoolRecord.setSendType(0);
                messagePoolRecord.setReceiveType(501);
                messagePoolRecord.setMsgTitle("旷世报餐失败通知");
                MessageBody messageBody = new MessageBody();
                messageBody.setTitle(title);
                messageBody.setType("旷世报餐失败通知");
                messageBody.setDepart("已推送");
                messageBody.setTime(DateUtils.formatLocalDateTime(LocalDateTime.now(), DateUtils.PATTERN_DATETIME));
                messageBody.setRemark("失败原因：" + errorMsg);
                messagePoolRecord.setMsgContent(JSON.toJSONString(messageBody));
                messagePoolRecord.setMsgDetailsUrl("");
                messagePoolRecord.setMsgTemplateId(vxMsgTemplateId);
                messagePoolRecord.setTipType(1);
                messagePoolRecord.setReceivePerson(user.getWxOpenId());
                messagePoolRecord.setReceivePersonId(user.getWxOpenId());
                messageSendService.send(messagePoolRecord);
            });
        } catch (Exception e) {
            log.error("发送消息中心失败", e);
        }
    }
}
