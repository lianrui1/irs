create table ibms_centralized_code_device_relation
(
    id                          varchar(36) not null comment '主键'
        primary key,
    created_by                  varchar(36) null comment '创建人',
    created_time                datetime    null comment '创建时间',
    is_del                      tinyint(1)  not null comment '是否已删除,0:否;1:是',
    updated_by                  varchar(36) null comment '更新人',
    updated_time                datetime    null comment '更新时间',
    version                     int         null comment '版本号',
    centralized_control_code_id varchar(36) not null comment '集控码id',
    device_id                   varchar(36) not null comment '设备id'
)
    comment '集控码关联设备，仅用于控制范围为部分的集控码';