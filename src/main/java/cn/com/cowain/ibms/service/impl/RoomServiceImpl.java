package cn.com.cowain.ibms.service.impl;

import cn.com.cowain.ibms.dao.*;
import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.dao.iot.DeviceToSpaceDao;
import cn.com.cowain.ibms.dao.iot.SpaceDao;
import cn.com.cowain.ibms.dao.meeting.IntelligentMeetingAppDao;
import cn.com.cowain.ibms.dao.space.ProjectDao;
import cn.com.cowain.ibms.entity.*;
import cn.com.cowain.ibms.entity.iot.DeviceToSpace;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.meeting.IntelligentMeetingApp;
import cn.com.cowain.ibms.entity.space.Project;
import cn.com.cowain.ibms.enumeration.ReservationRecordStatus;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.exceptions.DataNotFoundException;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.bean.TimeSlotBean;
import cn.com.cowain.ibms.rest.req.OpeningHoursReq;
import cn.com.cowain.ibms.rest.req.RoomReq;
import cn.com.cowain.ibms.rest.resp.RoomAllResp;
import cn.com.cowain.ibms.rest.resp.RoomSearchResp;
import cn.com.cowain.ibms.rest.resp.room.RoomResp;
import cn.com.cowain.ibms.service.ReservationRecordService;
import cn.com.cowain.ibms.service.RoomService;
import cn.com.cowain.ibms.service.WeChatService;
import cn.com.cowain.ibms.service.bean.ServiceMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.space.SpaceService;
import cn.com.cowain.ibms.utils.DaoTools;
import cn.com.cowain.ibms.utils.DateUtil;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.ibms.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Description
 *
 * @author 张宇鑫
 * @version 1.0
 * @date 2020/8/6 13:54
 */
@Slf4j
@Service
public class RoomServiceImpl implements RoomService {

    private RoomDao roomDao;

    private TimeSlotDao timeSlotDao;

    private OpeningHoursDao openingHoursDao;

    private RoomDeviceDao roomDeviceDao;


    public RoomServiceImpl(RoomDao roomDao,
                           TimeSlotDao timeSlotDao,
                           OpeningHoursDao openingHoursDao,
                           RoomDeviceDao roomDeviceDao) {
        this.roomDao = roomDao;
        this.timeSlotDao = timeSlotDao;
        this.openingHoursDao = openingHoursDao;
        this.roomDeviceDao = roomDeviceDao;

    }

    @Autowired
    private SpaceDao spaceDao;

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private ReservationRecordDao reservationRecordDao;

    @Resource
    private SpaceService spaceService;

    @Resource
    private DeviceToSpaceDao deviceToSpaceDao;

    @Autowired
    private ReservationRecordService reservationRecordService;

    @Resource
    private WeChatService weChatService;

    @Resource
    private ReservationRecordItemDao reservationRecordItemDao;

    @Autowired
    private IntelligentMeetingAppDao meetingAppDao;

    @Autowired
    private DeviceDao deviceDao;

    @Autowired
    private RedisUtil redisUtil;


    /**
     * 保存会议室
     *
     * @param roomReq
     * @return
     * @author 张宇鑫
     */
    @Override
    @Transactional
    public ServiceResult save(RoomReq roomReq) {
        // 1. 判断 参数 是否符合要求 （目前：判断openingHours 时间间隔是否大于30min）
        if (roomReq.getIsOpen() == 1) {
            if (!roomReq.getOpeningHours().isEmpty()) {
                boolean valid = this.valid(roomReq);
                if (!valid) {
                    return ServiceResult.error("开始时间小于结束时间");
                }
            } else {
                return ServiceResult.error("开始时间不能为空");
            }
        }
        // 2. 查询是否有相同名称的会议室
        Optional<Room> roomOptional = roomDao.findByName(roomReq.getName());
        if (roomOptional.isPresent()) {
            return ServiceResult.error("该名称已被使用");
        }

        // 检查空间是否存在
        Optional<Space> spaceOptional = spaceDao.findById(roomReq.getSpaceId());
        if (!spaceOptional.isPresent()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_SPACE, roomReq.getSpaceId()));
        }

        Space space = spaceOptional.get();
        // 检测space下是否已存在别的会议室
        Optional<Room> roomSpaceOptional = roomDao.findBySpaceId(space.getId());
        if(roomSpaceOptional.isPresent()){
            return ServiceResult.error(ServiceMessage.getMessageWithColon("所选空间下已存在会议室", roomSpaceOptional.get().getName()));
        }

        // 3. 创建一个 room
        Room room = new Room();
        //深拷贝
        BeanUtils.copyProperties(roomReq, room);
        room.setSpace(space);

        // 设置物品清单
        if (roomReq.getThingList() != null && !roomReq.getThingList().isEmpty()) {
            StringBuffer things = new StringBuffer();
            roomReq.getThingList().forEach(obj -> things.append(obj).append(","));
            room.setThings(things.deleteCharAt(things.length() - 1).toString());
        }

        //获取会议室排序最大值
        Integer maxSeq = roomDao.findMaxSeq();
        if (maxSeq != null) {
            maxSeq = maxSeq + 1;
        } else {
            maxSeq = 0;
        }
        room.setSeq(maxSeq);

        DaoTools.fillCreator(room);
        //保存
        roomDao.save(room);

        //创建一个roomDevice 空list
        List<RoomDevice> roomDeviceList = new ArrayList<>();
        //获取设备List
        List<String> roomDeviceStringList = roomReq.getDevices();
        //保存mysql
        if (roomDeviceStringList != null) {
            roomDeviceStringList.forEach(it -> {
                RoomDevice roomDevice = new RoomDevice();
                roomDevice.setName(it);
                roomDevice.setRoom(room);
                roomDeviceList.add(roomDevice);
            });
        }

        log.info(String.valueOf(roomDeviceList));
        roomDeviceDao.saveAll(roomDeviceList);
        if (roomReq.getIsOpen() == 1 && !roomReq.getOpeningHours().isEmpty()) {

            List<TimeSlot> list = new ArrayList<>();
            //创建 OpeningHours 空list
            List<OpeningHours> openingHoursList = new ArrayList<>();
            //开放时间
            List<OpeningHoursReq> openingHours = roomReq.getOpeningHours();
            Integer seq = 0;
            //循环
            for (OpeningHoursReq hoursReq : openingHours) {
                //创建一个OpeningHours
                OpeningHours openingHours1 = new OpeningHours();
                //获取开始时间 "08:00"
                String timeFrom = hoursReq.getFrom();
                //获取结束时间 "11:00"
                String timeTo = hoursReq.getTo();

                List<TimeSlotBean> timeSlotBeanList = DateUtil.splitDate(timeFrom, timeTo);
                for (TimeSlotBean timeSlotBean : timeSlotBeanList) {
                    //创建一个timeSlot类
                    TimeSlot timeSlot = new TimeSlot();
                    //设置开始时间
                    timeSlot.setFrom(timeSlotBean.getFrom());
                    //设置结束时间
                    timeSlot.setTo(timeSlotBean.getTo());
                    //设置房间信息
                    timeSlot.setRoom(room);
                    //设置排序
                    timeSlot.setSeq(seq);
                    //
                    list.add(timeSlot);
                    //排序自增
                    seq++;
                }
                //赋值TimeFrom
                openingHours1.setFrom(timeFrom);
                //赋值TimeTO
                openingHours1.setTo(timeTo);
                //赋值room id
                openingHours1.setRoom(room);
                // 添加到列表中 会议室开放时间表
                openingHoursList.add(openingHours1);
            }
            log.info(String.valueOf(list));
            timeSlotDao.saveAll(list);
            openingHoursDao.saveAll(openingHoursList);

        }

        // 保存会议室至缓存
        String key = redisUtil.createRoomKey(IConst.ROOM_MODULE, room.getId(), RedisUtil.Separator.COLON);
        redisUtil.set(key, room, RedisUtil.TWO_DAY_SECOND);
        return ServiceResult.ok();
    }

    /**
     * 更新会议室
     *
     * @param roomReq
     * @return
     * @author 张宇鑫
     */
    @Override
    @Transactional
    public ServiceResult update(String id, RoomReq roomReq) {
        // 1.先判断是否有记录
        Optional<Room> roomOptional = roomDao.findByName(roomReq.getName());
        //如果存在记录
        if (roomOptional.isPresent()) {
            Room roomTemp = roomOptional.get();
            //并且记录id不是当前id
            if (!id.equals(roomTemp.getId())) {
                return ServiceResult.error("该名称已被使用");
            }
        }
        // 2.更新 room 属性
        Room room = roomDao.findById(id).orElseThrow(() -> new DataNotFoundException("room id:" + id));
        //更新会议室名称
        room.setName(roomReq.getName());
        //更新会议室容纳人数
        room.setCapacity(roomReq.getCapacity());
//        //更新会议室楼栋
//        room.setBuilding(roomReq.getBuilding());
//        //更新会议室楼层
//        room.setFloor(roomReq.getFloor());
        //更新会议室是否开放
        room.setIsOpen(roomReq.getIsOpen());
        //设置更新时间
        room.setUpdatedTime(LocalDateTime.now());


        Optional<Space> spaceOptional = spaceDao.findById(roomReq.getSpaceId());
        if (!spaceOptional.isPresent()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_SPACE, roomReq.getSpaceId()));
        }

        Space space = spaceOptional.get();
        // 检测space下是否已存在别的会议室
        Optional<Room> roomSpaceOptional = roomDao.findBySpaceId(space.getId());
        if(roomSpaceOptional.isPresent()){
            Room updatedSpaceRoom = roomSpaceOptional.get();
            if(!updatedSpaceRoom.getId().equals(id)){
                return ServiceResult.error(ServiceMessage.getMessageWithColon("所选空间下已存在会议室", roomSpaceOptional.get().getName()));
            }
        }

        String thingStr = null;
        if (roomReq.getThingList() != null && !roomReq.getThingList().isEmpty()) {
            StringBuffer things = new StringBuffer();
            roomReq.getThingList().forEach(obj -> things.append(obj).append(","));
            thingStr = things.deleteCharAt(things.length() - 1).toString();
        }

        room.setThings(thingStr);
        room.setAttentions(roomReq.getAttentions());
        room.setImg(roomReq.getImg());
        room.setSpace(space);

        DaoTools.fillUpdater(room);
        //保存
        roomDao.save(room);
        // 4.更新 timeSlot
        //删除会议室timeSlotDao
        timeSlotDao.deleteByRoomId(id);
        //删除会议室opening hours
        openingHoursDao.deleteByRoomId(id);
        //删除会议室设备 roomDevice
        roomDeviceDao.deleteByRoomId(id);
        //获取设备List
        List<String> roomDeviceStringList = roomReq.getDevices();
        if (roomDeviceStringList != null) {
            //保存mysql
            roomDeviceStringList.forEach(it -> {
                RoomDevice roomDevice = new RoomDevice();
                roomDevice.setName(it);
                roomDevice.setRoom(room);
                roomDeviceDao.save(roomDevice);
            });
        }

        // 5. 更新开放时间, timeSlot
        List<OpeningHoursReq> openingHourWanted = roomReq.getOpeningHours();
        //循环
        Integer seq = 0;
        for (OpeningHoursReq hoursReq : openingHourWanted) {
            OpeningHours openingHours = new OpeningHours();
            //获取开始时间 "08:00"
            String timeFrom = hoursReq.getFrom();
            //获取结束时间 "11:00"
            String timeTo = hoursReq.getTo();
            // 按半小時间隔进行拆分
            List<TimeSlotBean> timeSlotBeanList = DateUtil.splitDate(timeFrom, timeTo);
            for (TimeSlotBean timeSlotBean : timeSlotBeanList) {
                TimeSlot timeSlot = new TimeSlot();
                timeSlot.setFrom(timeSlotBean.getFrom());
                timeSlot.setTo(timeSlotBean.getTo());
                timeSlot.setRoom(room);
                timeSlot.setSeq(seq);
                timeSlotDao.save(timeSlot);
                seq++;
            }
            //赋值TimeFrom
            openingHours.setFrom(timeFrom);
            //赋值TimeTO
            openingHours.setTo(timeTo);
            //赋值room id
            openingHours.setRoom(room);
            // 添加到列表中 会议室开放时间表
            openingHoursDao.save(openingHours);
        }

        // 保存会议室至缓存
        String key = redisUtil.createRoomKey(IConst.ROOM_MODULE, room.getId(), RedisUtil.Separator.COLON);
        redisUtil.set(key, room, RedisUtil.TWO_DAY_SECOND);
        return ServiceResult.ok();
    }

    /**
     * 更新会议室 timeslot device
     *
     * @param id      会议室id
     * @param integer 0存在 1 删除
     * @return
     * @author 张宇鑫
     */
    @Override
    @Transactional
    public Boolean updateRoomTimeSlotAndDeviceStatus(String id, Integer integer) {
        // 创建一个 timeSlot
        Set<TimeSlot> timeSlotSet = timeSlotDao.findByRoomId(id);
        //创建一个 空 list
        List<TimeSlot> timeSlotSetList = new ArrayList<>();
        // 循环
        timeSlotSet.forEach(it -> {
            //设置 is_del = integer
            it.setIsDelete(integer);
            // 添加
            timeSlotSetList.add(it);
        });
        //保存mysql
        timeSlotDao.saveAll(timeSlotSetList);
        // 创建一个 OpeningHours
        Set<OpeningHours> openingHoursSet = openingHoursDao.findByRoomId(id);
        //创建一个 空 list
        List<OpeningHours> openingHoursList = new ArrayList<>();
        // 循环
        openingHoursSet.forEach(it -> {
            //设置 is_del = integer
            it.setIsDelete(integer);
            // 添加
            openingHoursList.add(it);
        });
        //保存mysql
        openingHoursDao.saveAll(openingHoursList);

        Set<RoomDevice> roomDeviceSet = roomDeviceDao.findByRoomId(id);
        //创建一个roomDevice 空list
        List<RoomDevice> roomDeviceList = new ArrayList<>();

        //保存mysql
        roomDeviceSet.forEach(it -> {
            it.setIsDelete(integer);
            roomDeviceList.add(it);
        });
        log.info(String.valueOf(roomDeviceList));
        roomDeviceDao.saveAll(roomDeviceList);

        return true;
    }

    /**
     * 查询会议室
     *
     * @param roomName 模糊查找
     * @param page     当前页
     * @param size     每页数据量
     * @return
     * @author 张宇鑫
     */
    @Override
    @Transactional
    public PageBean<RoomSearchResp> search(String roomName, int page, int size) {
        Sort sort = Sort.by(Sort.Direction.ASC, "seq");
        Pageable pageable = PageRequest.of(page, size, sort);
        Page<Room> roomPage = roomDao.findByNameContains(roomName, pageable);
        PageBean<RoomSearchResp> pageBean = new PageBean<>();
        //当前页
        pageBean.setPage(pageable.getPageNumber());
        //每页记录数
        pageBean.setSize(pageable.getPageSize());
        //总记录数
        pageBean.setTotalElements(roomPage.getTotalElements());
        //总页数
        pageBean.setTotalPages(roomPage.getTotalPages());

        List<RoomSearchResp> list = new ArrayList<>();
        LocalDate now = LocalDate.now();
        //循环
        roomPage.getContent().forEach(room -> {
            RoomSearchResp roomSearchResp = new RoomSearchResp();
            BeanUtils.copyProperties(room, roomSearchResp);
            roomSearchResp.setCreateTime(room.getCreatedTime());
            // 设置项目ID
            roomSearchResp.setProjectId(room.getSpace().getProject().getId());
            //设置 更新时间
            roomSearchResp.setUpdateTime(room.getUpdatedTime());
            //填入开放时间信息
            Set<OpeningHours> openingHoursList = openingHoursDao.findByRoomId(roomSearchResp.getId());
            List<OpeningHoursReq> openingHoursReqList = new ArrayList<>();
            OpeningHoursReq openingHoursReq = new OpeningHoursReq();
            openingHoursList.forEach(it -> {
                openingHoursReq.setFrom(it.getFrom());
                openingHoursReq.setTo(it.getTo());
                openingHoursReqList.add(openingHoursReq);
            });
            roomSearchResp.setOpeningHours(openingHoursReqList);
            //填入设备信息
            Set<RoomDevice> roomDeviceSet = roomDeviceDao.findByRoomId(roomSearchResp.getId());
            List<String> stringListDevice = new ArrayList<>();
            roomDeviceSet.forEach(it -> stringListDevice.add(it.getName()));
            roomSearchResp.setDevices(stringListDevice);
            if (null != roomSearchResp.getUpdateTime()) {
                roomSearchResp.setCreateTime(roomSearchResp.getUpdateTime());
            }
            // 获取会议室区域
            Optional<Project> projectOption = projectDao.findById(room.getSpace().getProject().getId());
            if (projectOption.isPresent()) {
                roomSearchResp.setProjectArea(projectOption.get().getProjectArea().getName());
            }

            // 判断会议室状态
            Set<ReservationRecord> reservationRecord = reservationRecordDao.findByDateGreaterThanEqualAndRoomId(now, room.getId());
            Integer reservationRecordSize = reservationRecord.size();
            if (reservationRecordSize != 0) {
                roomSearchResp.setRemove(0);
            } else {
                roomSearchResp.setRemove(1);
            }

            roomSearchResp.setCreateUser(room.getCreatedBy());
            roomSearchResp.setUpdateUser(room.getUpdatedBy());

            if (StringUtils.isNotEmpty(room.getThings())) {
                roomSearchResp.setThingList(Arrays.asList(room.getThings().split(",")));
            } else {
                roomSearchResp.setThingList(new ArrayList<>());
            }

            roomSearchResp.setSpaceName(SpaceService.getFullSpaceName(room.getSpace(), room.getSpace().getName()));

            list.add(roomSearchResp);
        });

        pageBean.setList(list);
        return pageBean;
    }

    @Override
    @Transactional
    public boolean valid(RoomReq roomReq) {
        boolean isValid = true;
        //获取 openingHours
        List<OpeningHoursReq> openingHours = roomReq.getOpeningHours();
        //循环
        for (OpeningHoursReq hoursReq : openingHours) {
            //获取开始时间 "08:00"
            String timeFrom = hoursReq.getFrom();
            //获取结束时间 "11:00"
            String timeTo = hoursReq.getTo();
            if ("24:00".equals(timeTo)) {
                timeTo = "23:59";
                if ("23:30".equals(timeFrom)) {
                    timeFrom = "23:29";
                }
            }
            LocalTime localTimeTo = LocalTime.parse(timeTo);
            //转换为时间类型
            LocalTime localTimeFrom = LocalTime.parse(timeFrom);
            //判断两个时间差是否大于30分钟
            if (((localTimeTo.getHour() - localTimeFrom.getHour()) * IConst.MINUTE_TO_SECOND + localTimeTo.getMinute() - localTimeFrom.getMinute()) < IConst.TIME_MINUTE_ADD) {
                isValid = false;
                break;
            }
        }
        return isValid;
    }

    /**
     * 查询可用会议室列表
     *
     * @return
     * @author Yang.Lee
     */
    @Override
    @Transactional
    public List<RoomSearchResp> searchAvailableList() {

        // 根据seq字段排序
        Sort sort = Sort.by(Sort.Direction.ASC, "seq");
        List<Room> roomList = roomDao.findAll(sort);

        List<RoomSearchResp> roomSearchRespList = new ArrayList<>();
        RoomSearchResp roomSearchResp = null;

        for (Room room : roomList) {
            roomSearchResp = new RoomSearchResp();
            BeanUtils.copyProperties(room, roomSearchResp);
            roomSearchRespList.add(roomSearchResp);
        }

        return roomSearchRespList;
    }

    @Override
    public List<RoomAllResp> searchAllRoomList() {
        // 根据seq字段排序
        Sort sort = Sort.by(Sort.Direction.ASC, "seq");
        List<Room> roomList = roomDao.findAll(sort);

        List<RoomAllResp> roomSearchRespList = new ArrayList<>();
        RoomAllResp roomResp = null;

        for (Room room : roomList) {
            roomResp = new RoomAllResp();
            BeanUtils.copyProperties(room, roomResp);
            roomSearchRespList.add(roomResp);
        }

        return roomSearchRespList;
    }

    /**
     * 删除会议室
     *
     * @param roomId : 会议室ID
     * @return 逻辑结果
     * @author Yang.Lee
     * @date 2021/3/6 14:30
     **/
    @Override
    @Transactional
    public ServiceResult delete(String roomId, String token) {

        Optional<Room> roomOptional = roomDao.findById(roomId);
        if(!roomOptional.isPresent()){
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_MEETING_ROOM, roomId));
        }

        Boolean updateTimeSlotIsDeleteStatus = updateRoomTimeSlotAndDeviceStatus(roomId, 1);
        if (!Boolean.TRUE.equals(updateTimeSlotIsDeleteStatus)) {
            return ServiceResult.error(updateTimeSlotIsDeleteStatus);
        }

        Room room = roomOptional.get();

        // 删除关联的空间数据
        ServiceResult result = spaceService.delete(room.getSpace().getId());
        if (!result.isSuccess()) {
            return result;
        }

        room.setIsDelete(1);
        roomDao.save(room);

        //删除会议室opening hours
        Set<OpeningHours> openingHours = openingHoursDao.findByRoomId(roomId);
        if (!openingHours.isEmpty()){
            openingHours.forEach(it -> it.setIsDelete(1));
            openingHoursDao.saveAll(openingHours);
        }

        // 删除设备和空间关系
        List<DeviceToSpace> d2sList = deviceToSpaceDao.findAllBySpaceId(room.getSpace().getId());
        if (d2sList != null && !d2sList.isEmpty()) {
            d2sList.forEach(d2s -> {
                d2s.setIsDelete(1);
                deviceToSpaceDao.save(d2s);
            });
        }

        // 查询已存在的会议记录
        Set<ReservationRecord> reservationRecord = reservationRecordDao.findByDateGreaterThanEqualAndRoomId(LocalDate.now(), roomId);
        // 过滤已取消的会议
        reservationRecord = reservationRecord.stream().filter(obj -> !obj.getStatus().equals(ReservationRecordStatus.CANCEL)).collect(Collectors.toSet());

        if (!reservationRecord.isEmpty()) {
            List<ReservationRecord> reservationRecordList = new ArrayList<>();
            reservationRecord.forEach(it -> {

                String topic = it.getTopic();

                it.setStatus(ReservationRecordStatus.CANCEL);
                reservationRecordList.add(it);
                log.debug("此处推送取消会议提醒给用户");
                Set<ReservationRecordItem> reservationRecordItemSet = reservationRecordItemDao.findByReservationRecordId(it.getId());

                reservationRecordItemSet.forEach(reservationRecordItem -> {

                    log.debug("此处推送 取消会议消息给用户: " + reservationRecordItem.getOwnerEmpNo());
                    weChatService.channelRequirementsReminders(reservationRecordItem, topic);
                });

                reservationRecordService.cancel(it.getId() ,token);
            });
            reservationRecordDao.saveAll(reservationRecordList);
        }

        // 删除缓存中的会议室
        redisUtil.del(redisUtil.createRoomKey(IConst.ROOM_MODULE, room.getId(), RedisUtil.Separator.COLON));
        return ServiceResult.ok();
    }

    /**
     * 获取会议室详情
     *
     * @param roomId 会议室ID
     * @return 会议室详情
     * @author Yang.Lee
     * @date 2021/4/22 19:33
     **/
    @Override
    public RoomSearchResp get(String roomId) {

        Optional<Room> roomOptional = roomDao.findById(roomId);

        if (!roomOptional.isPresent()) {
            return new RoomSearchResp();
        }

        Room room = roomOptional.get();

        RoomSearchResp roomSearchResp = new RoomSearchResp();
        BeanUtils.copyProperties(room, roomSearchResp);
        roomSearchResp.setCreateTime(room.getCreatedTime());
        // 设置项目ID
        roomSearchResp.setProjectId(room.getSpace().getProject().getId());
        //设置 更新时间
        roomSearchResp.setUpdateTime(room.getUpdatedTime());
        //填入开放时间信息
        Set<OpeningHours> openingHoursList = openingHoursDao.findByRoomId(roomSearchResp.getId());
        List<OpeningHoursReq> openingHoursReqList = new ArrayList<>();
        OpeningHoursReq openingHoursReq = new OpeningHoursReq();
        openingHoursList.forEach(it -> {
            openingHoursReq.setFrom(it.getFrom());
            openingHoursReq.setTo(it.getTo());
            openingHoursReqList.add(openingHoursReq);
        });
        roomSearchResp.setOpeningHours(openingHoursReqList);
        //填入设备信息
        Set<RoomDevice> roomDeviceSet = roomDeviceDao.findByRoomId(roomSearchResp.getId());
        List<String> stringListDevice = new ArrayList<>();
        roomDeviceSet.forEach(it -> stringListDevice.add(it.getName()));
        roomSearchResp.setDevices(stringListDevice);
        if (null != roomSearchResp.getUpdateTime()) {
            roomSearchResp.setCreateTime(roomSearchResp.getUpdateTime());
        }
        // 获取会议室区域
        Optional<Project> projectOption = projectDao.findById(room.getSpace().getProject().getId());
        if (projectOption.isPresent()) {
            roomSearchResp.setProjectArea(projectOption.get().getProjectArea().getName());
        }

        // 判断会议室状态
        Set<ReservationRecord> reservationRecord = reservationRecordDao.findByDateGreaterThanEqualAndRoomId(LocalDate.now(), room.getId());
        Integer reservationRecordSize = reservationRecord.size();
        if (reservationRecordSize != 0) {
            roomSearchResp.setRemove(0);
        } else {
            roomSearchResp.setRemove(1);
        }

        if (StringUtils.isNotEmpty(room.getThings())) {
            roomSearchResp.setThingList(Arrays.asList(room.getThings().split(",")));
        } else {
            roomSearchResp.setThingList(new ArrayList<>());
        }

//        roomSearchResp.setAttentions(Optional.ofNullable(room.getAttentions()).orElse(""));
        roomSearchResp.setImg(Optional.ofNullable(room.getImg()).orElse(""));
        roomSearchResp.setSpaceId(Optional.ofNullable(room.getSpace()).map(Space::getId).orElse(""));
        roomSearchResp.setSpaceName(Optional.ofNullable(room.getSpace()).map(Space::getName).orElse(""));

        // 空间详细地址
        roomSearchResp.setRoomAddress(SpaceService.getFullSpaceName(room.getSpace(), room.getSpace().getName()));
        List<IntelligentMeetingApp> meetingAppList = meetingAppDao.findByRoomId(room.getId());
        if(!meetingAppList.isEmpty()){
            roomSearchResp.setIsMeetingApp(1);
        }else {
            roomSearchResp.setIsMeetingApp(2);
        }
        List<IotDevice> deviceList = deviceDao.findBySpaceId(room.getSpace().getId());
        roomSearchResp.setIsDoorMagnetic(2);
        for(IotDevice device : deviceList){
            if(DeviceType.DOOR_MAGNETIC.equals(device.getDeviceType())){
                roomSearchResp.setIsDoorMagnetic(1);
                break;
            }
        }

        return roomSearchResp;
    }

    /**
     * 根据区域id查询会议室列表
     *
     * @param projectId 区域id
     * @return 会议室列表
     * @author Yang.Lee
     * @date 2021/7/16 10:23
     **/
    @Override
    @Transactional(readOnly = true)
    public List<RoomSearchResp> getRoomByProjectId(String projectId) {

        List<Room> roomList = roomDao.findBySpaceProjectId(projectId);
        List<RoomSearchResp> roomSearchRespList = new ArrayList<>();
        RoomSearchResp roomSearchResp = null;

        for (Room room : roomList) {
            roomSearchResp = new RoomSearchResp();
            BeanUtils.copyProperties(room, roomSearchResp);
            roomSearchRespList.add(roomSearchResp);
        }
        return roomSearchRespList;
    }

    @Override
    public ServiceResult getMeeting(String roomId) {
        Optional<Room> byId = roomDao.findById(roomId);
        if(!byId.isPresent()){
            return ServiceResult.error("会议室id有误");
        }
        Room room = byId.get();
        return ServiceResult.ok(room.getSpace().getProject().getVisitor());
    }

    /**
     * @Param: 根据项目ID查询并且Space中isShow=1
     * @author：yanzy
     * @date 2021/12/28
     */
    @Override
    public List<RoomResp> findBySpaceProjectIdAndSpaceIsShow(String projectid) {
        List<Room> roomList = roomDao.findBySpaceProjectIdAndSpaceIsShowAndIsOpen(projectid,1,1);
        List<RoomResp> roomRespsList = new ArrayList<>();
        RoomResp roomResp = null;
        for (Room room : roomList){
            roomResp = new RoomResp();
            roomResp.setRoomId(room.getId());
            roomResp.setRoomName(room.getName());
            roomResp.setSpaceId(room.getSpace().getId());
            roomRespsList.add(roomResp);
        }
        return roomRespsList;
    }
}