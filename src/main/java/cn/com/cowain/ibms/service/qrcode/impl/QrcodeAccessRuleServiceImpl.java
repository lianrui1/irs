package cn.com.cowain.ibms.service.qrcode.impl;

import cn.com.cowain.ibms.dao.distinguish.DoorMagneticAccessRecordDao;
import cn.com.cowain.ibms.dao.distinguish.StaffGroupDao;
import cn.com.cowain.ibms.dao.distinguish.StaffGroupMembersDao;
import cn.com.cowain.ibms.dao.iot.AccessControlSpotDao;
import cn.com.cowain.ibms.dao.iot.AccessControlSpotUserDao;
import cn.com.cowain.ibms.dao.iot.DoorMagneticDao;
import cn.com.cowain.ibms.dao.qrcode.QrcodeAccessRuleDao;
import cn.com.cowain.ibms.dao.qrcode.QrcodeAccessRuleToStaffGroupDao;
import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.distinguish.DoorMagneticAccessRecord;
import cn.com.cowain.ibms.entity.distinguish.StaffGroup;
import cn.com.cowain.ibms.entity.distinguish.StaffGroupMembers;
import cn.com.cowain.ibms.entity.distinguish.TimePlan;
import cn.com.cowain.ibms.entity.iot.AccessControlSpot;
import cn.com.cowain.ibms.entity.iot.AccessControlSpotUser;
import cn.com.cowain.ibms.entity.iot.DoorMagnetic;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.qrcode.QrcodeAccessRules;
import cn.com.cowain.ibms.entity.qrcode.QrcodeAccessRulesToStaffGroup;
import cn.com.cowain.ibms.entity.space.Project;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.distinguish.HkFaceRecordPageReq;
import cn.com.cowain.ibms.rest.req.qrcode.QrcodeAccessRulePageReq;
import cn.com.cowain.ibms.rest.req.qrcode.QrcodeAccessRuleReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.qrcode.FaceRecordPageResp;
import cn.com.cowain.ibms.rest.resp.qrcode.QrcodeAccessRuleDetailResp;
import cn.com.cowain.ibms.rest.resp.qrcode.QrcodeAccessRulePageResp;
import cn.com.cowain.ibms.rest.resp.qrcode.UserPageResp;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.ServiceMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.disthinguish.StaffGroupService;
import cn.com.cowain.ibms.service.disthinguish.TimePlanService;
import cn.com.cowain.ibms.service.qrcode.QrcodeAccessRuleService;
import cn.com.cowain.ibms.service.qrcode.QrcodeAccessRulesToStaffGroupService;
import cn.com.cowain.ibms.service.space.SpaceService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nullable;
import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

/**
 * @author Yang.Lee
 * @date 2021/4/7 10:20
 */
@Slf4j
@Service
public class QrcodeAccessRuleServiceImpl implements QrcodeAccessRuleService {

    @Resource
    private QrcodeAccessRulesToStaffGroupService qrcodeAccessRulesToStaffGroupService;

    @Resource
    private StaffGroupService staffGroupService;

    @Resource
    private TimePlanService timePlanService;

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserService;

    @Resource
    private DoorMagneticDao doorMagneticDao;

    @Resource
    private QrcodeAccessRuleDao qrcodeAccessRuleDao;

    @Resource
    private QrcodeAccessRuleToStaffGroupDao qrcodeAccessRuleToStaffGroupDao;

    @Resource
    private DoorMagneticAccessRecordDao doorMagneticAccessRecordDao;

    @Resource
    private StaffGroupDao staffGroupDao;

    @Resource
    private StaffGroupMembersDao staffGroupMembersDao;

    @Resource
    private AccessControlSpotDao spotDao;

    @Resource
    private AccessControlSpotUserDao spotUserDao;


    /**
     * 创建二维码通行权限
     *
     * @param qrcodeAccessRuleReq 创建参数
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/4/6 16:25
     **/
    @Override
    @Transactional
    public ServiceResult createAccessRule(QrcodeAccessRuleReq qrcodeAccessRuleReq) {

        // 参数合法性校验
        ServiceResult result = ruleParamCheck(qrcodeAccessRuleReq);
        if (!result.isSuccess()) {
            return result;
        }

        // 校验该门磁是否已经有了权限信息，一个门磁只能对应一个权限
        Optional<QrcodeAccessRules> accessRulesOptional = qrcodeAccessRuleDao.findByDoorMagneticId(qrcodeAccessRuleReq.getMagneticDeviceId());
        if (accessRulesOptional.isPresent()) {
            // 如果设别已存在通行权限，则不可新建
            return ServiceResult.error(ServiceMessage.getMessageWithColon("该门磁设备已存在通行权限", qrcodeAccessRuleReq.getMagneticDeviceId()));
        }

        // 保存权限数据
        QrcodeAccessRules qrCodeAccessRules = (QrcodeAccessRules) result.getObject();
        qrcodeAccessRuleDao.save(qrCodeAccessRules);

        // 保存权限与人员组关系
        List<StaffGroup> groups = staffGroupDao.findAllById(qrcodeAccessRuleReq.getStaffGroups());
        qrcodeAccessRulesToStaffGroupService.saveGroupIdsByQrcodeRule(qrCodeAccessRules, groups);

        return ServiceResult.ok(new IdResp(qrCodeAccessRules.getId()));
    }

    /**
     * 创建二维码通行权限
     *
     * @param id                  权限ID
     * @param qrcodeAccessRuleReq 创建参数
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/4/6 16:25
     **/
    @Override
    @Transactional
    public ServiceResult updateAccessRule(String id, QrcodeAccessRuleReq qrcodeAccessRuleReq) {

        // 参数校验
        ServiceResult result = ruleParamCheck(qrcodeAccessRuleReq);
        if (!result.isSuccess()) {
            return result;
        }

        // 查询数据库中判断是否存在记录
        Optional<QrcodeAccessRules> qrcodeAccessRulesOptional = qrcodeAccessRuleDao.findById(id);
        if (!qrcodeAccessRulesOptional.isPresent()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUNT_QRCODE_ACCESS_RULES, id));
        }

        // 数据库里的原数据
        QrcodeAccessRules sourceRules = qrcodeAccessRulesOptional.get();
        // 将需要更新的数据赋值给sourceRules对象
        sourceRules.setTimePlan(sourceRules.getTimePlan());

        // 保存通行记录
        qrcodeAccessRuleDao.save(sourceRules);

        // 编辑rules - staff group 关系信息
        // 操作步骤：1.对比原数据库中已有的关系和更新请求中的数据，找到需要删除的数据以及需要添加的数据
        // 2. 删除已经不存在的关系，添加新的关系
        List<QrcodeAccessRulesToStaffGroup> qrcodeRule2GroupList = qrcodeAccessRulesToStaffGroupService.getListByRules(id);// 数据库中已存在的关系列表
        Set<QrcodeAccessRulesToStaffGroup> deletedQrcodeSet = new HashSet<>();   // 需要删除的数据集合
        Set<QrcodeAccessRulesToStaffGroup> noUpdateQrcodeSet = new HashSet<>();   // 依然存在的数据集合
        qrcodeRule2GroupList.forEach(rule2Group -> {
            if (!qrcodeAccessRuleReq.getStaffGroups().contains(rule2Group.getGroup().getId())) {
                // 设置为删除
                rule2Group.setIsDelete(1);
                deletedQrcodeSet.add(rule2Group);
            } else {
                noUpdateQrcodeSet.add(rule2Group);
            }
        });

        // 删除无效关系
        if (!deletedQrcodeSet.isEmpty()) {
            qrcodeAccessRuleToStaffGroupDao.saveAll(deletedQrcodeSet);
        }

        // 查询出需要添加的所有人员组集合，再减去不需要变化的数据，剩下的数据都新增到关系表中
        List<StaffGroup> targetGroups = staffGroupDao.findAllById(qrcodeAccessRuleReq.getStaffGroups());

        if (!targetGroups.isEmpty()) {
            // 需要修改的对象集合
            List<StaffGroup> noChange = new ArrayList<>();
            targetGroups.forEach(obj -> {
                for (QrcodeAccessRulesToStaffGroup access2Group : noUpdateQrcodeSet) {
                    if (obj.getId().equals(access2Group.getGroup().getId())) {
                        noChange.add(obj);
                    }
                }
            });
            targetGroups.removeAll(noChange);
            if (!targetGroups.isEmpty()) {
                qrcodeAccessRulesToStaffGroupService.saveGroupIdsByRule(sourceRules, targetGroups);
            }
        }

        return ServiceResult.ok();
    }

    /**
     * 删除通行权限
     *
     * @param id 权限ID
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/4/6 16:25
     **/
    @Override
    @Transactional
    public ServiceResult deleteAccessRule(String id) {

        Optional<QrcodeAccessRules> qrcodeAccessRulesOptional = qrcodeAccessRuleDao.findById(id);
        if (!qrcodeAccessRulesOptional.isPresent()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUNT_QRCODE_ACCESS_RULES, id));
        }

        QrcodeAccessRules qrcodeAccessRules = qrcodeAccessRulesOptional.get();
        // 删除权限
        qrcodeAccessRules.setIsDelete(1);
        qrcodeAccessRuleDao.save(qrcodeAccessRules);

        // 删除权限和人员组关系
        List<QrcodeAccessRulesToStaffGroup> qrcodeAccessRulesToStaffGroupList = qrcodeAccessRulesToStaffGroupService.getListByRules(id);
        qrcodeAccessRulesToStaffGroupService.deleteQrcodeAccessRulesToStaffGroup(qrcodeAccessRulesToStaffGroupList.toArray(new QrcodeAccessRulesToStaffGroup[0]));

        return ServiceResult.ok();
    }

    /**
     * 获取通行权限分页数据
     *
     * @param req 查询参数
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/4/6 16:30
     **/
    @Override
    @Transactional
    public PageBean<QrcodeAccessRulePageResp> getAccessRulePage(QrcodeAccessRulePageReq req) {
        // 查询分页数据
        Page<QrcodeAccessRules> qrcodeAccessRulesPage = qrcodeAccessRuleDao.findAll((root, criteriaQuery, criteriaBuilder) -> {
            //集合 用于封装查询条件
            List<Predicate> list = new ArrayList<>();

            //根据各个查询条加构造查询对象
            // 项目ID
            if (StringUtils.isNotEmpty(req.getProjectId())) {
                list.add(criteriaBuilder.equal(root.get("doorMagnetic").get("device").get("project").get("id"), req.getProjectId()));
            }
            // 设备名称
            if (StringUtils.isNotEmpty(req.getMagneticDeviceName())) {
                list.add(criteriaBuilder.like(root.get("doorMagnetic").get("name"), "%" + req.getMagneticDeviceName() + "%"));
            }
            // 设备ID
            if (StringUtils.isNotEmpty(req.getMagneticDeviceId())) {

                list.add(criteriaBuilder.equal(root.get("doorMagnetic").get("id"), req.getMagneticDeviceId()));
            }

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, req.createPageable());

        // 将Page对象转为可供输出的PageBean对象返回
        return convert(qrcodeAccessRulesPage);
    }

    /**
     * 查询二维码通行权限
     *
     * @param id 权限ID
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/4/6 16:29
     **/
    @Override
    @Transactional
    public QrcodeAccessRuleDetailResp getAccessRuleDetailByAccessId(String id) {

        Optional<QrcodeAccessRules> qrcodeAccessRulesOptional = qrcodeAccessRuleDao.findById(id);
        return qrcodeAccessRulesOptional.map(this::convert).orElse(null);
    }

    /*
     * 对象转换
     *
     * @param qrcodeAccessRules 待转对象
     * @return 转换结果
     * @author Yang.Lee
     * @date 2021/4/8 9:43
     **/
    @Override
    @Transactional
    public QrcodeAccessRuleDetailResp convert(QrcodeAccessRules qrcodeAccessRules) {

        if (qrcodeAccessRules == null) {
            return QrcodeAccessRuleDetailResp.builder().build();
        }

        // 查询对应的组-门磁关系
        List<StaffGroup> staffGroupList = new ArrayList<>();
        qrcodeAccessRulesToStaffGroupService.getListByRules(qrcodeAccessRules.getId())
                .forEach(obj -> staffGroupList.add(obj.getGroup()));

        return QrcodeAccessRuleDetailResp.builder()
                .id(qrcodeAccessRules.getId())
                .magneticDeviceId(qrcodeAccessRules.getDoorMagnetic().getId())
                .magneticDeviceName(Optional.ofNullable(qrcodeAccessRules.getDoorMagnetic().getName()).orElse(""))
                .projectId(Optional.ofNullable(qrcodeAccessRules.getDoorMagnetic().getDevice().getProject()).map(Project::getId).orElse(""))
                .projectName(Optional.ofNullable(qrcodeAccessRules.getDoorMagnetic().getDevice().getProject()).map(Project::getProjectName).orElse(""))
                .spaceId(Optional.ofNullable(qrcodeAccessRules.getDoorMagnetic().getDevice().getSpace()).map(BaseEntity::getId).orElse(""))
                .spaceName(Optional.ofNullable(qrcodeAccessRules.getDoorMagnetic().getDevice().getSpace()).map(Space::getName).orElse(""))
                .timePlanId(qrcodeAccessRules.getTimePlan().getId())
                .timePlanName(qrcodeAccessRules.getTimePlan().getName())
                .staffGroupList(staffGroupService.convert(staffGroupList))
                .deviceAddress(Optional.ofNullable(qrcodeAccessRules.getDoorMagnetic().getDevice().getAddress()).orElse(""))
                .build();
    }

    /*
     * 对象转换
     *
     * @param qrcodeAccessRules 待转对象
     * @return 转换结果
     * @author Yang.Lee
     * @date 2021/4/8 9:43
     **/
    @Override
    @Transactional
    public QrcodeAccessRulePageResp convertPage(QrcodeAccessRules qrcodeAccessRules) {

        if (qrcodeAccessRules == null) {
            return QrcodeAccessRulePageResp.builder().build();
        }

        // 查询对应的组-门磁关系
        List<StaffGroup> staffGroupList = new ArrayList<>();
        qrcodeAccessRulesToStaffGroupService.getListByRules(qrcodeAccessRules.getId())
                .forEach(obj -> staffGroupList.add(obj.getGroup()));

        StringBuilder groupName = new StringBuilder();
        if (!staffGroupList.isEmpty()) {
            staffGroupList.forEach(obj -> groupName.append(obj.getName()).append(","));
            groupName.deleteCharAt(groupName.length() - 1);
        }

        Space space = qrcodeAccessRules.getDoorMagnetic().getDevice().getSpace();

        return QrcodeAccessRulePageResp.builder()
                .deviceAddress(Optional.ofNullable(qrcodeAccessRules.getDoorMagnetic().getDevice().getAddress()).orElse(""))
                .id(qrcodeAccessRules.getId())
                .magneticDeviceName(Optional.ofNullable(qrcodeAccessRules.getDoorMagnetic().getName()).orElse(""))
                .projectName(Optional.ofNullable(qrcodeAccessRules.getDoorMagnetic().getDevice().getProject()).map(Project::getProjectName).orElse(""))
                .spaceName(SpaceService.getFullSpaceName(space, Optional.ofNullable(space).map(Space::getName).orElse("")))
                .staffGroupNames(groupName.toString())
                .deviceId(Optional.ofNullable(qrcodeAccessRules.getDoorMagnetic().getId()).orElse(""))
                .build();
    }

    /*
     * 对象转换
     *
     * @param qrcodeAccessRules 待转对象
     * @return 转换结果
     * @author Yang.Lee
     * @date 2021/4/8 9:43
     **/
    @Override
    @Transactional
    public List<QrcodeAccessRulePageResp> convert(Collection<QrcodeAccessRules> qrcodeAccessRulesCollection) {

        List<QrcodeAccessRulePageResp> result = new ArrayList<>();

        if (qrcodeAccessRulesCollection == null) {
            return result;
        }
        qrcodeAccessRulesCollection.forEach(obj -> result.add(convertPage(obj)));

        return result;
    }

    /**
     * 对象转换
     *
     * @param source 待转对象
     * @return 转换结果
     * @author Yang.Lee
     * @date 2021/4/8 16:55
     **/
    @Override
    @Transactional
    public PageBean<QrcodeAccessRulePageResp> convert(Page<QrcodeAccessRules> source) {

        PageBean<QrcodeAccessRulePageResp> resultPage = new PageBean<>(source.getNumber(), source.getSize());
        resultPage.setTotalElements(source.getTotalElements());
        resultPage.setTotalPages(source.getTotalPages());
        resultPage.setList(convert(source.getContent()));

        return resultPage;
    }

    /**
     * 判断用户是否有门磁权限
     *
     * @param dmNo  门磁编号
     * @param empNo 用户工号
     * @return true: 有权限。 false: 无权限
     * @author Yang.Lee
     * @date 2021/4/9 9:13
     **/
    @Override
    @Transactional
    public ServiceResult checkUserAuth(String dmNo, String empNo) {

        // 验证门磁是否存在
        Optional<DoorMagnetic> doorMagneticOptional = doorMagneticDao.findByNumber(dmNo);
        if (!doorMagneticOptional.isPresent()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUNT_DOOR_MAGNETISM, dmNo));
        }

        DoorMagnetic doorMagnetic = doorMagneticOptional.get();

        // 判断门磁是否绑定门禁点
        Optional<AccessControlSpot> spotOp = spotDao.findByDeviceId(doorMagnetic.getDevice().getId());
        if(spotOp.isEmpty()){
            return ServiceResult.error("该门磁未绑定门禁点，无法开门");
        }

        AccessControlSpot spot = spotOp.get();
        // 判断人员是否拥有门禁点通行权限
        List<AccessControlSpotUser> spotUserOp = spotUserDao.findByAccessControlSpotIdAndUserHrId(spot.getId(), empNo);
        if(spotUserOp.isEmpty()){
            return ServiceResult.error(ServiceMessage.UNAUTHORIZED);
        }

        LocalDateTime now = LocalDateTime.now();

        boolean auth = false;

        for(AccessControlSpotUser obj : spotUserOp){
            if(now.isAfter(obj.getAccessEndTime()) || now.isBefore(obj.getAccessStartTime())){
                continue ;
            }
            auth = true;
        }

        if(!auth){
            return ServiceResult.error("未到通行时间");
        }

        // 查找该门磁有权限的用户组
//        List<QrcodeAccessRulesToStaffGroup> qrcodeAccessRulesToStaffGroupList = qrcodeAccessRulesToStaffGroupService.getListByDoorMagneticId(doorMagnetic.getId());
//
//        // 如果门磁无任何权限，则返回成功，任何人都可以开门
//        if (qrcodeAccessRulesToStaffGroupList.isEmpty()) {
//            log.warn("门磁未关联用户组权限");
//            return ServiceResult.ok();
//        }
//
//        // 查询该门磁对应的所有人员组
//        List<StaffGroup> staffGroupMembersList = new ArrayList<>();
//        qrcodeAccessRulesToStaffGroupList.forEach(obj -> staffGroupMembersList.add(obj.getGroup()));
//
//        // 判断该用户是否在该门磁对应的用户组中
//        boolean success = staffGroupService.isStaffInGroups(empNo, staffGroupMembersList.toArray(new StaffGroup[0]));

        return ServiceResult.ok();
    }

    /**
     * 查询二维码通行权限
     *
     * @param deviceId 设备Id
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/4/6 16:29
     **/
    @Nullable
    @Transactional
    @Override
    public QrcodeAccessRuleDetailResp getByDeviceId(String deviceId) {

        Optional<DoorMagnetic> doorMagneticOptional = doorMagneticDao.findByDeviceId(deviceId);

        if (!doorMagneticOptional.isPresent()) {
            return null;
        }

        DoorMagnetic doorMagnetic = doorMagneticOptional.get();
        Optional<QrcodeAccessRules> qrcodeAccessRulesOptional = qrcodeAccessRuleDao.findByDoorMagneticId(doorMagnetic.getId());

        return qrcodeAccessRulesOptional.map(this::convert).orElse(null);
    }

    /**
     * 人员组删除后的联动操作
     *
     * @param staffGroupId 人员组ID
     * @return 处理结果
     */
    @Override
    @Transactional
    public ServiceResult staffGroupRemoveCheck(String staffGroupId) {

        // 查询该人员组所对应的关系列表
        List<QrcodeAccessRulesToStaffGroup> relationList = qrcodeAccessRulesToStaffGroupService.getListByStaffGroupId(staffGroupId);
        if (relationList.isEmpty()) {
            return ServiceResult.ok("人员组未关联二维码权限");
        }

        //找到该人员组锁对应的所有权限，再判断每一个权限是否只有唯一的人员组。如果是，则连权限一起删除
        relationList.forEach(relation -> {

            String ruleId = relation.getRule().getId();
            List<QrcodeAccessRulesToStaffGroup> ruleRelationList = qrcodeAccessRulesToStaffGroupService.getListByRules(ruleId);
            int size = ruleRelationList.size();
            if (size == 1) {
                deleteAccessRule(ruleId);
            } else if (size > 1) {
                // 有多个关系则直接删除本关系
                qrcodeAccessRulesToStaffGroupService.deleteQrcodeAccessRulesToStaffGroup(relation);
            }
        });

        return ServiceResult.ok();
    }

    /**
     * 获取设备人员列表
     *
     * @param name
     * @param ruleId
     * @param page
     * @param size
     * @return
     */
    @Override
    @Transactional
    public PageBean<UserPageResp> userList(String name, String ruleId, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        List<QrcodeAccessRulesToStaffGroup> byRuleId = qrcodeAccessRuleToStaffGroupDao.findByRuleId(ruleId);

        // 获取用户组ids
        List<StaffGroup> groupIdsList = byRuleId.stream().map(s -> s.getGroup()).collect(Collectors.toList());

        List<StaffGroupMembers> all = staffGroupMembersDao.findAll((root, criteriaQuery, criteriaBuilder) -> {
            //集合 用于封装查询条件
            List<Predicate> list = new ArrayList<>();
            if (StringUtils.isNoneBlank(name)) {
                list.add(
                        criteriaBuilder.or(
                                criteriaBuilder.equal(root.get("empNo"), name),
                                criteriaBuilder.equal(root.get("realName"), name))

                );
            }
            CriteriaBuilder.In<StaffGroup> inInitiator = criteriaBuilder.in(root.get("groupId"));
            groupIdsList.forEach(s -> inInitiator.value(s));
            list.add(criteriaBuilder.and(inInitiator));
            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        });

        // 去重后结果
        ArrayList<StaffGroupMembers> collect = all.stream().collect(collectingAndThen(
                toCollection(() -> new TreeSet<>(comparing(n -> n.getRealName()))), ArrayList::new));
        return transFormData(collect, pageable);
    }


    /**
     * 获取设备通行记录
     *
     * @param req
     * @return
     */
    @Override
    @Transactional
    public PageBean<FaceRecordPageResp> accessRecord(HkFaceRecordPageReq req) {
        Pageable pageable = PageRequest.of(req.getPage(), req.getSize(), Sort.by(Sort.Direction.DESC, "createdTime"));

        Optional<DoorMagnetic> byId = doorMagneticDao.findById(req.getDeviceId());
        // 查询分页数据
        Page<DoorMagneticAccessRecord> recordDaoAll = doorMagneticAccessRecordDao.findAll((root, criteriaQuery, criteriaBuilder) -> {
            //集合 用于封装查询条件
            List<Predicate> list = new ArrayList<>();
            list.add(criteriaBuilder.equal(root.get("doorMagnetic"), byId.get()));
            // 通行时间
            if (StringUtils.isNoneBlank(req.getStartDate()) &&
                    StringUtils.isNoneBlank(req.getEndDate())) {
                list.add(criteriaBuilder.between(root.get("dataTime").as(String.class),
                        req.getStartDate(),
                        req.getEndDate()));
            }
            // 姓名
            if (StringUtils.isNoneBlank(req.getName())) {
                list.add(
                        criteriaBuilder.or(
                                criteriaBuilder.equal(root.get("empNo"), req.getName()),
                                criteriaBuilder.equal(root.get("nameZH"), req.getName()))

                );
            }
            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, pageable);
        //格式化输出
        return transFormRecordData(recordDaoAll);
    }

    /**
     * 参数检测
     *
     * @param param 请求参数
     * @return 检测结果
     * @author Yang.Lee
     * @date 2021/4/7 10:12
     **/
    @Transactional
    public ServiceResult ruleParamCheck(QrcodeAccessRuleReq param) {

        ServiceResult result;

        QrcodeAccessRules rule = new QrcodeAccessRules();

        // 验证门磁设备是否存在
        Optional<DoorMagnetic> doorMagneticOptional = doorMagneticDao.findById(param.getMagneticDeviceId());
        if (!doorMagneticOptional.isPresent()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUNT_DOOR_MAGNETISM, param.getMagneticDeviceId()));
        }
        rule.setDoorMagnetic(doorMagneticOptional.get());

        // 验证用户组是否存在
        result = staffGroupService.isGroupExist(param.getStaffGroups().toArray(new String[0]));
        if (!result.isSuccess()) {
            return result;
        }

        // 验证时间计划
        result = timePlanService.isPlanExist(param.getTimePlanId());
        if (!result.isSuccess()) {
            return result;
        }
        rule.setTimePlan((TimePlan) result.getObject());

        return ServiceResult.ok(rule);
    }

    /**
     * 人员列表数据转换
     *
     * @param source
     * @return
     */
    private PageBean<UserPageResp> transFormData(List<StaffGroupMembers> source, Pageable pageable) {

        int seq = pageable.getPageNumber() * pageable.getPageSize();

        //总页数
        int left = source.size() % pageable.getPageSize();
        int right = source.size() / pageable.getPageSize();
        int totalPages = left == 0 ? right : right + 1;

        PageBean<UserPageResp> resultPage = new PageBean<>(pageable.getPageNumber(), pageable.getPageSize());
        resultPage.setTotalElements(source.size());
        resultPage.setTotalPages(totalPages);
        List<UserPageResp> result = new ArrayList<>();

        if (source.isEmpty()) {
            resultPage.setList(result);
        } else {
            // 手动分页
            for (int i = seq; i < seq + pageable.getPageSize(); i++) {
                if (source.size() > i) {
                    StaffGroupMembers staffGroupMember = source.get(i);
                    SysUser userByEmpNo = sysUserService.getUserByEmpNo(staffGroupMember.getEmpNo());
                    result.add(
                            UserPageResp.builder()
                                    .seq(i + 1)
                                    .name(Optional.ofNullable(staffGroupMember.getRealName()).orElse(""))
                                    .dept(userByEmpNo != null ? Optional.ofNullable(userByEmpNo.getDeptName()).orElse("") : "")
                                    .jobNum(Optional.ofNullable(staffGroupMember.getEmpNo()).orElse(""))
                                    .build());
                }
            }
            resultPage.setList(result);
        }
        return resultPage;
    }

    /**
     * 转化记录数据
     *
     * @param source
     * @return
     */
    private PageBean<FaceRecordPageResp> transFormRecordData(Page<DoorMagneticAccessRecord> source) {
        int seq = source.getNumber() * source.getSize();

        PageBean<FaceRecordPageResp> resultPage = new PageBean<>(source.getNumber(), source.getSize());
        resultPage.setTotalElements(source.getTotalElements());
        resultPage.setTotalPages(source.getTotalPages());
        List<FaceRecordPageResp> result = new ArrayList<>();
        if (source.getContent().isEmpty()) {
            resultPage.setList(result);
        } else {
            source.getContent().forEach(obj -> {
                int index = source.getContent().indexOf(obj);
                if (obj.getEmpNo() == null){
                    FaceRecordPageResp userPageResp = FaceRecordPageResp.builder()
                            .seq(seq + index + 1)
                            .jobNum("")
                            .name(Optional.ofNullable(obj.getNameZH()).orElse(""))
                            .status(Optional.ofNullable(obj.getAccessState().getName()).orElse(""))
                            .dept("")
                            .happenTime(obj.getCreatedTime())
                            .build();
                    result.add(userPageResp);
                }else {
                    SysUser userByEmpNo = sysUserService.getUserByEmpNo(obj.getEmpNo());
                    FaceRecordPageResp userPageResp = FaceRecordPageResp.builder()
                            .seq(seq + index + 1)
                            .jobNum(Optional.ofNullable(obj.getEmpNo()).orElse(""))
                            .name(Optional.ofNullable(obj.getNameZH()).orElse(""))
                            .status(Optional.ofNullable(obj.getAccessState().getName()).orElse(""))
                            .dept(userByEmpNo != null ? Optional.ofNullable(userByEmpNo.getDeptName()).orElse("") : "")
                            .happenTime(obj.getCreatedTime())
                            .build();
                    result.add(userPageResp);
                }
            });
            resultPage.setList(result);
        }
        return resultPage;
    }
}
