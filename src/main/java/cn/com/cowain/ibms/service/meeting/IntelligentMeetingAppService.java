package cn.com.cowain.ibms.service.meeting;

import cn.com.cowain.ibms.rest.req.working_mode.BandingReq;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.websocket.enumeration.MessageType;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: yanzy
 * @date: 2021/12/29/18:02
 * @version:W
 */
public interface IntelligentMeetingAppService {

    /**
     * 会议室绑定设备
     *
     * @author: yanzy
     * @date: 2021/12/29
     */
    ServiceResult banding(BandingReq req);

    /**
     * 会议室设备绑定修改
     *
     * @author yanzy
     * @date 2021/12/30
     */
    ServiceResult updateBanding(BandingReq req);

    /**
     * 二维码生成
     *
     * @author yanzy
     * @date 2021/1/4
     */
    ServiceResult createQrCode(String roomId);

    /**
     * 根据会议室ID查询会议详情
     *
     * @param roomId 会议室ID
     * @return 会议详情， 当查询是成功时返回MeetingAppResp对象
     * @author Yang.Lee
     * @date 2022/1/5 15:34
     **/
    ServiceResult getMeetingAppDetail(String roomId);

    /**
     * 处理app请求消息
     *
     * @param messageType end/extend   结束/延长会议
     * @param cmd    指令
     * @return
     * @author Yang.Lee
     * @date 2022/1/6 10:42
     **/
    ServiceResult meetingMessage(MessageType messageType, String[] cmd);

    /**
     * 推送会议室信息
     *
     * @param roomId 会议室ID,将会向所有连接到该会议室的设备推送消息
     * @return
     * @author Yang.Lee
     * @date 2022/1/19 9:39
     **/
    void pushMeetingRoomDetail(String roomId);
}
