package cn.com.cowain.ibms.rest.resp.iot;

import cn.com.cowain.ibms.enumeration.UserImportStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/4/18 16:27
 */
@Data
@ApiModel("门禁失败人员响应对象")
public class AccessControlUserFailPageResp {

    @ApiModelProperty(value = "姓名", required = true)
    private String name;

    @ApiModelProperty(value = "工号", required = true)
    private String empNo;

    @ApiModelProperty(value = "部门", required = true)
    private String dept;

    @ApiModelProperty(value = "来源")
    private String source = "";

    @ApiModelProperty(value = "原因")
    private String cause = "";

    @ApiModelProperty(value = "人员状态")
    private UserImportStatus userImportStatus;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "同步时间")
    private LocalDateTime createdTime;


}
