package cn.com.cowain.ibms.feign.bean.oa.guest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 访客邀请事由对象
 *
 * @author Yang.Lee
 * @date 2021/6/30 16:33
 */
@Data
@ApiModel("访客系统邀请事由对象")
public class InvitationReason {

    @ApiModelProperty(value = "code")
    private String code;

    @ApiModelProperty(value = "name")
    private String name;

    @ApiModelProperty(value = "sortNo")
    private int sortNo;

    @ApiModelProperty(value = "flg")
    private String flg;

    @ApiModelProperty(value = "comment")
    private String comment;

    public static final String getDefaultComment(String code) {

        if ("2003".equals(code)) {
            return "您的邀请单待访客填写后需要得到汪先姚（陈春梅，张秋萍，任素馨，师赟杰，伏脆芳，陈德娥，章雅萌，";
        } else {
            return "您的邀请单待访客填写后需要得到王斌的审批，请悉知";
        }
    }
}
