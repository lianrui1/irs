package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author Yang.Lee
 * @date 2022/6/24 9:28
 */
@Data
@Entity
@Table(name = "iot_left_column")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_left_column", comment = "升降柱设备表")
public class LiftColumn extends BaseEntity {


    /**
     * iot 设备
     */
    @OneToOne
    @JoinColumn(name = "device_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK。设备表主键id'")
    private IotDevice device;
}
