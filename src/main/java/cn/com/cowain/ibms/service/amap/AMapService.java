package cn.com.cowain.ibms.service.amap;

import cn.com.cowain.ibms.service.bean.amap.enumeration.CoordinateSystem;
import cn.com.cowain.ibms.service.bean.amap.geo.ReGeoCode;
import cn.com.cowain.ibms.service.bean.amap.poi.LonLat;
import cn.com.cowain.ibms.service.bean.amap.weather.ForceCastsTempResult;
import cn.com.cowain.ibms.service.bean.amap.weather.Lives;

import java.util.List;
import java.util.Map;

/**
 * 高德地图开放api Service
 *
 * @author Yang.Lee
 * @date 2021/4/19 14:31
 */
public interface AMapService {

    /**
     * 高德天气API地址
     **/
    String API_WEATHER = "https://restapi.amap.com/v3/weather/weatherInfo";

    /**
     * 高德坐标转换API地址
     **/
    String API_COORDINATE_CONVERT = "https://restapi.amap.com/v3/assistant/coordinate/convert";

    /**
     * 逆地理编码
     **/
    String API_REGEO = "https://restapi.amap.com/v3/geocode/regeo";

    /**
     * 获取实时天气信息
     *
     * @param cityCode 城市编号
     * @return 天气信息
     * @author Yang.Lee
     * @date 2021/4/19 15:01
     **/
    Lives getLiveWeather(String cityCode);
    /**
     * 获取天气预报(今天与未来三天的天气)
     *
     * @param cityCode 城市编号
     * @return 天气信息
     * @author Yang.Lee
     * @date 2021/4/19 15:01
     **/

    ForceCastsTempResult getWeatherForecasts(String cityCode);

    /**
     * 坐标转换
     *
     * @param cs      待转坐标所属坐标系
     * @param lonLats 待转坐标列表，单次转换最多40对坐标。单对坐标小数点后最多6位数字
     * @return 转后的高德坐标
     * @author Yang.Lee
     * @date 2021/4/19 19:38
     **/
    List<LonLat> coordinateConvert(CoordinateSystem cs, LonLat... lonLats);

    /**
     * 逆地理编码
     *
     * @param params   可选参数，具体参数参照高德api（无需包含location字段）。https://lbs.amap.com/api/webservice/guide/api/georegeo
     * @param location 经纬度坐标,经纬度小数点后不要超过 6 位
     * @return 逆地理解析结果
     * @author Yang.Lee
     * @date 2021/4/20 14:10
     **/
    List<ReGeoCode> reGeo(Map<String, Object> params, LonLat... location);
}
