package cn.com.cowain.ibms.websocket.bean;

import cn.com.cowain.ibms.websocket.client.RTASRClient;
import lombok.Data;

import javax.websocket.Session;

/**
 * WS 客户端消息对象类
 * @author Yang.Lee
 * @date 2021/1/12 10:03
 */
@Data
public class RTASRBean {

    public RTASRBean(){}

    public RTASRBean(String id, Session session, RTASRClient client){
        this.id = id;
        this.session = session;
        this.client = client;
    }

    /**
     * 音频ID
     */
    private String id;
    /**
     * ibms ws客户端session
     */
    private Session session;
    /**
     * 讯飞ws客户端对象
     */
    private RTASRClient client;
}
