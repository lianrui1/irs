-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 10.0.1.232    Database: ibms_uat
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `iot_door_magnetic`
--

DROP TABLE IF EXISTS `iot_door_magnetic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `iot_door_magnetic` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `created_by` varchar(36) DEFAULT NULL COMMENT '创建人',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(36) DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int(11) DEFAULT NULL COMMENT '版本号',
  `door_magnetic_mac` varchar(20) DEFAULT NULL COMMENT '门磁设备mac地址',
  `entity_id` varchar(36) NOT NULL COMMENT 'iot编号',
  `name` varchar(30) DEFAULT NULL COMMENT '名称',
  `number` varchar(50) NOT NULL COMMENT '编号',
  `device_id` varchar(36) NOT NULL COMMENT 'FK',
  `huawei_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '华为云硬件ID',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK9pmrehaunftbjp323iafk0npf` (`device_id`) USING BTREE,
  CONSTRAINT `FK9pmrehaunftbjp323iafk0npf` FOREIGN KEY (`device_id`) REFERENCES `iot_device` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='门磁';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `iot_door_magnetic`
--

LOCK TABLES `iot_door_magnetic` WRITE;
/*!40000 ALTER TABLE `iot_door_magnetic` DISABLE KEYS */;
INSERT INTO `iot_door_magnetic` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `door_magnetic_mac`, `entity_id`, `name`, `number`, `device_id`, `huawei_id`) VALUES ('1',NULL,'2020-12-30 20:06:47',0,NULL,'2020-12-30 20:06:52',1,'FCF5C48BA0F7','2d6c5330-3921-11eb-a2af-6ff688968db5','花桥','123','123',NULL);
INSERT INTO `iot_door_magnetic` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `door_magnetic_mac`, `entity_id`, `name`, `number`, `device_id`, `huawei_id`) VALUES ('2',NULL,'2020-12-30 20:07:05',0,NULL,'2020-12-30 20:07:07',1,'FCF5C48B643D','505133d0-3b55-11eb-a2af-6ff688968db5','昆山','fm21zu8v','ff566fe6-12ed-4eed-816f-37d664602dcf',NULL);
INSERT INTO `iot_door_magnetic` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `door_magnetic_mac`, `entity_id`, `name`, `number`, `device_id`, `huawei_id`) VALUES ('2bef40cf-18c6-4b8f-aba3-07e07a3225cd',NULL,'2021-02-08 14:13:20',0,NULL,'2021-02-08 14:13:20',0,NULL,'','DM9_开阳','010009','920a5838-aced-4d6b-9c8c-6461d09ddee3','60136fbc466c0502e27b8d02_000083102-010009');
INSERT INTO `iot_door_magnetic` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `door_magnetic_mac`, `entity_id`, `name`, `number`, `device_id`, `huawei_id`) VALUES ('2eb88d53-740d-4a83-8868-9f94d1cf78f7',NULL,'2021-02-08 14:13:20',0,NULL,'2021-02-08 14:13:20',0,NULL,'','DM6_天权','010006','c6681595-01b1-4dd0-ad17-8d9fb792d0b2','60136fbc466c0502e27b8d02_000083102-010006');
INSERT INTO `iot_door_magnetic` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `door_magnetic_mac`, `entity_id`, `name`, `number`, `device_id`, `huawei_id`) VALUES ('3',NULL,'2021-01-26 11:10:04',0,NULL,'2021-01-26 11:10:08',1,'FCF5C48B6BAF','1','花桥右','456','123',NULL);
INSERT INTO `iot_door_magnetic` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `door_magnetic_mac`, `entity_id`, `name`, `number`, `device_id`, `huawei_id`) VALUES ('4775d863-9917-4ab7-9667-9322af2d332c',NULL,'2021-02-08 14:13:20',0,NULL,'2021-02-08 14:13:20',0,NULL,'','DM7','010007','83e8ecf3-ac8c-44c0-b612-13bbd3e60352','60136fbc466c0502e27b8d02_000083102-010007');
INSERT INTO `iot_door_magnetic` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `door_magnetic_mac`, `entity_id`, `name`, `number`, `device_id`, `huawei_id`) VALUES ('541f72e4-bed4-4c6d-ad20-27ac2cadf4b4',NULL,'2021-02-08 14:13:19',0,NULL,'2021-02-08 14:13:19',0,NULL,'','DM2_天枢','010002','12995e85-158e-4551-91fe-7968e820bb7c','60136fbc466c0502e27b8d02_000083102-010002');
INSERT INTO `iot_door_magnetic` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `door_magnetic_mac`, `entity_id`, `name`, `number`, `device_id`, `huawei_id`) VALUES ('97720975-5579-471f-b557-9ec2062c5fc3',NULL,'2021-02-08 14:13:20',0,NULL,'2021-02-08 14:13:20',0,NULL,'','DM8_玉衡','010008','f4bdb7ed-f0d4-4792-8e48-51a713510de9','60136fbc466c0502e27b8d02_000083102-010008');
INSERT INTO `iot_door_magnetic` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `door_magnetic_mac`, `entity_id`, `name`, `number`, `device_id`, `huawei_id`) VALUES ('a9f04d40-7738-4fb8-b521-17e14a9f7b42',NULL,'2021-02-08 14:13:20',0,NULL,'2021-02-08 14:13:20',0,NULL,'','DM4_家在高新','010004','61094c79-3ffe-44a7-869e-31842977a704','60136fbc466c0502e27b8d02_000083102-010004');
INSERT INTO `iot_door_magnetic` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `door_magnetic_mac`, `entity_id`, `name`, `number`, `device_id`, `huawei_id`) VALUES ('ca56dc1e-13df-4805-967c-d2c265bc14c3',NULL,'2021-02-08 14:13:20',0,NULL,'2021-02-08 14:13:20',0,NULL,'','DM5_天玑','010005','28903ad9-af22-4330-895e-c2d19eb245fa','60136fbc466c0502e27b8d02_000083102-010005');
INSERT INTO `iot_door_magnetic` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `door_magnetic_mac`, `entity_id`, `name`, `number`, `device_id`, `huawei_id`) VALUES ('ea71aa78-a71a-4b4d-94af-b48f4dec0f17',NULL,'2021-02-08 14:13:19',0,NULL,'2021-02-08 14:13:19',0,NULL,'','DM3_劳动争议调解室','010003','0c55c1b5-28b9-4680-a929-b6949a56091f','60136fbc466c0502e27b8d02_000083102-010003');
INSERT INTO `iot_door_magnetic` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `door_magnetic_mac`, `entity_id`, `name`, `number`, `device_id`, `huawei_id`) VALUES ('efe56a14-5495-4e9e-ada0-c2269bde90ff',NULL,'2021-02-08 14:07:49',0,NULL,'2021-02-08 14:07:49',0,NULL,'','DM1_天璇','010001','054ca60f-b13d-403c-a481-b8f52af2fc1b','60136fbc466c0502e27b8d02_000083102-010001');
/*!40000 ALTER TABLE `iot_door_magnetic` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-26  9:34:26
