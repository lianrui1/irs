package cn.com.cowain.ibms.rest.req;

import lombok.Data;

import java.io.Serializable;

/**
 * OpeningHours
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/6/20
 */
@Data
public class OpeningHoursReq implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 从时间
     */
    private String from;

    /**
     * 到时间
     */
    private String to;

}
