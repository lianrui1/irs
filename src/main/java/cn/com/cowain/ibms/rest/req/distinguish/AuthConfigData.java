package cn.com.cowain.ibms.rest.req.distinguish;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/27 9:26
 */
@Data
public class AuthConfigData {

    @ApiModelProperty(value = "权限开始时间（报餐必填）", required = true)
    private String startTime;
    @ApiModelProperty(value = "权限结束时间（报餐必填）", required = true)
    private String endTime;
    @ApiModelProperty(value = "人员工号", required = true)
    private String jobNo;

}
