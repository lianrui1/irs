package cn.com.cowain.ibms.service.meeting;

import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.meeting.AttendancePageReq;
import cn.com.cowain.ibms.rest.resp.meeting.AttendancePageResp;

import javax.annotation.Nullable;

/**
 * 用户会议签到Service
 *
 * @author Yang.Lee
 * @date 2021/6/15 10:22
 */
public interface AttendanceService {

    /**
     * 会议签到(异步执行)
     *
     * @param reservationRecord 会议信息
     * @param empNo             用户工号
     * @author Yang.Lee
     * @date 2021/6/15 10:24
     **/
    void signIn(@Nullable ReservationRecord reservationRecord, String empNo);

    /**
     * 获取分页数据
     *
     * @param req 查询参数
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/7/16 16:11
     **/
    PageBean<AttendancePageResp> getPage(AttendancePageReq req);
}
