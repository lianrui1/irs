package cn.com.cowain.ibms.rest.audio;

import cn.com.cowain.ibms.dao.audio.AudioDao;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.voice.AudioRecord;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.audio.RealtimeUpload;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.ReservationRecordResp;
import cn.com.cowain.ibms.rest.resp.RoomAllResp;
import cn.com.cowain.ibms.rest.resp.audio.AudioDetailResp;
import cn.com.cowain.ibms.rest.resp.audio.AudioRecordResp;
import cn.com.cowain.ibms.rest.resp.audio.AudioRecordRespPage;
import cn.com.cowain.ibms.service.RoomService;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.audio.AudioService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.utils.DateUtil;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2020/12/29 14:53
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/audio")
@Api(tags = IConst.AUDIO)
public class AudioRecordController {

    @Autowired
    private AudioService audioService;
    @Autowired
    private RoomService roomService;
    @Autowired
    private AudioDao audioDao;
    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserService;

    /**
     * 新增会议记录
     *
     * @param audioRecordResp 会议记录信息
     * @param bindingResult
     * @return
     */
    @PostMapping
    @ApiOperation(value = "PC新增会议记录", tags = IConst.AUDIO)
    public ResponseEntity<JsonResult<IdResp>> save(@RequestBody @Validated @ApiParam("会议记录") AudioRecordResp audioRecordResp,
                                                   BindingResult bindingResult) {
        log.debug("save...");
        ServiceResult result = audioService.save(audioRecordResp);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error("会议记录不存在", null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.ok("OK", (IdResp) result.getObject()));
    }

    /**
     * 更新会议记录
     *
     * @param id 会议记录ID
     * @return
     */
    @PutMapping("/{id}")
    @ApiOperation(value = "PC更新会议记录", tags = IConst.AUDIO)
    public ResponseEntity<JsonResult<String>> update(@PathVariable @ApiParam("音频记录ID") String id) {
        log.debug("update...");
        Optional<AudioRecord> byId = audioDao.findById(id);
        if (!byId.isPresent()) {
            return ResponseEntity.ok(JsonResult.error("音频记录不存在", null, ErrConst.E01));
        }
        ServiceResult result = audioService.update(id);
        JsonResult<String> jsonResult = JsonResult.okNoData("OK");
        if (!result.isSuccess()) {
            jsonResult = JsonResult.error(result.getObject().toString(), null, ErrConst.E01);
        }
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 删除会议记录
     *
     * @param id 会议记录ID
     * @return
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "PC删除会议", tags = IConst.AUDIO)
    public ResponseEntity<JsonResult<String>> delete(@PathVariable @ApiParam("会议记录ID") String id, @RequestHeader("Authorization") String token) {

        SysUser sysUser = sysUserService.getUserByToken(token);
        // 获取用户工号
        String empNo = sysUser.getEmpNo();

        JsonResult<String> jsonResult = JsonResult.okNoData("OK");
        // 判断该音频记录是否存在
        Optional<AudioRecord> audioOptional = audioDao.findByIdAndReservationRecordInitiatorEmpNo(id, empNo);
        boolean present = audioOptional.isPresent();
        if (present) {
            AudioRecord audioRecord = audioOptional.get();
            audioRecord.setIsDelete(1);
            audioDao.save(audioRecord);
        } else {
            jsonResult = JsonResult.error("你无权删除该音频记录", "", ErrConst.E01);
        }
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 会议记录分页查询  根据会议室名称和会议时间模糊查
     *
     * @param name
     * @param from
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/search/page")
    @ApiOperation(value = "分页查询会议记录", tags = IConst.AUDIO)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "会议室名称"),
            @ApiImplicitParam(name = "from", value = "会议日期"),
            @ApiImplicitParam(name = "page", value = "查询页码"),
            @ApiImplicitParam(name = "size", value = "查询页长"),
    })
    public ResponseEntity<JsonResult<PageBean<AudioRecordRespPage>>> searchAll(@RequestParam(value = "name", required = false, defaultValue = "") String name,
                                                                               @RequestParam(value = "from", required = false, defaultValue = "") String from,
                                                                               @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
                                                                               @RequestParam(value = "size", required = false, defaultValue = "30") Integer size,
                                                                               @RequestHeader("Authorization") String token) {

        if (token.isEmpty()) {
            return ResponseEntity.ok(JsonResult.error("请正确登录", null, ErrConst.E01));
        }
        SysUser sysUser = sysUserService.getUserByToken(token);
        // 获取用户唯一ID
        String empNo = sysUser.getEmpNo();
        if (page < 0 || size < 0) {
            return ResponseEntity.ok(JsonResult.error("你输入参数有误", null, ErrConst.E01));
        }
        // 参数：搜索条件, 分页页码,
        // 数据：数据，页码，总页数，
        //初始化pageBean
        PageBean<AudioRecordRespPage> pageBean = new PageBean<>(page, size);
        //字符串转date
        if (!from.isEmpty()) {
            LocalDate localDate = DateUtil.convert(from);
            pageBean = audioService.findByRoomName(name, localDate, empNo, pageBean);
            JsonResult<PageBean<AudioRecordRespPage>> jr = JsonResult.ok("OK", pageBean);
            return ResponseEntity.ok(jr);
        } else {
            pageBean = audioService.findByName(name, empNo, pageBean);
            JsonResult<PageBean<AudioRecordRespPage>> jr = JsonResult.ok("OK", pageBean);
            return ResponseEntity.ok(jr);
        }
    }

    /**
     * 根据会议室ID和日期查询当天预约会议清单
     *
     * @param roomId
     * @param nowDate
     * @return
     */
    @GetMapping("/search")
    @ApiOperation(value = "查询会议预约清单", tags = IConst.AUDIO)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "roomId", value = "会议室ID"),
            @ApiImplicitParam(name = "nowDate", value = "查询日期")
    })
    public ResponseEntity<JsonResult<List<ReservationRecordResp>>> searchReservationRecordList(
            @RequestParam(value = "roomId") String roomId,
            @RequestParam(value = "nowDate") String nowDate,
            @RequestHeader("Authorization") String token) {
        if (token.isEmpty()) {
            return ResponseEntity.ok(JsonResult.error("请正确登录", null, ErrConst.E01));
        }
        SysUser sysUser = sysUserService.getUserByToken(token);
        // 获取用户唯一ID
        String empNo = sysUser.getEmpNo();

        //字符串转date
        LocalDate localDate = DateUtil.convert(nowDate);
        List<ReservationRecordResp> roomWithReservationResp = audioService.findByRoomIdAndDate(roomId, localDate, empNo);
        return ResponseEntity.ok(JsonResult.ok("OK", roomWithReservationResp));
    }

    /**
     * 查询所有会议室
     *
     * @return
     */
    @GetMapping
    @ApiOperation(value = "PC查询所有会议室", tags = IConst.AUDIO)
    public ResponseEntity<JsonResult<List<RoomAllResp>>> searchList() {
        // 查询可用会议室列表
        List<RoomAllResp> result = roomService.searchAllRoomList();
        return ResponseEntity.ok(JsonResult.ok("OK", result));
    }


    /**
     * 上传语音文件
     *
     * @param audioRecordId
     * @return
     * @author Yang.Lee
     */
    @PostMapping("/{id}/upload")
    @ApiOperation(value = "APP上传音频文件", tags = IConst.AUDIO)
    public ResponseEntity<JsonResult<String>> upload(@PathVariable("id") @ApiParam(value = "语音记录ID", required = true) String audioRecordId,
//                                                      @RequestParam("file") @ApiParam(value = "音频文件", required = true) MultipartFile file,
                                                     @RequestParam("endTime") @ApiParam(value = "会议结束时间", required = true) String endTime,
                                                     @RequestParam("path") @ApiParam(value = "文件在fastdfs中的地址", required = true) String path) throws IOException {

        log.debug("录音上传，文件地址：{}", path);

        ServiceResult result = null;

        // 进行文件上传
        try {
            result = audioService.uploadToFastDFS(audioRecordId, path, endTime);
        } catch (Exception e) {
            log.error("文件上传到fastDFS服务器出现异常！!", e);

            if(e instanceof InterruptedException){
                Thread.currentThread().interrupt();
            }

            return ResponseEntity.ok(JsonResult.error("文件上传失败", null, ErrConst.E03));
        }

        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error((String) result.getObject(), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.ok("OK", null));
    }

    /**
     * 查询音频详情
     *
     * @param audioRecoidId
     * @return
     * @author Yang.Lee
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "PC查询音频详情", tags = IConst.AUDIO)
    public ResponseEntity<JsonResult<AudioDetailResp>> get(@PathVariable("id") @ApiParam(value = "语音记录ID", required = true) String audioRecoidId) {

        ServiceResult result = audioService.get(audioRecoidId);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error((String) result.getObject(), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.ok("OK", (AudioDetailResp) result.getObject()));
    }

    /**
     * 查询实时音频详情
     *
     * @param audioRecordId
     * @return
     */
    @GetMapping("/{id}/realtime")
    @ApiOperation(value = "PC查询实时音频详情", tags = IConst.AUDIO)
    public ResponseEntity<JsonResult<AudioDetailResp>> getRealtime(@PathVariable("id") @ApiParam(value = "语音记录ID", required = true) String audioRecordId) {

        ServiceResult result = audioService.getRealTimeAudioRecord(audioRecordId);

        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E11));
        }

        return ResponseEntity.ok(JsonResult.ok("OK", (AudioDetailResp) result.getObject()));
    }

    /**
     * 补上传实时音频
     *
     * @param realtimeUpload 请求参数
     * @param audioRecordId  音频ID
     * @return
     * @author Yang.Lee
     */
    @PostMapping("/{id}/realtime/upload")
    @ApiOperation(value = "APP上传音频文件", tags = IConst.AUDIO)
    public ResponseEntity<JsonResult<String>> realtimeUpload(@PathVariable("id") @ApiParam(value = "语音记录ID", required = true) String audioRecordId,
                                                             RealtimeUpload realtimeUpload) {

        ServiceResult result = null;

        // 校验文件格式
        result = audioService.checkPcmFile(realtimeUpload.getFile());
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        // 进行上传操作
        result = audioService.realtimeUpload(audioRecordId, realtimeUpload);

        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error((String) result.getObject(), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.ok("OK", null));
    }

    /**
     * app断线消息
     *
     * @return
     */
    @PostMapping("/{id}/realtime/offline")
    @ApiOperation(value = "APP离线", tags = IConst.AUDIO)
    public ResponseEntity<JsonResult<String>> offLine(@PathVariable("id") @ApiParam(value = "语音记录ID", required = true) String audioRecordId) {

        log.debug("收到app断线消息，audioId :" + audioRecordId);
        audioService.rtasrBreak(audioRecordId);

        return ResponseEntity.ok(JsonResult.okNoData("ok"));
    }
}
