package cn.com.cowain.ibms.dao;

import cn.com.cowain.ibms.dao.iot.AccessControlTimePlanDao;
import cn.com.cowain.ibms.entity.iot.AccessControlTimePlan;
import cn.com.cowain.ibms.rest.bean.During;
import cn.com.cowain.ibms.rest.bean.WorkDay;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author wei.cheng
 * @date 2022/09/28 14:02
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AccessControlTimePlanDaoTest {
    @Autowired
    private AccessControlTimePlanDao accessControlTimePlanDao;

    @Test
    public void testSave() {
        AccessControlTimePlan accessControlTimePlan = new AccessControlTimePlan();
        accessControlTimePlan.setSpotId("test");
        accessControlTimePlan.setUserHrId("XBM3112");
        During during = new During();
        during.setFrom("09:00");
        during.setTo("10:00");
        WorkDay workDay = new WorkDay();
        workDay.setWeek(1);
        workDay.setDurings(Lists.newArrayList(during));
        accessControlTimePlan.setWorkDays(Lists.newArrayList(workDay));
        accessControlTimePlanDao.save(accessControlTimePlan);
    }

    @Test
    public void testQuery() {
        List<AccessControlTimePlan> accessControlTimePlanList = accessControlTimePlanDao.findAll();
        System.out.println(JSON.toJSONString(accessControlTimePlanList));
    }
}
