package cn.com.cowain.ibms.rest.distinguish;

import cn.com.cowain.ibms.rest.resp.distinguish.TimePlanListResp;
import cn.com.cowain.ibms.service.disthinguish.TimePlanService;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * 时间计划Controller
 *
 * @author Yang.Lee
 * @date 2021/3/11 13:26
 */
@RestController
@RequestMapping(API_BASE + "/distinguish/KsHtTimePlan")
@Api(tags = IConst.DISTINGUISH)
public class TimePlanController {

    @Resource
    private TimePlanService timePlanService;

    /**
     * 查询时间计划列表
     *
     * @return 查询结构
     * @author Yang.Lee
     * @date 2021/3/11 13:55
     **/
    @GetMapping("/list")
    @ApiOperation(value = "PC查询时间计划列表", tags = IConst.DISTINGUISH)
    public ResponseEntity<JsonResult<List<TimePlanListResp>>> list() {

        return ResponseEntity.ok(JsonResult.ok("", timePlanService.getList()));
    }
}
