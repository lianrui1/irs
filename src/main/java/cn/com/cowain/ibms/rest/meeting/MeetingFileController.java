package cn.com.cowain.ibms.rest.meeting;

import cn.com.cowain.ibms.annotation.Idempotent;
import cn.com.cowain.ibms.rest.req.meeting.MeetingFileCreateReq;
import cn.com.cowain.ibms.rest.resp.meeting.MeetingFileResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.meeting.MeetingFileService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author wei.cheng
 * @date 2022/02/22 16:58
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/meetingFile")
@Api(tags = {IConst.MODULE_MEETING_FILE})
public class MeetingFileController {
    @Autowired
    private MeetingFileService meetingFileService;

    @PostMapping
    @ApiOperation(value = "创建会议文档", tags = {IConst.MODULE_MEETING_FILE, IConst.V25})
    @Idempotent(value = API_BASE + "/meetingFile/post", expireTime = 2L)
    public ResponseEntity<JsonResult<MeetingFileResp>> createMeetingFile(@RequestHeader(name = "Authorization") @ApiParam(value = "当前用户token") String token,
                                                                         @RequestBody @Validated MeetingFileCreateReq req,
                                                                         BindingResult bindingResult) {
        JsonResult<MeetingFileResp> jsonResult;
        ServiceResult result = meetingFileService.createMeetingFile(req, token);
        if (!result.isSuccess()) {
            jsonResult = JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01);
        } else {
            jsonResult = JsonResult.ok("ok", (MeetingFileResp) result.getObject());
        }
        return ResponseEntity.ok(jsonResult);
    }

    @GetMapping("/search")
    @ApiOperation(value = "查询会议文档列表", tags = {IConst.MODULE_MEETING_FILE, IConst.V25})
    public ResponseEntity<JsonResult<List<MeetingFileResp>>> searchMeetingFileOfReservationRecord(@RequestParam(name = "reservationRecordId")
                                                                                                  @ApiParam(value = "会议室预约ID", required = true) String reservationRecordId) {
        ServiceResult result = meetingFileService.searchMeetingFileOfReservationRecord(reservationRecordId);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.ok("OK", (List<MeetingFileResp>) result.getObject()));
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除会议中文档", tags = {IConst.MODULE_MEETING_FILE, IConst.V25})
    public ResponseEntity<JsonResult<MeetingFileResp>> delete(@RequestHeader(name = "Authorization") @ApiParam(value = "当前用户token") String token,
                                                              @PathVariable(name = "id") @ApiParam(value = "会议中文档的ID", required = true) String id) {
        JsonResult<MeetingFileResp> jsonResult;
        ServiceResult result = meetingFileService.deleteMeetingFile(id, token);
        if (!result.isSuccess()) {
            jsonResult = JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01);
        } else {
            jsonResult = JsonResult.ok("ok", (MeetingFileResp) result.getObject());
        }
        return ResponseEntity.ok(jsonResult);
    }
}
