package cn.com.cowain.ibms.job;

import cn.com.cowain.ibms.dao.device.CentralizedControlCodeDao;
import cn.com.cowain.ibms.entity.centralized.CentralizedControlCode;
import cn.com.cowain.ibms.service.device.CentralizedControlService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author wei.cheng
 * @date 2022/09/19 16:22
 */
@Slf4j
@Component
public class CentralizedCodeCacheJob {
    @Autowired
    private CentralizedControlCodeDao centralizedControlCodeDao;
    @Autowired
    private CentralizedControlService centralizedControlService;

    /**
     * 每5分钟刷新一次集控码缓存数据
     */
    @Scheduled(fixedDelay = 1000 * 60 * 5)
    public void run() {
        log.info("定时任务开始，刷新集控码缓存数据");
        List<CentralizedControlCode> centralizedControlCodes = centralizedControlCodeDao.findAll();
        centralizedControlCodes.forEach(centralizedControlCode -> centralizedControlService.refreshCentralizedCodeCacheData(centralizedControlCode));
    }
}
