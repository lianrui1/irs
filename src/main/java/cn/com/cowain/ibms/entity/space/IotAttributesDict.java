package cn.com.cowain.ibms.entity.space;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * IOT 数据点 字典表
 */
@Data
@Entity
@Table(name = "iot_attributes_dict")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_attributes_dict", comment = "产品关联属性字典表")
public class IotAttributesDict extends BaseEntity {

    /**
     * 属性名称
     */
    @Column(name = "name", nullable = false, columnDefinition = "varchar(20) COMMENT '属性名称'")
    private String name;

    /**
     * 排序
     */
    @Column(name = "prod_seq", columnDefinition = "int(2) COMMENT '排序'")
    private int seq;

}
