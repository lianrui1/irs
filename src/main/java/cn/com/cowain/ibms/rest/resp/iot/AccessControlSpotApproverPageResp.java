package cn.com.cowain.ibms.rest.resp.iot;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/3/8 13:05
 */
@Data
@ApiModel("门禁点通行权限分配人列表响应对象")
public class AccessControlSpotApproverPageResp {

    @ApiModelProperty(value = "id", required = true)
    private String id;

    @ApiModelProperty(value = "分配人姓名", required = true)
    private String name;

    @ApiModelProperty(value = "分配人工号", required = true)
    private String empNo;

    @ApiModelProperty(value = "分配人部门", required = true)
    private String dept;

    @ApiModelProperty(value = "分配人员类型", required = true)
    private List<String> approveType;

    @ApiModelProperty(value = "门禁点个数", required = true)
    private int accessControlSpotNum;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间", required = true)
    private LocalDateTime createdTime;

    @ApiModelProperty(value = "分配人门禁点列表")
    private List<AccessControlSpotDetailResp> spotList;

}
