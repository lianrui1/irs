package cn.com.cowain.ibms.rest.req.iot;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author feng
 * @title: 绑定设备与组请求对象
 * @projectName ibms
 * @Date 2022/1/12 14:35
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GroupBindingReq implements Serializable {
    /**
     *  @title 
     *  @Description 地点 南通 nt 昆山 ks
     *  @author jf.sui
     *  @Date  
     */
    String chainSite;
    /**
     *  @title 
     *  @Description 通道类型 通道类型:1海康 2旷视 3华为 (设备类型：KSHT   HIK)
     *  @author jf.sui
     *  @Date  
     */
    String channelType;
    /**
     *  @title 
     *  @Description 请求参数
     *  @author jf.sui
     *  @Date
     */
    String jsonArg;
    /**
     *  @title
     *  @Description 处理方法 由安睿配置
     *  @author jf.sui
     *  @Date
     */
    String methodCode ;
    /**
     *  @title
     *  @Description 模块名称 由安睿排至
     *  @author jf.sui
     *  @Date
     */
    String moduleCode;


}
