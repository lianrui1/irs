package cn.com.cowain.ibms.component;

import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.dao.iot.HwProductPropertiesDao;
import cn.com.cowain.ibms.entity.iot.HwProductProperties;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.enumeration.iot.DataSource;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.service.iot.IotDeviceCacheService;
import cn.com.cowain.ibms.service.office.SpaceCacheService;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.ibms.utils.RedisUtil;
import cn.com.cowain.ibms.utils.ThreadPoolUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @Author tql
 * @Description 字典数据加载到缓存中
 * @Date 21-5-10 下午5:24
 * @Version 1.0
 */
@Order(999)
@Component
@Log4j2
public class DeviceDictionaryComponent {

    @Autowired
    DeviceDao deviceDao;
    @Autowired
    HwProductPropertiesDao hwProductPropertiesDao;
    @Autowired
    RedisUtil redisUtil;

    @Autowired
    IotDeviceCacheService iotDeviceCacheService;
    @Autowired
    SpaceCacheService spaceCacheService;

    @Scheduled(cron = "0 0 1 * * ?")
    @PostConstruct
    public void init() {
        ThreadPoolUtils.EXECUTOR_SERVICE_REDIS.submit(() -> {
            /*
             *  判断Redis中是否存在一级节点设备类型的list的key
             *  数据结构的设计为：
             *  key是env:device-iot
             *  value是一个List list中放 DeviceType对象
             */
            String deviceTypeListKey = redisUtil.createDeviceTypeListKey(IConst.DEVICE_MODULE, RedisUtil.Separator.COLON);

            List<IotDevice> iotDevices = deviceDao.findAllByDataSource(DataSource.HW_CLOUD);
            Map<DeviceType, List<IotDevice>> typeIotDeviceMap = iotDevices.stream().collect(Collectors.groupingBy(IotDevice::getDeviceType));

            redisUtil.del(deviceTypeListKey);
            typeIotDeviceMap.forEach((deviceType, iotDevices1) -> redisUtil.lSet(deviceTypeListKey, deviceType, false));
            if (!CollectionUtils.isEmpty(iotDevices)) {
                iotDevices.forEach(this::ifNotExistAdd);
            }
        });
    }

    public void ifNotExistAdd(IotDevice iotDevice) {
        final String deviceTypeListKey = redisUtil.createDeviceTypeListKey(IConst.DEVICE_MODULE, RedisUtil.Separator.COLON);
        List<String> deviceTypes = (List<String>) redisUtil.lGet(deviceTypeListKey, 0, -1);
        if (Boolean.TRUE.equals(redisUtil.hasKey(deviceTypeListKey)) && deviceTypes.stream().noneMatch(deviceType -> StringUtils.equals(deviceType, iotDevice.getDeviceType().toString()))) {
            redisUtil.lSet(deviceTypeListKey, iotDevice.getDeviceType(), false);
        }

        /*
         *  判断Redis中是否存在二级节点设备ID的list的key
         *  数据结构的设计为：
         *  key是env:device-iot:AIR_CONDITIONER
         *  value是一个List list中放 DeviceDataInfo 对象
         *  DeviceDataInfo有 hwDeviceId 和 serviceId的关键字
         */
        DeviceDataInfo deviceDataInfo = new DeviceDataInfo();
        String deviceIdListKey = redisUtil.createDeviceIdListKey(IConst.DEVICE_MODULE, String.valueOf(iotDevice.getDeviceType()), RedisUtil.Separator.COLON);
        Optional<HwProductProperties> hwProductPropertiesOptional = hwProductPropertiesDao.findByHwProductIdAndDeviceType(iotDevice.getHwProductId(), iotDevice.getDeviceType());
        deviceDataInfo.setHwDeviceId(iotDevice.getHwDeviceId());
        hwProductPropertiesOptional.ifPresent(hwProductProperties -> deviceDataInfo.setServiceId(hwProductProperties.getServiceId()));
        if (!Boolean.TRUE.equals(redisUtil.hasKey(deviceIdListKey))) {
            redisUtil.lSet(deviceIdListKey, deviceDataInfo, false);
        } else {
            List<DeviceDataInfo> deviceDataInfoList = redisUtil.lGet(deviceIdListKey, 0, -1, DeviceDataInfo.class);
            if (deviceDataInfoList.stream().noneMatch(dataInfo -> StringUtils.equals(dataInfo.getHwDeviceId(), iotDevice.getHwDeviceId()))) {
                redisUtil.lSet(deviceIdListKey, deviceDataInfo, false);
            }
        }

        /*
         *  判断Redis中是否存在三级节点设备
         *  数据结构的设计为：
         *  key是env:device-iot:AIR_CONDITIONER:${hwDeviceId}
         *  value是一个map 其中存放设备的属性字段
         */
        Map<String, Object> devicePropertiesMap = new ConcurrentHashMap<>();
        String deviceKey = redisUtil.createDeviceRealKey(IConst.DEVICE_MODULE, String.valueOf(iotDevice.getDeviceType()), iotDevice.getHwDeviceId(), RedisUtil.Separator.COLON);
        //判断Redis中是否存在key
        if (!Boolean.TRUE.equals(redisUtil.hasKey(deviceKey))) {
            devicePropertiesMap.put("hwDeviceId", iotDevice.getHwDeviceId());
            redisUtil.hmset(deviceKey, devicePropertiesMap, true);
        }
        //将门磁设备单独存入redis
        iotDeviceCacheService.doorMagneticRedis(iotDevice);

        //将门牌设备存入redis
        iotDeviceCacheService.doorPlateRedis(iotDevice);

        //将device存入redis
        iotDeviceCacheService.deviceRedis(iotDevice);

    }


    @PostConstruct
    public void initSpace() {
        ThreadPoolUtils.EXECUTOR_SERVICE_REDIS.submit(() -> spaceCacheService.initSpace());
    }
}
