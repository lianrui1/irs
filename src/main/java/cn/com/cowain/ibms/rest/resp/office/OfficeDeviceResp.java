package cn.com.cowain.ibms.rest.resp.office;

import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.office.IntelligentOffice;
import cn.com.cowain.ibms.entity.office.IntelligentOfficeDevice;
import cn.com.cowain.ibms.enumeration.iot.UsableStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * @author suijiafeng
 * @email 2332347075@qq.com
 * @date 2021/12/22 14:37
 */
@Data
@ApiModel("办公室查询可用设备响应对象")
public class OfficeDeviceResp {
    public OfficeDeviceResp(){}
    public OfficeDeviceResp(IntelligentOfficeDevice officeDevice){
        this.intelligentOffice=officeDevice.getIntelligentOffice();
        this.device=officeDevice.getDevice();
        this.usableStatus=officeDevice.getUsableStatus();
        this.userHrId=officeDevice.getUserHrId();
    }

    @ApiModelProperty(value = "office_id", required = true)
    private IntelligentOffice intelligentOffice;

    @ApiModelProperty(value = "device_id", required = true)
    private IotDevice device;

    @ApiModelProperty(value = "设备可用状态", required = true)
    private UsableStatus usableStatus;

    @ApiModelProperty(value = "用户工号", required = true)
    private String userHrId;
}
