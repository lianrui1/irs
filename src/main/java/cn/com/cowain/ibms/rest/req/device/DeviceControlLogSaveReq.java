package cn.com.cowain.ibms.rest.req.device;

import cn.com.cowain.ibms.entity.iot.IotDevice;
import lombok.Data;


/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/12/24 11:02
 */
@Data
public class DeviceControlLogSaveReq {

    // 设备
    private IotDevice device;

    // 控制指令
    private String cmd;

    // 用户工号
    private String userHrId;

    // 用户姓名
    private String userName;
}
