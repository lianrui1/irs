package cn.com.cowain.ibms.entity.message;

import lombok.Data;

import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/1/19 16:29
 */
@Data
public class SendMsgRequestVo {
    private String openId;
    private  String template;
    private List<SendParams> params;
}
