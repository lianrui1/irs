package cn.com.cowain.ibms.rest.resp.distinguish;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 通行记录数据返回对象
 *
 * @author Yang.Lee
 * @date 2021/3/15 12:59
 */
@Data
@Builder
@ApiModel("通行记录分页数据返回对象")
public class AccessRecordPageResp {

    @ApiModelProperty(value = "设备名称", required = true, example = "花桥设备A", position = 1)
    private String deviceName;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "识别时间", required = true, example = "2222-02-22 22:22", position = 2)
    private LocalDateTime dataTime;

    @ApiModelProperty(value = "抓拍图片", required = true, example = "http://xx/x/x/xx/picture.jpg", position = 3)
    private String capturePicture;

    @ApiModelProperty(value = "底库图片", required = true, example = "http://xx/x/x/xx/picture.jpg", position = 4)
    private String defaultPicture;

    @ApiModelProperty(value = "员工姓名", required = true, example = "张三", position = 5)
    private String staffName;

    @ApiModelProperty(value = "员工类型", example = "员工", position = 6)
    private String staffType;

    @ApiModelProperty(value = "设备地址", required = true, example = "花桥", position = 7)
    private String deviceAddress;

    @ApiModelProperty(value = "体温。",  example = "36.5/未检测", position = 8)
    private String temperature;

    @ApiModelProperty(value = "通行状态", required = true, example = "通行", position = 9)
    private String accessState;

    @ApiModelProperty(value = "部门", example = "通行", position = 10)
    private String department;
}
