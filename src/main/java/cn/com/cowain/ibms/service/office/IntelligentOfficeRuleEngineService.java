package cn.com.cowain.ibms.service.office;

import cn.com.cowain.ibms.feign.iotc.bean.DefaultCreateRuleChainReq;
import cn.com.cowain.ibms.rest.req.office.RuleEngineControlReq;
import cn.com.cowain.ibms.rest.req.office.RuleEngineDetailReq;
import cn.com.cowain.ibms.rest.resp.office.ControlledDeviceResp;
import cn.com.cowain.ibms.rest.resp.office.RuleEngineResp;
import cn.com.cowain.ibms.rest.resp.office.SensorDeviceResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;

import java.util.List;

/**
 * intelligentRuleEngineService
 *
 * @author: yanzy
 * @date: 2022/2/7 09:36
 */
public interface IntelligentOfficeRuleEngineService {

    //可控设备列表查询
    List<ControlledDeviceResp> findControlledDeviceAll(String officeId,String hrId);

    //传感器设备列表查询
    List<SensorDeviceResp> findSensorDevice(String officeId,String hrId);

    //添加联动规则
    ServiceResult creatRuleEngine(RuleEngineDetailReq req);

    //pc端添加联动规则
    ServiceResult pcCreatRuleEngine(DefaultCreateRuleChainReq req);

    //查询联动规则详情
    ServiceResult findRuleEngineDetail(String id,String hrId);

    //修改联动规则
    ServiceResult updateRileEngine(String id,RuleEngineDetailReq req);

    //删除联动规则
    ServiceResult deleteRuleEngine(String id,String hrId);

    //pc端删除联动规则
    ServiceResult pcDeleteRuleEngine(String id);

    //联动规则查询
    List<RuleEngineResp> findRuleEngine(String id);

    // 规则引擎控制
    ServiceResult ruleEngineControl(RuleEngineControlReq req);

    //判断可控设备是否在其他规则中已经存在
    ServiceResult findControlledDevice(String id,List<String> deviceId ,String hrId);
}
