package cn.com.cowain.ibms.service.impl;

import cn.com.cowain.ibms.service.ExcelService;
import cn.com.cowain.ibms.service.bean.ServiceMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.utils.DateUtils;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.FileUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/7/13 15:01
 */
@Service
@Slf4j
public class ExcelServiceImpl implements ExcelService {

    /**
     * 读取员工组相关excel，默认只读取第一个sheet。单元格格式固定为：
     * 第一行默认为员工组名称。第二行为员工姓名，第三行为员工工号。
     *
     * @param file excel文件
     * @return <p>
     * 如果读取成功，则ServiceResult.getObject()可以得到一个List<String[]>集合，
     * 集合中每一个元素表示excel中的一行。String[]对象表示对应行中的每一个单元格的文本内容
     * ，第一个元素为人员组名称（可以为空），第二个元素为员工姓名，第三个元素为员工工号
     * </p>。
     * <p>
     * 如果读取失败，则根据errorCode进行判断,如果errorCode="EXCEL_CELL_FORMAT_ERROR"(ErrCont.E22)，
     * 则ServiceResult.getObject()返回一个List<String[]>，数据同成功后的返回结果。如果
     * errorCode为其他值，则ServiceResult.getObject()
     * 返回一个字符串，描述错误原因
     * </p>
     * @author Yang.Lee如果errorCode
     * @date 2021/7/13 13:56
     **/
    @Override
    public ServiceResult readStaffGroupExcel(MultipartFile file) {

        // 校验文件格式
        String fileName = file.getOriginalFilename();
        String extName = FileUtil.getExt(fileName);

        if (!"xls".equals(extName) && !"xlsx".equals(extName)) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon
                            (ServiceMessage.FILE_FORMAT_ERROR, "只持支xls或xlsx文件"),
                    ErrConst.E23);
        }

        List<String[]> rowList = new ArrayList<>();
        // 读取文件
        InputStream is = null;
        Workbook workbook = null;
        try {

            is = file.getInputStream();
            workbook = WorkbookFactory.create(is);

            // 默认读取第一个sheet,忽略其他sheet
            Sheet sheet = workbook.getSheetAt(0);
            Row row;
            for (int i = sheet.getFirstRowNum(); i <= sheet.getLastRowNum(); i++) {
                // 跳过第一行标题行
                if (i == sheet.getFirstRowNum()) {
                    continue;
                }
                row = sheet.getRow(i);

                String[] cells = new String[4];

                // 获取员工组名称，给空白的数据默认添加名称
                Cell cell1 = row.getCell(0, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                String groupName =getCellValue(cell1);
                // 当前行无员工组名称，并且上一行有名称，则用上一行员工组名称
                if (StringUtils.isBlank(groupName) && !rowList.isEmpty()) {
                    groupName = rowList.get(rowList.size() - 1)[0].trim();
                }
                cells[0] = groupName;
                cells[1] = getCellValue(row.getCell(1));
                cells[2] = getCellValue(row.getCell(2));

                rowList.add(cells);

            }

        } catch (IOException e) {
            log.error(e.getMessage(), e);
            return ServiceResult.error(ServiceMessage.FILE_READ_ERROR);
        } finally {
            if(is != null){
                try {
                    is.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }

            if(workbook != null){
                try {
                    workbook.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }

        return ServiceResult.ok(rowList);
    }

    /**
     * 获取单元格的内容
     *
     * @param cell 单元格
     * @return 单元格内容
     * @author Yang.Lee
     * @date 2021/7/27 11:08
     **/
    private String getCellValue(Cell cell){

        if(cell == null){
            return "";
        }

        String result = null;
        CellType type = cell.getCellType();

        switch (type){
            case STRING:
                result = cell.getStringCellValue();
                break;
            case ERROR:
                break;
            case BOOLEAN:
                result = String.valueOf(cell.getBooleanCellValue());
                break;
            case FORMULA:
                result = cell.getRichStringCellValue().getString();
                break;
            case NUMERIC:
                if(DateUtil.isCellDateFormatted(cell)){
                    result = DateUtils.formatLocalDateTime(cell.getLocalDateTimeCellValue(), DateUtils.PATTERN_DATETIME);
                } else{
                    result = String.valueOf(cell.getNumericCellValue());
                }
                break;
            default:
                result = "";
        }

        if(result != null){
            result = result.trim();
        }
        return result;
    }

    /**
     * 创建上传人员组失败数据的excel文件
     *
     * @param dataList 数据。集合中每一个元素表示excel中的一行。String[]对象表示对应行中的
     *                 每一个单元格的文本内容，第一个元素为人员组名称（可以为空），第二个元
     *                 素为员工姓名，第三个元素为员工工号，第四个元素为错误信息
     * @return 文件二进制数组。 如果无数据则返回一个长度为0的数组
     * @author Yang.Lee
     * @date 2021/7/14 15:30
     **/
    @Override
    public byte[] createStaffGroupErrorExcel(List<String[]> dataList) {

        if (dataList.isEmpty()) {
            return new byte[0];
        }
        // 创建工作簿
        Workbook workbook = new XSSFWorkbook();
        // 创建sheet
        Sheet sheet = workbook.createSheet();
        int rowLine = 0;
        Row row = sheet.createRow(rowLine);
        Cell cell;

        // 输出title行
        String[] titles = new String[]{"人员组名称", "员工姓名", "员工工号", "失败原因"};
        for (int i = 0; i < titles.length; i++) {
            cell = row.createCell(i);
            cell.setCellValue(titles[i]);
        }
        rowLine++;

        // 输出文本内容
        for (String[] strings : dataList) {
            // 创建row
            row = sheet.createRow(rowLine);
            // 创建cell
            for (int cellNum = 0; cellNum < strings.length; cellNum++) {
                cell = row.createCell(cellNum);
                if (cellNum == 3) {
                    setErrorCell(workbook, cell, strings[3]);
                } else {
                    cell.setCellValue(strings[cellNum]);
                }
            }

            rowLine++;
        }

        // 将数据输出到outputStream中
        byte[] bytes = new byte[0];
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            workbook.write(outputStream);
            bytes = outputStream.toByteArray();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } finally {
            try {
                workbook.close();
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }

        return bytes;
    }

    @Override
    public ServiceResult readAddAccessRulesUserExcel(MultipartFile file) {
        // 校验文件格式
        String fileName = file.getOriginalFilename();
        String extName = FileUtil.getExt(fileName);

        if (!"xls".equals(extName) && !"xlsx".equals(extName)) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon
                            (ServiceMessage.FILE_FORMAT_ERROR, "只持支xls或xlsx文件"),
                    ErrConst.E23);
        }
        List<String[]> rowList = new ArrayList<>();
        // 读取文件
        InputStream is = null;
        Workbook workbook = null;

        try {

            is = file.getInputStream();
            workbook = WorkbookFactory.create(is);

            // 默认读取第一个sheet,忽略其他sheet
            Sheet sheet = workbook.getSheetAt(0);
            Row row;

            for (int i = sheet.getFirstRowNum(); i <= sheet.getLastRowNum(); i++) {
                // 跳过第一行标题行
                if (i == sheet.getFirstRowNum()) {
                    continue;
                }
                row = sheet.getRow(i);

                String[] cells = new String[6];

                if (StringUtils.isNotBlank(getCellValue(row.getCell(1)))) {
                    cells[0] = getCellValue(row.getCell(0));
                    cells[1] = getCellValue(row.getCell(1));
                    cells[2] = getCellValue(row.getCell(2));
                    cells[3] = getCellValue(row.getCell(3));
                    cells[4] = getCellValue(row.getCell(4));
                }

                rowList.add(cells);

            }

        } catch (IOException e) {
            log.error(e.getMessage(), e);
            return ServiceResult.error(ServiceMessage.FILE_READ_ERROR);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }

            if (workbook != null) {
                try {
                    workbook.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }

        }
        return ServiceResult.ok(rowList);
    }

    @Override
    public ServiceResult readAccessRulesExcel(MultipartFile file) {
        // 校验文件格式
        String fileName = file.getOriginalFilename();
        String extName = FileUtil.getExt(fileName);

        if (!"xls".equals(extName) && !"xlsx".equals(extName)) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon
                            (ServiceMessage.FILE_FORMAT_ERROR, "只持支xls或xlsx文件"),
                    ErrConst.E23);
        }

        List<String[]> rowList = new ArrayList<>();
        // 读取文件
        InputStream is = null;
        Workbook workbook = null;
        try {

            is = file.getInputStream();
            workbook = WorkbookFactory.create(is);


            // 默认读取第一个sheet,忽略其他sheet
            Sheet sheet = workbook.getSheetAt(0);
            Row row;
            // 获取开始时间结束时间，给空白的数据默认添加时间
            Cell cell1 = sheet.getRow(1).getCell(2, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
            String startTime = getCellValue(cell1);
            Cell cell2 = sheet.getRow(1).getCell(3, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
            String endTime = getCellValue(cell2);
            for (int i = sheet.getFirstRowNum(); i <= sheet.getLastRowNum(); i++) {
                // 跳过第一行标题行
                if (i == sheet.getFirstRowNum()) {
                    continue;
                }
                row = sheet.getRow(i);

                String[] cells = new String[5];

                // 当前行无员工组名称，并且上一行有名称，则用上一行员工组名称
                /*if (StringUtils.isBlank(startTime) && StringUtils.isBlank(endTime) && !rowList.isEmpty()) {
                    startTime = rowList.get(rowList.size() - 1)[2].trim();
                    endTime = rowList.get(rowList.size() - 1)[3].trim();
                }*/
                if (StringUtils.isNotBlank(getCellValue(row.getCell(1)))) {
                    cells[0] = getCellValue(row.getCell(0));
                    cells[1] = getCellValue(row.getCell(1));
                    cells[2] = startTime;
                    cells[3] = endTime;
                }

                rowList.add(cells);

            }

        } catch (IOException e) {
            log.error(e.getMessage(), e);
            return ServiceResult.error(ServiceMessage.FILE_READ_ERROR);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }

            if (workbook != null) {
                try {
                    workbook.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }

        }
        return ServiceResult.ok(rowList);
    }

    @Override
    public ServiceResult readDeleteAccessRulesExcel(MultipartFile file) {
        // 校验文件格式
        String fileName = file.getOriginalFilename();
        String extName = FileUtil.getExt(fileName);

        if (!"xls".equals(extName) && !"xlsx".equals(extName)) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon
                            (ServiceMessage.FILE_FORMAT_ERROR, "只持支xls或xlsx文件"),
                    ErrConst.E23);
        }

        List<String[]> rowList = new ArrayList<>();
        // 读取文件
        InputStream is = null;
        Workbook workbook = null;
        try {

            is = file.getInputStream();
            workbook = WorkbookFactory.create(is);


            // 默认读取第一个sheet,忽略其他sheet
            Sheet sheet = workbook.getSheetAt(0);
            Row row;
            for (int i = sheet.getFirstRowNum(); i <= sheet.getLastRowNum(); i++) {
                // 跳过第一行标题行
                if (i == sheet.getFirstRowNum()) {
                    continue;
                }
                row = sheet.getRow(i);

                String[] cells = new String[3];

                // 获取员工组名称，给空白的数据默认添加名称
                /*Cell cell1 = row.getCell(0, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                String groupName =getCellValue(cell1);*/
                // 当前行无员工组名称，并且上一行有名称，则用上一行员工组名称
                /*if (StringUtils.isBlank(groupName) && !rowList.isEmpty()) {
                    groupName = rowList.get(rowList.size() - 1)[0];
                }*/
                if (StringUtils.isNotBlank(getCellValue(row.getCell(1)))) {
                    cells[0] = getCellValue(row.getCell(0));
                    cells[1] = getCellValue(row.getCell(1));
                }

                rowList.add(cells);

            }

        } catch (IOException e) {
            log.error(e.getMessage(), e);
            return ServiceResult.error(ServiceMessage.FILE_READ_ERROR);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }

            if (workbook != null) {
                try {
                    workbook.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }

        }
        return ServiceResult.ok(rowList);
    }

    @Override
    public ServiceResult readDeleteAccessRulesUserExcel(MultipartFile file) {
        // 校验文件格式
        String fileName = file.getOriginalFilename();
        String extName = FileUtil.getExt(fileName);

        if (!"xls".equals(extName) && !"xlsx".equals(extName)) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon
                            (ServiceMessage.FILE_FORMAT_ERROR, "只持支xls或xlsx文件"),
                    ErrConst.E23);
        }
        List<String[]> rowList = new ArrayList<>();
        // 读取文件
        InputStream is = null;
        Workbook workbook = null;
        try {
            is = file.getInputStream();
            workbook = WorkbookFactory.create(is);

            // 默认读取第一个sheet,忽略其他sheet
            Sheet sheet = workbook.getSheetAt(0);
            Row row;
            for (int i = sheet.getFirstRowNum(); i <= sheet.getLastRowNum(); i++){
                // 跳过第一行标题行
                if (i == sheet.getFirstRowNum()) {
                    continue;
                }
                row = sheet.getRow(i);

                String[] cells = new String[3];
                if (StringUtils.isNotBlank(getCellValue(row.getCell(1)))){
                    cells[0] = getCellValue(row.getCell(0));
                    cells[1] = getCellValue(row.getCell(1));
                    cells[2] = getCellValue(row.getCell(2));
                }
                rowList.add(cells);
            }

        }catch (IOException e){
            log.error(e.getMessage(), e);
            return ServiceResult.error(ServiceMessage.FILE_READ_ERROR);
        }finally {
            if (is != null){
                try {
                    is.close();
                }catch (IOException e){
                    log.error(e.getMessage(),e);
                }
            }

            if (workbook != null) {
                try {
                    workbook.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
        return ServiceResult.ok(rowList);
    }

    @Override
    public byte[] createUploadAccessRulesErrorExcel(List<String[]> dataList) {
        if (dataList.isEmpty()) {
            return new byte[0];
        }
        // 创建工作簿
        Workbook workbook = new XSSFWorkbook();
        // 创建sheet
        Sheet sheet = workbook.createSheet();
        int rowLine = 0;
        Row row = sheet.createRow(rowLine);
        Cell cell;

        // 输出title行
        String[] titles = new String[]{"姓名", "工号", "通行生效时间", "通行结束时间", "失败原因"};
        for (int i = 0; i < titles.length; i++) {
            cell = row.createCell(i);
            cell.setCellValue(titles[i]);
        }
        rowLine++;

        // 输出文本内容
        for (String[] strings : dataList) {
            // 创建row
            row = sheet.createRow(rowLine);
            // 创建cell
            for (int cellNum = 0; cellNum < strings.length; cellNum++) {
                cell = row.createCell(cellNum);
                if (cellNum == 4) {
                    setErrorCell(workbook, cell, strings[4]);
                } else {
                    cell.setCellValue(strings[cellNum]);
                }
            }

            rowLine++;
        }

        // 将数据输出到outputStream中
        byte[] bytes = new byte[0];
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            workbook.write(outputStream);
            bytes = outputStream.toByteArray();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } finally {
            try {
                workbook.close();
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }

        return bytes;
    }

    @Override
    public byte[] createUploadAccessRulesUserErrorExcel(List<String[]> dataList) {
        if (dataList.isEmpty()) {
            return new byte[0];
        }
        // 创建工作簿
        Workbook workbook = new XSSFWorkbook();
        // 创建sheet
        Sheet sheet = workbook.createSheet();
        int rowLine = 0;
        Row row = sheet.createRow(rowLine);
        Cell cell;

        // 输出title行
        String[] titles = new String[]{"姓名", "工号", "人员类型","通行生效时间", "通行结束时间", "失败原因"};
        for (int i = 0; i < titles.length; i++) {
            cell = row.createCell(i);
            cell.setCellValue(titles[i]);
        }
        rowLine++;

        // 输出文本内容
        for (String[] strings : dataList) {
            // 创建row
            row = sheet.createRow(rowLine);
            // 创建cell
            for (int cellNum = 0; cellNum < strings.length; cellNum++) {
                cell = row.createCell(cellNum);
                if (cellNum == 5) {
                    setErrorCell(workbook, cell, strings[5]);
                } else {
                    cell.setCellValue(strings[cellNum]);
                }
            }

            rowLine++;
        }
        // 将数据输出到outputStream中
        byte[] bytes = new byte[0];
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            workbook.write(outputStream);
            bytes = outputStream.toByteArray();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } finally {
            try {
                workbook.close();
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }

        return bytes;
    }

    @Override
    public byte[] createDelAccessRulesErrorExcel(List<String[]> dataList) {
        if (dataList.isEmpty()) {
            return new byte[0];
        }
        // 创建工作簿
        Workbook workbook = new XSSFWorkbook();
        // 创建sheet
        Sheet sheet = workbook.createSheet();
        int rowLine = 0;
        Row row = sheet.createRow(rowLine);
        Cell cell;

        // 输出title行
        String[] titles = new String[]{"姓名", "工号", "失败原因"};
        for (int i = 0; i < titles.length; i++) {
            cell = row.createCell(i);
            cell.setCellValue(titles[i]);
        }
        rowLine++;

        // 输出文本内容
        for (String[] strings : dataList) {
            // 创建row
            row = sheet.createRow(rowLine);
            // 创建cell
            for (int cellNum = 0; cellNum < strings.length; cellNum++) {
                cell = row.createCell(cellNum);
                if (cellNum == 2) {
                    setErrorCell(workbook, cell, strings[2]);
                } else {
                    cell.setCellValue(strings[cellNum]);
                }
            }

            rowLine++;
        }

        // 将数据输出到outputStream中
        byte[] bytes = new byte[0];
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            workbook.write(outputStream);
            bytes = outputStream.toByteArray();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } finally {
            try {
                workbook.close();
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }

        return bytes;
    }

    @Override
    public byte[] downloadHkFaceRecord(List<String[]> recordList) {
        // 创建工作簿
        Workbook workbook = new XSSFWorkbook();
        // 创建sheet
        Sheet sheet = workbook.createSheet();
        int rowLine = 0;
        Row row = sheet.createRow(rowLine);
        Cell cell;

        // 输出title行
        String[] titles = new String[]{"序号","人员姓名", "工号", "所属部门", "抓拍时间", "状态"};
        for (int i = 0; i < titles.length; i++) {
            cell = row.createCell(i);
            cell.setCellValue(titles[i]);
        }
        rowLine++;

        // 输出文本内容
        for (String[] strings : recordList) {
            // 创建row
            row = sheet.createRow(rowLine);
            // 创建cell
            for (int cellNum = 0; cellNum < strings.length; cellNum++) {
                cell = row.createCell(cellNum);
                if (cellNum == 5) {
                    setRecordStatus(workbook, cell, strings[5]);
                } else {
                    cell.setCellValue(strings[cellNum]);
                }
            }

            rowLine++;
        }

        // 将数据输出到outputStream中
        byte[] bytes = new byte[0];
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            workbook.write(outputStream);
            bytes = outputStream.toByteArray();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } finally {
            try {
                workbook.close();
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }

        return bytes;
    }

    private void setRecordStatus(Workbook workbook, Cell cell, String cellText) {
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setColor(IndexedColors.GREEN.getIndex());
        style.setFont(font);
        cell.setCellStyle(style);
        cell.setCellValue(cellText);
    }

    /**
     * 设置错误单元格
     *
     * @param workbook 工作簿对象
     * @param cell     单元格
     * @param cellText 单元格内容
     * @author Yang.Lee
     * @date 2021/7/13 15:37
     **/
    private void setErrorCell(Workbook workbook, Cell cell, String cellText) {
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setColor(IndexedColors.RED.getIndex());
        style.setFont(font);
        cell.setCellStyle(style);
        cell.setCellValue(cellText);
    }
}
