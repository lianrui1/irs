package cn.com.cowain.ibms.rest.resp.office;

import cn.com.cowain.ibms.rest.resp.device.AirConditionerResp;
import cn.com.cowain.ibms.rest.resp.device.TbDataResp;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author suijiafeng
 * @email 2332347075@qq.com
 * @date 2021/12/22 14:37
 */
@Data
@ApiModel("办公室设备列表详情返回数据")
public class OfficeEquipmentResp {
    @ApiModelProperty(value = "当前办公室", required = true)
    OccupyOfficeResp nowOffice;
    @ApiModelProperty(value = "办公司设备列表", required = true)
    List<OfficeDeviceResp> deviceList=new ArrayList<>();

    @ApiModelProperty(value = "空调列表", required = true)
    List<AirConditionerResp> airList=new ArrayList<>();

    @ApiModelProperty(value = "温度，阳光，噪音，湿度检测", required = true)
    List<TbDataResp>   tbList=new ArrayList<>();

    @ApiModelProperty(value = "窗帘列表", required = true)
    List<CurtainDetailResp> cutainList=new ArrayList<>();


    @ApiModelProperty(value = "四路开关", required = true)
    List<SwitchDetailResp> switchDetailResps=new ArrayList<>();



    @ApiModelProperty(value = "四路开关", required = true)
    List<DoorlResp> doorlResp=new ArrayList<>();

}