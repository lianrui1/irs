package cn.com.cowain.ibms.rest.req.iot;

import cn.com.cowain.ibms.rest.req.IdAndSeqBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 门牌数据排序请求
 * @author Yang.Lee
 * @date 2020/12/1 14:23
 */
@Data
@ApiModel("门牌数据排序请求")
public class DoorPlateSortReq implements Serializable {

    @NotEmpty
    @ApiModelProperty(value = "排序数据", required = true)
    private List<IdAndSeqBean> data;

    @NotNull
    @ApiModelProperty(value = "排序数据当前页码", required = true, position = 1)
    private int page;
    @NotNull
    @ApiModelProperty(value = "排序数据当前页长", required = true, position = 2)
    private int size;
}
