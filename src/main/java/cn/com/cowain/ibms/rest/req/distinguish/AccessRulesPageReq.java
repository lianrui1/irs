package cn.com.cowain.ibms.rest.req.distinguish;

import cn.com.cowain.ibms.enumeration.iot.DataSource;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/14 13:46
 */
@Data
@ApiModel("通行权限分页数据请求对象")
public class AccessRulesPageReq extends PageReq {

    @ApiModelProperty(value = "地区（项目）ID")
    private String projectId;

    @ApiModelProperty(value = "楼宇（空间）ID")
    private String buildingsId;

    @ApiModelProperty(value = "楼层（空间）ID")
    private String floorId;

    @ApiModelProperty(value = "（空间）ID")
    private String spaceId;

    @ApiModelProperty(value = "设备类型")
    private DeviceType deviceType;

    @ApiModelProperty(value = "设备来源")
    private DataSource deviceSource;

    @ApiModelProperty(value = "sn码/设备名称")
    private String deviceName;

    @ApiModelProperty(value = "姓名/工号")
    private String staff;
}
