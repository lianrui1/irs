package cn.com.cowain.ibms.service.iot.lift_column.impl;

import cn.com.cowain.ibms.dao.iot.lift_column.LiftColumnGroupDao;
import cn.com.cowain.ibms.dao.iot.lift_column.LiftColumnGroupRelationDao;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.iot.LiftColumnGroup;
import cn.com.cowain.ibms.entity.iot.LiftColumnGroupRelation;
import cn.com.cowain.ibms.rest.resp.BaseListResp;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.DoorMagneticHWParasReq;
import cn.com.cowain.ibms.service.bean.DoorMagneticHWReq;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.device.HwProductsService;
import cn.com.cowain.ibms.service.iot.lift_column.LiftColumnService;
import cn.com.cowain.ibms.utils.ErrConst;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Yang.Lee
 * @date 2022/6/24 13:07
 */
@Slf4j
@Service
public class LiftColumnServiceImpl implements LiftColumnService {

    @Value("${cowain.door-magnetic.huawei.open.url}")
    private String openUrl;

    @Resource
    private LiftColumnGroupDao liftColumnGroupDao;

    @Resource
    private LiftColumnGroupRelationDao liftColumnGroupRelationDao;

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserService;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private HwProductsService hwProductsService;

    /**
     * 控制设备
     *
     * @param userHrId 用户工号
     * @param parkId   停车场ID
     * @param cmd      控制指令。up: 升柱， down: 降柱
     * @return 控制结果
     * @author Yang.Lee
     * @date 2022/6/24 10:58
     **/
    @Override
    public ServiceResult control(String userHrId, String parkId, String cmd) {

        // 根据parkID查出所有的设备列表
        List<LiftColumnGroupRelation> liftColumnGroupRelationList = liftColumnGroupRelationDao.findByColumnGroupId(parkId);

        List<IotDevice> deviceList = liftColumnGroupRelationList.stream().map(obj -> obj.getDevice()).collect(Collectors.toList());

        if(CollectionUtils.isEmpty(deviceList)){
            return ServiceResult.error("该停车场内无升降柱设备", ErrConst.E12);
        }

        // 新工厂的升降柱，  升降分别是通过控制不通设备来实现的，升控制2，降控制1 ，3无用。  指令都是发送open
        Optional<IotDevice> deviceOptional;
        if(cmd.toLowerCase(Locale.ROOT).equals("up")){
            deviceOptional = liftColumnGroupRelationList.stream().filter(obj -> obj.getControlType() == 1).map(LiftColumnGroupRelation::getDevice).findFirst();
        } else {
            deviceOptional = liftColumnGroupRelationList.stream().filter(obj -> obj.getControlType() == 2).map(LiftColumnGroupRelation::getDevice).findFirst();

        }
        if(deviceOptional.isEmpty()){
            return ServiceResult.error("未找到主控设备");
        }
        control(deviceOptional.get(), cmd, userHrId);


        return ServiceResult.ok();
    }

    /**
     * 获取停车场列表
     *
     * @return 停车场列表
     * @author Yang.Lee
     * @date 2022/6/24 13:11
     **/
    @Override
    public List<BaseListResp> getParkList() {

        // 查询停车场数据，根据名称排序
        List<LiftColumnGroup> groupList = liftColumnGroupDao.findAll(Sort.by("name"));

        return groupList.stream().map(obj -> new BaseListResp(obj.getId(), obj.getName())).collect(Collectors.toList());
    }

    /**
     * 控制升降柱设备
     *
     * @param device   设备
     * @param cmd      控制指令。up: 升柱， down: 降柱
     * @param userHrId 控制人工号
     * @return 控制结果
     * @author Yang.Lee
     * @date 2022/6/27 10:45
     **/
    private ServiceResult control(IotDevice device, String cmd, String userHrId) {

        log.debug("开始控制升降柱，设备华为云ID {}, 指令 {}, 控制人工号 {}", device.getHwDeviceId(), cmd, userHrId);

        // 现在升降柱和门磁在华为云上是同样的模型，因此使用控制门磁的方式来控制升降柱
        DoorMagneticHWReq doorMagneticHWReq = new DoorMagneticHWReq();
        doorMagneticHWReq.setDeviceId(device.getId());
        doorMagneticHWReq.setDevice_id(device.getHwDeviceId());
        DoorMagneticHWParasReq doorMagneticHWParasReq = new DoorMagneticHWParasReq();
        // 发送的指令
        String action = "open";

        doorMagneticHWParasReq.setAction(action);
        doorMagneticHWReq.setParas(doorMagneticHWParasReq);

        SysUser sysUser = sysUserService.getUserByEmpNo(userHrId);
        // 保存控制日志
        hwProductsService.saveDeviceControlLog(doorMagneticHWReq, userHrId, sysUser.getNameZh());

        // json参数
        String requestParam = JSON.toJSONString(doorMagneticHWReq);
        log.debug("发送控制升降柱请求json参数 ： " + requestParam);

        // 发送Http请求
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity<>(requestParam, headers);
        ResponseEntity<String> responseEntity = null;
        try {
            responseEntity = restTemplate.postForEntity(openUrl, requestEntity, String.class);
        } catch (Exception e) {
            log.error("控制升降柱失败！！！", e);
            return ServiceResult.error("控制升降柱失败", ErrConst.E500);
        }

        // 解析请求结果
        String responseBody = responseEntity.getBody();
        log.debug("控制升降柱结果 response : " + responseBody);
        JSONObject jsonResult = JSON.parseObject(responseBody);
        Integer status = jsonResult.getInteger("status");
        if (status != 1) {
            return ServiceResult.error("设备控制失败");
        }
        String data = jsonResult.getString("data");
        JSONObject jsonData = JSON.parseObject(data);
        // 根据返回的数据中是否包含error_code判断请求成功与否

        if (jsonData.containsKey("error_code")) {
            log.error("华为云开门失败！！！" + responseBody);
            return ServiceResult.error(responseBody, ErrConst.E500);
        }

        return ServiceResult.ok();
    }
}
