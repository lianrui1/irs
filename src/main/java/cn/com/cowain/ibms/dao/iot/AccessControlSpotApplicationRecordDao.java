package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.AccessControlSpotApplicationRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * @author wei.cheng
 * @date 2022/03/09 13:07
 */
@Repository
public interface AccessControlSpotApplicationRecordDao extends JpaRepository<AccessControlSpotApplicationRecord, String>, JpaSpecificationExecutor<AccessControlSpotApplicationRecord> {
    /**
     * 查询申请人对于门禁最近一次申请
     *
     * @param applicantHrId
     * @param spotId
     * @return
     */
    AccessControlSpotApplicationRecord findFirstByApplicantHrIdAndSpotIdOrderByCreatedTimeDesc(String applicantHrId, String spotId);

    List<AccessControlSpotApplicationRecord> findBySpotId(String id);

    /**
     * 查询申请人对于门禁的申请
     *
     * @param applicationHrId
     * @param spotIds
     * @return
     */
    List<AccessControlSpotApplicationRecord> findAllByApplicantHrIdAndSpotIdIn(String applicationHrId, Collection<String> spotIds);
}
