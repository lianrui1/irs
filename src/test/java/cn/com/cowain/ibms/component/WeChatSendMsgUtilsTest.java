package cn.com.cowain.ibms.component;

import cn.com.cowain.ibms.service.bean.WeChatMsg;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author: cuiEnming
 * @title: WeChatSendMsgUtilsTest
 * @date 2020/11/25  11:24
 * @since s2
 */
@SpringBootTest
class WeChatSendMsgUtilsTest {
    @Autowired
    private WeChatSendMsgUtils weChatSendMsgUtils;

    @Test
    void sendMsg3Lines() {
        /**
         * 只支持 3 行的微信模板
         *
         * @param templateId 模板id
         * @param url        跳转url
         * @param toUserWxId 目标wxId
         * @param weChatMsg  信息数组/集合
         */
        WeChatMsg weChatMsg= new WeChatMsg("测试","看消息","有没有","好不好使","","");
        String s = weChatSendMsgUtils.sendMsg3Lines("pdsDExb8FcjGVRF2KmMRLO2bVWg0u-YlLdCd2A2X9FE", "", "oLh_dv-FF3EEhroR6wpzUKw75ZXY", weChatMsg);
        System.out.println(s);
    }
}