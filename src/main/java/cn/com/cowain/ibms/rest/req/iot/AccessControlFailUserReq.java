package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("失败人员同步")
public class AccessControlFailUserReq {

    @ApiModelProperty(value = "工号")
    private String empNo;

    @ApiModelProperty(value = "门禁ID")
    private String spotId;

}
