package cn.com.cowain.ibms.rest.resp.space;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 父空间列表对象
 * @author Yang.Lee
 * @date 2020/12/22 15:39
 */
@Data
@ApiModel("父空间列表对象")
public class SpaceParentResp {

    @ApiModelProperty(value = "主键ID", required = true)
    private String id;

    @ApiModelProperty(value = "名称", required = true, example = "A楼/1层/303室")
    private String name;
}
