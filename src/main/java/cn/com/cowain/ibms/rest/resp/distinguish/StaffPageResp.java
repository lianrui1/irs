package cn.com.cowain.ibms.rest.resp.distinguish;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/10/18 18:15
 */
@Data
@ApiModel("人员列表分页响应对象")
public class StaffPageResp {

    @ApiModelProperty(value = "id", required = true)
    private String id;

    @ApiModelProperty(value = "人员姓名", required = true)
    private String name;

    @ApiModelProperty(value = "人员照片", required = true)
    private String imgUrl = "";

    @ApiModelProperty(value = "类型.1-访客。0-员工", required = true)
    private Integer isStaff;

    @ApiModelProperty(value = "工号", required = true)
    private String empNo;

    @ApiModelProperty(value = "部门", required = true)
    private String dept;

    @ApiModelProperty(value = "手机号码", required = true)
    private String mobile;
}
