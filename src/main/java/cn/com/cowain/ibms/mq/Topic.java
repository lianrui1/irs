package cn.com.cowain.ibms.mq;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Yang.Lee
 * @date 2021/6/17 14:34
 */
@Component
public class Topic {

    /**
     * 设备控制topic
     **/
    public static final String DEVICE_CONTROL = "device_control";
    // 失败设备控制
    public static final String ERROR_DEVICE_CONTROL = "error_device_control";
    // 海康服务通行记录
    public static final String HIK_SERVER_ACCESS_RECORD = "hik-server-access-record";
    /**
     * 发送信息Tag
     */
    public static final String SEND_MSG = "send-msg";
    /**
     * 开放能力
     **/
    public static final String OPEN_ABILITY = "ibms-open-ability";
    /**
     * iotc 人脸处理topic
     **/
    public static final String BIZ_TO_IOTC = "biz-to-iotc";
    /**
     * iotc 人脸处理消息返回topic
     **/
    public static final String BIZ_TO_IOTC_RETURN = "biz-to-iotc-message-back";

    /**
     * 人员绑定返回topic
     */
    public static final String USER_BIND_RETURN = "user-bind-message-back";

    public static final String USER_UNBIND_RETURN = "user-unbind-message-back";
    /**
     * 批量删除人员解绑返回topic
     *
     * @author: yanzy
     * @data: 2022/5/31 15:05:20
     */
    public static final String USER_BATCH_UNBIND_RETURN = "user-batch-unbind-return";

    /**
     * 旷视通行记录
     **/
    public static final String MEGVII_ACCESS_RECORD = "conn-to-ehr-record";

    // 设备操作日志
    public static final String DEVICE_CONTROL_LOG = "ibms-to-tsdb-device-control";
    /**
     * 访客结果回调
     **/
    public static final String VISITOR_RETURN = "ibms-oa-face-result";
    /**
     * 门禁点授权申请通过请求iotc更新用户回调
     */
    public static final String ACCESS_APPLICATION_HANDLING_TO_IOTC_MESSAGE_BACK = "access-application-handling-to-iotc-message-back";
    public static final String ACCESS_APPLICATION_HANDLING_UPDATE_USER_TO_IOTC_MESSAGE_BACK = "access-application-handling-update-user-to-iotc-message-back";

    /**
     * 门禁点授权申请通过请求iotc绑定用户回调
     */
    public static final String ACCESS_APPLICATION_HANDLING_BIND_USER_TO_IOTC_MESSAGE_BACK = "access-application-handling-bind-user-to-iotc-message-back";

    /**
     * iotc 处理门禁点更新人脸照片返回topic
     */
    public static final String FACE_TO_IOTC_RETURN = "face-to-iotc-message-back";
    /**
     * 设置南通排班出口门禁点通行时间段为空请求iotc权限下发回调
     */
    public static final String SET_NT_SCHEDULE_OUT_ACCESS_TIME_TO_EMPTY_MESSAGE_BACK = "set-nt-schedule-out-access-time-to-empty-message-back";
    /**
     * iotc 处理ksht设备 访客通行时间
     *
     * @author: yanzy
     */
    public static final String ACCESS_CONTROL_KSHT_VISITOR_RETURN = "access-control-ksht-visitor-message-back";

    /**
     * iotc删除设备同步ibms设备删除
     */
    public static final String IOTC_DELETE_DEVICE = "iotc-to-ibms-del";
    /**
     * 旷世鸿图警戒抓拍事件通知
     */
    public static final String IOTC_ALERT_CAPTUREOR = "ksht-alert-captureor-record";

    private static String env;

    /**
     *
     * iotc修改设备同步ibms 存入缓存
     */
    public static final String IOTC_TO_IBMS_UPD_DEVICE = "iotc-to-ibms-upd-device";

    private Topic() {
    }

    private static void setEnvValue(String env) {
        Topic.env = env;
    }

    /**
     * 获取topic(包含环境信息)
     *
     * @param topic topic
     * @return 添加环境后的topic
     * @author Yang.Lee
     * @date 2022/1/27 17:56
     **/
    public static String get(String topic) {

        return env + "-" + topic;
    }

    @Value("${rocketmq.env}")
    public void setEnv(String env) {
        Topic.setEnvValue(env);
    }
}
