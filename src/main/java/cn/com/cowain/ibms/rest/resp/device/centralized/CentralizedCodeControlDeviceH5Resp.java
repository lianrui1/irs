package cn.com.cowain.ibms.rest.resp.device.centralized;

import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author wei.cheng
 * @date 2022/09/20 16:55
 */
@Data
@ApiModel(value = "h5端集控码控制设备")
public class CentralizedCodeControlDeviceH5Resp {

    @ApiModelProperty(value = "设备ID")
    private String id;

    @ApiModelProperty(value = "设备类型")
    private DeviceType deviceType;

    @ApiModelProperty(value = "设备名称")
    private String name;

    @ApiModelProperty(value = "设备状态")
    private DeviceStatus status;

    @ApiModelProperty(value = "设备状态描述")
    private String statusDesc;

    @ApiModelProperty(value = "是否开启")
    private Boolean isOn;
}
