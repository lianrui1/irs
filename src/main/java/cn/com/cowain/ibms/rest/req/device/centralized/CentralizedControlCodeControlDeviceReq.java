package cn.com.cowain.ibms.rest.req.device.centralized;

import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 集控码控制设备请求参数
 *
 * @author: yanzy
 * @date: 2022/9/16 15:16
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "查询集控码控制设备请求参数")
public class CentralizedControlCodeControlDeviceReq extends PageReq {

    @NotBlank(message = "id不能为空")
    @ApiModelProperty(value = "集控码id", required = true)
    private String id;

    @NotNull(message = "设备类型不能为空")
    @ApiModelProperty(value = "设备类型", required = true)
    private DeviceType deviceType;
}
