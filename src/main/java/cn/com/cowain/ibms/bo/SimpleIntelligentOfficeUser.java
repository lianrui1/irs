package cn.com.cowain.ibms.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author wei.cheng
 * @date 2022/10/17 10:31
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class SimpleIntelligentOfficeUser implements Serializable {
    /**
     * 邀请人工号
     */
    private String invitedUserHrId;
}
