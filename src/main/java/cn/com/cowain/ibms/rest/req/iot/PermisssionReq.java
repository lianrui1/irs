package cn.com.cowain.ibms.rest.req.iot;

import lombok.Data;

/**
 * @author feng
 * @title: PermisssionReq
 * @projectName ibms
 * @Date 2022/1/12 15:22
 */
@Data
public class PermisssionReq {
    String [] deviceUuidList;
    String [] groupUuidList;
    String timePlanId;
}
