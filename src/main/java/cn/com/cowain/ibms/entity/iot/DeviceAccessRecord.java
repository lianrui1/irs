package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.enumeration.iot.DeviceAccessRecordType;
import cn.com.cowain.ibms.enumeration.iot.PersonType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author Yang.Lee
 * @date 2021/12/2 19:05
 */
@Data
@Entity
@Table(name = "iot_device_access_record")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_device_access_record", comment = "设备通行记录表")
public class DeviceAccessRecord extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "device_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK。设备表主键id'")
    private IotDevice iotDevice;

    @Enumerated(EnumType.STRING)
    @Column(name = "record_type", nullable = false, columnDefinition = "varchar(20) COMMENT '通行记录类型'")
    private DeviceAccessRecordType recordType;

    /**
     * 数据时间
     */
    @Column(name = "dataTime", nullable = false, columnDefinition = "datetime COMMENT '数据时间'")
    private LocalDateTime dataTime;

    /**
     * 抓怕照片
     */
    @Column(name = "capture_pic", columnDefinition = "varchar(256) COMMENT '抓拍照片'")
    private String capturePicture;

    /**
     * 底库照片
     */
    @Column(name = "default_pic", columnDefinition = "varchar(256) COMMENT '底库照片'")
    private String defaultPicture;

    /**
     * 通行人员工号
     */
    @Column(name = "empNo", columnDefinition = "varchar(36) COMMENT '员工工号'")
    private String empNo;

    /**
     * 通行人员姓名
     */
    @Column(name = "nameZh", columnDefinition = "varchar(36) COMMENT '员工姓名'")
    private String nameZh;

    @Column(name = "department", columnDefinition = "varchar(36) COMMENT '员工所属部门'")
    private String department;

    /**
     * 员工类型
     **/
    @Enumerated(EnumType.STRING)
    @Column(name = "staff_type", nullable = false, columnDefinition = "varchar(36) COMMENT '员工类型:员工；陌生人；访客'")
    private PersonType staffType;

    /**
     * 通行状态
     **/
    @Column(name = "access_status", columnDefinition = "varchar(10) COMMENT '通行状态'")
    private String accessStatus;

    /**
     * 体温
     */
    @Column(name = "temperature", columnDefinition = "varchar(10) COMMENT '体温'")
    private String temperature;

}
