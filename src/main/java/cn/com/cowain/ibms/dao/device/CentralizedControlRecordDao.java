package cn.com.cowain.ibms.dao.device;


import cn.com.cowain.ibms.entity.iot.centralizedControl.CentralizedControlRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CentralizedControlRecordDao extends JpaRepository<CentralizedControlRecord, String>, JpaSpecificationExecutor<CentralizedControlRecord> {
    Optional<CentralizedControlRecord> findByTraceId(String traceId);

    Optional<CentralizedControlRecord> findCentralizedControlRecordByTraceId(String traceId);
}
