package cn.com.cowain.ibms.service.hk.impl;

import cn.com.cowain.ibms.feign.hk_server_connector.ks.HqAccessRecordService;
import cn.com.cowain.ibms.feign.hk_server_connector.nt.NtAccessRecordService;
import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.service.hk.HealthCheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author tql
 * @Description 心跳检测实体类
 * @Date 21-5-21 下午3:45
 * @Version 1.0
 */
@Service
public class HealthCheckServiceImpl implements HealthCheckService {

    @Autowired
    private HqAccessRecordService hqAccessRecordService;

    @Autowired
    private NtAccessRecordService ntAccessRecordService;

    /**
     * 健康检测
     *
     * @return
     */
    @Override
    public boolean healthCheckHq() {
        JsonResult<String> jsonResult = hqAccessRecordService.healthCheck();
        if ("0".equals(jsonResult.getCode())) {
            return true;
        }
        return false;
    }

    /**
     * 南通健康检测
     *
     * @return
     */
    @Override
    public boolean healthCheckNt() {
        JsonResult<String> stringJsonResult = ntAccessRecordService.healthCheck();
        if ("0".equals(stringJsonResult.getCode())) {
            return true;
        }
        return false;
    }
}
