package cn.com.cowain.ibms.service.iot.impl;

import cn.com.cowain.ibms.dao.SysUserDao;
import cn.com.cowain.ibms.dao.iot.AccessControlSpotApplicationRecordDao;
import cn.com.cowain.ibms.dao.iot.AccessControlSpotApproverDao;
import cn.com.cowain.ibms.dao.iot.AccessControlSpotApproverRelationDao;
import cn.com.cowain.ibms.dao.iot.AccessControlSpotDao;
import cn.com.cowain.ibms.dao.task.TaskCenterTaskDao;
import cn.com.cowain.ibms.entity.MessageBody;
import cn.com.cowain.ibms.entity.MessagePoolRecord;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.iot.AccessControlSpot;
import cn.com.cowain.ibms.entity.iot.AccessControlSpotApplicationRecord;
import cn.com.cowain.ibms.entity.iot.AccessControlSpotApprover;
import cn.com.cowain.ibms.entity.iot.AccessControlSpotApproverRelation;
import cn.com.cowain.ibms.entity.task.TaskCenterTask;
import cn.com.cowain.ibms.enumeration.ApprovalStatus;
import cn.com.cowain.ibms.enumeration.iot.ApproverType;
import cn.com.cowain.ibms.enumeration.task.TaskOperateResult;
import cn.com.cowain.ibms.enumeration.task.TaskStatus;
import cn.com.cowain.ibms.feign.task_center.bean.UpdateTaskReq;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.iot.AccessControlSpotApproverPageReq;
import cn.com.cowain.ibms.rest.resp.iot.AccessControlSpotApproverPageResp;
import cn.com.cowain.ibms.rest.resp.iot.AccessControlSpotDetailResp;
import cn.com.cowain.ibms.service.MessageSendService;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.iot.AccessControlSpotApproverService;
import cn.com.cowain.ibms.service.space.SpaceService;
import cn.com.cowain.ibms.service.task.TaskCenterTaskService;
import cn.com.cowain.ibms.utils.DateUtils;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/3/9 13:36
 */
@Slf4j
@Service
public class AccessControlSpotApproverServiceImpl implements AccessControlSpotApproverService {

    @Autowired
    private SysUserDao sysUserDao;

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserServiceUC;

    @Resource
    private MessageSendService messageSendService;

    @Autowired
    private AccessControlSpotDao spotDao;

    @Autowired
    private AccessControlSpotApproverDao accessControlSpotApproverDao;

    @Autowired
    private AccessControlSpotApproverRelationDao accessControlSpotApproverRelationDao;

    @Autowired
    private AccessControlSpotApplicationRecordDao accessControlSpotApplicationRecordDao;
    @Resource
    private TaskCenterTaskDao taskCenterTaskRecordDao;
    @Resource
    private TaskCenterTaskService taskCenterTaskService;

    @Value("${cowain.send.message.vx.msgTemplateId}")
    private String vxMsgTemplateId;
    @Value("${ms-task-center.access-control-spot-application.applicant-h5-url}")
    private String accessControlSpotApplicationClaimerUrl;


    @Override
    public ServiceResult accessControlSpotApproverPage(int page, int size) {

        Pageable pageable = PageRequest.of(page, size, Sort.by("createdTime"));
        Page<AccessControlSpotApprover> accessControlSpotPage = accessControlSpotApproverDao.findAll(pageable);

        PageBean<AccessControlSpotApproverPageResp> pageBean = new PageBean<>();

        ArrayList<AccessControlSpotApproverPageResp> list = new ArrayList<>();
        accessControlSpotPage.getContent().forEach(it ->{
            AccessControlSpotApproverPageResp resp = new AccessControlSpotApproverPageResp();
            resp.setId(it.getId());
            resp.setEmpNo(it.getUserHrId());
            SysUser sysUser = sysUserServiceUC.getUserByEmpNo(it.getUserHrId());
            resp.setName(sysUser.getNameZh());
            resp.setDept(sysUser.getFullDepartmentName());
            String[] split = it.getApproveType().split(",");
            List<String> strings = Arrays.asList(split);
            resp.setApproveType(strings);
            resp.setCreatedTime(it.getCreatedTime());
            List<AccessControlSpotApproverRelation> spotApproverRelationList = accessControlSpotApproverRelationDao.findByUserHrId(it.getUserHrId());
            resp.setAccessControlSpotNum(spotApproverRelationList.size());
            list.add(resp);
        });
        pageBean.setList(list);
        pageBean.setSize(page);
        pageBean.setSize(size);
        pageBean.setTotalPages(accessControlSpotPage.getTotalPages());
        pageBean.setTotalElements(accessControlSpotPage.getTotalElements());
        return ServiceResult.ok(pageBean);
    }

    @Override
    @Transactional
    public ServiceResult accessControlSpotApproverSave(AccessControlSpotApproverPageReq req) {

        SysUser userByEmpNo = sysUserServiceUC.getUserByEmpNo(req.getEmpNo());
        if(null == userByEmpNo || StringUtils.isBlank(userByEmpNo.getEmpNo())){
            return ServiceResult.error("添加失败工号有误");
        }
        // 根据工号获取审批人
        Optional<AccessControlSpotApprover> approverOp = accessControlSpotApproverDao.findByUserHrId(req.getEmpNo());
        if(approverOp.isPresent()){
            // 业务处理失败，手动回滚事务,返回异常
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return ServiceResult.error("不可重复配置分配人");
        }

        // 保存审批人
        AccessControlSpotApprover approver = new AccessControlSpotApprover();
        approver.setUserHrId(req.getEmpNo());
        String approveType = "";
        for(ApproverType type : req.getApproveType()){
            approveType += type + ",";
        }
        approver.setApproveType(approveType.substring(0, approveType.length() - 1));
        accessControlSpotApproverDao.save(approver);
        IdResp idResp = new IdResp();
        idResp.setId(approver.getId());

        // 保存门禁点审批人关系
        if(null != req.getAccessControlSpotIds()){
            for(String spotId : req.getAccessControlSpotIds()){
                Optional<AccessControlSpot> spotOp = spotDao.findById(spotId);
                if(spotOp.isEmpty()){
                    // 业务处理失败，手动回滚事务,返回异常
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return ServiceResult.error("保存失败,门禁点不存在");
                }
                AccessControlSpot accessControlSpot = spotOp.get();
                AccessControlSpotApproverRelation accessControlSpotApproverRelation = new AccessControlSpotApproverRelation();
                accessControlSpotApproverRelation.setUserHrId(req.getEmpNo());
                accessControlSpotApproverRelation.setSpot(accessControlSpot);
                accessControlSpotApproverRelationDao.save(accessControlSpotApproverRelation);
            }
        }

        return ServiceResult.ok(idResp);
    }

    @Override
    public ServiceResult accessControlSpotApproverDetails(String id) {

        Optional<AccessControlSpotApprover> approverOp = accessControlSpotApproverDao.findById(id);
        if(approverOp.isEmpty()){
            return ServiceResult.error("id有误");
        }
        AccessControlSpotApproverPageResp resp = new AccessControlSpotApproverPageResp();

        AccessControlSpotApprover approver = approverOp.get();
        resp.setId(id);
        resp.setEmpNo(approver.getUserHrId());
        SysUser sysUser = sysUserServiceUC.getUserByEmpNo(approver.getUserHrId());
        resp.setName(sysUser.getNameZh());
        resp.setDept(sysUser.getFullDepartmentName());
        String[] split = approver.getApproveType().split(",");
        List<String> strings = Arrays.asList(split);
        resp.setApproveType(strings);
        resp.setCreatedTime(approver.getCreatedTime());

        //获取分配人门禁
        List<AccessControlSpotDetailResp> spotList = new ArrayList<>();
        accessControlSpotApproverRelationDao.findByUserHrId(approver.getUserHrId()).forEach(it ->{

            AccessControlSpot spot = it.getSpot();
            AccessControlSpotDetailResp spotDetailResp = new AccessControlSpotDetailResp();
            spotDetailResp.setId(spot.getId());
            spotDetailResp.setName(spot.getName());
            if(null != spot.getAccessControlSpotType()){
                spotDetailResp.setType(spot.getAccessControlSpotType());
                spotDetailResp.setTypeName(spot.getAccessControlSpotType().getName());
            }
            spotDetailResp.setAddress(spot.getSpace().getProject().getProjectName() + " / " + SpaceService.getFullSpaceName(spot.getSpace(), spot.getSpace().getName()) + " / " + spot.getAddress());
            spotList.add(spotDetailResp);
        });


        resp.setSpotList(spotList);

        return ServiceResult.ok(resp);
    }

    @Override
    @Transactional
    public ServiceResult accessControlSpotApproverDelete(String id) {

        Optional<AccessControlSpotApprover> approverOp = accessControlSpotApproverDao.findById(id);
        if(approverOp.isEmpty()){
            return ServiceResult.error("id有误,分配人不存在");
        }

        AccessControlSpotApprover approver = approverOp.get();
        List<AccessControlSpotApproverRelation> deleteRelationList = new ArrayList<>();
        List<AccessControlSpotApproverRelation> byUserHrIdList = accessControlSpotApproverRelationDao.findByUserHrId(approver.getUserHrId());
        for(AccessControlSpotApproverRelation it : byUserHrIdList){

            // 推送拒绝消息
            List<AccessControlSpotApproverRelation> relationList = accessControlSpotApproverRelationDao.findAllBySpotId(it.getSpot().getId());
            if(relationList.size() == 1 && relationList.get(0).getUserHrId().equals(approver.getUserHrId())){
                List<AccessControlSpotApplicationRecord> bySpotIdList = accessControlSpotApplicationRecordDao.findBySpotId(relationList.get(0).getSpot().getId());
                for(AccessControlSpotApplicationRecord record : bySpotIdList){
                    if(ApprovalStatus.PENDING.equals(record.getStatus())){

                        // 更新待办中心状态
                        List<TaskCenterTask> centerTaskList = taskCenterTaskRecordDao.findAllByOperateResultAndUcHrId(TaskOperateResult.PENDING, approver.getUserHrId());
                        for(TaskCenterTask taskCenterTask : centerTaskList){
                            UpdateTaskReq updateTaskReq = new UpdateTaskReq();
                            updateTaskReq.setId(taskCenterTask.getTaskCenterId());
                            updateTaskReq.setOperateResult(TaskOperateResult.DECLINED.getValue());
                            updateTaskReq.setStatus(TaskStatus.DONE.getValue());
                            ServiceResult updateTaskResult = taskCenterTaskService.updateTask(taskCenterTask, updateTaskReq);
                            if (!updateTaskResult.isSuccess()) {
                                // 业务处理失败，手动回滚事务,返回异常
                                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                                return ServiceResult.error(updateTaskResult.getObject());
                            }
                        }

                        Optional<SysUser> sysUserOp = sysUserDao.findByEmpNo(record.getApplicantHrId());
                        if(sysUserOp.isPresent() && StringUtils.isNotEmpty(sysUserOp.get().getWxOpenId())){
                            MessagePoolRecord messagePoolRecord = new MessagePoolRecord();

                            messagePoolRecord.setMsgFromSys(8);
                            messagePoolRecord.setMsgType(3);
                            messagePoolRecord.setPriority(0);
                            messagePoolRecord.setSendType(0);
                            messagePoolRecord.setReceiveType(501);
                            messagePoolRecord.setMsgTitle("门禁通行权限申请结果");
                            MessageBody messageBody = new MessageBody();
                            messageBody.setTitle("您好，您申请的门禁通行权限已处理，请知悉");
                            messageBody.setType("通行权限分配");
                            messageBody.setDepart("已取消");
                            messageBody.setTime(DateUtils.formatLocalDateTime(LocalDateTime.now(), DateUtils.PATTERN_DATETIME1));
                            messageBody.setRemark("该门禁没有分配人");
                            messagePoolRecord.setMsgContent(JSON.toJSONString(messageBody));

                            messagePoolRecord.setReceivePerson(sysUserOp.get().getWxOpenId());
                            messagePoolRecord.setReceivePersonId(sysUserOp.get().getWxOpenId());
                            messagePoolRecord.setMsgDetailsUrl(accessControlSpotApplicationClaimerUrl + record.getId());
                            messagePoolRecord.setMsgTemplateId(vxMsgTemplateId);
                            messagePoolRecord.setTipType(1);

                            messageSendService.send(messagePoolRecord);
                        }

                        // 更新审批记录状态
                        record.setStatus(ApprovalStatus.CANCEL);
                        record.setDeniedReason("分配人不存在，系统自动取消");
                        accessControlSpotApplicationRecordDao.save(record);
                    }
                }
            }
            // 删除审批人门禁点关系
            it.setIsDelete(1);
            deleteRelationList.add(it);
        }
        accessControlSpotApproverRelationDao.saveAll(deleteRelationList);

        // 删除审批人
        approver.setIsDelete(1);
        accessControlSpotApproverDao.save(approver);

        return ServiceResult.ok();
    }

    @Override
    @Transactional
    public ServiceResult accessControlSpotApproverUpdate(String id, AccessControlSpotApproverPageReq req) {

        Optional<AccessControlSpotApprover> approverOp = accessControlSpotApproverDao.findById(id);
        if(approverOp.isEmpty()){
            // 业务处理失败，手动回滚事务,返回异常
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return ServiceResult.error("id有误,编辑失败");
        }
        // 判断是否已是分配人
        Optional<AccessControlSpotApprover> approverOptional = accessControlSpotApproverDao.findByUserHrId(req.getEmpNo());
        if(approverOptional.isPresent() && !approverOptional.get().getId().equals(id)){
            // 业务处理失败，手动回滚事务,返回异常
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return ServiceResult.error("该成员已是分配人不可重复分配");
        }

        AccessControlSpotApprover approver = approverOp.get();
        if(!approver.getUserHrId().equals(req.getEmpNo())){
            // 删除原有分配人
            accessControlSpotApproverDelete(id);
            // 保存新增分配人
            accessControlSpotApproverSave(req);
        }

        // 编辑原有分配人信息
        String approveType = "";
        for(ApproverType type : req.getApproveType()){
            approveType += type + ",";
        }
        approver.setApproveType(approveType.substring(0, approveType.length() - 1));
        accessControlSpotApproverDao.save(approver);

        // 更新分配人门禁点关系
        List<String> spotIds = new ArrayList<>();
        accessControlSpotApproverRelationDao.findByUserHrId(req.getEmpNo()).forEach(it -> spotIds.add(it.getSpot().getId()));

        List<AccessControlSpotApproverRelation> deleteRelationList = new ArrayList<>();
        Map<String, String> tempMap = req.getAccessControlSpotIds().parallelStream().collect(Collectors.toMap(Function.identity(), Function.identity(), (oldData, newData) -> newData));
        List<String> collect = spotIds.parallelStream().filter(str -> !tempMap.containsKey(str)).collect(Collectors.toList());
        for(int i = 0;i < collect.size();i++){
            Optional<AccessControlSpotApproverRelation> relationOp = accessControlSpotApproverRelationDao.findByUserHrIdAndSpotId(req.getEmpNo(),collect.get(i));
            if(relationOp.isPresent()){
                // 推送拒绝消息
                List<AccessControlSpotApproverRelation> relationList = accessControlSpotApproverRelationDao.findAllBySpotId(relationOp.get().getSpot().getId());
                if(relationList.size() == 1 && relationList.get(0).getUserHrId().equals(approver.getUserHrId())){
                    accessControlSpotApplicationRecordDao.findBySpotId(relationList.get(0).getSpot().getId()).forEach(record ->{
                        if(ApprovalStatus.PENDING.equals(record.getStatus())){

                            Optional<SysUser> sysUserOp = sysUserDao.findByEmpNo(record.getApplicantHrId());
                            if(sysUserOp.isPresent() && StringUtils.isNotEmpty(sysUserOp.get().getWxOpenId())){
                                MessagePoolRecord messagePoolRecord = new MessagePoolRecord();

                                messagePoolRecord.setMsgFromSys(8);
                                messagePoolRecord.setMsgType(3);
                                messagePoolRecord.setPriority(0);
                                messagePoolRecord.setSendType(0);
                                messagePoolRecord.setReceiveType(501);
                                messagePoolRecord.setMsgTitle("门禁通行权限申请结果");
                                MessageBody messageBody = new MessageBody();
                                messageBody.setTitle("您好，您申请的门禁通行权限已处理，请知悉");
                                messageBody.setType("通行权限分配");
                                messageBody.setDepart("已取消");
                                messageBody.setTime(DateUtils.formatLocalDateTime(LocalDateTime.now(), DateUtils.PATTERN_DATETIME1));
                                messageBody.setRemark("该门禁没有分配人");
                                messagePoolRecord.setMsgContent(JSON.toJSONString(messageBody));

                                messagePoolRecord.setReceivePerson(sysUserOp.get().getWxOpenId());
                                messagePoolRecord.setReceivePersonId(sysUserOp.get().getWxOpenId());
                                messagePoolRecord.setMsgDetailsUrl(accessControlSpotApplicationClaimerUrl + record.getId());
                                messagePoolRecord.setMsgTemplateId(vxMsgTemplateId);
                                messagePoolRecord.setTipType(1);

                                messageSendService.send(messagePoolRecord);
                            }

                            // 更新审批记录状态
                            record.setStatus(ApprovalStatus.CANCEL);
                            record.setDeniedReason("分配人不存在，系统自动取消");
                            accessControlSpotApplicationRecordDao.save(record);
                        }
                    });
                }

                // 删除人员和门禁点关系
                AccessControlSpotApproverRelation relation = relationOp.get();
                relation.setIsDelete(1);
                deleteRelationList.add(relation);
            }
        }
        accessControlSpotApproverRelationDao.saveAll(deleteRelationList);

        // 保存新增门禁点和分配人关系
        for(String spotId : req.getAccessControlSpotIds()){

            Optional<AccessControlSpot> spotOp = spotDao.findById(spotId);
            if(spotOp.isEmpty()){
                // 业务处理失败，手动回滚事务,返回异常
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error("编辑失败,门禁点不存在");
            }
            AccessControlSpot spot = spotOp.get();
            Optional<AccessControlSpotApproverRelation> relationOp = accessControlSpotApproverRelationDao.findByUserHrIdAndSpotId(req.getEmpNo(), spotId);
            if(relationOp.isEmpty()){
                AccessControlSpotApproverRelation relation = new AccessControlSpotApproverRelation();
                relation.setSpot(spot);
                relation.setUserHrId(req.getEmpNo());
                accessControlSpotApproverRelationDao.save(relation);
            }
        }
        return ServiceResult.ok();
    }
}
