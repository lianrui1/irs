package cn.com.cowain.ibms.feign.ehr;

import cn.com.cowain.ibms.feign.bean.ehr.CheckLabourResp;
import cn.com.cowain.sfp.comm.JsonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author wei.cheng
 * @date 2022/03/25 16:36
 */
@Service
@RequestMapping({"/ehr"})
@FeignClient(value = "ehr", url = "${cowain.gateway.address}")
public interface EHRAPi {
    /**
     * 判断是否是劳务
     *
     * @param jobNum
     * @return
     */
    @GetMapping("/external/staff/checkLabour")
    JsonResult<CheckLabourResp> checkLabourResp(@RequestParam String jobNum);
}
