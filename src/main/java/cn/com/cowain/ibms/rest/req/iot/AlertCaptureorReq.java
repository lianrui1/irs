package cn.com.cowain.ibms.rest.req.iot;

import cn.com.cowain.ibms.enumeration.AlertCaptureorStatus;
import cn.com.cowain.ibms.enumeration.AlertType;
import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 警戒抓拍记录查询参数
 *
 * @author: yanzy
 * @date: 2022/4/13 13:25
 */
@Data
@ApiModel("警戒抓拍记录查询参数")
public class AlertCaptureorReq extends PageReq {

    @ApiModelProperty(value = "查看状态",position = 1)
    private AlertCaptureorStatus status;

    @ApiModelProperty(value = "监控点id",position = 2)
    private String alertSpotId;

    @ApiModelProperty(value = "抓拍类型",position = 3)
    private AlertType alertType;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "抓拍查询开始时间",position = 4)
    private LocalDateTime alertTimeStartTime ;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "抓拍结束时间",position = 5)
    private LocalDateTime alertTimeEndTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "抓拍时间",position = 6)
    private LocalDate alertTimeDay;
}
