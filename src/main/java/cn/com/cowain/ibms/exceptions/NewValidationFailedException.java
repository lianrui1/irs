package cn.com.cowain.ibms.exceptions;

import cn.com.cowain.ibms.aop.FieldErrorWrapper;

/**
 * 新参数校验失败异常，V1.2迭代开始使用
 *
 * @author Hu Jingling
 * @since 2020-08-04
 */
public class NewValidationFailedException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private final FieldErrorWrapper fieldErrorWrapper;

    public FieldErrorWrapper getFieldErrorWrapper() {
        return fieldErrorWrapper;
    }

    public NewValidationFailedException(FieldErrorWrapper fieldErrorWrapper) {
        this.fieldErrorWrapper = fieldErrorWrapper;
    }

}
