package cn.com.cowain.ibms.rest.req.iot;

import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2021/10/11 9:22
 */
@Data
@ApiModel("门磁列表分页查询对象")
public class DoorMagneticPageReq extends PageReq {

    @ApiModelProperty(value = "项目ID", example = "1", position = 1)
    private String projectId;

    @ApiModelProperty(value = "空间ID", example = "1", position = 2)
    private String spaceId;

    @ApiModelProperty(value = "所属应用", example = "IBMS", position = 3)
    private String application;

    @ApiModelProperty(value = "设备状态", example = "ONLINE", position = 4)
    private DeviceStatus status;

    @ApiModelProperty(value = "查询关键字，设备名称 / sn", example = "123", position = 5)
    private String keyword;
}
