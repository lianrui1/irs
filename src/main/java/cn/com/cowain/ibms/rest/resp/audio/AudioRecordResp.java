package cn.com.cowain.ibms.rest.resp.audio;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2020/12/29 16:12
 */
@Data
@ApiModel("会议记录详情")
public class AudioRecordResp {

    @ApiModelProperty(value = "主键ID")
    private String id;

    /**
     * 会议预约记录ID，外键
     */
    @ApiModelProperty(value = "会议id", required = true, position = 0)
    private String reservationRecordId;




}
