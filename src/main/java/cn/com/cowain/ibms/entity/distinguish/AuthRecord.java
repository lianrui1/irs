package cn.com.cowain.ibms.entity.distinguish;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author wei.cheng
 * @date 2022/03/28 10:12
 */
@Data
@Entity
@Table(name = "ibms_auth_record")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_auth_record", comment = "南通排班记录表")
@Where(clause = "is_del=0")
public class AuthRecord extends BaseEntity {

    @Column(name = "content", nullable = false, columnDefinition = "json COMMENT '请求内容'")
    private String content;
}
