package cn.com.cowain.ibms.enumeration;

/**
 * @author Yang.Lee
 * @date 2022/2/28 13:40
 */
public enum Condition {

    EQUAL("等于", "="),

    GREATER("大于", ">"),

    LESS("小于", "<"),

    GREATER_EQ("大于等于", ">="),

    LESS_EQ("小于等于", "<="),

    IN("在范围内", "in"),

    NOT_IN("不在范围内", "not in");

    private String name;

    private String symbol;


    Condition(String name, String symbol){
        this.name = name;
        this.symbol = symbol;
    }
}
