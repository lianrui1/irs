package cn.com.cowain.ibms.dao.distinguish;

import cn.com.cowain.ibms.entity.distinguish.StaffGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StaffGroupDao extends JpaRepository<StaffGroup, String>, JpaSpecificationExecutor<StaffGroup> {

    /**
     * 获取会议室排序最大值
     * @return
     */
    @Query("SELECT MAX(staff.seq) FROM StaffGroup AS staff")
    Integer findMaxSeq();

    /**
     * 根据名称获取人员组
     * @return
     */
    Optional<StaffGroup> findByName(String name);
}
