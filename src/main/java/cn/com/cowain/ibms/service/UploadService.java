package cn.com.cowain.ibms.service;


import cn.com.cowain.ibms.service.bean.ServiceResult;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

public interface UploadService {

    // 文件上传
    ServiceResult upload(MultipartFile multipartFile);

    /**
     * 上传人脸照片并校验
     *
     * @param file
     * @author wei.cheng
     * @date 2022/3/10 9:26 上午
     **/
    ServiceResult uploadAndCheckFace(MultipartFile file);

    /**
     * 上传图片
     *
     * @param file 文件
     * @return
     * @author Yang.Lee
     * @date 2022/7/11 19:49
     **/
    ServiceResult upload(File file);
}
