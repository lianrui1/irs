package cn.com.cowain.ibms.service.meeting;

import cn.com.cowain.ibms.feign.bean.oa.guest.InvitationReason;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/6/30 17:07
 */
public interface InvitationService {

    /**
     * 获取邀请事由列表
     *
     * @param token ehr短token
     * @param empNo 员工工号
     * @return 事由列表
     * @author Yang.Lee
     * @date 2021/6/30 17:08
     **/
    List<InvitationReason> getInvitationReason(String token, String empNo);
}
