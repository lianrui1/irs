package cn.com.cowain.ibms.rest.resp.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 智能断路器返回数据对象
 * @author Yang.Lee
 * @date 2021/4/29 8:58
 */
@Data
@ApiModel("智能断路器用电数据")
public class IntelligentCircuitBreakerDataResp {


    @ApiModelProperty(value = "设备id", required = true, position = 1)
    private String deviceId;

    @ApiModelProperty(value = "用电时间", required = true, position = 2)
    private String datetime;

    @ApiModelProperty(value = "用电量", required = true, position = 3)
    private Integer electricity;

}
