package cn.com.cowain.ibms.exceptions;

import cn.com.cowain.ibms.service.EhrRemoteService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

/**
 * RemoteServiceUnavailableExceptionTest
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/21/20
 */
@SpringBootTest
class RemoteServiceUnavailableExceptionTest {

    @MockBean
    private EhrRemoteService ehrRemoteService;
}