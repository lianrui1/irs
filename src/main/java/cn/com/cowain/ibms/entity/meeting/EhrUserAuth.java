package cn.com.cowain.ibms.entity.meeting;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/9 10:28
 */
@Data
public class EhrUserAuth {

    // 访客工号
    private String jobNum;

    private String vruid;
}
