package cn.com.cowain.ibms.rest.req.device.centralized;

import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author wei.cheng
 * @date 2022/09/26 10:05
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "h5端查询集控码设备列表请求")
public class CentralizedControlCodeDeviceListH5Req extends PageReq {

    @ApiModelProperty(value = "用户uc id", required = true)
    @NotBlank
    private String uid;

    @ApiModelProperty(value = "设备类型")
    @NotNull
    private DeviceType deviceType;
}
