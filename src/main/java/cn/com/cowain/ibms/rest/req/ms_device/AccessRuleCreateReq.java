package cn.com.cowain.ibms.rest.req.ms_device;

import lombok.Data;

/**
 * 创建通行记录请求参数对象
 *
 * @author Yang.Lee
 * @date 2021/3/19 20:32
 */
@Data
public class AccessRuleCreateReq {

    /**
     * 员工组集合
     **/
    private String[] groupUuidList;

    /**
     * 设备组集合
     **/
    private String[] deviceUuidList;
}
