package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Yang.Lee
 * @date 2022/5/4 10:14
 */
@Data
@ApiModel("考勤数据分析对象")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AttendanceStatisticsResp {


    @ApiModelProperty(value = "考勤设备总数", required = true, example = "1", position = 1)
    private long deviceNumber;

    @ApiModelProperty(value = "未通行人员总数", required = true, example = "1000", position = 2)
    private long unrecognizedNumber;

}
