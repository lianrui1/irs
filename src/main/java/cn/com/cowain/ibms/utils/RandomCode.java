package cn.com.cowain.ibms.utils;

import lombok.extern.slf4j.Slf4j;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

@Slf4j
public class RandomCode {

	private static Random random;

	static {
		try {
			random = SecureRandom.getInstanceStrong();
		} catch (NoSuchAlgorithmException e) {
			log.error(e.getMessage(), e);
		}
	}

	public RandomCode() throws NoSuchAlgorithmException {
	}

	/**
	 * 获取N位随机字符串,数字、大写字母、小写字母
	 * @param num 字符串位数
	 * */
	public static String getRandom(int num){
		//初始化种子
		String[] str={"0","1","2","3","4","5","6","7","8","9",
			       "a","b","c","d","e","f","g","h","i","j",
			       "k","l","m","n","p","q","r","s","t",
			       "A","B","C","D","E","F","G","H","I","J",
			       "K","L","M","N","P","Q","R","S","T"
			       };
		int number=str.length;

		StringBuffer code = new StringBuffer("");
		//随机产生一个10个字符的字符串
		for( int i=0 ; i < num ; i++ ){
			code.append(str[random.nextInt(number)]);
		}		 
		return code.toString();
	}
	
	
	/**
	 * 获取N位随机纯数字字符串
	 * @param num 随机数字位数
	 * */
	public static String getRandomNum(int num){
		//初始化种子
		String[] str={"0","1","2","3","4","5","6","7","8","9"};
		int number=str.length;

		StringBuffer code = new StringBuffer("");
		//随机产生一个10个字符的字符串
		for( int i=0 ; i < num ; i++ ){
			code.append(str[random.nextInt(number)]);
		}		 
		return code.toString();
	}

}
