package cn.com.cowain.ibms.rest.resp.device.centralized;

import cn.com.cowain.ibms.enumeration.iot.ApproverType;
import cn.com.cowain.ibms.enumeration.iot.DeviceControlStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel("集控操作日志列表响应对象")
public class RecordResp {

    @ApiModelProperty(value = "id", required = true)
    private String id;

    @ApiModelProperty(value = "工号", required = true)
    private String hrId;

    @ApiModelProperty(value = "姓名", required = true)
    private String name;

    @ApiModelProperty(value = "人员类型", required = true)
    private ApproverType type;

    @ApiModelProperty(value = "人员类型名称", required = true)
    private String typeName;

    @ApiModelProperty(value = "操作名称", required = true)
    private String controlName;

    @ApiModelProperty(value = "状态", required = true)
    private DeviceControlStatus status;

    @ApiModelProperty(value = "状态名称", required = true)
    private String statusName;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "控制时间", required = true)
    private LocalDateTime controlTime;

    @ApiModelProperty(value = "traceId")
    private String traceId;
}
