package cn.com.cowain.ibms.service.office;

import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.office.IntelligentOffice;
import cn.com.cowain.ibms.entity.office.IntelligentOfficeDevice;

import java.util.List;
import java.util.Map;

/**
 * @author wei.cheng
 * @date 2022/02/28 17:27
 */
public interface IntelligentOfficeDeviceService {

    /**
     * 查询用户真实应该关联的IntelligentOfficeDevice
     * 1. 当用户为会议所属空间的管理员时，查询本人的IntelligentOfficeDevice
     * 2. 当用户为会议的普通成员时，查询邀请本人的管理员所属的IntelligentOfficeDevice
     * 3. 其它情况，查询本人的IntelligentOfficeDevice
     *
     * @param userHrId
     * @param intelligentOffice
     * @return
     */
    Map<String, IntelligentOfficeDevice> getDeviceIdToIntelligentOfficeDeviceMapOfUserHrIdAndOfficeId(String userHrId, IntelligentOffice intelligentOffice);

    /**
     * 查询用户真实应该关联的IntelligentOfficeDevice
     * 1. 当用户为会议所属空间的管理员时，查询本人的IntelligentOfficeDevice
     * 2. 当用户为会议的普通成员时，查询邀请本人的管理员所属的IntelligentOfficeDevice
     * 3. 其它情况，查询本人的IntelligentOfficeDevice
     *
     * @param userHrId
     * @param intelligentOffice
     * @return
     */
    List<IntelligentOfficeDevice> getIntelligentOfficeDeviceListOfUserHrIdAndOfficeId(String userHrId, IntelligentOffice intelligentOffice);

    // 根据工号和空间获取当前空间及所有子空间设备
    List<IntelligentOfficeDevice> getDeviceListOfHrIdAndSpaceId(String hrId, IntelligentOffice intelligentOffice);

    void getListBySpace(List<String> spaceIds, Space space);
}
