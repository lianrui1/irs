package cn.com.cowain.ibms.rest.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.domain.Sort;

/**
 * @author Yang.Lee
 * @date 2021/4/8 14:47
 */
@Data
public class Sorts {

    /**
     * 公共排序字段： createdTime
     **/
    public static final String CREATED_TIME = "createdTime";

    public Sorts() {
    }

    public Sorts(String sortName, Sort.Direction direction) {
        this.sortName = sortName;
        this.direction = direction;
    }

    @ApiModelProperty(value = "排序字段", position = 1)
    private String sortName;

    @ApiModelProperty(value = "排序方式", position = 2)
    private Sort.Direction direction;

    /**
     * 获取默认排序实例（根据创建时间倒叙）
     *
     * @return 排序实例
     * @author Yang.Lee
     * @date 2021/4/8 16:10
     **/
    public static Sorts getDefaultDesc() {

        return getInstance(Sorts.CREATED_TIME, Sort.Direction.DESC);
    }

    /**
     * 获取默认排序实例（根据创建时间正序）
     *
     * @return 排序实例
     * @author Yang.Lee
     * @date 2021/4/8 16:10
     **/
    public static Sorts getDefaultAsc() {

        return getInstance(Sorts.CREATED_TIME, Sort.Direction.ASC);
    }

    /**
     * 获取排序实例
     *
     * @return 排序实例
     * @author Yang.Lee
     * @date 2021/4/8 16:10
     **/
    public static Sorts getInstance(String sortName, Sort.Direction direction) {

        return new Sorts(sortName, direction);
    }
}
