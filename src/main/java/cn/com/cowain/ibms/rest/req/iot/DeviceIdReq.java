package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/11/15 17:07
 */
@Data
public class DeviceIdReq {

    @ApiModelProperty(value = "设备ID集合", position = 1)
    private List<String> idList;
}
