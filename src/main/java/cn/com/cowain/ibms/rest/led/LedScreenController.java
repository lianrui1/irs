package cn.com.cowain.ibms.rest.led;

import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.req.iot.AttendanceInOutResp;
import cn.com.cowain.ibms.rest.req.iot.AttendanceStatisticsResp;
import cn.com.cowain.ibms.rest.req.meeting.MeetingStatisticsResp;
import cn.com.cowain.ibms.rest.resp.ChartDataResp;
import cn.com.cowain.ibms.service.ReservationRecordService;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.ibms.utils.RedisUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * 大屏数据controller
 * 
 * @author Yang.Lee
 * @date 2021/9/27 19:40
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/led")
@Api(tags = IConst.MODULE_LED)
public class LedScreenController {

    @Resource
    private ReservationRecordService reservationRecordService;

    @Resource
    private RedisUtil redisUtil;


    @GetMapping("/meetingNumberStatistics/{projectId}")
    @ApiOperation("获取今天会议统计数据")
    public ResponseEntity<JsonResult<MeetingStatisticsResp>> getTodayMeetingNumberStatistics(@PathVariable String projectId) {

        return ResponseEntity.ok(JsonResult.ok("OK", reservationRecordService.getTodayMeetingNumberStatistics(projectId)));
    }

    @GetMapping("/meetingTimePeriodStatistics/{projectId}")
    @ApiOperation("获取今天会议统计数据")
    public ResponseEntity<JsonResult<List<ChartDataResp>>> getTodayMeetingTimePeriodStatistics(@PathVariable String projectId){

        return ResponseEntity.ok(JsonResult.ok("OK", reservationRecordService.getTodayMeetingTimePeriodStatistics(projectId)));
    }


    @GetMapping("/attendanceStatistics/{projectId}")
    @ApiOperation("获取今天考勤统计数据")
    public ResponseEntity<JsonResult<AttendanceStatisticsResp>> getTodayAttendanceStatistics(@PathVariable String projectId){

        String redisKey = "attendanceStatistics-" + projectId;
        String realKey = redisUtil.createRealKey(redisKey);
        AttendanceStatisticsResp resp = new AttendanceStatisticsResp();
        if(Boolean.TRUE.equals(redisUtil.hasKey(realKey))){
            resp = JSON.parseObject(redisUtil.get(realKey).toString(), AttendanceStatisticsResp.class);
        }

        return ResponseEntity.ok(JsonResult.ok("OK", resp));
    }

    @GetMapping("/attendanceInOut/{projectId}")
    @ApiOperation("获取12小时内考勤设备进出的统计数据")
    public ResponseEntity<JsonResult<AttendanceInOutResp>> getAttendanceInOutData(@PathVariable String projectId){

        String inOutRedisKey = "attendanceInOut-" + projectId;
        String inOutRealKey = redisUtil.createRealKey(inOutRedisKey);
        AttendanceInOutResp resp = new AttendanceInOutResp();
        if(Boolean.TRUE.equals(redisUtil.hasKey(inOutRealKey))){
            resp = JSON.parseObject(redisUtil.get(inOutRealKey).toString(),new TypeReference<>(){});
        }

        return ResponseEntity.ok(JsonResult.ok("OK", resp));
    }

}
