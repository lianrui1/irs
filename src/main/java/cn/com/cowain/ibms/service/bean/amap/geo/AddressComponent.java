package cn.com.cowain.ibms.service.bean.amap.geo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * 地址元素
 * @author Yang.Lee
 * @date 2021/4/20 9:50
 */
@Data
public class AddressComponent {

    /**
     * 坐标点所在省名称
     **/
    private String province;

    /**
     * 坐标点所在城市名称
     **/
    private String city;

    /**
     * 城市编码
     **/
    @JSONField(name = "citycode")
    private String cityCode;

    /**
     * 坐标点所在区
     **/
    private String district;

    /**
     * 行政区编码
     **/
    @JSONField(name = "adcode")
    private String adCode;

    /**
     * 坐标点所在乡镇/街道（此街道为社区街道，不是道路信息
     **/
    private String township;

    /**
     * 乡镇街道编码
     **/
    @JSONField(name = "towncode")
    private String townCode;

    /**
     * 社区信息列表
     **/
    private Neighborhood neighborhood;

    /**
     * 楼信息列表
     **/
    private Building building;

    /**
     * 门牌信息列表
     **/
    private StreetNumber streetNumber;

    /**
     * 所属海域信息
     **/
    private String seaArea;

    /**
     * 经纬度所属商圈列表
     **/
    private List<BusinessAreas> businessAreas;

    /**
     * 社区信息
     * @author Yang.Lee
     * @date 2021/4/20 10:06
     **/
    @Data
    public static class Neighborhood{

        /**
         * 社区名称
         **/
        private String name;

        /**
         * POI类型
         **/
        private String type;
    }

    /**
     * 楼信息
     * @author Yang.Lee
     * @date 2021/4/20 10:06
     **/
    @Data
    public static class Building{

        /**
         * 建筑名称
         **/
        private String name;

        /**
         * 类型
         **/
        private String type;
    }

    /**
     * 门牌信息
     * @author Yang.Lee
     * @date 2021/4/20 10:06
     **/
    @Data
    public static class StreetNumber{

        /**
         * 街道名称
         **/
        private String street;

        /**
         * 门牌号
         **/
        private String number;

        /**
         * 经纬度坐标点：经度，纬度
         **/
        private String location;

        /**
         * 坐标点所处街道方位
         **/
        private String direction;

        /**
         * 门牌地址到请求坐标的距离，单位：米
         **/
        private String distance;
    }

    /**
     * 经纬度所属商圈信息
     * @author Yang.Lee
     * @date 2021/4/20 10:06
     **/
    @Data
    public static class BusinessAreas{

        /**
         * 商圈信息
         **/
        private String businessArea;
        /**
         * 商圈中心点经纬度
         **/
        private String location;
        /**
         * 商圈名称
         **/
        private String name;
        /**
         * 商圈所在区域的adcode
         **/
        private String id;
    }
}
