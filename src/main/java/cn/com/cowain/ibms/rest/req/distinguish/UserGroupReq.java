package cn.com.cowain.ibms.rest.req.distinguish;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/18 18:09
 */
@Data
public class UserGroupReq {

    private String id;
    private String name;
    private Integer type;
}
