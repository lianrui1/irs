package cn.com.cowain.ibms.service.iot.impl;

import cn.com.cowain.ibms.bo.SimpleUserBO;
import cn.com.cowain.ibms.dao.SysUserDao;
import cn.com.cowain.ibms.dao.ability.*;
import cn.com.cowain.ibms.dao.distinguish.*;
import cn.com.cowain.ibms.dao.iot.*;
import cn.com.cowain.ibms.dao.space.ProjectDao;
import cn.com.cowain.ibms.entity.MessageBody;
import cn.com.cowain.ibms.entity.MessagePoolRecord;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.ability.OpenAbility;
import cn.com.cowain.ibms.entity.ability.OpenAbilityAccessControlSpot;
import cn.com.cowain.ibms.entity.ability.OpenAbilityTask;
import cn.com.cowain.ibms.entity.ability.OpenAbilityTaskDetail;
import cn.com.cowain.ibms.entity.distinguish.*;
import cn.com.cowain.ibms.entity.iot.*;
import cn.com.cowain.ibms.entity.space.Project;
import cn.com.cowain.ibms.enumeration.ApprovalStatus;
import cn.com.cowain.ibms.enumeration.UserImportStatus;
import cn.com.cowain.ibms.enumeration.ability.Ability;
import cn.com.cowain.ibms.enumeration.ability.AbilityTaskStatus;
import cn.com.cowain.ibms.enumeration.ability.MethodType;
import cn.com.cowain.ibms.enumeration.ability.TaskType;
import cn.com.cowain.ibms.enumeration.iot.*;
import cn.com.cowain.ibms.feign.bean.iotc.resp.GetUserResp;
import cn.com.cowain.ibms.feign.iotc.CheckFaceImageApi;
import cn.com.cowain.ibms.feign.iotc.IOTCApi;
import cn.com.cowain.ibms.feign.iotc.bean.CreateHTMemberGroupReq;
import cn.com.cowain.ibms.feign.iotc.bean.FindPersonPicReq;
import cn.com.cowain.ibms.feign.iotc.enumeration.MethodCode;
import cn.com.cowain.ibms.feign.iotc.enumeration.ModuleCode;
import cn.com.cowain.ibms.mq.Topic;
import cn.com.cowain.ibms.mq.bean.AccessPermissions;
import cn.com.cowain.ibms.mq.bean.OpenAbilityMessage;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.PageReq;
import cn.com.cowain.ibms.rest.req.Sorts;
import cn.com.cowain.ibms.rest.req.distinguish.*;
import cn.com.cowain.ibms.rest.req.iot.UserReq;
import cn.com.cowain.ibms.rest.req.iot.*;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.StaffResp;
import cn.com.cowain.ibms.rest.resp.UploadResp;
import cn.com.cowain.ibms.rest.resp.distinguish.*;
import cn.com.cowain.ibms.rest.resp.iot.*;
import cn.com.cowain.ibms.rest.resp.space.ProjectSpaceTreeResp;
import cn.com.cowain.ibms.rest.resp.space.SpaceTreeResp;
import cn.com.cowain.ibms.service.ExcelService;
import cn.com.cowain.ibms.service.MessageSendService;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.UploadService;
import cn.com.cowain.ibms.service.ability.IotcConnectService;
import cn.com.cowain.ibms.service.bean.Guest;
import cn.com.cowain.ibms.service.bean.ServiceMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.disthinguish.AccessRulesService;
import cn.com.cowain.ibms.service.disthinguish.AuthService;
import cn.com.cowain.ibms.service.disthinguish.StaffGroupClient;
import cn.com.cowain.ibms.service.disthinguish.StaffGroupService;
import cn.com.cowain.ibms.service.guest.GuestService;
import cn.com.cowain.ibms.service.iot.AccessControlSpotService;
import cn.com.cowain.ibms.service.iot.AccessControlTimePlanService;
import cn.com.cowain.ibms.service.iotc.IOTCService;
import cn.com.cowain.ibms.service.ms_device.MSDeviceService;
import cn.com.cowain.ibms.service.qrcode.QrCodeService;
import cn.com.cowain.ibms.service.space.IotDeviceService;
import cn.com.cowain.ibms.service.space.SpaceService;
import cn.com.cowain.ibms.utils.*;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.usercenter.api.service.UserCenterStaffApi;
import cn.com.cowain.usercenter.common.resp.PassportRespForFeign;
import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static cn.com.cowain.ibms.service.impl.UploadServiceImpl.VALID_FACE_URL_PREFIX;


/**
 * @author Yang.Lee
 * @date 2021/11/19 14:09
 */
@Service
@Slf4j
public class AccessControlSpotServiceImpl implements AccessControlSpotService {

    private static final String STAFF_KEY = "staffList";
    @Resource
    private AccessControlSpotDao accessControlSpotDao;
    @Resource
    private AccessControlSpotAccessRecordDao accessControlSpotAccessRecordDao;
    @Resource
    private SpaceDao spaceDao;
    @Resource
    private RedisUtil redisUtil;
    @Resource
    private IotDeviceService iotDeviceService;
    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserServiceUC;
    @Autowired
    private AccessControlSpotUserDao spotUserDao;
    @Autowired
    private DeviceDao deviceDao;
    @Autowired
    private OpenAbilityAccessControlSpotDao openAbilityAccessControlSpotDao;

    @Value("${iotc.timeplan:e62817832b764a088f6d52033f9c9db1}")
    private String timePlan;

    @Value("${cowain.ms-iotc.device.userList.url}")
    private String iotcDeviceUserListUrl;

    @Value("${access-control-spot.batch-add-max-limit}")
    private Integer batchAddMaxLimit;

    @Value("${access-control-spot.batch-delete-max-limit}")
    private Integer batchDeleteMaxLimit;


    @Autowired
    private OpenAbilityDao openAbilityDao;

    @Resource
    private AccessRulesService accessRulesService;

    @Autowired
    private TimePlanDao timePlanDao;

    @Resource
    private DeviceCloudDao deviceCloudDao;

    @Autowired
    private StaffGroupMembersDao membersDao;

    @Autowired
    private StaffGroupDao staffGroupDao;

    @Autowired
    private AccessControlSpotStaffGroupRelationDao accessControlSpotStaffGroupRelationDao;

    @Autowired
    private UserCenterStaffApi userCenterStaffApi;

    @Resource
    private SpaceService spaceService;

    @Resource
    private ExcelService excelService;

    @Resource
    private ProjectDao projectDao;

    @Autowired
    private StaffGroupService staffGroupService;

    @Resource
    private MSDeviceService msDeviceService;

    @Autowired
    private StaffGroupMembersDao staffGroupMembersDao;

    @Resource
    private StaffGroupClient staffGroupClient;

    @Resource
    private IOTCApi iotcApi;

    @Resource
    private CheckFaceImageApi checkFaceImageApi;

    @Resource
    private IOTCService iotcService;

    @Resource
    private AccessControlSpotKSHTMemberGroupDao accessControlSpotKSHTMemberGroupDao;

    @Autowired
    private AccessControlSpotApplicationRecordDao accessControlSpotApplicationRecordDao;

    @Autowired
    private AccessControlSpotApproverRelationDao accessControlSpotApproverRelationDao;

    @Autowired
    private AccessControlSpotApproverDao accessControlSpotApproverDao;

    @Resource
    private AccessControlSpotUserDao accessControlSpotUserDao;

    @Resource
    private UploadService uploadService;

    @Value("${cowain.wechat.room_reservation.url-h5}")
    private String environmentAddress;

    @Resource
    private MessageSendService messageSendService;

    @Autowired
    private SysUserDao sysUserDao;

    @Value("${cowain.send.message.vx.msgTemplateId}")
    private String vxMsgTemplateId;

    @Value("${ms-task-center.access-control-spot-application.applicant-h5-url}")
    private String accessControlSpotApplicationClaimerUrl;

    @Resource
    private DoorMagneticDao doorMagneticDao;

    @Resource
    private QrCodeService qrCodeService;

    @Resource
    private GuestService guestService;


    @Resource
    private AccessControlSpotService accessControlSpotService;

    @Resource
    private IotcConnectService iotcConnectService;

    @Resource
    private AuthService authService;

    @Resource
    private OpenAbilityTaskDao openAbilityTaskDao;

    @Resource
    private OpenAbilityTaskDetailDao openAbilityTaskDetailDao;

    @Autowired
    private PlatformTransactionManager platformTransactionManager;

    @Autowired
    private AccessControlSpotUserSyncTaskDao accessControlSpotUserSyncTaskDao;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private AccessControlTimePlanService accessControlTimePlanService;


    public static boolean isLegalDate(int length, String sDate, String format) {
        int legalLen = length;
        if (sDate == null || sDate.length() != legalLen) {
            return false;
        }
        DateFormat formatter = new SimpleDateFormat(format);
        try {
            Date date = formatter.parse(sDate);
            return sDate.equals(formatter.format(date));
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 创建门禁点
     *
     * @param req 编辑参数
     * @return 创建结果，创建成功返回数据主键ID
     * @author Yang.Lee
     * @date 2021/11/19 14:03
     **/
    @Override
    @Transactional
    public ServiceResult create(AccessControlSpotEditReq req) {

        Optional<Space> spaceOptional = spaceDao.findById(req.getSpaceId());
        if (spaceOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_SPACE, req.getSpaceId()));
        }

        // 根据名称获取门禁点信息
        Optional<AccessControlSpot> spotOp = accessControlSpotDao.findByName(req.getName());
        if (spotOp.isPresent()) {
            return ServiceResult.error("名称已存在不可重复创建");
        }

        AccessControlSpot accessControlSpot = new AccessControlSpot();
        accessControlSpot.setSpace(spaceOptional.get());
        accessControlSpot.setName(req.getName());
        accessControlSpot.setAddress(req.getAddress());
        accessControlSpot.setAccessControlSpotLevel(req.getSpotLevel());
        accessControlSpot.setAccessControlSpotPlace(req.getAccessControlSpotPlace());
        accessControlSpot.setInOut(req.getInOut());
        accessControlSpot.setAuthType(req.getAuthType());
        accessControlSpotDao.save(accessControlSpot);

        return ServiceResult.ok(IdResp.builder().id(accessControlSpot.getId()).build());
    }

    /**
     * 更新门禁点信息
     *
     * @param spotId 门禁点ID
     * @param req    请求参数
     * @return 修改结果
     * @author Yang.Lee
     * @date 2021/11/19 14:04
     **/
    @Override
    public ServiceResult update(String spotId, AccessControlSpotEditReq req) {

        Optional<AccessControlSpot> accessControlSpotOptional = accessControlSpotDao.findById(spotId);
        if (accessControlSpotOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_ACCESS_CONTROL_SPOT, spotId), ErrConst.E01);
        }

        Optional<Space> spaceOptional = spaceDao.findById(req.getSpaceId());
        if (spaceOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_SPACE, req.getSpaceId()), ErrConst.E01);
        }

        AccessControlSpot accessControlSpot = accessControlSpotOptional.get();

        // 根据名称获取门禁点信息
        Optional<AccessControlSpot> spotOp = accessControlSpotDao.findByName(req.getName());
        if (spotOp.isPresent()) {
            AccessControlSpot spot = spotOp.get();
            if (!spot.getId().equals(accessControlSpot.getId())) {
                return ServiceResult.error("编辑失败,门禁点名称已存在");
            }
        }

        accessControlSpot.setAccessControlSpotLevel(req.getSpotLevel());
        accessControlSpot.setSpace(spaceOptional.get());
        accessControlSpot.setAddress(req.getAddress());
        accessControlSpot.setName(req.getName());
        accessControlSpot.setAccessControlSpotPlace(req.getAccessControlSpotPlace());
        accessControlSpot.setInOut(req.getInOut());
        accessControlSpot.setAuthType(req.getAuthType());
        accessControlSpotDao.save(accessControlSpot);

        return ServiceResult.ok();
    }

    /**
     * 查询门禁点详情
     *
     * @param spotId 门禁点ID
     * @return 门禁点详情
     * @author Yang.Lee
     * @date 2021/11/19 14:06
     **/
    @Override
    public AccessControlSpotDetailResp get(String spotId) {

        Optional<AccessControlSpot> accessControlSpotOptional = accessControlSpotDao.findById(spotId);

        if (accessControlSpotOptional.isEmpty()) {
            return AccessControlSpotDetailResp.builder().build();
        }

        return AccessControlSpotDetailResp.convert(accessControlSpotOptional.get());
    }

    /**
     * 删除门禁点
     *
     * @param spotId 门禁点ID
     * @return 删除结果
     * @author Yang.Lee
     * @date 2021/11/19 14:06
     **/
    @Override
    @Transactional
    public ServiceResult delete(String spotId) {

        Optional<AccessControlSpot> accessControlSpotOptional = accessControlSpotDao.findById(spotId);
        if (accessControlSpotOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_ACCESS_CONTROL_SPOT, spotId), ErrConst.E01);
        }

        AccessControlSpot spot = accessControlSpotOptional.get();

        // 获取门禁点人员
        List<String> jobNos = new ArrayList<>();
        List<AccessControlSpotUser> spotUserList = spotUserDao.findByAccessControlSpotId(spot.getId());
        spotUserList.forEach(it -> {
            it.setIsDelete(1);
            jobNos.add(it.getUserHrId());
        });

        // 向海康删除人员
        if (null != spot.getDevice() && DeviceType.HIK_FACE_MACHINE.equals(spot.getDevice().getDeviceType())) {
            ServiceResult result = accessRulesService.deleteHkUser(spot.getDevice().getId(), jobNos);
            if (!result.isSuccess()) {
                // 业务处理失败，手动回滚事务,返回异常
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error(String.valueOf(result.getObject()));
            }
        } else if (null != spot.getDevice() && DeviceType.FACE_RECOGNITION.equals(spot.getDevice().getDeviceType())) {

            // 向旷视删除权限
            Optional<DeviceCloud> deviceCloudOp = deviceCloudDao.findByIotDeviceId(spot.getDevice().getId());
            if (deviceCloudOp.isPresent()) {
                // 发送请求
                ServiceResult result = msDeviceService.deleteAccessRule(deviceCloudOp.get().getKsDeviceId());
                if (!result.isSuccess()) {
                    // 业务处理失败，手动回滚事务,返回异常
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return result;
                }
            }

            // 删除门禁点默认人员组
            accessControlSpotStaffGroupRelationDao.findByAccessControlSpotIdAndIsDefault(spotId, 1).forEach(it -> {
                it.setIsDelete(1);
                StaffGroup staffGroup = it.getStaffGroup();
                staffGroup.setIsDelete(1);
                // 旷世同步删除人员组
                staffGroupClient.deleteStaffGroup(cn.com.cowain.ibms.service.bean.DeviceType.KS.toString(), staffGroup.getId());
                // 删除门禁下人员
                staffGroupMembersDao.findByGroupId(staffGroup).forEach(member -> {
                    member.setIsDelete(1);
                    staffGroupMembersDao.save(member);
                });

                staffGroupDao.save(staffGroup);
                accessControlSpotStaffGroupRelationDao.save(it);
            });

            // 删除门禁点和人员组关系
            accessControlSpotStaffGroupRelationDao.findByAccessControlSpotId(spotId).forEach(it -> {
                it.setIsDelete(1);
                accessControlSpotStaffGroupRelationDao.save(it);
            });
        }

        // 删除门禁下通行记录
        List<AccessControlSpotAccessRecord> recordList = accessControlSpotAccessRecordDao.findByAccessControlSpotId(spot.getId());
        recordList.forEach(it -> it.setIsDelete(1));
        accessControlSpotAccessRecordDao.saveAll(recordList);

        // 删除门禁下人员
        spotUserDao.saveAll(spotUserList);

        // 删除门禁分配人关系
        accessControlSpotApproverRelationDao.findAllBySpotId(spotId).forEach(it -> {
            it.setIsDelete(1);
            accessControlSpotApproverRelationDao.save(it);
        });

        // 更新门禁点审批记录状态
        accessControlSpotApplicationRecordDao.findBySpotId(spotId).forEach(it -> {
            if (ApprovalStatus.PENDING.equals(it.getStatus())) {
                Optional<SysUser> sysUserOp = sysUserDao.findByEmpNo(it.getApplicantHrId());
                if (sysUserOp.isPresent() && StringUtils.isNotEmpty(sysUserOp.get().getWxOpenId())) {
                    MessagePoolRecord messagePoolRecord = new MessagePoolRecord();

                    messagePoolRecord.setMsgFromSys(8);
                    messagePoolRecord.setMsgType(3);
                    messagePoolRecord.setPriority(0);
                    messagePoolRecord.setSendType(0);
                    messagePoolRecord.setReceiveType(501);
                    messagePoolRecord.setMsgTitle("门禁通行权限申请结果");
                    MessageBody messageBody = new MessageBody();
                    messageBody.setTitle("您好，您申请的门禁通行权限已处理，请知悉");
                    messageBody.setType("通行权限分配");
                    messageBody.setDepart("已取消");
                    messageBody.setTime(DateUtils.formatLocalDateTime(LocalDateTime.now(), DateUtils.PATTERN_DATETIME1));
                    messageBody.setRemark("该门禁已被删除");
                    messagePoolRecord.setMsgContent(JSON.toJSONString(messageBody));

                    messagePoolRecord.setReceivePerson(sysUserOp.get().getWxOpenId());
                    messagePoolRecord.setReceivePersonId(sysUserOp.get().getWxOpenId());
                    messagePoolRecord.setMsgDetailsUrl(accessControlSpotApplicationClaimerUrl + it.getId());
                    messagePoolRecord.setMsgTemplateId(vxMsgTemplateId);
                    messagePoolRecord.setTipType(1);

                    messageSendService.send(messagePoolRecord);
                }

                it.setStatus(ApprovalStatus.CANCEL);
                it.setDeniedReason("门禁点不存在，系统自动取消");
                accessControlSpotApplicationRecordDao.save(it);
            }
        });

        // 删除门禁
        spot.setIsDelete(1);
        accessControlSpotDao.save(spot);

        List<OpenAbilityAccessControlSpot> spotList = openAbilityAccessControlSpotDao.findBySpotId(spot.getId());
        if (!CollectionUtils.isEmpty(spotList)) {
            for (OpenAbilityAccessControlSpot abilitSpot : spotList) {
                abilitSpot.setIsDelete(1);
                openAbilityAccessControlSpotDao.save(abilitSpot);
            }
        }
        return ServiceResult.ok();
    }

    /**
     * 获取门禁列表（分页）
     *
     * @param req 查询参数
     * @return 门禁列表
     * @author Yang.Lee
     * @date 2021/11/24 9:51
     **/
    @Override
    @Transactional
    public PageBean<AccessControlSpotPageResp> getPage(AccessControlSpotPageReq req) {

        Page<AccessControlSpot> accessControlSpotPage = accessControlSpotDao.findAll((root, query, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();

            if (StringUtils.isNotBlank(req.getProjectId())) {
                list.add(criteriaBuilder.equal(root.get("space").get("project").get("id"), req.getProjectId()));
            }

            if (StringUtils.isNotBlank(req.getSpaceId())) {
                list.add(criteriaBuilder.equal(root.get("space").get("id"), req.getSpaceId()));
            }

            if (StringUtils.isNotBlank(req.getName())) {
                String like = "%" + req.getName() + "%";
                list.add(criteriaBuilder.like(root.get("name"), like));
            }

            if (ObjectUtils.isNotEmpty(req.getType())) {
                list.add(criteriaBuilder.equal(root.get("accessControlSpotType"), req.getType()));
            }

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, PageRequest.of(req.getPage(), req.getSize(), Sort.by(req.getOrders())));


        PageBean<AccessControlSpotPageResp> pageBean = new PageBean<>();
        pageBean.setPage(accessControlSpotPage.getPageable().getPageNumber());
        pageBean.setSize(accessControlSpotPage.getPageable().getPageSize());
        pageBean.setTotalPages(accessControlSpotPage.getTotalPages());
        pageBean.setTotalElements(accessControlSpotPage.getTotalElements());


        List<AccessControlSpotPageResp> resps = new ArrayList<>();
        accessControlSpotPage.getContent().forEach(accessControlSpot -> {

            SpotStatus spotStatus = null;
            String spaceId = "";
            String deviceId = "";
            String deviceType = "";
            String deviceTypeName = "";
            String url = "access/apply?spotId=";

            if (null != accessControlSpot.getDevice() && !DeviceType.DOOR_MAGNETIC.equals(accessControlSpot.getDevice().getDeviceType())) {

                List<String> taskIds = new ArrayList<>();
                List<AccessControlSpotUserSyncTask> syncTaskList = accessControlSpotUserSyncTaskDao.findBySpotIdOrderByCreatedTimeDesc(accessControlSpot.getId());
                if (!CollectionUtils.isEmpty(syncTaskList)) {
                    taskIds.add(syncTaskList.get(0).getTask().getId());
                }

                if (!CollectionUtils.isEmpty(openAbilityTaskDetailDao.findByTaskIdInAndUserImportStatus(taskIds, UserImportStatus.DOING))) {
                    spotStatus = SpotStatus.DOING;
                } else if (!CollectionUtils.isEmpty(openAbilityTaskDetailDao.findByTaskIdInAndUserImportStatus(taskIds, UserImportStatus.FAILED))) {
                    spotStatus = SpotStatus.FAILED;
                } else {
                    spotStatus = SpotStatus.SUCCESS;
                }

            }
            if (null != accessControlSpot.getDevice()) {
                IotDevice device = accessControlSpot.getDevice();
                deviceId = device.getId();
                deviceType = String.valueOf(device.getDeviceType());
                if (DeviceType.FACE_RECOGNITION.equals(device.getDeviceType())) {
                    deviceType = String.valueOf(DeviceType.HIK_FACE_MACHINE);
                }
                deviceTypeName = device.getDeviceType().getName();
            }
            if (null != accessControlSpot.getSpace()) {
                spaceId = accessControlSpot.getSpace().getId();
            }

            resps.add(AccessControlSpotPageResp.builder()
                    .id(accessControlSpot.getId())
                    .name(accessControlSpot.getName())
                    .type(accessControlSpot.getAccessControlSpotType())
                    .typeName(Optional.ofNullable(accessControlSpot.getAccessControlSpotType()).map(AccessControlSpotType::getName).orElse(""))
                    .address(accessControlSpot.getSpace().getProject().getProjectName() + " / " + SpaceService.getFullSpaceName(accessControlSpot.getSpace(), accessControlSpot.getSpace().getName()) + " / " + accessControlSpot.getAddress())
                    .spaceId(spaceId)
                    .spaceName(accessControlSpot.getSpace().getName())
                    .projectId(accessControlSpot.getSpace().getProject().getId())
                    .projectName(accessControlSpot.getSpace().getProject().getProjectName())
                    .deviceId(deviceId)
                    .deviceType(deviceType)
                    .deviceTypeName(deviceTypeName)
                    .inOutName(accessControlSpot.getInOut() != null ? accessControlSpot.getInOut().getName() : "")
                    .url(environmentAddress + url + accessControlSpot.getId())
                    .spotStatus(spotStatus)
                    .build());
        });
        pageBean.setList(resps);

        return pageBean;
    }

    @Override
    public PageBean<AccessControlPageResp> userPage(AccessControlUserPageReq req) {

        PageBean<AccessControlPageResp> userPage = new PageBean<>();

        String realKey = redisUtil.createRealKey(STAFF_KEY);
        // 判断redis是否存在数据
        if (Boolean.TRUE.equals(redisUtil.hasKey(realKey))) {
            List<StaffPageResp> staffList = JSON.parseArray(String.valueOf(redisUtil.get(realKey)), StaffPageResp.class);

            List<StaffPageResp> collect = staffList;
            // 根据姓名或工号筛选
            if (StringUtils.isNotBlank(req.getKeyword())) {
                collect = collect.stream().
                        filter(staff -> staff.getEmpNo().indexOf(req.getKeyword()) > -1 || staff.getName().indexOf(req.getKeyword()) > -1)
                        .collect(Collectors.toList());
            }

            // 根据部门筛选
            /*if(StringUtils.isNotBlank(dept)){
                List<String> strings = new ArrayList<>();
                strings.add(dept);
                collect = collect.stream().filter(staff -> strings.contains(staff.getDept())).collect(Collectors.toList());
            }*/

            // 筛选是否员工或访客
            if (null != req.getUserType()) {
                List<Integer> integers = new ArrayList<>();
                integers.add(req.getUserType());
                collect = collect.stream().filter(staff -> integers.contains(staff.getIsStaff())).collect(Collectors.toList());
            }

            Collections.reverse(collect);
            PageBean<StaffPageResp> pageBeanByAllData = PageBean.createPageBeanByAllData(collect, req.getPage(), req.getSize());


            List<AccessControlPageResp> userPageResps = new ArrayList<>();
            pageBeanByAllData.getList().forEach(it -> {
                AccessControlPageResp userResp = new AccessControlPageResp();
                userResp.setUserId(it.getId());
                userResp.setName(it.getName());
                userResp.setEmpNo(it.getEmpNo());
                userResp.setFaceUrl("");
                userResp.setDept("");
                userResp.setUserType(it.getIsStaff());
                /*if (it.getIsStaff() == 1) {
                    userResp.setName(userResp.getName().substring(0, 1) + "*");
                    userResp.setEmpNo("**" + userResp.getEmpNo().substring(2));
                }*/

                // 获取员工部门
                if (it.getIsStaff() == 0) {
                    try {
                        SysUser userByEmpNo = sysUserServiceUC.getUserByEmpNo(it.getEmpNo());
                        userResp.setDept(userByEmpNo.getFullDepartmentName());
                    } catch (Exception e) {
                        log.error("工号:" + it.getEmpNo());
                        log.error("获取人员信息失败" + e.getMessage());
                    }
                }

                // 门禁数
                int accessControlNum = spotUserDao.countByUserHrId(it.getEmpNo());
                userResp.setAccessControlNum(accessControlNum);

                userPageResps.add(userResp);
            });

            userPage.setList(userPageResps);
            userPage.setPage(req.getPage());
            userPage.setSize(req.getSize());
            userPage.setTotalElements(pageBeanByAllData.getTotalElements());
            userPage.setTotalPages(pageBeanByAllData.getTotalPages());
            return userPage;
        }

        userPage.setList(new ArrayList<>());
        return userPage;
    }

    @Override
    @Transactional
    public ServiceResult updateDevice(String id, String deviceId) {

        Optional<AccessControlSpot> spotOp = accessControlSpotDao.findById(id);
        if (spotOp.isEmpty()) {
            return ServiceResult.error("绑定失败,门禁点不存在");
        }
        AccessControlSpot accessControlSpot = spotOp.get();
        if (null != accessControlSpot.getDevice()) {
            return ServiceResult.error("请先解除设备绑定");
        }

        Optional<IotDevice> deviceOp = deviceDao.findById(deviceId);
        if (deviceOp.isEmpty()) {
            return ServiceResult.error("绑定失败,设备不存在");
        }
        IotDevice device = deviceOp.get();


        /*// 门禁类型
        if (DeviceType.HIK_FACE_MACHINE.equals(device.getDeviceType())) {
            accessControlSpot.setAccessControlSpotType(AccessControlSpotType.FACE_MACHINE);
        } else if (DeviceType.FACE_RECOGNITION.equals(device.getDeviceType())) {

            // 绑定旷视设备创建人员组
            List<AccessControlSpotStaffGroupRelation> list = accessControlSpotStaffGroupRelationDao.findByAccessControlSpotIdAndIsDefault(accessControlSpot.getId(), 1);
            if (list.isEmpty()) {
                // 判断人员组名称是否已被使用
                Optional<StaffGroup> op = staffGroupDao.findByName(accessControlSpot.getName() + "人员组");
                if (op.isEmpty()) {
                    // 保存人员组
                    StaffGroup staffGroup = new StaffGroup();
                    staffGroup.setName(accessControlSpot.getName() + "人员组");
                    staffGroup.setCount(0);
                    // 获取人员组排序最大值
                    Integer maxSeq = staffGroupDao.findMaxSeq();
                    if (maxSeq != null) {
                        maxSeq = maxSeq + 1;
                    } else {
                        maxSeq = 1;
                    }
                    staffGroup.setSeq(maxSeq);
                    // 保存
                    staffGroupDao.save(staffGroup);

                    // 向旷视增加人员组
                    try {
                        UserGroupReq userGroupReq = new UserGroupReq();
                        userGroupReq.setId(staffGroup.getId());
                        userGroupReq.setName(staffGroup.getName());
                        userGroupReq.setType(1);
                        // 向ms-device增加人员组
                        log.info("旷世新增人员组： " + userGroupReq);
                        String s = staffGroupClient.save(cn.com.cowain.ibms.service.bean.DeviceType.KS.toString(), userGroupReq);
                        JSONObject jsonResult = JSON.parseObject(s);
                        if (jsonResult.getInteger("status") != 1) {
                            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                            return ServiceResult.error(jsonResult.get("msg"));
                        }
                    } catch (Exception e) {
                        log.error(e.getMessage());
                        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                        return ServiceResult.error("旷世新增人员组失败");
                    }

                    // 保存门禁点与人员组
                    AccessControlSpotStaffGroupRelation accessControlSpotStaffGroupRelation = new AccessControlSpotStaffGroupRelation();
                    accessControlSpotStaffGroupRelation.setAccessControlSpot(accessControlSpot);
                    accessControlSpotStaffGroupRelation.setStaffGroup(staffGroup);
                    accessControlSpotStaffGroupRelation.setIsDefault(1);
                    accessControlSpotStaffGroupRelationDao.save(accessControlSpotStaffGroupRelation);
                } else {
                    return ServiceResult.error("人员组已存在,请更改门禁点名称");
                }
            }

            accessControlSpot.setAccessControlSpotType(AccessControlSpotType.FACE_MACHINE);
        } else if (DeviceType.DOOR_MAGNETIC.equals(device.getDeviceType())) {
            accessControlSpot.setAccessControlSpotType(AccessControlSpotType.DOOR_MAGNETIC);
        } else {
            accessControlSpot.setAccessControlSpotType(AccessControlSpotType.OTHERS);
        }

        // 更新门禁点权限

        // 向新设备添加权限
        // 向海康设备添加人员权限
        if (DeviceType.HIK_FACE_MACHINE.equals(device.getDeviceType())) {

            // 开始时间 结束时间
            LocalDateTime startTime = LocalDateTime.now().withSecond(0).withNano(0);
            LocalDateTime endTime = LocalDateTime.now().withSecond(0).withNano(0).plusYears(5);

            // 获取门禁点人员
            List<String> jobNos = new ArrayList<>();
            List<AccessControlSpotUser> spotUserList = spotUserDao.findByAccessControlSpotId(accessControlSpot.getId());
            spotUserList.forEach(it -> {
                jobNos.add(it.getUserHrId());
                it.setAccessStartTime(startTime);
                it.setAccessEndTime(endTime);
                spotUserDao.save(it);
            });

            HkUserAddReq hkUserAddReq = new HkUserAddReq();
            hkUserAddReq.setJobNum(jobNos);
            hkUserAddReq.setDeviceId(deviceId);
            hkUserAddReq.setStartTime(startTime);
            hkUserAddReq.setEndTime(endTime);
            // 指定设备添加人员
            ServiceResult result = accessRulesService.addHkUser(hkUserAddReq);
            if (!result.isSuccess()) {
                // 业务处理失败，手动回滚事务,返回异常
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error(String.valueOf(result.getObject()));
            }
        } else if (DeviceType.FACE_RECOGNITION.equals(device.getDeviceType())) {

            // 向旷视添加人员
            Optional<DeviceCloud> deviceCloudOp = deviceCloudDao.findByIotDeviceId(device.getId());
            if (deviceCloudOp.isPresent()) {
                // 获取门禁点下人员组
                List<String> newGroupUuidList = new ArrayList<>();
                List<AccessControlSpotStaffGroupRelation> list = accessControlSpotStaffGroupRelationDao.findByAccessControlSpotId(accessControlSpot.getId());
                list.forEach(it -> newGroupUuidList.add(it.getStaffGroup().getId()));
                AccessRuleUpdateReq accessRuleUpdateReq = new AccessRuleUpdateReq();
                accessRuleUpdateReq.setNewGroupUuidList(newGroupUuidList);
                accessRuleUpdateReq.setDeviceUuid(deviceCloudOp.get().getKsDeviceId());
                // 发送请求
                ServiceResult result = msDeviceService.updateAccessRule(accessRuleUpdateReq);
                if (!result.isSuccess()) {
                    // 业务处理失败，手动回滚事务,返回异常
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return result;
                }
            }
        }*/


        //创建task
        List<IotDevice> deviceList = new ArrayList<>();
        deviceList.add(device);
        //所有添加人员的OpenAbilityMessage.User
        List<OpenAbilityMessage.User> openAbilityMessageUserList = new ArrayList<>();
        List<AccessControlSpotUser> spotUserList = spotUserDao.findByAccessControlSpotId(accessControlSpot.getId());
        spotUserList.forEach(spotUser -> {
            OpenAbilityMessage.User user = new OpenAbilityMessage.User();
            user.setName(spotUser.getUserName());
            user.setHrId(spotUser.getUserHrId());
            user.setStartTime(spotUser.getAccessStartTime());
            user.setEndTime(spotUser.getAccessEndTime());
            openAbilityMessageUserList.add(user);
        });
        OpenAbilityTask task = createTask(Ability.ADMINISTRATOR_ADD, openAbilityMessageUserList, deviceList, TaskType.MQ, MethodType.ADDBATCH);

        // 保存门禁点用户同步任务表
        AccessControlSpotUserSyncTask accessControlSpotUserSyncTask = new AccessControlSpotUserSyncTask();
        accessControlSpotUserSyncTask.setTask(task);
        accessControlSpotUserSyncTask.setSpot(accessControlSpot);
        accessControlSpotUserSyncTaskDao.save(accessControlSpotUserSyncTask);

        // 绑定设备
        accessControlSpot.setDevice(device);
        accessControlSpot.setAccessControlSpotType(getAccessControlSpotType(device));
        accessControlSpotDao.save(accessControlSpot);

        //创建需要发送mq的消息
        AccessPermissions message = new AccessPermissions();

        if (DeviceType.HIK_FACE_MACHINE.equals(device.getDeviceType())) {
            List<AccessPermissions.User> userList = new ArrayList<>();
            spotUserList.forEach(spotUser -> {
                AccessPermissions.User user = new AccessPermissions.User();
                user.setName(spotUser.getUserName());
                user.setJobNo(spotUser.getUserHrId());
                user.setStartTime(DateUtils.formatZoneDateTime(spotUser.getAccessStartTime().atZone(ZoneId.of(DateUtils.GMT8)), DateUtils.PATTERN_DATETIME_WITH_ZONE));
                user.setEndTime(DateUtils.formatZoneDateTime(spotUser.getAccessEndTime().atZone(ZoneId.of(DateUtils.GMT8)), DateUtils.PATTERN_DATETIME_WITH_ZONE));
                user.setPersonType(AccessPermissions.PersonType.EMPLOYEE);
                userList.add(user);
            });
            message.setUserList(userList);
            message.setMethodName(AccessPermissions.Method.BIND.getName());
        } else if (DeviceType.FACE_RECOGNITION.equals(device.getDeviceType())) {
            List<AccessControlSpotKSHTMemberGroup> coverGroupList = accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotId(accessControlSpot.getId());
            List<AccessPermissions.User> userList = new ArrayList<>();
            List<String> groupIds = new ArrayList<>();
            if (!CollectionUtils.isEmpty(coverGroupList)) {
                List<AccessControlSpotKSHTMemberGroup> groupList = accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotId(
                        accessControlSpot.getId());
                groupList.forEach(group -> groupIds.add(group.getId()));
            } else {

                // 创建矿石人员组
                Optional<OpenAbility> openAbilityOptional = openAbilityDao.findByAbility(Ability.OTHER);
                OpenAbility openAbility;
                if (openAbilityOptional.isEmpty()) {
                    openAbility = new OpenAbility();
                    openAbility.setAbility(Ability.OTHER);
                    openAbility.setDescribe("其它");
                    openAbilityDao.save(openAbility);
                } else {
                    openAbility = openAbilityOptional.get();
                }
                AccessControlSpotKSHTMemberGroup memberGroup = new AccessControlSpotKSHTMemberGroup();
                memberGroup.setGroupType(KSHTMemberGroupType.STAFF);
                memberGroup.setGroupName(accessControlSpot.getName() + "_" + memberGroup.getGroupType().getName());
                memberGroup.setAccessControlSpot(accessControlSpot);
                memberGroup.setOpenAbility(openAbility);
                accessControlSpotKSHTMemberGroupDao.save(memberGroup);
                CreateHTMemberGroupReq.Param param = CreateHTMemberGroupReq.Param.builder()
                        .id(memberGroup.getId())
                        .type(memberGroup.getGroupType().getCode())
                        .name(memberGroup.getGroupName())
                        .build();
                // 向iotc发送创建员工组消息
                ServiceResult result = iotcService.createStaffHTMemberGroup(CreateHTMemberGroupReq.builder().groupList(Lists.newArrayList(param)).build());
                if (!result.isSuccess()) {
                    log.warn("创建旷世人员组:{}失败", param.getName());
                    memberGroup.setGroupName(memberGroup.getGroupName() + RandomCode.getRandom(3));
                    accessControlSpotKSHTMemberGroupDao.save(memberGroup);
                    param.setName(memberGroup.getGroupName());
                    // 以防旷世已存在重名的人员组，修改人员组名后，再次向iotc发送创建员工组消息
                    ServiceResult result2 = iotcService.createStaffHTMemberGroup(CreateHTMemberGroupReq.builder().groupList(Lists.newArrayList(param)).build());
                    if (!result2.isSuccess()) {
                        log.warn("创建旷世人员组:{}失败", param.getName());
                        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                        return result2;
                    }
                }
                // 像iotc发送创建员工组消息
                PermisssionReq permisssionReq = new PermisssionReq();
                String[] groupUuidList = new String[]{memberGroup.getId()};
                String[] deviceUuidList = new String[]{memberGroup.getAccessControlSpot().getDevice().getSn()};
                groupIds.add(memberGroup.getId());
                permisssionReq.setDeviceUuidList(deviceUuidList);
                permisssionReq.setGroupUuidList(groupUuidList);
                permisssionReq.setTimePlanId(timePlan);
                result = iotcConnectService.bindingDevice(permisssionReq, "2", "KS");
                //失败
                if (!result.isSuccess()) {
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return result;
                }

            }
            spotUserList.forEach(spotUser -> {
                AccessPermissions.User user = new AccessPermissions.User();
                user.setName(spotUser.getUserName());
                user.setJobNo(spotUser.getUserHrId());
                user.setCreateTime(DateUtils.formatZoneDateTime(spotUser.getAccessStartTime().atZone(ZoneId.of(DateUtils.GMT8)), DateUtils.PATTERN_DATETIME_WITH_ZONE));
                user.setEndTime(DateUtils.formatZoneDateTime(spotUser.getAccessEndTime().atZone(ZoneId.of(DateUtils.GMT8)), DateUtils.PATTERN_DATETIME_WITH_ZONE));
                user.setPersonType(AccessPermissions.PersonType.EMPLOYEE);
                user.setGroupIds(groupIds);
                userList.add(user);
            });
            message.setUserList(userList);
            message.setMethodName(AccessPermissions.Method.INCREMENT_BIND.getName());
        }
//        else if(DeviceType.FACE_RECOGNITION.equals(device.getDeviceType())){
//
//        }
        message.setResourceId(task.getId());
        message.setReturnTopic(Topic.get(Topic.USER_BIND_RETURN));
        message.setSourceName("IBMS");
        message.setModuleName(AccessPermissions.Model.PERSON.getName());
        List<String> deviceCodeList = new ArrayList<>();
        deviceCodeList.add(device.getSn());
        message.setDeviceCodeList(deviceCodeList);
        message.setOver("1");

        log.debug("发送mq员工参数{}", message);
        iotcService.bindUserWithDevices(message);

        return ServiceResult.ok(task.getId());
    }

    /**
     * 获取门禁点绑定了设备的门禁类型
     *
     * @param device
     * @return
     */
    private AccessControlSpotType getAccessControlSpotType(IotDevice device) {
        AccessControlSpotType accessControlSpotType;
        if (DeviceType.HIK_FACE_MACHINE.equals(device.getDeviceType())) {
            accessControlSpotType = AccessControlSpotType.FACE_MACHINE;
        } else if (DeviceType.FACE_RECOGNITION.equals(device.getDeviceType())) {
            accessControlSpotType = AccessControlSpotType.FACE_MACHINE;
        } else if (DeviceType.DOOR_MAGNETIC.equals(device.getDeviceType())) {
            accessControlSpotType = AccessControlSpotType.DOOR_MAGNETIC;
        } else {
            accessControlSpotType = AccessControlSpotType.OTHERS;
        }
        return accessControlSpotType;
    }

    @Override
    @Transactional
    public ServiceResult createHK(AccessControlHkCreateReq req) {

        if (req.getUsers().size() > 800) {
            return ServiceResult.error("添加失败,一次最多800人");
        }

        /*ServiceResult hkResult = hkIpsResult(req.getEndTime(), req.getStartTime(), req.getDeviceIds());
        if (!hkResult.isSuccess()) {
            return ServiceResult.error(hkResult.getObject());
        }*/

        // 比较通行时间
        if (req.getStartTime().isAfter(req.getEndTime())) {
            return ServiceResult.error("请输入正确的通行时间");
        }

        /*// 向海康发送的人员集合
        List<String> empNos = new ArrayList<>();
        req.getUsers().forEach(it -> empNos.add(it.getEmpNo()));
        List<AuthConfigData> data = new ArrayList<>();
        for (String empNo : empNos) {
            AuthConfigData authConfigData = new AuthConfigData();
            authConfigData.setJobNo(empNo);
            authConfigData.setStartTime(req.getStartTime() + ":00+08:00");
            authConfigData.setEndTime(req.getEndTime().toString() + ":59+08:00");
            data.add(authConfigData);
        }

        // 下发失败人员
        List<String> errorList = new ArrayList<>();
        // 向海康批量新增人员
        UserAddReq userAddReq = new UserAddReq();
        userAddReq.setData(data);
        userAddReq.setOver("1");

        HkIpsResp hkIps = (HkIpsResp) hkResult.getObject();
        try {
            JSONObject jsonObjectKs = getHkJSONObject(hkIps.getKsIps(), errorList, userAddReq, hkUserAddMoreKsUrl, empNos);
            if (jsonObjectKs != null && "-1".equals(jsonObjectKs.getString("code"))) {
                return ServiceResult.error("当前正有海康任务进行,请稍后操作");
            }
            JSONObject jsonObjectNt = getHkJSONObject(hkIps.getNtIps(), errorList, userAddReq, hkUserAddMoreNtUrl, empNos);
            if (jsonObjectNt != null && "-1".equals(jsonObjectNt.getString("code"))) {
                return ServiceResult.error("当前正有海康任务进行,请稍后操作");
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ServiceResult.error("海康下发人员失败");
        }
        if (!errorList.isEmpty()) {
            String errorUser = "";
            for (String user : errorList) {
                errorUser += user + ",";
            }
            return ServiceResult.error("失败人员:" + errorUser.substring(0, errorUser.length() - 1), "2");
        }*/

        //创建task
        List<IotDevice> deviceList = new ArrayList<>();
        for (String deviceId : req.getDeviceIds()) {
            Optional<IotDevice> deviceOp = deviceDao.findById(deviceId);
            if (deviceOp.isEmpty()) {
                return ServiceResult.error("设备ID有误,设备不存在");
            }
            deviceList.add(deviceOp.get());
        }

        //所有添加人员的OpenAbilityMessage.User
        List<OpenAbilityMessage.User> openAbilityMessageUserList = new ArrayList<>();
        req.getUsers().forEach(userReq -> {
            OpenAbilityMessage.User user = new OpenAbilityMessage.User();
            user.setName(userReq.getName());
            user.setHrId(userReq.getEmpNo());
            user.setStartTime(req.getStartTime());
            user.setEndTime(req.getEndTime());
            openAbilityMessageUserList.add(user);
        });
        OpenAbilityTask task = createTask(Ability.ADMINISTRATOR_ADD, openAbilityMessageUserList, deviceList, TaskType.MQ, MethodType.ADDBATCH);


        // HK门禁点
        List<AccessControlSpot> hkSpotList = new ArrayList<>();
        // KS门禁点
        List<AccessControlSpot> ksSpotList = new ArrayList<>();

        for (String accessControlSpotId : req.getAccessControlIds()) {
            Optional<AccessControlSpot> spotOp = accessControlSpotDao.findById(accessControlSpotId);
            if (spotOp.isEmpty()) {
                // 业务处理失败，手动回滚事务,返回异常
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error("门禁点ID有误,门禁不存在");
            }
            AccessControlSpot spot = spotOp.get();
            if (null == spot.getDevice()) {
                // 业务处理失败，手动回滚事务,返回异常
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error("添加失败,门禁未绑定设备");
            }

            // 保存门禁点用户同步任务表
            AccessControlSpotUserSyncTask accessControlSpotUserSyncTask = new AccessControlSpotUserSyncTask();
            accessControlSpotUserSyncTask.setTask(task);
            accessControlSpotUserSyncTask.setSpot(spot);
            accessControlSpotUserSyncTaskDao.save(accessControlSpotUserSyncTask);

            req.getUsers().forEach(user -> {
                List<AccessControlSpotUser> userList = spotUserDao.findByAccessControlSpotIdAndUserHrId(spot.getId(), user.getEmpNo());

                if (userList.isEmpty()) {
                    AccessControlSpotUser accessControlSpotUser = new AccessControlSpotUser();
                    accessControlSpotUser.setAccessControlSpot(spot);
                    accessControlSpotUser.setAccessStartTime(req.getStartTime());
                    accessControlSpotUser.setAccessEndTime(req.getEndTime());
                    accessControlSpotUser.setUserHrId(user.getEmpNo());
                    accessControlSpotUser.setAbilityName(Ability.ADMINISTRATOR_ADD.getName());
                    accessControlSpotUser.setUserName(user.getName());
                    spotUserDao.save(accessControlSpotUser);
                } else {
                    userList.forEach(obj -> {
                        Optional<AccessControlSpotUser> spotUserOp = Optional.ofNullable(obj);
                        if (spotUserOp.isEmpty()) {
                            AccessControlSpotUser accessControlSpotUser = new AccessControlSpotUser();
                            accessControlSpotUser.setAccessControlSpot(spot);
                            accessControlSpotUser.setAccessStartTime(req.getStartTime());
                            accessControlSpotUser.setAccessEndTime(req.getEndTime());
                            accessControlSpotUser.setUserHrId(user.getEmpNo());
                            accessControlSpotUser.setUserName(user.getName());
                            accessControlSpotUser.setAbilityName(Ability.ADMINISTRATOR_ADD.getName());
                            spotUserDao.save(accessControlSpotUser);
                        } else {
                            AccessControlSpotUser spotUser = spotUserOp.get();
                            spotUser.setAccessStartTime(req.getStartTime());
                            spotUser.setAccessEndTime(req.getEndTime());
                            spotUser.setAbilityName(Ability.ADMINISTRATOR_ADD.getName());
                            spotUserDao.save(spotUser);
                        }
                    });
                }
            });

            if (DeviceType.HIK_FACE_MACHINE.equals(spot.getDevice().getDeviceType())) {
                hkSpotList.add(spot);
            } else if (DeviceType.FACE_RECOGNITION.equals(spot.getDevice().getDeviceType())) {
                ksSpotList.add(spot);
            }
        }

        if (!CollectionUtils.isEmpty(hkSpotList)) {
            List<AccessPermissions.User> userList = new ArrayList<>();
            req.getUsers().forEach(userReq -> {
                AccessPermissions.User user = new AccessPermissions.User();
                user.setName(userReq.getName());
                user.setJobNo(userReq.getEmpNo());
                user.setStartTime(DateUtils.formatZoneDateTime(req.getStartTime().atZone(ZoneId.of(DateUtils.GMT8)), DateUtils.PATTERN_DATETIME_WITH_ZONE));
                user.setEndTime(DateUtils.formatZoneDateTime(req.getEndTime().atZone(ZoneId.of(DateUtils.GMT8)), DateUtils.PATTERN_DATETIME_WITH_ZONE));
                user.setPersonType(AccessPermissions.PersonType.EMPLOYEE);
                userList.add(user);
            });

            //创建需要发送mq的消息
            AccessPermissions message = new AccessPermissions();
            message.setUserList(userList);
            message.setMethodName(AccessPermissions.Method.BIND.getName());

            message.setResourceId(task.getId());
            message.setReturnTopic(Topic.get(Topic.USER_BIND_RETURN));
            message.setSourceName("IBMS");
            message.setModuleName(AccessPermissions.Model.PERSON.getName());
            List<String> deviceCodeList = new ArrayList<>();
            hkSpotList.forEach(hkSpot -> deviceCodeList.add(hkSpot.getDevice().getSn()));
            message.setDeviceCodeList(deviceCodeList);
            message.setOver("1");

            log.debug("发送mq员工参数{}", message);
            iotcService.bindUserWithDevices(message);
        } else if (!CollectionUtils.isEmpty(ksSpotList)) {

            // 设备code
            List<String> deviceCodeList = new ArrayList<>();

            // 人员信息
            List<AccessPermissions.User> userList = new ArrayList<>();

            // 人员组ID
            List<String> groupIds = new ArrayList<>();

            for (AccessControlSpot spot : ksSpotList) {
                deviceCodeList.add(spot.getDevice().getSn());
                List<AccessControlSpotKSHTMemberGroup> coverGroupList = accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotId(spot.getId());
                if (!CollectionUtils.isEmpty(coverGroupList)) {
                    List<AccessControlSpotKSHTMemberGroup> groupList = accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotId(
                            spot.getId());
                    groupList.forEach(group -> groupIds.add(group.getId()));
                } else {

                    // 创建矿石人员组
                    Optional<OpenAbility> openAbilityOptional = openAbilityDao.findByAbility(Ability.OTHER);
                    OpenAbility openAbility;
                    if (openAbilityOptional.isEmpty()) {
                        openAbility = new OpenAbility();
                        openAbility.setAbility(Ability.OTHER);
                        openAbility.setDescribe("其它");
                        openAbilityDao.save(openAbility);
                    } else {
                        openAbility = openAbilityOptional.get();
                    }
                    AccessControlSpotKSHTMemberGroup memberGroup = new AccessControlSpotKSHTMemberGroup();
                    memberGroup.setGroupType(KSHTMemberGroupType.STAFF);
                    memberGroup.setGroupName(spot.getName() + "_" + memberGroup.getGroupType().getName());
                    memberGroup.setAccessControlSpot(spot);
                    memberGroup.setOpenAbility(openAbility);
                    accessControlSpotKSHTMemberGroupDao.save(memberGroup);
                    CreateHTMemberGroupReq.Param param = CreateHTMemberGroupReq.Param.builder()
                            .id(memberGroup.getId())
                            .type(memberGroup.getGroupType().getCode())
                            .name(memberGroup.getGroupName())
                            .build();
                    // 向iotc发送创建员工组消息
                    ServiceResult result = iotcService.createStaffHTMemberGroup(CreateHTMemberGroupReq.builder().groupList(Lists.newArrayList(param)).build());
                    if (!result.isSuccess()) {
                        log.warn("创建旷世人员组:{}失败", param.getName());
                        memberGroup.setGroupName(memberGroup.getGroupName() + RandomCode.getRandom(3));
                        accessControlSpotKSHTMemberGroupDao.save(memberGroup);
                        param.setName(memberGroup.getGroupName());
                        // 以防旷世已存在重名的人员组，修改人员组名后，再次向iotc发送创建员工组消息
                        ServiceResult result2 = iotcService.createStaffHTMemberGroup(CreateHTMemberGroupReq.builder().groupList(Lists.newArrayList(param)).build());
                        if (!result2.isSuccess()) {
                            log.warn("创建旷世人员组:{}失败", param.getName());
                            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                            return result2;
                        }
                    }
                    // 像iotc发送创建员工组消息
                    PermisssionReq permisssionReq = new PermisssionReq();
                    String[] groupUuidList = new String[]{memberGroup.getId()};
                    String[] deviceUuidList = new String[]{memberGroup.getAccessControlSpot().getDevice().getSn()};
                    groupIds.add(memberGroup.getId());
                    permisssionReq.setDeviceUuidList(deviceUuidList);
                    permisssionReq.setGroupUuidList(groupUuidList);
                    permisssionReq.setTimePlanId(timePlan);
                    result = iotcConnectService.bindingDevice(permisssionReq, "2", "KS");
                    //失败
                    if (!result.isSuccess()) {
                        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                        return result;
                    }

                }
            }

            req.getUsers().forEach(userReq -> {
                AccessPermissions.User user = new AccessPermissions.User();
                user.setName(userReq.getName());
                user.setJobNo(userReq.getEmpNo());
                user.setCreateTime(DateUtils.formatZoneDateTime(req.getStartTime().atZone(ZoneId.of(DateUtils.GMT8)), DateUtils.PATTERN_DATETIME_WITH_ZONE));
                user.setEndTime(DateUtils.formatZoneDateTime(req.getEndTime().atZone(ZoneId.of(DateUtils.GMT8)), DateUtils.PATTERN_DATETIME_WITH_ZONE));
                user.setPersonType(AccessPermissions.PersonType.EMPLOYEE);
                user.setGroupIds(groupIds);
                userList.add(user);
            });

            //创建需要发送mq的消息
            AccessPermissions message = new AccessPermissions();

            message.setUserList(userList);
            message.setMethodName(AccessPermissions.Method.INCREMENT_BIND.getName());
            message.setResourceId(task.getId());
            message.setReturnTopic(Topic.get(Topic.USER_BIND_RETURN));
            message.setSourceName("IBMS");
            message.setModuleName(AccessPermissions.Model.PERSON.getName());
            message.setDeviceCodeList(deviceCodeList);
            message.setOver("1");

            log.debug("发送mq员工参数{}", message);
            iotcService.bindUserWithDevices(message);
        }

        /*// 获取所有门禁点
        List<AccessControlSpot> spotList = accessControlSpotDao.findByIdIn(req.getAccessControlIds());
        // 保存人员至门禁点
        spotList.forEach(it -> req.getUsers().forEach(user -> {
            List<AccessControlSpotUser> userList = spotUserDao.findByAccessControlSpotIdAndUserHrId(it.getId(), user.getEmpNo());

            if (userList.isEmpty()) {
                AccessControlSpotUser accessControlSpotUser = new AccessControlSpotUser();
                accessControlSpotUser.setAccessControlSpot(it);
                accessControlSpotUser.setAccessStartTime(req.getStartTime());
                accessControlSpotUser.setAccessEndTime(req.getEndTime());
                accessControlSpotUser.setUserHrId(user.getEmpNo());
                accessControlSpotUser.setUserName(user.getName());
                spotUserDao.save(accessControlSpotUser);
            } else {
                userList.forEach(obj -> {
                    Optional<AccessControlSpotUser> spotUserOp = Optional.ofNullable(obj);
                    if (spotUserOp.isEmpty()) {
                        AccessControlSpotUser accessControlSpotUser = new AccessControlSpotUser();
                        accessControlSpotUser.setAccessControlSpot(it);
                        accessControlSpotUser.setAccessStartTime(req.getStartTime());
                        accessControlSpotUser.setAccessEndTime(req.getEndTime());
                        accessControlSpotUser.setUserHrId(user.getEmpNo());
                        accessControlSpotUser.setUserName(user.getName());
                        spotUserDao.save(accessControlSpotUser);
                    } else {
                        AccessControlSpotUser spotUser = spotUserOp.get();
                        spotUser.setAccessStartTime(req.getStartTime());
                        spotUser.setAccessEndTime(req.getEndTime());
                        spotUserDao.save(spotUser);
                    }
                });
            }
        }));*/


        return ServiceResult.ok();
    }

    @Override
    public PageBean<AccessControlPageResp> spotUserPage(SpotUserPageReq req) {

        // 创建分页查询对象
        req.addOrderDesc(Sorts.CREATED_TIME);
        Sort sort = Sort.by(req.getOrders());
        Pageable pageable = PageRequest.of(req.getPage(), req.getSize(), sort);
        Page<AccessControlSpotUser> spotUserPage = spotUserDao.findAll((root, criteriaQuery, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();

            // 门禁点
            predicates.add(criteriaBuilder.equal(root.get("accessControlSpot").get("id"), req.getSpotId()));

            // 人员姓名工号
            if (StringUtils.isNotBlank(req.getKeyword())) {
                String like = "%" + req.getKeyword() + "%";
                predicates.add(criteriaBuilder.or(
                        criteriaBuilder.like(root.get("userHrId"), like),
                        criteriaBuilder.like(root.get("userName"), like)
                ));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        }, pageable);

        // 门禁点人员列表
        PageBean<AccessControlPageResp> pageBean = new PageBean<>();

        // 门禁点任务ID
        List<String> taskIDs = new ArrayList<>();
        accessControlSpotUserSyncTaskDao.findBySpotId(req.getSpotId()).forEach(spotUserSyncTask -> taskIDs.add(spotUserSyncTask.getTask().getId()));

        List<AccessControlPageResp> accessControlPageResps = new ArrayList<>();
        spotUserPage.getContent().forEach(it -> {
            AccessControlPageResp accessControlPageResp = new AccessControlPageResp();
            accessControlPageResp.setUserId(it.getId());
            accessControlPageResp.setEmpNo(it.getUserHrId());
            accessControlPageResp.setName(it.getUserName());
            accessControlPageResp.setStartTime(it.getAccessStartTime());
            accessControlPageResp.setEndTime(it.getAccessEndTime());
            openAbilityTaskDetailDao.findByTaskIdInAndUserHrId(taskIDs, it.getUserHrId()).forEach(taskDetail -> accessControlPageResp.setSource(taskDetail.getTask().getAbility().getName()));
            try {
                SysUser userByEmpNo = sysUserServiceUC.getUserByEmpNo(it.getUserHrId());
                accessControlPageResp.setDept(userByEmpNo.getDeptName());
            } catch (Exception e) {
                log.error("工号:" + it.getUserHrId());
                log.error("获取人员信息失败" + e.getMessage());
            }
            accessControlPageResp.setTimePlan(accessControlTimePlanService.getAccessControlTimePlan(it.getAccessControlSpot().getId(), it.getUserHrId()));
            accessControlPageResps.add(accessControlPageResp);
        });
        pageBean.setList(accessControlPageResps);
        pageBean.setPage(req.getPage());
        pageBean.setSize(req.getSize());
        pageBean.setTotalPages(spotUserPage.getTotalPages());
        pageBean.setTotalElements(spotUserPage.getTotalElements());

        return pageBean;
    }

    @Override
    public PageBean<AccessControlSpotPageResp> userSpotPage(String empNo, PageReq req) {

        // 创建分页查询对象
        req.addOrderDesc(Sorts.CREATED_TIME);
        Sort sort = Sort.by(req.getOrders());
        Pageable pageable = PageRequest.of(req.getPage(), req.getSize(), sort);

        // 获取人员门禁点分页
        Page<AccessControlSpotUser> spotUserPage = spotUserDao.findByUserHrId(empNo, pageable);

        List<AccessControlSpotPageResp> accessControlSpotPageResps = new ArrayList<>();
        spotUserPage.getContent().forEach(it -> {
            AccessControlSpotPageResp resp = new AccessControlSpotPageResp();
            AccessControlSpot accessControlSpot = it.getAccessControlSpot();
            resp.setId(accessControlSpot.getId());
            resp.setName(accessControlSpot.getName());
            resp.setType(Optional.ofNullable(accessControlSpot.getAccessControlSpotType()).orElse(null));
            resp.setTypeName(Optional.ofNullable(accessControlSpot.getAccessControlSpotType()).map(AccessControlSpotType::getName).orElse(""));
            resp.setAddress(accessControlSpot.getAddress());
            resp.setDeviceId(Optional.ofNullable(accessControlSpot.getDevice()).map(device -> device.getId()).orElse(""));
            resp.setStartTime(it.getAccessStartTime());
            resp.setEndTime(it.getAccessEndTime());

            accessControlSpotPageResps.add(resp);
        });


        PageBean<AccessControlSpotPageResp> pageBean = new PageBean<>();
        pageBean.setList(accessControlSpotPageResps);
        pageBean.setPage(req.getPage());
        pageBean.setSize(req.getSize());
        pageBean.setTotalPages(spotUserPage.getTotalPages());
        pageBean.setTotalElements(spotUserPage.getTotalElements());
        return pageBean;
    }

    @Override
    @Transactional
    public ServiceResult updateUserTime(AccessControlUpdateTimeReq req) {

        // 根据面板机和工号获取通行人员信息
        List<AccessControlSpotUser> spotUserOp = spotUserDao.findByAccessControlSpotIdAndUserHrId(req.getId(), req.getEmpNo());
        if (spotUserOp.isEmpty()) {
            return ServiceResult.error("该门禁点下无此员工");
        }

        for (AccessControlSpotUser spotUser : spotUserOp) {

            // 向HK更新人员通行时间
            if (null != spotUser.getAccessControlSpot().getDevice() && DeviceType.HIK_FACE_MACHINE.equals(spotUser.getAccessControlSpot().getDevice().getDeviceType())) {

                UserTimeReq userTimeReq = new UserTimeReq();
                userTimeReq.setJobNo(req.getEmpNo());
                userTimeReq.setStartTime(req.getStartTime());
                userTimeReq.setEndTime(req.getEndTime());
                userTimeReq.setDeviceId(spotUser.getAccessControlSpot().getDevice().getId());

                String requestParam = JSON.toJSONString(userTimeReq);
                log.debug("向HK更新人员通行时间json参数 ： " + requestParam);

                ServiceResult result = accessRulesService.updateUserHkTime(userTimeReq);
                if (!result.isSuccess()) {
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return ServiceResult.error(String.valueOf(result.getObject()));
                }
            }/*else if(DeviceType.FACE_RECOGNITION.equals(spotUser.getAccessControlSpot().getDevice().getDeviceType())){
            // 向KS更新人员通行时间
        }*/

            // 更改门禁点人员信息
            spotUser.setAccessStartTime(req.getStartTime());
            spotUser.setAccessEndTime(req.getEndTime());
            spotUserDao.save(spotUser);
        }

        return ServiceResult.ok();
    }

    @Override
    @Transactional
    public ServiceResult deleteUserTime(String id, String empNo) {

        // 根据面板机和工号获取通行人员信息
        List<AccessControlSpotUser> spotUserOp = spotUserDao.findByAccessControlSpotIdAndUserHrId(id, empNo);
        if (spotUserOp.isEmpty()) {
            return ServiceResult.error("该门禁点下无此员工");
        }

        // List<String> userHrIdList = Arrays.asList(empNo);
        List<AccessPermissions.User> userList = new ArrayList<>();
        List<OpenAbilityMessage.User> openAbilityMessageUserList = new ArrayList<>();
        List<String> deviceCodeList = new ArrayList<>();
        List<IotDevice> deviceList = new ArrayList<>();

        for (AccessControlSpotUser obj : spotUserOp) {
            AccessControlSpotUser spotUser = obj;

            if (null != spotUser.getAccessControlSpot().getDevice()) {

                deviceList.add(spotUser.getAccessControlSpot().getDevice());
                deviceCodeList.add(spotUser.getAccessControlSpot().getDevice().getSn());

                List<String> groupIds = new ArrayList<>();
                if (DeviceType.FACE_RECOGNITION.equals(spotUser.getAccessControlSpot().getDevice().getDeviceType())) {
                    accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotId(spotUser.getAccessControlSpot().getId()).forEach(group -> groupIds.add(group.getId()));
                }

                AccessPermissions.User user = new AccessPermissions.User();
                user.setJobNo(spotUser.getUserHrId());
                user.setName(spotUser.getUserName());
                user.setPersonType(AccessPermissions.PersonType.EMPLOYEE);
                user.setGroupIds(groupIds);
                userList.add(user);

                OpenAbilityMessage.User messageUser = new OpenAbilityMessage.User();
                messageUser.setName(spotUser.getUserName());
                messageUser.setHrId(spotUser.getUserHrId());
                messageUser.setStartTime(spotUser.getAccessStartTime());
                messageUser.setEndTime(spotUser.getAccessEndTime());
                openAbilityMessageUserList.add(messageUser);
                /*// KS设备不允许删除通行权限
                if (DeviceType.FACE_RECOGNITION.equals(spotUser.getAccessControlSpot().getDevice().getDeviceType())) {

                    if (IotDeviceService.isKSHTDevice(spotUser.getAccessControlSpot().getDevice())) {
                        // 从鸿图删除人
                        // 查询出门禁点对应的鸿图人员组
                        List<AccessControlSpotKSHTMemberGroup> groupList = accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotId(spotUser.getAccessControlSpot().getId());

                        groupList.forEach(group -> iotcService.removeUserFromKSHTGroup(group.getId(), userHrIdList));
                    } else {
                        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                        return ServiceResult.error("旷视九霄设备请在人员组删除权限");
                    }

                } else if (DeviceType.HIK_FACE_MACHINE.equals(spotUser.getAccessControlSpot().getDevice().getDeviceType())) {
                    // 删除HK机器人员通行权限
                    List<String> jobNos = new ArrayList<>();
                    jobNos.add(empNo);
                    ServiceResult result = accessRulesService.deleteHkUser(spotUser.getAccessControlSpot().getDevice().getId(), jobNos);
                    if (!result.isSuccess()) {
                        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                        return ServiceResult.error(String.valueOf(result.getObject()));
                    }
                }*/
            }

            //创建task
            //所有添加人员的OpenAbilityMessage.User
            OpenAbilityTask task = createTask(Ability.REMOVE_AUT, openAbilityMessageUserList, deviceList, TaskType.MQ, MethodType.DELETE);

            //创建需要发送mq的消息
            AccessPermissions message = new AccessPermissions();
            message.setUserList(userList);

            message.setReturnTopic(Topic.get(Topic.USER_UNBIND_RETURN));
            message.setResourceId(task.getId());
            message.setSourceName("IBMS");
            message.setMethodName(AccessPermissions.Method.UNBIND.getName());
            message.setModuleName(AccessPermissions.Model.PERSON.getName());
            message.setDeviceCodeList(deviceCodeList);
            message.setOver("1");

            log.debug("发送mq员工参数{}", message);
            iotcService.unBindUserWithDevices(message);

            // 删除门禁点人员通行权限
            spotUser.setIsDelete(1);
            spotUserDao.save(spotUser);
            // 删除用户门禁点权限绑定的时间计划
            accessControlTimePlanService.deleteTimePlan(id, empNo);
        }

        return ServiceResult.ok();
    }

    @Override
    @Transactional
    public ServiceResult createKS(AccessControlKsCreateReq req) {

        // 门禁点
        Optional<AccessControlSpot> spotOp = accessControlSpotDao.findById(req.getId());
        if (spotOp.isEmpty()) {
            return ServiceResult.error("添加失败,门禁点不存在");
        }
        AccessControlSpot spot = spotOp.get();

        // 获取云端设备
        Optional<DeviceCloud> deviceCloudOp = deviceCloudDao.findByIotDeviceId(spot.getDevice().getId());
        if (deviceCloudOp.isEmpty()) {
            return ServiceResult.error("添加失败,旷视设备不存在");
        }
        DeviceCloud deviceCloud = deviceCloudOp.get();
        // 获取时间计划
        List<TimePlan> timePlanList = timePlanDao.findAll();
        if (timePlanList.isEmpty()) {
            return ServiceResult.error("添加失败,没有时间计划");
        }

        List<AccessControlSpotStaffGroupRelation> relations = accessControlSpotStaffGroupRelationDao.findByAccessControlSpotIdAndIsDefault(spot.getId(), 1);
        if (!relations.isEmpty()) {
            req.getUserGroups().add(relations.get(0).getStaffGroup().getId());
        }

        // 向KS添加人员权限
        AccessRulesReq accessRulesReq = new AccessRulesReq();
        accessRulesReq.setProjectId(spot.getSpace().getProject().getId());
        accessRulesReq.setSpaceId(spot.getSpace().getId());
        accessRulesReq.setStaffGroups(req.getUserGroups());
        accessRulesReq.setDeviceSource(DeviceSource.KUANGSHI);
        accessRulesReq.setTimePlanId(timePlanList.get(0).getId());
        accessRulesReq.setDistinguishDeviceId(deviceCloud.getId());
        ServiceResult result = accessRulesService.updateKs(accessRulesReq);
        if (!result.isSuccess()) {
            // 业务处理失败，手动回滚事务,返回异常
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return ServiceResult.error(String.valueOf(result.getObject()));
        }
        /*Optional<AccessRules> accessRulesOp = accessRulesDao.findByDeviceCloud(deviceCloud);
        if (accessRulesOp.isEmpty()) {
            ServiceResult result = accessRulesService.saveKs(accessRulesReq);
            if (!result.isSuccess()) {
                // 业务处理失败，手动回滚事务,返回异常
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error(String.valueOf(result.getObject()));
            }
        } else {

        }*/

        // 删除门禁点和人员组关系及人员组成员
        List<AccessControlSpotStaffGroupRelation> list = accessControlSpotStaffGroupRelationDao.findByAccessControlSpotIdAndIsDefault(spot.getId(), 0);
        list.forEach(it -> {
            it.setIsDelete(1);
            membersDao.findByGroupIdId(it.getStaffGroup().getId()).forEach(member -> {
                List<AccessControlSpotUser> userList = spotUserDao.findByAccessControlSpotIdAndUserHrId(spot.getId(), member.getEmpNo());
                userList.forEach(obj -> {
                    Optional<AccessControlSpotUser> spotUserOp = Optional.ofNullable(obj);
                    if (spotUserOp.isPresent()) {
                        AccessControlSpotUser spotUser = spotUserOp.get();
                        spotUser.setIsDelete(1);
                        spotUserDao.save(spotUser);
                    }
                });

            });
            accessControlSpotStaffGroupRelationDao.save(it);
        });

        // 保存门禁点人员权限
        req.getUserGroups().forEach(id -> {
            // 根据人员组ID获取人员组成员
            List<StaffGroupMembers> membersList = membersDao.findByGroupIdId(id);
            membersList.forEach(member -> {
                Integer spotUserOp = spotUserDao.checkSpotUser(member.getEmpNo(),
                        DateUtils.formatLocalDateTime(req.getStartTime(), DateUtils.PATTERN_DATETIME),
                        DateUtils.formatLocalDateTime(req.getEndTime(), DateUtils.PATTERN_DATETIME), spot.getId());
                if (spotUserOp == null) {
                    AccessControlSpotUser spotUser = new AccessControlSpotUser();
                    spotUser.setAccessControlSpot(spot);
                    spotUser.setAccessEndTime(req.getEndTime());
                    spotUser.setAccessStartTime(req.getStartTime());
                    spotUser.setUserHrId(member.getEmpNo());
                    spotUser.setUserName(member.getRealName());
                    spotUserDao.save(spotUser);
                }
//                else {
//                    AccessControlSpotUser spotUser = spotUserOp.get();
//                    spotUser.setAccessEndTime(req.getEndTime());
//                    spotUser.setAccessStartTime(req.getStartTime());
//                    spotUserDao.save(spotUser);
//                }
            });
        });

        // 循环保存门禁点和人员组关系


        for (String id : req.getUserGroups()) {
            Optional<StaffGroup> staffGroupOp = staffGroupDao.findById(id);
            if (staffGroupOp.isEmpty()) {
                // 业务处理失败，手动回滚事务,返回异常
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error("id有误, 人员组不存在");
            }

            Optional<AccessControlSpotStaffGroupRelation> optional = accessControlSpotStaffGroupRelationDao.findByAccessControlSpotIdAndStaffGroupId(spot.getId(), id);
            if (optional.isEmpty()) {
                AccessControlSpotStaffGroupRelation accessControlSpotStaffGroupRelation = new AccessControlSpotStaffGroupRelation();
                accessControlSpotStaffGroupRelation.setStaffGroup(staffGroupOp.get());
                accessControlSpotStaffGroupRelation.setAccessControlSpot(spot);
                accessControlSpotStaffGroupRelationDao.save(accessControlSpotStaffGroupRelation);
            }

        }

        return ServiceResult.ok();
    }

    @Override
    @Transactional
    public ServiceResult createRules(AccessControlRulesCreateReq req) {

        // hk设备ID 门禁ID
        List<String> hkDeviceIds = new ArrayList<>();
        List<String> hkAccessControlIds = new ArrayList<>();
        // ks设备ID 门禁ID
        List<String> ksDeviceIds = new ArrayList<>();
        List<AccessControlSpot> ksAccessControlList = new ArrayList<>();

        // 所有门禁点
        List<AccessControlSpot> accessControlSpotList;

        // 是否全部门禁
        if (1 == req.getIsAll()) {
            accessControlSpotList = accessControlSpotDao.findByAccessControlSpotLevel(AccessControlSpotLevel.NORMAL);

            deviceIds(accessControlSpotList, hkDeviceIds, ksDeviceIds, hkAccessControlIds, ksAccessControlList);
        } else {
            accessControlSpotList = accessControlSpotDao.findByIdIn(req.getAccessControlIds());
            deviceIds(accessControlSpotList, hkDeviceIds, ksDeviceIds, hkAccessControlIds, ksAccessControlList);
        }

        //根据ids查询门禁点
        List<AccessControlSpot> accessControlSpots = accessControlSpotDao.findByIdIn(req.getAccessControlIds());
        for (AccessControlSpot accessControlSpot : accessControlSpots) {
            //判断设备是否是 门磁
            if (DeviceType.DOOR_MAGNETIC.equals(accessControlSpot.getDevice().getDeviceType())) {
                AccessControlSpotUser spotUser = new AccessControlSpotUser();
                for (UserReq userReq : req.getUsers()) {
                    spotUser.setUserName(userReq.getName());
                    spotUser.setUserHrId(userReq.getEmpNo());
                    spotUser.setAccessStartTime(req.getStartTime());
                    spotUser.setAccessEndTime(req.getEndTime());
                    spotUser.setAccessControlSpot(accessControlSpot);
                    spotUserDao.save(spotUser);
                }
            }
        }

        // 向HK设备添加人员
        if (!hkDeviceIds.isEmpty() || !hkAccessControlIds.isEmpty()) {

            List<AccessControlSpot> hkAccessControlSpotList = accessControlSpotDao.findByIdIn(hkAccessControlIds);
            // 保存hk设备门禁点人员权限
            hkAccessControlSpotList.forEach(it -> req.getUsers().forEach(user -> {

                AccessControlSpotUser spotUser = new AccessControlSpotUser();
                spotUser.setUserName(user.getName());
                spotUser.setUserHrId(user.getEmpNo());
                spotUser.setAccessStartTime(req.getStartTime());
                spotUser.setAccessEndTime(req.getEndTime());
                spotUser.setAccessControlSpot(it);
                spotUserDao.save(spotUser);

            }));

            AccessControlHkCreateReq accessControlHkCreateReq = new AccessControlHkCreateReq();
            accessControlHkCreateReq.setAccessControlIds(hkAccessControlIds);
            accessControlHkCreateReq.setDeviceIds(hkDeviceIds);
            accessControlHkCreateReq.setEndTime(req.getEndTime());
            accessControlHkCreateReq.setStartTime(req.getStartTime());
            accessControlHkCreateReq.setUsers(req.getUsers());
            ServiceResult hkServiceResult = createHK(accessControlHkCreateReq);
            if (!hkServiceResult.isSuccess()) {
                log.error("门禁点批量分配: HK人员添加失败");
                return ServiceResult.error("海康设备" + hkServiceResult.getObject());
            }
        }

        // 向KS添加人员权限
        if (!ksAccessControlList.isEmpty()) {

            //KSJX list
            List<AccessControlSpot> KSJXAccessControlList = new ArrayList<>();
            //KSHT list
            List<AccessControlSpot> KSHTAccessControlList = new ArrayList<>();

            for (AccessControlSpot accessControlSpot : ksAccessControlList) {
                // 判断是九霄设备还是鸿图设备
                if (IotDeviceService.isKSHTDevice(accessControlSpot.getDevice())) {
                    AccessControlSpot KSHTAccessControl = new AccessControlSpot();
                    BeanUtils.copyProperties(accessControlSpot, KSHTAccessControl);
                    KSHTAccessControlList.add(KSHTAccessControl);
                } else {
                    AccessControlSpot KSJXAccessControl = new AccessControlSpot();
                    BeanUtils.copyProperties(accessControlSpot, KSJXAccessControl);
                    KSJXAccessControlList.add(KSJXAccessControl);
                }
            }

            //判断用户在ksht平台上人员是否存在
            for (UserReq userReq : req.getUsers()) {
                ServiceResult userFromPlatform = iotcService.getUserFromPlatform(userReq.getEmpNo());
                GetUserResp getUserResp = JSONObject.parseObject(JSONObject.toJSONString(userFromPlatform.getObject()), GetUserResp.class);
                log.debug("查询人员{}{}海康、旷视平台人员结果{}", userReq.getName(), userReq.getEmpNo(), getUserResp);
                if (Boolean.FALSE.equals(getUserResp.getIsKs())) {
                    return ServiceResult.error("该用户:" + userReq.getEmpNo() + "在旷视鸿图平台上不存在");
                }
            }

            //添加人员中员工
            List<UserReq> staffUserReqList = new ArrayList<>();
            //添加人员中访客
            List<UserReq> visitorUserReqList = new ArrayList<>();

            for (UserReq userReq : req.getUsers()) {
                //员工
                if (!SysUserService.isVisitor(userReq.getEmpNo())) {
                    UserReq staffUser = new UserReq();
                    BeanUtils.copyProperties(userReq, staffUser);
                    staffUserReqList.add(staffUser);
                }
                if (SysUserService.isVisitor(userReq.getEmpNo())) {
                    UserReq visitorUser = new UserReq();
                    BeanUtils.copyProperties(userReq, visitorUser);
                    visitorUserReqList.add(visitorUser);
                }
            }

            //有鸿图设备
            if (!KSHTAccessControlList.isEmpty()) {

                // 保存KSHT设备门禁点人员权限
                KSHTAccessControlList.forEach(it -> req.getUsers().forEach(user -> {

                    AccessControlSpotUser spotUser = new AccessControlSpotUser();
                    spotUser.setUserName(user.getName());
                    spotUser.setUserHrId(user.getEmpNo());
                    spotUser.setAccessStartTime(req.getStartTime());
                    spotUser.setAccessEndTime(req.getEndTime());
                    spotUser.setAccessControlSpot(it);
                    spotUserDao.save(spotUser);

                }));

                for (AccessControlSpot accessControlSpot : KSHTAccessControlList) {
                    // 鸿图
                    log.debug("向旷视鸿图添加权限");

                    //查询鸿图员工组是否存 员工组类型"员工组"
                    Optional<AccessControlSpotKSHTMemberGroup> KSHTMemberGroupStaff = accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotIdAndGroupTypeAndOpenAbilityAbility(accessControlSpot.getId(), KSHTMemberGroupType.STAFF, Ability.OTHER);

                    //查询鸿图员工组是否存 员工组类型"访客"
                    Optional<AccessControlSpotKSHTMemberGroup> KSHTMemberGroupVisitor = accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotIdAndGroupTypeAndOpenAbilityAbility(accessControlSpot.getId(), KSHTMemberGroupType.VISITOR, Ability.OTHER);


                    //查询开发能力other
                    Optional<OpenAbility> ability = openAbilityDao.findByAbility(Ability.OTHER);
                    if (ability.isEmpty()) {
                        return ServiceResult.error("未找到开发能力other");
                    }
                    OpenAbility openAbility = ability.get();

                    //添加人员中有 员工
                    if (!staffUserReqList.isEmpty() && KSHTMemberGroupStaff.isEmpty()) {
                        // 创建鸿图员工组对象
                        List<AccessControlSpotKSHTMemberGroup> kshtMemberGroupList = new ArrayList<>();
                        //创建员工组
                        AccessControlSpotKSHTMemberGroup memberGroup = new AccessControlSpotKSHTMemberGroup();
                        //门禁点id
                        memberGroup.setAccessControlSpot(accessControlSpot);
                        //鸿图人员组名称
                        memberGroup.setGroupName(accessControlSpot.getName() + "_" + KSHTMemberGroupType.STAFF.getName());
                        //人员组类型
                        memberGroup.setGroupType(KSHTMemberGroupType.STAFF);
                        //开发能力
                        memberGroup.setOpenAbility(openAbility);

                        kshtMemberGroupList.add(memberGroup);
                        //保存到数据库
                        accessControlSpotKSHTMemberGroupDao.saveAll(kshtMemberGroupList);

                        List<CreateHTMemberGroupReq.Param> groupReqParamList = kshtMemberGroupList.stream()
                                .map(obj -> CreateHTMemberGroupReq.Param.builder()
                                        .id(obj.getId())
                                        .type(obj.getGroupType().getCode())
                                        .name(obj.getGroupName())
                                        .build())
                                .collect(Collectors.toList());

                        // 向iotc发送创建员工组消息
                        ServiceResult result = iotcService.createStaffHTMemberGroup(CreateHTMemberGroupReq.builder().groupList(groupReqParamList).build());
                        if (!result.isSuccess()) {
                            log.error("iotc创建员工组失败");
                            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                            return result;
                        }

                        // 向iotc发送设备绑定员工组消息
                        for (AccessControlSpotKSHTMemberGroup group : kshtMemberGroupList) {
                            PermisssionReq permisssionReq = new PermisssionReq();
                            String[] groupUuidList = new String[]{group.getId()};
                            String[] deviceUuidList = new String[]{group.getAccessControlSpot().getDevice().getSn()};
                            permisssionReq.setDeviceUuidList(deviceUuidList);
                            permisssionReq.setGroupUuidList(groupUuidList);
                            permisssionReq.setTimePlanId(timePlan);
                            result = iotcConnectService.bindingDevice(permisssionReq, "2", "KS");
                            //失败
                            if (!result.isSuccess()) {
                                log.error("iotc设备绑定员工组失败");
                                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                                return result;
                            }
                        }
                    }

                    //添加人员中有 访客
                    if (!visitorUserReqList.isEmpty() && KSHTMemberGroupVisitor.isEmpty()) {
                        // 创建鸿图员工组对象
                        List<AccessControlSpotKSHTMemberGroup> kshtMemberGroupList = new ArrayList<>();
                        //创建员工组
                        AccessControlSpotKSHTMemberGroup memberGroup = new AccessControlSpotKSHTMemberGroup();
                        //门禁点id
                        memberGroup.setAccessControlSpot(accessControlSpot);
                        //鸿图人员组名称
                        memberGroup.setGroupName(accessControlSpot.getName() + "_" + KSHTMemberGroupType.VISITOR.getName());
                        //人员组类型
                        memberGroup.setGroupType(KSHTMemberGroupType.VISITOR);
                        //开发能力
                        memberGroup.setOpenAbility(openAbility);

                        kshtMemberGroupList.add(memberGroup);
                        //保存到数据库
                        accessControlSpotKSHTMemberGroupDao.saveAll(kshtMemberGroupList);

                        List<CreateHTMemberGroupReq.Param> groupReqParamList = kshtMemberGroupList.stream()
                                .map(obj -> CreateHTMemberGroupReq.Param.builder()
                                        .id(obj.getId())
                                        .type(obj.getGroupType().getCode())
                                        .name(obj.getGroupName())
                                        .build())
                                .collect(Collectors.toList());

                        // 向iotc发送创建员工组消息
                        ServiceResult result = iotcService.createStaffHTMemberGroup(CreateHTMemberGroupReq.builder().groupList(groupReqParamList).build());
                        if (!result.isSuccess()) {
                            log.error("iotc创建员工(访客)组失败");
                            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                            return result;
                        }

                        // 向iotc发送设备绑定员工组消息
                        for (AccessControlSpotKSHTMemberGroup group : kshtMemberGroupList) {
                            PermisssionReq permisssionReq = new PermisssionReq();
                            String[] groupUuidList = new String[]{group.getId()};
                            String[] deviceUuidList = new String[]{group.getAccessControlSpot().getDevice().getSn()};
                            permisssionReq.setDeviceUuidList(deviceUuidList);
                            permisssionReq.setGroupUuidList(groupUuidList);
                            permisssionReq.setTimePlanId(timePlan);
                            result = iotcConnectService.bindingDevice(permisssionReq, "2", "KS");
                            //失败
                            if (!result.isSuccess()) {
                                log.error("iotc设备绑定员工(访客)组失败");
                                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                                return result;
                            }
                        }
                    }

                    //查询该门禁点下所有人员
                    List<AccessControlSpotUser> byAccessControlSpotId = accessControlSpotUserDao.findByAccessControlSpotId(accessControlSpot.getId());

                    //personUuidsStaff 所有员工id
                    List<String> personUuidsStaff = new ArrayList<>();
                    //personUuidsVisitor 所有访客id
                    List<String> personUuidsVisitor = new ArrayList<>();

                    for (AccessControlSpotUser accessControlSpotUser1 : byAccessControlSpotId) {
                        if (!SysUserService.isVisitor(accessControlSpotUser1.getUserHrId())) {
                            //判断是否是员工
                            personUuidsStaff.add(accessControlSpotUser1.getUserHrId());
                        } else if (SysUserService.isVisitor(accessControlSpotUser1.getUserHrId())) {
                            personUuidsVisitor.add(accessControlSpotUser1.getUserHrId());
                        }
                    }

                    //去重
                    List<String> collectStaff = personUuidsStaff.stream().distinct().collect(Collectors.toList());
                    List<String> collectVisitor = personUuidsVisitor.stream().distinct().collect(Collectors.toList());

                    //新增人员中有 员工
                    if (!staffUserReqList.isEmpty()) {
                        Optional<AccessControlSpotKSHTMemberGroup> KSHTMemberGroup = accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotIdAndGroupTypeAndOpenAbilityAbility(accessControlSpot.getId(), KSHTMemberGroupType.STAFF, Ability.OTHER);
                        if (KSHTMemberGroup.isEmpty()) {
                            return ServiceResult.error("未找到对应的人员组");
                        }
                        AccessControlSpotKSHTMemberGroup accessControlSpotKSHTMemberGroup = KSHTMemberGroup.get();
                        //向iotc发送下发人员信息
                        ServiceResult serviceResultStaff = iotcService.addUserFromKSHTGroup(accessControlSpotKSHTMemberGroup.getId(), collectStaff);
                        if (!serviceResultStaff.isSuccess()) {
                            log.debug("正向iotc下发人员");
                            log.error("向iotc下发人员失败对象{}");
                            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                            return ServiceResult.error(String.valueOf(serviceResultStaff.getObject()));
                        }
                    }

                    //新增人员中有 访客
                    if (!visitorUserReqList.isEmpty()) {
                        Optional<AccessControlSpotKSHTMemberGroup> KSHTMemberGroup = accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotIdAndGroupTypeAndOpenAbilityAbility(accessControlSpot.getId(), KSHTMemberGroupType.VISITOR, Ability.OTHER);
                        if (KSHTMemberGroup.isEmpty()) {
                            return ServiceResult.error("未找到对应的访客人员组");
                        }
                        AccessControlSpotKSHTMemberGroup accessControlSpotKSHTMemberGroup = KSHTMemberGroup.get();
                        //向iotc发送下发访客人员信息
                        log.debug("正向iotc下发访客人员");
                        ServiceResult serviceResultVisitor = iotcService.addUserFromKSHTGroup(accessControlSpotKSHTMemberGroup.getId(), collectVisitor);
                        if (!serviceResultVisitor.isSuccess()) {
                            log.error("向iotc下发访客人员失败对象{}");
                            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                            return ServiceResult.error(String.valueOf(serviceResultVisitor.getObject()));
                        }
                    }
                }
            }

            //下发人员中有访客 修改访客权限
            if (!visitorUserReqList.isEmpty() && !KSHTAccessControlList.isEmpty()) {

                //获取所有设备集合
                List<IotDevice> deviceList = KSHTAccessControlList.stream()
                        .map(AccessControlSpot::getDevice)
                        .collect(Collectors.toList());

                //获取设备的code集合
                List<String> deviceCodeList = deviceList.stream().map(IotDevice::getSn).collect(Collectors.toList());

                //获取ht门禁点id
                List<String> kshtAccessControlSpotIds = KSHTAccessControlList.stream().map(AccessControlSpot::getId).collect(Collectors.toList());

                //ht 访客 人员组
                List<AccessControlSpotKSHTMemberGroup> kshtMemberGroupList = accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotIdInAndOpenAbilityAbilityAndGroupType(kshtAccessControlSpotIds, Ability.OTHER, KSHTMemberGroupType.VISITOR);

                //ht 访客 人员组id
//                List<String> kshtMemberGroupIds = kshtMemberGroupList.stream().map(AccessControlSpotKSHTMemberGroup::getId).collect(Collectors.toList());

                List<OpenAbilityMessage.User> users = new ArrayList<>();
                for (UserReq userReq : visitorUserReqList) {
                    OpenAbilityMessage.User user = new OpenAbilityMessage.User();
                    user.setHrId(userReq.getEmpNo());
                    user.setName(userReq.getName());
                    user.setStartTime(req.getStartTime());
                    user.setEndTime(req.getEndTime());
                    users.add(user);
                }

                users.forEach(obj -> {

                    AccessPermissions.User user = AccessPermissions.User.convert(obj, kshtMemberGroupList.stream()
                            .map(AccessControlSpotKSHTMemberGroup::getId)
                            .collect(Collectors.toList()));

                    //创建任务
                    OpenAbilityTask task = createUpdateTask(Ability.OTHER, users, deviceList, TaskType.MQ, MethodType.UPDATE);

                    // 创建需要发送mq的消息体
                    AccessPermissions message = new AccessPermissions();
                    message.setDeviceCodeList(deviceCodeList);
                    message.setSourceName("IBMS");
                    message.setMethodName(AccessPermissions.Method.UPDATE.getName());
                    message.setModuleName(AccessPermissions.Model.PERSON.getName());
                    message.setUserList(Arrays.asList(user));
                    message.setReturnTopic(Topic.get(Topic.ACCESS_CONTROL_KSHT_VISITOR_RETURN));
                    message.setResourceId(task.getId());

                    // 向iotc发送消息
                    log.debug("向iotc发送修改访客信息");
                    iotcService.updateFace(message);
                });

            }

            //有九霄设备
            if (!KSJXAccessControlList.isEmpty()) {

                // 保存KSJX设备门禁点人员权限
                KSJXAccessControlList.forEach(it -> req.getUsers().forEach(user -> {

                    AccessControlSpotUser spotUser = new AccessControlSpotUser();
                    spotUser.setUserName(user.getName());
                    spotUser.setUserHrId(user.getEmpNo());
                    spotUser.setAccessStartTime(req.getStartTime());
                    spotUser.setAccessEndTime(req.getEndTime());
                    spotUser.setAccessControlSpot(it);
                    spotUserDao.save(spotUser);

                }));

                for (AccessControlSpot accessControlSpot : KSJXAccessControlList) {
                    // 九霄

                    AccessControlKsCreateReq accessControlKsCreateReq = new AccessControlKsCreateReq();
                    accessControlKsCreateReq.setId(accessControlSpot.getId());
                    accessControlKsCreateReq.setEndTime(req.getEndTime());
                    accessControlKsCreateReq.setStartTime(req.getStartTime());
                    List<String> userGroups = new ArrayList<>();
                    StaffGroup staffGroup = null;
                    // 获取门禁点对应人员组
                    List<AccessControlSpotStaffGroupRelation> spotStaffGroupRelationList = accessControlSpotStaffGroupRelationDao.findByAccessControlSpotIdAndIsDefault(accessControlSpot.getId(), 1);
                    if (!spotStaffGroupRelationList.isEmpty()) {
                        AccessControlSpotStaffGroupRelation accessControlSpotStaffGroupRelation = spotStaffGroupRelationList.get(0);

                        staffGroup = accessControlSpotStaffGroupRelation.getStaffGroup();
                    }
                    // 循环保存人员组成员
                    if (null != staffGroup) {
                        List<String> participantList = new ArrayList<>();
                        for (UserReq user : req.getUsers()) {
                            participantList.add(user.getEmpNo() + "|" + user.getName());
                        }

                        membersDao.findByGroupIdId(staffGroup.getId()).forEach(it -> participantList.add(it.getEmpNo() + "|" + it.getRealName()));

                        StaffGroupReq staffGroupReq = new StaffGroupReq();
                        staffGroupReq.setName(staffGroup.getName());
                        staffGroupReq.setParticipantList(participantList);
                        // 更新人员组
                        ServiceResult result = staffGroupService.update(staffGroup.getId(), staffGroupReq);
                        if (!result.isSuccess()) {
                            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                            return result;
                        }
                    }

                    List<AccessControlSpotStaffGroupRelation> list = accessControlSpotStaffGroupRelationDao.findByAccessControlSpotId(accessControlSpot.getId());
                    list.forEach(it -> userGroups.add(it.getStaffGroup().getId()));
                    accessControlKsCreateReq.setUserGroups(userGroups);
                    ServiceResult ksServiceResult = createKS(accessControlKsCreateReq);
                    if (!ksServiceResult.isSuccess()) {
                        // 业务处理失败，手动回滚事务,返回异常
                        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                        return ksServiceResult;
                    }
                }


            }
        }

        return ServiceResult.ok();
    }

    /**
     * 创建任务
     *
     * @param ability    开放能力
     * @param userList   用户列表
     * @param deviceList 设备列表
     * @param type       消息类型
     * @param methodType 执行类型
     * @author Yanzy
     **/
    @Transactional
    public OpenAbilityTask createUpdateTask(Ability ability, List<OpenAbilityMessage.User> userList, List<IotDevice> deviceList, TaskType type, MethodType methodType) {

        // 创建 task
        OpenAbilityTask task = new OpenAbilityTask();

        task.setAbility(ability);
        task.setStartTime(LocalDateTime.now());
        task.setStatus(AbilityTaskStatus.DOING);
        task.setTopic(Topic.BIZ_TO_IOTC);
        task.setType(type);
        task.setMethodType(methodType);
        openAbilityTaskDao.save(task);

        // 创建 task detail
        List<OpenAbilityTaskDetail> allDetail = new ArrayList<>();
        deviceList.forEach(device -> {

            List<OpenAbilityTaskDetail> detailList = userList.stream().map(obj -> {
                OpenAbilityTaskDetail openAbilityTaskDetail = new OpenAbilityTaskDetail();

                openAbilityTaskDetail.setTask(task);
                openAbilityTaskDetail.setDevice(device);
                openAbilityTaskDetail.setUserHrId(obj.getHrId());
                openAbilityTaskDetail.setUserName(obj.getName());

                return openAbilityTaskDetail;
            }).collect(Collectors.toList());

            allDetail.addAll(detailList);
        });
        openAbilityTaskDetailDao.saveAll(allDetail);

        return task;
    }

    private void deviceIds(List<AccessControlSpot> spotList, List<String> hkDeviceIds, List<String> ksDeviceIds, List<String> hkAccessControlIds, List<AccessControlSpot> ksAccessControlList) {
        spotList.forEach(it -> {
            if (null != it.getDevice() && DeviceType.HIK_FACE_MACHINE.equals(it.getDevice().getDeviceType())) {
                hkDeviceIds.add(it.getDevice().getId());
                hkAccessControlIds.add(it.getId());
            } else if (null != it.getDevice() && DeviceType.FACE_RECOGNITION.equals(it.getDevice().getDeviceType())) {
                ksDeviceIds.add(it.getDevice().getId());
                ksAccessControlList.add(it);
            }
        });
    }

    private void deviceCodes(List<AccessControlSpot> spotList, List<String> hkDeviceCodes, List<String> ksDeviceCodes, List<String> hkAccessControlIds, List<AccessControlSpot> ksAccessControlList) {
        spotList.forEach(it -> {
            if (null != it.getDevice() && DeviceType.HIK_FACE_MACHINE.equals(it.getDevice().getDeviceType())) {
                hkDeviceCodes.add(it.getDevice().getSn());
                hkAccessControlIds.add(it.getId());
            } else if (null != it.getDevice() && DeviceType.FACE_RECOGNITION.equals(it.getDevice().getDeviceType())) {
                ksDeviceCodes.add(it.getDevice().getSn());
                ksAccessControlList.add(it);
            }
        });
    }


    /*private JSONObject getHkJSONObject(List<String> ips, List<String> errorList, UserAddReq userAddReq, String hkUserAddMoreUrl, List<String> empNos) {
        JSONObject jsonObject = null;
        if (!ips.isEmpty()) {
            userAddReq.setKey(ips);
            String requestParam = JSON.toJSONString(userAddReq);
            log.debug("json参数 ： " + requestParam);
            String s = restTemplateUtil.jsonPost(hkUserAddMoreUrl, requestParam, String.class);
            jsonObject = addHkErrorList(errorList, s, empNos);
        }
        return jsonObject;
    }*/

    /*private JSONObject addHkErrorList(List<String> errorList, String s, List<String> empNos) {
        log.debug("添加结果: " + s);
        JSONObject jsonObject = JSON.parseObject(s);
        JSONObject dataJson = jsonObject.getJSONObject("data");
        if (dataJson != null) {
            JSONArray failData = dataJson.getJSONArray("fail_data");
            if (failData != null) {
                List<HkAddUser> list = JSON.parseArray(failData.toJSONString(), HkAddUser.class);
                if (!list.isEmpty()) {
                    for (HkAddUser user : list) {
                        log.error("HK添加人员失败: " + user.getJobNo());
                        empNos.forEach(it -> {
                            if (it.equals(user.getJobNo())) {
                                errorList.add(it);
                            }
                        });
                    }
                }
            }
        }
        return jsonObject;
    }*/

    /*private ServiceResult hkIpsResult(LocalDateTime endTime, LocalDateTime startTime, List<String> deviceIds) {

        // 比较通行时间
        if (startTime.isAfter(endTime)) {
            return ServiceResult.error("请输入正确的通行时间");
        }

        // 根据hk设备id获取ip
        List<HkDevice> hkDeviceList = hkDeviceDao.findByDeviceIdIn(deviceIds);
        List<String> ksIps = new ArrayList<>();
        List<String> ntIps = new ArrayList<>();
        hkDeviceList.forEach(it -> {
            if (HkAddress.KS.equals(it.getAddress())) {
                ksIps.add(it.getIp());
            } else {
                ntIps.add(it.getIp());
            }
        });
        HkIpsResp hkIpsResp = new HkIpsResp();
        hkIpsResp.setKsIps(ksIps);
        hkIpsResp.setNtIps(ntIps);
        return ServiceResult.ok(hkIpsResp);
    }*/

    /**
     * @param personPermissionReq 分页查询参数
     * @return
     * @description 人员权限列表(分页)
     * @author tql
     * @date 21-12-2
     */
    @Override
    public PageBean<PersonPermissionPageResp> personPage(PersonPermissionReq personPermissionReq) {
        // 获取门禁点列表
        List<String> spotIds = getControlSpotBySpaceOrProject(personPermissionReq.getType(), personPermissionReq.getId());
        log.info("门禁点列表:{}", JSONObject.toJSONString(spotIds));

        Page<AccessControlSpotAccessRecord> accessControlSpotPage = accessControlSpotAccessRecordDao.findAll((root, query, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();
            // 空间id
            if (StringUtils.isNotBlank(personPermissionReq.getId())) {
                // 空间ids查询
                CriteriaBuilder.In<Object> inInitiator = criteriaBuilder.in(root.get("accessControlSpot").get("id"));
                spotIds.forEach(v -> inInitiator.value(v));
                list.add(criteriaBuilder.or(criteriaBuilder.and(inInitiator)));
            }

            // 开始时间
            if (personPermissionReq.getStartTime() != null) {
                list.add(criteriaBuilder.greaterThanOrEqualTo(root.get("deviceAccessRecord").get("dataTime"),
                        personPermissionReq.getStartTime()));
            }
            // 结束时间
            if (personPermissionReq.getEndTime() != null) {
                list.add(criteriaBuilder.lessThanOrEqualTo(root.get("deviceAccessRecord").get("dataTime"),
                        personPermissionReq.getEndTime()));
            }
            // 访客类型
            if (ObjectUtils.isNotEmpty(personPermissionReq.getPersonType())) {
                list.add(criteriaBuilder.equal(root.get("deviceAccessRecord").get("staffType"), personPermissionReq.getPersonType()));
            }

            // 工号/姓名
            if (StringUtils.isNotBlank(personPermissionReq.getPersonMsg())) {
                list.add(criteriaBuilder.or(criteriaBuilder.like(root.get("deviceAccessRecord").get("empNo"), "%" + personPermissionReq.getPersonMsg() + "%"),
                        criteriaBuilder.like(root.get("deviceAccessRecord").get("nameZh"), "%" + personPermissionReq.getPersonMsg() + "%")));
            }

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, PageRequest.of(personPermissionReq.getPage(), personPermissionReq.getSize(), Sort.by(personPermissionReq.getOrders())));


        PageBean<PersonPermissionPageResp> pageBean = new PageBean<>();
        pageBean.setPage(accessControlSpotPage.getPageable().getPageNumber());
        pageBean.setSize(accessControlSpotPage.getPageable().getPageSize());
        pageBean.setTotalPages(accessControlSpotPage.getTotalPages());
        pageBean.setTotalElements(accessControlSpotPage.getTotalElements());

        pageBean.setList(accessControlSpotPage.getContent()
                .stream()
                .map(PersonPermissionPageResp::convert)
                .collect(Collectors.toList()));

        return pageBean;
    }

    /**
     * @param departmentPersonReq 部门人员权限入参
     * @return
     * @description 部门人员门禁点权限
     * @author tql
     * @date 21-12-2
     */
    @Override
    public ServiceResult departPerson(DepartmentPersonReq departmentPersonReq) {
        return null;
    }

    @Override
    public ServiceResult getUserByDept(List<String> departmentIds, List<UserReq> users) {

        for (String departmentId : departmentIds) {
            JsonResult<List<PassportRespForFeign>> result = userCenterStaffApi.passportList(Long.parseLong(departmentId));

            if (result.getStatus() != 1) {
                log.error("从UC获取部门内员工数据失败！！！UC返回信息：" + result);
                return ServiceResult.error(result.getMsg());
            } else {
                List<PassportRespForFeign> ucStaffList = result.getData();
                ucStaffList.forEach(person -> {
                    UserReq user = new UserReq();
                    user.setEmpNo(person.getJobNo());
                    user.setName(person.getRealName());
                    users.add(user);
                });
            }
        }

        return ServiceResult.ok();
    }

    @Override
    public PageBean<SpotRecordPageResp> spotRecordPage(SpotRecordPageReq req) {

        req.addOrderDesc("deviceAccessRecord.dataTime");

        PageBean<SpotRecordPageResp> pageBean = new PageBean<>();
        Page<AccessControlSpotAccessRecord> recordPage = accessControlSpotAccessRecordDao.findAll((root, query, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();
            // 门禁点ID
            if (StringUtils.isNotBlank(req.getAccessControlSpotId())) {
                // 门禁点ID
                list.add(criteriaBuilder.equal(root.get("accessControlSpot").get("id"), req.getAccessControlSpotId()));
            }

            // 开始时间
            if (req.getStartTime() != null) {
                list.add(criteriaBuilder.greaterThanOrEqualTo(root.get("deviceAccessRecord").get("dataTime"),
                        req.getStartTime()));
            }
            // 结束时间
            if (req.getEndTime() != null) {
                list.add(criteriaBuilder.lessThanOrEqualTo(root.get("deviceAccessRecord").get("dataTime"),
                        req.getEndTime()));
            }

            // 工号/姓名
            if (StringUtils.isNotBlank(req.getKeyword())) {
                list.add(criteriaBuilder.or(criteriaBuilder.like(root.get("deviceAccessRecord").get("empNo"), "%" + req.getKeyword() + "%"),
                        criteriaBuilder.like(root.get("deviceAccessRecord").get("nameZh"), "%" + req.getKeyword() + "%")));
            }

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, PageRequest.of(req.getPage(), req.getSize(), Sort.by(req.getOrders())));

        List<SpotRecordPageResp> recordList = new ArrayList<>();

        recordPage.getContent().forEach(record -> {
            SpotRecordPageResp resp = new SpotRecordPageResp();
            resp.setEmpNo(record.getDeviceAccessRecord().getEmpNo());
            resp.setName(record.getDeviceAccessRecord().getNameZh());
            resp.setDept(record.getDeviceAccessRecord().getDepartment());
            resp.setPassTime(record.getDeviceAccessRecord().getDataTime());
            resp.setStatus(record.getDeviceAccessRecord().getAccessStatus());
            resp.setStatus(record.getDeviceAccessRecord().getAccessStatus());
            resp.setCapturePicture(Optional.ofNullable(record.getDeviceAccessRecord().getCapturePicture()).orElse(""));
            resp.setDefaultPicture(Optional.ofNullable(record.getDeviceAccessRecord().getDefaultPicture()).orElse(""));
            resp.setTemperature(Optional.ofNullable(record.getDeviceAccessRecord().getTemperature()).orElse(""));

            recordList.add(resp);
        });

        pageBean.setList(recordList);
        pageBean.setSize(req.getSize());
        pageBean.setPage(req.getPage());
        pageBean.setTotalPages(recordPage.getTotalPages());
        pageBean.setTotalElements(recordPage.getTotalElements());
        return pageBean;
    }

    @Override
    public byte[] spotRecordDownload(PageBean<SpotRecordPageResp> pageBean) {
        if (null == pageBean.getList() || pageBean.getList().isEmpty()) {
            return new byte[0];
        }

        List<String[]> recordList = new ArrayList<>();
        for (int i = 0; i < pageBean.getList().size(); i++) {

            SpotRecordPageResp resp = pageBean.getList().get(i);
            String[] cells = new String[6];
            cells[0] = String.valueOf(i + 1);
            cells[1] = resp.getName();
            cells[2] = resp.getEmpNo();
            cells[3] = resp.getDept();
            cells[4] = DateUtils.formatLocalDateTime(resp.getPassTime(), DateUtils.PATTERN_DATETIME);
            cells[5] = resp.getStatus();

            recordList.add(cells);
        }

        return excelService.downloadHkFaceRecord(recordList);
    }

    /**
     * 获取门禁点树
     *
     * @return
     * @author Yang.Lee
     * @date 2021/12/3 16:24
     **/
    @Override
    @Transactional
    public List<ProjectSpaceTreeResp> getTree() {

        // 获取项目及空间结构树
        List<ProjectSpaceTreeResp> projectSpaceTreeResp = spaceService.getProjectSpaceTree();
        // 将树转为list
        List<ProjectSpaceTreeResp> projectSpaceList = treeToList(projectSpaceTreeResp);
        // 将list转为map
        Map<String, ProjectSpaceTreeResp> projectSpaceMap = projectSpaceList.stream().collect(Collectors.toMap(ProjectSpaceTreeResp::getId, s -> s, (oldData, newData) -> oldData));

        // 查询所有的门禁点
        List<AccessControlSpot> accessControlSpotList = accessControlSpotDao.findAll();

        // 遍历所有的门禁点，将其放入空间树结构中
        accessControlSpotList.forEach(obj -> {
            ProjectSpaceTreeResp node = projectSpaceMap.get(obj.getSpace().getId());
            ProjectSpaceTreeResp spotNode = new ProjectSpaceTreeResp();
            spotNode.setId(obj.getId());
            spotNode.setName(obj.getName());
            spotNode.setNodeType("ACCESS_CONTROL_SPOT");

            if (node.getChildrenList() == null) {
                node.setChildrenList(new ArrayList<>());
            }

            node.getChildrenList().add(spotNode);
        });


        return projectSpaceTreeResp;
    }

    @Override
    public List<StaffGroupResp> getStaffGroupByAccessControlSpotId(String id) {

        List<StaffGroupResp> staffGroupList = new ArrayList<>();

        accessControlSpotStaffGroupRelationDao.findByAccessControlSpotId(id).forEach(it -> {
            StaffGroupResp staffGroupResp = new StaffGroupResp();
            staffGroupResp.setId(it.getStaffGroup().getId());
            staffGroupResp.setName(it.getStaffGroup().getName());
            staffGroupResp.setIsDefault(it.getIsDefault());
            staffGroupList.add(staffGroupResp);
        });
        return staffGroupList;
    }

    /**
     * @param type 类型空间/项目
     * @param id   空间id/项目id
     * @return
     * @description 根据空间/项目id获取门禁点
     * @author tql
     * @date 21-12-9
     */
    @Override
    public List<String> getControlSpotBySpaceOrProject(String type, String id) {
        List<String> list = new ArrayList<>();
        if ("SPACE".equals(type)) {
            // 根据空间获取门禁点
            // 获取所有空间
            List<Space> spaces = iotDeviceService.getChildSpacesBySpaceId(id);
            List<String> spaceIds = spaces.stream().map(Space::getId).collect(Collectors.toList());
            List<AccessControlSpot> bySpaceIdIn = accessControlSpotDao.findBySpaceIdIn(spaceIds);
            // 门禁点ids
            list = bySpaceIdIn.stream().map(AccessControlSpot::getId).collect(Collectors.toList());
        } else if ("PROJECT".equals(type)) {
            // 根据空间获取门禁点
            Set<Space> byProjectIdAndIsShow = spaceDao.findByProjectIdAndIsShow(id, 1);
            // 获取空间列表
            List<String> spaceIds = byProjectIdAndIsShow.stream().map(Space::getId).collect(Collectors.toList());
            List<AccessControlSpot> bySpaceIdIn = accessControlSpotDao.findBySpaceIdIn(spaceIds);
            // 门禁点ids
            list = bySpaceIdIn.stream().map(AccessControlSpot::getId).collect(Collectors.toList());
        } else if ("ACCESS_CONTROL_SPOT".equals(type)) {
            // 根据空间获取门禁点
            list.add(id);
        }

        return list;
    }

    @Override
    @Transactional
    public ServiceResult deleteDevice(String id, String coerce) {

        Optional<AccessControlSpot> spotOp = accessControlSpotDao.findById(id);
        if (spotOp.isEmpty()) {
            return ServiceResult.error("id有误,门禁点不存在");
        }
        AccessControlSpot spot = spotOp.get();
        List<String> groupIds = new ArrayList<>();
        if (DeviceType.FACE_RECOGNITION.equals(spot.getDevice().getDeviceType())) {
            accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotId(spot.getId()).forEach(group -> groupIds.add(group.getId()));
        }
        List<AccessPermissions.User> userList = new ArrayList<>();
        List<OpenAbilityMessage.User> openAbilityMessageUserList = new ArrayList<>();
        spotUserDao.findByAccessControlSpotId(id).forEach(spotUser -> {
            AccessPermissions.User user = new AccessPermissions.User();
            user.setJobNo(spotUser.getUserHrId());
            user.setName(spotUser.getUserName());
            user.setPersonType(AccessPermissions.PersonType.EMPLOYEE);
            user.setGroupIds(groupIds);
            userList.add(user);

            OpenAbilityMessage.User messageUser = new OpenAbilityMessage.User();
            messageUser.setName(spotUser.getUserName());
            messageUser.setHrId(spotUser.getUserHrId());
            messageUser.setStartTime(spotUser.getAccessStartTime());
            messageUser.setEndTime(spotUser.getAccessEndTime());
            openAbilityMessageUserList.add(messageUser);
        });
        //创建task
        List<IotDevice> deviceList = new ArrayList<>();
        deviceList.add(spot.getDevice());
        //所有添加人员的OpenAbilityMessage.User
        OpenAbilityTask task = createTask(Ability.REMOVE_AUT, openAbilityMessageUserList, deviceList, TaskType.MQ, MethodType.DELETE);

        //创建需要发送mq的消息
        AccessPermissions message = new AccessPermissions();
        message.setUserList(userList);


        message.setReturnTopic(Topic.get(Topic.USER_UNBIND_RETURN));
        message.setResourceId(task.getId());
        message.setSourceName("IBMS");
        message.setMethodName(AccessPermissions.Method.UNBIND.getName());
        message.setModuleName(AccessPermissions.Model.PERSON.getName());
        List<String> deviceCodeList = Arrays.asList(spot.getDevice().getSn());
        message.setDeviceCodeList(deviceCodeList);
        message.setOver("1");

        log.debug("发送mq员工参数{}", message);
        iotcService.unBindUserWithDevices(message);

        /*if (null != spot.getDevice() && !"1".equals(coerce)) {
            // 向旧设备删除人员
            if (DeviceType.HIK_FACE_MACHINE.equals(spot.getDevice().getDeviceType())) {

                // 获取门禁点人员
                List<String> jobNos = new ArrayList<>();
                List<AccessControlSpotUser> spotUserList = spotUserDao.findByAccessControlSpotId(spot.getId());
                spotUserList.forEach(it -> jobNos.add(it.getUserHrId()));

                // 删除海康指定设备下人员
                ServiceResult result = accessRulesService.deleteHkUser(spot.getDevice().getId(), jobNos);
                if (!result.isSuccess()) {
                    // 业务处理失败，手动回滚事务,返回异常
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return ServiceResult.error(String.valueOf(result.getObject()), "3");
                }
            } else if (DeviceType.FACE_RECOGNITION.equals(spot.getDevice().getDeviceType())) {

                // 向旷视删除权限
                Optional<DeviceCloud> deviceCloudOp = deviceCloudDao.findByIotDeviceId(spot.getDevice().getId());
                if (deviceCloudOp.isPresent()) {
                    // 发送请求
                    ServiceResult result = msDeviceService.deleteAccessRule(deviceCloudOp.get().getKsDeviceId());
                    if (!result.isSuccess()) {
                        // 业务处理失败，手动回滚事务,返回异常
                        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                        return ServiceResult.error(String.valueOf(result.getObject()), "3");
                    }
                }
            }
        }*/

        spot.setAccessControlSpotType(null);
        spot.setDevice(null);
        accessControlSpotDao.save(spot);
        return ServiceResult.ok();
    }

    /**
     * 获取树形式的门禁点列表
     *
     * @return 门禁点列表
     * @author Yang.Lee
     * @date 2021/12/13 13:19
     **/
    @Override
    @Transactional
    public List<ProjectSpaceTreeResp> getListWithTree(boolean withSpot) {

        List<Space> accessSpaceList = accessControlSpotDao.findAllSpotGroupBySpace();

        List<Space> spaceList = new ArrayList<>();

        accessSpaceList.forEach(obj -> {

            Space space = obj;
            spaceList.add(space);

            while ((space = space.getParent()) != null) {
                spaceList.add(space);
            }
        });

        List<SpaceTreeResp> spaceTreeRespList = spaceService.getSpaceTreeResp(spaceList);

        Map<String, List<SpaceTreeResp>> spaceTreeRespMap = spaceTreeRespList.stream()
                .collect(Collectors.toMap(SpaceTreeResp::getProjectId,
                        e -> new ArrayList<>(List.of(e)), (oldList, newList) -> {
                            oldList.addAll(newList);
                            return oldList;
                        }));


        Set<String> projectIdSet = spaceTreeRespList.stream().map(SpaceTreeResp::getProjectId).collect(Collectors.toSet());
        List<Project> projectList = projectDao.findAllById(projectIdSet);
        List<ProjectSpaceTreeResp> result = projectList.stream().map(obj -> {

            ProjectSpaceTreeResp resp = new ProjectSpaceTreeResp();
            resp.setId(obj.getId());
            resp.setName(obj.getProjectName());
            resp.setNodeType("PROJECT");

            resp.setChildrenList(spaceTreeRespMap.get(obj.getId()).stream()
                    .map(ProjectSpaceTreeResp::convert)
                    .collect(Collectors.toList()));

            return resp;
        }).collect(Collectors.toList());

        if (withSpot) {
            List<AccessControlSpot> accessControlSpotList = accessControlSpotDao.findAll();
            Map<String, List<AccessControlSpot>> accessControlSpotMap = accessControlSpotList.stream()
                    .collect(Collectors.toMap(obj -> obj.getSpace().getId(),
                            e -> new ArrayList<>(List.of(e)),
                            (oldList, newList) -> {
                                oldList.addAll(newList);
                                return oldList;
                            }));

            List<ProjectSpaceTreeResp> allTree = new ArrayList<>();
            result.forEach(obj -> allTree.addAll(obj.getChildrenList()));

            List<ProjectSpaceTreeResp> allList = treeToList(allTree);
            allList.forEach(obj -> {
                if (accessControlSpotMap.containsKey(obj.getId())) {

                    List<ProjectSpaceTreeResp> spot = accessControlSpotMap.get(obj.getId()).stream().map(ProjectSpaceTreeResp::convert).collect(Collectors.toList());
                    if (CollectionUtils.isEmpty(obj.getChildrenList())) {
                        obj.setChildrenList(spot);
                    } else {
                        obj.getChildrenList().addAll(spot);
                    }

                }
            });
        }

        return result;
    }

    /**
     * 向门禁点中添加用户（仅添加软件层面的权限，不会将用户数据下发到设备）
     *
     * @param userList 用户列表
     * @param spots    门禁点
     * @return 添加结果
     * @author Yang.Lee
     * @date 2022/2/7 14:33
     **/
    @Override
    @Transactional
    public ServiceResult addUsers(List<AccessControlSpotUserReq> userList, Ability ability, AccessControlSpot... spots) {

        for (AccessControlSpot spot : spots) {

            // 过滤已存在的数据
            List<AccessControlSpotUser> accessControlSpotUserList = userList.stream().filter(obj -> {

                Integer exists = spotUserDao.checkSpotUser(obj.getEmpNo(),
                        DateUtils.formatLocalDateTime(obj.getStartTime(), DateUtils.PATTERN_DATETIME),
                        DateUtils.formatLocalDateTime(obj.getEndTime(), DateUtils.PATTERN_DATETIME),
                        spot.getId());

                return exists == null;
            }).map(obj -> {

                AccessControlSpotUser accessControlSpotUser = new AccessControlSpotUser();
                accessControlSpotUser.setAccessControlSpot(spot);
                accessControlSpotUser.setUserHrId(obj.getEmpNo());
                accessControlSpotUser.setUserName(obj.getName());
                accessControlSpotUser.setAccessStartTime(obj.getStartTime());
                accessControlSpotUser.setAccessEndTime(obj.getEndTime());
                accessControlSpotUser.setIsVip(obj.isVip() ? 1 : 0);

                return accessControlSpotUser;
            }).collect(Collectors.toList());

            spotUserDao.saveAll(accessControlSpotUserList);
        }

        return ServiceResult.ok();
    }

    /**
     * 移除用户所有权限 （仅添加软件层面的权限，不会将用户数据下发到设备）
     *
     * @param userIdList 用户工号列表
     * @return 操作结果
     * @author Yang.Lee
     * @date 2022/2/9 9:39
     **/
    @Override
    @Transactional
    public ServiceResult removeUserAllAccess(List<String> userIdList) {

        List<AccessControlSpotUser> editList = new ArrayList<>();

        userIdList.forEach(obj -> {
            List<AccessControlSpotUser> userList = spotUserDao.findByUserHrId(obj);
            userList.forEach(user -> user.setIsDelete(1));
            editList.addAll(userList);
        });

        spotUserDao.saveAll(editList);

        return ServiceResult.ok();
    }

    /**
     * 检查用户权限并开门
     *
     * @param userHrId 用户工号
     * @param deviceSn 设备SN
     * @return 结果
     * @author Yang.Lee
     * @date 2022/2/9 20:00
     **/
    @Override
    @Transactional
    public ServiceResult checkUserAccess(String userHrId, String deviceSn) {

        log.debug("判断开门权限。userId {}, deviceSn {}", userHrId, deviceSn);

        Optional<IotDevice> deviceOptional = deviceDao.findBySn(deviceSn);
        if (deviceOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_DEVICE, deviceSn), ErrConst.E01);
        }

        IotDevice device = deviceOptional.get();
        //List<AccessControlSpotUser> spotUserList = spotUserDao.findByAccessControlSpotDeviceIdAndUserHrId(device.getId(), userHrId);
        Optional<AccessControlSpot> accessControlSpotOptional = accessControlSpotDao.findByDeviceId(device.getId());
        if (accessControlSpotOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_ACCESS_CONTROL_SPOT, deviceSn), ErrConst.E01);
        }
        List<AccessControlSpotUser> spotUserList = spotUserDao.findAccessControlSpotUserByUserHrIdAndAccessControlSpot(userHrId, accessControlSpotOptional.get());

        if (spotUserList.isEmpty()) {
            return ServiceResult.error(ServiceMessage.PERMISSION_DENIED, ErrConst.E401);
        }

        boolean access = false;
        LocalDateTime now = LocalDateTime.now();
        for (AccessControlSpotUser accessControlSpotUser : spotUserList) {

            if (now.isBefore(accessControlSpotUser.getAccessStartTime()) || now.isAfter(accessControlSpotUser.getAccessEndTime())) {
                continue;
            }

            access = true;
        }

        if (!access) {
            return ServiceResult.error("开门失败，当前非用户通行时间", ErrConst.E401);
        }

        // 获取南通排班能力绑定的出口门禁点设备号列表
        List<String> ntScheduleOutDeviceSnList = getNtScheduleOutDeviceSnList();
        // 如果设备属于南通排班能力绑定的出口门禁点设备就校验排班信息来判断是否开门
        if (ntScheduleOutDeviceSnList.contains(deviceSn)) {
            return authService.openKHSTDoor(deviceSn, userHrId);
        }

        JSONObject reqJson = new JSONObject();
        reqJson.put("deviceId", deviceSn);

        // 权限判断成功，调用开门接口
        GroupBindingReq req = GroupBindingReq.builder()
                .chainSite("KS")
                .methodCode(MethodCode.OPEN_KSHT_DOOR.getName())
                .moduleCode(ModuleCode.KSHT.getName())
                .jsonArg(reqJson.toJSONString())
                .channelType("2")
                .build();
        log.debug("调用iotc开门接口，参数：{} " + JSON.toJSONString(req));
        JsonResult<Object> openResult = iotcApi.postToConn(req);
        log.debug("调用iotc开门接口，返回：{} " + openResult);
        if (openResult.getStatus() == 0) {
            return ServiceResult.error(openResult.getMsg(), openResult.getCode());
        }

        return ServiceResult.ok();
    }

    /**
     * 获取南通排班能力绑定的出口门禁点设备号列表
     *
     * @author wei.cheng
     * @date 2022/3/31 5:39 下午
     **/
    private List<String> getNtScheduleOutDeviceSnList() {
        List<String> list = new ArrayList<>();
        // 查询南通排班业务关联门禁点
        List<OpenAbilityAccessControlSpot> ntScheduleList = openAbilityAccessControlSpotDao.findByOpenAbilityAbility(Ability.NT_SCHEDULE);
        for (OpenAbilityAccessControlSpot openAbilityAccessControlSpot : ntScheduleList) {
            AccessControlSpot accessControlSpot = openAbilityAccessControlSpot.getSpot();
            if (Objects.isNull(accessControlSpot)) {
                continue;
            }
            if (!AccessControlInOut.OUT.equals(accessControlSpot.getInOut())) {
                continue;
            }
            IotDevice iotDevice = accessControlSpot.getDevice();
            if (Objects.isNull(iotDevice)) {
                continue;
            }
            if (list.contains(iotDevice.getSn())) {
                continue;
            }
            list.add(iotDevice.getSn());
        }
        return list;
    }

    /**
     * 批量移除用户权限
     *
     * @param userIdList 用户工号列表
     * @param spots      门禁点
     * @return 移除结果
     * @author Yang.Lee
     * @date 2022/3/1 15:29
     **/
    @Override
    @Transactional
    public ServiceResult removeUserAccess(List<String> userIdList, AccessControlSpot... spots) {

        List<AccessControlSpotUser> all = new ArrayList<>();
        userIdList.forEach(hrId -> {
            List<AccessControlSpotUser> spotUserList = spotUserDao.findByUserHrIdAndAccessControlSpotIdIn(hrId,
                    Arrays.stream(spots).map(AccessControlSpot::getId).collect(Collectors.toList()));

            all.addAll(spotUserList);
        });
        all.forEach(obj -> obj.setIsDelete(1));
        spotUserDao.saveAll(all);

        return ServiceResult.ok();
    }

    @Override
    public PageBean<AccessControlSpotPageResp> getAccessControlSpotPage(AccessControlSpotPageReq req) {

        Page<AccessControlSpot> accessControlSpotPage = accessControlSpotDao.findAll((root, query, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();

            if (StringUtils.isNotBlank(req.getProjectId())) {
                list.add(criteriaBuilder.equal(root.get("space").get("project").get("id"), req.getProjectId()));
            }

            if (StringUtils.isNotBlank(req.getSpaceId())) {
                list.add(criteriaBuilder.equal(root.get("space").get("id"), req.getSpaceId()));
            }

            if (StringUtils.isNotBlank(req.getName())) {
                String like = "%" + req.getName() + "%";
                list.add(criteriaBuilder.like(root.get("name"), like));
            }

            if (null != req.getIds() && !req.getIds().isEmpty()) {
                list.add(criteriaBuilder.not(criteriaBuilder.in(root.get("id")).value(req.getIds())));
            }


            list.add(criteriaBuilder.isNotNull(root.get("device")));

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, PageRequest.of(req.getPage(), req.getSize(), Sort.by(req.getOrders())));


        PageBean<AccessControlSpotPageResp> pageBean = new PageBean<>();
        pageBean.setPage(accessControlSpotPage.getPageable().getPageNumber());
        pageBean.setSize(accessControlSpotPage.getPageable().getPageSize());
        pageBean.setTotalPages(accessControlSpotPage.getTotalPages());
        pageBean.setTotalElements(accessControlSpotPage.getTotalElements());
        pageBean.setList(accessControlSpotPage.getContent()
                .stream()
                .map(AccessControlSpotPageResp::convert)
                .collect(Collectors.toList()));

        return pageBean;
    }

    private List<ProjectSpaceTreeResp> treeToList(List<ProjectSpaceTreeResp> messageList) {
        List<ProjectSpaceTreeResp> result = new ArrayList<>();
        for (ProjectSpaceTreeResp entity : messageList) {
            result.add(entity);
            List<ProjectSpaceTreeResp> childMsg = entity.getChildrenList();
            if (childMsg != null && !childMsg.isEmpty()) {
                List<ProjectSpaceTreeResp> entityList = this.treeToList(childMsg);
                result.addAll(entityList);
            }
        }

        return result;
    }

    /**
     * 查询我对门禁点拥有的权限信息
     *
     * @param token  当前token
     * @param spotId 门禁点ID
     * @author wei.cheng
     * @date 2022/3/9 9:59
     **/
    @Override
    public ServiceResult getMyPermissionOfAccessControlSpot(String token, String spotId, String doorMagnetismNo, String guestMobile) {
        log.info("start to get my permission of accessControlSpot, token:{}, spotId:{}, doorMagnetismNo:{}, guestMobile:{}", token, spotId, doorMagnetismNo,
                guestMobile);
        if (StringUtils.isEmpty(spotId) && StringUtils.isEmpty(doorMagnetismNo)) {
            return ServiceResult.error("spotId和doorMagnetismNo不能同时为空", ErrConst.E01);
        }
        GetMyPermissionOfAccessControlSpotResp resp = new GetMyPermissionOfAccessControlSpotResp();
        resp.setIsAccess(false);
        resp.setAccessControlSpotApproverList(Lists.newArrayList());
        //获取门禁点
        Optional<AccessControlSpot> accessControlSpotOptional;
        if (StringUtils.isNotEmpty(spotId)) {
            accessControlSpotOptional = accessControlSpotDao.findById(spotId);
        } else {
            // 验证门磁是否存在
            Optional<DoorMagnetic> doorMagneticOptional = doorMagneticDao.findByNumber(doorMagnetismNo);
            if (doorMagneticOptional.isEmpty()) {
                log.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUNT_DOOR_MAGNETISM, doorMagnetismNo));
                return ServiceResult.ok(resp);
            }
            DoorMagnetic doorMagnetic = doorMagneticOptional.get();
            // 判断门磁是否绑定门禁点
            accessControlSpotOptional = accessControlSpotDao.findByDeviceId(doorMagnetic.getDevice().getId());
            if (accessControlSpotOptional.isEmpty()) {
                log.error("该门磁未绑定门禁点，无法开门");
                return ServiceResult.ok(resp);
            }
            spotId = accessControlSpotOptional.get().getId();
        }
        //门禁点设值
        accessControlSpotOptional.ifPresent(accessControlSpot -> resp.setAccessControlSpot(AccessControlSpotPageResp.convert(accessControlSpot)));

        //获取当前用户信息
        SimpleUserBO user = getSimpleUserBO(token, guestMobile);
        if (Objects.isNull(user)) {
            return ServiceResult.ok(resp);
        }
        String hrId = user.getHrId();
        //判断用户是否是访客类型
        boolean isVistor = SysUserService.isVisitor(hrId);

        //当前用户信息设值
        AccessControlUserResp accessControlUserResp = AccessControlUserResp.convert(user);
        accessControlUserResp.setPersonType(isVistor ? 2 : 1);
        resp.setCurrentUser(accessControlUserResp);

        if (StringUtils.isNotEmpty(spotId)) {
            //获取用户对门禁点最近一次申请
            AccessControlSpotApplicationRecord accessControlSpotApplicationRecord = accessControlSpotApplicationRecordDao.
                    findFirstByApplicantHrIdAndSpotIdOrderByCreatedTimeDesc(hrId, spotId);
            if (Objects.nonNull(accessControlSpotApplicationRecord)) {
                resp.setLatestAccessControlSpotApplicationRecord(AccessControlSpotApplicationRecordResp.convert(accessControlSpotApplicationRecord));
            }
        }
        if (accessControlSpotOptional.isEmpty()) {
            return ServiceResult.ok(resp);
        }
        //查询用户对与门禁点具有的通行时间段
        List<AccessControlSpotUser> spotUserList = spotUserDao.findByAccessControlSpotIdAndUserHrId(spotId, hrId);
        //判断用户是否具有通过门禁点的权限
        boolean isAccess = isAccess(spotUserList);
        resp.setIsAccess(isAccess);
        //设置用户对门禁点通行时间段
        if (!CollectionUtils.isEmpty(spotUserList)) {
            resp.setAccessTimeList(spotUserList.stream().sorted((o1, o2) -> ObjectUtils.compare(o2.getCreatedTime(), o1.getCreatedTime()))
                    .map(AccessTimeResp::convert).collect(Collectors.toList()));
        }
        if (!isAccess && isVistor) {
            // 访客无权限申请
            return ServiceResult.ok(resp);
        }
        //门禁点分配人设值
        List<SysUser> accessContrlSpotApproverList = getAccessControlSpotApproverList(spotId, hrId);
        if (!CollectionUtils.isEmpty(accessContrlSpotApproverList)) {
            resp.setAccessControlSpotApproverList(accessContrlSpotApproverList.stream().map(StaffResp::convert).collect(Collectors.toList()));
        } else {
            resp.setAccessControlSpotApproverList(new ArrayList<>());
        }
        if (Objects.nonNull(resp.getCurrentUser()) &&
                AccessControlSpotType.FACE_MACHINE.equals(accessControlSpotOptional.get().getAccessControlSpotType())) {
            resp.getCurrentUser().setFaceImg(getFaceImg(hrId));
        }
        return ServiceResult.ok(resp);
    }

    @Override
    public ServiceResult doorMagneticOpenDoor(String token, String spotId, String doorMagnetismNo, String guestMobile) {
        log.info("start to open doorMagnetic, token:{}, spotId:{}, doorMagnetismNo:{}, guestMobile:{}", token, spotId, doorMagnetismNo,
                guestMobile);
        if (StringUtils.isEmpty(spotId) && StringUtils.isEmpty(doorMagnetismNo)) {
            return ServiceResult.error("spotId和doorMagnetismNo不能同时为空", ErrConst.E01);
        }
        //获取门禁点
        Optional<AccessControlSpot> accessControlSpotOptional;
        if (StringUtils.isNotEmpty(spotId)) {
            accessControlSpotOptional = accessControlSpotDao.findById(spotId);
            if (accessControlSpotOptional.isEmpty()) {
                log.error("门禁点不存在，spotId:{}", spotId);
                return ServiceResult.error("门禁点不存在");
            }
            AccessControlSpot accessControlSpot = accessControlSpotOptional.get();
            IotDevice device = accessControlSpot.getDevice();
            if (Objects.nonNull(device)) {
                Optional<DoorMagnetic> doorMagneticOptional = doorMagneticDao.findByDeviceId(device.getId());
                if (doorMagneticOptional.isPresent()) {
                    doorMagnetismNo = doorMagneticOptional.get().getNumber();
                } else {
                    log.error("门禁点未绑定门磁");
                    return ServiceResult.error("门禁点未绑定门磁");
                }
            } else {
                log.error("门禁点未绑定设备，spotId:{}", spotId);
                return ServiceResult.error("门禁点未绑定设备");
            }
        } else {
            // 验证门磁是否存在
            Optional<DoorMagnetic> doorMagneticOptional = doorMagneticDao.findByNumber(doorMagnetismNo);
            if (doorMagneticOptional.isEmpty()) {
                log.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUNT_DOOR_MAGNETISM, doorMagnetismNo));
                return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUNT_DOOR_MAGNETISM, doorMagnetismNo));
            }
            DoorMagnetic doorMagnetic = doorMagneticOptional.get();
            // 判断门磁是否绑定门禁点
            accessControlSpotOptional = accessControlSpotDao.findByDeviceId(doorMagnetic.getDevice().getId());
            if (accessControlSpotOptional.isEmpty()) {
                log.error("门磁未绑定门禁点，无法开门");
                return ServiceResult.error("门磁未绑定门禁点，无法开门");
            }
            spotId = accessControlSpotOptional.get().getId();
        }

        if (StringUtils.isBlank(doorMagnetismNo) || StringUtils.isBlank(spotId)) {
            log.error("门磁无效");
            return ServiceResult.error("门磁无效");
        }

        //获取当前用户信息
        SimpleUserBO user = getSimpleUserBO(token, guestMobile);
        if (Objects.isNull(user)) {
            return ServiceResult.error("无权限");
        }
        try {
            ServiceResult serviceResult = qrCodeService.scanStaticQRCode(doorMagnetismNo, user.getHrId());
            if (!serviceResult.isSuccess()) {
                log.error("开门失败，msg:{}", JSONObject.toJSONString(serviceResult));
                return serviceResult;
            }
        } catch (Exception e) {
            log.error("开门失败，msg:{}", e.getMessage());
            return ServiceResult.error("开门失败");
        }
        return ServiceResult.ok("已开门");
    }

    /**
     * 从token或者访客号码获取用户
     *
     * @param token
     * @param guestMobile
     * @author wei.cheng
     * @date 2022/4/2 1:32 下午
     **/
    private SimpleUserBO getSimpleUserBO(String token, String guestMobile) {
        if (StringUtils.isNotBlank(token)) {
            SysUser sysUser = sysUserServiceUC.getUserByToken(token);
            if (Objects.nonNull(sysUser)) {
                return SimpleUserBO.convert(sysUser);
            }
        }
        if (StringUtils.isNotBlank(guestMobile)) {
            Optional<Guest> guestOptional = guestService.getByMobile(guestMobile);
            if (guestOptional.isPresent()) {
                return SimpleUserBO.convert(guestOptional.get());
            }
        }
        return null;
    }

    @Override
    public String getFaceImg(String hrId) {
        String faceImg = null;
        // 构建iotc构建参数
        GroupBindingReq req = GroupBindingReq.builder()
                .moduleCode(ModuleCode.HIK.getName())
                .channelType("1")
                .jsonArg(JSON.toJSONString(FindPersonPicReq.builder().jobNos(Lists.newArrayList(hrId)).build()))
                .chainSite("KS")
                .methodCode(MethodCode.FIND_PERSON_PIC.getName())
                .build();
        try {
            log.debug("req:{}", JSONObject.toJSONString(req));
            JsonResult<Object> resp = iotcApi.postToConn(req);
            if (resp.getStatus() == 0) {
                log.error("请求IOTC获取员工人脸照片失败, msg:{}", resp.getMsg());
            } else {
                Map<String, String> map = (Map<String, String>) resp.getData();
                String hkFaceUrl = map.get(hrId);
                if (StringUtils.isNotBlank(hkFaceUrl)) {
                    faceImg = hkFaceUrl;
                } else {
                    log.warn("请求IOTC获取员工人脸照片未找到");
                }
            }
        } catch (Exception e) {
            log.error("请求IOTC获取员工人脸照片失败", e);
        }
        // 如果从IOTC没有获取人脸照片就看看redis中有没有缓存照片，不能保证是最新的人脸照片
        if (Objects.isNull(faceImg)) {
            String faceKey = redisUtil.createRealKey("face-image-data:" + hrId);
            if (redisUtil.hasKey(faceKey)) {
                try {
                    faceImg = (String) redisUtil.get(faceKey);
                } catch (Exception e) {
                    log.error("从redis获取人脸照片异常，hrId:{}", hrId);
                }
            }
        }
        if (Objects.nonNull(faceImg)) {
            // 校验人脸照片是否有效
            String redisKey = redisUtil.createRealKey(VALID_FACE_URL_PREFIX + faceImg);
            if (!redisUtil.hasKey(redisKey)) {
                try {
                    JsonResult<Object> resp = checkFaceImageApi.checkFaceImage(faceImg);
                    if (resp.getStatus() == 0) {
                        log.warn("IOTC 校验人脸照片失败，imageUrl: {}，结果：{}", faceImg, resp);
                        // 人脸校验失败不返回无效人脸
                        faceImg = null;
                    } else {
                        //将人脸照片存入缓存，已便从IOTC那儿查询照片失败时做的容错处理
                        redisUtil.set(redisUtil.createRealKey("face-image-data:" + hrId), faceImg);
                        //将有效的人脸照片存入缓存，以免短时间内校验相同的照片
                        redisUtil.set(redisKey, "", RedisUtil.FIFTEEN_MIN_SECOND);
                    }
                } catch (Exception e) {
                    log.warn("IOTC 校验人脸照片失败，imageUrl: {}，异常：{}", faceImg, e.getMessage());
                    // 人脸校验失败不返回无效人脸
                    faceImg = null;
                }
            }
        }
        return faceImg;
    }

    @Override
    public List<SysUser> getAccessControlSpotApproverList(String spotId, String hrId) {
        boolean isVistor = SysUserService.isVisitor(hrId);
        List<SysUser> sysUserList = new ArrayList<>();
        List<AccessControlSpotApproverRelation> accessControlSpotApproverRelationList = accessControlSpotApproverRelationDao.findAllBySpotId(spotId);
        if (!CollectionUtils.isEmpty(accessControlSpotApproverRelationList)) {
            List<String> userHrIdList = accessControlSpotApproverRelationList.stream().map(AccessControlSpotApproverRelation::getUserHrId)
                    .collect(Collectors.toList());
            List<AccessControlSpotApprover> accessControlSpotApproverList = accessControlSpotApproverDao.findAllByUserHrIdIn(userHrIdList);
            List<String> accessControlSpotApproverHrIdList = accessControlSpotApproverList.stream().filter(a -> a.getApproveType()
                            .contains(isVistor ? ApproverType.LABOR.name() : ApproverType.STAFF.name())).map(AccessControlSpotApprover::getUserHrId)
                    .collect(Collectors.toList());
            sysUserList = accessControlSpotApproverHrIdList.stream().map(id -> sysUserServiceUC.getUserByEmpNo(id))
                    .filter(Objects::nonNull).collect(Collectors.toList());
        }
        return sysUserList;
    }

    /**
     * 判断员工当前是否具有通过门禁点的权限
     *
     * @param spotId 门禁点ID
     * @param hrId   员工工号
     * @return true: 有权限；false：无权限
     * @author wei.cheng
     * @date 2022/3/9 10:04
     **/
    @Override
    public boolean isAccess(String spotId, String hrId) {
        // 判断人员是否拥有门禁点通行权限
        List<AccessControlSpotUser> spotUserList = spotUserDao.findByAccessControlSpotIdAndUserHrId(spotId, hrId);
        return isAccess(spotUserList);
    }

    /**
     * 判断员工当前是否具有通过门禁点的权限
     *
     * @param spotUserList
     * @author wei.cheng
     * @date 2022/3/17 5:22 下午
     **/
    private boolean isAccess(List<AccessControlSpotUser> spotUserList) {
        boolean isAccess = false;
        if (!CollectionUtils.isEmpty(spotUserList)) {
            LocalDateTime now = LocalDateTime.now();
            for (AccessControlSpotUser obj : spotUserList) {
                if (now.isAfter(obj.getAccessEndTime()) || now.isBefore(obj.getAccessStartTime())) {
                    continue;
                }
                isAccess = true;
            }
        }
        return isAccess;
    }

    @Override
    public ServiceResult updateUserFace(String token, String spotId, String guestMobile, MultipartFile file) {
        log.info("start to update user face, token:{}, spotId:{}, guestMobile:{}, fileName:{}", token, spotId, guestMobile, file.getOriginalFilename());
        SimpleUserBO userBo = getSimpleUserBO(token, guestMobile);
        if (Objects.isNull(userBo)) {
            return ServiceResult.error("无权限", ErrConst.E01);
        }
        String hrId = userBo.getHrId();
        Optional<AccessControlSpot> accessControlSpotOptional = accessControlSpotDao.findById(spotId);
        //获取门禁点
        if (accessControlSpotOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_ACCESS_CONTROL_SPOT, spotId), ErrConst.E01);
        }
        if (!Objects.equals(AccessControlSpotType.FACE_MACHINE, accessControlSpotOptional.get().getAccessControlSpotType())) {
            return ServiceResult.error("只有面板机类型的门禁点才能更新人脸", ErrConst.E01);
        }
        List<AccessControlSpotUser> accessControlSpotUserList = accessControlSpotUserDao.findByUserHrId(hrId);
        List<String> spotIdList = new ArrayList<>();
        LocalDateTime startTime = null;
        LocalDateTime endTime = null;
        List<String> deviceSnList = new ArrayList<>();
        for (AccessControlSpotUser accessControlSpotUser : accessControlSpotUserList) {
            AccessControlSpot accessControlSpot = accessControlSpotUser.getAccessControlSpot();
            if (Objects.isNull(accessControlSpot)) {
                continue;
            }
            if (!spotIdList.contains(accessControlSpot.getId())) {
                spotIdList.add(accessControlSpot.getId());
            }
            IotDevice iotDevice = accessControlSpot.getDevice();
            if (Objects.isNull(iotDevice)) {
                continue;
            }
            if (Objects.equals(DeviceType.FACE_RECOGNITION, iotDevice.getDeviceType()) ||
                    Objects.equals(DeviceType.HIK_FACE_MACHINE, iotDevice.getDeviceType())) {
                if (Objects.isNull(startTime) || startTime.isAfter(accessControlSpotUser.getAccessStartTime())) {
                    startTime = accessControlSpotUser.getAccessStartTime();
                }
                if (Objects.isNull(endTime) || endTime.isBefore(accessControlSpotUser.getAccessEndTime())) {
                    endTime = accessControlSpotUser.getAccessEndTime();
                }
                if (StringUtils.isNotBlank(iotDevice.getSn()) && !deviceSnList.contains(iotDevice.getSn())) {
                    deviceSnList.add(iotDevice.getSn());
                }
            }
        }
        //判断用户是否具有更新此门禁点人脸权限
        if (!spotIdList.contains(spotId)) {
            return ServiceResult.error(ServiceMessage.PERMISSION_DENIED, ErrConst.E01);
        }
        ServiceResult result = uploadService.uploadAndCheckFace(file);
        if (!result.isSuccess()) {
            log.error("人脸照片上传失败，{}", result.getObject());
            return ServiceResult.error("人脸照片上传失败：" + result.getObject(), result.getErrorCode());
        }
        UploadResp uploadResp = (UploadResp) result.getObject();
        if (!CollectionUtils.isEmpty(deviceSnList) && Objects.nonNull(startTime) && Objects.nonNull(endTime)) {
            AccessPermissions accessPermissions = new AccessPermissions();
            accessPermissions.setReturnTopic(Topic.get(Topic.FACE_TO_IOTC_RETURN));
            accessPermissions.setSourceName("IBMS");
            accessPermissions.setResourceId("更新面板机人脸数据");
            accessPermissions.setMethodName(AccessPermissions.Method.UPDATE.getName());
            accessPermissions.setModuleName(AccessPermissions.Model.PERSON.getName());
            accessPermissions.setDeviceCodeList(deviceSnList);
            AccessPermissions.User user = new AccessPermissions.User();
            user.setJobNo(userBo.getHrId());
            user.setName(userBo.getName());
            user.setPhoto(uploadResp.getUrl());
            user.setGroupIds(getKsHtGroupIdListOfUser(hrId));
            user.setVisitedWorkNo("");
            user.setCreateTime(DateUtils.formatLocalDateTime(startTime, DateUtils.PATTERN_DATETIME));
            user.setStartTime(DateUtils.formatZoneDateTime(startTime.atZone(ZoneId.of(DateUtils.GMT8)), DateUtils.PATTERN_DATETIME_WITH_ZONE));
            user.setEndTime(DateUtils.formatZoneDateTime(endTime.atZone(ZoneId.of(DateUtils.GMT8)), DateUtils.PATTERN_DATETIME_WITH_ZONE));
            user.setPersonType(SysUserService.isVisitor(userBo.getHrId()) ? AccessPermissions.PersonType.VISITOR : AccessPermissions.PersonType.EMPLOYEE);
            accessPermissions.setUserList(Lists.newArrayList(user));
            accessPermissions.setOver("1");
            ServiceResult serviceResult = iotcService.updateFace(accessPermissions);
            if (!serviceResult.isSuccess()) {
                log.error("人脸照片上传失败，{}", serviceResult.getObject());
                return ServiceResult.error("人脸照片上传失败", ErrConst.E01);
            }
        }
        //由于系统暂未保存员工的人脸数据，暂临时保存在redis
        String faceKey = redisUtil.createRealKey("face-image-data:" + hrId);
        redisUtil.set(faceKey, uploadResp.getUrl());
        return result;
    }

    /**
     * 获取用户绑定的旷世鸿图人员组
     *
     * @param hrId
     * @return
     */
    private List<String> getKsHtGroupIdListOfUser(String hrId) {
        boolean isVisitor = SysUserService.isVisitor(hrId);
        List<String> groupIdList = new ArrayList<>();
        List<AccessControlSpotUser> accessControlSpotUserList = accessControlSpotUserDao.findByUserHrId(hrId);
        LocalDateTime now = LocalDateTime.now();
        accessControlSpotUserList.removeIf(obj -> {
            // 已过期权限的门禁点不给用户绑定该门禁点的人员组
            if (now.isAfter(obj.getAccessEndTime())) {
                return true;
            } else {
                return false;
            }
        });
        if (!CollectionUtils.isEmpty(accessControlSpotUserList)) {
            List<String> accessControlSpotIdList = accessControlSpotUserList.stream().map(AccessControlSpotUser::getAccessControlSpot)
                    .map(AccessControlSpot::getId).distinct().collect(Collectors.toList());
            List<AccessControlSpotKSHTMemberGroup> allCoverGroupList = accessControlSpotKSHTMemberGroupDao.
                    findByAccessControlSpotIdIn(accessControlSpotIdList);
            allCoverGroupList.removeIf(g -> {
                if (isVisitor) {
                    return KSHTMemberGroupType.STAFF.equals(g.getGroupType()) || KSHTMemberGroupType.MEAL_STAFF.equals(g.getGroupType());
                } else {
                    return KSHTMemberGroupType.VISITOR.equals(g.getGroupType()) || KSHTMemberGroupType.MEAL_VISITOR.equals(g.getGroupType());
                }
            });
            if (!CollectionUtils.isEmpty(allCoverGroupList)) {
                for (AccessControlSpotKSHTMemberGroup g : allCoverGroupList) {
                    if (!groupIdList.contains(g.getId())) {
                        groupIdList.add(g.getId());
                    }
                }
            }
        }
        return groupIdList;
    }

    @Override
    @Transactional
    public ServiceResult userBatchAdd(MultipartFile file, List<String> spotIds) {

        //门禁id为空
        if (spotIds.isEmpty()) {
            return ServiceResult.error("未选择上传门禁点", ErrConst.E00001);
        }
        // 判断门禁点当前是否有正在进行的批量导入任务
        List<String> keys = spotIds.stream().map(spotId -> redisUtil.createRealKey("batch-add-user:spotId:" + spotId)).collect(Collectors.toList());
        for (String key : keys) {
            if (redisUtil.hasKey(key)) {
                return ServiceResult.error("门禁点当前已存在批量导入任务，请稍后再进行导入");
            }
        }
        try {
            // 门禁点批量导入时进行加锁
            keys.forEach(key -> redisUtil.set(key, "", 600));

            //根据设备ID获取门禁点信息
            List<AccessControlSpot> accessControlSpotList = accessControlSpotDao.findAllById(spotIds);
            //获取所有门禁点ID
            List<String> accessControlSpotIdList = accessControlSpotList.stream()
                    .map(AccessControlSpot::getId)
                    .collect(Collectors.toList());

            // hk设备code 门禁ID
            List<String> hkDeviceCodeList = new ArrayList<>();
            List<String> hkAccessControlIdList = new ArrayList<>();
            // ks设备code 门禁点
            List<String> ksDeviceCodeList = new ArrayList<>();
            List<AccessControlSpot> ksAccessControlList = new ArrayList<>();

            deviceCodes(accessControlSpotList, hkDeviceCodeList, ksDeviceCodeList, hkAccessControlIdList, ksAccessControlList);


            ServiceResult readExcelResult;
            //读取excel文件
            try {
                readExcelResult = excelService.readAddAccessRulesUserExcel(file);
                if (!readExcelResult.isSuccess() && ErrConst.E23.equals(readExcelResult.getErrorCode())) {
                    return readExcelResult;
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                return ServiceResult.error("excel文件内容格式异常");
            }

            // 所有上传的数据行
            List<String[]> dataList = (List<String[]>) readExcelResult.getObject();
            if (dataList.size() > batchAddMaxLimit) {
                return ServiceResult.error(String.format("数据上传过多,请分批次上传,每次最多%s人", batchAddMaxLimit));
            }

            // 数据总数
            int totalCount = dataList.size();
            // 有错误的数据集合
            List<String[]> errorList = getAddErrorExcelData(dataList);
            // 所有数据去除错误数据，剩下的就是正常数据
            dataList.removeAll(errorList);

            //所有人员list
            List<UserDataReq> list = new ArrayList<>();
            for (String[] data : dataList) {
                UserDataReq userDataReq = new UserDataReq();
                userDataReq.setName(data[0]);
                userDataReq.setEmpNo(data[1]);
                DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime startTime = LocalDateTime.parse(data[3], df);
                LocalDateTime endTime = LocalDateTime.parse(data[4], df);
                userDataReq.setStartTime(startTime);
                userDataReq.setEndTime(endTime);
                list.add(userDataReq);
            }

            //员工list
            List<UserDataReq> staffList = new ArrayList<>();
            //访客list
            List<UserDataReq> visitorList = new ArrayList<>();

            for (UserDataReq userDataReq : list) {
                if (SysUserService.isVisitor(userDataReq.getEmpNo())) {
                    UserDataReq userDataReqVisitor = new UserDataReq();
                    BeanUtils.copyProperties(userDataReq, userDataReqVisitor);
                    visitorList.add(userDataReqVisitor);
                } else {
                    UserDataReq userDataReqStaff = new UserDataReq();
                    BeanUtils.copyProperties(userDataReq, userDataReqStaff);
                    staffList.add(userDataReqStaff);
                }
            }

            //需要向ks设备添加人员 创建人员组
            if (!ksAccessControlList.isEmpty()) {

                for (AccessControlSpot accessControlSpot : ksAccessControlList) {
                    // 判断是九霄设备还是鸿图设备
                    if (IotDeviceService.isKSHTDevice(accessControlSpot.getDevice())) {
                        //查询鸿图员工组是否存 员工组类型"员工组"
                        Optional<AccessControlSpotKSHTMemberGroup> KSHTMemberGroupStaff = accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotIdAndGroupTypeAndOpenAbilityAbility(accessControlSpot.getId(), KSHTMemberGroupType.STAFF, Ability.OTHER);
                        //查询鸿图员工组是否存 员工组类型"访客"
                        Optional<AccessControlSpotKSHTMemberGroup> KSHTMemberGroupVisitor = accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotIdAndGroupTypeAndOpenAbilityAbility(accessControlSpot.getId(), KSHTMemberGroupType.VISITOR, Ability.OTHER);

                        //添加人员中有员工 没有鸿图员工组
                        if (!staffList.isEmpty() && KSHTMemberGroupStaff.isEmpty()) {
                            //创建鸿图员工类型 员工组 并绑定设备
                            accessControlSpotService.createkshtGroupStaff(accessControlSpot, staffList);
                        }

                        //添加人员中有访客 没有鸿图访客组
                        if (!visitorList.isEmpty() && KSHTMemberGroupVisitor.isEmpty()) {
                            //创建鸿图访客类型 员工组 并绑定设备
                            accessControlSpotService.createKshtGroupVisitor(accessControlSpot, visitorList);
                        }

                    } else {
                        //九霄
                        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                        return ServiceResult.error("旷视九霄设备请在人员组增加权限");
                    }
                }
            }


            //所有添加人员的OpenAbilityMessage.User
            List<OpenAbilityMessage.User> openAbilityMessageUserList = new ArrayList<>();
            for (String[] data : dataList) {
                OpenAbilityMessage.User user = new OpenAbilityMessage.User();
                user.setName(data[0]);
                user.setHrId(data[1]);
                DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime startTime = LocalDateTime.parse(data[3], df);
                LocalDateTime endTime = LocalDateTime.parse(data[4], df);
                user.setStartTime(startTime);
                user.setEndTime(endTime);
                openAbilityMessageUserList.add(user);
            }

            //员工OpenAbilityMessage.User list
            List<OpenAbilityMessage.User> staffUserList = new ArrayList<>();
            //访客OpenAbilityMessage.User list
            List<OpenAbilityMessage.User> visitorUserList = new ArrayList<>();

            for (OpenAbilityMessage.User user : openAbilityMessageUserList) {
                if (!SysUserService.isVisitor(user.getHrId())) {
                    OpenAbilityMessage.User staffUser = new OpenAbilityMessage.User();
                    BeanUtils.copyProperties(user, staffUser);
                    staffUserList.add(staffUser);
                } else {
                    OpenAbilityMessage.User visitorUser = new OpenAbilityMessage.User();
                    BeanUtils.copyProperties(user, visitorUser);
                    visitorUserList.add(visitorUser);
                }
            }

            List<IotDevice> deviceList = accessControlSpotList.stream()
                    .filter(spot -> spot.getDevice() != null)
                    .map(AccessControlSpot::getDevice)
                    .collect(Collectors.toList());

            //创建task
            OpenAbilityTask task = createTask(Ability.OTHER, openAbilityMessageUserList, deviceList, TaskType.MQ, MethodType.ADDBATCH);
            //将数据写入门禁点
            List<AccessControlSpotUserReq> userList = new ArrayList<>();
            for (UserDataReq userDataReq : list) {
                AccessControlSpotUserReq req = new AccessControlSpotUserReq();
                req.setEmpNo(userDataReq.getEmpNo());
                req.setName(userDataReq.getName());
                req.setVip(false);
                req.setStartTime(userDataReq.getStartTime());
                req.setEndTime(userDataReq.getEndTime());
                userList.add(req);
            }
            accessControlSpotService.addUsers(userList, Ability.ADMINISTRATOR_ADD, new ArrayList<>(accessControlSpotList).toArray(new AccessControlSpot[0]));

            if (!CollectionUtils.isEmpty(hkDeviceCodeList)) {
                // 需要绑定海康设备的人员列表
                List<AccessPermissions.User> users = new ArrayList<>();
                if (!staffUserList.isEmpty()) {
                    staffUserList.forEach(obj -> {
                        AccessPermissions.User user = AccessPermissions.User.convert(obj, null);
                        users.add(user);
                    });
                }
                if (!visitorUserList.isEmpty()) {
                    visitorUserList.forEach(obj -> {
                        AccessPermissions.User user = AccessPermissions.User.convert(obj, null);
                        users.add(user);
                    });
                }
                if (!CollectionUtils.isEmpty(users)) {
                    //创建需要发送mq的消息
                    AccessPermissions message = new AccessPermissions();
                    message.setDeviceCodeList(hkDeviceCodeList);
                    message.setResourceId(task.getId());
                    message.setReturnTopic(Topic.get(Topic.USER_BIND_RETURN));
                    message.setUserList(users);
                    message.setSourceName("IBMS");
                    message.setMethodName(AccessPermissions.Method.BIND.getName());
                    message.setModuleName(AccessPermissions.Model.PERSON.getName());
                    message.setOver("1");

                    log.debug("发送mq员工参数{}", message);
                    iotcService.bindUserWithDevices(message);
                }
            }

            if (!CollectionUtils.isEmpty(ksDeviceCodeList)) {
                //员工平台已有 发送绑定mq
                List<AccessPermissions.User> staffUsers = new ArrayList<>();
                if (!staffUserList.isEmpty()) {
                    //查询所有的 旷世 员工人员组
                    List<AccessControlSpotKSHTMemberGroup> staffGroupList = accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotIdInAndGroupTypeAndOpenAbilityAbility(accessControlSpotIdList, KSHTMemberGroupType.STAFF, Ability.OTHER);
                    staffUserList.forEach(obj -> {
                        //发送mq
                        AccessPermissions.User user = AccessPermissions.User.convert(obj, staffGroupList.stream()
                                .map(AccessControlSpotKSHTMemberGroup::getId)
                                .collect(Collectors.toList()));
                        staffUsers.add(user);

                    });

                    //创建需要发送mq的消息
                    AccessPermissions message = new AccessPermissions();
                    message.setDeviceCodeList(ksDeviceCodeList);
                    message.setResourceId(task.getId());
                    message.setReturnTopic(Topic.get(Topic.USER_BIND_RETURN));
                    message.setUserList(staffUsers);
                    message.setSourceName("IBMS");
                    message.setMethodName(AccessPermissions.Method.INCREMENT_BIND.getName());
                    message.setModuleName(AccessPermissions.Model.PERSON.getName());
                    message.setOver("1");

                    log.debug("发送mq员工参数{}", message);
                    iotcService.bindUserWithDevices(message);
                }

                //访客平台已有 发送绑定mq
                List<AccessPermissions.User> visitorUsers = new ArrayList<>();
                if (!visitorUserList.isEmpty()) {
                    //查询所有的 旷视 访客员工组
                    List<AccessControlSpotKSHTMemberGroup> VisitorGroupList = accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotIdInAndGroupTypeAndOpenAbilityAbility(accessControlSpotIdList, KSHTMemberGroupType.VISITOR, Ability.OTHER);

                    visitorUserList.forEach(obj -> {
                        //发送mq
                        AccessPermissions.User user = AccessPermissions.User.convert(obj, VisitorGroupList.stream()
                                .map(AccessControlSpotKSHTMemberGroup::getId)
                                .collect(Collectors.toList()));
                        visitorUsers.add(user);
                    });

                    //创建需要发送mq的消息
                    AccessPermissions message = new AccessPermissions();
                    message.setDeviceCodeList(ksDeviceCodeList);
                    message.setResourceId(task.getId());
                    message.setReturnTopic(Topic.get(Topic.USER_BIND_RETURN));
                    message.setUserList(visitorUsers);
                    message.setSourceName("IBMS");
                    message.setMethodName(AccessPermissions.Method.INCREMENT_BIND.getName());
                    message.setModuleName(AccessPermissions.Model.PERSON.getName());
                    message.setOver("1");

                    log.debug("发送mq访客参数{}", message);
                    iotcService.bindUserWithDevices(message);
                }
            }

            String uuid = UUID.randomUUID().toString();
            if (!errorList.isEmpty()) {
                // 将失败数据存入缓存，供用户下载
                String redisKey = redisUtil.createRealKey("AccessRulesImportError-" + uuid);
                redisUtil.set(redisKey, errorList, RedisUtil.ONE_DAY_SECOND);
            }
            if (!dataList.isEmpty()) {
                //将正确数据存入缓存
                String redisKey = redisUtil.createRealKey("AccessRulesImportSuccess-" + uuid);
                redisUtil.set(redisKey, dataList, RedisUtil.ONE_DAY_SECOND);
            }
            return ServiceResult.ok(StaffGroupImportResp.builder()
                    .totalCount(totalCount)
                    .successCount(dataList.size())
                    .errorCount(errorList.size())
                    .errorDataId(uuid)
                    .taskId(task.getId())
                    .build());
        } finally {
            TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
                @Override
                public void afterCommit() {
                    // 事务提交后删除加的锁
                    keys.forEach(key -> redisUtil.del(key));
                }
            });
        }
    }

    public OpenAbilityTask createTask(Ability ability, List<OpenAbilityMessage.User> userList, List<IotDevice> deviceList, TaskType type, MethodType methodType) {

        // 创建 task
        OpenAbilityTask task = new OpenAbilityTask();

        task.setAbility(ability);
        task.setStartTime(LocalDateTime.now());
        task.setStatus(AbilityTaskStatus.DOING);
        task.setTopic(Topic.BIZ_TO_IOTC);
        task.setType(type);
        task.setMethodType(methodType);

        // 创建 task detail
        List<OpenAbilityTaskDetail> allDetail = new ArrayList<>();
        deviceList.forEach(device -> {

            List<OpenAbilityTaskDetail> detailList = userList.stream().map(obj -> {
                OpenAbilityTaskDetail openAbilityTaskDetail = new OpenAbilityTaskDetail();

                openAbilityTaskDetail.setTask(task);
                openAbilityTaskDetail.setDevice(device);
                openAbilityTaskDetail.setUserHrId(obj.getHrId());
                openAbilityTaskDetail.setUserName(obj.getName());
                openAbilityTaskDetail.setUserImportStatus(UserImportStatus.DOING);

                return openAbilityTaskDetail;
            }).collect(Collectors.toList());

            allDetail.addAll(detailList);
        });

        //手动提交事物
        DefaultTransactionDefinition transactionDefinition = new DefaultTransactionDefinition();
        transactionDefinition.setName("ChansonTransaction");
        //默认:TransactionDefinition.ISOLATION_DEFAULT
        transactionDefinition.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
        //默认:TransactionDefinition.PROPAGATION_REQUIRED
        transactionDefinition.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
        TransactionStatus txStatus = platformTransactionManager.getTransaction(transactionDefinition);
        try {
            openAbilityTaskDao.save(task);
            openAbilityTaskDetailDao.saveAll(allDetail);
        } catch (DataAccessException e) {
            platformTransactionManager.rollback(txStatus);
            throw e;
        } catch (Exception e) {
            platformTransactionManager.rollback(txStatus);
            throw e;
        }
        platformTransactionManager.commit(txStatus);


        return task;
    }

    private List<String[]> getAddErrorExcelData(List<String[]> dataList) {

        //员工list
        List<String[]> staffList = new ArrayList<>();
        //访客list
        List<String[]> visitorList = new ArrayList<>();

        MultiValueMap<String[], String> errorMap = new LinkedMultiValueMap<>();

        for (String[] data : dataList) {

            // 判断工号不能为空
            if (StringUtils.isBlank(data[1])) {
                errorMap.add(data, "工号不能为空");
            }

            // 验证时间
            if (!StringUtils.isBlank(data[3]) && !StringUtils.isBlank(data[4])) {
                if (isLegalDate(data.length, data[3], "yyyy-MM-dd HH:mm:ss") || isLegalDate(data.length, data[4], "yyyy-MM-dd HH:mm:ss")) {
                    errorMap.add(data, "时间格式不正确");
                } else {
                    DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                    LocalDateTime startTime = LocalDateTime.parse(data[3], df);
                    LocalDateTime endTime = LocalDateTime.parse(data[4], df);
                    if (startTime.isAfter(endTime)) {
                        errorMap.add(data, "开始时间不能大于结束时间");
                    }
                    if (!endTime.isAfter(LocalDateTime.now())) {
                        errorMap.add(data, "结束时间不能小于当前时间");
                    }
                }
            } else {
                errorMap.add(data, "时间不能为空");
            }

            //验证人员类型
            if (!StringUtils.isBlank(data[2])) {
                if (SysUserService.isVisitor(data[1]) && !"访客".equals(data[2])) {
                    errorMap.add(data, "人员类型访客不正确");
                } else if (!SysUserService.isVisitor(data[1]) && !"员工".equals(data[2])) {
                    errorMap.add(data, "人员类型不正确");
                }
            } else if (StringUtils.isBlank(data[2])) {
                errorMap.add(data, "人员类型不能为空");
            }
        }

        dataList.removeAll(errorMap.keySet());

        //员工和访客分组
        for (String[] data : dataList) {
            //判断 员还是访客
            if (SysUserService.isVisitor(data[1])) {
                visitorList.add(data);
            } else {
                staffList.add(data);
            }
        }

        for (String[] staffData : staffList) {

            // 判断工号是否存在
            SysUser sysUser = null;
            try {
                sysUser = sysUserServiceUC.getUserByEmpNo(staffData[1]);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                errorMap.add(staffData, "未查询到员工信息");
            }
            if (sysUser == null) {
                errorMap.add(staffData, "未查询到员工信息");
            } else if (!staffData[0].equals(sysUser.getNameZh())) {
                try {
                    errorMap.add(staffData, "员工姓名不正确");
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                    errorMap.add(staffData, "员工姓名不正确");
                }
            }
        }

        String realKey = redisUtil.createRealKey(STAFF_KEY);
        // 判断redis是否存在数据
        if (Boolean.TRUE.equals(redisUtil.hasKey(realKey))) {
            List<StaffPageResp> collect = JSON.parseArray(String.valueOf(redisUtil.get(realKey)), StaffPageResp.class);

            // 筛选是否员工或访客
            List<Integer> integers = new ArrayList<>();
            integers.add(1);
            collect = collect.stream().filter(staff -> integers.contains(staff.getIsStaff())).collect(Collectors.toList());

            // 缓存中访客id List
            List<String> visitorIdList = new ArrayList<>();
            // 缓存中访客姓名 List
            List<String> visitorNameList = new ArrayList<>();
            collect.forEach(it -> {
                visitorIdList.add(it.getEmpNo());
                visitorNameList.add(it.getName());
            });

            for (String[] visitorData : visitorList) {
                if (!visitorIdList.contains(visitorData[1])) {
                    errorMap.add(visitorData, "未找到访客信息");
                } else if (!visitorNameList.contains(visitorData[0])) {
                    errorMap.add(visitorData, "访客姓名不正确");
                }
            }
        } else {
            for (String[] visitorData : visitorList) {
                errorMap.add(visitorData, "未找到访客信息");
            }
        }

        dataList.removeAll(errorMap.keySet());

        //向iotc发送人员工号  判断人员在hk 和 旷视 平台上是否存在
        dataList.forEach(obj -> {
            ServiceResult userFromPlatform = iotcService.getUserFromPlatform(obj[1]);
            GetUserResp getUserResp = JSONObject.parseObject(JSONObject.toJSONString(userFromPlatform.getObject()), GetUserResp.class);
            log.debug("查询人员{}{}海康、旷视平台人员结果{}", obj[0], obj[1], getUserResp);
            if (Boolean.FALSE.equals(getUserResp.getIsHk()) && Boolean.TRUE.equals(getUserResp.getIsKs())) {
                errorMap.add(obj, "该人员在海康平台中不存在");
            }
            if (Boolean.FALSE.equals(getUserResp.getIsHk()) && Boolean.FALSE.equals(getUserResp.getIsKs())) {
                errorMap.add(obj, "该人员在海康和旷视平台中都不存在");
            }
            if (Boolean.TRUE.equals(getUserResp.getIsHk()) && Boolean.FALSE.equals(getUserResp.getIsKs())) {
                errorMap.add(obj, "该人员在旷视平台中不存在");
            }
        });
        dataList.removeAll(errorMap.keySet());
        List<String[]> errorList = new ArrayList<>();
        errorMap.forEach((key, valueList) -> {
            key[5] = StringUtils.join(valueList, ";");
            errorList.add(key);
        });
        return errorList;
    }

    @Override
    @Transactional
    public ServiceResult createkshtGroupStaff(AccessControlSpot accessControlSpot, List<UserDataReq> staffList) {

        //查询开发能力other
        Optional<OpenAbility> ability = openAbilityDao.findByAbility(Ability.OTHER);
        if (ability.isEmpty()) {
            return ServiceResult.error("未找到开发能力other");
        }
        OpenAbility openAbility = ability.get();

        // 创建鸿图员工组对象
        List<AccessControlSpotKSHTMemberGroup> kshtMemberGroupList = new ArrayList<>();
        //创建员工组
        AccessControlSpotKSHTMemberGroup memberGroup = new AccessControlSpotKSHTMemberGroup();
        //门禁点id
        memberGroup.setAccessControlSpot(accessControlSpot);
        //鸿图人员组名称
        memberGroup.setGroupName(accessControlSpot.getName() + "_" + KSHTMemberGroupType.STAFF.getName() + "_" + Ability.OTHER.getName());
        //人员组类型
        memberGroup.setGroupType(KSHTMemberGroupType.STAFF);
        //开发能力
        memberGroup.setOpenAbility(openAbility);

        kshtMemberGroupList.add(memberGroup);
        //保存到数据库
        accessControlSpotKSHTMemberGroupDao.saveAll(kshtMemberGroupList);

        List<CreateHTMemberGroupReq.Param> groupReqParamList = kshtMemberGroupList.stream()
                .map(obj -> CreateHTMemberGroupReq.Param.builder()
                        .id(obj.getId())
                        .type(obj.getGroupType().getCode())
                        .name(obj.getGroupName())
                        .build())
                .collect(Collectors.toList());

        // 向iotc发送创建员工组消息
        ServiceResult result = iotcService.createStaffHTMemberGroup(CreateHTMemberGroupReq.builder().groupList(groupReqParamList).build());
        if (!result.isSuccess()) {
            log.error("iotc创建员工组失败");
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return result;
        }

        // 向iotc发送设备绑定员工组消息
        for (AccessControlSpotKSHTMemberGroup group : kshtMemberGroupList) {
            PermisssionReq permisssionReq = new PermisssionReq();
            String[] groupUuidList = new String[]{group.getId()};
            String[] deviceUuidList = new String[]{group.getAccessControlSpot().getDevice().getSn()};
            permisssionReq.setDeviceUuidList(deviceUuidList);
            permisssionReq.setGroupUuidList(groupUuidList);
            permisssionReq.setTimePlanId(timePlan);
            result = iotcConnectService.bindingDevice(permisssionReq, "2", "KS");
            //失败
            if (!result.isSuccess()) {
                log.error("iotc设备绑定员工组失败");
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return result;
            }
        }

        IdResp idResp = new IdResp();
        idResp.setId(memberGroup.getId());
        return ServiceResult.ok(idResp);
    }

    @Override
    @Transactional
    public ServiceResult createKshtGroupVisitor(AccessControlSpot accessControlSpot, List<UserDataReq> visitorList) {

        //查询开发能力other
        Optional<OpenAbility> ability = openAbilityDao.findByAbility(Ability.OTHER);
        if (ability.isEmpty()) {
            return ServiceResult.error("未找到开发能力other");
        }
        OpenAbility openAbility = ability.get();

        // 创建鸿图员工组对象
        List<AccessControlSpotKSHTMemberGroup> kshtMemberGroupList = new ArrayList<>();
        //创建员工组
        AccessControlSpotKSHTMemberGroup memberGroup = new AccessControlSpotKSHTMemberGroup();
        //门禁点id
        memberGroup.setAccessControlSpot(accessControlSpot);
        //鸿图人员组名称
        memberGroup.setGroupName(accessControlSpot.getName() + "_" + KSHTMemberGroupType.VISITOR.getName() + "_" + Ability.OTHER.getName());
        //人员组类型
        memberGroup.setGroupType(KSHTMemberGroupType.VISITOR);
        //开发能力
        memberGroup.setOpenAbility(openAbility);

        kshtMemberGroupList.add(memberGroup);
        //保存到数据库
        accessControlSpotKSHTMemberGroupDao.saveAll(kshtMemberGroupList);

        List<CreateHTMemberGroupReq.Param> groupReqParamList = kshtMemberGroupList.stream()
                .map(obj -> CreateHTMemberGroupReq.Param.builder()
                        .id(obj.getId())
                        .type(obj.getGroupType().getCode())
                        .name(obj.getGroupName())
                        .build())
                .collect(Collectors.toList());

        // 向iotc发送创建员工组消息
        ServiceResult result = iotcService.createStaffHTMemberGroup(CreateHTMemberGroupReq.builder().groupList(groupReqParamList).build());
        if (!result.isSuccess()) {
            log.error("iotc创建员工(访客)组失败");
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return result;
        }

        // 向iotc发送设备绑定员工组消息
        for (AccessControlSpotKSHTMemberGroup group : kshtMemberGroupList) {
            PermisssionReq permisssionReq = new PermisssionReq();
            String[] groupUuidList = new String[]{group.getId()};
            String[] deviceUuidList = new String[]{group.getAccessControlSpot().getDevice().getSn()};
            permisssionReq.setDeviceUuidList(deviceUuidList);
            permisssionReq.setGroupUuidList(groupUuidList);
            permisssionReq.setTimePlanId(timePlan);
            result = iotcConnectService.bindingDevice(permisssionReq, "2", "KS");
            //失败
            if (!result.isSuccess()) {
                log.error("iotc设备绑定员工(访客)组失败");
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return result;
            }
        }

        IdResp idResp = new IdResp();
        idResp.setId(memberGroup.getId());
        return ServiceResult.ok(idResp);
    }

    @Override
    @Transactional
    public ServiceResult userBatchDelete(MultipartFile file, List<String> spotIds) {
        //门禁id为空
        if (spotIds.isEmpty()) {
            return ServiceResult.error("未选择上传门禁点", ErrConst.E00001);
        }

        //获取门禁点信息
        List<AccessControlSpot> accessControlSpotList = accessControlSpotDao.findAllById(spotIds);

        //所有设备list
        List<IotDevice> deviceList = accessControlSpotList.stream()
                .filter(spot -> spot.getDevice() != null)
                .map(AccessControlSpot::getDevice)
                .collect(Collectors.toList());

        //获取deviceCode
        List<String> deviceCode = deviceList.stream()
                .map(IotDevice::getSn)
                .collect(Collectors.toList());

        ServiceResult result;
        //读取excel文件
        try {
            result = excelService.readDeleteAccessRulesUserExcel(file);
            if (!result.isSuccess() && ErrConst.E23.equals(result.getErrorCode())) {
                return result;
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ServiceResult.error("excel文件内容格式异常");
        }

        // 所有上传的数据行
        List<String[]> dataList = (List<String[]>) result.getObject();
        if (dataList.size() > batchDeleteMaxLimit) {
            return ServiceResult.error(String.format("数据上传过多,请分批次上传,每次最多%s人", batchDeleteMaxLimit));
        }

        // 数据总数
        int totalCount = dataList.size();
        // 有错误的数据集合
        List<String[]> errorList = getErrorDeleteExcelData(dataList);
        // 所有数据去除错误数据，剩下的就是正常数据，准备写入数据库
        dataList.removeAll(errorList);

        // 获取删除的人员工号集合
        List<String> userHrIdList = new ArrayList<>();
        for (String[] data : dataList) {
            userHrIdList.add(data[1]);
        }

        //OpenAbilityMessage.User
        List<OpenAbilityMessage.User> openAbilityMessageUserList = new ArrayList<>();
        for (String[] data : dataList) {
            OpenAbilityMessage.User user = new OpenAbilityMessage.User();
            user.setName(data[0]);
            user.setHrId(data[1]);
            openAbilityMessageUserList.add(user);
        }

        //创建task
        OpenAbilityTask task = createTask(Ability.OTHER, openAbilityMessageUserList, deviceList, TaskType.MQ, MethodType.DELETE);

        List<String> groupIds = new ArrayList<>();
        spotIds.forEach(id ->
            accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotId(id).forEach(it -> groupIds.add(it.getId()))
        );

        List<AccessPermissions.User> users = new ArrayList<>();
        openAbilityMessageUserList.forEach(obj -> {
            AccessPermissions.User user = new AccessPermissions.User();
            user.setName(obj.getName());
            user.setJobNo(obj.getHrId());
            user.setPersonType(AccessPermissions.PersonType.EMPLOYEE);
            user.setGroupIds(groupIds);
            users.add(user);
        });

        //创建所需要的mq消息
        AccessPermissions message = new AccessPermissions();
        message.setDeviceCodeList(deviceCode);
        message.setResourceId(task.getId());
        message.setReturnTopic(Topic.get(Topic.USER_BATCH_UNBIND_RETURN));
        message.setUserList(users);
        message.setSourceName("IBMS");
        message.setMethodName(AccessPermissions.Method.UNBIND.getName());
        message.setModuleName(AccessPermissions.Model.PERSON.getName());
        message.setOver("1");

        log.debug("发送mq批量删除参数{}", message);
        iotcService.unBindUserWithDevices(message);

//        String uuid = UUID.randomUUID().toString();
        if (!errorList.isEmpty()) {
            // 将失败数据存入缓存，供用户下载
            String redisKey = redisUtil.createRealKey("AccessRulesImportError-" + task.getId());
            redisUtil.set(redisKey, errorList, RedisUtil.ONE_DAY_SECOND);
        }

        return ServiceResult.ok(StaffGroupImportResp.builder()
                .totalCount(totalCount)
                .successCount(userHrIdList.size())
                .errorCount(errorList.size())
                .errorDataId(task.getId())
                .build());
    }

    private List<String[]> getErrorDeleteExcelData(List<String[]> dataList) {
        List<String[]> errorList = new ArrayList<>();

        //员工list
        List<String[]> staffList = new ArrayList<>();
        //访客list
        List<String[]> visitorList = new ArrayList<>();

        for (String[] data : dataList) {
            // 判断工号不能为空
            if (StringUtils.isBlank(data[1])) {
                data[1] = "工号不能为空";
                errorList.add(data);
            } else {
                if (SysUserService.isVisitor(data[1])) {
                    visitorList.add(data);
                } else {
                    staffList.add(data);
                }
            }
        }

        for (String[] staffData : staffList) {
            // 判断工号是否存在
            SysUser sysUser = null;
            try {
                sysUser = sysUserServiceUC.getUserByEmpNo(staffData[1]);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                staffData[2] = "未查询到员工信息";
                errorList.add(staffData);
            }
            if (sysUser == null) {
                staffData[2] = "未查询到员工信息";
                errorList.add(staffData);
            } else if (!StringUtils.equals(staffData[0], sysUser.getNameZh())) {
                try {
                    staffData[2] = "员工姓名不正确";
                    errorList.add(staffData);
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                    staffData[2] = "员工姓名不正确";
                    errorList.add(staffData);
                }
            }

        }

        String realKey = redisUtil.createRealKey(STAFF_KEY);
        // 判断redis是否存在数据
        if (Boolean.TRUE.equals(redisUtil.hasKey(realKey))) {
            List<StaffPageResp> collect = JSON.parseArray(String.valueOf(redisUtil.get(realKey)), StaffPageResp.class);

            // 筛选是否员工或访客
            List<Integer> integers = new ArrayList<>();
            integers.add(1);
            collect = collect.stream().filter(staff -> integers.contains(staff.getIsStaff())).collect(Collectors.toList());

            // 缓存中访客id List
            List<String> visitorIdList = new ArrayList<>();
            // 缓存中访客姓名 List
            List<String> visitorNameList = new ArrayList<>();
            collect.forEach(it -> {
                visitorIdList.add(it.getEmpNo());
                visitorNameList.add(it.getName());
            });


            for (String[] visitorData : visitorList) {
                if (!visitorIdList.contains(visitorData[1])) {

                    visitorData[2] = "未找到访客信息";
                    errorList.add(visitorData);
                } else if (!visitorNameList.contains(visitorData[0])) {

                    visitorData[2] = "访客姓名不正确";
                    errorList.add(visitorData);
                }
            }

        }
        return errorList;
    }

    @Override
    public byte[] getImportErrorDataFileByte(String errorDataId) {
        List<String[]> errorList = new ArrayList<>();
        String redisKey = redisUtil.createRealKey("AccessRulesImportError-" + errorDataId);
        if (!Boolean.TRUE.equals(redisUtil.hasKey(redisKey))) {
            return new byte[0];
        }

        getErrorList(redisKey, errorList);
        return excelService.createUploadAccessRulesUserErrorExcel(errorList);
    }

    private void getErrorList(String redisKey, List<String[]> errorList) {
        JSONArray jsonArray = JSON.parseArray(redisUtil.get(redisKey).toString());

        jsonArray.stream().forEach(obj -> {
            JSONArray dataArray = JSON.parseArray(obj.toString());
            String[] strings = new String[dataArray.size()];
            for (int i = 0; i < dataArray.size(); i++) {
                strings[i] = dataArray.getString(i);
            }
            errorList.add(strings);
        });
    }

    @Override
    public ServiceResult importAddFailedResult(ImportAddFailedReq req) {

        ImportAddFailedResultResp resp = new ImportAddFailedResultResp();

        //获取缓存中失败人员
        List<String[]> errorList = new ArrayList<>();
        String redisKey = redisUtil.createRealKey("AccessRulesImportError-" + req.getErrorDataId());
        if (Boolean.TRUE.equals(redisUtil.hasKey(redisKey))) {
            getErrorList(redisKey, errorList);
        }

        // 获取成功的人员
        List<String[]> dataList = new ArrayList<>();
        String redisSuccessKey = redisUtil.createRealKey("AccessRulesImportSuccess-" + req.getErrorDataId());
        if (Boolean.TRUE.equals(redisUtil.hasKey(redisSuccessKey))) {
            getErrorList(redisSuccessKey, dataList);
        }

        List<OpenAbilityTaskDetail> taskDetailList = openAbilityTaskDetailDao.findByTaskId(req.getTaskId());
        taskDetailList.forEach(obj -> {
            if (UserImportStatus.FAILED.equals(obj.getUserImportStatus())) {
                List<String[]> errors = new ArrayList<>();
                dataList.forEach(data -> {
                    if (data[1].equals(obj.getUserHrId())) {
                        data[5] = "向iotc设备添加人员失败";
                        errors.add(data);
                    }
                });
                errorList.addAll(errors);
                dataList.removeAll(errorList);
            }
        });

        List<OpenAbilityTaskDetail> doingUserImportStatusList = openAbilityTaskDetailDao.findByTaskIdAndUserImportStatus(req.getTaskId(), UserImportStatus.DOING);

        if (doingUserImportStatusList.isEmpty()) {
            resp.setImportStatusResult(UserImportStatus.SUCCESS.getName());
        } else {
            resp.setImportStatusResult(UserImportStatus.DOING.getName());
        }


        // 将失败数据存入redis
        if (Boolean.TRUE.equals(redisUtil.hasKey(redisKey))) {
            resp.setErrorDataId(req.getErrorDataId());
            redisUtil.set(redisKey, errorList, RedisUtil.ONE_DAY_SECOND);
        } else {
            String uuid = UUID.randomUUID().toString();
            resp.setErrorDataId(uuid);
            // 将失败数据存入缓存，供用户下载
            redisKey = redisUtil.createRealKey("AccessRulesImportError-" + uuid);
            redisUtil.set(redisKey, errorList, RedisUtil.ONE_DAY_SECOND);
        }

        resp.setErrorCount(errorList.size());
        resp.setSuccessCount(dataList.size());
        resp.setTotalCount(req.getTotalCount());
        return ServiceResult.ok(resp);
    }

    @Override
    public AccessControlUserFailResp synchronization(AccessControlUserPageaReq req) {

        AccessControlUserFailResp resp = new AccessControlUserFailResp();
        List<AccessControlUserFailPageResp> pageResps = new ArrayList<>();
        if ("1".equals(req.getType())) {
            String taskId = "";
            List<String> syncTaskIds = new ArrayList<>();
            accessControlSpotUserSyncTaskDao.findBySpotIdOrderByCreatedTimeDesc(req.getSpotId()).forEach(spotUserSyncTask -> {
                syncTaskIds.add(spotUserSyncTask.getTask().getId());
            });
            List<OpenAbilityTaskDetail> taskDetailList1 = openAbilityTaskDetailDao.findAll((root, criteriaQuery, criteriaBuilder) -> {
                List<Predicate> list = new ArrayList<>();

                // 门禁点ID
                list.add(criteriaBuilder.in(root.get("task").get("id")).value(syncTaskIds));

                list.add(criteriaBuilder.notEqual(root.get("userImportStatus"), UserImportStatus.DEL_SUCCESS));
                list.add(criteriaBuilder.notEqual(root.get("userImportStatus"), UserImportStatus.DEL_FAILED));
                return criteriaBuilder.and(list.toArray(new Predicate[0]));
            }, Sort.by(Sort.Direction.DESC, "createdTime"));
            if (!CollectionUtils.isEmpty(taskDetailList1)) {
                taskId = taskDetailList1.get(0).getTask().getId();
            }
            String finalTaskId = taskId;
            /*List<AccessControlSpotUserSyncTask> syncTaskList = accessControlSpotUserSyncTaskDao.findAll((root, criteriaQuery, criteriaBuilder) -> {
                List<Predicate> list = new ArrayList<>();

                // 门禁点ID
                list.add(criteriaBuilder.equal(root.get("spot").get("id"), req.getSpotId()));

                list.add(criteriaBuilder.equal(root.get("task").get("id"), finalTaskId));
                return criteriaBuilder.and(list.toArray(new Predicate[0]));
            });

            List<String> taskIds = new ArrayList<>();
            syncTaskList.forEach(it -> taskIds.add(it.getTask().getId()));*/

            List<OpenAbilityTaskDetail> taskDetailList = openAbilityTaskDetailDao.findAll((root, criteriaQuery, criteriaBuilder) -> {
                List<Predicate> list = new ArrayList<>();

                // 任务ID
                list.add(criteriaBuilder.equal(root.get("task").get("id"), finalTaskId));

                // 人员状态
                if (null != req.getUserImportStatus()) {
                    list.add(criteriaBuilder.equal(root.get("userImportStatus"), req.getUserImportStatus()));
                }
                return criteriaBuilder.and(list.toArray(new Predicate[0]));
            });
            int success = 0;
            int fail = 0;
            int wait = 0;
            for (OpenAbilityTaskDetail taskDetail : taskDetailList) {
                if (UserImportStatus.SUCCESS.equals(taskDetail.getUserImportStatus())) {
                    success++;
                } else if (UserImportStatus.FAILED.equals(taskDetail.getUserImportStatus())) {
                    fail++;
                } else if (UserImportStatus.DOING.equals(taskDetail.getUserImportStatus())) {
                    wait++;
                }
            }
            PageBean<OpenAbilityTaskDetail> pageBeanByAllData = PageBean.createPageBeanByAllData(taskDetailList, req.getPage(), req.getSize());
            pageBeanByAllData.getList().forEach(it -> {
                AccessControlUserFailPageResp pageResp = new AccessControlUserFailPageResp();
                // pageResp.setCause(taskDetail.getCause());
                pageResp.setName(it.getUserName());
                pageResp.setEmpNo(it.getUserHrId());
                pageResp.setSource(it.getTask().getAbility().getName());
                pageResp.setCause(it.getFailMsg());
                pageResp.setCreatedTime(it.getCreatedTime());
                pageResp.setUserImportStatus(it.getUserImportStatus());
                try {
                    SysUser userByEmpNo = sysUserServiceUC.getUserByEmpNo(it.getUserHrId());
                    if (null != userByEmpNo) {
                        pageResp.setDept(userByEmpNo.getFullDepartmentName());
                    }
                } catch (Exception e) {
                    log.error("获取人员信息失败" + it.getUserHrId());
                }
                pageResps.add(pageResp);
            });
            resp.setAll(taskDetailList.size());
            resp.setSuccess(success);
            resp.setFail(fail);
            resp.setWait(wait);
            PageBean<AccessControlUserFailPageResp> pageBean = new PageBean<>();
            pageBean.setList(pageResps);
            pageBean.setPage(pageBeanByAllData.getPage());
            pageBean.setSize(pageBeanByAllData.getSize());
            pageBean.setTotalPages(pageBeanByAllData.getTotalPages());
            pageBean.setTotalElements(pageBeanByAllData.getTotalElements());
            resp.setPageBean(pageBean);
        } else if ("2".equals(req.getType())) {
            Optional<AccessControlSpot> spotOptional = accessControlSpotDao.findById(req.getSpotId());
            if (spotOptional.isPresent() && null != spotOptional.get().getDevice()) {

                // 获取设备人员
                List<IotcDeviceUserResp> deviceUserList = findIotcDeviceUserListByDevice(spotOptional.get().getDevice());

                // 获取门禁点人员
                List<AccessControlSpotUser> spotUserList = accessControlSpotUserDao.findByAccessControlSpotId(req.getSpotId());

                // 比较人员获取设备多出
                deviceUserList = deviceUserList.stream().filter(deviceUser -> {
                    for (AccessControlSpotUser spotUser : spotUserList) {
                        if (spotUser.getUserHrId().equals(deviceUser.getJobNo())) {
                            return false;
                        }
                    }
                    return true;
                }).collect(Collectors.toList());

                deviceUserList.forEach(deviceUser -> {
                    AccessControlUserFailPageResp pageResp = new AccessControlUserFailPageResp();
                    pageResp.setName(deviceUser.getName());
                    pageResp.setEmpNo(deviceUser.getJobNo());
                    pageResp.setCreatedTime(deviceUser.getTime());
                    try {
                        SysUser sysUser = sysUserServiceUC.getUserByEmpNo(deviceUser.getJobNo());
                        pageResp.setDept(sysUser.getFullDepartmentName());
                    } catch (Exception e) {
                        log.error("获取人员信息失败" + deviceUser.getJobNo());
                    }
                    pageResp.setSource("面板机添加");
                    pageResps.add(pageResp);
                });
            }
            resp.setPageBean(PageBean.createPageBeanByAllData(pageResps, req.getPage(), req.getSize()));
        }
        return resp;
    }

    @Override
    public ServiceResult synchronizationUser(String id) {

        Optional<AccessControlSpot> spotOptional = accessControlSpotDao.findById(id);
        if (spotOptional.isEmpty()) {
            return ServiceResult.error("ID有误,门禁不存在");
        }
        AccessControlSpot spot = spotOptional.get();
        if (null == spot.getDevice()) {
            return ServiceResult.error("门禁未绑定设备");
        }

        //创建task
        List<IotDevice> deviceList = new ArrayList<>();
        deviceList.add(spot.getDevice());
        //所有添加人员的OpenAbilityMessage.User
        List<OpenAbilityMessage.User> openAbilityMessageUserList = new ArrayList<>();
        List<AccessControlSpotUser> spotUserList = spotUserDao.findByAccessControlSpotId(spot.getId());
        spotUserList.forEach(spotUser -> {
            OpenAbilityMessage.User user = new OpenAbilityMessage.User();
            user.setName(spotUser.getUserName());
            user.setHrId(spotUser.getUserHrId());
            user.setStartTime(spotUser.getAccessStartTime());
            user.setEndTime(spotUser.getAccessEndTime());
            openAbilityMessageUserList.add(user);
        });
        OpenAbilityTask task = createTask(Ability.OTHER, openAbilityMessageUserList, deviceList, TaskType.MQ, MethodType.ADDBATCH);

        // 保存门禁点用户同步任务表
        AccessControlSpotUserSyncTask accessControlSpotUserSyncTask = new AccessControlSpotUserSyncTask();
        accessControlSpotUserSyncTask.setTask(task);
        accessControlSpotUserSyncTask.setSpot(spot);
        accessControlSpotUserSyncTaskDao.save(accessControlSpotUserSyncTask);

//        List<AccessControlSpotUser> spotUserList = spotUserDao.findByAccessControlSpotIdAndUserHrIdIn(id ,hrIds);
        //创建需要发送mq的消息
        AccessPermissions message = new AccessPermissions();

        if (DeviceType.HIK_FACE_MACHINE.equals(spot.getDevice().getDeviceType())) {
            List<AccessPermissions.User> userList = new ArrayList<>();
            spotUserList.forEach(spotUser -> {
                AccessPermissions.User user = new AccessPermissions.User();
                user.setName(spotUser.getUserName());
                user.setJobNo(spotUser.getUserHrId());
                user.setStartTime(DateUtils.formatZoneDateTime(spotUser.getAccessStartTime().atZone(ZoneId.of(DateUtils.GMT8)), DateUtils.PATTERN_DATETIME_WITH_ZONE));
                user.setEndTime(DateUtils.formatZoneDateTime(spotUser.getAccessEndTime().atZone(ZoneId.of(DateUtils.GMT8)), DateUtils.PATTERN_DATETIME_WITH_ZONE));
                user.setPersonType(AccessPermissions.PersonType.EMPLOYEE);
                userList.add(user);
            });
            message.setUserList(userList);
            message.setMethodName(AccessPermissions.Method.BIND.getName());
        } else if (DeviceType.FACE_RECOGNITION.equals(spot.getDevice().getDeviceType())) {
            List<AccessControlSpotKSHTMemberGroup> coverGroupList = accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotId(spot.getId());
            List<AccessPermissions.User> userList = new ArrayList<>();
            List<String> groupIds = new ArrayList<>();
            if (!CollectionUtils.isEmpty(coverGroupList)) {
                List<AccessControlSpotKSHTMemberGroup> groupList = accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotId(
                        spot.getId());
                groupList.forEach(group -> groupIds.add(group.getId()));
            } else {

                // 创建矿石人员组
                Optional<OpenAbility> openAbilityOptional = openAbilityDao.findByAbility(Ability.OTHER);
                OpenAbility openAbility;
                if (openAbilityOptional.isEmpty()) {
                    openAbility = new OpenAbility();
                    openAbility.setAbility(Ability.OTHER);
                    openAbility.setDescribe("其它");
                    openAbilityDao.save(openAbility);
                } else {
                    openAbility = openAbilityOptional.get();
                }
                AccessControlSpotKSHTMemberGroup memberGroup = new AccessControlSpotKSHTMemberGroup();
                memberGroup.setGroupType(KSHTMemberGroupType.STAFF);
                memberGroup.setGroupName(spot.getName() + "_" + memberGroup.getGroupType().getName());
                memberGroup.setAccessControlSpot(spot);
                memberGroup.setOpenAbility(openAbility);
                accessControlSpotKSHTMemberGroupDao.save(memberGroup);
                CreateHTMemberGroupReq.Param param = CreateHTMemberGroupReq.Param.builder()
                        .id(memberGroup.getId())
                        .type(memberGroup.getGroupType().getCode())
                        .name(memberGroup.getGroupName())
                        .build();
                // 向iotc发送创建员工组消息
                ServiceResult result = iotcService.createStaffHTMemberGroup(CreateHTMemberGroupReq.builder().groupList(Lists.newArrayList(param)).build());
                if (!result.isSuccess()) {
                    log.warn("创建旷世人员组:{}失败", param.getName());
                    memberGroup.setGroupName(memberGroup.getGroupName() + RandomCode.getRandom(3));
                    accessControlSpotKSHTMemberGroupDao.save(memberGroup);
                    param.setName(memberGroup.getGroupName());
                    // 以防旷世已存在重名的人员组，修改人员组名后，再次向iotc发送创建员工组消息
                    ServiceResult result2 = iotcService.createStaffHTMemberGroup(CreateHTMemberGroupReq.builder().groupList(Lists.newArrayList(param)).build());
                    if (!result2.isSuccess()) {
                        log.warn("创建旷世人员组:{}失败", param.getName());
                        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                        return result2;
                    }
                }
                // 像iotc发送创建员工组消息
                PermisssionReq permisssionReq = new PermisssionReq();
                String[] groupUuidList = new String[]{memberGroup.getId()};
                String[] deviceUuidList = new String[]{memberGroup.getAccessControlSpot().getDevice().getSn()};
                groupIds.add(memberGroup.getId());
                permisssionReq.setDeviceUuidList(deviceUuidList);
                permisssionReq.setGroupUuidList(groupUuidList);
                permisssionReq.setTimePlanId(timePlan);
                result = iotcConnectService.bindingDevice(permisssionReq, "2", "KS");
                //失败
                if (!result.isSuccess()) {
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return result;
                }

            }
            spotUserList.forEach(spotUser -> {
                AccessPermissions.User user = new AccessPermissions.User();
                user.setName(spotUser.getUserName());
                user.setJobNo(spotUser.getUserHrId());
                user.setCreateTime(DateUtils.formatZoneDateTime(spotUser.getAccessStartTime().atZone(ZoneId.of(DateUtils.GMT8)), DateUtils.PATTERN_DATETIME_WITH_ZONE));
                user.setEndTime(DateUtils.formatZoneDateTime(spotUser.getAccessEndTime().atZone(ZoneId.of(DateUtils.GMT8)), DateUtils.PATTERN_DATETIME_WITH_ZONE));
                user.setPersonType(AccessPermissions.PersonType.EMPLOYEE);
                user.setGroupIds(groupIds);
                userList.add(user);
            });
            message.setUserList(userList);
            message.setMethodName(AccessPermissions.Method.INCREMENT_BIND.getName());
        }
//        else if(DeviceType.FACE_RECOGNITION.equals(device.getDeviceType())){
//
//        }
        message.setResourceId(task.getId());
        message.setReturnTopic(Topic.get(Topic.USER_BIND_RETURN));
        message.setSourceName("IBMS");
        message.setModuleName(AccessPermissions.Model.PERSON.getName());
        List<String> deviceCodeList = new ArrayList<>();
        deviceCodeList.add(spot.getDevice().getSn());
        message.setDeviceCodeList(deviceCodeList);
        message.setOver("1");

        log.debug("发送mq员工参数{}", message);
        iotcService.bindUserWithDevices(message);
        // todo 下发人员权限
        return ServiceResult.ok();
    }

    @Override
    public ServiceResult progress(String taskId) {

        if (openAbilityTaskDetailDao.findByTaskId(taskId).size() == openAbilityTaskDetailDao.findByTaskIdAndUserImportStatus(taskId, UserImportStatus.SUCCESS).size()) {
            return ServiceResult.ok();
        }
        return ServiceResult.error("数据同步中", "3");
    }

    @Override
    public ServiceResult deleteFailUserTime(String id, String empNo) {
        Optional<AccessControlSpot> spotOp = accessControlSpotDao.findById(id);
        if (spotOp.isEmpty()) {
            return ServiceResult.error("id有误,门禁不存在");
        }
        AccessControlSpot accessControlSpot = spotOp.get();

        // 更改任务人员同步状态
        //创建task
        List<IotDevice> deviceList = new ArrayList<>();
        deviceList.add(accessControlSpot.getDevice());
        //所有添加人员的OpenAbilityMessage.User
        List<OpenAbilityMessage.User> openAbilityMessageUserList = new ArrayList<>();
        OpenAbilityMessage.User user1 = new OpenAbilityMessage.User();

        user1.setName(empNo);
        user1.setHrId("");
        openAbilityMessageUserList.add(user1);
        OpenAbilityTask task = createTask(Ability.OTHER, openAbilityMessageUserList, deviceList, TaskType.MQ, MethodType.ADDBATCH);


        //创建需要发送mq的消息
        List<String> groupIds = new ArrayList<>();
        accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotId(id).forEach(it -> groupIds.add(it.getId()));
        AccessPermissions message = new AccessPermissions();

        List<AccessPermissions.User> userList = new ArrayList<>();
        AccessPermissions.User user = new AccessPermissions.User();
        user.setJobNo(empNo);
        user.setPersonType(AccessPermissions.PersonType.EMPLOYEE);
        user.setGroupIds(groupIds);
        userList.add(user);

        message.setUserList(userList);


        message.setReturnTopic(Topic.get(Topic.USER_UNBIND_RETURN));
        message.setSourceName("IBMS");
        message.setResourceId(task.getId());
        message.setMethodName(AccessPermissions.Method.UNBIND.getName());
        message.setModuleName(AccessPermissions.Model.PERSON.getName());
        List<String> deviceCodeList = Arrays.asList(accessControlSpot.getDevice().getSn());
        message.setDeviceCodeList(deviceCodeList);
        message.setOver("1");

        log.debug("发送mq员工参数{}", message);
        iotcService.unBindUserWithDevices(message);


        /*if (null != accessControlSpot.getDevice()) {
            // KS设备不允许删除通行权限
            if (DeviceType.FACE_RECOGNITION.equals(accessControlSpot.getDevice().getDeviceType())) {

                if (IotDeviceService.isKSHTDevice(accessControlSpot.getDevice())) {
                    // 从鸿图删除人
                    // 查询出门禁点对应的鸿图人员组
                    List<AccessControlSpotKSHTMemberGroup> groupList = accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotId(accessControlSpot.getId());

                    groupList.forEach(group -> iotcService.removeUserFromKSHTGroup(group.getId(), userHrIdList));
                } else {
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return ServiceResult.error("旷视九霄设备请在人员组删除权限");
                }

            } else if (DeviceType.HIK_FACE_MACHINE.equals(accessControlSpot.getDevice().getDeviceType())) {
                // 删除HK机器人员通行权限
                List<String> jobNos = new ArrayList<>();
                jobNos.add(empNo);
                ServiceResult result = accessRulesService.deleteHkUser(accessControlSpot.getDevice().getId(), jobNos);
                if (!result.isSuccess()) {
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return ServiceResult.error(String.valueOf(result.getObject()));
                }
            }
        }*/


        return ServiceResult.ok();
    }

    @Override
    public ServiceResult failUserAdd(AccessControlFailUserReq req) {

        Optional<AccessControlSpot> spotOptional = accessControlSpotDao.findById(req.getSpotId());
        if (spotOptional.isEmpty()) {
            return ServiceResult.error("门禁点ID有误,门禁不存在");
        }
        AccessControlSpot spot = spotOptional.get();
        if (null == spot.getDevice()) {
            return ServiceResult.error("该门禁点未绑定设备");
        }

        AccessControlSpotUser spotUser = new AccessControlSpotUser();
        spotUser.setUserHrId(req.getEmpNo());
        spotUser.setAccessControlSpot(spot);
        SysUser sysUser = sysUserServiceUC.getUserByEmpNo(req.getEmpNo());
        if (null == sysUser) {
            return ServiceResult.error("人员不存在");
        }
        spotUser.setUserName(sysUser.getNameZh());
        List<AccessControlSpotUser> spotUserList = accessControlSpotUserDao.findByAccessControlSpotIdAndUserHrId(req.getSpotId(), req.getEmpNo());
        if (CollUtil.isEmpty(spotUserList)) {
            spotUser.setAccessStartTime(LocalDateTime.now().withNano(0));
            spotUser.setAccessEndTime(LocalDateTime.now().withNano(0).plusYears(5));
        } else {
            spotUser.setAccessStartTime(spotUserList.get(0).getAccessStartTime());
            spotUser.setAccessEndTime(spotUserList.get(0).getAccessEndTime());
        }
        spotUserDao.save(spotUser);

        // 更改任务人员同步状态
        //创建task
        List<IotDevice> deviceList = new ArrayList<>();
        deviceList.add(spotOptional.get().getDevice());
        //所有添加人员的OpenAbilityMessage.User
        List<OpenAbilityMessage.User> openAbilityMessageUserList = new ArrayList<>();
        spotUserList.forEach(controlSpotUser -> {
            OpenAbilityMessage.User user = new OpenAbilityMessage.User();
            user.setName(spotUser.getUserName());
            user.setHrId(spotUser.getUserHrId());
            user.setStartTime(spotUser.getAccessStartTime());
            user.setEndTime(spotUser.getAccessEndTime());
            openAbilityMessageUserList.add(user);
        });
        OpenAbilityTask task = createTask(Ability.OTHER, openAbilityMessageUserList, deviceList, TaskType.MQ, MethodType.ADDBATCH);

        // 保存门禁点用户同步任务表
        AccessControlSpotUserSyncTask accessControlSpotUserSyncTask = new AccessControlSpotUserSyncTask();
        accessControlSpotUserSyncTask.setTask(task);
        accessControlSpotUserSyncTask.setSpot(spot);
        accessControlSpotUserSyncTaskDao.save(accessControlSpotUserSyncTask);


        //创建需要发送mq的消息
        AccessPermissions message = new AccessPermissions();

        if (DeviceType.HIK_FACE_MACHINE.equals(spot.getDevice().getDeviceType())) {
            List<AccessPermissions.User> userList = new ArrayList<>();
            AccessPermissions.User user = new AccessPermissions.User();
            user.setName(spotUser.getUserName());
            user.setJobNo(spotUser.getUserHrId());
            user.setStartTime(DateUtils.formatZoneDateTime(spotUser.getAccessStartTime().atZone(ZoneId.of(DateUtils.GMT8)), DateUtils.PATTERN_DATETIME_WITH_ZONE));
            user.setEndTime(DateUtils.formatZoneDateTime(spotUser.getAccessEndTime().atZone(ZoneId.of(DateUtils.GMT8)), DateUtils.PATTERN_DATETIME_WITH_ZONE));
            user.setPersonType(AccessPermissions.PersonType.EMPLOYEE);
            userList.add(user);
            message.setUserList(userList);
            message.setMethodName(AccessPermissions.Method.BIND.getName());
        } else if (DeviceType.FACE_RECOGNITION.equals(spot.getDevice().getDeviceType())) {
            List<AccessControlSpotKSHTMemberGroup> coverGroupList = accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotId(spot.getId());
            List<AccessPermissions.User> userList = new ArrayList<>();
            List<String> groupIds = new ArrayList<>();
            if (!CollectionUtils.isEmpty(coverGroupList)) {
                List<AccessControlSpotKSHTMemberGroup> groupList = accessControlSpotKSHTMemberGroupDao.findByAccessControlSpotId(
                        spot.getId());
                groupList.forEach(group -> groupIds.add(group.getId()));
            } else {

                // 创建矿石人员组
                Optional<OpenAbility> openAbilityOptional = openAbilityDao.findByAbility(Ability.OTHER);
                OpenAbility openAbility;
                if (openAbilityOptional.isEmpty()) {
                    openAbility = new OpenAbility();
                    openAbility.setAbility(Ability.OTHER);
                    openAbility.setDescribe("其它");
                    openAbilityDao.save(openAbility);
                } else {
                    openAbility = openAbilityOptional.get();
                }
                AccessControlSpotKSHTMemberGroup memberGroup = new AccessControlSpotKSHTMemberGroup();
                memberGroup.setGroupType(KSHTMemberGroupType.STAFF);
                memberGroup.setGroupName(spot.getName() + "_" + memberGroup.getGroupType().getName());
                memberGroup.setAccessControlSpot(spot);
                memberGroup.setOpenAbility(openAbility);
                accessControlSpotKSHTMemberGroupDao.save(memberGroup);
                CreateHTMemberGroupReq.Param param = CreateHTMemberGroupReq.Param.builder()
                        .id(memberGroup.getId())
                        .type(memberGroup.getGroupType().getCode())
                        .name(memberGroup.getGroupName())
                        .build();
                // 向iotc发送创建员工组消息
                ServiceResult result = iotcService.createStaffHTMemberGroup(CreateHTMemberGroupReq.builder().groupList(Lists.newArrayList(param)).build());
                if (!result.isSuccess()) {
                    log.warn("创建旷世人员组:{}失败", param.getName());
                    memberGroup.setGroupName(memberGroup.getGroupName() + RandomCode.getRandom(3));
                    accessControlSpotKSHTMemberGroupDao.save(memberGroup);
                    param.setName(memberGroup.getGroupName());
                    // 以防旷世已存在重名的人员组，修改人员组名后，再次向iotc发送创建员工组消息
                    ServiceResult result2 = iotcService.createStaffHTMemberGroup(CreateHTMemberGroupReq.builder().groupList(Lists.newArrayList(param)).build());
                    if (!result2.isSuccess()) {
                        log.warn("创建旷世人员组:{}失败", param.getName());
                        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                        return result2;
                    }
                }
                // 像iotc发送创建员工组消息
                PermisssionReq permisssionReq = new PermisssionReq();
                String[] groupUuidList = new String[]{memberGroup.getId()};
                String[] deviceUuidList = new String[]{memberGroup.getAccessControlSpot().getDevice().getSn()};
                groupIds.add(memberGroup.getId());
                permisssionReq.setDeviceUuidList(deviceUuidList);
                permisssionReq.setGroupUuidList(groupUuidList);
                permisssionReq.setTimePlanId(timePlan);
                result = iotcConnectService.bindingDevice(permisssionReq, "2", "KS");
                //失败
                if (!result.isSuccess()) {
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return result;
                }

            }
            AccessPermissions.User user = new AccessPermissions.User();
            user.setName(spotUser.getUserName());
            user.setJobNo(spotUser.getUserHrId());
            user.setCreateTime(DateUtils.formatZoneDateTime(spotUser.getAccessStartTime().atZone(ZoneId.of(DateUtils.GMT8)), DateUtils.PATTERN_DATETIME_WITH_ZONE));
            user.setEndTime(DateUtils.formatZoneDateTime(spotUser.getAccessEndTime().atZone(ZoneId.of(DateUtils.GMT8)), DateUtils.PATTERN_DATETIME_WITH_ZONE));
            user.setPersonType(AccessPermissions.PersonType.EMPLOYEE);
            user.setGroupIds(groupIds);
            userList.add(user);
            message.setUserList(userList);
            message.setMethodName(AccessPermissions.Method.INCREMENT_BIND.getName());
        }
//        else if(DeviceType.FACE_RECOGNITION.equals(device.getDeviceType())){
//
//        }
        message.setResourceId(task.getId());
        message.setReturnTopic(Topic.get(Topic.USER_BIND_RETURN));
        message.setSourceName("IBMS");
        message.setModuleName(AccessPermissions.Model.PERSON.getName());
        List<String> deviceCodeList = new ArrayList<>();
        deviceCodeList.add(spot.getDevice().getSn());
        message.setDeviceCodeList(deviceCodeList);
        message.setOver("1");

        log.debug("发送mq员工参数{}", message);
        iotcService.bindUserWithDevices(message);

        return ServiceResult.ok();
    }

    private List<IotcDeviceUserResp> findIotcDeviceUserListByDevice(IotDevice device) {

        List<IotcDeviceUserResp> list = new ArrayList<>();
        // 向iotc发送请求获取设备人员列表
        try {
            // url
            StringBuilder urlTemplate = new StringBuilder();
            urlTemplate.append(iotcDeviceUserListUrl).append("?deviceCode=").append(device.getSn());
            // 发送Http请求
            HttpHeaders headers = new HttpHeaders();

            // 获取TB遥测数据
            HttpEntity requestGet = new HttpEntity<>(headers);
            ResponseEntity<String> response = restTemplate.exchange(urlTemplate.toString(), HttpMethod.GET, requestGet, String.class);
            String body = response.getBody();
            // 解析数据
            JSONObject jsonObject = JSON.parseObject(body);
            JSONArray data = jsonObject.getJSONArray("data");
            list = JSON.parseArray(data.toJSONString(), IotcDeviceUserResp.class);
        } catch (Exception e) {
            log.error("设备ID:" + device.getId());
            log.error("向Iotc获取指定设备人员失败");
        }
        return list;
    }

    //    /**
//     * 同步修改海康面板机人脸数据
//     *
//     * @param file
//     * @param hrId
//     * @param hkAccessControlSpotList
//     * @author wei.cheng
//     * @date 2022/3/16 6:21 下午
//     **/
//    private boolean updateHkFace(MultipartFile file, String hrId, List<AccessControlSpot> hkAccessControlSpotList) {
//        if (CollectionUtils.isEmpty(hkAccessControlSpotList)) {
//            return true;
//        }
//        InputStream inputStream = PicUtils.getPhotoFitHIK(file);
//        String faceBase64Data = PicUtils.getImgInputStream(inputStream);
//        // 根据hk设备id获取ip
//        List<HkDevice> hkDeviceList = hkDeviceDao.findByDeviceIdIn(hkAccessControlSpotList.stream().map(AccessControlSpot::getDevice).map(IotDevice::getId)
//                .distinct().collect(Collectors.toList()));
//        List<String> ksIps = new ArrayList<>();
//        List<String> ntIps = new ArrayList<>();
//        hkDeviceList.forEach(it -> {
//            if (HkAddress.KS.equals(it.getAddress())) {
//                ksIps.add(it.getIp());
//            } else {
//                ntIps.add(it.getIp());
//            }
//        });
//        if (!CollectionUtils.isEmpty(ksIps)) {
//            ServiceResult updateHkFaceResult = updateHkFace(hrId, faceBase64Data, hkFindPidKsUrl, hkUpdateFaceKsUrl);
//            if (!updateHkFaceResult.isSuccess()) {
//                return true;
//            }
//        }
//        if (!CollectionUtils.isEmpty(ntIps)) {
//            ServiceResult updateHkFaceResult = updateHkFace(hrId, faceBase64Data, hkFindPidNtUrl, hkUpdateFaceNtUrl);
//            if (!updateHkFaceResult.isSuccess()) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    /**
//     * 更新海康面板机人脸数据
//     *
//     * @param hrId            员工工号
//     * @param faceBase64Data  人脸照片 base64格式
//     * @param hkFindPidUrl    向hk获取人员personId url
//     * @param hkUpdateFaceUrl 向hk更新人脸照片 url
//     * @author wei.cheng
//     * @date 2022/3/16 5:17 下午
//     **/
//    private ServiceResult updateHkFace(String hrId, String faceBase64Data, String hkFindPidUrl, String hkUpdateFaceUrl) {
//        try {
//            // 向hk获取人员personId
//            String s = restTemplateUtil.get(hkFindPidUrl + hrId);
//            JSONObject jsonObject = JSON.parseObject(s);
//            if ("-1".equals(jsonObject.getString("code"))) {
//                return ServiceResult.error("工号不存在");
//            }
//            String personId = jsonObject.getString("data");
//            // 更新人脸参数
//            HkUpdateFaceReq hkUpdateFaceReq = new HkUpdateFaceReq();
//            hkUpdateFaceReq.setPersonId(personId);
//            hkUpdateFaceReq.setFaceData(faceBase64Data);
//            String requestParam = JSON.toJSONString(hkUpdateFaceReq);
//            String faceJson = restTemplateUtil.jsonPost(hkUpdateFaceUrl, requestParam, String.class);
//            JSONObject parseObject = JSON.parseObject(faceJson);
//            if ("0".equals(parseObject.getString("status"))) {
//                log.error("hk人脸更新失败, response:" + faceJson);
//                return ServiceResult.error("hk人脸修改失败");
//            }
//        } catch (Exception e) {
//            log.error(e.getMessage());
//            log.error("hk人脸更新失败:" + hrId);
//            return ServiceResult.error("hk人脸修改失败");
//        }
//        return ServiceResult.ok();
//    }
}
