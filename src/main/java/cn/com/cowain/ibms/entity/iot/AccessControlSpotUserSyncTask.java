package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.ability.OpenAbilityTask;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Yang.Lee
 * @date 2022/4/20 11:30
 */
@Data
@Entity
@Table(name = "ibms_access_spot_user_sync_task")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_access_spot_user_sync_task", comment = "门禁点用户同步任务表")
@Where(clause = "is_del=0")
public class AccessControlSpotUserSyncTask extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "access_control_spot_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK。门禁点id'")
    private AccessControlSpot spot;

    @ManyToOne
    @JoinColumn(name = "task_id", nullable = false, columnDefinition = "varchar(36) COMMENT '任务ID'")
    private OpenAbilityTask task;
}
