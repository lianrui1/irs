package cn.com.cowain.ibms.rest.resp.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/10/19 11:01
 */
@Data
@ApiModel("门磁详情")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DoorMagneticDetailResp {

    @ApiModelProperty(value = "门磁ID", required = true, example = "KS00123", position = 1)
    private String magneticId;

    @ApiModelProperty(value = "设备信息", required = true,  position = 2)
    private IotDeviceResp deviceInfo;

    @ApiModelProperty(value = "门磁编号", required = true, example = "KS00123", position = 3)
    private String number;

    @ApiModelProperty(value = "二维码内容", required = true, example = "https://a.b.com", position = 4)
    private String qrcodeContent;

    @ApiModelProperty(value = "关联的面板机列表", required = true, position = 5)
    private List<IotDeviceResp> relatedHkDeviceList;

}
