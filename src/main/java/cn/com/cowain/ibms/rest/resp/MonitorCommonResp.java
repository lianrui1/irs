package cn.com.cowain.ibms.rest.resp;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author feng
 * @title: NodeSit 监控点列表
 * @projectName bims
 * @Date 2021/11/18 11:01
 */
@Data
public class MonitorCommonResp {
    @ApiModelProperty("id")
    String id;
    /** 当前节点名称 */
    @ApiModelProperty("当前节点名称")
    String name;
    /** 编码 */
    @ApiModelProperty("编码")
    String code;
    /** 子节点 列表 */
    @ApiModelProperty("子节点列表")
   String token;
}
