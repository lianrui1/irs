package cn.com.cowain.ibms.dao.meeting;

import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.entity.meeting.ReservationRecord2CustomGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReservationRecord2CustomGroupDao extends JpaRepository<ReservationRecord2CustomGroup,String> {

    // 根据会议记录获取会议组
    List<ReservationRecord2CustomGroup> findByReservationRecord(ReservationRecord reservationRecord);
}
