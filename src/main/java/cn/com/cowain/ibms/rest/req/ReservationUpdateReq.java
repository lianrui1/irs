package cn.com.cowain.ibms.rest.req;

import cn.com.cowain.ibms.enumeration.ReservationRecordType;
import cn.com.cowain.ibms.rest.req.meeting.UpdateMeetingFileReq;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Description
 *
 * @author 张宇鑫
 * @version 1.0
 * @date 2020/8/4 17:58
 */
@Data
public class ReservationUpdateReq {

    /**
     * 会议主题
     */
    @NotBlank
    @ApiModelProperty(name = "会议主题", required = true, example = "2000-12-31")
    private String topic;

    /**
     * 预定日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(name = "预定日期", required = true, example = "2000-12-31")
    private LocalDate date;

    /**
     * 从时间
     */
//    @Future
    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(name = "开始时间", required = true, example = "2000-12-31 09:30")
    private LocalDateTime from;

    /**
     * 到时间
     */
//    @Future
    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(name = "结束时间", required = true, example = "2000-12-31 11:30")
    private LocalDateTime to;

    /**
     * 会议室房间ID
     */
    @NotBlank
    @ApiModelProperty(name = "会议室id", required = true, example = "2000-12-31")
    private String roomId;

    /**
     * 参与人，冗余字段
     */
    @ApiModelProperty(name = "参与人列表", required = true, example = "2000-12-31")
    private List<UserStatusBean> participantList;

    /**
     * 备注
     */
    @Length(min = 0, max = 200)
    @ApiModelProperty(name = "备注", example = "2000-12-31")
    private String remark;

    /**
     * 需要的服务
     */
    @ApiModelProperty(name = "需要的服务", example = "2000-12-31")
    private String service;
    /**
     * 发起人
     */
    @ApiModelProperty(name = "发起人", required = true, example = "2000-12-31")
    private SysUserBean initiator;

    /**
     * 免打扰模式
     *
     * @author Yang.Lee
     * @since 1.1
     */
    @Max(value = 1, message = "免打扰状态不合法")
    @Min(value = 0, message = "免打扰状态不合法")
    @ApiModelProperty(value = "免打扰模式, 0:允许打扰;1:不要打扰;null无门磁", position = 9)
    private Integer noDisturb;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "访客离开时间", example = "2000-12-31 09:30", position = 10)
    private LocalDateTime endTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "访客到访时间", example = "2000-12-31 09:30", position = 11)
    private LocalDateTime startTime;

    @ApiModelProperty(value = "事由", example = "面试", position = 12)
    private String reason;

    @ApiModelProperty(value = "是否VIP 1 是； 0 否", example = "0", position = 13)
    private Integer isVip;

    @ApiModelProperty(value = "访客类型名称",example = "例：普通访客",position = 14)
    private String roleRemark;

    @ApiModelProperty(value = "邀请卡备注", example = "*****", position = 15)
    private String invitationRemark;

    /**
     * 会议组列表
     */
    @ApiModelProperty(value = "会议组列表", position = 16)
    private List<String> userGroupList;

    @ApiModelProperty(value = "是否是内部会议", example = "VISITOR", position = 17)
    private ReservationRecordType recordType;

    /**
     * 会议文档
     *
     * @author: wei.cheng
     */
    @ApiModelProperty(value = "会议文档", position = 18)
    @Valid
    private List<UpdateMeetingFileReq> meetingFileList;

    /**
     * 欢迎语
     *
     * @author: yanzy
     */
    @ApiModelProperty(value = "欢迎语设置", position = 20)
    @Length(max = 100, message = "欢迎语不能超过100字符")
    private String welcomeWords;

    @Max(value = 2, message = "会议大屏状态不合法")
    @Min(value = 1, message = "会议大屏状态不合法")
    @ApiModelProperty(value = "会议大屏状态 1:是； 2：否", position = 30)
    private Integer screenStatus;

    @ApiModelProperty(value = "上传链接")
    private List<ReservationUploadUrlReq> urls;

    @ApiModelProperty(value = "访客邀请函编码，仅访客使用", example = "12333", position =21)
    private String invitationCardNo;

    @ApiModelProperty(value = "访客类型代码code",example = "20006",position = 22)
    private String roleCode;

    @ApiModelProperty(value = "忽略会议室容量标记。ignoreCapacity=1则不计算参会人数与会议室容量之间的关系",example = "1",position = 23)
    private Integer ignoreCapacity = 0;

    @ApiModelProperty(value = "忽略日程标记，该标记为1时不向日程中心发送消息",example = "1",position = 24)
    private Integer ignoreSchedule = 0;
}
