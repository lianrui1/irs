package cn.com.cowain.ibms.service.iot;

import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.iot.AccessControlSpot;
import cn.com.cowain.ibms.enumeration.ability.Ability;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.PageReq;
import cn.com.cowain.ibms.rest.req.distinguish.ImportAddFailedReq;
import cn.com.cowain.ibms.rest.req.iot.*;
import cn.com.cowain.ibms.rest.resp.distinguish.StaffGroupResp;
import cn.com.cowain.ibms.rest.resp.iot.*;
import cn.com.cowain.ibms.rest.resp.space.ProjectSpaceTreeResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 门禁点Service
 *
 * @author Yang.Lee
 * @date 2021/11/19 13:59
 */
public interface AccessControlSpotService {

    /**
     * 创建门禁点
     *
     * @param req 编辑参数
     * @return 创建结果，创建成功返回数据主键ID
     * @author Yang.Lee
     * @date 2021/11/19 14:03
     **/
    ServiceResult create(AccessControlSpotEditReq req);

    /**
     * 更新门禁点信息
     *
     * @param spotId 门禁点ID
     * @param req    请求参数
     * @return 修改结果
     * @author Yang.Lee
     * @date 2021/11/19 14:04
     **/
    ServiceResult update(String spotId, AccessControlSpotEditReq req);

    /**
     * 查询门禁点详情
     *
     * @param spotId 门禁点ID
     * @return 门禁点详情
     * @author Yang.Lee
     * @date 2021/11/19 14:06
     **/
    AccessControlSpotDetailResp get(String spotId);

    /**
     * 删除门禁点
     *
     * @param spotId 门禁点ID
     * @return 删除结果
     * @author Yang.Lee
     * @date 2021/11/19 14:06
     **/
    ServiceResult delete(String spotId);

    /**
     * 获取门禁列表（分页）
     *
     * @param req 查询参数
     * @return 门禁列表
     * @author Yang.Lee
     * @date 2021/11/24 9:51
     **/
    PageBean<AccessControlSpotPageResp> getPage(AccessControlSpotPageReq req);

    // 通行权限人员列表
    PageBean<AccessControlPageResp> userPage(AccessControlUserPageReq req);

    // 绑定设备
    ServiceResult updateDevice(String id, String deviceId);

    // 向HK设备添加人员
    ServiceResult createHK(AccessControlHkCreateReq req);

    // 获取门禁点下人员列表
    PageBean<AccessControlPageResp> spotUserPage(SpotUserPageReq req);

    // 获取人员门禁点列表
    PageBean<AccessControlSpotPageResp> userSpotPage(String empNo, PageReq req);

    // 修改人员通行时间
    ServiceResult updateUserTime(AccessControlUpdateTimeReq req);

    // 删除人员门禁点通行权限
    ServiceResult deleteUserTime(String id, String empNo);

    // 向KS设备添加人员
    ServiceResult createKS(AccessControlKsCreateReq req);

    // 批量分配人员权限
    ServiceResult createRules(AccessControlRulesCreateReq req);

    /**
     * @param personPermissionReq 分页查询参数
     * @return
     * @description 人员权限列表(分页)
     * @author tql
     * @date 21-12-2
     */
    PageBean<PersonPermissionPageResp> personPage(PersonPermissionReq personPermissionReq);

    /**
     * @param departmentPersonReq 部门人员权限入参
     * @return
     * @description 部门人员门禁点权限
     * @author tql
     * @date 21-12-2
     */
    ServiceResult departPerson(DepartmentPersonReq departmentPersonReq);

    // 根据部门获取人员
    ServiceResult getUserByDept(List<String> departmentIds, List<UserReq> users);

    // 门禁点人员通行记录
    PageBean<SpotRecordPageResp> spotRecordPage(SpotRecordPageReq req);

    // 门禁点人员通行记录导出
    byte[] spotRecordDownload(PageBean<SpotRecordPageResp> pageBean);

    /**
     * 获取门禁点树
     *
     * @return
     * @author Yang.Lee
     * @date 2021/12/3 16:24
     **/
    List<ProjectSpaceTreeResp> getTree();

    // 查询门禁点人员组列表
    List<StaffGroupResp> getStaffGroupByAccessControlSpotId(String id);

    /**
     * @param type 类型空间/项目
     * @param id   空间id/项目id
     * @return
     * @description 根据空间/项目id获取门禁点
     * @author tql
     * @date 21-12-9
     */
    List<String> getControlSpotBySpaceOrProject(String type, String id);

    // 解除设备绑定
    ServiceResult deleteDevice(String id, String coerce);

    /**
     * 获取树形式的门禁点列表
     *
     * @return 门禁点列表
     * @author Yang.Lee
     * @date 2021/12/13 13:19
     **/
    List<ProjectSpaceTreeResp> getListWithTree(boolean withSpot);

    /**
     * 向门禁点中添加用户（仅添加软件层面的权限，不会将用户数据下发到设备）
     *
     * @param userList 用户列表
     * @param ability  开放能力
     * @param spots    门禁点
     * @return 添加结果
     * @author Yang.Lee
     * @date 2022/2/7 14:33
     **/
    ServiceResult addUsers(List<AccessControlSpotUserReq> userList, Ability ability, AccessControlSpot... spots);

    /**
     * 移除用户所有权限 （仅添加软件层面的权限，不会将用户数据下发到设备）
     *
     * @param userIdList 用户工号列表
     * @return 操作结果
     * @author Yang.Lee
     * @date 2022/2/9 9:39
     **/
    ServiceResult removeUserAllAccess(List<String> userIdList);

    /**
     * 检查用户权限并开门
     *
     * @param userHrId 用户工号
     * @param deviceSn 设备SN
     * @return 结果
     * @author Yang.Lee
     * @date 2022/2/9 20:00
     **/
    ServiceResult checkUserAccess(String userHrId, String deviceSn);

    // 分配人查看门禁点列表
    PageBean<AccessControlSpotPageResp> getAccessControlSpotPage(AccessControlSpotPageReq req);

    /**
     * 查询我对门禁点拥有的权限信息
     *
     * @param token           当前用户token
     * @param spotId          门禁点ID
     * @param doorMagnetismNo 门磁编号
     * @param guestMobile     访客号码
     * @return 结果
     * @author wei.cheng
     * @date 2022/3/9 9:29 上午
     **/
    ServiceResult getMyPermissionOfAccessControlSpot(String token, String spotId, String doorMagnetismNo, String guestMobile);

    /**
     * 判断员工当前是否具有通过门禁点的权限
     *
     * @param spotId 门禁点ID
     * @param hrId   员工工号
     * @return true: 有权限；false：无权限
     * @author wei.cheng
     * @date 2022/3/9 10:04
     **/
    boolean isAccess(String spotId, String hrId);

    /**
     * 获取门禁点可以为员工分配权限的分配人列表
     *
     * @param spotId
     * @param hrId
     * @return 分配人列表
     * @author wei.cheng
     * @date 2022/3/9 6:07 下午
     **/
    List<SysUser> getAccessControlSpotApproverList(String spotId, String hrId);

    /**
     * 批量移除用户权限
     *
     * @param userIdList 用户工号列表
     * @param spots      门禁点
     * @return 移除结果
     * @author Yang.Lee
     * @date 2022/3/1 15:29
     **/
    ServiceResult removeUserAccess(List<String> userIdList, AccessControlSpot... spots);

    /**
     * 更新门禁点中当前用户人脸照片
     *
     * @param token
     * @param spotId
     * @param guestMobile 访客号码
     * @param file
     * @author wei.cheng
     * @date 2022/3/15 4:16 下午
     **/
    ServiceResult updateUserFace(String token, String spotId, String guestMobile, MultipartFile file);

    /**
     * 根据excel文件批量删除用户
     *
     * @param file
     * @param spotIds
     * @return
     */
    ServiceResult userBatchDelete(MultipartFile file,List<String> spotIds);

    /**
     * 根据excel文件批量添加用户
     *
     * @author: yanzy
     * @data: 2022/3/31 16:03:17
     */
    ServiceResult userBatchAdd(MultipartFile file,List<String> spotIds);

    /**
     *  创建ht员工类型人员组
     *
     * @author: yanzy
     * @data: 2022/3/31 19:03:00
     */
    ServiceResult createkshtGroupStaff(AccessControlSpot accessControlSpot,List<UserDataReq> staffList);

    /**
     * 创建ht访客类型人员组
     *
     * @author: yanzy
     * @data: 2022/4/1 10:04:07
     */
    ServiceResult createKshtGroupVisitor(AccessControlSpot accessControlSpot,List<UserDataReq> visitorList);

    // 获取批量添加通行数据导入失败数据的文件二进制数组
    byte[] getImportErrorDataFileByte(String errorDataId);

    /**
     * 查询批量添加导入人员结果
     *
     * @author: yanzy
     * @data: 2022/4/8 14:04:29
     */
    ServiceResult importAddFailedResult(ImportAddFailedReq req);

    // 查询指定门禁同步人员列表
    AccessControlUserFailResp synchronization(AccessControlUserPageaReq req);

    // 同步门禁点数据
    ServiceResult synchronizationUser(String id);

    ServiceResult progress(String taskId);

    // 删除异常人员门禁点通行权限
    ServiceResult deleteFailUserTime(String id, String empNo);

    // 失败人员同步
    ServiceResult failUserAdd(AccessControlFailUserReq req);

    /**
     * 门磁门禁点扫码开门
     *
     * @param token
     * @param spotId
     * @param doorMagnetismNo
     * @param guestMobile
     * @return
     */
    ServiceResult doorMagneticOpenDoor(String token, String spotId, String doorMagnetismNo, String guestMobile);

    /**
     * 获取用户在面板机的人脸照片
     *
     * @param hrId
     * @return
     */
    String getFaceImg(String hrId);
}
