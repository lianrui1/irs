package cn.com.cowain.ibms.rest.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 访客类型返回参数
 *
 * @author: yanzy
 * @date: 2022/4/19 14:26
 */
@Data
@ApiModel("访客类型返回")
public class VisitorTypeResp {

    @ApiModelProperty(value = "访客类型代码code",position = 1)
    private String roleCode;

    @ApiModelProperty(value = "访客类型名称",example = "例：普通访客",position = 2)
    private String roleRemark;

    @ApiModelProperty(value = "签到",example = "0：不需要，1：需要",position = 3)
    private Integer needSign;

    @ApiModelProperty(value = "亲到审批",example = "0：不需要，1：需要",position = 4)
    private Integer approveAfterSign;

    @ApiModelProperty(value = "驾车",example = "0：不需要，1：需要",position = 5)
    private Integer allowCar;

    @ApiModelProperty(value = "免检离场",example = "0：不需要，1：需要",position = 6)
    private Integer leaveNotCheck;

    @ApiModelProperty(value = "是否默认",example = "0：不需要，1：需要",position = 7)
    private Integer isDefault;
}
