package cn.com.cowain.ibms.rest.resp.office;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/10 14:37
 */
@Data
@ApiModel("人员办公室列表(分页)响应对象")
public class OccupyOfficeResp {

    @ApiModelProperty(value = "空间ID", required = true)
    private String spaceId;

    @ApiModelProperty(value = "地址", required = true)
    private String address;
    @ApiModelProperty(value = "空间名称", required = true)
    private String spaceName;

    @ApiModelProperty(value = "办公室ID", required = true)
    private String officeId;

    @ApiModelProperty(value = "办公室名称", required = true)
    private String officeName;
    @ApiModelProperty(value = "是否已被初始化", required = true)
    private Boolean hasCreated;
    @ApiModelProperty(value = "初始化后办公室Id", required = true)
    private String intellgentOfficeId;
    @ApiModelProperty(value = "初始化后的办公室别名", required = true)
    private String alias;
    @ApiModelProperty(value = "二级权限", required = true)
    private Boolean isSecondePower=false;

}
