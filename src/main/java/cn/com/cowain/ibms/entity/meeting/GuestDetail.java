package cn.com.cowain.ibms.entity.meeting;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author Yang.Lee
 * @date 2021/6/24 18:07
 */
@Data
@Entity
@Table(name = "ibms_meeting_guest_detail")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_meeting_guest_detail", comment = "会议访客详情表")
@Where(clause = "is_del=0")
public class GuestDetail extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "invitaion_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 邀请id'")
    private InvitationCard invitation;

    @Column(name = "name",  columnDefinition = "varchar(20) COMMENT '访客姓名'")
    private String name;

    @Column(name = "company",  columnDefinition = "varchar(50) COMMENT '访客单位'")
    private String company;

    @Column(name = "photo_url",  columnDefinition = "varchar(200) COMMENT '访客通行照片'")
    private String photoUrl;

    @Column(name = "mobile",  columnDefinition = "varchar(20) COMMENT '访客手机号码'")
    private String mobile;

    @Column(name = "job_num",  columnDefinition = "varchar(20) COMMENT '访客工号'")
    private String jobNum;
}
