package cn.com.cowain.ibms.rest.resp.device;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

/**
 * @Author tql
 * @Description 楼宇总览 出参类
 * @Date 21-11-4 下午3:13
 * @Version 1.0
 */
@Data
@Builder
@ApiModel("楼宇总览")
public class FullViewRadioResp {

    @ApiModelProperty(value = "工厂名",  example = "123456", position = 1)
    private String type;

    @ApiModelProperty(value = "在线数",  example = "123456", position = 2)
    private Integer value;

}
