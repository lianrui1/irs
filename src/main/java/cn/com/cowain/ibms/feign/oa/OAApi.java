package cn.com.cowain.ibms.feign.oa;

import cn.com.cowain.ibms.feign.bean.oa.guest.InvitationReasonList;
import cn.com.cowain.ibms.feign.bean.oa.guest.VisitorPassReturn;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

/**
 * @author Yang.Lee
 * @date 2021/6/30 16:27
 */
@Service
@FeignClient(value = "ms-oa", url = "${cowain.gateway.address}")
public interface OAApi {

    /**
     * 获取事由列表
     *
     * @param jobNum 用户工号
     * @param token  ehr短token
     * @return 事由列表
     * @author Yang.Lee
     * @date 2021/6/30 16:39
     **/
    @GetMapping("/oa/wechat/visitor/ehrcommon/getReasonList/{jobNum}}")
    InvitationReasonList getInvitationList(@PathVariable String jobNum, @RequestHeader(name = "token") String token);

    /**
     * 报餐回调
     *
     * @param jobNum   工号
     * @param mealTime 时间 yyyy-MM-dd HH:mm:ss
     * @return 结果
     * @author Yang.Lee
     * @date 2022/3/3 14:30
     **/
    @GetMapping("/oa/app/fromibms/saveMealRecordFromIBMS")
    String mealReturn(@RequestParam String jobNum, @RequestParam String mealTime);

    /**
     * 访客通行回调
     *
     * @param visitorPassReturn 回调参数
     * @return 结果
     * @author Yang.Lee
     * @date 2022/4/26 15:01
     **/
    @PostMapping("/visitor/app/visitor/visitoruser/gate")
    String visitorUserGate(@RequestBody VisitorPassReturn visitorPassReturn);
}
