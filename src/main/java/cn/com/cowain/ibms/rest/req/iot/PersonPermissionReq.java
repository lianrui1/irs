package cn.com.cowain.ibms.rest.req.iot;

import cn.com.cowain.ibms.enumeration.iot.PersonType;
import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * @Author tql
 * @Description 人员权限列表 请求体
 * @Date 21-11-29 上午10:22
 * @Version 1.0
 */
@Data
@ApiModel("设备联动分页查询对象")
public class PersonPermissionReq extends PageReq {

    @ApiModelProperty(value = "空间id", position = 1)
    private String id;

    @ApiModelProperty(value = "类型", position = 1,example = "SPACE/PROJECT/ACCESS_CONTROL_SPOT")
    private String type;

    @ApiModelProperty(value = "人员类型", position = 2)
    private PersonType personType;

    @ApiModelProperty(value = "人员信息", position = 3)
    private String personMsg;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "通行时间(开始时间)")
    private LocalDateTime startTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "通行时间(结束时间)")
    private LocalDateTime endTime;
}
