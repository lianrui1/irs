package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.DoorPlate;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

/**
 * @author ROG
 */
public interface DoorPlateDao extends JpaRepository<DoorPlate, String> {

    /**
     * 根据deviceID查询门牌信息
     * @param deviceId
     * @return
     * @author Yang.Lee
     */
    Optional<DoorPlate> findByDeviceId(String deviceId);


    /**
     * 根据门牌名称查询(分页)
     * @param name     名称查询关键字,模糊查询
     * @param pageable 分页信息
     * @return
     */
    Page<DoorPlate> findByNameContains(String name, Pageable pageable);

    /**
     * 获取会议室排序最大值
     * @return
     */
    @Query("SELECT MAX(doorplate.seq) FROM DoorPlate AS doorplate")
    Integer findMaxSeq();

    /**
     * 根据密码打开门牌
     */
    Optional<DoorPlate> findByIdAndPassword(String id, String password);

    /**
     * 根据门牌硬件编号查询
     *
     * @param hardwareId 硬件编号
     * @return 门牌信息
     * @author Yang.Lee
     * @date 2021/3/23 10:21
     **/
    Optional<DoorPlate> findByHardwareId(String hardwareId);

    /**
     * 根据设备查询门牌
     *
     * @author: yanzy
     * @data: 2022/9/8 15:09:06
     */
    Optional<DoorPlate> findByDevice(IotDevice iotDevice);

    Optional<DoorPlate> findByMagneticNum(String num);
}
