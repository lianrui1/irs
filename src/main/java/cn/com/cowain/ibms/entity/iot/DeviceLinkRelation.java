package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "iot_device_link_relation")
@org.hibernate.annotations.Table(appliesTo = "iot_device_link_relation", comment = "设备联动关系表")
@Where(clause = "is_del=0")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DeviceLinkRelation extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "device_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 设备'")
    private IotDevice iotDevice;

    @ManyToOne
    @JoinColumn(name = "device_link_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 设备联动基础信息'")
    private DeviceLink deviceLink;
}
