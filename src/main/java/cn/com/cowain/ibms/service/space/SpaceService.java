package cn.com.cowain.ibms.service.space;

import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.feign.bean.common.CommonReq;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.space.SpacePageReq;
import cn.com.cowain.ibms.rest.req.space.SpacePurposeEditReq;
import cn.com.cowain.ibms.rest.req.space.SpaceTreeReq;
import cn.com.cowain.ibms.rest.resp.BaseListResp;
import cn.com.cowain.ibms.rest.resp.iot.IotDeviceResp;
import cn.com.cowain.ibms.rest.resp.meeting.OngoingMeetingResp;
import cn.com.cowain.ibms.rest.resp.space.*;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.usercenter.common.req.PassportRoleEditReq;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Objects;

/**
 * 空间管理逻辑处理service
 *
 * @author Yang.Lee
 * @date 2020/12/21 9:12
 */
public interface SpaceService {
    /**
     * 新增空间信息
     *
     * @param spaceTreeReq
     * @return
     * @author Yang.Lee
     */
    ServiceResult save(SpaceTreeReq spaceTreeReq);

    /**
     * 编辑空间信息
     *
     * @param id
     * @param spaceTreeReq
     * @return
     * @author Yang.Lee
     */
    ServiceResult update(String id, SpaceTreeReq spaceTreeReq);

    /**
     * 格局空间ID删除空间
     *
     * @param spaceId
     * @return
     * @author Yang.Lee
     */
    ServiceResult delete(String spaceId);

    /**
     * 根据空间ID查询空间信息
     *
     * @param spaceId
     * @return
     * @author Yang.Lee
     */
    ServiceResult get(String spaceId);

    /**
     * 根据空间ID查询空间平面信息
     *
     * @param spaceId
     * @return
     */
    ServiceResult spacePlaneDetail(String spaceId);

    /**
     * 分页查询空间列表
     *
     * @param page
     * @param size
     * @return
     */
    ServiceResult getPage(int page, int size);

    /**
     * 根据项目ID获取所有空间列表
     *
     * @param projectID
     * @return
     * @author Yang.Lee
     */
    ServiceResult getListByProjectId(String projectID);

    /**
     * 查询所有空间列表（空间树）
     *
     * @return
     * @author Yang.Lee
     */
    ServiceResult getList();

    /**
     * 获取上级空间列表
     *
     * @param projectId
     * @param level
     * @return
     */
    ServiceResult getPreLevelList(String projectId, int level);

    /**
     * 按照创建时间排序
     *
     * @param source
     * @return
     */
    List<SpaceTreeResp> sortByCreateTime(List<SpaceTreeResp> source);

    /**
     * 查询项目内最大空间层级
     *
     * @param projectId
     * @return
     */
    ServiceResult getMaxLevel(String projectId);


    /**
     * 根据空间查询空间下的设备列表
     *
     * @param spaceId 空间ID
     * @return 业务处理结果，当ServiceResult.isSuccess() = true是，返回设备列表。否则返回错误信息（String）
     */
    ServiceResult getDeviceList(String spaceId);

    /**
     * 获取空间的全称，例：A楼 / 2层 / 会议室
     *
     * @param space   空间
     * @param content 当前层级名称
     * @return 空间全称
     * @author Yang.Lee
     * @date 2021/3/19 9:08
     **/
    static String getFullSpaceName(Space space, String content) {

        if (space == null) {
            return content;
        }

        StringBuilder stringBuilder = new StringBuilder(content);
        String parentName = "";

        if (space.getLevel() > 1) {
            Space parent = space.getParent();
            parentName = getFullSpaceName(parent, parent.getName());
            stringBuilder.insert(0, " / ").insert(0, parentName);
        }

        return stringBuilder.toString();
    }

    // 根据空间ID获取设备
    ServiceResult getDeviceListAll(String id, OngoingMeetingResp ongoingMeetingResp);

    // 根据空间ID获取可控制设备
    ServiceResult getDeviceListControl(String id);

    /**
     * 获取空间列表（无子空间）
     *
     * @param projectId 项目ID, null表示查询所有空间
     * @param parentId  父空间ID, null表示查询level = 1 的空间列表
     * @return 空间列表
     * @author Yang.Lee
     * @date 2021/6/28 10:49
     **/
    List<SpaceTreeResp> getListWithoutChildren(@Nullable String projectId, @Nullable String parentId);


    /**
     * 获取项目及空间树
     *
     * @return 结果
     * @author Yang.Lee
     * @date 2021/10/18 11:15
     **/
    List<ProjectSpaceTreeResp> getProjectSpaceTree();

    /**
     * 查询空间下的详情（包含设备，人员）
     *
     * @param spaceId 空间ID
     * @return 空间详情
     * @author Yang.Lee
     * @date 2021/11/15 15:03
     **/
    SpaceAllDetailResp getAllDetail(String spaceId);

    /**
     * 从空间中移除设备
     *
     * @param spaceId      空间id
     * @param deviceIdList 设备id列表
     * @return 操作结果
     * @author Yang.Lee
     * @date 2021/11/15 17:12
     **/
    ServiceResult removeDevices(String spaceId, List<String> deviceIdList);

    List<SpaceAllDetailShowRoomResp> getAllDetailForShowRoom(String spaceId) ;
    /**
     * 向空间中添加设备
     *
     * @param spaceId      空间ID
     * @param deviceIdList 设备id列表
     * @return 操作结果
     * @author Yang.Lee
     * @date 2021/11/15 18:42
     **/
    ServiceResult addDevices(String spaceId, List<String> deviceIdList);

    // 根据空间ID查询管理人员列表
    List<SpaceAdminResp> findSpaceAdminList(String spaceId);

    // 获取空间下设备列表(分页)
    PageBean<IotDeviceResp> getDevicePageBySpace(String id, int page, int size);

    /**
     * 添加空间用途
     *
     * @param req 请求参数
     * @return 添加结果
     * @author Yang.Lee
     * @date 2021/11/17 20:08
     **/
    ServiceResult addSpacePurpose(SpacePurposeEditReq req);

    /**
     * 获取空间用途列表
     *
     * @return 列表
     * @author Yang.Lee
     * @date 2021/11/17 20:29
     **/
    List<BaseListResp> getSpacePurposeList();

    /**
     * 获取空间列表（分页）
     *
     * @param req 请求参数
     * @return 分页列表
     * @author Yang.Lee
     * @date 2021/11/18 9:59
     **/
    PageBean<SpaceDetailResp> getListWithChildrenAllByProject(SpacePageReq req);

    /**
     * 获取空间下所有空间列表（分页）
     *
     * @param req 请求参数
     * @return 分页列表
     * @author Yang.Lee
     * @date 2021/11/18 9:59
     **/
    PageBean<SpaceDetailResp> getListWithChildrenAllBySpace(SpacePageReq req);

    /**
     * 获取BIM中的设备-空间数据
     *
     * @param parentSpaceId 父空间ID
     * @param keyword       查询关键字
     * @return 结果
     * @author Yang.Lee
     * @date 2021/11/18 22:17
     **/
    List<SpaceDeviceBIMResp> getBIMDeviceList(String parentSpaceId, String keyword);

    /**
     * 获取空间有权限操作设备的用户列表
     *
     * @param parentSpaceId 父空间ID
     * @param keyword       查询关键字
     * @return 结果
     * @author Yang.Lee
     * @date 2021/11/18 22:33
     **/
    List<SpaceDeviceBIMResp> getBIMUserList(String parentSpaceId, String keyword);

    /**
     * 查询用户空间内设备管理信息
     *
     * @param spaceId   空间ID
     * @param userEmpNo 用户工号
     * @return 详情
     * @author Yang.Lee
     * @date 2021/11/19 15:46
     **/
    SpaceAdminResp getUserSpaceAdmin(String spaceId, String userEmpNo);

    /**
     * 判断用户是否是空间管理员
     *
     * @param hrId    用户工号
     * @param spaceId 空间id
     * @return true: 是；false：否
     * @author Yang.Lee
     * @date 2021/11/25 9:49
     **/
    boolean isUserSpaceAdmin(String hrId, String spaceId);

    /**
     * 向UC的智能办公室权限发送请求
     *
     * @param spaceId 空间ID
     * @param req     编辑参数
     * @return 操作结果
     * @author Yang.Lee
     * @date 2021/11/29 11:01
     **/
    ServiceResult editUCIntelligentOfficeRole(String spaceId, PassportRoleEditReq req);

    /**
     * 将空间列表转换成空间树对象
     *
     * @param spaceList 空间列表
     * @return 空间树
     * @Yang.Lee
     */
    List<SpaceTreeResp> getSpaceTreeResp(List<Space> spaceList);

    // 获取空间下设备属性
    ServiceResult searchDeviceProperties(String spaceId);

    // 根据项目ID  空间ID获取楼层信息
    ServiceResult searchFloorTree(String projectId, String spaceId);

    // PC查询左侧项目楼栋树
    List<ProjectSpaceTreeResp> getSpaceTree();


     DeviceInfoShowRoomResp getDeviceInfo( String deviceId) ;
     DeviceInfoShowRoomResp getCreateTVDevice(CommonReq req);

    /**
     * 查询空间详情，优选从缓存进行查询
     * @param spaceId
     * @return
     */
     Space getSpaceFromCacheFirst(String spaceId);

     /**
     * 获取空间的全称，包括项目名称，例：项目A / A楼 / 2层 / 会议室
     *
     * @param space   空间
     * @return 空间全称
     **/
    static String getFullSpaceName(Space space) {
        if (space == null) {
            return "";
        }

        StringBuilder stringBuilder = new StringBuilder(space.getName());

        if (space.getLevel() > 1) {
            Space parent = space.getParent();
            String parentName = getFullSpaceName(parent);
            stringBuilder.insert(0, " / ").insert(0, parentName);
        }else if(Objects.nonNull(space.getProject())){
            stringBuilder.insert(0, " / ").insert(0, space.getProject().getProjectName());
        }
        return stringBuilder.toString();
    }

    /**
     * 根据父空间ID从缓存中获取所有子节点ID
     *
     * @param parentId
     * @return
     */
    List<String> getChildIdListByParentIdFromCache(String parentId);
}
