package cn.com.cowain.ibms.feign.uc;

import cn.com.cowain.sfp.comm.JsonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Yang.Lee
 * @date 2021/12/20 17:47
 */
@Service
@RequestMapping({"/api/v1/ms-user"})
@FeignClient(value = "ms-uc-v1", url = "${ms-uc-v1.url}")
public interface UCApiV1 {

    /**
     * 获取UC token
     *
     * @param userHrId 用户工号
     * @return token 信息
     * @author Yang.Lee
     * @date 2021/12/20 17:50
     **/
    @PostMapping("/staffNumber/login/{userHrId}")
    JsonResult<String> getUCToken(@PathVariable String userHrId);
}
