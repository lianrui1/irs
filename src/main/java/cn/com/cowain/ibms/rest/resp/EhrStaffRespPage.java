package cn.com.cowain.ibms.rest.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * EhrStaffRespPage
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/4 21:11
 */
@Data
@ApiModel("EHR 员工分页数据")
public class EhrStaffRespPage implements Serializable {

    /**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "记录总数",required = true)
    private Integer totalCount;

    @ApiModelProperty(value = "每页记录数",required = true)
    private Integer pageSize;

    @ApiModelProperty(value = "总页数",required = true)
    private Integer totalPage;

    @ApiModelProperty(value = "当前页码",required = true)
    private Integer currPage;

    @ApiModelProperty("数据列表")
    private List<EhrStaffResp> list;

}
