package cn.com.cowain.ibms.service;

import cn.com.cowain.ibms.utils.FileUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * @author wei.cheng
 * @date 2022/03/30 09:28
 */
@Slf4j
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RestTemplateTest {

    @Autowired
    private RestTemplate restTemplate;

    @Test
    public void testGet() {
        String url = "http://192.168.70.108:6040/pic?2dd150zb7-=s11c497464644e5=t1i9m*dp=*6pdi=*1sdi=*1b8i935e198c4e459--9a8290-c00id65*e7a5i18=";
        // 发送请求
        URI uri = null;
        try {
            uri = new URI(url);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        byte[] result = restTemplate.getForObject(uri, byte[].class);
        File file = new File("人脸.jpg");
        try {
            FileUtil.inputStreamToFile(new ByteArrayInputStream(result), file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
