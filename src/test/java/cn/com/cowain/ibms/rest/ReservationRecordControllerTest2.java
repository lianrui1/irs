package cn.com.cowain.ibms.rest;

import cn.com.cowain.ibms.service.ReservationRecordService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/11/20
 */
@WebMvcTest(ReservationRecordController.class)
class ReservationRecordControllerTest2 {

    @MockBean
    private ReservationRecordService reservationRecordService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void test1() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(API_BASE + "/reservation")
                .param("date", "2020-08-12")).andReturn();
        String content = mvcResult.getResponse().getContentAsString();
        System.out.println(content);
    }

}
