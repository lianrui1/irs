CREATE TABLE `ibms_user_room_used_time`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `created_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `version` int(11) NULL DEFAULT NULL COMMENT '版本号',
  `user_emp_no` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '员工工号',
  `used_time` int(11) NOT NULL COMMENT '使用时长',
  `room_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'FK',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKif3oadv68konsbym4mbom1uni`(`room_id`) USING BTREE,
  CONSTRAINT `FKif3oadv68konsbym4mbom1uni` FOREIGN KEY (`room_id`) REFERENCES `ibms_room` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户会议室使用时长记录表' ROW_FORMAT = Dynamic;


ALTER TABLE `ibms_prod`.`ibms_sys_user` ADD COLUMN `wx_head_img_url` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '微信头像 headImgUrl' AFTER `emp_no`;

ALTER TABLE `ibms_prod`.`iot_device` DROP COLUMN `hw_description`;

ALTER TABLE `ibms_prod`.`iot_device` DROP COLUMN `hw_fw_version`;

ALTER TABLE `ibms_prod`.`iot_device` DROP COLUMN `hw_gateway_id`;

ALTER TABLE `ibms_prod`.`iot_device` DROP COLUMN `hw_node_id`;

ALTER TABLE `ibms_prod`.`iot_device` DROP COLUMN `hw_sw_version`;

ALTER TABLE `ibms_prod`.`iot_door_magnetic` ADD COLUMN `address` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '设备位置' AFTER `version`;