package cn.com.cowain.ibms.entity.meal;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.enumeration.iot.AccessControlSpotPlace;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 报餐计划
 *
 * @author Yang.Lee
 * @date 2022/1/10 15:40
 */
@Data
@Entity
@Table(name = "ibms_meal_time_plan")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_meal_time_plan", comment = "IBMS 报餐计划")
@Where(clause = "is_del=0")
public class KsHtTimePlan extends BaseEntity {

    @Enumerated(EnumType.STRING)
    @Column(name = "place", nullable = false, columnDefinition = "varchar(500) COMMENT '报餐地址'")
    private AccessControlSpotPlace place;

    /**
     * 报餐开始时间
     */
    @Column(name = "start_time", nullable = false, columnDefinition = "varchar(500)  COMMENT '报餐开始时间'")
    private String startTime;

    /**
     * 报餐结束时间
     */
    @Column(name = "end_time", nullable = false, columnDefinition = "varchar(500)  COMMENT '报餐结束时间'")
    private String endTime;


    /**
     * 时间计划名
     */
    @Column(name = "name", nullable = false, columnDefinition = "varchar(500)  COMMENT '时间计划名'")
    private String name;
}
