package cn.com.cowain.ibms.rest.req.iot;

import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/12/3 15:56
 */
@Data
@ApiModel("门禁点人员通行记录请求对象")
public class SpotRecordPageReq extends PageReq {


    @ApiModelProperty(value = "姓名/工号")
    private String keyword;

    @ApiModelProperty(value = "门禁点ID")
    private String accessControlSpotId;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "通行时间(开始时间)", example = "2021-12-02 16:21:00")
    private LocalDateTime startTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "通行时间(结束时间)", example = "2021-12-02 16:21:00")
    private LocalDateTime endTime;
}
