package cn.com.cowain.ibms.feign.bean.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author feng
 * @title: CommonReq
 * @projectName ibms
 * @Date 2022/2/25 15:46
 */
@Data
public class CommonReq implements Serializable {
    @ApiModelProperty(value = "id")
    String id;
    @ApiModelProperty(value = "sn")
    String sn;
    @ApiModelProperty(value = "name")
    String name;
}