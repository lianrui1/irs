package cn.com.cowain.ibms.rest.req.iot;

import cn.com.cowain.ibms.enumeration.iot.PersonType;
import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * @Author tql
 * @Description PersonPassPageReq
 * @Date 21-11-9 下午9:05
 * @Version 1.0
 */
@Data
@ApiModel("人员通行参数对象")
public class PersonPassPageReq extends PageReq {

    @ApiModelProperty(value = "人员类型")
    private PersonType personType;

    @ApiModelProperty(value = "人员信息")
    private String personMsg;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm" )
    @ApiModelProperty(value = "通行时间(开始时间)")
    private LocalDateTime startTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm" )
    @ApiModelProperty(value = "通行时间(结束时间)")
    private LocalDateTime endTime;
}
