package cn.com.cowain.ibms.service.device.impl;

import cn.com.cowain.ibms.dao.ReservationRecordItemDao;
import cn.com.cowain.ibms.dao.device.*;
import cn.com.cowain.ibms.dao.iot.*;
import cn.com.cowain.ibms.dao.space.IotProductDao;
import cn.com.cowain.ibms.dao.space.ProjectDao;
import cn.com.cowain.ibms.dao.space.SpaceAdminDao;
import cn.com.cowain.ibms.dao.space.SpaceDeviceControlDao;
import cn.com.cowain.ibms.entity.ReservationRecordItem;
import cn.com.cowain.ibms.entity.iot.*;
import cn.com.cowain.ibms.entity.office.DeviceModeStatus;
import cn.com.cowain.ibms.entity.office.DeviceWorkingMode;
import cn.com.cowain.ibms.entity.space.IotProduct;
import cn.com.cowain.ibms.entity.space.Project;
import cn.com.cowain.ibms.entity.space.SpaceAdmin;
import cn.com.cowain.ibms.entity.space.SpaceDeviceControl;
import cn.com.cowain.ibms.enumeration.LogStatus;
import cn.com.cowain.ibms.enumeration.iot.DataSource;
import cn.com.cowain.ibms.enumeration.iot.DeviceControlStatus;
import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.mq.bean.InterFaceLogMessage;
import cn.com.cowain.ibms.mq.producer.DeviceControlLogProducer;
import cn.com.cowain.ibms.mq.producer.IotDeviceMessageProducer;
import cn.com.cowain.ibms.mq.producer.SendLogProducer;
import cn.com.cowain.ibms.rest.req.device.ServiceCapability;
import cn.com.cowain.ibms.rest.req.iot.centralized.CommandTemplate;
import cn.com.cowain.ibms.rest.req.iot.centralized.PropertiesTemplate;
import cn.com.cowain.ibms.rest.resp.device.*;
import cn.com.cowain.ibms.rest.resp.distinguish.SwitchResp;
import cn.com.cowain.ibms.rest.resp.iot.CircuitBreakerResp;
import cn.com.cowain.ibms.rest.resp.iot.IntelligentCircuitBreakerDataResp;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.*;
import cn.com.cowain.ibms.service.bean.amap.Devices;
import cn.com.cowain.ibms.service.bean.hw.AirServices;
import cn.com.cowain.ibms.service.bean.hwCloud.DeviceDetail;
import cn.com.cowain.ibms.service.device.HwProductsService;
import cn.com.cowain.ibms.service.space.ProjectService;
import cn.com.cowain.ibms.service.space.SpaceService;
import cn.com.cowain.ibms.utils.*;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.sfp.comm.utils.JwtUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Sort;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/2/26 15:54
 */
@Slf4j
@Service
public class HwProductsServiceImpl implements HwProductsService {

    @Value("${cowain.products.huawei.url}")
    private String productsUrl;
    @Value("${cowain.devcides.huawei.url}")
    private String devcidesUrl;
    @Value("${cowain.product.huawei.url}")
    private String productUrl;
    @Value("${cowain.door-magnetic.huawei.open.url}")
    private String commandUrl;
    @Value("${cowain.command.issued.huawei.url}")
    private String commandIssuedUrl;
    @Value("${cowain.data.huawei.url}")
    private String dataUrl;
    @Value(value = "${cowain.tb.id.huawei.url}")
    private String getTbIdUrl;
    @Value(value = "${cowain.dingding.url}")
    private String dingUrl;
    @Value(value = "${cowain.dingding.phone}")
    private String phone;
    @Value("${cowain.huawei.properties.url}")
    private String propertiesUrl;
    @Value("${cowain.device.properties.huawei.url}")
    private String switchPropertiesUrl;
    @Value("${cowain.air_conditioner.temp}")
    private int temp;
    @Value("${cowain.telemetry_time.minute}")
    private int minute;

    @Value("${cowain.devcides.deteail.url}")
    private String deviceDetailUrl;


    @Autowired
    private CircuitBreakerDao circuitBreakerDao;

    @Autowired
    private HwProductsDao productsDao;

    @Autowired
    private DeviceDao deviceDao;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private AirConditionerDao airConditionerDao;

    @Autowired
    private CurtainDao curtainDao;

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private IotProductDao iotProductDao;

    @Autowired
    private HardwareDao hardwareDao;

    @Autowired
    private DeviceDao iotDeviceDao;

    @Autowired
    private ReservationRecordItemDao itemDao;

    @Resource
    private IotDeviceNetworkChangeDao iotDeviceNetworkChangeDao;

    @Resource
    private IotDeviceMessageProducer iotDeviceMessageProducer;

    @Autowired
    private DeviceModeStatusDao modeStatusDao;

    @Autowired
    private DeviceWorkingModeDao workingModeDao;

    @Autowired
    private SpaceDeviceControlDao spaceDeviceControlDao;

    @Resource
    private SpaceAdminDao spaceAdminDao;

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserServiceUC;

    @Autowired
    private DeviceControlLogDao deviceControlLogDao;

    @Autowired
    private HwProductPropertiesDao hwProductPropertiesDao;

    @Autowired
    private DeviceControlLogProducer deviceControlLogProducer;

    @Autowired
    private SendLogProducer sendLogProducer;

    @Resource
    private RestTemplateUtil restTemplateUtil;

    @Autowired
    private RedisUtil redisUtil;

    @Override
    public CircuitBreakerResp getIntelligentCircuitBreakerData(IotDevice device, String startTime, String endTime) {
        // 日期转化
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        DateTimeFormatter fmt1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime sTime = LocalDateTime.parse(startTime + " 00:00:00", fmt);
        LocalDateTime eTime = LocalDateTime.parse(endTime + " 23:59:59", fmt);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        // 获取区间日期
        List<String> strings = new ArrayList<>();
        try {
            Date startDate = dateFormat.parse(startTime);
            Date endDate = dateFormat.parse(endTime);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(startDate);
            strings.add(dateFormat.format(startDate));
            while (calendar.getTime().before(endDate)) {
                calendar.add(Calendar.DAY_OF_MONTH, 1);
                strings.add(dateFormat.format(calendar.getTime()));

            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        // 根据设备及日期范围查出期间用电数据
        List<CircuitBreaker> list = circuitBreakerDao.findByIotDeviceAndCreatedTimeGreaterThanEqualAndCreatedTimeLessThanEqualOrderByCreatedTime(device, sTime, eTime);
        List<IntelligentCircuitBreakerDataResp> circuitBreakerList = new ArrayList<>();
        CircuitBreakerResp circuitBreakerResp = new CircuitBreakerResp();
        int sum = 0;
        for (CircuitBreaker circuitBreaker : list) {
            IntelligentCircuitBreakerDataResp circuitBreakers = new IntelligentCircuitBreakerDataResp();
            circuitBreakers.setDatetime(circuitBreaker.getDateTime().toString());
            circuitBreakers.setDeviceId(circuitBreaker.getIotDevice().getId());
            circuitBreakers.setElectricity(circuitBreaker.getDayElectricity());
            circuitBreakerList.add(circuitBreakers);
            sum += circuitBreaker.getDayElectricity();
        }
        List<IntelligentCircuitBreakerDataResp> circuitBreakerList1 = new ArrayList<>();
        for (int i = 0; i < strings.size(); i++) {
            IntelligentCircuitBreakerDataResp circuitBreakers = new IntelligentCircuitBreakerDataResp();
            circuitBreakers.setDatetime(LocalDate.parse(strings.get(i), fmt1).toString());
            circuitBreakers.setElectricity(0);
            circuitBreakerList1.add(circuitBreakers);
        }
        for (IntelligentCircuitBreakerDataResp circuitBreakerDataResp : circuitBreakerList1) {
            for (IntelligentCircuitBreakerDataResp circuitBreakerDataResp1 : circuitBreakerList) {
                if (circuitBreakerDataResp1.getDatetime().equals(circuitBreakerDataResp.getDatetime())) {
                    circuitBreakerDataResp.setElectricity(circuitBreakerDataResp1.getElectricity());
                }
            }
        }
        // 赋值
        circuitBreakerResp.setCircuitBreakerDataResps(circuitBreakerList1);
        circuitBreakerResp.setSum(sum);
        return circuitBreakerResp;
    }

    @Override
    @Transactional
    public void findHwProducts() {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyyMMdd'T'HHmmss'Z'");
        List<IotProduct> productsList = new ArrayList<>();
        // 发送Http请求
        HttpHeaders headers = new HttpHeaders();

        // 获取华为云产品列表
        HttpEntity<IotProduct> requestGet = new HttpEntity<>(headers);
        ResponseEntity<String> response = restTemplate.exchange(productsUrl + "?marker=", HttpMethod.GET, requestGet, String.class);
        String body = response.getBody();
        // 解析数据
        JSONObject jsonObject = JSON.parseObject(body);
        JSONObject jsonData = jsonObject.getJSONObject("data");
        JSONArray jsonIndex = jsonData.getJSONArray("products");
        List<HwProductsResp> list = JSON.parseArray(jsonIndex.toJSONString(), HwProductsResp.class);
        JSONObject total = jsonData.getJSONObject("page");
        String marker = total.getString("marker");
        Integer count = Integer.parseInt(total.getString("count"));
        Integer page = count / 50;
        if (page >= 1) {
            for (int i = 0; i < page; i++) {
                ResponseEntity<String> response1 = restTemplate.exchange(productsUrl + "?marker=" + marker, HttpMethod.GET, requestGet, String.class);
                String body1 = response1.getBody();
                // 解析数据
                JSONObject jsonObject1 = JSON.parseObject(body1);
                JSONObject jsonData1 = jsonObject1.getJSONObject("data");
                JSONArray jsonIndex1 = jsonData1.getJSONArray("products");
                List<HwProductsResp> list1 = JSON.parseArray(jsonIndex1.toJSONString(), HwProductsResp.class);
                list.addAll(list1);
                JSONObject total1 = jsonData1.getJSONObject("page");
                String marker1 = total1.getString("marker");
                marker = marker1;
            }
        }
        int no = 0;
        for (HwProductsResp hwProducts : list) {
            IotProduct iotProduct = new IotProduct();
            BeanUtils.copyProperties(hwProducts, iotProduct);
            iotProduct.setHwProductId(hwProducts.getProductId());
            iotProduct.setHwName(hwProducts.getName());
            iotProduct.setHwDeviceType(hwProducts.getDeviceType());
            iotProduct.setHwProtocolType(hwProducts.getProtocolType());
            iotProduct.setHwDataFormat(hwProducts.getDataFormat());
            iotProduct.setHwManufacturerName(hwProducts.getManufacturerName());
            iotProduct.setHwIndustry(hwProducts.getIndustry());
            iotProduct.setHwDescription(hwProducts.getDescription());
            iotProduct.setHwCreateTime(LocalDateTime.parse(hwProducts.getCreateTime(), df));
            //产品编号
            Integer maxProductNo = iotProductDao.findMaxProductNo();
            if (maxProductNo != null) {
                maxProductNo = maxProductNo + 1 + no++;
            } else {
                maxProductNo = 1 + no++;
            }

            iotProduct.setProductNo(maxProductNo);
            productsList.add(iotProduct);
        }
        // 数据比较  将新增的产品存入数据库
        List<IotProduct> all = productsDao.findAll();
        List<IotProduct> newList = productsList.stream().filter(a ->
                !all.stream().map(IotProduct::getHwProductId).collect(Collectors.toList()).contains(a.getHwProductId())
        ).collect(Collectors.toList());
        productsDao.saveAll(newList);
    }

    @Override
    @Transactional
    public void findHwDevices() {
        List<IotDevice> devicesList = new ArrayList<>();
        // 发送Http请求
        HttpHeaders headers = new HttpHeaders();
        // 获取华为云设备列表
        HttpEntity<IotDevice> requestGet = new HttpEntity<>(headers);
        ResponseEntity<String> response = restTemplate.exchange(devcidesUrl, HttpMethod.GET, requestGet, String.class);
        String body = response.getBody();
        // 解析数据
        JSONObject jsonObject = JSON.parseObject(body);
        JSONObject jsonData = jsonObject.getJSONObject("data");
        JSONArray jsonIndex = jsonData.getJSONArray("devices");
        List<HwDevicesResp> list = JSON.parseArray(jsonIndex.toJSONString(), HwDevicesResp.class);
        JSONObject total = jsonData.getJSONObject("page");
        String marker = total.getString("marker");
        Integer count = Integer.parseInt(total.getString("count"));
        Integer page = count / 50;
        if (page >= 1) {
            for (int i = 0; i < page; i++) {
                ResponseEntity<String> response1 = restTemplate.exchange(devcidesUrl + marker, HttpMethod.GET, requestGet, String.class);
                String body1 = response1.getBody();
                // 解析数据
                JSONObject jsonObject1 = JSON.parseObject(body1);
                JSONObject jsonData1 = jsonObject1.getJSONObject("data");
                JSONArray jsonIndex1 = jsonData1.getJSONArray("devices");
                List<HwDevicesResp> list1 = JSON.parseArray(jsonIndex1.toJSONString(), HwDevicesResp.class);
                list.addAll(list1);
                JSONObject total1 = jsonData1.getJSONObject("page");
                String marker1 = total1.getString("marker");
                marker = marker1;
            }
        }
        int no = 0;
        int seq = 0;
        // 查询设备编号
        Integer maxDeviceNo = iotDeviceDao.findMaxDeviceNo();
        // 查询设备排序信息
        Integer maxSort = iotDeviceDao.findMaxSeq();
        // 查询默认项目组
        Optional<Project> projectOptional = projectDao.findByNo(ProjectService.DEFAULT_PROJECT_NUMBER);
        for (HwDevicesResp hwDevices : list) {
            IotDevice iotDevice = new IotDevice();
            BeanUtils.copyProperties(hwDevices, iotDevice);
            iotDevice.setHwDeviceId(hwDevices.getDeviceId());
            iotDevice.setHwDeviceName(hwDevices.getDeviceName());
            iotDevice.setHwNodeType(hwDevices.getNodeType());
            iotDevice.setHwProductId(hwDevices.getProductId());
            iotDevice.setHwProductName(hwDevices.getProductName());
            iotDevice.setHwStatus(DeviceStatus.valueOf(hwDevices.getStatus()));
            iotDevice.setHwGatewayId(hwDevices.getGatewayId());
            iotDevice.setSn(hwDevices.getNodeId());
            //设备编号
            if (maxDeviceNo != null) {
                maxDeviceNo = maxDeviceNo + 1 + no++;
            } else {
                maxDeviceNo = 0 + no++;
            }
            iotDevice.setDeviceNo(maxDeviceNo);
            // 设备排序
            if (maxSort != null) {
                maxSort = maxSort + 1 + seq++;
            } else {
                maxSort = 0 + seq++;
            }
            iotDevice.setSeq(maxSort);
            // 根据SN码获取硬件清单信息存入设备
            Optional<Hardware> byHardWareSn = hardwareDao.findByHardWareSn(hwDevices.getNodeId());
            if (byHardWareSn.isPresent()) {
                Hardware hardware = byHardWareSn.get();
                // 硬件清单中文名称
                iotDevice.setDeviceName(hardware.getNameZh());
                // 硬件清单产品
                iotDevice.setIotProduct(hardware.getIotProduct());
                // 硬件清单设备类型
                iotDevice.setIotNodeType(hardware.getDeviceType());
            }

            // 保存至默认项目  项目编号为00000000
            if (projectOptional.isPresent()) {
                iotDevice.setProject(projectOptional.get());
            }
            // 设备类型保存默认
            iotDevice.setDeviceType(DeviceType.DEFAULT);
            // 设备来源默认华为
            iotDevice.setDataSource(DataSource.HW_CLOUD);
            devicesList.add(iotDevice);
        }

        updateDeviceStatusChange(devicesList);

        // 数据比较  将新增的设备存入数据库
        List<IotDevice> all = deviceDao.findAll();
        // 判断设备是否包含网关id 如果为空则保存
        List<IotDevice> arrayList = new ArrayList<>();
        for (IotDevice device : all) {
            for (IotDevice hwDevice : devicesList) {
                if (device.getHwDeviceId() != null && device.getHwDeviceId().equals(hwDevice.getHwDeviceId()) && StringUtils.isBlank(device.getHwGatewayId())) {
                    device.setHwGatewayId(hwDevice.getHwGatewayId());
                    arrayList.add(device);
                }
            }
        }

        for (IotDevice device : devicesList) {
            if (!all.toString().contains(device.getHwDeviceName())) {
//                device.setLastHwStatusChangedDatetime(LocalDateTime.now());
                arrayList.add(device);
                // 新设备添加网络变换
//                IotDeviceNetworkChange iotDeviceNetworkChange = new IotDeviceNetworkChange();
//                iotDeviceNetworkChange.setDevice(device);
//                iotDeviceNetworkChange.setDeviceStatus(device.getHwStatus());
//                iotDeviceNetworkChange.setDataTime(LocalDateTime.now());
//                iotDeviceNetworkChangeDao.save(iotDeviceNetworkChange);
            } else {
                // 判断该设备状态是否发生变化
                Optional<IotDevice> d = deviceDao.findByHwDeviceId(device.getHwDeviceId());
                if (d.isPresent()) {
                    IotDevice dev = d.get();
                    if (dev.getHwStatus().equals(DeviceStatus.ONLINE) && device.getHwStatus().equals(DeviceStatus.OFFLINE)) {
                        try {

                            //是否通知所有人
                            boolean isAtAll = false;
                            //通知具体人的手机号码列表
                            List<String> mobileList = Lists.newArrayList();
                            mobileList.add(phone);
                            //钉钉机器人消息内容
                            String content = device.getHwDeviceName() + "设备离线";
                            //组装请求内容
                            String reqStr = buildReqStr(content, isAtAll, mobileList);

                            //推送消息（http请求）
                            HttpUtil.post(dingUrl, reqStr);
                        } catch (Exception e) {
                            log.error(e.getMessage(), e);
                        }
                    }
                    // 直连设备网络状态变化更改
                    if (dev.getHwStatus() != device.getHwStatus() && !dev.getHwNodeType().equals("ENDPOINT")) {
                        dev.setHwStatus(device.getHwStatus());
//                        dev.setLastHwStatusChangedDatetime(LocalDateTime.now());
                        dev.setHwGatewayId(device.getHwGatewayId());
                        arrayList.add(dev);
                    } else if (dev.getHwDeviceId().equals(device.getHwDeviceId()) && !dev.getHwDeviceName().equals(device.getHwDeviceName())) {
                        BeanUtils.copyProperties(device, dev);
                        arrayList.add(dev);
                    }
                    if (dev.getHwNodeType().equals("ENDPOINT")) {
                        // 获取网关设备信息
                        Optional<IotDevice> deviceOp = deviceDao.findByHwDeviceIdAndHwNodeType(dev.getHwGatewayId(), "GATEWAY");
                        if (deviceOp.isPresent() && dev.getHwStatus() != deviceOp.get().getHwStatus()) {
                            dev.setHwStatus(deviceOp.get().getHwStatus());
                            arrayList.add(dev);
                        }
                    }
                }
            }
        }
        Collections.reverse(arrayList);
        arrayList = arrayList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(IotDevice::getHwDeviceId))), ArrayList::new));

        for (IotDevice device : arrayList) {
            Optional<IotDevice> byId = deviceDao.findById(device.getId());
            if (byId.isPresent()) {
                device.setVersion(byId.get().getVersion());
            }
        }
        deviceDao.saveAll(arrayList);


    }

    @Transactional
    public void updateDeviceStatusChange(List<IotDevice> hwDeviceList) {
        // 找出设备状态发生变化的设备，并记录
        List<IotDevice> networkChangedList = hwDeviceList.stream().filter(obj -> {

            Optional<IotDevice> dbDeviceOp = deviceDao.findByHwDeviceId(obj.getHwDeviceId());
            if (dbDeviceOp.isPresent()) {

                obj.setId(dbDeviceOp.get().getId());
                DeviceStatus hwStatus = obj.getHwStatus();
                if (obj.getHwNodeType().equals("ENDPOINT")) {
                    // 查找对应网关的id
                    String parentGatewayId = obj.getHwGatewayId();
                    // 找到对应网关的状态
                    Optional<IotDevice> gatewayDeviceOptional = hwDeviceList.stream().filter(device -> device.getHwDeviceId().equals(parentGatewayId)).findFirst();
                    if (gatewayDeviceOptional.isPresent()) {
                        hwStatus = gatewayDeviceOptional.get().getHwStatus();
                        obj.setHwStatus(hwStatus);
                    }
                }

                return !dbDeviceOp.get().getHwStatus().equals(hwStatus);

            } else {
                // 数据库中并无该设备记录, 不记录
                return true;
            }

        }).collect(Collectors.toList());

        networkChangedList.forEach(device -> {

            Optional<IotDevice> dbDeviceOp = deviceDao.findByHwDeviceId(device.getHwDeviceId());
            IotDevice finalDevice;
            if (dbDeviceOp.isEmpty()) {
                finalDevice = device;

            } else {
                finalDevice = dbDeviceOp.get();
            }
            finalDevice.setLastHwStatusChangedDatetime(LocalDateTime.now());
            deviceDao.save(finalDevice);

            IotDeviceNetworkChange iotDeviceNetworkChange = new IotDeviceNetworkChange();
            iotDeviceNetworkChange.setDevice(device);
            iotDeviceNetworkChange.setDeviceStatus(device.getHwStatus());
            iotDeviceNetworkChange.setDataTime(LocalDateTime.now());
            iotDeviceNetworkChangeDao.save(iotDeviceNetworkChange);

        });
    }

    @Override
    @Transactional
    public DoorMagneticHWReq findProduct(String hwProductId) {
        // url
        StringBuilder urlTemplate = new StringBuilder();
        urlTemplate.append(productUrl).append("/?productId=").append(hwProductId);
        // 发送Http请求
        HttpHeaders headers = new HttpHeaders();

        // 获取华为云产品属性
        HttpEntity<IotDevice> requestGet = new HttpEntity<>(headers);
        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(urlTemplate.toString(), HttpMethod.GET, requestGet, String.class);
        } catch (Exception e) {
            DoorMagneticHWReq hwReq = new DoorMagneticHWReq();
            hwReq.setRead("error");
            log.error(e.getMessage(), e);
            return hwReq;
        }
        String body = response.getBody();
        // 解析数据
        JSONObject jsonObject = JSON.parseObject(body);
        JSONObject jsonData = jsonObject.getJSONObject("data");
        JSONArray jsonIndex = jsonData.getJSONArray("service_capabilities");
        List<ServiceCapability> list = JSON.parseArray(jsonIndex.toJSONString(), ServiceCapability.class);
        DoorMagneticHWReq service = new DoorMagneticHWReq();
        service.setService_id(list.get(0).getServiceId());
        if (list.get(0).getCommands() != null) {
            service.setCommand_name(list.get(0).getCommands().get(0).getCommandName());
        }
        return service;
    }

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public ServiceResult command(DoorMagneticHWReq hwReq, String hrId, String name) {
        // 保存设备操作日志
        //saveDeviceControlLog(hwReq, hrId, name);
        // 保存设备控制日志至iotc
        DeviceControlLogMessage logMessage = new DeviceControlLogMessage();
        logMessage.setHrId(hrId);
        logMessage.setName(name);
        if (StringUtils.isNotEmpty(hwReq.getMode())) {
            logMessage.setMode(hwReq.getMode());
        }
        logMessage.setHwDeviceId(hwReq.getDevice_id());
        Optional<IotDevice> deviceOptional = deviceDao.findByHwDeviceId(hwReq.getDevice_id());
        if (deviceOptional.isEmpty()) {
            return ServiceResult.error("控制失败,设备不存在");
        }
        IotDevice iotDevice = deviceOptional.get();
        logMessage.setHwDeviceName(iotDevice.getHwDeviceName());
        logMessage.setAddress(SpaceService.getFullSpaceName(iotDevice.getSpace(), iotDevice.getSpace().getName()));
        logMessage.setSource("IBMS");
        logMessage.setCommand(JSON.toJSONString(hwReq.getParas()));
        logMessage.setControlTime(LocalDateTime.now().withNano(0));

        Optional<AirConditioner> airConditionerOp = airConditionerDao.findByDeviceHwDeviceId(hwReq.getDevice_id());
        if (airConditionerOp.isPresent() && 1 == airConditionerOp.get().getIsOne()) {
            hwReq.setIsOne("1");
        }
        Optional<Curtain> curtainOp = curtainDao.findByDeviceHwDeviceId(hwReq.getDevice_id());
        if (curtainOp.isPresent() && 1 == curtainOp.get().getIsOne()) {
            hwReq.setIsOne("1");
        }

        if ("0".equals(hwReq.getIsOne())) {
            // json参数
            String requestParam = JSON.toJSONString(hwReq);
            log.debug("json参数 ： " + requestParam);
            // 发送Http请求 下发控制命令
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> requestEntity = new HttpEntity<>(requestParam, headers);
            ResponseEntity<String> responseEntity;

            // 保存接口日志至iotc
            InterFaceLogMessage interFaceLogMessage = new InterFaceLogMessage();
            interFaceLogMessage.setType("post");
            interFaceLogMessage.setArg(requestParam);
            interFaceLogMessage.setUrl(commandUrl);
            interFaceLogMessage.setSource("IBMS");
            interFaceLogMessage.setMsgTime(DateUtils.formatLocalDateTime(LocalDateTime.now(), DateUtils.PATTERN_DATETIME));

            try {
                responseEntity = restTemplate.postForEntity(commandUrl, requestEntity, String.class);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                logMessage.setResult(e.getMessage());
                logMessage.setDeviceControlStatus(DeviceControlStatus.FAIL);
                deviceControlLogProducer.push(logMessage);

                interFaceLogMessage.setLogStatus(LogStatus.EXCEPTION);
                interFaceLogMessage.setReturnCode(IConst.SERVICE_ERROR);
                interFaceLogMessage.setReturnMsg("请求超时");
                sendLogProducer.sendLogMsg(interFaceLogMessage);
                return ServiceResult.error("控制失败");
            }

            // 解析请求结果
            String responseBody = responseEntity.getBody();
            JSONObject jsonResult = JSON.parseObject(responseBody);
            Integer status = jsonResult.getInteger("status");

            interFaceLogMessage.setReturnCode(jsonResult.getString("code") == null ? "" : jsonResult.getString("code"));
            interFaceLogMessage.setReturnMsg(jsonResult.getString("msg") == null ? "" : jsonResult.getString("msg"));
            interFaceLogMessage.setReturnData(jsonResult.getString("data") == null ? "" : jsonResult.getString("data"));

            if (status != 1) {
                interFaceLogMessage.setLogStatus(LogStatus.EXCEPTION);
                sendLogProducer.sendLogMsg(interFaceLogMessage);

                logMessage.setResult(responseBody);
                logMessage.setDeviceControlStatus(DeviceControlStatus.FAIL);
                deviceControlLogProducer.push(logMessage);
                return ServiceResult.error("设备控制失败");
            }
            String data = jsonResult.getString("data");
            JSONObject jsonData = JSON.parseObject(data);
            // 根据返回的数据中是否包含error_code判断请求成功与否

            if (jsonData.containsKey("error_code")) {
                interFaceLogMessage.setLogStatus(LogStatus.EXCEPTION);
                sendLogProducer.sendLogMsg(interFaceLogMessage);

                logMessage.setResult(responseBody);
                logMessage.setDeviceControlStatus(DeviceControlStatus.FAIL);
                deviceControlLogProducer.push(logMessage);
                return ServiceResult.error(responseBody);
            }

            logMessage.setResult(responseBody);
            logMessage.setDeviceControlStatus(DeviceControlStatus.SUCCESS);
            deviceControlLogProducer.push(logMessage);

            interFaceLogMessage.setLogStatus(LogStatus.NORMAL);
            sendLogProducer.sendLogMsg(interFaceLogMessage);

        } else if ("1".equals(hwReq.getIsOne())) {
            CommandReq commandReq = new CommandReq();
            BeanUtils.copyProperties(hwReq, commandReq);
            // 窗帘开关
            if (StringUtils.isNotEmpty(hwReq.getParas().getAction())) {
                switch (hwReq.getParas().getAction()) {
                    case "open":
                        commandReq.setParas("{" + "\"open\":" + 1 + "}");
                        break;
                    case "close":
                        commandReq.setParas("{" + "\"close\":" + 1 + "}");
                        break;
                    case "stop":
                        commandReq.setParas("{" + "\"stop\":" + 1 + "}");
                        break;
                    default:
                        break;
                }
                ServiceResult result = controlDevice(commandReq, hrId, name);
                if (!result.isSuccess()) {
                    return ServiceResult.error(result.getObject());
                }
            }
            // 开关
            if (StringUtils.isNotEmpty(hwReq.getParas().getSetPower())) {
                if (hwReq.getParas().getSetPower().equals("on")) {
                    hwReq.getParas().setSetPower("1");
                } else if (hwReq.getParas().getSetPower().equals("off")) {
                    hwReq.getParas().setSetPower("0");
                }
                commandReq.setParas("{" + "\"setPower\":" + Integer.parseInt(hwReq.getParas().getSetPower()) + "}");
                ServiceResult result = controlDevice(commandReq, hrId, name);
                if (!result.isSuccess()) {
                    return ServiceResult.error(result.getObject());
                } else if (hwReq.getParas().getSetPower().equals("off") || hwReq.getParas().getSetPower().equals("0")) {
                    // 将空调命令参数保存至redis
                    saveDeviceParasToRedis(iotDevice, hwReq.getParas());
                    // 命令下发成功 保存操作本次操作参数
                    saveDeviceParas(hwReq);
                    return ServiceResult.ok();
                }
                //休眠一秒钟
                try {
                    Thread.sleep(1250);
                } catch (InterruptedException e) {
                    // Restore interrupted state...
                    Thread.currentThread().interrupt();
                }
            }
            // 温度
            if (hwReq.getParas().getSetTemp() != null) {
                commandReq.setParas("{" + "\"setTemp\":" + hwReq.getParas().getSetTemp() + "}");
                ServiceResult result = controlDevice(commandReq, hrId, name);
                if (!result.isSuccess()) {
                    return ServiceResult.error(result.getObject());
                }
            }
            // 模式
            if (StringUtils.isNotEmpty(hwReq.getParas().getSetMode())) {
                switch (hwReq.getParas().getSetMode()) {
                    case "fan":
                        hwReq.getParas().setSetMode("1");
                        break;
                    case "cool":
                        hwReq.getParas().setSetMode("2");
                        break;
                    case "heat":
                        hwReq.getParas().setSetMode("3");
                        break;
                    case "auto":
                        hwReq.getParas().setSetMode("4");
                        break;
                    case "dry":
                        hwReq.getParas().setSetMode("5");
                        break;
                    default:
                        break;
                }
                commandReq.setParas("{" + "\"setMode\": " + Integer.parseInt(hwReq.getParas().getSetMode()) + "}");
                ServiceResult result = controlDevice(commandReq, hrId, name);
                if (!result.isSuccess()) {
                    return ServiceResult.error(result.getObject());
                }
            }
            // 风速
            if (StringUtils.isNotEmpty(hwReq.getParas().getSetFanSpeed())) {
                switch (hwReq.getParas().getSetFanSpeed()) {
                    case "auto":
                        hwReq.getParas().setSetFanSpeed("0");
                        break;
                    case "min":
                        hwReq.getParas().setSetFanSpeed("9");
                        break;
                    case "med":
                        hwReq.getParas().setSetFanSpeed("10");
                        break;
                    case "max":
                        hwReq.getParas().setSetFanSpeed("11");
                        break;
                    default:
                        break;
                }
                commandReq.setParas("{" + "\"setFanSpeed\": " + Integer.parseInt(hwReq.getParas().getSetFanSpeed()) + "}");
                ServiceResult result = controlDevice(commandReq, hrId, name);
                if (!result.isSuccess()) {
                    return ServiceResult.error(result.getObject());
                }
            }

        }

        // 将空调命令参数保存至redis
        saveDeviceParasToRedis(iotDevice, hwReq.getParas());

        // 命令下发成功 保存操作本次操作参数
        saveDeviceParas(hwReq);
        return ServiceResult.ok();
    }

    private void saveDeviceParas(DoorMagneticHWReq hwReq) {
        Optional<IotDevice> byHwDeviceId = deviceDao.findByHwDeviceId(hwReq.getDevice_id());
        if (byHwDeviceId.isPresent()) {
            // 开关
            if (StringUtils.isNotEmpty(hwReq.getParas().getSetPower())) {
                if (hwReq.getParas().getSetPower().equals("1")) {
                    hwReq.getParas().setSetPower("on");
                } else if (hwReq.getParas().getSetPower().equals("0")) {
                    hwReq.getParas().setSetPower("off");
                }
            }
            // 模式
            if (StringUtils.isNotEmpty(hwReq.getParas().getSetMode())) {
                switch (hwReq.getParas().getSetMode()) {
                    case "1":
                        hwReq.getParas().setSetMode("fan");
                        break;
                    case "2":
                        hwReq.getParas().setSetMode("cool");
                        break;
                    case "3":
                        hwReq.getParas().setSetMode("heat");
                        break;
                    case "4":
                        hwReq.getParas().setSetMode("auto");
                        break;
                    case "5":
                        hwReq.getParas().setSetMode("dry");
                        break;
                    default:
                        break;
                }
            }

            // 风速
            if (StringUtils.isNotEmpty(hwReq.getParas().getSetFanSpeed())) {
                switch (hwReq.getParas().getSetFanSpeed()) {
                    case "0":
                        hwReq.getParas().setSetFanSpeed("auto");
                        break;
                    case "9":
                        hwReq.getParas().setSetFanSpeed("min");
                        break;
                    case "10":
                        hwReq.getParas().setSetFanSpeed("med");
                        break;
                    case "11":
                        hwReq.getParas().setSetFanSpeed("max");
                        break;
                    default:
                        break;
                }
            }

            // 保存空调下发命令参数
            Optional<AirConditioner> airOp = airConditionerDao.findByDevice(byHwDeviceId.get());
            if (airOp.isPresent()) {
                AirConditioner airConditioner = airOp.get();
                if (hwReq.getParas().getSetPower() != null) {
                    airConditioner.setSetPower(StringUtils.isNotEmpty(hwReq.getParas().getSetPower()) ? hwReq.getParas().getSetPower() : "");
                    airConditioner.setMode(StringUtils.isNotEmpty(hwReq.getParas().getSetMode()) ? hwReq.getParas().getSetMode() : "");
                    airConditioner.setTemperature(hwReq.getParas().getSetTemp() != null ? hwReq.getParas().getSetTemp() : 0);
                    airConditioner.setWindDirection(StringUtils.isNotEmpty(hwReq.getParas().getSetFanDirection()) ? hwReq.getParas().getSetFanDirection() : "");
                    airConditioner.setWindSpeed(StringUtils.isNotEmpty(hwReq.getParas().getSetFanSpeed()) ? hwReq.getParas().getSetFanSpeed() : "");
                    airConditioner.setSetFanAuto(StringUtils.isNotEmpty(hwReq.getParas().getSetFanAuto()) ? hwReq.getParas().getSetFanAuto() : "");
                    airConditioner.setSetSwing(StringUtils.isNotEmpty(hwReq.getParas().getSetSwing()) ? hwReq.getParas().getSetSwing() : "");
                    airConditionerDao.save(airConditioner);
                }

            } else if (hwReq.getParas().getSetPower() != null && DeviceType.AIR_CONDITIONER.equals(byHwDeviceId.get().getDeviceType())) {
                AirConditioner air = new AirConditioner();
                air.setDevice(byHwDeviceId.get());
                air.setSetPower(hwReq.getParas().getSetPower());
                air.setMode(hwReq.getParas().getSetMode());
                air.setTemperature(hwReq.getParas().getSetTemp());
                air.setWindDirection(hwReq.getParas().getSetFanDirection());
                air.setWindSpeed(hwReq.getParas().getSetFanSpeed());
                air.setSetFanAuto(hwReq.getParas().getSetFanAuto());
                airConditionerDao.save(air);
            }

            // 保存窗帘命令下发参数
            if (hwReq.getParas().getAction() != null && byHwDeviceId.get().getDeviceType().equals(DeviceType.CURTAINS)) {
                Optional<Curtain> curOp = curtainDao.findByDevice(byHwDeviceId.get());
                if (curOp.isPresent()) {
                    Curtain curtain = curOp.get();
                    curtain.setAction(hwReq.getParas().getAction());
                    curtainDao.save(curtain);
                } else {
                    Curtain curtain = new Curtain();
                    curtain.setDevice(byHwDeviceId.get());
                    curtain.setAction(hwReq.getParas().getAction());
                    curtainDao.save(curtain);
                }
            }
        }
    }

    private ServiceResult controlDevice(CommandReq commandReq, String hrId, String name) {

        // 保存设备控制日志至iotc
        DeviceControlLogMessage logMessage = new DeviceControlLogMessage();
        logMessage.setHrId(hrId);
        logMessage.setName(name);
        if (StringUtils.isNotEmpty(commandReq.getMode())) {
            logMessage.setMode(commandReq.getMode());
        }
        logMessage.setHwDeviceId(commandReq.getDevice_id());
        Optional<IotDevice> deviceOptional = deviceDao.findByHwDeviceId(commandReq.getDevice_id());
        if (deviceOptional.isEmpty()) {
            return ServiceResult.error("控制失败,设备不存在");
        }
        IotDevice iotDevice = deviceOptional.get();
        logMessage.setHwDeviceName(iotDevice.getHwDeviceName());
        logMessage.setAddress(SpaceService.getFullSpaceName(iotDevice.getSpace(), iotDevice.getSpace().getName()));
        logMessage.setSource("IBMS");
        logMessage.setCommand(JSON.toJSONString(commandReq.getParas()));
        logMessage.setControlTime(LocalDateTime.now().withNano(0));

        // json参数
        String requestParam = JSON.toJSONString(commandReq);
        log.debug("json参数 ： " + requestParam);
        // 发送Http请求 下发控制命令
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity<>(requestParam, headers);
        ResponseEntity<String> responseEntity;

        // 保存接口日志至iotc
        InterFaceLogMessage interFaceLogMessage = new InterFaceLogMessage();
        interFaceLogMessage.setType("post");
        interFaceLogMessage.setArg(requestParam);
        interFaceLogMessage.setUrl(commandUrl);
        interFaceLogMessage.setSource("IBMS");
        interFaceLogMessage.setMsgTime(DateUtils.formatLocalDateTime(LocalDateTime.now(), DateUtils.PATTERN_DATETIME));
        try {
            responseEntity = restTemplate.postForEntity(commandIssuedUrl, requestEntity, String.class);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            logMessage.setResult(e.getMessage());
            logMessage.setDeviceControlStatus(DeviceControlStatus.FAIL);
            deviceControlLogProducer.push(logMessage);

            interFaceLogMessage.setLogStatus(LogStatus.EXCEPTION);
            interFaceLogMessage.setReturnCode(IConst.SERVICE_ERROR);
            interFaceLogMessage.setReturnMsg("请求超时");
            sendLogProducer.sendLogMsg(interFaceLogMessage);
            return ServiceResult.error("控制失败");
        }

        // 解析请求结果
        String responseBody = responseEntity.getBody();
        JSONObject jsonResult = JSON.parseObject(responseBody);
        Integer status = jsonResult.getInteger("status");

        interFaceLogMessage.setReturnCode(jsonResult.getString("code") == null ? "" : jsonResult.getString("code"));
        interFaceLogMessage.setReturnMsg(jsonResult.getString("msg") == null ? "" : jsonResult.getString("msg"));
        interFaceLogMessage.setReturnData(jsonResult.getString("data") == null ? "" : jsonResult.getString("data"));

        if (status != 1) {
            interFaceLogMessage.setLogStatus(LogStatus.EXCEPTION);
            sendLogProducer.sendLogMsg(interFaceLogMessage);

            logMessage.setResult(responseBody);
            logMessage.setDeviceControlStatus(DeviceControlStatus.FAIL);
            deviceControlLogProducer.push(logMessage);

            return ServiceResult.error("设备控制失败");
        }

        logMessage.setResult(responseBody);
        logMessage.setDeviceControlStatus(DeviceControlStatus.SUCCESS);
        deviceControlLogProducer.push(logMessage);

        interFaceLogMessage.setLogStatus(LogStatus.NORMAL);
        sendLogProducer.sendLogMsg(interFaceLogMessage);
        return ServiceResult.ok();
    }

    @Override
    @Transactional
    public List<TbDataResp> findData(String id, int days) {
        // 获取查询时间戳
        Calendar calendar = Calendar.getInstance();
        long time = new Date().getTime();
        long zero = 0;
        if (days == 1) {
            zero = time / (1000 * 3600 * 24) * (1000 * 3600 * 24) - TimeZone.getDefault().getRawOffset();// 今天零点零分零秒的毫秒数
        } else {
            calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - days);
            long timeInMillis = calendar.getTimeInMillis();
            zero = timeInMillis / (1000 * 3600 * 24) * (1000 * 3600 * 24) - TimeZone.getDefault().getRawOffset();// 传入天数零点零分零秒的毫秒数
        }
        // url
        StringBuilder urlTemplate = new StringBuilder();
        urlTemplate.append(dataUrl).append("/?entityId=").append(id).append("&keys=humidity,temperature,luminance,isPeopleThere")
                .append("&useStrictDataTypes=false")
                .append("&startTs=").append(zero).append("&endTs=").append(time)
                .append("&agg=SUM").append("&entityType=DEVICE");
        // 发送Http请求
        HttpHeaders headers = new HttpHeaders();

        // 获取TB遥测数据
        HttpEntity requestGet = new HttpEntity<>(headers);
        ResponseEntity<String> response = restTemplate.exchange(urlTemplate.toString(), HttpMethod.GET, requestGet, String.class);
        String body = response.getBody();
        // 解析数据
        JSONObject jsonObject = JSON.parseObject(body);
        JSONObject jsonData = jsonObject.getJSONObject("data");
        JSONArray jsonIndex = jsonData.getJSONArray("isPeopleThere");
        List<TbDataResp> list = new ArrayList<>();
        if (jsonIndex != null) {
            list = JSON.parseArray(jsonIndex.toJSONString(), TbDataResp.class);
        }
        return list;
    }

    @Override
    @Transactional
    public String findTbId(String id) {
        // url
        StringBuilder urlTemplate = new StringBuilder();
        urlTemplate.append(getTbIdUrl).append("/?deviceId=").append(id);
        // 发送Http请求
        HttpHeaders headers = new HttpHeaders();

        // 获取TB设备ID
        HttpEntity<IotDevice> requestGet = new HttpEntity<>(headers);
        ResponseEntity<String> response = restTemplate.exchange(urlTemplate.toString(), HttpMethod.GET, requestGet, String.class);
        String body = response.getBody();
        // 解析数据
        JSONObject jsonObject = JSON.parseObject(body);
        String tbId = jsonObject.getString("data");
        return tbId;
    }

    @Override
    @Transactional
    public List<TbDataResp> findNowData(String tbId) {
        // 获取查询时间戳
        Calendar calendar = Calendar.getInstance();
        long now = new Date().getTime();
        calendar.add(Calendar.MINUTE, -minute);// 1分钟之前的时间并转为时间戳
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        List<TbDataResp> list = new ArrayList<>();
        try {
            Date beforeD = calendar.getTime();
            String tim = sdf.format(beforeD);
            Date date = sdf.parse(tim);
            long ts = date.getTime();
            // url
            StringBuilder urlTemplate = new StringBuilder();
            urlTemplate.append(dataUrl).append("/?entityId=").append(tbId).append("&keys=humidity,temperature,luminance,noise,pm2_5,pm10,isPeopleThere")
                    .append("&useStrictDataTypes=false")
                    .append("&startTs=").append(ts).append("&endTs=").append(now)
                    .append("&agg=SUM").append("&entityType=DEVICE");
            // 发送Http请求
            HttpHeaders headers = new HttpHeaders();

            // 获取TB遥测数据
            HttpEntity requestGet = new HttpEntity<>(headers);
            ResponseEntity<String> response = restTemplate.exchange(urlTemplate.toString(), HttpMethod.GET, requestGet, String.class);
            String body = response.getBody();
            // 判断body是否为空
            if (StringUtils.isEmpty(body)){
                log.error("传感器【{}】请求失败！",tbId);
                return list;
            }
            // 解析数据
            JSONObject jsonObject = JSON.parseObject(body);
            JSONObject jsonData = jsonObject.getJSONObject("data");
            if (Objects.isNull(jsonData)){
                log.error("传感器【{}】获取数据为空！",tbId);
                return list;
            }
            JSONArray jsonLuminance = jsonData.getJSONArray("luminance");
            JSONArray jsonTemperature = jsonData.getJSONArray("temperature");
            JSONArray jsonHumidity = jsonData.getJSONArray("humidity");
            JSONArray jsonNoise = jsonData.getJSONArray("noise");
            JSONArray jsonPm2_5 = jsonData.getJSONArray("pm2_5");
            JSONArray jsonPm10 = jsonData.getJSONArray("pm10");
            JSONArray jsonIsPeopleThere = jsonData.getJSONArray("isPeopleThere");
            TbDataResp tbDataResp = new TbDataResp();
            // 获取光感
            if (jsonLuminance != null) {
                List<TbDataResp> listLuminance = JSON.parseArray(jsonLuminance.toJSONString(), TbDataResp.class);
                tbDataResp.setLuminance(listLuminance.get(0).getValue());
            } else {
                tbDataResp.setLuminance("");
            }
            // 获取温度
            if (jsonTemperature != null) {
                List<TbDataResp> listTemperature = JSON.parseArray(jsonTemperature.toJSONString(), TbDataResp.class);
                String value = listTemperature.get(0).getValue();
                if (value.length() > 4) {
                    tbDataResp.setTemperature(value.substring(0, 5));
                } else {
                    tbDataResp.setTemperature(value);
                }
            } else {
                tbDataResp.setTemperature("");
            }
            // 获取湿度
            if (jsonHumidity != null) {
                List<TbDataResp> listHumidity = JSON.parseArray(jsonHumidity.toJSONString(), TbDataResp.class);
                String value = listHumidity.get(0).getValue();
                if (value.length() > 4) {
                    tbDataResp.setHumidity(value.substring(0, 5));
                } else {
                    tbDataResp.setHumidity(value);
                }
            } else {
                tbDataResp.setHumidity("");
            }
            // 获取噪音
            if (jsonNoise != null) {
                List<TbDataResp> listNoise = JSON.parseArray(jsonNoise.toJSONString(), TbDataResp.class);
                String value = listNoise.get(0).getValue();
                if (value.length() > 4) {
                    tbDataResp.setNoise(value.substring(0, 5));
                } else {
                    tbDataResp.setNoise(value);
                }
            } else {
                tbDataResp.setNoise("");
            }
            // 获取pm2.5
            if (jsonPm2_5 != null) {
                List<TbDataResp> listPm2_5 = JSON.parseArray(jsonPm2_5.toJSONString(), TbDataResp.class);
                tbDataResp.setPm2_5(listPm2_5.get(0).getValue());
            } else {
                tbDataResp.setPm2_5("");
            }
            // 获取pm10
            if (jsonPm10 != null) {
                List<TbDataResp> listPm10 = JSON.parseArray(jsonPm10.toJSONString(), TbDataResp.class);
                tbDataResp.setPm10(listPm10.get(0).getValue());
            } else {
                tbDataResp.setPm10("");
            }
            // 获取人体传感器
            if (jsonIsPeopleThere != null) {
                List<TbDataResp> listIsPeopleThere = JSON.parseArray(jsonIsPeopleThere.toJSONString(), TbDataResp.class);
                tbDataResp.setIsPeopleThere(listIsPeopleThere.get(0).getValue());
            } else {
                tbDataResp.setIsPeopleThere("");
            }
            list.add(tbDataResp);
        } catch (ParseException e) {
            log.error(e.getMessage());
        }
        return list;
    }

    @Override
    @Transactional
    public ServiceResult updateSwitch(DoorMagneticHWReq hwReq, String hrId, String name) {

        // 保存设备控制日志至iotc
        DeviceControlLogMessage logMessage = new DeviceControlLogMessage();
        logMessage.setHrId(hrId);
        logMessage.setName(name);
        if (StringUtils.isNotEmpty(hwReq.getMode())) {
            logMessage.setMode(hwReq.getMode());
        }
        logMessage.setHwDeviceId(hwReq.getDevice_id());
        Optional<IotDevice> deviceOptional = deviceDao.findByHwDeviceId(hwReq.getDevice_id());
        if (deviceOptional.isEmpty()) {
            return ServiceResult.error("控制失败,设备不存在");
        }
        IotDevice iotDevice = deviceOptional.get();
        logMessage.setHwDeviceName(iotDevice.getHwDeviceName());
        logMessage.setAddress(SpaceService.getFullSpaceName(iotDevice.getSpace(), iotDevice.getSpace().getName()));
        logMessage.setSource("IBMS");
        logMessage.setCommand(JSON.toJSONString(hwReq.getParas()));
        logMessage.setControlTime(LocalDateTime.now().withNano(0));

        SwitchReq switchReq = new SwitchReq();
        List<Services> list = new ArrayList<>();
        Services services = new Services();
        services.setProperties(hwReq.getParas());
        services.setServiceId(hwReq.getService_id());
        list.add(services);
        switchReq.setServices(list);
        // json参数
        String requestParam = JSON.toJSONString(switchReq);
        log.debug("json参数 ： " + requestParam);
        // url
        StringBuilder urlTemplate = new StringBuilder();
        urlTemplate.append(propertiesUrl).append(hwReq.getDevice_id());
        // 发送Http请求 下发控制命令
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity<>(requestParam, headers);
        ResponseEntity<String> responseEntity = null;

        // 保存接口日志至iotc
        InterFaceLogMessage interFaceLogMessage = new InterFaceLogMessage();
        interFaceLogMessage.setType("put");
        interFaceLogMessage.setArg(requestParam);
        interFaceLogMessage.setUrl(urlTemplate.toString());
        interFaceLogMessage.setSource("IBMS");
        interFaceLogMessage.setMsgTime(DateUtils.formatLocalDateTime(LocalDateTime.now(), DateUtils.PATTERN_DATETIME));
        try {
            responseEntity = restTemplate.exchange(urlTemplate.toString(), HttpMethod.PUT, requestEntity, String.class);
        } catch (Exception e) {
            log.error(e.getMessage());

            interFaceLogMessage.setLogStatus(LogStatus.EXCEPTION);
            interFaceLogMessage.setReturnCode(IConst.SERVICE_ERROR);
            interFaceLogMessage.setReturnMsg("请求超时");
            sendLogProducer.sendLogMsg(interFaceLogMessage);

            logMessage.setResult(e.getMessage());
            logMessage.setDeviceControlStatus(DeviceControlStatus.FAIL);
            deviceControlLogProducer.push(logMessage);

            return ServiceResult.error("控制失败");
        }

        // 解析请求结果
        String responseBody = responseEntity.getBody();
        JSONObject jsonResult = JSON.parseObject(responseBody);
        String data = jsonResult.getString("data");
        JSONObject jsonData = JSON.parseObject(data);

        interFaceLogMessage.setReturnCode(jsonResult.getString("code") == null ? "" : jsonResult.getString("code"));
        interFaceLogMessage.setReturnMsg(jsonResult.getString("msg") == null ? "" : jsonResult.getString("msg"));
        interFaceLogMessage.setReturnData(jsonResult.getString("data") == null ? "" : jsonResult.getString("data"));

        // 根据返回的数据中是否包含error_code判断请求成功与否

        if (jsonData.containsKey("error_code")) {
            interFaceLogMessage.setLogStatus(LogStatus.EXCEPTION);
            sendLogProducer.sendLogMsg(interFaceLogMessage);

            logMessage.setResult(responseBody);
            logMessage.setDeviceControlStatus(DeviceControlStatus.FAIL);
            deviceControlLogProducer.push(logMessage);
            return ServiceResult.error(responseBody);
        }

        interFaceLogMessage.setLogStatus(LogStatus.NORMAL);
        sendLogProducer.sendLogMsg(interFaceLogMessage);

        logMessage.setResult(responseBody);
        logMessage.setDeviceControlStatus(DeviceControlStatus.SUCCESS);
        deviceControlLogProducer.push(logMessage);

        // 保存思路开关数据至redis
        saveDeviceParasToRedis(iotDevice, hwReq.getParas());

        return ServiceResult.ok();

    }

    private void saveDeviceParasToRedis(IotDevice iotDevice, DoorMagneticHWParasReq paras) {
        Map<String, Object> devicePropertiesMap = new ConcurrentHashMap<>();
        if (iotDevice.getDeviceType().equals(DeviceType.FOUR_WAY_SWITCH)) {
            if(StringUtils.isNotEmpty(paras.getLight1())){
                devicePropertiesMap.put("light1", paras.getLight1());
            }
            if(StringUtils.isNotEmpty(paras.getLight2())){
                devicePropertiesMap.put("light2", paras.getLight2());
            }
            if(StringUtils.isNotEmpty(paras.getLight3())){
                devicePropertiesMap.put("light3", paras.getLight3());
            }
            if(StringUtils.isNotEmpty(paras.getLight4())){
                devicePropertiesMap.put("light4", paras.getLight4());
            }

        } else if (iotDevice.getDeviceType().equals(DeviceType.AIR_CONDITIONER)) {
            devicePropertiesMap.put("temperature", paras.getSetTemp().toString());
            switch (paras.getSetPower()) {
                case "on":
                case "1":
                    devicePropertiesMap.put("setPower", "on");
                    break;
                case "off":
                case "0":
                    devicePropertiesMap.put("setPower", "off");
                    break;

                default:
                    break;
            }

            switch (paras.getSetMode()) {
                case "fan":
                case "1":
                    devicePropertiesMap.put("mode", "fan");
                    break;
                case "0":
                case "4":
                case "auto":
                    devicePropertiesMap.put("mode", "auto");
                    break;
                case "2":
                case "6":
                case "cool":
                    devicePropertiesMap.put("mode", "cool");
                    break;
                case "3":
                case "heat":
                    devicePropertiesMap.put("mode", "heat");
                    break;
                case "5":
                case "dry":
                    devicePropertiesMap.put("mode", "dry");
                    break;
                default:
                    break;
            }

            switch (paras.getSetFanSpeed()) {
                case "auto":
                case "0":
                    devicePropertiesMap.put("windSpeed", "auto");
                    break;
                case "min":
                case "9":
                    devicePropertiesMap.put("windSpeed", "min");
                    break;
                case "med":
                case "10":
                    devicePropertiesMap.put("windSpeed", "med");
                    break;
                case "max":
                case "11":
                    devicePropertiesMap.put("windSpeed", "max");
                    break;
                default:
                    break;
            }
        }
        String deviceKey = redisUtil.createDeviceRealKey(IConst.DEVICE_MODULE, String.valueOf(iotDevice.getDeviceType()), iotDevice.getHwDeviceId(), RedisUtil.Separator.COLON);
        redisUtil.hmset(deviceKey, devicePropertiesMap, true);
    }

    @Async
    @Override
    public void saveDeviceControlLog(DoorMagneticHWReq hwReq, String hrId, String name) {
        try {
            Optional<IotDevice> deviceOp = deviceDao.findById(hwReq.getDeviceId());
            if (deviceOp.isPresent()) {
                DeviceControlLog deviceControlLog = new DeviceControlLog();
                deviceControlLog.setCmd(JSON.toJSONString(hwReq.getParas()));
                deviceControlLog.setDevice(deviceOp.get());
                deviceControlLog.setUserHrId(hrId);
                deviceControlLog.setUserName(name);
                deviceControlLogDao.save(deviceControlLog);
                log.info("保存设备操作日志:" + JSON.toJSONString(deviceControlLog));
            } else {
                log.info("保存设备操作日志失败,设备不存在:" + hwReq.getDevice_id());
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 获取设备详情
     *
     * @param hwDeviceId 华为云设备ID
     * @return 设备详情, 找不到设备或获取失败时，返回Optional.empty();
     * @author Yang.Lee
     * @date 2022/7/13 16:48
     **/
    @Override
    public Optional<DeviceDetail> getDeviceDetail(String hwDeviceId) {


        String url = deviceDetailUrl + "?deviceId=" + hwDeviceId;

        // 发送请求获取设备信息
        JsonResult<DeviceDetail> deviceDetailJsonResult = restTemplateUtil.get(url, null, new ParameterizedTypeReference<JsonResult<DeviceDetail>>() {
        });

        if (deviceDetailJsonResult.getStatus() == 0) {

            log.error("获取华为云设备详情异常，返回信息：{}", deviceDetailJsonResult);

            return Optional.empty();
        }

        return Optional.ofNullable(deviceDetailJsonResult.getData());
    }

    @Override
    public ServiceResult findProductProperties(IotDevice device) {

        Optional<HwProductProperties> hwProductPropertiesOp = hwProductPropertiesDao.findByHwProductIdAndDeviceType(device.getHwProductId(), device.getDeviceType());
        if (hwProductPropertiesOp.isEmpty()) {
            log.info("配置失败产品：【{}】", device.getHwProductId());
            return ServiceResult.error("控制失败,设备产品未配置");
        }
        HwProductProperties hwProductProperties = hwProductPropertiesOp.get();
        DoorMagneticHWReq req = new DoorMagneticHWReq();
        req.setService_id(hwProductProperties.getServiceId());
        req.setCommand_name(hwProductProperties.getCommandName());

        return ServiceResult.ok(req);
    }
    @Override
    public JsonResult<HwProductProperties> findProductPropertiesByHwProductId(String hwProductId,DeviceType deviceType) {
        Optional<HwProductProperties> hwProductPropertiesOp = hwProductPropertiesDao.findByHwProductIdAndDeviceType(hwProductId, deviceType);
        if (hwProductPropertiesOp.isEmpty()) {
            log.info("无配置属性产品：【{}】", hwProductId);
            return JsonResult.error("控制失败,设备产品未配置",null,"-1");
        }
        return JsonResult.ok("success",hwProductPropertiesOp.get());
    }

    /**
     * 组装命令模板
     * @param hwProductId
     * @param deviceType
     * @return
     */
    @Override
    public JsonResult<CommandTemplate> packageCommandTemplate(String hwProductId, DeviceType deviceType) {

        Optional<HwProductProperties> hwProductPropertiesOp = hwProductPropertiesDao.findByHwProductIdAndDeviceType(hwProductId, deviceType);
        if (hwProductPropertiesOp.isEmpty()) {
            log.info("配置失败产品：【{}】", hwProductId);
            return JsonResult.error("控制失败,设备产品未配置",null,ErrConst.E06);
        }
        HwProductProperties hwProductProperties = hwProductPropertiesOp.get();
        CommandTemplate commandReq = new CommandTemplate();
        commandReq.setServiceId(hwProductProperties.getServiceId());
        commandReq.setCommandName(hwProductProperties.getCommandName());

        return JsonResult.ok(commandReq);
    }

    /**
     * 组装属性模板
     * @param hwProductId
     * @param deviceType
     * @param action
     * @return
     */
    @Override
    public JsonResult<PropertiesTemplate> packagePropertiesTemplate(String hwProductId, DeviceType deviceType,String action) {

        Optional<HwProductProperties> hwProductPropertiesOp = hwProductPropertiesDao.findByHwProductIdAndDeviceType(hwProductId, deviceType);
        if (hwProductPropertiesOp.isEmpty()) {
            log.info("配置失败产品：【{}】", hwProductId);
            return JsonResult.error("控制失败,设备产品未配置",null,ErrConst.E06);
        }
        HwProductProperties hwProductProperties = hwProductPropertiesOp.get();
        PropertiesTemplate propertiesTemplate = new PropertiesTemplate();
        propertiesTemplate.setServiceId(hwProductProperties.getServiceId());
        propertiesTemplate.setLight1(action);
        propertiesTemplate.setLight2(action);
        propertiesTemplate.setLight3(action);
        propertiesTemplate.setLight4(action);
        return JsonResult.ok(propertiesTemplate);
    }

    @Override
    @Transactional
    public SwitchResp findProperties(String id, String serviceId) {
//        log.info("查询设备属性:" + id);
        Optional<IotDevice> hwDeviceId = deviceDao.findByHwDeviceId(id);
        IotDevice device = new IotDevice();
        if (hwDeviceId.isPresent()) {
            device = hwDeviceId.get();
        }
        SwitchResp switchResp = new SwitchResp();
        // url
        StringBuilder urlTemplate = new StringBuilder();
        urlTemplate.append(switchPropertiesUrl).append("?deviceId=").append(id).append("&serviceId=").append(serviceId);
        // 发送Http请求
        HttpHeaders headers = new HttpHeaders();

        // 获取华为云设备属性
        HttpEntity<IotDevice> requestGet = new HttpEntity<>(headers);
        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(urlTemplate.toString(), HttpMethod.GET, requestGet, String.class);
        } catch (Exception e) {
            switchResp.setRead("error");
            log.error(e.getMessage(), e);
            return switchResp;
        }
        String body = response.getBody();
        // 解析数据
        JSONObject jsonObject = JSON.parseObject(body);
        if ("0".equals(jsonObject.getString("status"))) {
            switchResp.setRead("error");
            return switchResp;
        }
        JSONObject jsonData = jsonObject.getJSONObject("data");
        if(StringUtils.isNotEmpty(jsonData.getString("error_code")) || StringUtils.isNotEmpty(jsonData.getString("error_msg"))){
            switchResp.setRead("error");
            return switchResp;
        }
        JSONObject jsonResponse = jsonData.getJSONObject("response");
        JSONArray jsonIndex = jsonResponse.getJSONArray("devices");
        if (jsonIndex != null) {
            List<Devices> list = JSON.parseArray(jsonIndex.toJSONString(), Devices.class);
            // 返回设备属性
            BeanUtils.copyProperties(list.get(0).getServices().get(0).getProperties(), switchResp);
            return switchResp;
        } else {
            if (DeviceType.SENSOR_BODY_SENSING.equals(device.getDeviceType())) {
                JSONArray jsonIndex1 = jsonResponse.getJSONArray("services");
                List<cn.com.cowain.ibms.service.bean.hw.Services> list1 = JSON.parseArray(jsonIndex1.toJSONString(), cn.com.cowain.ibms.service.bean.hw.Services.class);
                // 返回设备属性
                switchResp.setIsPeopleThere(list1.get(0).getProperties().get(0).getIsPeopleThere());
                return switchResp;
            }
            JSONArray jsonIndex1 = jsonResponse.getJSONArray("services");
            List<Services> list1 = JSON.parseArray(jsonIndex1.toJSONString(), Services.class);
            // 返回设备属性
            BeanUtils.copyProperties(list1.get(0).getProperties(), switchResp);
            return switchResp;
        }

    }

    @Override
    @Transactional
    public ServiceResult findAirProperties(String id, String serviceId) {
        log.info("查询空调设备属性：" + id);
        Optional<IotDevice> hwDeviceId = deviceDao.findByHwDeviceId(id);
        IotDevice device = new IotDevice();
        if (hwDeviceId.isPresent()) {
            device = hwDeviceId.get();
        }
        return findByDeviceAndServiceId(device, serviceId);

    }

    @Override
    @Transactional
    public ServiceResult findByDeviceAndServiceId(IotDevice device, String serviceId) {
        AirConditionerResp airConditionerResp = new AirConditionerResp();
        //url
        StringBuilder urlTemplate = new StringBuilder();
        urlTemplate.append(switchPropertiesUrl).append("?deviceId=").append(device.getHwDeviceId()).append("&serviceId=").append(serviceId);

        //发送Http请求
        HttpHeaders headers = new HttpHeaders();

        //获取华为云设备属性
        HttpEntity<IotDevice> requestGet = new HttpEntity<>(headers);
        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(urlTemplate.toString(), HttpMethod.GET, requestGet, String.class);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ServiceResult.error("向iot获取设备属性失败{}", e.getMessage());
        }

        String body = response.getBody();
        //解析数据
        JSONObject jsonObject = JSON.parseObject(body);
        if ("0".equals(jsonObject.getString("status"))) {
            return ServiceResult.error("向iot获取设备属性失败");
        }

        JSONObject jsonData = jsonObject.getJSONObject("data");
        log.info("向iot获取设备属性:{}", jsonData);
        JSONObject jsonResponse = jsonData.getJSONObject("response");
        JSONArray jsonIndex = jsonResponse.getJSONArray("services");

        if (jsonIndex != null) {

            IotDevice finalDevice = device;

            List<AirServices> list = JSON.parseArray(jsonIndex.toJSONString(), AirServices.class);

            if (list.size() > 0) {
                AirServices itt = list.get(0);
                //返回设备属性
                itt.getProperties().forEach(it -> {
                    //设备名称
                    airConditionerResp.setDeviceName(finalDevice.getDeviceName());
                    airConditionerResp.setHwStatus(jsonObject.getString("status"));
                    //设备设备开关
                    if (it.getReadPowerAndMode() != null) {

                        if ("0".equals(it.getReadPowerAndMode())) {
                            airConditionerResp.setSetPower("off");
                        } else {
                            airConditionerResp.setSetPower("on");
                        }
                    }
                    //设备温度
                    if (it.getReadTemp() != null) {
                        int integer = Integer.parseInt(it.getReadTemp());
                        airConditionerResp.setTemperature(integer);
                    }
                    //风向
                    airConditionerResp.setWindDirection("");
                    //
                    airConditionerResp.setSetFanAuto("");

                    //风速
                    if (it.getReadFanSpeed() != null) {
                        switch (it.getReadFanSpeed()) {
                            case "1":
                                airConditionerResp.setWindSpeed("min");
                                break;
                            case "2":
                                airConditionerResp.setWindSpeed("min");
                                break;
                            case "3":
                                airConditionerResp.setWindSpeed("med");
                                break;
                            case "4":
                                airConditionerResp.setWindSpeed("med");
                                break;
                            case "5":
                                airConditionerResp.setWindSpeed("med");
                                break;
                            case "6":
                                airConditionerResp.setWindSpeed("max");
                                break;
                            case "7":
                                airConditionerResp.setWindSpeed("max");
                                break;
                            case "8":
                                airConditionerResp.setWindSpeed("min");
                                break;
                            case "9":
                                airConditionerResp.setWindSpeed("min");
                                break;
                            case "10":
                                airConditionerResp.setWindSpeed("med");
                                break;
                            case "11":
                                airConditionerResp.setWindSpeed("max");
                                break;
                            default:
                                airConditionerResp.setWindSpeed("auto");
                                break;
                        }
                    }
                    //模式
                    if (it.getReadPowerAndMode() != null) {
                        if (!"0".equals(it.getReadPowerAndMode())) {
                            if ("1".equals(it.getReadPowerAndMode())) {
                                //送风
                                airConditionerResp.setMode("fan");
                            }
                            if ("2".equals(it.getReadPowerAndMode())) {
                                //制冷
                                airConditionerResp.setMode("cool");
                            }
                            if ("3".equals(it.getReadPowerAndMode())) {
                                //制热
                                airConditionerResp.setMode("heat");
                            }
                            if ("4".equals(it.getReadPowerAndMode())) {
                                //自动
                                airConditionerResp.setMode("auto");
                            }
                            if ("5".equals(it.getReadPowerAndMode())) {
                                //除湿
                                airConditionerResp.setMode("dry");
                            }
                            if ("6".equals(it.getReadPowerAndMode())) {
                                //强制制冷
                                airConditionerResp.setMode("cool");
                            }
                        } else {
                            airConditionerResp.setMode("auto");
                        }
                    }
                    airConditionerResp.setSetSwing("");
                });
            }
        }
        return ServiceResult.ok(airConditionerResp);
    }

    @Override
    public void saveCircuitBreaker(IotDevice device, SwitchResp data) {
        // 赋值
        CircuitBreaker circuitBreaker = new CircuitBreaker();
        BeanUtils.copyProperties(data, circuitBreaker);
        circuitBreaker.setIotDevice(device);
        // 获取用电日期
        LocalDate now = LocalDate.now();
        circuitBreaker.setDateTime(now);
        List<CircuitBreaker> all = circuitBreakerDao.findAll(Sort.by(Sort.Direction.DESC, "createdTime"));
        if (all.isEmpty()) {
            circuitBreaker.setDayElectricity(0);
        } else {
            int electricity = (int) Math.ceil(data.getElectricity() - all.get(0).getElectricity());
            circuitBreaker.setDayElectricity(electricity);
        }
        circuitBreakerDao.save(circuitBreaker);
    }

    @Override
    public SwitchResp findPropertiesTwo(String deviceId, String serviceId) {
        // url
        // 发送Http请求
        HttpHeaders headers = new HttpHeaders();

        // 获取华为云设备属性
        HttpEntity<IotDevice> requestGet = new HttpEntity<>(headers);
        ResponseEntity<String> response = restTemplate.exchange(switchPropertiesUrl + "?deviceId=" + deviceId + "&serviceId=" + serviceId
                // 发送Http请求
                // 获取华为云设备属性
                , HttpMethod.GET, requestGet, String.class);
        String body = response.getBody();
        // 解析数据
        JSONObject jsonObject = JSON.parseObject(body);
        JSONObject jsonData = jsonObject.getJSONObject("data");
        JSONObject jsonResponse = jsonData.getJSONObject("response");
        JSONArray jsonIndex = jsonResponse.getJSONArray("services");
        List<Services> list = JSON.parseArray(jsonIndex.toJSONString(), Services.class);
        SwitchResp switchResp = new SwitchResp();
        // 返回设备属性
        BeanUtils.copyProperties(list.get(0).getProperties(), switchResp);
        return switchResp;
    }

    @Override
    @Transactional
    public void sensorBodySensing(List<IotDevice> deviceList) {
        if (!deviceList.isEmpty()) {
            for (IotDevice device : deviceList) {
                List<IotDevice> isPeoplethereList = deviceDao.findBySpaceIdAndDeviceType(device.getSpace().getId(), DeviceType.SENSOR_BODY_SENSING);
                if (isPeoplethere(isPeoplethereList)) {

                /*}
                String tbId = this.findTbId(device.getHwDeviceId());
                if (!tbId.isEmpty() && DeviceStatus.ONLINE.equals(device.getHwStatus())) {
                    List<TbDataResp> data = this.findData(tbId, 1);
                    // 获取毫米波传感器最后一次状态时间与当前时间差
                    Duration duration = Duration.between(data.get(0).getTs(), LocalDateTime.now());

                    if(data.get(0).getValue().equals("false") && duration.toMinutes() >= 10 ){*/
                    // 10分钟该空间下没有人
                    List<IotDevice> list = deviceDao.findBySpaceIdAndSpaceControl(device.getSpace().getId(), 1);
                    for (IotDevice dev : list) {
                        // 获取设备所属产品的服务ID和命令名称
                        ServiceResult serviceResult = findProductProperties(device);
                        if (!serviceResult.isSuccess()) {
                            continue;
                        }
                        DoorMagneticHWReq hwReq = (DoorMagneticHWReq) serviceResult.getObject();
                        // 根据设备id判断设备开关
                        // 空调
                        if (DeviceType.AIR_CONDITIONER.equals(dev.getDeviceType())) {
                            Optional<AirConditioner> airOp = airConditionerDao.findByDevice(dev);
                            // 空调如果是打开则关闭
                            if (airOp.isPresent() && "on".equals(airOp.get().getSetPower())) {
                                log.info("毫米波控制空调 当前设备状态" + JSON.toJSONString(airOp.get()));
                                control(dev.getId());
                            }
                        }
                        // 窗帘
                        if (DeviceType.CURTAINS.equals(dev.getDeviceType())) {
                            Optional<Curtain> curtain = curtainDao.findByDevice(dev);
                            // 空调如果是打开则关闭
                            if (curtain.isPresent() && !"close".equals(curtain.get().getAction())) {
                                log.info("毫米波控制窗帘 当前设备状态" + JSON.toJSONString(curtain.get()));
                                control(dev.getId());
                            }
                        }
                        // 断路器 四路开关
                        if (DeviceType.FOUR_WAY_SWITCH.equals(dev.getDeviceType()) || DeviceType.CIRCUIT_BREAKER.equals(dev.getDeviceType())) {
                            SwitchResp properties = this.findProperties(dev.getHwDeviceId(), hwReq.getService_id());
                            log.info("毫米波控制断路器/四路开关 :" + dev.getHwDeviceId() + "当前设备状态:" + JSON.toJSONString(properties));
                            if ("on".equals(properties.getIsSwitch())
                                    || "on".equals(properties.getLight1())
                                    || "on".equals(properties.getLight2())
                                    || "on".equals(properties.getLight3())
                                    || "on".equals(properties.getLight4())) {
                                control(dev.getId());
                            }
                        }
                    }
                }
            }
        }
    }

    private boolean isPeoplethere(List<IotDevice> isPeoplethereList) {
        for (IotDevice device : isPeoplethereList) {
            String tbId = this.findTbId(device.getHwDeviceId());
            if (!tbId.isEmpty() && DeviceStatus.ONLINE.equals(device.getHwStatus())) {
                List<TbDataResp> data = this.findData(tbId, 1);

                Optional<TbDataResp> tbDataRespOptional = data.stream().findFirst();

                if (tbDataRespOptional.isPresent()) {
                    LocalDateTime now = LocalDateTime.now();
                    boolean isTBTrue = data.get(0).getValue().equals("true");
                    log.info("从tb获取到毫米波状态{}", isTBTrue);
                    long minutes = Duration.between(data.get(0).getTs(), now).toMinutes();
                    log.info("毫米波状态时间{}", minutes);
                    if (minutes < 10 && (now.getHour() < 6 || now.getHour() > 17)) {
                        return false;
                    }
                    if (now.getHour() > 5 && now.getHour() < 18 && minutes < 60) {
                        return false;
                    }
                    if (isTBTrue) {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    @Transactional
    public ServiceResult properties(String id, String hrId, DoorMagneticHWParasReq paras) {
        // 判断控制人是否为参会人员
        Optional<ReservationRecordItem> reservationRecordItemOp = itemDao.findByReservationRecordIdAndOwnerEmpNo(paras.getRecordId(), hrId);
        if (!reservationRecordItemOp.isPresent()) {
            return ServiceResult.error("你不是参会人员,没有权限操作此设备");
        }
        // 判断该设备在华为云是否存在
        Optional<IotDevice> dev = deviceDao.findByHwDeviceId(id);
        if (dev.isPresent()) {
            IotDevice device = dev.get();
            if (!device.getHwStatus().equals(DeviceStatus.ONLINE)) {
                return ServiceResult.error(ServiceMessage.ERR_STATUS);
            }
            // 获取设备所属产品的服务ID和命令名称
            ServiceResult serviceResult = findProductProperties(device);
            if (!serviceResult.isSuccess()) {
                return ServiceResult.error(serviceResult.getObject());
            }
            DoorMagneticHWReq hwReq = (DoorMagneticHWReq) serviceResult.getObject();
            String name = "";
            try {
                // 保存人员权限
                name = sysUserServiceUC.getUserByEmpNo(hrId).getNameZh();
            } catch (Exception e) {
                log.error(e.getMessage());
                log.error("获取人员信息失败");
            }
            // 下发控制设备
            hwReq.setDevice_id(id);
            hwReq.setParas(paras);
            hwReq.setDeviceId(device.getId());
            ServiceResult result = this.updateSwitch(hwReq, hrId, name);
            if (!result.isSuccess()) {
                return ServiceResult.error(result.getObject());
            }
        } else {
            return ServiceResult.error(ServiceMessage.ERR_DEVICE_ID);
        }
        return ServiceResult.ok();
    }

    @Override
    @Transactional
    public ServiceResult commandRecord(String id, String hrId, DoorMagneticHWParasReq paras) {
        // 判断控制人是否为参会人员
        Optional<ReservationRecordItem> reservationRecordItemOp = itemDao.findByReservationRecordIdAndOwnerEmpNo(paras.getRecordId(), hrId);
        if (!reservationRecordItemOp.isPresent()) {
            return ServiceResult.error("你不是参会人员,没有权限操作此设备");
        }
        // 判断该设备在华为云是否存在
        Optional<IotDevice> dev = deviceDao.findByHwDeviceId(id);
        if (dev.isPresent()) {
            IotDevice device = dev.get();
            if (!device.getHwStatus().equals(DeviceStatus.ONLINE)) {
                return ServiceResult.error(ServiceMessage.ERR_STATUS);
            }
            // 获取设备所属产品的服务ID和命令名称
            ServiceResult serviceResult = findProductProperties(device);
            if (!serviceResult.isSuccess()) {
                return ServiceResult.error(serviceResult.getObject());
            }
            DoorMagneticHWReq hwReq = (DoorMagneticHWReq) serviceResult.getObject();
            String name = "";
            try {
                // 保存人员权限
                name = sysUserServiceUC.getUserByEmpNo(hrId).getNameZh();
            } catch (Exception e) {
                log.error(e.getMessage());
                log.error("获取人员信息失败");
            }
            // 下发设备控制命令
            hwReq.setDeviceId(device.getId());
            hwReq.setDevice_id(id);
            hwReq.setParas(paras);
            ServiceResult result = this.command(hwReq, hrId, name);
            if (!result.isSuccess()) {
                return ServiceResult.error(result.getObject());
            }
        } else {
            return ServiceResult.error(ServiceMessage.ERR_DEVICE_ID);
        }
        IotDevice device = dev.get();
        // 将下发命令返回
        AirConditionerReq airConditionerReq = new AirConditionerReq();
        airConditionerReq.setDeviceName(device.getHwDeviceName());
        airConditionerReq.setMode(paras.getSetMode());
        airConditionerReq.setSetFanAuto(paras.getSetFanAuto());
        airConditionerReq.setSetPower(paras.getSetPower());
        airConditionerReq.setTemperature(paras.getSetTemp() != null ? paras.getSetTemp() : 0);
        airConditionerReq.setWindDirection(paras.getSetFanDirection());
        airConditionerReq.setWindSpeed(paras.getSetFanSpeed());
        return ServiceResult.ok(airConditionerReq);
    }

    @Override
    @Transactional
    public ServiceResult workingMode(String id) {

        String hrId = JwtUtil.getHrId(TokenUtils.getToken());
        Optional<DeviceWorkingMode> byId = workingModeDao.findById(id);
        if (byId.isEmpty()) {
            return ServiceResult.error("控制失败,模式不存在");
        }
        List<DeviceModeStatus> modeStatusList = modeStatusDao.findByModeId(id);
        if (modeStatusList.isEmpty()) {
            return ServiceResult.error("该模式未绑定设备,不可控");
        }
        StringBuilder msg = new StringBuilder();

        String name = "";
        try {
            // 保存人员权限
            name = sysUserServiceUC.getUserByEmpNo(hrId).getNameZh();
        } catch (Exception e) {
            log.error(e.getMessage());
            log.error("获取人员信息失败");
        }

        // 循环控制设备
        for (DeviceModeStatus deviceStatus : modeStatusList) {
            IotDevice device = deviceStatus.getDevice();


            List<SpaceAdmin> spaceAdminList = spaceAdminDao.findBySpaceId(device.getSpace().getId());

            List<String> hrIdList = spaceAdminList.stream()
                    .map(SpaceAdmin::getAdminEmpNo)
                    .filter(obj -> obj.equals(hrId))
                    .collect(Collectors.toList());

            // 判断是否是空间管理员，管理员放行，非管理员判断权限
            if (hrIdList.isEmpty()) {
                // 判断人员是否有权限控制
                Optional<SpaceDeviceControl> deviceControlOp = spaceDeviceControlDao.findByUserEmpNoAndDeviceId(hrId, device.getId());
                if (deviceControlOp.isEmpty()) {
                    return ServiceResult.error("该设备你无权控制");
                }
            }

            // 获取设备所属产品的服务ID和命令名称
            ServiceResult serviceResult = findProductProperties(device);
            if (!serviceResult.isSuccess()) {
                msg.append(device.getHwDeviceName()).append("设备离线;");
                continue;
            }
            DoorMagneticHWReq hwReq = (DoorMagneticHWReq) serviceResult.getObject();

            // 控制四路开关
            if (DeviceType.FOUR_WAY_SWITCH.equals(device.getDeviceType()) && DeviceStatus.ONLINE.equals(device.getHwStatus())) {
                DoorMagneticHWParasReq paras = new DoorMagneticHWParasReq();
                paras.setLight1(deviceStatus.getStatus());
                paras.setLight2(deviceStatus.getStatus());
                paras.setLight3(deviceStatus.getStatus());
                paras.setLight4(deviceStatus.getStatus());
                // 下发控制设备
                hwReq.setDeviceId(device.getId());
                hwReq.setDevice_id(device.getHwDeviceId());
                hwReq.setParas(paras);
                ServiceResult result = this.updateSwitch(hwReq, hrId, name);
                if (!result.isSuccess()) {
                    msg.append(device.getHwDeviceName()).append(result.getObject()).append(";");
                    continue;
                }
            }

            // 控制空调
            if (DeviceType.AIR_CONDITIONER.equals(device.getDeviceType()) && DeviceStatus.ONLINE.equals(device.getHwStatus())) {
                /*DoorMagneticHWReq hwReq = this.findProduct(device.getHwProductId());
                if(hwReq.getRead().equals("error")){
                    msg.append(device.getHwDeviceName()).append("设备离线;");
                }*/
                DoorMagneticHWParasReq paras = new DoorMagneticHWParasReq();
                // 下发控制设备
                paras.setSetPower(deviceStatus.getStatus());
                paras.setSetTemp(temp);
                paras.setSetMode("cool");
                paras.setSetFanSpeed("auto");
                paras.setSetSwing("aroundSwing");
                hwReq.setDeviceId(device.getId());
                hwReq.setDevice_id(device.getHwDeviceId());
                hwReq.setParas(paras);
                ServiceResult result = this.command(hwReq, hrId, name);
                if (!result.isSuccess()) {
                    msg.append(device.getHwDeviceName()).append(result.getObject()).append(";");
                    continue;
                } else {
                    // 保存空调下发命令参数
                    Optional<AirConditioner> airOp = airConditionerDao.findByDevice(device);
                    if (airOp.isPresent()) {
                        AirConditioner airConditioner = airOp.get();
                        if (hwReq.getParas().getSetPower() != null) {
                            airConditioner.setSetPower(hwReq.getParas().getSetPower());
                            airConditioner.setMode(hwReq.getParas().getSetMode());
                            airConditioner.setTemperature(hwReq.getParas().getSetTemp());
                            airConditioner.setWindDirection(hwReq.getParas().getSetFanDirection());
                            airConditioner.setWindSpeed(hwReq.getParas().getSetFanSpeed());
                            airConditioner.setSetFanAuto(hwReq.getParas().getSetFanAuto());
                            airConditioner.setSetSwing(hwReq.getParas().getSetSwing());
                            airConditionerDao.save(airConditioner);
                        }

                    } else if (hwReq.getParas().getSetPower() != null && DeviceType.AIR_CONDITIONER.equals(device.getDeviceType())) {
                        AirConditioner air = new AirConditioner();
                        air.setDevice(device);
                        air.setSetPower(hwReq.getParas().getSetPower());
                        air.setMode(hwReq.getParas().getSetMode());
                        air.setTemperature(hwReq.getParas().getSetTemp());
                        air.setWindDirection(hwReq.getParas().getSetFanDirection());
                        air.setWindSpeed(hwReq.getParas().getSetFanSpeed());
                        air.setSetFanAuto(hwReq.getParas().getSetFanAuto());
                        airConditionerDao.save(air);
                    }
                }
            }

            // 控制窗帘
            if (DeviceType.CURTAINS.equals(device.getDeviceType()) && DeviceStatus.ONLINE.equals(device.getHwStatus())) {
                /*DoorMagneticHWReq hwReq = this.findProduct(device.getHwProductId());
                if(hwReq.getRead().equals("error")){
                    msg.append(device.getHwDeviceName()).append("设备离线;");
                }*/
                DoorMagneticHWParasReq paras = new DoorMagneticHWParasReq();
                // 下发控制设备
                paras.setAction(deviceStatus.getStatus());
                hwReq.setDeviceId(device.getId());
                hwReq.setDevice_id(device.getHwDeviceId());
                hwReq.setParas(paras);
                ServiceResult result = this.command(hwReq, hrId, name);
                if (!result.isSuccess()) {
                    msg.append(device.getHwDeviceName()).append(result.getObject()).append(";");
                    continue;
                }
            }
        }
        if (StringUtils.isNotBlank(msg.toString())) {
            return ServiceResult.error(msg.toString());
        }
        // 保存操作时间
        Optional<DeviceWorkingMode> workingModeOp = workingModeDao.findById(id);
        if (workingModeOp.isPresent()) {
            DeviceWorkingMode workingMode = workingModeOp.get();
            LocalDateTime localDateTime = LocalDateTime.now().withNano(0);
            workingMode.setControlTime(localDateTime);
            workingModeDao.save(workingMode);
        }
        return ServiceResult.ok();
    }

    // 设备智能控制
    private ServiceResult control(String id) {
        ServiceResult serviceResult = iotDeviceMessageProducer.deviceControl(id, "off");
        if (!serviceResult.isSuccess()) {
            log.error("开启设备 {} 失败，{}", id, serviceResult.getObject());
        }
        return ServiceResult.ok();
    }

    /**
     * 钉钉组装请求报文
     *
     * @param content
     * @return
     */
    private static String buildReqStr(String content, boolean isAtAll, List<String> mobileList) {
        //消息内容
        Map<String, String> contentMap = Maps.newHashMap();
        contentMap.put("content", content);
        //通知人
        Map<String, Object> atMap = Maps.newHashMap();
        //1.是否通知所有人
        atMap.put("isAtAll", isAtAll);
        //2.通知具体人的手机号码列表
        atMap.put("atMobiles", mobileList);

        Map<String, Object> reqMap = Maps.newHashMap();
        reqMap.put("msgtype", "text");
        reqMap.put("text", contentMap);
        reqMap.put("at", atMap);

        return JSON.toJSONString(reqMap);
    }
}
