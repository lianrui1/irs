package cn.com.cowain.ibms.rest.resp.iot;

import cn.com.cowain.ibms.rest.bean.PageBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Yang.Lee
 * @date 2021/10/21 10:49
 */
@Data
@ApiModel("设备群组详情响应对象")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DeviceGroupDetailResp {

    @ApiModelProperty(value = "设备群组ID", required = true, position = 1)
    private String id;

    @ApiModelProperty(value = "设备群组名称", required = true, position = 2)
    private String name;

    @ApiModelProperty(value = "父ID", position = 3)
    private String parentId;

    @ApiModelProperty(value = "父群组名称", position = 4)
    private String parentName;

    @ApiModelProperty(value = "层级", required = true, position = 5)
    private int level;

    @ApiModelProperty(value = "描述", required = true, position = 6)
    private String description;

    @ApiModelProperty(value = "群组编号", required = true, position = 7)
    private String number;

    @ApiModelProperty(value = "群组中的设备列表", required = true, position = 8)
    private PageBean<IotDeviceResp> deviceList;
}
