package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 添加人员门禁点权限请求对象人员信息
 *
 * @author: yanzy
 * @date: 2022/3/31 19:08
 */
@Data
@ApiModel("添加人员门禁点权限请求对象人员信息")
public class UserDataReq {

    @ApiModelProperty(value = "人员工号")
    private String empNo;

    @ApiModelProperty(value = "人员姓名")
    private String name;

    @ApiModelProperty(value = "开始时间")
    private LocalDateTime startTime;

    @ApiModelProperty(value = "结束时间")
    private LocalDateTime endTime;
}
