package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author wei.cheng
 * @date 2022/03/08 15:06
 */
@Data
@ApiModel("权限申请")
public class AuthApplicationReq {

    @ApiModelProperty("本次申请的权限id")
    @NotBlank
    private String authId;

    @ApiModelProperty("有效的人脸照片")
    @NotBlank
    private String faceUrl;

    @ApiModelProperty("工号")
    @NotBlank
    private String empNo;
}
