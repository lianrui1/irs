package cn.com.cowain.ibms.entity.iot.centralizedControl;


import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.enumeration.iot.ApproverType;
import cn.com.cowain.ibms.enumeration.iot.DeviceControlStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "centralized_control_record")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "centralized_control_record", comment = "集控控制记录")
public class CentralizedControlRecord extends BaseEntity {

    @Column(name = "hrId", nullable = false, columnDefinition = "varchar(30) COMMENT '人员工号'")
    private String hrId;

    @Column(name = "name", nullable = false, columnDefinition = "varchar(30) COMMENT '人员名称'")
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false, columnDefinition = "varchar(30) COMMENT '人员类型'")
    private ApproverType type;

    @Column(name = "control_name", nullable = false, columnDefinition = "varchar(30) COMMENT '操作名称'")
    private String controlName;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false, columnDefinition = "varchar(30) COMMENT '状态'")
    private DeviceControlStatus status;

    @Column(name = "control_time", updatable = false, columnDefinition = "datetime COMMENT '控制时间'")
    private LocalDateTime controlTime;

    @Column(name = "trace_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'traceId'")
    private String traceId;

    @Column(name = "centralized_control_code_id", nullable = false, columnDefinition = "varchar(36) COMMENT '集控码ID'")
    private String centralizedControlCodeId;
}
