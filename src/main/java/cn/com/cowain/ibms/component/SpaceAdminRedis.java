package cn.com.cowain.ibms.component;

import lombok.Data;

@Data
public class SpaceAdminRedis {
    private String adminEmpNo;

    private String adminName;

    private String adminDept;

    private String spaceId;
}
