package cn.com.cowain.ibms.service.disthinguish;

import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.distinguish.*;
import cn.com.cowain.ibms.rest.resp.distinguish.*;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import com.alibaba.fastjson.JSONObject;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Nullable;
import java.util.List;

/**
 * 通行权限Service
 *
 * @author Yang.Lee
 * @date 2021/3/10 9:01
 */
public interface AccessRulesService {

    /**
     * 新建通行权限
     *
     * @param req: 请求参数
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/3/10 9:03
     **/
    ServiceResult save(AccessRulesReq req);

    /**
     * 更新通行权限
     *
     * @param id  权限ID
     * @param req 请求参数
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/3/10 14:34
     **/
    ServiceResult update(String id, AccessRulesReq req);

    /**
     * 查询通行权限（分页）
     *
     * @param deviceName 设备名称，模糊搜索，可选
     * @param deviceId   设备ID，可选
     * @param projectId  项目ID，可选
     * @param page       页码
     * @param size       页长
     * @return 分页数据
     * @author Yang.Lee
     * @date 2021/3/10 19:01
     **/
    PageBean<AccessRulePageResp> getPage(String deviceName, String deviceId, String projectId, int page, int size);

    /**
     * 删除通行权限
     *
     * @param id 权限ID
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/3/11 10:02
     **/
    ServiceResult delete(String id);

    /**
     * 扫脸开门
     *
     * @return 处理结果
     * @date 2021/3/11 10:02
     **/
    ServiceResult open(JSONObject partDaily);

    /**
     * 获取通行计划权限详情
     *
     * @param id 权限ID
     * @return 权限详情
     * @author Yang.Lee
     * @date 2021/3/12 16:35
     **/
    AccessRuleDetailResp get(String id);

    /**
     * 根据设备ID查询通行权限
     *
     * @param deviceId 设备ID
     * @return 权限详情
     * @author Yang.Lee
     * @date 2021/4/23 9:19
     **/
    @Nullable
    AccessRuleDetailResp getByDeviceId(String deviceId);

    // 权限列表查询(分页)
    PageBean<AccessRulePageResp> getPage(AccessRulesPageReq req);

    // 设备添加单个人员
    ServiceResult addHkUser(HkUserAddReq req);

    // 获取设备人员列表
    PageBean<HkUserPageResp> hkUserList(String name, String deviceId, int page, int size);

    // 获取设备通行记录
    PageBean<HkFaceRecordPageResp> hkFaceRecord(HkFaceRecordPageReq req);

    // 删除指定设备下人员
    ServiceResult deleteHkUser(String deviceId, List<String> jobNos);

    // 更新人员通行时间
    ServiceResult updateUserHkTime(UserTimeReq req);

    // 更新人脸
    ServiceResult updateFace(HkUpdateFaceReq req);

    // 批量添加通行人员
    ServiceResult uploadAccessRules(MultipartFile file, List<String> deviceIds);

    // 批量删除通行人员
    ServiceResult uploadDeleteAccessRules(MultipartFile file, List<String> deviceIds);

    // 获取批量添加通行数据导入失败数据的文件二进制数组
    byte[] getImportErrorDataFileByte(String errorDataId);

    // 获取批量删除人员导入失败数据的文件二进制数组
    byte[] getDelUserImportErrorDataFileByte(String errorDataId);

    // 查询海康失败结果
    ServiceResult hkResult(ImportFailedReq req);

    // 获取员工及访客列表
    PageBean<StaffPageResp> findByStaff(String keyword, Pageable pageable, String dept, Integer isStaff);

    // 获取所有访客
    void getStaff();

    // 查询海康设备
    PageBean<HkDevicePageResp> findByHkDevice(String keyword, Pageable pageable, String nodeType, String id);

    // 批量下发人员
    ServiceResult updateManyUserHkTime(ManyUserTimeReq req);

    // 单个更新人脸并下发
    ServiceResult updateUserFace(UserFaceReq req);

    // 下载HK人员通行记录
    byte[] downloadHkFaceRecord(PageBean<HkFaceRecordPageResp> pageBean);

    // 向旷视修改权限和人员组
    ServiceResult updateKs(AccessRulesReq accessRulesReq);
}
