package cn.com.cowain.ibms.rest.resp.space;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Yang.Lee
 * @date 2021/1/27 14:58
 */
@Data
@ApiModel("项目区域信息响应对象")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProjectAreaResp {
    @ApiModelProperty(value = "项目ID", required = true)
    private String id;

    @ApiModelProperty(value = "项目区域", required = true, position = 1)
    private String projectArea;
}
