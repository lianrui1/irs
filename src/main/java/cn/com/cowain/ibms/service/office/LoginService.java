package cn.com.cowain.ibms.service.office;

import cn.com.cowain.ibms.service.bean.ServiceResult;

/**
 * @author Yang.Lee
 * @date 2021/12/20 17:43
 */
public interface LoginService {

    /**
     * 智能办公室登录（小程序登录）
     *
     * @param code   UC V2 返回的code
     * @return 用户登录信息
     * @author Yang.Lee
     * @date 2021/12/20 17:43
     **/
    ServiceResult login(String code);

    /**
     * 根据token登录
     *
     * @param token
     * @return
     * @author Yang.Lee
     * @date 2021/12/21 13:15
     **/
    ServiceResult loginByToken(String token);
}
