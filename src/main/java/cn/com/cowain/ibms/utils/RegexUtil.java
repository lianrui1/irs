package cn.com.cowain.ibms.utils;

import lombok.experimental.UtilityClass;

import java.util.regex.Pattern;

/**
 * 正则表达式
 *
 * @author Yang.Lee
 * @date 2021/2/19 17:19
 */
@UtilityClass
public class RegexUtil {

    /**
     * 标点符号
     */
    public static final String PUNCTUATION = "\\pP";

    /**
     * 手机号
     **/
    public static final String MOBILE_PHONE_NUMBER = "^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\\d{8}$";
    /**
     * 文件名称
     */
    public static final String FILE_NAME = "[^\\s\\\\/:\\*\\?\\\"<>\\|](\\x20|[^\\s\\\\/:\\*\\?\\\"<>\\|])*[^\\s\\\\/:\\*\\?\\\"<>\\|\\.]$";

    /**
     * 判断文本是否匹配正则表达式
     *
     * @param context 待匹配的文本
     * @param regex   正则表达式
     * @return true: 匹配；false:不匹配
     */
    public boolean match(CharSequence context, String regex) {

        return Pattern.compile(regex).matcher(context).matches();
    }
}
