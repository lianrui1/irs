package cn.com.cowain.ibms.mq.bean;

import lombok.Data;

/**
 * @author wei.cheng
 * @date 2022/03/28 15:54
 */
@Data
public class IotcDeleteDeviceMessage {
    /**
     * 设备
     */
    private String resourceIndexCode;
}
