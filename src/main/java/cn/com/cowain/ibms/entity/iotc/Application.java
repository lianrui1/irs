package cn.com.cowain.ibms.entity.iotc;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Yang.Lee
 * @date 2021/10/20 10:32
 */
@Data
@Entity
@Table(name = "iotc_application")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iotc_application", comment = "iotc应用数据")
public class Application extends BaseEntity {

    @Column(name = "name", nullable = false, columnDefinition = "varchar(50) COMMENT '应用名称'")
    private String name;

    @Column(name = "`describe`", columnDefinition = "varchar(500) COMMENT '应用描述'")
    private String describe;
}
