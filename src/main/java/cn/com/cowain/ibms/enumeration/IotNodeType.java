package cn.com.cowain.ibms.enumeration;

/**
 * 设备接入类型 枚举类
 *
 * @author hu jingling
 * @date 2020-12-17
 * @since s3
 */
public enum IotNodeType {

    DEFAULT(""),

    /**
     * 网关子设备
     */
    GATEWAY_SUB_DEVICE("网关子设备"),

    /**
     * 网关
     */
    GATEWAY("网关设备"),

    /**
     * 直连设备
     */
    DIRECT_DEVICE("直连设备"),

    ENDPOINT("其他");

    IotNodeType(String name){
        this.name = name;
    }

    private  String name;

    public String getName(){
        return this.name;
    }
}
