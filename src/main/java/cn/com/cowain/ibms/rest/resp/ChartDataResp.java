package cn.com.cowain.ibms.rest.resp;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Yang.Lee
 * @date 2021/9/27 20:25
 */
@Data
@ApiModel("图表数据")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChartDataResp {

    private String x;

    private double y;
}
