package cn.com.cowain.ibms.dao.meeting;

import cn.com.cowain.ibms.entity.meeting.IntelligentMeetingAppMode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author feng
 * @title: IntelligentMeetingAppDao
 * @projectName ibms
 * @Date 2021/12/29 10:29
 */
@Repository
public interface IntelligentMeetingAppModeDao extends JpaRepository<IntelligentMeetingAppMode, String> {
    Optional<IntelligentMeetingAppMode>  findByName(String name);
}
