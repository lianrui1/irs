package cn.com.cowain.ibms.enumeration.space;

/**
 * 项目产权属性
 *
 * @author Yang.Lee
 * @date 2021/11/7 16:52
 */
public enum ProjectPropertyRightType {

    RENT("租用"),

    OWN("自有");

    ProjectPropertyRightType(String name){
        this.name = name;
    }

    private  String name;

    public String getName(){
        return this.name;
    }
}
