package cn.com.cowain.ibms.rest.office;

import cn.com.cowain.ibms.annotation.Idempotent;
import cn.com.cowain.ibms.enumeration.iot.UsableStatus;
import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.office.CreatDefaultWorkingModeToMyWorkingModeReq;
import cn.com.cowain.ibms.rest.req.PageReq;
import cn.com.cowain.ibms.rest.req.working_mode.ManagementSaveReq;
import cn.com.cowain.ibms.rest.req.working_mode.WorkingModePageReq;
import cn.com.cowain.ibms.rest.req.working_mode.WorkingModeSaveReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.working_mode.DefaultWorkingModePageResp;
import cn.com.cowain.ibms.rest.resp.working_mode.ManagementPageResp;
import cn.com.cowain.ibms.rest.resp.working_mode.WorkingModeDetailResp;
import cn.com.cowain.ibms.rest.resp.working_mode.WorkingModePageResp;
import cn.com.cowain.ibms.service.office.IntelligentOfficeAppletService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.utils.IConst;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/12/6 15:59
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/office/applet")
@Api(tags = IConst.INTELLIGENT_OFFICE_APPLET)
public class IntelligentOfficeAppletController {

    @Autowired
    private IntelligentOfficeAppletService intelligentOfficeAppletService;

    @PostMapping
    @Idempotent(value = API_BASE + "/office/applet/post", expireTime = 2L)
    @ApiOperation(value = "添加工作模式场景-小程序", tags = {IConst.INTELLIGENT_OFFICE_APPLET, IConst.V21})
    public ResponseEntity<JsonResult<IdResp>> save(@RequestBody @Validated WorkingModeSaveReq req, BindingResult bindingResult) {
        log.debug("save...");

        ServiceResult result = intelligentOfficeAppletService.save(req);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "查询工作模式场景详情-小程序", tags = {IConst.INTELLIGENT_OFFICE_APPLET, IConst.V21})
    public ResponseEntity<JsonResult<WorkingModeDetailResp>> detail(@PathVariable @ApiParam("工作模式ID") String id) {

        ServiceResult result = intelligentOfficeAppletService.detail(id);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.ok("OK" ,(WorkingModeDetailResp)result.getObject()));
    }

    @PutMapping("/{id}")
    @Idempotent(value = API_BASE + "/office/applet/put", expireTime = 2L)
    @ApiOperation(value = "修改工作模式场景-小程序", tags = {IConst.INTELLIGENT_OFFICE_APPLET, IConst.V21})
    public ResponseEntity<JsonResult<String>> update(@PathVariable @ApiParam(value = "工作模式ID", name = "id") String id, @RequestBody @Validated WorkingModeSaveReq req, BindingResult bindingResult) {

        ServiceResult result = intelligentOfficeAppletService.update(id, req);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @PutMapping("/status/{id}")
    @ApiOperation(value = "工作模式启用/禁用-小程序", tags = {IConst.INTELLIGENT_OFFICE_APPLET, IConst.V21})
    public ResponseEntity<JsonResult<String>> updateStatus(@PathVariable @ApiParam(value = "工作模式ID", name = "id") String id, @ApiParam(value = "启用ENABLE/禁用DISABLED", name = "status") UsableStatus status) {

        ServiceResult result = intelligentOfficeAppletService.updateStatus(id, status);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除工作模式场景-小程序", tags = {IConst.INTELLIGENT_OFFICE_APPLET, IConst.V21})
    public ResponseEntity<JsonResult<String>> delete(@PathVariable @ApiParam("工作模式ID") String id) {

        ServiceResult result = intelligentOfficeAppletService.delete(id);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @GetMapping("/page")
    @ApiOperation(value = "查询工作模式场景列表(分页)-小程序", tags = {IConst.INTELLIGENT_OFFICE_APPLET, IConst.V21})
    public ResponseEntity<JsonResult<PageBean<WorkingModePageResp>>> getPage(WorkingModePageReq req) {


        return ResponseEntity.ok(JsonResult.ok("OK" ,intelligentOfficeAppletService.getPage(req)));
    }

    @GetMapping("/default/working/mode/page")
    @ApiOperation(value = "查询默认工作模式场景列表(分页)-小程序", tags = {IConst.INTELLIGENT_OFFICE_APPLET, IConst.V21})
    public ResponseEntity<JsonResult<PageBean<DefaultWorkingModePageResp>>> getDefaultWorkingModePage(@ApiParam(name = "officeId", value = "智能办公室ID") String officeId, PageReq req) {


        return ResponseEntity.ok(JsonResult.ok("OK" ,intelligentOfficeAppletService.getDefaultWorkingModePage(officeId, req)));
    }

    @PostMapping("/default")
    @ApiOperation(value = "添加默认工作模式场景到我的-小程序", tags = {IConst.INTELLIGENT_OFFICE_APPLET, IConst.V21})
    public ResponseEntity<JsonResult<String>> creatDefaultWorkingModeToMyWorkingMode(@RequestBody @Validated CreatDefaultWorkingModeToMyWorkingModeReq req, BindingResult bindingResult) {

        ServiceResult result = intelligentOfficeAppletService.creatDefaultWorkingModeToMyWorkingMode(req);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.ok("OK" ,null));
    }

    @PostMapping("/management")
    @Idempotent(value = API_BASE + "/office/applet/management/post", expireTime = 2L)
    @ApiOperation(value = "新增权限人员-小程序", tags = {IConst.INTELLIGENT_OFFICE_APPLET, IConst.V21})
    public ResponseEntity<JsonResult<IdResp>> managementCreate(@RequestBody @Validated ManagementSaveReq req, BindingResult bindingResult) {

        ServiceResult result = intelligentOfficeAppletService.managementCreate(req);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @GetMapping("/management/page/{id}")
    @ApiOperation(value = "查询权限人员列表-小程序", tags = {IConst.INTELLIGENT_OFFICE_APPLET, IConst.V21})
    public ResponseEntity<JsonResult<PageBean<ManagementPageResp>>> managementPage(@PathVariable @ApiParam(value = "办公室ID", name = "id") String id, @RequestParam(value = "page", defaultValue = "0") int page,
                                                                                   @RequestParam(value = "size", defaultValue = "20") int size) {


        return ResponseEntity.ok(JsonResult.ok("ok" ,intelligentOfficeAppletService.managementPage(id, page, size)));
    }

    @DeleteMapping("/management/{id}")
    @ApiOperation(value = "删除权限人员-小程序", tags = {IConst.INTELLIGENT_OFFICE_APPLET, IConst.V21})
    public ResponseEntity<JsonResult<String>> managementDelete(@PathVariable @ApiParam(value = "办公室ID", name = "id") String id, @RequestParam("empNos") @ApiParam(value = "人员工号", name = "empNos") List<String> empNos) {

        ServiceResult result = intelligentOfficeAppletService.managementDelete(id, empNos);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK" ));
    }

    @PutMapping("/control/working/mode/{id}")
    @ApiOperation(value = "控制工作模式-小程序", tags = {IConst.INTELLIGENT_OFFICE_APPLET, IConst.V21})
    public ResponseEntity<JsonResult<String>> controlWorkingMode(@PathVariable @ApiParam(value = "工作模式ID", name = "id") String id) {

        ServiceResult result = intelligentOfficeAppletService.controlWorkingMode(id);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }
}
