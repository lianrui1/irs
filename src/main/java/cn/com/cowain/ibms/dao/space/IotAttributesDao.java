package cn.com.cowain.ibms.dao.space;

import cn.com.cowain.ibms.entity.space.IotAttributes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Description
 *
 * @author yuchunlei
 * @version 1.0.0
 * @createTime 2020年12月22日 10:29:00
 */
@Repository
public interface IotAttributesDao extends JpaRepository<IotAttributes, String> {

    List<IotAttributes> findByIotProductId(String id);

    //根据dictId productId查询
    @Query(value="select * from iot_attributes where dict_id = ?1 and product_id = ?2",nativeQuery=true)
    Optional<IotAttributes> findByDictIdAndIotProduct(String dictId, String productId);


}
