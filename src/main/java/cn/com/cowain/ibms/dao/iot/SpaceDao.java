package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.enumeration.iot.SpaceType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * Space对象数据持久层
 *
 * @author Yang.Lee
 * @date 2020/12/9 18:19
 */
@Repository
public interface SpaceDao extends JpaRepository<Space, String>, JpaSpecificationExecutor<Space> {

    /**
     * 根据项目ID和空间层级查询空间数量
     *
     * @param projectId
     * @return
     */
    int countByProjectIdAndLevel(String projectId, int level);

    /**
     * 根据项目ID查询所有的空间列表
     *
     * @param projectId 项目ID
     * @param level     空间层级
     * @return
     */
    List<Space> findAllByProjectIdAndLevelGreaterThanOrderByCreatedTimeDesc(String projectId, int level);

    /**
     * 根据父ID查询子列表
     *
     * @param parentId
     * @return
     */
    List<Space> findAllByParentId(String parentId);

    List<Space> findAllByIdOrParentId(String id, String parentId);

    /**
     * 根据项目ID查询小于等于层级的数据
     *
     * @param projectId
     * @param level
     * @return
     */
    List<Space> findAllByProjectIdAndLevelLessThanEqual(String projectId, int level);

    /**
     * 查询projectId = {@code projectId} & isShow = {@code isShow}的space列表
     *
     * @param projectId
     * @param isShow
     * @return
     */
    List<Space> findAllByProjectIdAndIsShow(String projectId, int isShow);

    /**
     * 根据空间ID批量删除(执行update语句，将数据表中的is_show字段修改为0，而不是执行delete语句)
     *
     * @param ids
     */
    @Modifying
    @Query("update Space s set isShow = 0 where  s.id in (:ids) ")
    void deleteByIds(List<String> ids);

    /**
     * 查询项目下最大空间层级
     *
     * @param projectId
     * @return
     */
    @Query(value = "select max(s_level) from iot_space s where is_del = 0 and project_id = ?1", nativeQuery = true)
    Integer findMaxLevelByProjectId(String projectId);

    /**
     * 根据项目id获取空间信息
     */
    Set<Space> findByProjectIdAndIsShow(String id, int isShow);


    /**
     * 根据项目id获取空间信息
     */
    Set<Space> findByProjectIdAndIsShowAndLevelNot(String id, int isShow, int level);

    /**
     * 查询所有大于目标层级的数据
     *
     * @param level
     * @return
     */
    List<Space> findByLevelGreaterThan(int level);

    /**
     * 批量更新空间项目id
     *
     * @param projectId
     * @param ids
     */
    @Modifying
    @Query(value = "update iot_space set project_id = ?1 where id in (?2) ", nativeQuery = true)
    void updateByIds(String projectId, List<String> ids);

    // 根据id获取多个空间
    List<Space> findByIdIn(List<String> spaceIds);

    /**
     * 根据项目和层级查询空间ID
     *
     * @param projectId 项目ID
     * @param level     空间层级
     * @return 空间列表
     * @author Yang.Lee
     * @date 2021/11/11 14:23
     **/
    List<Space> findByProjectIdAndLevel(String projectId, int level);

    /**
     * 根据空间扩展数据模糊查询
     *
     * @param projectId 项目id
     * @param keyword   查询关键字
     * @param isShow    空间是否显示
     * @return 空间列表
     * @author Yang.Lee
     * @date 2021/11/22 16:47
     **/
    List<Space> findByProjectIdAndPropertiesLikeAndIsShow(String projectId, String keyword, int isShow);

    List<Space> findByIdAndIsShowAndLevelNot(String spaceId, int isShow, int level);

    List<Space> findByParentIdAndSpaceType(String id, SpaceType floor);

    List<Space> findByProjectIdAndIsShowAndLevel(String id, int i, int i1);
    List<Space> findByParentId(String parentId);

    /**
     * 查询项目下
     *
     * @param projectId
     * @return
     */
    @Query(
            value = "select id from iot_space where project_id = ?1 and is_del = 0",
            nativeQuery = true
    )
    List<String> findIdByProjectId(String projectId);
}

