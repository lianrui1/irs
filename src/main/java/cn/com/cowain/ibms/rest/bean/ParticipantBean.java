package cn.com.cowain.ibms.rest.bean;


import cn.com.cowain.ibms.enumeration.ReservationRecordItemStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 参与人 和 状态
 *
 * @author Zhang yuxin
 */
@Data
@ApiModel("参与人和状态")
public class ParticipantBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 参与人id
     */
    @ApiModelProperty(value = "参与人id", required = true, position = 0)
    private String owner;

    /**
     * 参与人姓名
     */
    @ApiModelProperty(value = "参与人姓名", required = true, position = 1)
    private String ownerName;


    /**
     * 状态
     */
    @ApiModelProperty(value = "状态", required = true, position = 2)
    private ReservationRecordItemStatus status;

    /**
     * 部门id
     */
    @ApiModelProperty(value = "部门id", required = true, position = 3)
    private String deptId;

    /**
     * 部门名称
     */
    @ApiModelProperty(value = "部门名称", required = true, position = 4)
    private String deptName;
    /**
     * 部门名称
     */
    @ApiModelProperty(value = "参与人工号", required = true, position = 5)
    private String hrId;

    @ApiModelProperty(value = "微信头像", required = true, position = 6)
    private String headImgUrl;

    /**
     * 是否是外部人员
     */
    @ApiModelProperty(value = "是否是访客，1 是； 0 否", position = 7)
    private Integer isGuest;

    @ApiModelProperty(value = "访客邀请记录code", position = 8)
    private String code;

    /**
     * 是否是外部人员
     */
    @ApiModelProperty(value = "是否是会议组成员，1 是； 0 否", position = 9)
    private Integer isGroup;

    @ApiModelProperty(value = "是否签到，1 是； 2 否", position = 10)
    private Integer isAttendance;
}
