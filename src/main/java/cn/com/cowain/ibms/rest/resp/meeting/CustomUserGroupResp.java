package cn.com.cowain.ibms.rest.resp.meeting;

import cn.com.cowain.ibms.rest.resp.StaffResp;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/6/8 18:14
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("用户自定义用户组返回对象")
public class CustomUserGroupResp {

    @ApiModelProperty(value = "用户组ID", required = true, example = "123")
    private String id;

    @ApiModelProperty(value = "用户组名称", required = true, example = "用户组", position = 1)
    private String name;

    @ApiModelProperty(value = "成员列表", required = true, position = 2)
    private List<StaffResp> userList;
}
