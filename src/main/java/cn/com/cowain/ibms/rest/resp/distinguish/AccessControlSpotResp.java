package cn.com.cowain.ibms.rest.resp.distinguish;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**

/**
 * @author yanzy
 * @date 2022/1/10 17:19
 */
@Data
@ApiModel("关联门禁点响应对象")
public class AccessControlSpotResp {

    @ApiModelProperty(value = "门禁点ID",required = true,position = 1)
    private String id;

    @ApiModelProperty(value = "门禁点名称",required = true,position = 2)
    private String name;

    @ApiModelProperty(value = "门禁点类型",required = true,position = 3)
    private String type;

    @ApiModelProperty(value = "门禁点位置",required = true,position = 4)
    private String address;
}
