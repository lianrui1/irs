package cn.com.cowain.ibms.rest.resp.meeting;

import cn.com.cowain.ibms.rest.resp.DeviceResp;
import cn.com.cowain.ibms.rest.resp.ReservationRecordDetailsResp;
import cn.com.cowain.ibms.rest.resp.iot.DeviceWechatResp;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/6/8 18:46
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("正在进行中会议返回对象")
public class OngoingMeetingResp{

    @ApiModelProperty(value = "会议详情", required = true)
    private ReservationRecordDetailsResp reservationDetail;

    @ApiModelProperty(value = "会议室设备列表", required = true)
    private List<DeviceWechatResp> deviceWechatList;

    @ApiModelProperty(value = "可控设备列表", required = true)
    private List<DeviceWechatResp> deviceControlList;

    @ApiModelProperty(value = "设备数据", required = true)
    private DeviceResp value;
}
