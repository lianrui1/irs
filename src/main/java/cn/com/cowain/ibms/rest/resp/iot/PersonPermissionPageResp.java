package cn.com.cowain.ibms.rest.resp.iot;

import cn.com.cowain.ibms.entity.iot.AccessControlSpotAccessRecord;
import cn.com.cowain.ibms.enumeration.iot.AccessControlSpotType;
import cn.com.cowain.ibms.service.space.SpaceService;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * @Author tql
 * @Description 人员权限分页数据响应对象
 * @Date 21-11-29 上午10:42
 * @Version 1.0
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel("人员权限分页数据响应对象")
public class PersonPermissionPageResp {

    @ApiModelProperty(value = "通行人员信息")
    private String personMsg;

    @ApiModelProperty(value = "人员所属部门")
    private String department;

    @ApiModelProperty(value = "人员类型")
    private String personType;

    @ApiModelProperty(value = "门禁点")
    private String controlSpot;

    @ApiModelProperty(value = "门禁类型")
    private String controlType;

    @ApiModelProperty(value = "门禁点位置")
    private String controlSpotLocation;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "通行时间")
    private LocalDateTime passTime;

    @ApiModelProperty(value = "状态")
    private String status;

    @ApiModelProperty(value = "抓拍图片", required = true, example = "http://xx/x/x/xx/picture.jpg", position = 3)
    private String capturePicture;

    @ApiModelProperty(value = "底库图片", required = true, example = "http://xx/x/x/xx/picture.jpg", position = 4)
    private String defaultPicture;

    @ApiModelProperty(value = "体温。", example = "36.5/未检测", position = 8)
    private String temperature;

    /**
     * @param source
     * @return
     * @description 对象源转换
     * @author tql
     * @date 21-12-2
     */
    public static PersonPermissionPageResp convert(AccessControlSpotAccessRecord source) {
        String personMsg = "";
        if (StringUtils.isNotBlank(source.getDeviceAccessRecord().getNameZh()) && StringUtils.isNotBlank(source.getDeviceAccessRecord().getEmpNo())) {
            personMsg = source.getDeviceAccessRecord().getNameZh() + "/" + source.getDeviceAccessRecord().getEmpNo();
        }else {
            personMsg = source.getDeviceAccessRecord().getNameZh();
        }

        return PersonPermissionPageResp.builder()
                // 通行人员信息
                .personMsg(personMsg)
                // 人员所属部门
                .department(source.getDeviceAccessRecord().getDepartment())
                // 人员类型
                .personType(source.getDeviceAccessRecord().getStaffType().getName())
                // 门禁点
                .controlSpot(source.getAccessControlSpot().getName())
                // 门禁类型
                .controlType(Optional.ofNullable(source.getAccessControlSpot().getAccessControlSpotType()).map(AccessControlSpotType::getName).orElse(""))
                // 门禁点位置
                .controlSpotLocation(source.getAccessControlSpot().getSpace().getProject().getProjectName() + " / " + SpaceService.getFullSpaceName(source.getAccessControlSpot().getSpace(), source.getAccessControlSpot().getSpace().getName()) + " / " + source.getAccessControlSpot().getAddress())
                // 通行时间
                .passTime(source.getDeviceAccessRecord().getDataTime())
                // 状态
                .status(source.getDeviceAccessRecord().getAccessStatus())
                .capturePicture(Optional.ofNullable(source.getDeviceAccessRecord().getCapturePicture()).orElse(""))
                .defaultPicture(Optional.ofNullable(source.getDeviceAccessRecord().getDefaultPicture()).orElse(""))
                .temperature(Optional.ofNullable(source.getDeviceAccessRecord().getTemperature()).orElse(""))
                .build();
    }
}
