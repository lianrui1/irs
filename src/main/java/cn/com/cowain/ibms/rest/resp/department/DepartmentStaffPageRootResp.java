package cn.com.cowain.ibms.rest.resp.department;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户分页数据root对象
 * @author Yang.Lee
 * @date 2021/2/8 10:51
 */
@Data
@ApiModel("用户分页数据root对象")
public class DepartmentStaffPageRootResp implements Serializable {

    private static final long serialVersionUID = -2867205963676750518L;

    @ApiModelProperty(value = "消息", required = true)
    private String msg;

    @ApiModelProperty(value = "代码", required = true, position = 1)
    private Integer code;

    @ApiModelProperty(value = "部门员工分页数据", required = true, position = 2)
    private DepartmentStaffPageResp page;
}
