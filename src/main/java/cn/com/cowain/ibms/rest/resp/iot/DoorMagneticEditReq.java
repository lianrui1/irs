package cn.com.cowain.ibms.rest.resp.iot;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/10/19 18:09
 */
@Data
@ApiModel("门磁编辑参数")
public class DoorMagneticEditReq {

    private List<String> deviceIdList;
}
