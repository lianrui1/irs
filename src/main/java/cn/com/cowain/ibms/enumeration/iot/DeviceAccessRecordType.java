package cn.com.cowain.ibms.enumeration.iot;

/**
 * @author Yang.Lee
 * @date 2021/12/2 19:06
 */
public enum DeviceAccessRecordType {

    HIK("海康面板机记录"),

    MEGVII("旷世面板机记录"),

    DOOR_MAGNETIC("门磁记录");

    DeviceAccessRecordType(String name) {
        this.name = name;
    }

    private String name;

    public String getName() {
        return this.name;
    }
}
