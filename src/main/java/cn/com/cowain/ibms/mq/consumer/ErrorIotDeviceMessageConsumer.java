package cn.com.cowain.ibms.mq.consumer;

import cn.com.cowain.ibms.mq.bean.IotDeviceControlMessage;
import cn.com.cowain.ibms.service.space.IotDeviceService;
import com.alibaba.fastjson.JSON;
import org.apache.rocketmq.spring.core.RocketMQListener;

import javax.annotation.Resource;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/5 18:25
 */
//@Slf4j
//@RocketMQMessageListener(
//        topic = "${redis.key.projectName}-${rocketmq.group}-"+Topic.ERROR_DEVICE_CONTROL,
//        consumerGroup = "device-${rocketmq.group}"
//)
public class ErrorIotDeviceMessageConsumer implements RocketMQListener<String> {

    @Resource
    private IotDeviceService iotDeviceService;

    @Override
    public void onMessage(String message) {

        IotDeviceControlMessage iotDeviceControlMessage = JSON.parseObject(message, IotDeviceControlMessage.class);

        iotDeviceService.control(iotDeviceControlMessage.getDeviceId(), iotDeviceControlMessage.getCmd());
    }
}