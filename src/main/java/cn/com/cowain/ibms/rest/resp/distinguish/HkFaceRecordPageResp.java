package cn.com.cowain.ibms.rest.resp.distinguish;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/22 14:13
 */
@Data
public class HkFaceRecordPageResp {

    @ApiModelProperty(value = "排序")
    private Integer seq;

    @ApiModelProperty(value = "人员personId")
    private String personId;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "工号")
    private String jobNum;

    @ApiModelProperty(value = "部门")
    private String dept;

    @ApiModelProperty(value = "底库照片")
    private String photo;

    @ApiModelProperty(value = "抓拍照片")
    private String happenPhoto;

    @ApiModelProperty(value = "抓拍时间")
    private String happenTime;

    @ApiModelProperty(value = "状态 全部通行")
    private String status;

}
