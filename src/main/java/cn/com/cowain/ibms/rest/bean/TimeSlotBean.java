package cn.com.cowain.ibms.rest.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * 时间段,以半小时为间隔
 * 比如: 09:00 - 09:30
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/5/20
 */
@Data
public class TimeSlotBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 从时间
     */
    private String from;

    /**
     * 到时间
     */
    private String to;

}
