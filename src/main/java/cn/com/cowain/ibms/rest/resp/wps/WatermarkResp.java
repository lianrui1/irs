package cn.com.cowain.ibms.rest.resp.wps;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wei.cheng
 * @date 2022/02/22 15:11
 */
@Data
@ApiModel("用于预览时添加第三方水印")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WatermarkResp {

    @ApiModelProperty(value = "水印类型， 0为无水印； 1为文字水印", position = 1)
    private Integer type;

    @ApiModelProperty(value = "文字水印的文字，当type为1时，此字段必填", position = 2)
    private String value;

    @ApiModelProperty(value = "水印的透明度，非必填，有默认值", position = 3)
    private String fillstyle;

    @ApiModelProperty(value = "水印的字体，非必填，有默认值", position = 4)
    private String font;

    @ApiModelProperty(value = "水印的旋转度，非必填，有默认值", position = 5)
    private Double rotate;

    @ApiModelProperty(value = "水印水平间距，非必填，有默认值", position = 6)
    private Integer horizontal;

    @ApiModelProperty(value = "水印垂直间距，非必填，有默认值", position = 7)
    private Integer vertical;
}
