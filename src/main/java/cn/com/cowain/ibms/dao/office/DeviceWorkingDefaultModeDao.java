package cn.com.cowain.ibms.dao.office;


import cn.com.cowain.ibms.entity.office.DeviceWorkingDefaultMode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface DeviceWorkingDefaultModeDao extends JpaRepository<DeviceWorkingDefaultMode, String>, JpaSpecificationExecutor<DeviceWorkingDefaultMode> {
}
