package cn.com.cowain.ibms.service;

import cn.com.cowain.ibms.entity.ReservationRecord;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Set;

/**
 * Description
 *
 * @author 张宇鑫
 * @version 1.0
 * @since 2020 8/14/20
 */
@SpringBootTest
class MeetingPollServiceTest{


    @Autowired
    private MeetingPollService meetingPollService;

    @Test
    void getUpcomingMeeting(){
        Integer timeMinute = 20;
        Set<ReservationRecord> reservationRecord = meetingPollService.getUpcomingMeeting(timeMinute);
        System.out.println(reservationRecord);
    }

}
