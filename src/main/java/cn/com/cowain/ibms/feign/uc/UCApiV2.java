package cn.com.cowain.ibms.feign.uc;

import cn.com.cowain.ibms.feign.bean.uc.AuthTokenResDto;
import cn.com.cowain.ibms.feign.bean.uc.UserBaseInfoResDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Yang.Lee
 * @date 2021/12/20 17:34
 */
@Service
@FeignClient(value = "ms-uc-v2", url = "${ms-uc-v2.url}")
public interface UCApiV2 {

    @GetMapping("/user/baseInfo")
    UserBaseInfoResDto userBaseInfo(@RequestParam(value = "token") String token, @RequestParam(value = "userid") String userId);

    @GetMapping("/auth/token")
    AuthTokenResDto authToken(@RequestParam(value = "client_id") String clientId,
                              @RequestParam(value = "secret") String secret,
                              @RequestParam(value = "code") String code);

}
