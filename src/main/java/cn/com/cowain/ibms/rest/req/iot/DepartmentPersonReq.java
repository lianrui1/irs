package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author tql
 * @Description 部门人员权限 请求体
 * @Date 21-11-29 上午10:22
 * @Version 1.0
 */
@Data
@ApiModel("部门人员权限请求对象")
public class DepartmentPersonReq{

    @ApiModelProperty(value = "门禁点id", position = 1)
    private String id;

    @ApiModelProperty(value = "部门id数组", position = 2)
    private List<String> departmentIds;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "通行时间(开始时间)")
    private LocalDateTime startTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "通行时间(结束时间)")
    private LocalDateTime endTime;
}
