package cn.com.cowain.ibms.service.iot;

import cn.com.cowain.ibms.enumeration.meeting.DoorMagneticAccessResult;
import cn.com.cowain.ibms.enumeration.meeting.UserType;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.iot.DoorMagneticPageReq;
import cn.com.cowain.ibms.rest.resp.BaseListResp;
import cn.com.cowain.ibms.rest.resp.StaffResp;
import cn.com.cowain.ibms.rest.resp.iot.AccessControlSpotApplicationRecordResp;
import cn.com.cowain.ibms.rest.resp.iot.DoorMagneticDetailResp;
import cn.com.cowain.ibms.rest.resp.iot.DoorMagneticEditReq;
import cn.com.cowain.ibms.rest.resp.iot.DoorMagneticPageResp;
import cn.com.cowain.ibms.rest.resp.iot.doorMagnetic.DoorMagneticStatusResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Optional;

/**
 * 门磁Service
 *
 * @author Yang.Lee
 * @date 2021/1/28 10:53
 */
public interface DoorMagneticService {

    /**
     * 获取门磁设备列表
     *
     * @param projectId 项目ID
     * @param spaceId   空间ID
     * @return 门磁列表
     * @author Yang.Lee
     * @date 2021/4/19 11:03
     **/
    List<BaseListResp> getList(@Nullable String projectId, @Nullable String spaceId);

    /**
     * 门磁开门，优先使用华为云方式开门
     *
     * @param magneticNum 门磁编号
     * @param hrId        工号
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/5/6 9:38
     **/
    ServiceResult open(String magneticNum, String hrId);

    /**
     * 保存通行记录
     *
     * @param empNo          工号
     * @param doorMagneticId 门磁ID
     * @param userType       用户类型
     * @param accessResult   通行结果
     * @return
     * @author Yang.Lee
     * @date 2021/8/17 10:32
     **/
    ServiceResult saveAccessRecord(String empNo, String doorMagneticId, UserType userType, DoorMagneticAccessResult accessResult);

    /**
     * 保存访客通行记录
     *
     * @param mobile         手机号
     * @param doorMagneticId 门磁ID
     * @param accessResult   通行结果
     * @return
     * @author Yang.Lee
     * @date 2021/8/31 14:01
     **/
    ServiceResult saveGuestAccessRecord(String mobile, String doorMagneticId, DoorMagneticAccessResult accessResult);

    /**
     * 保存访客通行记录
     *
     * @param hrId           访客工号
     * @param name           访客姓名
     * @param doorMagneticId 门磁ID
     * @param accessResult   通行结果
     * @return
     * @author Yang.Lee
     * @date 2021/8/31 14:01
     **/
    ServiceResult saveGuestAccessRecord(String hrId, String name, String doorMagneticId, DoorMagneticAccessResult accessResult);

    /**
     * 查询门磁列表（分页）
     *
     * @param req 查询参数
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/10/15 14:15
     **/
    PageBean<DoorMagneticPageResp> getPage(DoorMagneticPageReq req);

    /**
     * 获取门磁详情
     *
     * @param id 门磁ID
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/10/19 13:53
     **/
    DoorMagneticDetailResp get(String id);

    /**
     * 更新门磁数据
     *
     * @param id  门磁ID
     * @param req 修改内容
     * @return 结果
     * @author Yang.Lee
     * @date 2021/10/19 18:15
     **/
    ServiceResult update(String id, DoorMagneticEditReq req);

    /**
     * 根据门磁编号查询设备状态
     *
     * @param dm 门磁编号
     * @return 设备状态, 若查询不到设备或设备状态，则返回Optional.empty()
     * @author Yang.Lee
     * @date 2022/7/13 13:41
     **/
    Optional<DoorMagneticStatusResp> getStatus(String dm);

    /**
     * 推送异常消息给管理员
     *
     * @param dm  门磁编号
     * @param uid 上报人ucId
     * @return 消息发送结果
     * @author Yang.Lee
     * @date 2022/7/14 15:48
     **/
    ServiceResult sendErrorMessageToAdmin(String dm, String uid);

    /**
     * 门磁权限审核(静态权限)
     *
     * @param dm  门磁编号
     * @param uid 上报人ucId
     * @return 检查结果，若用户有权限则ServiceResult.getObject()返回true,无权限返回false
     * @author Yang.Lee
     * @date 2022/7/15 14:04
     **/
    ServiceResult checkStaticPermission(String dm, String uid);

    /**
     * 门磁权限审核(动态权限)
     *
     * @param dm  门磁编号
     * @param uid 上报人ucId
     * @param doorPlateId 门牌ID
     * @return 检查结果，若用户有权限则ServiceResult.getObject()返回true,无权限返回false
     * @author Yang.Lee
     * @date 2022/7/15 14:04
     **/
    ServiceResult checkDynamicPermission(String dm, String uid, String doorPlateId);

    /**
     * 2.0版本门磁开门。
     *
     * @param dm       门磁编号
     * @param uid      用户UCid
     * @param openType 开门类型。 static: 静态二维码开门。 dynamic：动态二维码开门
     * @param doorPlateId 门牌ID
     * @return 开门结果
     * @author Yang.Lee
     * @date 2022/7/18 9:41
     **/
    ServiceResult openV2(String dm, String uid, String openType, String doorPlateId);

    /**
     * 获取门磁审批管理员列表
     *
     * @param dm 门磁编号
     * @return 管理员列表
     * @author Yang.Lee
     * @date 2022/7/18 10:38
     **/
    List<StaffResp> getDoorMagneticApprovalAdminList(String dm);

    /**
     * 查询用户对设备的申请记录
     *
     * @param uid ucId
     * @param dm  门磁编号
     * @return 申请记录
     * @author Yang.Lee
     * @date 2022/7/18 14:26
     **/
    List<AccessControlSpotApplicationRecordResp> getUserApplyRecordList(String uid, String dm);

    /**
     * 权限申请
     *
     * @param dm 门磁编号
     * @param uid ucId
     * @return 申请结果
     * @author Yang.Lee
     * @date 2022/7/18 17:22
     **/
    ServiceResult permissionApply(String dm, String uid);

    /**
     * 获取门磁设备管理员列表
     *
     * @param dm 门磁编号
     * @return 管理员列表
     * @author Yang.Lee
     * @date 2022/7/18 10:38
     **/
    List<StaffResp> getDoorMagneticDeviceAdminList(String dm);
}
