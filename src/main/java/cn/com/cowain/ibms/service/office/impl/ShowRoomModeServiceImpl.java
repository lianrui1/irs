package cn.com.cowain.ibms.service.office.impl;

import cn.com.cowain.ibms.dao.device.DeviceModeStatusDao;
import cn.com.cowain.ibms.dao.device.DeviceWorkingModeDao;
import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.dao.iot.SpaceDao;
import cn.com.cowain.ibms.dao.office.IntelligentOfficeDao;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.office.DeviceModeStatus;
import cn.com.cowain.ibms.entity.office.DeviceWorkingMode;
import cn.com.cowain.ibms.entity.office.IntelligentOffice;
import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.enumeration.iot.IntelligentOfficeStatus;
import cn.com.cowain.ibms.feign.bean.common.CommonReq;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.PageReq;
import cn.com.cowain.ibms.rest.req.working_mode.DeviceModeStatusSaveReq;
import cn.com.cowain.ibms.rest.req.working_mode.ShowRoomWorkingModeSaveReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.iot.IotDeviceResp;
import cn.com.cowain.ibms.rest.resp.working_mode.DefaultWorkingModePageResp;
import cn.com.cowain.ibms.rest.resp.working_mode.ShowRoomDeviceModeStatusDetailResp;
import cn.com.cowain.ibms.rest.resp.working_mode.ShowRoomWorkingModeDetailResp;
import cn.com.cowain.ibms.service.bean.DoorMagneticHWParasReq;
import cn.com.cowain.ibms.service.bean.DoorMagneticHWReq;
import cn.com.cowain.ibms.service.bean.ServiceMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.device.HwProductsService;
import cn.com.cowain.ibms.service.office.IOfficeRedisService;
import cn.com.cowain.ibms.service.office.ShowRoomModelService;
import cn.com.cowain.ibms.utils.ErrConst;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author feng
 * @title: ShowRoomModeImpl
 * @projectName ibms
 * @Date 2022/3/31 15:35
 */
@Slf4j
@Service
public class ShowRoomModeServiceImpl implements ShowRoomModelService {

    @Autowired
    private DeviceWorkingModeDao workingModeDao;

    @Autowired
    private DeviceModeStatusDao modeStatusDao;

    @Autowired
    private DeviceDao deviceDao;

    @Autowired
    private IntelligentOfficeDao officeDao;
    @Autowired
    private SpaceDao spaceDao;

    @Autowired
    private HwProductsService productsService;


    @Autowired
    private DeviceDao iotDeviceDao;

    @Override
    @Transactional
    public ServiceResult save(ShowRoomWorkingModeSaveReq req) {

        // 根据会议室ID获取会议室信息
        Optional<Space> officeOp = spaceDao.findById(req.getSpaceId());
        if (officeOp.isEmpty()) {
            return ServiceResult.error("空间不存在");
        }
        // 判断该空间下创建人该模式是否已经存在
        Optional<DeviceWorkingMode> workingModeOp = workingModeDao.findBySpaceIdAndName(req.getSpaceId(), req.getName());
        if (workingModeOp.isPresent()) {
            return ServiceResult.error("此空间下该模式已存在,不可重复创建");
        }

        // 工作模式
        DeviceWorkingMode workingMode = new DeviceWorkingMode();
        Optional<IntelligentOffice> officeOpt = officeDao.findBySpaceId(officeOp.get().getId());
        if (officeOpt.isEmpty()) {
            IntelligentOffice office = new IntelligentOffice();
            office.setName(officeOp.get().getName());
            office.setSpace(officeOp.get());
            office.setStatus(IntelligentOfficeStatus.ENABLE);
            officeDao.save(office);
            iOfficeRedisService.setIntelligentOfficeRedis(office);
            workingMode.setIntelligentOffice(office);
        } else {
            workingMode.setIntelligentOffice(officeOpt.get());
        }
        workingMode.setName(req.getName());
        workingMode.setImg(req.getImgUrl());
        workingMode.setUserHrId("000");
        workingMode.setSpace(officeOp.get());
        workingMode.setUsableStatus(req.getUsableStatus());
        workingMode.setShowroomCentralize(req.getShowroomCentralize());
        workingMode.setPurpose(req.getPurpose());
        // 保存
        workingModeDao.save(workingMode);

        // 保存模式下对应设备
        for (DeviceModeStatusSaveReq modeStatusSaveReq : req.getSaveReqs()) {

            Optional<IotDevice> deviceOp = deviceDao.findByHwDeviceId(modeStatusSaveReq.getDeviceId());
            if (deviceOp.isEmpty()) {
                // 业务处理失败，手动回滚事务,返回异常
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error("设备不存在，绑定失败");
            }

            // 设备状态
            IotDevice device = deviceOp.get();
            DeviceModeStatus modeStatus = new DeviceModeStatus();
            modeStatus.setProperties(JSON.toJSONString(modeStatusSaveReq));
            modeStatus.setDevice(device);
            modeStatus.setMode(workingMode);
            modeStatusDao.save(modeStatus);
        }

        log.info("场景新增成功: " + workingMode.getName());

        // 返回主键ID
        IdResp idResp = new IdResp();
        idResp.setId(workingMode.getId());
        return ServiceResult.ok(idResp);
    }

    @Override
    @Transactional
    public ServiceResult update(ShowRoomWorkingModeSaveReq req) {

        // 根据会议室ID获取会议室信息


        // 判断该空间下创建人该模式是否已经存在
        Optional<DeviceWorkingMode> workingModeOp = workingModeDao.findById(req.getId());
        if (!workingModeOp.isPresent()) {
            return ServiceResult.error("此空间下该模式不存在");
        }
        // 工作模式
        DeviceWorkingMode workingMode = workingModeOp.get();
        workingMode.setIsDefault(1);
        if (!StringUtils.isEmpty(req.getName())) {
            workingMode.setName(req.getName());
        }
        if (req.getShowroomCentralize() != null) {
            workingMode.setShowroomCentralize(req.getShowroomCentralize());
        }
        if (!StringUtils.isEmpty(req.getImgUrl())) {
            workingMode.setImg(req.getImgUrl());
        }
        if (null != req.getUsableStatus()) {
            workingMode.setUsableStatus(req.getUsableStatus());
        }
        if (!StringUtils.isEmpty(req.getPurpose())) {
            workingMode.setPurpose(req.getPurpose());
        }
        // 保存
        workingModeDao.save(workingMode);
        List<DeviceModeStatus> delModeStatus = modeStatusDao.findByModeId(workingMode.getId());
        if (!CollectionUtils.isEmpty(delModeStatus)) {
            for (DeviceModeStatus modeStatus : delModeStatus) {
                modeStatus.setIsDelete(1);
                modeStatusDao.save(modeStatus);
            }
        }
        // 保存模式下对应设备
        for (DeviceModeStatusSaveReq modeStatusSaveReq : req.getSaveReqs()) {

            Optional<IotDevice> deviceOp = deviceDao.findByHwDeviceId(modeStatusSaveReq.getDeviceId());
            if (deviceOp.isEmpty()) {
                // 业务处理失败，手动回滚事务,返回异常
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error("设备不存在，绑定失败");
            }

            // 设备状态
            IotDevice device = deviceOp.get();
            DeviceModeStatus modeStatus = new DeviceModeStatus();
            modeStatus.setProperties(JSON.toJSONString(modeStatusSaveReq));
            modeStatus.setDevice(device);
            modeStatus.setMode(workingMode);
            modeStatusDao.save(modeStatus);
        }
        log.info("场景新增成功: " + workingMode.getName());
        // 返回主键ID
        IdResp idResp = new IdResp();
        idResp.setId(workingMode.getId());
        return ServiceResult.ok(idResp);
    }

    @Override
    public ServiceResult controlWorkingMode(String id) {

        Optional<DeviceWorkingMode> workingModeOp = workingModeDao.findById(id);
        if (workingModeOp.isEmpty()) {
            return ServiceResult.error("控制失败,模式不存在");
        }
        DeviceWorkingMode workingMode = workingModeOp.get();
        String hrId = "CowainAdmin";
        String name = "展厅";
        // 控制时间
        workingMode.setControlTime(LocalDateTime.now());
        workingModeDao.save(workingMode);

        // 循环控制设备
        StringBuilder errorMsg = new StringBuilder();
        List<DeviceModeStatus> list = modeStatusDao.findByModeId(id);
        for (DeviceModeStatus it : list) {
            IotDevice device = it.getDevice();

            if (!DeviceStatus.ONLINE.equals(device.getHwStatus())) {
                errorMsg.append(it.getDevice().getHwDeviceName()).append(",");
                continue;
            }

            // 获取设备所属产品的服务ID和命令名称
            ServiceResult serviceResult = productsService.findProductProperties(device);
            if (!serviceResult.isSuccess()) {
                errorMsg.append(it.getDevice().getHwDeviceName()).append(",");
                continue;
            }
            DoorMagneticHWReq hwReq = (DoorMagneticHWReq) serviceResult.getObject();
            DoorMagneticHWParasReq paras = JSON.parseObject(it.getProperties(), DoorMagneticHWParasReq.class);
            String hwDeviceId = device.getHwDeviceId();
            // 下发控制设备
            hwReq.setDeviceId(device.getId());
            hwReq.setDevice_id(hwDeviceId);
            hwReq.setParas(paras);

            // 空调
            if (DeviceType.AIR_CONDITIONER.equals(device.getDeviceType())) {
                ServiceResult result = productsService.command(hwReq, hrId, name);
                if (!result.isSuccess()) {
                    errorMsg.append(it.getDevice().getHwDeviceName()).append(",");
                    continue;
                }
            }

            // 窗帘
            if (DeviceType.CURTAINS.equals(device.getDeviceType())) {
                ServiceResult result = productsService.command(hwReq, hrId, name);
                if (!result.isSuccess()) {
                    errorMsg.append(it.getDevice().getHwDeviceName()).append(",");
                    continue;
                }
            }

            // 门磁
            if (DeviceType.DOOR_MAGNETIC.equals(device.getDeviceType())) {
                ServiceResult result = productsService.command(hwReq, hrId, name);
                if (!result.isSuccess()) {
                    errorMsg.append(it.getDevice().getHwDeviceName()).append(",");
                    continue;
                }
            }

            // 四路开关
            if (DeviceType.FOUR_WAY_SWITCH.equals(device.getDeviceType())) {
                ServiceResult result = productsService.updateSwitch(hwReq, hrId, name);
                if (!result.isSuccess()) {
                    errorMsg.append(it.getDevice().getHwDeviceName()).append(",");
                }
            }

        }

        if (StringUtils.isNotBlank(errorMsg.toString())) {
            return ServiceResult.error("控制失败设备" + errorMsg.substring(0, errorMsg.length() - 1));
        }

        return ServiceResult.ok();
    }


    @Override
    public PageBean<DefaultWorkingModePageResp> getDefaultWorkingModePage(String spaceId, Integer showroomCentralize, PageReq req) {
        PageBean<DefaultWorkingModePageResp> pageBean = new PageBean<>();
        Pageable pageable = PageRequest.of(req.getPage(), req.getSize(), Sort.by(Sort.Direction.DESC, "createdTime"));

        Page<DeviceWorkingMode> all = null;
        if (showroomCentralize != null && showroomCentralize > 0) {
            all = workingModeDao.findBySpaceIdAndShowroomCentralize(spaceId, showroomCentralize, pageable);
        } else {
            all = workingModeDao.findBySpaceId(spaceId, pageable);
        }

        List<DefaultWorkingModePageResp> list = new ArrayList<>();
        all.getContent().forEach(it -> {
            DefaultWorkingModePageResp resp = new DefaultWorkingModePageResp();
            resp.setId(it.getId());
            resp.setImgUrl(it.getImg());
            resp.setName(it.getName());
            resp.setPurpose(it.getPurpose());
            list.add(resp);
        });

        pageBean.setList(list);
        pageBean.setPage(req.getPage());
        pageBean.setSize(req.getSize());
        pageBean.setTotalElements(all.getTotalElements());
        pageBean.setTotalPages(all.getTotalPages());
        return pageBean;
    }

    @Override
    public ServiceResult changeDeviceName(CommonReq req) {
        Optional<IotDevice> deviceNew = deviceDao.findById(req.getId());

        if (deviceNew.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_DEVICE, req.getId()), ErrConst.E01);
        }

        deviceNew.get().setDeviceName(req.getName());
        deviceDao.save(deviceNew.get());
        return ServiceResult.ok();
    }

    @Override
    public ServiceResult detail(String id) {

        Optional<DeviceWorkingMode> workingModeOp = workingModeDao.findById(id);
        if (workingModeOp.isEmpty()) {
            return ServiceResult.error("id有误,模式不存在");
        }
        DeviceWorkingMode workingMode = workingModeOp.get();

        // 详情返回对象
        ShowRoomWorkingModeDetailResp showRoomWorkingModeDetailResp = new ShowRoomWorkingModeDetailResp();
        showRoomWorkingModeDetailResp.setId(workingMode.getId());
        showRoomWorkingModeDetailResp.setImgUrl(workingMode.getImg());
        showRoomWorkingModeDetailResp.setName(workingMode.getName());
        showRoomWorkingModeDetailResp.setSpaceId(workingMode.getSpace().getId());
        showRoomWorkingModeDetailResp.setSpaceName(workingMode.getSpace().getName());
        showRoomWorkingModeDetailResp.setIsDefault(workingMode.getIsDefault());
        showRoomWorkingModeDetailResp.setShowroomCentralize(workingMode.getShowroomCentralize());
        showRoomWorkingModeDetailResp.setPurpose(workingMode.getPurpose());
        // 模式下设备
        List<ShowRoomDeviceModeStatusDetailResp> detailResps = new ArrayList<>();
        List<DeviceModeStatus> modeStatusList = modeStatusDao.findByModeId(workingMode.getId());
        modeStatusList.forEach(it -> {

            ShowRoomDeviceModeStatusDetailResp statusDetailResp = new ShowRoomDeviceModeStatusDetailResp();
            IotDevice device = it.getDevice();
            statusDetailResp = JSON.parseObject(it.getProperties(), ShowRoomDeviceModeStatusDetailResp.class);
            statusDetailResp.setId(device.getId());
            statusDetailResp.setDeviceName(device.getDeviceName());
            statusDetailResp.setDeviceType(device.getDeviceType());
            statusDetailResp.setDeviceId(device.getHwDeviceId());
            detailResps.add(statusDetailResp);
        });

        showRoomWorkingModeDetailResp.setDetailResps(detailResps);

        return ServiceResult.ok(showRoomWorkingModeDetailResp);
    }

    /***---
     * 获取当前空间的下层空间的所有设备--**/
    @Override
    public List<IotDeviceResp> search(String spaceId) {


        List<Space> sonSpaceList = spaceDao.findByParentId(spaceId);
        List<IotDevice> titleDevice = new ArrayList<>();


        List<IotDevice> headList = iotDeviceDao.findBySpaceId(spaceId);
        if (!CollectionUtils.isEmpty(headList)) {
            titleDevice.addAll(headList);
        }
        if (!CollectionUtils.isEmpty(sonSpaceList)) {
            for (Space spaceTemp : sonSpaceList) {
                List<IotDevice> tempList = iotDeviceDao.findBySpaceId(spaceTemp.getId());
                if (!CollectionUtils.isEmpty(tempList)) {
                    titleDevice.addAll(tempList);
                }
            }
        }
        //剔除tv设备
        List<IotDeviceResp> resultDevice = new ArrayList<>();
        if (!CollectionUtils.isEmpty(titleDevice)) {
            for (IotDevice device : titleDevice) {
                if (!device.getDeviceType().equals(DeviceType.SCREEN)) {
                    resultDevice.add(IotDeviceResp.convert(device));
                }
            }
        }
        return resultDevice;
    }

    @Autowired
    private IOfficeRedisService iOfficeRedisService;

    @Override
    public ServiceResult delette(String id) {
        Optional<DeviceWorkingMode> workingModeOp = workingModeDao.findById(id);
        if (!workingModeOp.isPresent()) {
            return ServiceResult.error("此空间下该模式不存在");
        }
        // 工作模式
        DeviceWorkingMode workingMode = workingModeOp.get();
        workingMode.setIsDelete(1);

        // 保存

        List<DeviceModeStatus> delModeStatus = modeStatusDao.findByModeId(workingMode.getId());
        if (!CollectionUtils.isEmpty(delModeStatus)) {
            for (DeviceModeStatus modeStatus : delModeStatus) {
                modeStatus.setIsDelete(1);
                modeStatusDao.save(modeStatus);
            }
        }
        workingModeDao.save(workingMode);
        IntelligentOffice office = workingMode.getIntelligentOffice();
        List<DeviceWorkingMode> reultModes = workingModeDao.findByIntelligentOfficeId(office.getId());
        if (CollectionUtils.isEmpty(reultModes)) {
            office.setIsDelete(1);
            officeDao.save(office);
            iOfficeRedisService.delIntelligentOfficeRedis(office);
        }

        return ServiceResult.ok();
    }
}
