package cn.com.cowain.ibms.service.office;

import cn.com.cowain.ibms.enumeration.iot.IntelligentOfficeStatus;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.office.DeviceNameEditReq;
import cn.com.cowain.ibms.rest.req.office.DeviceNameResp;
import cn.com.cowain.ibms.rest.req.office.IntelligentOfficeCreateReq;
import cn.com.cowain.ibms.rest.req.office.IntelligentOfficePageReq;
import cn.com.cowain.ibms.rest.resp.office.IntelligentOfficePageResp;
import cn.com.cowain.ibms.rest.resp.office.UserOfficeResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;

import java.util.List;

public interface IntelligentOfficeService {

    // 创建智能办公室
    ServiceResult create(IntelligentOfficeCreateReq req);

    // 更新智能办公室状态
    ServiceResult updateStatus(String id, IntelligentOfficeStatus status);

    // 智能办公室删除
    ServiceResult delete(String id);

    // 查询智能办公室列表分页
    PageBean<IntelligentOfficePageResp> page(IntelligentOfficePageReq req);

    // 智能办公室详情
    ServiceResult detail(String id);

    // 智能办公室预览
    ServiceResult detailH5(String id);

    // 用户办公室列表（分页）
    PageBean<UserOfficeResp> userOfficePage(int page, int size);

    /**
     * 更新设备别名
     *
     * @param userId   设备所属用户ID
     * @param deviceId 设备ID
     * @param req      修改参数
     * @return 更改结果
     * @author Yang.Lee
     * @date 2022/2/9 15:31
     **/
    ServiceResult updateDeviceAlias(String userId, String deviceId, DeviceNameEditReq req);

    /**
     * 查询办公室设备名称列表（只显示启用设备）
     *
     * @param officeId 办公室ID
     * @param userId   用户ID
     * @return 设备列表（若用户不含有设备权限，则返回空集合）
     * @author Yang.Lee
     * @date 2022/2/10 13:22
     **/
    List<DeviceNameResp> getDeviceNameList(String officeId, String userId);
}
