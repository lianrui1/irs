package cn.com.cowain.ibms.utils;

import com.alibaba.fastjson.JSON;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ReflectionUtils;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Log4j2
public class DataUtils {
    public static <T>T invokeEnum(String enumName,Class<?> clazz) throws ClassNotFoundException {

        Object[] enumConstants = clazz.getEnumConstants();

        Optional<Object> select = Arrays.stream(enumConstants)
                .filter(item -> item.toString().equalsIgnoreCase(enumName))
                .findFirst();

        return (T) select.orElse(null);
    }
//    public static <T>T invokeClass(Class<?> clazz) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
//
//        Field[] fields = clazz.getDeclaredFields();
//
//            T t = (T) clazz.getDeclaredConstructor().newInstance();
//            for (Field field : fields) {
//                    field.setAccessible(true);
//                    Type fieldType=field.getGenericType();
//                    //log.info("字段的类型是：{}",fieldType.getTypeName());
//                    if(StringUtils.equals(fieldType.getTypeName(),"java.lang.String")){
//                        field.set(t, deviceMap.get(field.getName()));
//                    }
//                    else if(StringUtils.equals(fieldType.getTypeName(),"int")){
//                        field.set(t, Integer.valueOf((String) deviceMap.get(field.getName())));
//                    }
//                    else if(StringUtils.equals(fieldType.getTypeName(),"java.lang.Double")){
//                        field.set(t, Double.valueOf((String) deviceMap.get(field.getName())));
//                    }
//                    else if(StringUtils.equals(fieldType.getTypeName(),"java.lang.Boolean")){
//                        field.set(t, Boolean.valueOf((String) deviceMap.get(field.getName())));
//                    }
//                    else if(field.getType().isEnum()){
//                        T enumObject= invokeEnum((String) deviceMap.get(field.getName()),field.getType());
//                        field.set(t,enumObject);
//                    }
//
//        return (T) select.orElse(null);
//    }}
    public  static <T> T mapCastTargetObject(Map<Object, Object> deviceMap, Class<T> deviceDetailRespClass) {
        Field[] fields = deviceDetailRespClass.getDeclaredFields();
        try {
            T t = deviceDetailRespClass.getDeclaredConstructor().newInstance();
            for (Field field : fields) {
                if (deviceMap.containsKey(field.getName())) {
                    field.setAccessible(true);
                    Type fieldType=field.getGenericType();
                    //log.info("字段的类型是：{}",fieldType.getTypeName());
                    if(StringUtils.equals(fieldType.getTypeName(),"java.lang.String")){
                        field.set(t, deviceMap.get(field.getName()));
                    }
                    else if(StringUtils.equals(fieldType.getTypeName(),"int")){
                        field.set(t, Integer.valueOf(String.valueOf(deviceMap.get(field.getName()))));
                    }
                    else if(StringUtils.equals(fieldType.getTypeName(),"java.lang.Integer")){
                        field.set(t, Integer.valueOf(String.valueOf(deviceMap.get(field.getName()))));
                    }
                    else if(StringUtils.equals(fieldType.getTypeName(),"java.lang.Double")){
                        field.set(t, Double.valueOf(String.valueOf(deviceMap.get(field.getName()))));
                    }
                    else if(StringUtils.equals(fieldType.getTypeName(),"java.lang.Boolean")){
                        field.set(t, Boolean.valueOf(String.valueOf(deviceMap.get(field.getName()))));
                    }
                    else if(field.getType().isEnum()){
                       T enumObject= invokeEnum(String.valueOf(deviceMap.get(field.getName())),field.getType());
                        field.set(t,enumObject);
                    }
                    else if(StringUtils.equals(fieldType.getTypeName(),"java.lang.Long")||StringUtils.equals(fieldType.getTypeName(),"long")){
                        field.set(t, Long.valueOf(String.valueOf(deviceMap.get(field.getName()))));
                    }
                    else {
                       // T object= (T) field.getType().getDeclaredConstructor().newInstance();
                        T object= (T) JSON.toJavaObject((JSON) deviceMap.get(field.getName()),field.getType());
                        field.set(t,object);
                    }

                }
            }
            return t;
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException | ClassNotFoundException e) {
            log.info("对象{}转换异常", deviceDetailRespClass.getName());
            e.printStackTrace();
            return null;
        }
    }
    public static <T> Map<String, Object> targetObjectCastMap( T t, Class<T> deviceDetailRespClass) {
        Map<String, Object> objectObjectMap=new ConcurrentHashMap<>();
        Field[] fields = deviceDetailRespClass.getDeclaredFields();
        try {
           // T t = deviceDetailRespClass.getDeclaredConstructor().newInstance();

            for (Field field : fields) {
                PropertyDescriptor pd= new PropertyDescriptor(field.getName(), deviceDetailRespClass);

                Method getMethod= pd.getReadMethod();
                Object fieldValue = ReflectionUtils.invokeMethod(getMethod, t);
                if (fieldValue == null) {
                    continue;

                }
                if(StringUtils.equals(field.getName(),"temperature")){
                    objectObjectMap.put(field.getName(),fieldValue+"");
                }
                else {
                    objectObjectMap.put(field.getName(),fieldValue);
                }
            }
            return objectObjectMap;
        } catch (IntrospectionException e) {
            log.info("对象{}转换异常", deviceDetailRespClass.getName());
            e.printStackTrace();
            return objectObjectMap;
        }
    }
}
