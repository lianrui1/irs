package cn.com.cowain.ibms.service;

import cn.com.cowain.ibms.service.bean.ServiceResult;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/7/13 13:55
 */
public interface ExcelService {

    /**
     * 读取员工组相关excel，默认只读取第一个sheet。单元格格式固定为：
     * 第一行默认为员工组名称。第二行为员工姓名，第三行为员工工号。
     *
     * @param file excel文件
     * @return <p>
     * 如果读取成功，则ServiceResult.getObject()可以得到一个List<String[]>集合，
     * 集合中每一个元素表示excel中的一行。String[]对象表示对应行中的每一个单元格的文本内容
     * ，第一个元素为人员组名称（可以为空），第二个元素为员工姓名，第三个元素为员工工号
     * </p>。
     * <p>
     * 如果读取失败，则根据errorCode进行判断,如果errorCode="EXCEL_CELL_FORMAT_ERROR"(ErrCont.E22)，
     * 则ServiceResult.getObject()返回一个List<String[]>，数据同成功后的返回结果。如果
     * errorCode为其他值，则ServiceResult.getObject()
     * 返回一个字符串，描述错误原因
     * </p>
     * @author Yang.Lee如果errorCode
     * @date 2021/7/13 13:56
     **/
    ServiceResult readStaffGroupExcel(MultipartFile file);


    /**
     * 创建上传人员组失败数据的excel文件
     *
     * @param dataList 数据。集合中每一个元素表示excel中的一行。String[]对象表示对应行中的
     *                 每一个单元格的文本内容，第一个元素为人员组名称（可以为空），第二个元
     *                 素为员工姓名，第三个元素为员工工号，第四个元素为错误信息
     * @return 文件二进制数组。 如果无数据则返回一个长度为0的数组
     * @author Yang.Lee
     * @date 2021/7/14 15:30
     **/
    byte[] createStaffGroupErrorExcel(List<String[]> dataList);

    /**
     *
     *  读取通行人员信息excel
     *  第一行为姓名  第二行为工号  第三行为通行生效时间   第四行为通行结束时间
     **/
    ServiceResult readAccessRulesExcel(MultipartFile file);

    /**
     *
     *  读取添加人员信息excel
     *  第一行为姓名  第二行为工号 第三行为人员类型 第四行为通行生效时间   第五行为通行结束时间
     **/
    ServiceResult readAddAccessRulesUserExcel(MultipartFile file);

    /**
     *
     *  读取删除人员信息excel
     *  第一行为姓名  第二行为工号
     **/
    ServiceResult readDeleteAccessRulesExcel(MultipartFile file);

    /**
     *  yanzy
     *  读取删除人员信息excel
     *  第一行为姓名  第二行为工号  第三名员工类型
     **/
    ServiceResult readDeleteAccessRulesUserExcel(MultipartFile file);

    // 获取批量添加导入失败数据的文件二进制数组
    byte[] createUploadAccessRulesErrorExcel(List<String[]> errorList);

    // 获取批量删除导入失败数据的文件二进制数组
    byte[] createDelAccessRulesErrorExcel(List<String[]> errorList);

    // HK设备通行记录导出
    byte[] downloadHkFaceRecord(List<String[]> recordList);

    // 获取批量添加导入失败数据文件V1.27
    byte[] createUploadAccessRulesUserErrorExcel(List<String[]> errorList);


}
