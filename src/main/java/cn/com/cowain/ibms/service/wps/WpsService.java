package cn.com.cowain.ibms.service.wps;

import cn.com.cowain.ibms.enumeration.wps.WpsScene;

/**
 * @author wei.cheng
 * @date 2022/02/23 15:36
 */
public interface WpsService {
    /**
     * 构建WPS预览文档URL，具体规则可见：https://wwo.wps.cn/docs/server/access-mode/
     *
     * @param fileId   文件的唯一标识
     * @param fileExt  文件类型/扩展名/后缀（如`txt`）
     * @param token    当前登陆的token
     * @param wpsScene 使用场景
     * @return WPS预览文档的URL
     * @author wei.cheng
     */
    String buildWpsPreviewUrl(String fileId, String fileExt, String token, WpsScene wpsScene);
}
