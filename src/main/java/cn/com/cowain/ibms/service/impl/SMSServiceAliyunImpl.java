package cn.com.cowain.ibms.service.impl;

import cn.com.cowain.ibms.service.SMSService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.bean.aliyun.sms.GuestInvitationReq;
import cn.com.cowain.ibms.utils.RandomCode;
import cn.com.cowain.ibms.utils.RedisUtil;
import cn.com.cowain.ibms.utils.aliyun.SMSSender;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 短信service 阿里云实现
 *
 * @author Yang.Lee
 * @date 2021/6/30 10:27
 */
@Slf4j
@Service
public class SMSServiceAliyunImpl implements SMSService {

    @Value("${aliyun.sms.template.verCode}")
    private String pinNumberTemplateId;

    /**
     * 访客邀请短信模板ID
     **/
    @Value("${aliyun.sms.template.guestInvitation}")
    private String guestInvitationTemplateId;

    /**
     * 访客邀请卡连接地址（无域名）, 域名写死在模板里
     **/
    @Value("${aliyun.sms.guestInvitationCardUrl}")
    private String guestInvitationCardUrl;

    @Resource
    private RedisUtil redisUtil;


    /**
     * 发送会议邀请消息(异步执行)
     *
     * @param hostName       邀请人
     * @param hostMobile     邀请人手机号码
     * @param guestMobile    受邀人手机号码
     * @param invitationCode 会议邀请码
     * @param recordCode     会议邀请记录code
     * @author Yang.Lee
     * @date 2021/6/30 10:26
     **/
    @Async
    @Override
    public void asyncSendMeetingInvitationSMS(String hostName, String hostMobile, String guestMobile, String invitationCode, String recordCode) {

        GuestInvitationReq req = GuestInvitationReq.builder()
                .tel(hostMobile)
                .name(hostName)
                .vid(guestInvitationCardUrl + "?code=" + invitationCode + "&recordCode=" + recordCode)
                .build();

        SMSSender.sendSms(guestMobile, guestInvitationTemplateId, JSON.toJSONString(req));
    }

    /**
     * 同步发送短信验证码
     *
     * @param mobile 接受短信的手机号码
     * @author Yang.Lee
     * @date 2021/7/5 13:49
     **/
    @Override
    public ServiceResult syncSendPinNumber(String mobile) {

        String redisKey = redisUtil.createRealKey("sms-pin-" + mobile);

        String code = RandomCode.getRandomNum(6);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", RandomCode.getRandomNum(6));

        redisUtil.set(redisKey, code, RedisUtil.FIFTEEN_MIN_SECOND);
        SMSSender.sendSms(mobile, pinNumberTemplateId, jsonObject.toJSONString());

        return ServiceResult.ok();
    }
}
