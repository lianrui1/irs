package cn.com.cowain.ibms.service.ability.impl;

import cn.com.cowain.ibms.feign.iotc.IOTCApi;
import cn.com.cowain.ibms.rest.req.iot.GroupBindingReq;
import cn.com.cowain.ibms.rest.req.iot.PermisssionReq;
import cn.com.cowain.ibms.rest.req.iot.UpdatePermisssionReq;
import cn.com.cowain.ibms.service.ability.IotcConnectService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.sfp.comm.JsonResult;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author feng
 * @title: IotcConnectServiceImpl
 * @projectName ibms
 * @Date 2022/1/12 9:49
 */

@Slf4j
@Service
public class IotcConnectServiceImpl implements IotcConnectService {

    @Value("${iotc.bind.methodcode:configAcross}")
    private String bindMethodCode;
    @Value("${iotc.bind.modulecode:KSHT}")
    private String bindModuleCode;

    @Value("${iotc.delete.methodcode:updateConfigAcross}")
    private String deleteMethodCode;
    @Value("${iotc.delete.modulecode:KSHT}")
    private String deleteModuleCode;
    @Resource
    private IOTCApi iotcApi;

    @Override
    public ServiceResult bindingDevice(PermisssionReq paramReq, String channelType, String chainSite) {

        GroupBindingReq req = new GroupBindingReq();
        String param = JSON.toJSONString(paramReq);
        req.setJsonArg(param);
        // 方法是请求的后缀吗？例如： /across/permission
        req.setMethodCode(bindMethodCode);
        //通道类型 channelType 怎么填
        // 地点 chainSite
        // 模块 moduleCode
        req.setModuleCode(bindModuleCode);
        req.setChannelType(channelType);
        req.setChainSite(chainSite);
        String reqParam = JSON.toJSONString(req);

        log.debug("旷视鸿图绑定设备与员工组关系开始 {}", reqParam);
        JsonResult<Object> resp = iotcApi.postToConn(req);
        if (resp.getStatus() == 0) {
            log.error("旷视鸿图绑定设备与员工组关系失败，参数 ： {}， 结果：{}", req, resp);
            return ServiceResult.error(resp.getMsg(), resp.getCode());
        }
        return ServiceResult.ok();
    }

    @Override
    public String deleteBinding(UpdatePermisssionReq updateReq, String channelType, String chainSite) {
        GroupBindingReq req = new GroupBindingReq();
        String param = JSON.toJSONString(updateReq);
        req.setJsonArg(param);
        req.setChannelType(channelType);
        req.setChainSite(chainSite);
        req.setMethodCode(deleteMethodCode);
        //通道类型 channelType 怎么填
        // 地点 chainSite
        // 模块 moduleCode
        req.setModuleCode(deleteModuleCode);
        log.info(req.toString());
        JsonResult<Object> resp = iotcApi.postToConn(req);
        if (resp != null && resp.getCode() != null && resp.getCode().equals("-1")) {
            log.error("删除失败原因：" + resp.getMsg() + " code:" + resp.getCode());
            return "删除失败：" + resp.getMsg();
        }
        return "删除成功";
    }
}
