package cn.com.cowain.ibms.rest.req.working_mode;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/12/21 17:26
 */
@Data
@ApiModel("权限人员新增对象")
public class ManagementSaveReq {

    @NotEmpty
    @ApiModelProperty(value = "空间ID")
    private String spaceId;

    @NotEmpty
    @ApiModelProperty(value = "办公室ID")
    private String officeId;

    @NotEmpty
    @ApiModelProperty(value = "人员工号")
    private List<String> empNos;
}
