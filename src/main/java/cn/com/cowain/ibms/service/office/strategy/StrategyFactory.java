package cn.com.cowain.ibms.service.office.strategy;

import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.service.office.strategy.deviceType.*;

import java.util.HashMap;
import java.util.Map;

public class StrategyFactory {
    private static final Map<String, DeviceTypeStrategy> DEVICE_TYPE_STRATEGY_MAP = new HashMap<String, DeviceTypeStrategy>();
    private static final DeviceTypeStrategy deviceTypeDefaultStrategy = new DeviceTypeDefaultStrategy();

    static {
        DEVICE_TYPE_STRATEGY_MAP.put(DeviceTypeKey.AIR_CONDITIONER, new AirConditionerStrategy());
        DEVICE_TYPE_STRATEGY_MAP.put(DeviceTypeKey.CURTAINS, new CurtainsStrategy());
        DEVICE_TYPE_STRATEGY_MAP.put(DeviceTypeKey.SENSOR_BODY_SENSING, new SensorBodySensingStrategy());
        DEVICE_TYPE_STRATEGY_MAP.put(DeviceTypeKey.FOUR_WAY_SWITCH, new FourWaySwitchStrategy());
        DEVICE_TYPE_STRATEGY_MAP.put(DeviceTypeKey.DOOR_MAGNETIC, new DoorMagneticStrategy());
        DEVICE_TYPE_STRATEGY_MAP.put(DeviceTypeKey.DEFAULT, new DeviceTypeDefaultStrategy());
        DEVICE_TYPE_STRATEGY_MAP.put(DeviceTypeKey.ILLUMINATION_SENSING, new SensorBodySensingStrategy());
        DEVICE_TYPE_STRATEGY_MAP.put(DeviceTypeKey.TEMPERATURE_AND_HUMIDITY_SENSOR, new SensorBodySensingStrategy());
        DEVICE_TYPE_STRATEGY_MAP.put(DeviceTypeKey.TEMPERATURE_AND_HUMIDITY_AND_ILLUMINATION_SENSOR, new SensorBodySensingStrategy());
        DEVICE_TYPE_STRATEGY_MAP.put(DeviceTypeKey.NOISE_SENSOR, new SensorBodySensingStrategy());
        DEVICE_TYPE_STRATEGY_MAP.put(DeviceTypeKey.PM_SENSOR, new SensorBodySensingStrategy());

    }

    private StrategyFactory() {
    }


    public static DeviceTypeStrategy getDeviceTypeStrategy(String deviceTypeName) {
        DeviceTypeStrategy deviceTypeStrategy = DEVICE_TYPE_STRATEGY_MAP.get(deviceTypeName);
        return deviceTypeStrategy == null ? deviceTypeDefaultStrategy : deviceTypeStrategy;
    }

    private interface DeviceTypeKey {
        String AIR_CONDITIONER = String.valueOf(DeviceType.AIR_CONDITIONER);//空调
        String CURTAINS = String.valueOf(DeviceType.CURTAINS);//窗帘
        String SENSOR_BODY_SENSING = String.valueOf(DeviceType.SENSOR_BODY_SENSING);//人体传感器
        String FOUR_WAY_SWITCH = String.valueOf(DeviceType.FOUR_WAY_SWITCH);//四路开关
        String DOOR_MAGNETIC = String.valueOf(DeviceType.DOOR_MAGNETIC);//门磁
        String DEFAULT = String.valueOf(DeviceType.DEFAULT);//
        String ILLUMINATION_SENSING = String.valueOf(DeviceType.ILLUMINATION_SENSING);//
        String TEMPERATURE_AND_HUMIDITY_SENSOR = String.valueOf(DeviceType.TEMPERATURE_AND_HUMIDITY_SENSOR);//
        String TEMPERATURE_AND_HUMIDITY_AND_ILLUMINATION_SENSOR = String.valueOf(DeviceType.TEMPERATURE_AND_HUMIDITY_AND_ILLUMINATION_SENSOR);//
        String NOISE_SENSOR = String.valueOf(DeviceType.NOISE_SENSOR);//
        String PM_SENSOR = String.valueOf(DeviceType.PM_SENSOR);//

    }
}
