package cn.com.cowain.ibms.service.department;

import cn.com.cowain.ibms.rest.resp.department.DepartmentStaffPageResp;
import cn.com.cowain.ibms.rest.resp.department.DepartmentStaffPageRootResp;
import cn.com.cowain.ibms.rest.resp.department.DepartmentTreeResp;

/**
 * 组织部门Service
 *
 * @author Yang.Lee
 * @date 2021/2/5 10:42
 */
public interface DepartmentService {

    /**
     * 获取组织部门树
     *
     * @return 组织部门树
     */
    DepartmentTreeResp getDepartmentTree();

    /**
     * 获取部门下员工数据
     *
     * @param departmentId 部门ID
     * @return 部门下员工列表（分页）
     */
    DepartmentStaffPageResp getDepartmentStaffPage(long departmentId);

    /**
     * 模糊查询员工信息
     *
     * @param name 姓名关键字
     * @return 员工数据
     */
    DepartmentStaffPageRootResp searchStaff(String name);
}
