package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.enumeration.ApprovalStatus;
import cn.com.cowain.ibms.enumeration.iot.AccessControlSpotType;
import cn.com.cowain.ibms.enumeration.iot.AccessPermissionDistributionStatus;
import cn.com.cowain.ibms.service.space.SpaceService;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * @author Yang.Lee
 * @date 2022/3/8 11:00
 */
@Data
@Entity
@Table(name = "iot_access_control_spot_application_record")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_access_control_spot_application_record", comment = "门禁点审批记录表")
public class AccessControlSpotApplicationRecord extends BaseEntity {

    @Column(name = "applicant_hr_id", nullable = false, columnDefinition = "varchar(30) COMMENT '申请人工号'")
    private String applicantHrId;

    @Column(name = "applicant_name", nullable = false, columnDefinition = "varchar(30) COMMENT '申请人姓名'")
    private String applicantName;

    @Column(name = "applicant_department_code", columnDefinition = "varchar(50) COMMENT '申请人部门编号'")
    private String applicantDepartmentCode;

    @Column(name = "applicant_department_name", columnDefinition = "varchar(50) COMMENT '申请人部门名称'")
    private String applicantDepartmentFullName;

    @Column(name = "face_img", columnDefinition = "varchar(500) COMMENT '人脸图片'")
    private String faceImg;

    @Column(name = "approver_hr_id", columnDefinition = "varchar(30) COMMENT '审批人工号'")
    private String approverHrId;

    @Column(name = "approver_name", columnDefinition = "varchar(30) COMMENT '审批人姓名'")
    private String approverName;

    @Column(name = "approver_department_name", columnDefinition = "varchar(50) COMMENT '审批人部门名称'")
    private String approverDepartmentFullName;

    @Column(name = "approver_time", columnDefinition = "datetime COMMENT '审批时间'")
    private LocalDateTime approverTime;

    @Column(name = "access_start_time", columnDefinition = "datetime COMMENT '权限开始时间'")
    private LocalDateTime accessStartTime;

    @Column(name = "access_end_time", columnDefinition = "datetime COMMENT '权限结束时间'")
    private LocalDateTime accessEndTime;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false, columnDefinition = "varchar(20) COMMENT '审批状态'")
    private ApprovalStatus status;

    @Enumerated(EnumType.STRING)
    @Column(name = "access_permission_distribution_status", columnDefinition = "varchar(20) COMMENT '门禁点权限下发状态'")
    private AccessPermissionDistributionStatus accessPermissionDistributionStatus;

    @Column(name = "denied_reason", columnDefinition = "varchar(500) COMMENT '拒绝原因'")
    private String deniedReason;

    @Column(name = "spot_id", nullable = false, columnDefinition = "varchar(36) COMMENT '门禁点ID'")
    private String spotId;

    @Column(name = "spot_name", nullable = false, columnDefinition = "varchar(50) COMMENT '门禁点名称'")
    private String spotName;

    @Enumerated(EnumType.STRING)
    @Column(name = "spot_type", columnDefinition = "varchar(20) COMMENT '门禁点类型'")
    private AccessControlSpotType accessControlSpotType;

    @Column(name = "spot_address", columnDefinition = "varchar(500) COMMENT '门禁点位置'")
    private String spotAddress;

    /**
     * 构建申请
     *
     * @param faceImg
     * @param applicant
     * @param accessControlSpot
     * @return
     */
    public static AccessControlSpotApplicationRecord buildAccessControlSpotApplicationRecord(String faceImg, SysUser applicant,
                                                                                             AccessControlSpot accessControlSpot) {
        AccessControlSpotApplicationRecord applicationRecord = new AccessControlSpotApplicationRecord();
        applicationRecord.setApplicantHrId(applicant.getEmpNo());
        applicationRecord.setApplicantName(applicant.getNameZh());
        applicationRecord.setApplicantDepartmentCode(applicant.getDeptId());
        applicationRecord.setApplicantDepartmentFullName(applicant.getFullDepartmentName());
        applicationRecord.setFaceImg(faceImg);
        applicationRecord.setStatus(ApprovalStatus.PENDING);
        applicationRecord.setSpotId(accessControlSpot.getId());
        applicationRecord.setSpotName(accessControlSpot.getName());
        applicationRecord.setAccessControlSpotType(accessControlSpot.getAccessControlSpotType());
        applicationRecord.setSpotAddress(getSpotAddress(accessControlSpot));
        return applicationRecord;
    }

    /**
     * 获取门禁地址
     *
     * @param accessControlSpot
     * @return
     */
    public static String getSpotAddress(AccessControlSpot accessControlSpot) {
        if (Objects.isNull(accessControlSpot.getSpace())) {
            return accessControlSpot.getAddress();
        }
        if (Objects.isNull(accessControlSpot.getSpace().getProject())) {
            return SpaceService.getFullSpaceName(accessControlSpot.getSpace(), accessControlSpot.getSpace().getName()) + " / " + accessControlSpot.getAddress();
        } else {
            return accessControlSpot.getSpace().getProject().getProjectName() + " / " + SpaceService.getFullSpaceName(accessControlSpot.getSpace(),
                    accessControlSpot.getSpace().getName()) + " / " + accessControlSpot.getAddress();
        }
    }
}
