package cn.com.cowain.ibms.rest.resp.iot;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/18 15:43
 */
@Data
@ApiModel("用户列表分页响应对象")
public class AccessControlPageResp {

    @ApiModelProperty(value = "人员id")
    private String userId;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "工号")
    private String empNo;

    @ApiModelProperty(value = "部门")
    private String dept;

    @ApiModelProperty(value = "人脸照片")
    private String faceUrl;

    @ApiModelProperty(value = "人员类型0员工 1访客")
    private Integer userType;

    @ApiModelProperty(value = "门禁数")
    private Integer accessControlNum;

    @ApiModelProperty(value = "权限来源")
    private String source = "";

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "开始时间", required = true, example = "2099-01-01 12:34", position = 2)
    private LocalDateTime startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "结束时间", required = true, example = "2099-01-01 12:34", position = 3)
    private LocalDateTime endTime;

    @ApiModelProperty(value = "时间计划")
    private AccessControlTimePlanResp timePlan;
}
