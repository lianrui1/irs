package cn.com.cowain.ibms.service.bean.amap.geo;

import lombok.Data;

/**
 * 道路信息
 * @author Yang.Lee
 * @date 2021/4/20 10:25
 */
@Data
public class Road {

    /**
     * 道路id
     **/
    private String id;

    /**
     * 道路名称
     **/
    private String name;

    /**
     *道路到请求坐标的距离,单位：米
     **/
    private String distance;

    /**
     * 方位, 输入点和此路的相对方位
     **/
    private String direction;

    /**
     * 坐标点
     **/
    private String location;
}
