package cn.com.cowain.ibms.entity.distinguish;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/6 11:15
 */
public enum DeviceSource {

    /**
     * 旷世
     */
    KUANGSHI("旷世"),

    /**
     * 海康
     */
    HAIKANG("海康");

    DeviceSource(String name){
        this.name = name;
    }
    private String name;

    public String getName(){
        return this.name;
    }
}
