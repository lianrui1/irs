package cn.com.cowain.ibms.rest.req.meeting;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/9 10:07
 */
@Data
public class InvitationDetailReq {

    // 邀请函单号
    private String visitorRecordNo;
}
