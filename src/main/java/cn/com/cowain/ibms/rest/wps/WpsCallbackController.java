package cn.com.cowain.ibms.rest.wps;

/**
 * @author wei.cheng
 * @date 2022/02/22 14:35
 */

import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.req.wps.WpsOnnotifyReq;
import cn.com.cowain.ibms.rest.resp.wps.FileInfoResp;
import cn.com.cowain.ibms.rest.resp.wps.WpsSaveFileResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.wps.WpsCallbackService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * 接受WPS开放平台回调请求
 * 详情见：https://open.wps.cn/docs/app-file/callback
 *
 * @author wei.cheng
 * @date 2022/02/22 14:37
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/wps/callback")
@Api(tags = {IConst.MODULE_WPS, IConst.V25})
public class WpsCallbackController {
    @Autowired
    private WpsCallbackService wpsCallbackService;

    @GetMapping("/v1/3rd/file/info")
    @ApiOperation(value = "WPS获取文件元数据", tags = {IConst.MODULE_WPS, IConst.V25})
    public ResponseEntity<?> getFileInfo(
            @RequestParam(name = "_w_appid") @ApiParam(value = "应用id", required = true) String appId,
            @RequestParam(name = "_w_fileid") @ApiParam(value = "文件id,字符串长度小于40", required = true) String fileId,
            @RequestParam(name = "_w_token", required = false) @ApiParam(value = "用户token") String token,
            @RequestParam(name = "_w_scene") @ApiParam(value = "使用场景", required = true) String scene) {
        ServiceResult result = wpsCallbackService.getFileInfo(appId, fileId, token, scene);
        if (!result.isSuccess()) {
            return ResponseEntity.status(40005).body(result);
        } else {
            return ResponseEntity.ok(result.getObject());
        }
    }

    @PostMapping("/v1/3rd/onnotify")
    @ApiOperation(value = "打开或关闭文件时，WPS回调事件通知", tags = {IConst.MODULE_WPS, IConst.V25})
    public ResponseEntity<JsonResult<String>> onnotify(
            @RequestParam(name = "_w_appid") @ApiParam(value = "应用id", required = true) String appId,
            @RequestParam(name = "_w_fileid") @ApiParam(value = "文件id,字符串长度小于40", required = true) String fileId,
            @RequestParam(name = "_w_token", required = false) @ApiParam(value = "用户token") String token,
            @RequestParam(name = "_w_scene") @ApiParam(value = "使用场景", required = true) String scene,
            @RequestBody @Validated @ApiParam(value = "requestBody", required = true) WpsOnnotifyReq req,
            BindingResult bindingResult) {
        JsonResult<String> jsonResult;
        ServiceResult result = wpsCallbackService.onNotify(appId, fileId, token, scene, req);
        if (!result.isSuccess()) {
            jsonResult = JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01);
        } else {
            jsonResult = JsonResult.ok("ok", (String) result.getObject());
        }
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 目前暂无需要保存在线编辑后的文件的需求，该API无保存更新后的文件功能
     *
     * @param fileId
     * @param version
     * @param file
     * @return
     */
    @PostMapping("/v1/3rd/file/save")
    @ApiOperation(value = "保存新版本文件，WPS回调事件通知", tags = {IConst.MODULE_WPS, IConst.V25})
    public ResponseEntity<WpsSaveFileResp> saveFile(@RequestParam(name = "_w_fileid") String fileId,
                                                    @RequestHeader(name = "x-weboffice-save-version") Long version,
                                                    @RequestParam("file") MultipartFile file) {
        log.info("start to save file by wps, fileId:{}, version:{}", fileId, version);
        WpsSaveFileResp resp = new WpsSaveFileResp();
        FileInfoResp fileInfoResp = new FileInfoResp();
        fileInfoResp.setId(fileId);
        fileInfoResp.setName(file.getOriginalFilename());
        fileInfoResp.setVersion(version + 1);
        fileInfoResp.setSize(file.getSize());
        resp.setFile(fileInfoResp);
        return ResponseEntity.ok(resp);
    }
}
