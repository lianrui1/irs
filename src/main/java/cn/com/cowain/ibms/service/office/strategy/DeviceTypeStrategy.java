package cn.com.cowain.ibms.service.office.strategy;

import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.rest.resp.device.OfficeCommonDeviceResp;
import cn.com.cowain.ibms.rest.resp.device.centralized.CentralizedControlDeviceFilter;
import cn.com.cowain.ibms.rest.resp.office.ShowDeviceResp;

import java.util.List;

public interface DeviceTypeStrategy {
    void  addCommonDeviceResp  (ShowDeviceResp showDeviceResp, List<OfficeCommonDeviceResp> commonDeviceRespList);

    void filterDevicePropertiesStatus(IotDevice iotDevice, CentralizedControlDeviceFilter controlDeviceFilter, String targetAction);

    default boolean isOpen(IotDevice iotDevice){
        return false;
    }

    default DeviceStatus getNewestStatus(IotDevice iotDevice){
        return null;
    }
}
