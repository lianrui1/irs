package cn.com.cowain.ibms.service.meeting;

import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.rest.req.meeting.CustomUserGroupEditReq;
import cn.com.cowain.ibms.service.bean.ServiceResult;

import java.util.List;

public interface CustomUserGroupService {

    // 新建用户组
    ServiceResult save(CustomUserGroupEditReq req, String hrId);

    // 查询用户组信息
    ServiceResult get(String id, String hrId);

    // 更新用户组
    ServiceResult update(CustomUserGroupEditReq req, String id);

    // 删除用户组
    ServiceResult delete(String id);

    // 查询用户组列表
    ServiceResult list(String hrId);

    void delLeave(List<SysUser> sysUsers1);
}
