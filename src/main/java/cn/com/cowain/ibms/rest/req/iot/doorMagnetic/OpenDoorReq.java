package cn.com.cowain.ibms.rest.req.iot.doorMagnetic;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * @author Yang.Lee
 * @date 2022/7/13 13:17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("门磁2.0开门请求参数")
public class OpenDoorReq {

    @NotBlank
    @ApiModelProperty(value = "用户ucId", example = "123abcABC", required = true, position = 1)
    private String uid;

    @ApiModelProperty(value = "门牌ID，动态二维码时必传", example = "123abcABC", position = 2)
    private String doorPlateId;
}
