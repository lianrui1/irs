package cn.com.cowain.ibms.entity.meal;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.enumeration.iot.AccessControlSpotPlace;
import cn.com.cowain.ibms.enumeration.meal.MealStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 报餐人员
 *
 * @author Yang.Lee
 * @date 2022/1/10 15:40
 */
@Data
@Entity
@Table(name = "ibms_meal_person")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_meal_person", comment = "IBMS 报餐人员")
@Where(clause = "is_del=0")
public class MealPerson extends BaseEntity {

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false, columnDefinition = "varchar(50) COMMENT '下发状态'")
    private MealStatus status;

    @Enumerated(EnumType.STRING)
    @Column(name = "place", nullable = false, columnDefinition = "varchar(500) COMMENT '报餐地址'")
    private AccessControlSpotPlace place;

    /**
     * 报餐开始时间
     */
    @Column(name = "start_time", nullable = false, columnDefinition = "datetime COMMENT '报餐开始时间'")
    private LocalDateTime startTime;

    /**
     * 报餐结束时间
     */
    @Column(name = "end_time", nullable = false, columnDefinition = "datetime COMMENT '报餐结束时间'")
    private LocalDateTime endTime;


    /**
     * 工号
     */
    @Column(name = "work_no", nullable = false, columnDefinition = "varchar(500)  COMMENT '工号'")
    private String workNo;


    /**
     * 失败信息
     */
    @Column(name = "fail_msg", columnDefinition = "varchar(500)  COMMENT '失败信息'")
    private String failMsg;
}
