package cn.com.cowain.ibms.entity.meeting;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Yang.Lee
 * @date 2021/12/27 10:49
 */
@Data
@Entity
@Table(name = "ibms_intelligent_meeting_app_mode")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "ibms_intelligent_meeting_app_mode", comment = "会议大屏模式表")
public class IntelligentMeetingAppMode extends BaseEntity {

    @Column(name = "name", nullable = false, columnDefinition = "varchar(50) COMMENT '工作模式名称'")
    private String name;

    @Column(name = "img", nullable = false, columnDefinition = "varchar(100) COMMENT '工作模式图标'")
    private String img;

    @Column(name = "purpose", columnDefinition = "varchar(50) COMMENT '工作模式用途'")
    private String purpose;
}
