package cn.com.cowain.ibms.rest.req.meeting;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/8 10:16
 */
@Data
public class UploadAndCheckFaceReq {

    @ApiModelProperty(value = "受邀者照片", required = true, example = "xxxxxxxx", position = 3)
    private MultipartFile file;
}
