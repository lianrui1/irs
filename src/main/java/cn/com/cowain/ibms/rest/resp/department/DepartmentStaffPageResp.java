package cn.com.cowain.ibms.rest.resp.department;

import cn.com.cowain.ibms.rest.resp.StaffResp;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 部门员工数据返回对象
 *
 * @author Yang.Lee
 * @date 2021/2/8 9:21
 */
@Data
@ApiModel("部门员工分页数据")
public class DepartmentStaffPageResp implements Serializable {

    private static final long serialVersionUID = -2332990526448186927L;

    @ApiModelProperty(value = "记录总数", required = true, example = "20")
    private Integer totalCount;

    @ApiModelProperty(value = "每页记录数", required = true, example = "10", position = 1)
    private Integer pageSize;

    @ApiModelProperty(value = "总页数", required = true, example = "2", position = 2)
    private Integer totalPage;

    @ApiModelProperty(value = "当前页码", required = true, example = "1", position = 3)
    private Integer currPage;

    @ApiModelProperty(value = "员工列表", required = true, position = 4)
    private List<StaffResp> list;
}
