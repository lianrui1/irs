package cn.com.cowain.ibms.rest.iotc;

import cn.com.cowain.ibms.feign.bean.iotc.resp.ApplicationResp;
import cn.com.cowain.ibms.service.iotc.ApplicationService;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author Yang.Lee
 * @date 2021/10/20 14:11
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/iotc/application")
@Api(tags = IConst.MODULE_IOTC_APPLICATION)
public class ApplicationController {

    @Resource
    private ApplicationService applicationService;

    /**
     * 获取应用列表
     * @return
     */
    @GetMapping("/list")
    @ApiOperation(value = "获取应用列表", tags = {IConst.MODULE_IOTC_APPLICATION, IConst.V17})
    public ResponseEntity<JsonResult<List<ApplicationResp>>> getProvinceList(String name) {

        return ResponseEntity.ok(JsonResult.ok("OK", applicationService.getList(name)));
    }
}
