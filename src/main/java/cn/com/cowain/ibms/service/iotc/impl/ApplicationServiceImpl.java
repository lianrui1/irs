package cn.com.cowain.ibms.service.iotc.impl;

import cn.com.cowain.ibms.dao.iotc.ApplicationDao;
import cn.com.cowain.ibms.dao.iotc.DeviceApplicationRelationDao;
import cn.com.cowain.ibms.entity.iotc.Application;
import cn.com.cowain.ibms.entity.iotc.DeviceApplicationRelation;
import cn.com.cowain.ibms.feign.bean.iotc.resp.ApplicationResp;
import cn.com.cowain.ibms.feign.iotc.IOTCApi;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.iotc.ApplicationService;
import cn.com.cowain.sfp.comm.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Yang.Lee
 * @date 2021/10/20 13:54
 */
@Slf4j
@Service
public class ApplicationServiceImpl implements ApplicationService {

    @Resource
    private ApplicationDao applicationDao;

    @Resource
    private IOTCApi iotcApi;

    @Resource
    private DeviceApplicationRelationDao deviceApplicationRelationDao;

    /**
     * 保存应用数据数据
     *
     * @param applicationList 应用列表
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/10/20 13:54
     **/
    @Override
    @Transactional
    public ServiceResult save(List<ApplicationResp> applicationList) {

        if (CollectionUtils.isEmpty(applicationList)) {
            return ServiceResult.ok();
        }

        List<Application> iotApplicationList = applicationList.stream().map(obj -> {

            Application application = new Application();
            BeanUtils.copyProperties(obj, application);
            return application;
        }).collect(Collectors.toList());


        iotApplicationList.forEach(obj -> {
            Optional<Application> applicationOptional = applicationDao.findByName(obj.getName());

            if (applicationOptional.isPresent()) {
                BeanUtils.copyProperties(applicationOptional.get(), obj);
            }
        });

        applicationDao.saveAll(iotApplicationList);

        return ServiceResult.ok();
    }

    /**
     * 向IOTC获取应用列表
     *
     * @return 应用列表
     * @author Yang.Lee
     * @date 2021/10/20 14:02
     **/
    @Override
    public List<ApplicationResp> getApplicationListFromIOTC() {

        // 默认获取9999条数据
        JsonResult<PageBean<ApplicationResp>> applicationPageResult = iotcApi.getApplicationList(0, 9999);
        if (applicationPageResult.getStatus() == 0) {
            log.error("同步应用数据失败，IOTC返回内容： " + applicationPageResult);
            return new ArrayList<>();
        }

        return applicationPageResult.getData().getList();
    }

    /**
     * 获取本地数据库中的应用列表
     *
     * @return 应用列表
     * @author Yang.Lee
     * @date 2021/10/20 14:05
     **/
    @Override
    @Transactional
    public List<ApplicationResp> getList(String name) {

        List<Application> applicationList = applicationDao.findAll((root, query, criteriaBuilder) -> {
            List<Predicate> list = new ArrayList<>();

            if (StringUtils.isNotEmpty(name)) {
                String like = "%" + name + "%";
                list.add(criteriaBuilder.like(root.get("name"), like));
            }

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        });

        return applicationList.stream().map(obj -> {
            ApplicationResp resp = new ApplicationResp();
            BeanUtils.copyProperties(obj, resp);
            return resp;
        }).collect(Collectors.toList());
    }

    /**
     * 查询设备的应用列表
     *
     * @param deviceId 设备ID
     * @return 应用列表
     * @author Yang.Lee
     * @date 2021/10/26 11:06
     **/
    @Override
    @Transactional
    public List<ApplicationResp> getListByDevice(String deviceId) {

        List<DeviceApplicationRelation> relationList = deviceApplicationRelationDao.findByDeviceId(deviceId);
        return relationList.stream().map(DeviceApplicationRelation::getApplication).map(obj -> {
            ApplicationResp resp = new ApplicationResp();
            BeanUtils.copyProperties(obj, resp);
            return resp;
        }).collect(Collectors.toList());
    }
}
