package cn.com.cowain.ibms.service.iot;

import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.iot.AccessControlSpotApplicationRecord;
import cn.com.cowain.ibms.mq.bean.BizReturnMessage;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.iot.AccessControlSpotApplicationRecordParamReq;
import cn.com.cowain.ibms.rest.req.iot.AccessControlSpotApplicationRecordReq;
import cn.com.cowain.ibms.rest.req.iot.HandlingAccessControlSpotApplicationRecordReq;
import cn.com.cowain.ibms.rest.resp.iot.AccessControlSpotApplicationRecordResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;

import java.util.List;

/**
 * 申请分配记录service
 *
 * @author: yanzy
 * @date: 2022/3/9 10:00
 */
public interface AccessControlSpotApplicationRecordService {

    /**
     * 门禁点权限申请
     *
     * @param token
     * @param req
     * @return 返回结果
     * @author wei.cheng
     * @date 2022/3/9 4:39 下午
     **/
    ServiceResult createAccessControlSpotApplicationRecord(String token, AccessControlSpotApplicationRecordReq req);

    /**
     * 获取申请|分配记录列表（分页）
     *
     * @author: yanzy
     * @data: 2022/3/9 10:03:04
     */
    PageBean<AccessControlSpotApplicationRecordResp> getPage(AccessControlSpotApplicationRecordParamReq req);

    /**
     * 查询门禁权限申请详情
     *
     * @param id 申请ID
     * @author wei.cheng
     * @date 2022/3/10 5:43 下午
     **/
    ServiceResult getAccessControlSpotApplicationRecord(String id);

    /**
     * 门禁点权限申请审批
     *
     * @param token
     * @param id
     * @param req
     * @author wei.cheng
     * @date 2022/3/11 9:21 上午
     **/
    ServiceResult handlingAccessControlSpotApplication(String token, String id, HandlingAccessControlSpotApplicationRecordReq req);

    /**
     * 门禁点授权申请通过请求iotc权限下发回调处理
     *
     * @param message
     * @param topic
     */
    ServiceResult saveReturnMessage(BizReturnMessage message, String topic);

    /**
     * 发送通知消息
     *
     * @param applicant
     * @param applicationRecord
     * @param approver
     */
    void sendMessageToApprover(SysUser applicant, AccessControlSpotApplicationRecord applicationRecord, SysUser approver);

    /**
     * 查询用户对于门禁点的权限申请记录
     *
     * @param userHrId 用户工号
     * @param stopIds  门禁点列表
     * @return 申请记录列表
     * @author Yang.Lee
     * @date 2022/7/18 14:18
     **/
    List<AccessControlSpotApplicationRecordResp> getUserApplyList(String userHrId, String... stopIds);
}
