package cn.com.cowain.ibms.service;

import cn.com.cowain.ibms.service.bean.ServiceResult;

/**
 * 短信Service
 *
 * @author Yang.Lee
 * @date 2021/6/30 10:22
 */
public interface SMSService {

    /**
     * 发送会议邀请消息(异步执行)
     *
     * @param hostName       邀请人
     * @param hostMobile     邀请人手机号码
     * @param guestMobile    受邀人手机号码
     * @param invitationCode 会议邀请码
     * @param recordCode     会议邀请记录code
     * @author Yang.Lee
     * @date 2021/6/30 10:26
     **/
    void asyncSendMeetingInvitationSMS(String hostName, String hostMobile, String guestMobile, String invitationCode, String recordCode);

    /**
     * 异步发送短信验证码
     *
     * @param mobile 接受短信的手机号码
     * @author Yang.Lee
     * @date 2021/7/5 13:49
     **/
    ServiceResult syncSendPinNumber(String mobile);
}
