package cn.com.cowain.ibms.rest.resp.iot;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/3/23 2:30
 */
@Data
@ApiModel("梯控按人员列表响应对象")
public class ElevatorSpotUserPageResp {

    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "人员姓名")
    private String name;

    @ApiModelProperty(value = "人员工号")
    private String empNo;

    @ApiModelProperty(value = "部门ID")
    private String deptId;

    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @ApiModelProperty(value = "照片")
    private String faceUrl;

    @ApiModelProperty(value = "人员类型")
    private int isStaff;

    @ApiModelProperty(value = "权限楼层")
    private List<ProjectResp> floorList;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "通行开始时间")
    private LocalDateTime accessStartTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "通行结束时间")
    private LocalDateTime accessEndTime;
}
