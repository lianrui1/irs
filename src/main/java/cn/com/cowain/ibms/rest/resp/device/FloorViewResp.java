package cn.com.cowain.ibms.rest.resp.device;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * @Author tql
 * @Description 楼层视图 出参类
 * @Date 21-11-4 下午3:13
 * @Version 1.0
 */
@Data
@Builder
@ApiModel("楼层视图")
public class FloorViewResp {

    @ApiModelProperty(value = "空间信息列表",  example = "", position = 1)
    private List<SpaceListViewResp> spaceListViewResps;
}
