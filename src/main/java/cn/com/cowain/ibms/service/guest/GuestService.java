package cn.com.cowain.ibms.service.guest;

import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.rest.bean.ParticipantBean;
import cn.com.cowain.ibms.rest.resp.distinguish.StaffPageResp;
import cn.com.cowain.ibms.service.bean.Guest;

import java.util.List;
import java.util.Optional;

/**
 * 访客相关Service
 *
 * @author Yang.Lee
 * @date 2021/12/14 9:49
 */
public interface GuestService {

    /**
     * 根据手机号查询访客信息
     *
     * @param mobile 手机号码
     * @return 访客信息
     * @author Yang.Lee
     * @date 2021/12/14 9:51
     **/
    Optional<Guest> getByMobile(String mobile);

    /**
     * 获取访客列表
     *
     * @return 访客列表
     * @author Yang.Lee
     * @date 2021/12/14 10:48
     **/
    List<StaffPageResp> getGuestList();

    /**
     * 获取访客列表
     *
     * @param reservationRecord 会议信息
     * @return 访客列表
     * @author Yang.Lee
     * @date 2022/1/15 14:14
     **/
    List<ParticipantBean> getGuest(ReservationRecord reservationRecord);
}
