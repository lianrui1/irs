package cn.com.cowain.ibms.bo;

import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.service.bean.Guest;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

/**
 * @author wei.cheng
 * @date 2022/04/02 12:00
 */
@Data
@NoArgsConstructor
public class SimpleUserBO {
    /**
     * 工号
     */
    private String hrId;
    /**
     * 名称
     */
    private String name;
    /**
     * 手机号码
     */
    private String mobile;
    /**
     * 部门名称
     */
    private String deptName;
    /**
     * 部门ID
     */
    private String deptId;
    /**
     * 部门全称
     */
    private String fullDepartmentName;

    public static SimpleUserBO convert(SysUser sysUser) {
        if (Objects.isNull(sysUser)) {
            return null;
        }
        SimpleUserBO user = new SimpleUserBO();
        user.setHrId(sysUser.getEmpNo());
        user.setName(sysUser.getNameZh());
        user.setMobile(sysUser.getMobile());
        user.setDeptName(sysUser.getDeptName());
        user.setDeptId(sysUser.getDeptId());
        user.setFullDepartmentName(sysUser.getFullDepartmentName());
        return user;
    }

    public static SimpleUserBO convert(Guest guest) {
        if (Objects.isNull(guest)) {
            return null;
        }
        SimpleUserBO user = new SimpleUserBO();
        user.setHrId(guest.getHrId());
        user.setName(guest.getName());
        user.setMobile(guest.getMobile());
        user.setDeptName("");
        return user;
    }
}
