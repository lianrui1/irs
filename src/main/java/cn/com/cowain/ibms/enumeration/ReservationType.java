package cn.com.cowain.ibms.enumeration;

/**
 * 预定类型
 *
 * @author 胡荆陵
 * @since 2020-7-30
 */
public enum ReservationType {
    //开会
    MEETING,
    //接待贵宾
    VIP,
    //培训
    TRAINING,
    //维护
    MAINTAIN

}
