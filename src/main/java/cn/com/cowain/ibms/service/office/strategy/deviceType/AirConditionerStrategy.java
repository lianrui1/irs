package cn.com.cowain.ibms.service.office.strategy.deviceType;

import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.rest.resp.device.AirConditionerResp;
import cn.com.cowain.ibms.rest.resp.device.OfficeCommonDeviceResp;
import cn.com.cowain.ibms.rest.resp.device.centralized.CentralizedControlDeviceFilter;
import cn.com.cowain.ibms.rest.resp.office.AirConditionerStatusDetailResp;
import cn.com.cowain.ibms.rest.resp.office.ShowDeviceResp;
import cn.com.cowain.ibms.service.office.DevicePropertiesService;
import cn.com.cowain.ibms.service.office.strategy.DeviceTypeStrategy;
import cn.com.cowain.ibms.utils.IConst;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Log4j2
public class AirConditionerStrategy implements DeviceTypeStrategy {

    private final DevicePropertiesService devicePropertiesService = SpringUtil.getBean(DevicePropertiesService.class);

    private final DeviceDao deviceDao = SpringUtil.getBean(DeviceDao.class);

    @Override
    public void addCommonDeviceResp(ShowDeviceResp resp, List<OfficeCommonDeviceResp> commonDeviceRespList) {
        log.info("进入空调设备的策略方法！");
        AirConditionerStatusDetailResp airConditionerStatusDetailResp = devicePropertiesService.getPropertiesFromDeviceRedis(resp.getHwDeviceId(), resp.getDeviceType(), AirConditionerStatusDetailResp.class);
        log.info("通过Redis拿到的结果：{}", airConditionerStatusDetailResp);
        if (airConditionerStatusDetailResp == null || StringUtils.isEmpty(airConditionerStatusDetailResp.getHwStatus())) {
            Optional<IotDevice> iotDeviceOptional = deviceDao.findByHwDeviceId(resp.getHwDeviceId());
            if (iotDeviceOptional.isEmpty()) {
                log.info("查询不到设备【{}】", resp.getDeviceName());
                return;
            }
            airConditionerStatusDetailResp = new AirConditionerStatusDetailResp();
            getAirConditionerRespFromApi(iotDeviceOptional.get(), airConditionerStatusDetailResp);

        }
        airConditionerStatusDetailResp.setHwDeviceId(resp.getHwDeviceId());
        airConditionerStatusDetailResp.setId(resp.getDeviceId());
        airConditionerStatusDetailResp.setDeviceName(resp.getDeviceName());
        //airConditionerStatusDetailResp.setHwStatus(resp.getHwStatus().name());
        commonDeviceRespList.add(new OfficeCommonDeviceResp(airConditionerStatusDetailResp, resp.getDeviceTypeName()));
    }

    private void getAirConditionerRespFromApi(IotDevice iotDevice, AirConditionerStatusDetailResp airConditionerStatusDetailResp) {
        AirConditionerResp airConditionerFromApi = devicePropertiesService.addPropertiesToDeviceRedis(iotDevice, AirConditionerResp.class);
        log.info("通过Api拿到的结果：{}", airConditionerFromApi);
        if (airConditionerFromApi == null) {
            airConditionerStatusDetailResp.setMode("");
            airConditionerStatusDetailResp.setDeviceName("");
            airConditionerStatusDetailResp.setSetFanAuto("");
            airConditionerStatusDetailResp.setWindSpeed("");
            airConditionerStatusDetailResp.setWindDirection("");
            airConditionerStatusDetailResp.setSetPower("");
            airConditionerStatusDetailResp.setSetSwing("");
        } else {
            BeanUtils.copyProperties(airConditionerFromApi, airConditionerStatusDetailResp);
        }
    }

    @Override
    public void filterDevicePropertiesStatus(IotDevice iotDevice, CentralizedControlDeviceFilter controlDeviceFilter, String targetAction) {
        log.info("进入空调筛选设备的策略方法！");
        AirConditionerStatusDetailResp airConditionerStatusDetailResp = devicePropertiesService.getPropertiesFromDeviceRedis(iotDevice.getHwDeviceId(), AirConditionerStatusDetailResp.class);
        if (airConditionerStatusDetailResp == null || StringUtils.isEmpty(airConditionerStatusDetailResp.getHwStatus())) {
            airConditionerStatusDetailResp = new AirConditionerStatusDetailResp();
            getAirConditionerRespFromApi(iotDevice, airConditionerStatusDetailResp);
        }
        Map<String, String> map = new ConcurrentHashMap<>();
        map.put(IConst.DICT_TARGET_POWER, targetAction);
        map.put(IConst.DICT_POWER, airConditionerStatusDetailResp.getSetPower());
        //map.put(IConst.DICT_STATUS, airConditionerStatusDetailResp.getHwStatus());
        map.put(IConst.DICT_STATUS,IConst.STATUS_ONLINE);
        devicePropertiesService.filterDevice(iotDevice, map, controlDeviceFilter);
    }

    @Override
    public boolean isOpen(IotDevice iotDevice) {
        log.info("进入空调筛选设备的策略方法！");
        if (StrUtil.isBlank(iotDevice.getHwDeviceId())) {
            return false;
        }
        AirConditionerStatusDetailResp airConditionerStatusDetailResp = devicePropertiesService.getPropertiesFromDeviceRedis(iotDevice, AirConditionerStatusDetailResp.class);
        if (airConditionerStatusDetailResp == null || StringUtils.isEmpty(airConditionerStatusDetailResp.getHwStatus())) {
            airConditionerStatusDetailResp = new AirConditionerStatusDetailResp();
            getAirConditionerRespFromApi(iotDevice, airConditionerStatusDetailResp);
        }
        if(StringUtils.isNotBlank(airConditionerStatusDetailResp.getHwStatus())){
            iotDevice.setHwStatus(DeviceStatus.valueOf(airConditionerStatusDetailResp.getHwStatus()));
        }
        return StrUtil.equalsIgnoreCase(IConst.STATUS_ONLINE, airConditionerStatusDetailResp.getHwStatus()) &&
                StrUtil.equalsIgnoreCase(IConst.SET_POWER_ON, airConditionerStatusDetailResp.getSetPower());
    }

    @Override
    public DeviceStatus getNewestStatus(IotDevice iotDevice) {
        AirConditionerStatusDetailResp airConditionerStatusDetailResp = devicePropertiesService.getPropertiesFromDeviceRedis(iotDevice, AirConditionerStatusDetailResp.class);
        if (airConditionerStatusDetailResp == null || StringUtils.isEmpty(airConditionerStatusDetailResp.getHwStatus())) {
            airConditionerStatusDetailResp = new AirConditionerStatusDetailResp();
            getAirConditionerRespFromApi(iotDevice, airConditionerStatusDetailResp);
        }
        if(StringUtils.isNotBlank(airConditionerStatusDetailResp.getHwStatus())){
            return DeviceStatus.valueOf(airConditionerStatusDetailResp.getHwStatus());
        }
        return null;
    }
}
