package cn.com.cowain.ibms.rest.resp.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/5/18 9:04
 */
@Data
@ApiModel("智能断路器用电数据")
public class CircuitBreakerResp {

    @ApiModelProperty(value = "每日用电数据", required = true, position = 1)
    private List<IntelligentCircuitBreakerDataResp> circuitBreakerDataResps;

    @ApiModelProperty(value = "用电总量", required = true, position = 1)
    private Integer sum;
}
