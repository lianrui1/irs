package cn.com.cowain.ibms.service.iot.impl;

import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.dao.iot.DeviceGroupDao;
import cn.com.cowain.ibms.dao.iot.DeviceGroupItemDao;
import cn.com.cowain.ibms.entity.iot.DeviceGroup;
import cn.com.cowain.ibms.entity.iot.DeviceGroupItem;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.PageReq;
import cn.com.cowain.ibms.rest.req.Sorts;
import cn.com.cowain.ibms.rest.req.iot.DeviceGroupEditReq;
import cn.com.cowain.ibms.rest.req.space.DevicePageReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.iot.DeviceGroupDetailResp;
import cn.com.cowain.ibms.rest.resp.iot.DeviceGroupTreeResp;
import cn.com.cowain.ibms.rest.resp.iot.IotDeviceResp;
import cn.com.cowain.ibms.service.bean.ServiceMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.iot.DeviceGroupService;
import cn.com.cowain.ibms.utils.DateUtils;
import cn.com.cowain.ibms.utils.ErrConst;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Yang.Lee
 * @date 2021/10/21 13:28
 */
@Service
@Slf4j
public class DeviceGroupServiceImpl implements DeviceGroupService {

    @Resource
    private DeviceGroupDao deviceGroupDao;

    @Resource
    private DeviceDao deviceDao;

    @Resource
    private DeviceGroupItemDao deviceGroupItemDao;

    /**
     * 创建会议联动
     *
     * @param req 请求参数
     * @return 创建结果，创建成功后object中包含新数据的id
     * @author Yang.Lee
     * @date 2021/10/21 13:27
     **/
    @Override
    @Transactional
    public ServiceResult create(DeviceGroupEditReq req) {

        DeviceGroup parent = null;
        int level = 1;

        if (StringUtils.isNotEmpty(req.getParentId())) {
            // 查询父节点信息
            Optional<DeviceGroup> parentOp = deviceGroupDao.findById(req.getParentId());
            if (parentOp.isEmpty()) {
                return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_DEVICE_GROUP, req.getParentId()), ErrConst.E01);
            }
            parent = parentOp.get();
            level = parentOp.get().getLevel() + 1;
        }


        DeviceGroup deviceGroup = new DeviceGroup();
        deviceGroup.setName(req.getName());
        deviceGroup.setDescription(req.getDescription());
        deviceGroup.setParentGroup(parent);
        deviceGroup.setNumber("DG-" + DateUtils.formatLocalDateTime(LocalDateTime.now(), DateUtils.DATE_PATTERN2));
        deviceGroup.setLevel(level);

        deviceGroupDao.save(deviceGroup);

        return ServiceResult.ok(IdResp.builder().id(deviceGroup.getId()).build());
    }

    /**
     * 获取设备联动数据树
     *
     * @return 联动树
     * @author Yang.Lee
     * @date 2021/10/21 15:52
     **/
    @Override
    @Transactional
    public List<DeviceGroupTreeResp> getTree() {

        List<DeviceGroup> deviceGroupList = deviceGroupDao.findAll(Sort.by(Sort.Direction.DESC, Sorts.CREATED_TIME));
        return getDeviceGroupTreeResp(deviceGroupList);
    }

    /**
     * 香群组中添加设备
     *
     * @param deviceGroupId 群组ID
     * @param deviceIdList  设备ID集合
     * @return 添加结果
     * @author Yang.Lee
     * @date 2021/10/21 16:23
     **/
    @Override
    @Transactional
    public ServiceResult addDevice(String deviceGroupId, List<String> deviceIdList) {

        Optional<DeviceGroup> deviceGroupOptional = deviceGroupDao.findById(deviceGroupId);
        if (deviceGroupOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_DEVICE_GROUP, deviceGroupId), ErrConst.E01);
        }

        DeviceGroup deviceGroup = deviceGroupOptional.get();
        List<DeviceGroupItem> itemList = new ArrayList<>();
        for (String deviceId : deviceIdList) {

            Optional<IotDevice> deviceOptional = deviceDao.findById(deviceId);
            if (deviceOptional.isEmpty()) {
                return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_DEVICE, deviceGroupId), ErrConst.E01);
            }


            Optional<DeviceGroupItem> deviceGroupItemOptional = deviceGroupItemDao.findByDeviceGroupIdAndIotDeviceId(deviceGroup.getId(), deviceId);
            if (deviceGroupItemOptional.isPresent()) {
                continue;
            }

            DeviceGroupItem item = new DeviceGroupItem();
            item.setDeviceGroup(deviceGroup);
            item.setIotDevice(deviceOptional.get());
            itemList.add(item);
        }

        deviceGroupItemDao.saveAll(itemList);

        return ServiceResult.ok();
    }

    /**
     * 获取设备群组详情
     *
     * @param groupId 群组ID
     * @param req     分页参数
     * @return 详情
     * @author Yang.Lee
     * @date 2021/10/21 17:02
     **/
    @Override
    @Transactional(readOnly = true)
    public DeviceGroupDetailResp get(String groupId, PageReq req) {

        Optional<DeviceGroup> deviceGroupOptional = deviceGroupDao.findById(groupId);
        if (deviceGroupOptional.isEmpty()) {
            return DeviceGroupDetailResp.builder().build();
        }

        DeviceGroup deviceGroup = deviceGroupOptional.get();
        Optional<DeviceGroup> parentOp = Optional.ofNullable(deviceGroup.getParentGroup());

        Page<DeviceGroupItem> page = deviceGroupItemDao.findByDeviceGroupId(groupId, PageRequest.of(req.getPage(), req.getSize(), Sort.by(req.getOrders())));

        PageBean<IotDeviceResp> pageBean = new PageBean<>();
        pageBean.setPage(page.getPageable().getPageNumber());
        pageBean.setSize(page.getPageable().getPageSize());
        pageBean.setTotalPages(page.getTotalPages());
        pageBean.setTotalElements(page.getTotalElements());
        pageBean.setList(page.getContent().stream().map(DeviceGroupItem::getIotDevice).map(IotDeviceResp::convert).collect(Collectors.toList()));

        return DeviceGroupDetailResp.builder()
                .description(deviceGroup.getDescription())
                .id(deviceGroup.getId())
                .level(deviceGroup.getLevel())
                .name(deviceGroup.getName())
                .number(deviceGroup.getNumber())
                .parentId(parentOp.map(DeviceGroup::getId).orElse(""))
                .parentName(parentOp.map(DeviceGroup::getName).orElse(""))
                .deviceList(pageBean)
                .build();
    }

    /**
     * 删除设备群组
     *
     * @param groupId 群组ID
     * @return 删除结果
     * @author Yang.Lee
     * @date 2021/10/21 18:55
     **/
    @Override
    @Transactional
    public ServiceResult delete(String groupId) {

        Optional<DeviceGroup> deviceGroupOptional = deviceGroupDao.findById(groupId);
        if (deviceGroupOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_DEVICE_GROUP, groupId), ErrConst.E01);
        }

        DeviceGroup deviceGroup = deviceGroupOptional.get();

        // 删除该群组之前判断该群组是否有子节点，如果有则不允许删除

        List<DeviceGroup> children = deviceGroupDao.findByParentGroupId(deviceGroup.getId());
        if(!CollectionUtils.isEmpty(children)){
            return ServiceResult.error("该群组下包含子群组，无法删除", ErrConst.E01);
        }

        deviceGroup.setIsDelete(1);

        List<DeviceGroupItem> itemList = deviceGroupItemDao.findByDeviceGroupId(deviceGroup.getId());
        itemList.forEach(obj -> obj.setIsDelete(1));

        deviceGroupDao.save(deviceGroup);
        deviceGroupItemDao.saveAll(itemList);

        return ServiceResult.ok();
    }

    /**
     * 从群组中移除设备
     *
     * @param groupId  群组ID
     * @param deviceId 设备ID
     * @return 操作结果
     * @author Yang.Lee
     * @date 2021/10/25 9:21
     **/
    @Override
    @Transactional
    public ServiceResult removeDevice(String groupId, String deviceId) {

        Optional<DeviceGroup> deviceGroupOptional = deviceGroupDao.findById(groupId);
        if (deviceGroupOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_DEVICE_GROUP, groupId), ErrConst.E01);
        }

        Optional<DeviceGroupItem> deviceGroupItemOptional = deviceGroupItemDao.findByDeviceGroupIdAndIotDeviceId(groupId, deviceId);
        if (deviceGroupItemOptional.isEmpty()) {
            return ServiceResult.error("删除失败。未在该群组下找到对应设备。");
        }

        DeviceGroupItem item = deviceGroupItemOptional.get();
        item.setIsDelete(1);

        deviceGroupItemDao.save(item);

        return ServiceResult.ok();
    }

    /**
     * 编辑设备群组
     *
     * @param groupId 群组ID
     * @param req     编辑参数
     * @return 操作结果
     * @author Yang.Lee
     * @date 2021/10/25 9:34
     **/
    @Override
    @Transactional
    public ServiceResult edit(String groupId, DeviceGroupEditReq req) {

        Optional<DeviceGroup> deviceGroupOptional = deviceGroupDao.findById(groupId);
        if (deviceGroupOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_DEVICE_GROUP, groupId), ErrConst.E01);
        }

        DeviceGroup dbGroup = deviceGroupOptional.get();
        dbGroup.setName(req.getName());
        dbGroup.setDescription(req.getDescription());

        return ServiceResult.ok();
    }

    /**
     * 查询所有设备信息
     *
     * @param req 查询参数
     * @return 结果
     * @author Yang.Lee
     * @date 2021/10/25 13:38
     **/
    @Override
    @Transactional
    public PageBean<IotDeviceResp> getAllDevice(DevicePageReq req) {

        Page<IotDevice> devicePage = deviceDao.findAll((root, query, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();

            if (StringUtils.isNotBlank(req.getProductId())) {
                list.add(criteriaBuilder.equal(root.get("iotProduct").get("id"), req.getProductId()));
            }

            if (StringUtils.isNoneBlank(req.getProjectId())) {
                list.add(criteriaBuilder.equal(root.get("project").get("id"), req.getProjectId()));
            }

            if (StringUtils.isNotBlank(req.getSpaceId())) {
                list.add(criteriaBuilder.equal(root.get("space").get("id"), req.getSpaceId()));
            }

            if (req.getDeviceStatus() != null) {
                list.add(criteriaBuilder.equal(root.get("hwStatus"), req.getDeviceStatus()));
            }

            if (StringUtils.isNotBlank(req.getSnOrName())) {
                String like = "%" + req.getSnOrName() + "%";
                list.add(criteriaBuilder.or(criteriaBuilder.like(root.get("sn"), like),
                        criteriaBuilder.like(root.get("deviceName"), like)));
            }

            list.add(root.get("id").in(deviceGroupItemDao.findAll().stream().map(DeviceGroupItem::getIotDevice).map(IotDevice::getId).collect(Collectors.toList())));

            return criteriaBuilder.and(list.toArray(new Predicate[0]));

        }, PageRequest.of(req.getPage(), req.getSize(), Sort.by(req.getOrders())));



        PageBean<IotDeviceResp> respPageBean = new PageBean<>();
        respPageBean.setPage(devicePage.getPageable().getPageNumber());
        respPageBean.setSize(devicePage.getPageable().getPageSize());
        respPageBean.setTotalPages(devicePage.getTotalPages());
        respPageBean.setTotalElements(devicePage.getTotalElements());
        respPageBean.setList(devicePage.getContent().stream().map(IotDeviceResp::convert).collect(Collectors.toList()));

        return respPageBean;
    }


    /**
     * 将设备群组列表转换成空间树对象
     *
     * @param deviceGroupList 设备群组列表
     * @return 转换后结果
     * @author Yang.Lee
     */
    private List<DeviceGroupTreeResp> getDeviceGroupTreeResp(List<DeviceGroup> deviceGroupList) {

        List<DeviceGroupTreeResp> spaceTreeRespList = new ArrayList<>();

        if (CollectionUtils.isEmpty(deviceGroupList)) {
            return spaceTreeRespList;
        }

        // 将list对象转成Map
        Map<String, DeviceGroupTreeResp> spaceMap = new HashMap<>();
        DeviceGroupTreeResp spaceTreeResp;
        for (DeviceGroup space : deviceGroupList) {
            spaceTreeResp = new DeviceGroupTreeResp();
            // 数据赋值
            BeanUtils.copyProperties(space, spaceTreeResp);

            spaceTreeResp.setId(space.getId());
            spaceTreeResp.setName(space.getName());
            spaceTreeResp.setLevel(space.getLevel());
            spaceTreeResp.setParentId(space.getParentGroup() == null ? null : space.getParentGroup().getId());
            spaceTreeResp.setParentName(Optional.ofNullable(space.getParentGroup()).map(DeviceGroup::getName).orElse(""));

            spaceMap.put(space.getId(), spaceTreeResp);
        }

        // 循环查询父节点
        for (Map.Entry<String, DeviceGroupTreeResp> entry : spaceMap.entrySet()) {
            DeviceGroupTreeResp node = entry.getValue();
            if (node.getParentId() == null) {
                // 如果是顶层节点，直接添加到结果集合中
                spaceTreeRespList.add(node);
            } else {
                // 如果不是顶层节点，找其父节点，并且添加到父节点的子节点集合中
                if (spaceMap.get(node.getParentId()) != null) {
                    if (spaceMap.get(node.getParentId()).getChildrenList() == null) {
                        spaceMap.get(node.getParentId()).setChildrenList(new ArrayList<>());
                    }
                    spaceMap.get(node.getParentId()).getChildrenList().add(node);
                }
            }
        }

        sortTree(spaceTreeRespList);
        return spaceTreeRespList;
    }

    /**
     * 排序设备群组树
     *
     * @param tree 待排序的树
     * @author Yang.Lee
     * @date 2021/10/21 16:18
     **/
    private void sortTree(List<DeviceGroupTreeResp> tree) {

        tree.sort(Comparator.comparing(DeviceGroupTreeResp::getName));
        tree.forEach(obj -> {
            if (!CollectionUtils.isEmpty(obj.getChildrenList())) {
                sortTree(obj.getChildrenList());
            }
        });
    }
}
