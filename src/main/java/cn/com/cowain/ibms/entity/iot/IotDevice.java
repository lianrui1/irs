package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.space.Project;
import cn.com.cowain.ibms.entity.space.IotProduct;
import cn.com.cowain.ibms.enumeration.IotNodeType;
import cn.com.cowain.ibms.enumeration.iot.DataSource;
import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Where;
import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * iot Device
 *
 * @author cuiEnming
 * @date 2020/11/19  18:51
 * @since s2
 */
@ToString(callSuper=true)
@Data
@Entity
@Table(name = "iot_device" ,indexes = {@Index(columnList = "space_id"),@Index(columnList = "hw_device_id"),@Index(columnList = "product_id")})
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_device", comment = "iot设备")
public class IotDevice extends BaseEntity {

    /**
     * 设备状态 0:禁用;1:启用
     */
    @Column(name = "status", nullable = false, columnDefinition = "tinyint(1) COMMENT '设备状态 0:禁用;1:启用'")
    private Integer status = 1;

    /**
     * 设备类型类型
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "device_type", columnDefinition = "varchar(50) COMMENT '设备类型'")
    private DeviceType deviceType;

    /**
     * IOT网络节点 类型
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "iot_node_type", columnDefinition = "varchar(50) COMMENT 'IOT网络节点 类型'")
    private IotNodeType iotNodeType;

    /**
     * 设备密码
     */
    @Column(name = "password", columnDefinition = "varchar(50) COMMENT '设备密码'")
    private String password;


    /**
     * SN码
     */
    @Column(name = "sn", columnDefinition = "varchar(50) COMMENT 'SN码'")
    private String sn;

    /**
     * 设备名称
     */
    @Column(name = "device_name", columnDefinition = "varchar(50) COMMENT '设备名称'")
    private String deviceName;
    /**
     * 所属产品
     */
    @ManyToOne
    @JoinColumn(name = "product_id", columnDefinition = "varchar(36) COMMENT 'FK'")
    private IotProduct iotProduct;

    /**
     * 所属项目
     */
    @ManyToOne
    @JoinColumn(name = "project_id", columnDefinition = "varchar(36) COMMENT 'FK'")
    private Project project;

    /**
     * 所属空间
     */
    @ManyToOne
    @JoinColumn(name = "space_id", columnDefinition = "varchar(36) COMMENT 'FK'")
    private Space space;

    /**
     * 设备编号
     */
    @Column(name = "device_no", columnDefinition = "int(4) COMMENT '设备编号'")
    private int deviceNo;

    /**
     * 排序字段
     */
    @Column(name = "seq", nullable = false, columnDefinition = "int(10) COMMENT '设备排序'")
    private int seq;

    /**
     * 华为云中的设备ID
     */
    @Column(name = "hw_device_id", columnDefinition = "varchar(256) COMMENT '华为云中的设备ID'")
    private String hwDeviceId;

    /**
     * 华为云中的设备名称
     */
    @Column(name = "hw_device_name", columnDefinition = "varchar(256) COMMENT '华为云中的设备名称'")
    private String hwDeviceName;

    /**
     * 华为云中的设备节点类型
     *
     * ENDPOINT：非直连设备。
     * GATEWAY：直连设备或网关。
     * UNKNOWN：未知。
     */
    @Column(name = "hw_node_type", columnDefinition = "varchar(256) COMMENT '华为云中的设备节点类型.ENDPOINT：非直连设备,GATEWAY：直连设备或网关,UNKNOWN：未知'")
    private String hwNodeType;


    /**
     * 华为云中的设备关联的产品ID
     */
    @Column(name = "hw_product_id", columnDefinition = "varchar(256) COMMENT '华为云中的设备关联的产品ID'")
    private String hwProductId;

    /**
     * 华为云中的设备关联的产品名称
     */
    @Column(name = "hw_product_name", columnDefinition = "varchar(256) COMMENT '华为云中的设备关联的产品名称'")
    private String hwProductName;

    /**
     * 华为云中的设备关联的网关ID，用于标识设备所属的父设备，即父设备的设备ID
     */
    @Column(name = "hw_gateway_id", columnDefinition = "varchar(256) COMMENT '华为云中的设备关联的网关ID'")
    private String hwGatewayId;

    /**
     * 华为云中的设备的状态。
     *
     * ONLINE：设备在线。
     * OFFLINE：设备离线。
     * ABNORMAL：设备异常。
     * INACTIVE：设备未激活。
     * FROZEN：设备冻结
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "hw_status", columnDefinition = "varchar(256) COMMENT '华为云中的设备的状态。ONLINE：设备在线。OFFLINE：设备离线。ABNORMAL：设备异常。INACTIVE：设备未激活。FROZEN：设备冻结'")
    private DeviceStatus hwStatus;

    /**
     * 数据来源
     **/
    @Enumerated(EnumType.STRING)
    @Column(name = "data_source", columnDefinition = "varchar(30) COMMENT '数据来源'")
    private DataSource dataSource;

    /**
     * 设备位置
     **/
    @Column(name = "address", columnDefinition = "varchar(50) COMMENT '设备位置'")
    private String address;

    /**
     * 华为云最近一次状态时间
     **/
    @Column(name = "last_hw_status_changed_datetime", columnDefinition = "datetime COMMENT '华为云最近一次状态时间'")
    private LocalDateTime lastHwStatusChangedDatetime;

    /**
     * 设备是否纳入空间控制
     **/
    @Column(name = "space_control", columnDefinition = "TINYINT (1) DEFAULT 0  COMMENT '设备是否纳入空间控制,1:是.2:否.纳入控制的设备可以被用户手动控制'")
    private Integer spaceControl;
}
