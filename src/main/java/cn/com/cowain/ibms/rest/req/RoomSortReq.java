package cn.com.cowain.ibms.rest.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/3 21:16
 */
@Data
public class RoomSortReq implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @NotEmpty
    private List<IdAndSeqBean> data;

    @NotNull
    @ApiModelProperty(value = "排序数据当前页码", required = true, position = 1)
    private int page;

    @NotNull
    @ApiModelProperty(value = "排序数据当前页长", required = true, position = 2)
    private int size;
}
