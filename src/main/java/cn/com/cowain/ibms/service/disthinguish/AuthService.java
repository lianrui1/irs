package cn.com.cowain.ibms.service.disthinguish;

import cn.com.cowain.ibms.rest.req.distinguish.AuthTimeDataReq;
import cn.com.cowain.ibms.service.bean.ServiceResult;

import java.util.List;

/**
 * @author wei.cheng
 * @date 2022/03/23 15:15
 */
public interface AuthService {
    /**
     * 接受ehr 南通排班以及人员外出申请
     *
     * @param authTimeDataList
     * @return
     */
    ServiceResult updateAuth(List<AuthTimeDataReq> authTimeDataList);

    /**
     * 旷世鸿图南通排班出口闸门设备开门请求
     *
     * @param deviceSn
     * @param hrId
     * @return
     */
    ServiceResult openKHSTDoor(String deviceSn, String hrId);

    /**
     * 权限缓存数据恢复
     *
     * @param startTime 要恢复的数据起始时间，格式为`yyyy-MM-dd HH:mm:ss`
     * @return
     */
    ServiceResult recoveryAuth(String startTime);
}
