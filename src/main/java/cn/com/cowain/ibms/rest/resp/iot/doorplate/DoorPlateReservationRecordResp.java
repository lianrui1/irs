package cn.com.cowain.ibms.rest.resp.iot.doorplate;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 门牌状态中显示会议信息的对象
 * @author Yang.Lee
 * @date 2020/12/2 18:27
 */
@Data
public class DoorPlateReservationRecordResp {

    /**
     * 开始时间
     */
    @ApiModelProperty(value = "开始时间", required = true)
    private String from;

    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间", required = true, position = 1)
    private String to;

    /**
     * 发起人
     */
    @ApiModelProperty(value = "发起人", required = true, position = 2)
    private String initiator;
}
