package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/3/23 2:42
 */
@Data
@ApiModel("人员信息请求对象")
public class ElevatorSpotUserSaveReq {

    @ApiModelProperty(value = "工号")
    private String empNo;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "部门ID")
    private String deptId;

    @ApiModelProperty(value = "人员类型1员工2访客")
    private int isStaff;

}
