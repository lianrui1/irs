package cn.com.cowain.ibms.entity;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/9 9:16
 */
@Data
public class Staff {

    // 人员ID(根据工号MD5加密)
    private String  id;

    // 人员姓名
    private String  name;

    // 人员工号
    private String  empNo;

    // 部门ID
    private String  deptId;

    // 是否员工 0员工 1访客
    private Integer isStaff = 0;
}
