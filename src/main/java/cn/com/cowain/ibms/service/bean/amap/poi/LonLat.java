package cn.com.cowain.ibms.service.bean.amap.poi;

import lombok.Data;

import java.text.DecimalFormat;

/**
 * 坐标对象
 *
 * @author Yang.Lee
 * @date 2021/4/19 19:27
 */
@Data
public class LonLat {

    private static DecimalFormat df = new DecimalFormat("######0.000000");

    public LonLat(double lon, double lat){

        // 对经纬度进行操作，只保留小数点后6位
        this.lon = Double.valueOf(df.format(lon));
        this.lat = Double.valueOf(df.format(lat));
    }
    /**
     * 经度坐标
     **/
    private double lon;

    /**
     * 纬度坐标
     **/
    private double lat;
}
