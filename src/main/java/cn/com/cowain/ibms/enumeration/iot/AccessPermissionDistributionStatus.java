package cn.com.cowain.ibms.enumeration.iot;

/**
 * 通行权限下发状态
 *
 * @author wei.cheng
 * @date 2022/03/14 13:50
 */
public enum AccessPermissionDistributionStatus {

    DOING("执行中"),

    SUCCESS("成功"),

    FAILED("失败");

    String name;

    AccessPermissionDistributionStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
