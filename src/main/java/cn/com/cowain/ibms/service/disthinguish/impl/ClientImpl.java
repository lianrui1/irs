package cn.com.cowain.ibms.service.disthinguish.impl;

import cn.com.cowain.ibms.rest.req.distinguish.GroupExistReq;
import cn.com.cowain.ibms.rest.req.distinguish.UserGroupReq;
import cn.com.cowain.ibms.rest.req.distinguish.UserReq;
import cn.com.cowain.ibms.service.disthinguish.StaffGroupClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/18 17:06
 */
@Service
@FeignClient(value = "ms-device-connector" ,url="${cowain.ms-device.host}")
public interface ClientImpl extends StaffGroupClient {

    @DeleteMapping("/ms-device-conn/api/v1/user/group/{type}/{id}")
    String deleteStaffGroup(@PathVariable String type, @PathVariable String id);

    @PostMapping("/ms-device-conn/api/v1/user/group/{type}")
    String save(@PathVariable String type, UserGroupReq userGroupReq);

    @PutMapping("/ms-device-conn/api/v1/user/{type}")
    String updateUser(@PathVariable String type, ArrayList<UserReq> userReq);

    @PutMapping("/ms-device-conn/api/v1/user/group/{type}")
    String update(@PathVariable String type, ArrayList<UserGroupReq> userGroupReq);

    @PostMapping("/ms-device-conn/api/v1/user/group/exist")
    String exist(GroupExistReq groupExistReq);
}
