package cn.com.cowain.ibms.rest.iot;

import cn.com.cowain.ibms.annotation.Idempotent;
import cn.com.cowain.ibms.enumeration.iot.DeviceLinkStatus;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.Sorts;
import cn.com.cowain.ibms.rest.req.iot.DeviceLinkEditReq;
import cn.com.cowain.ibms.rest.req.iot.DeviceLinkPageReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.iot.DeviceLinkPageResp;
import cn.com.cowain.ibms.rest.resp.iot.doorplate.DeviceLinkDetailResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.iot.DeviceLinkService;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * 设备联动Controller
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/deviceLink")
@Api(tags = IConst.MODULE_DEVICE_MODULE)
public class DeviceLinkController {

    @Resource
    private DeviceLinkService deviceLinkService;

    /**
     * 创建设备联动
     *
     * @param req           请求参数
     * @param bindingResult 校验对象
     * @return 创建结果，成功时返回数据主键id
     */
    @Idempotent("/deviceLink/post")
    @PostMapping
    @ApiOperation(value = "创建设备联动", tags = {IConst.MODULE_DEVICE_MODULE, IConst.V17})
    public ResponseEntity<JsonResult<IdResp>> create(@RequestBody @Validated DeviceLinkEditReq req, BindingResult bindingResult) {

        ServiceResult result = deviceLinkService.create(req);

        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, result.getErrorCode()));
        }
        return ResponseEntity.ok(JsonResult.ok((IdResp) result.getObject()));
    }

    /**
     * 编辑设备联动信息
     *
     * @param id            联动ID
     * @param req           请求参数
     * @param bindingResult 校验对象
     * @return 修改结果
     */
    @Idempotent("/deviceLink/put")
    @PutMapping("/{id}")
    @ApiOperation(value = "编辑设备联动", tags = {IConst.MODULE_DEVICE_MODULE, IConst.V17})
    public ResponseEntity<JsonResult<String>> update(@PathVariable String id,
                                                     @RequestBody @Validated DeviceLinkEditReq req, BindingResult bindingResult) {

        ServiceResult result = deviceLinkService.update(id, req);

        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, result.getErrorCode()));
        }
        return ResponseEntity.ok(JsonResult.ok(null));
    }

    /**
     * 删除设备联动
     *
     * @param id 联动ID
     * @return 操作结果
     */
    @Idempotent("/deviceLink/delete")
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除设备联动", tags = {IConst.MODULE_DEVICE_MODULE, IConst.V17})
    public ResponseEntity<JsonResult<String>> delete(@PathVariable String id) {
        ServiceResult result = deviceLinkService.delete(id);

        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, result.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.ok(null));
    }

    /**
     * 修改联动状态
     *
     * @param id     联动ID
     * @param status 需要被修改成的状态
     * @return 操作结果
     */
    @Idempotent("/deviceLink/status")
    @PutMapping("/{id}/status/{status}")
    @ApiOperation(value = "编辑设备联动状态", tags = {IConst.MODULE_DEVICE_MODULE, IConst.V17})
    public ResponseEntity<JsonResult<String>> update(@PathVariable String id,
                                                     @PathVariable DeviceLinkStatus status) {
        ServiceResult result = deviceLinkService.updateStatus(id, status);

        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, result.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.ok(null));
    }

    /**
     * 查询设备联动分页列表
     *
     * @param req 查询参数
     * @return 查询结果
     */
    @GetMapping("/page")
    @ApiOperation(value = "查询设备联动分页列表", tags = {IConst.MODULE_DEVICE_MODULE, IConst.V17})
    public ResponseEntity<JsonResult<PageBean<DeviceLinkPageResp>>> getPage(DeviceLinkPageReq req) {

        req.addOrderDesc(Sorts.CREATED_TIME);
        return ResponseEntity.ok(JsonResult.ok(deviceLinkService.getPage(req)));
    }

    /**
     * 查询设备联动详情
     *
     * @param id 联动ID
     * @return 详情
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "查询设备联动详情", tags = {IConst.MODULE_DEVICE_MODULE, IConst.V17})
    public ResponseEntity<JsonResult<DeviceLinkDetailResp>> getDetail(@PathVariable String id) {


        return ResponseEntity.ok(JsonResult.ok(deviceLinkService.getDetail(id)));
    }
}
