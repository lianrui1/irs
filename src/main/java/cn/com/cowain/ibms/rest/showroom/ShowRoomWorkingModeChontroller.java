package cn.com.cowain.ibms.rest.showroom;

import cn.com.cowain.ibms.feign.bean.common.CommonReq;
import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.PageReq;
import cn.com.cowain.ibms.rest.req.working_mode.ShowRoomWorkingModeSaveReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.iot.IotDeviceResp;
import cn.com.cowain.ibms.rest.resp.working_mode.DefaultWorkingModePageResp;
import cn.com.cowain.ibms.rest.resp.working_mode.ShowRoomWorkingModeDetailResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.office.ShowRoomModelService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author feng
 * @title: ShowRoomWorkingModeChontroller
 * @projectName ibms
 * @Date 2022/3/31 14:59
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/showroom/mode")
@Api(tags = IConst.SHOW_ROOM_MODEL)
public class ShowRoomWorkingModeChontroller {

    @Autowired
    private ShowRoomModelService showRoomModelService;

    @PostMapping
    @ApiOperation(value = "展厅-保存模式", tags = {IConst.INTELLIGENT_OFFICE_APPLET, IConst.V21})
    public ResponseEntity<JsonResult<IdResp>> save(@RequestBody @Validated ShowRoomWorkingModeSaveReq req) {
        log.debug("save...");

        ServiceResult result = showRoomModelService.save(req);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }
    @PostMapping("/update")
    @ApiOperation(value = "展厅-修改模式", tags = {IConst.INTELLIGENT_OFFICE_APPLET, IConst.V21})
    public ResponseEntity<JsonResult<IdResp>> updateModel(@RequestBody @Validated ShowRoomWorkingModeSaveReq req) {
        log.debug("save...");

        ServiceResult result = showRoomModelService.update(req);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }
    @PutMapping("/control/working/mode/{id}")
    @ApiOperation(value = "展厅-控制工作模式", tags = {IConst.INTELLIGENT_OFFICE_APPLET, IConst.V21})
    public ResponseEntity<JsonResult<String>> controlWorkingMode(@PathVariable @ApiParam(value = "工作模式ID", name = "id") String id) {

        ServiceResult result = showRoomModelService.controlWorkingMode(id);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @GetMapping("/default/working/mode/page")
    @ApiOperation(value = "查询默认工作模式场景列表(分页)", tags = {IConst.INTELLIGENT_OFFICE_APPLET, IConst.V21})
    public ResponseEntity<JsonResult<PageBean<DefaultWorkingModePageResp>>> getDefaultWorkingModePage(@ApiParam(name = "spaceId", value = "智能办公室ID") String spaceId,@ApiParam(name = "showroomCentralize", value = "是否是总控") Integer showroomCentralize, PageReq req) {

        return ResponseEntity.ok(JsonResult.ok("OK" , showRoomModelService.getDefaultWorkingModePage(spaceId, showroomCentralize,req)));
    }

    @PutMapping("/control/changename")
    @ApiOperation(value = "修改设备名称", tags = {IConst.INTELLIGENT_OFFICE_APPLET, IConst.V21})
    public ResponseEntity<JsonResult<String>> changeName(@RequestBody CommonReq req) {

        ServiceResult result = showRoomModelService.changeDeviceName(req);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }
    @GetMapping("/control/working/modedetail/{id}")
    @ApiOperation(value = "展厅-工作模式详情", tags = {IConst.INTELLIGENT_OFFICE_APPLET, IConst.V21})
    public ResponseEntity<JsonResult<ShowRoomWorkingModeDetailResp>> controlWorkingModeDetail(@PathVariable @ApiParam(value = "工作模式ID", name = "id") String id) {
        ServiceResult result = showRoomModelService.detail(id);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.ok("ok" ,(ShowRoomWorkingModeDetailResp)result.getObject()));
    }
    @GetMapping("/searchbyspace/{id}")
    @ApiOperation(value = "展厅-获取当前空间下以及下一空间下的所有设备", tags = {IConst.INTELLIGENT_OFFICE_APPLET, IConst.V21})
    public ResponseEntity<JsonResult<List<IotDeviceResp>>> searchBySpaceId(@PathVariable @ApiParam(value = "空间Id", name = "id") String id) {
        return ResponseEntity.ok(JsonResult.ok("ok" ,showRoomModelService.search(id)));
    }
    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "展厅-工作模式-删除", tags = {IConst.INTELLIGENT_OFFICE_APPLET, IConst.V21})
    public ResponseEntity<JsonResult<String>> deleteWorkingMode(@PathVariable @ApiParam(value = "工作模式ID", name = "id") String id) {
         showRoomModelService.delette(id);
        return ResponseEntity.ok(JsonResult.okNoData("ok"));
    }

}
