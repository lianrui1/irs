package cn.com.cowain.ibms.exceptions;

/**
 * 并发异常类
 * @author lianrui
 * @date 2022/09/02 13:26
 */
public class ConcurrentException extends RuntimeException {
    public ConcurrentException(String message) {
        super(message);
    }
}
