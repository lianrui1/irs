package cn.com.cowain.ibms.config;

import cn.com.cowain.ibms.config.prop.WeChatProperties;
import cn.com.cowain.ibms.exceptions.WeChatConfigException;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.config.WxMpConfigStorage;
import me.chanjar.weixin.mp.config.impl.WxMpDefaultConfigImpl;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * wechat mp configuration
 *
 * @author Binary Wang(https://github.com/binarywang)
 */
@Slf4j
@Configuration
@EnableConfigurationProperties(WeChatProperties.class)
public class WxMpConfiguration {

    private WeChatProperties weChatProperties;

    public WxMpConfiguration(WeChatProperties weChatProperties) {
        log.debug("WxMpConfiguration()...");
        this.weChatProperties = weChatProperties;
    }

    /**
     * WxMpConfigStorage
     *
     * @return
     */
    @Bean
    public WxMpConfigStorage wxMpConfigStorage() {
        log.debug("wxMpConfigStorage()...");
        //获取配置信息
        String appId = weChatProperties.getAppId();
        String secret = weChatProperties.getSecret();
        String token = weChatProperties.getToken();
        String aesKey = weChatProperties.getAesKey();
        if (isBlank(appId) || isBlank(secret)) {
            throw new WeChatConfigException("配置信息不存在");
        }
        //初始化 wxMpConfigStorage
        WxMpDefaultConfigImpl configStorage = new WxMpDefaultConfigImpl();
        configStorage.setAppId(appId);
        configStorage.setSecret(secret);
        configStorage.setToken(token);
        configStorage.setAesKey(aesKey);
        return configStorage;
    }

    @Bean
    public WxMpService wxMpService() {
        log.debug("wxMpService()...");
        //初始化 WxMpService
        WxMpService wxMpService = new WxMpServiceImpl();
        wxMpService.setWxMpConfigStorage(wxMpConfigStorage());
        return wxMpService;
    }

}
