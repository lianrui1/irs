package cn.com.cowain.ibms.rest.req.distinguish;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/4 17:43
 */
@Data
@ApiModel("单个下发人员更新人脸请求对象")
public class UserFaceReq {

    @NotNull
    @ApiModelProperty(value = "人员ID", required = true)
    private String id;

    @ApiModelProperty(value = "设备ID", required = true)
    private List<String> deviceIds;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "通行生效时间", required = true)
    private LocalDateTime startTime;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "通行结束时间", required = true)
    private LocalDateTime endTime;

    @NotEmpty
    @ApiModelProperty(value = "url", required = true)
    private String faceUrl;

    @ApiModelProperty(value = "是否全部 0 是")
    private Integer all;
}
