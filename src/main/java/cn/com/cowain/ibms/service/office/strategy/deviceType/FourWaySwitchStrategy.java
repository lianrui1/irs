package cn.com.cowain.ibms.service.office.strategy.deviceType;

import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.rest.resp.device.OfficeCommonDeviceResp;
import cn.com.cowain.ibms.rest.resp.device.centralized.CentralizedControlDeviceFilter;
import cn.com.cowain.ibms.rest.resp.distinguish.SwitchResp;
import cn.com.cowain.ibms.rest.resp.office.ShowDeviceResp;
import cn.com.cowain.ibms.rest.resp.office.SwitchDetailResp;
import cn.com.cowain.ibms.service.bean.DoorMagneticHWReq;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.device.HwProductsService;
import cn.com.cowain.ibms.service.office.DevicePropertiesService;
import cn.com.cowain.ibms.service.office.strategy.DeviceTypeStrategy;
import cn.com.cowain.ibms.utils.IConst;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Log4j2
public class FourWaySwitchStrategy implements DeviceTypeStrategy {
    private final DevicePropertiesService devicePropertiesService = SpringUtil.getBean(DevicePropertiesService.class);
    private final HwProductsService hwProductsService = SpringUtil.getBean(HwProductsService.class);

    private final DeviceDao deviceDao = SpringUtil.getBean(DeviceDao.class);

    @Override
    public void addCommonDeviceResp(ShowDeviceResp resp, List<OfficeCommonDeviceResp> commonDeviceRespList) {
        log.info("进入四路开关设备的策略方法！");
        SwitchDetailResp switchDetailResp = devicePropertiesService.getPropertiesFromDeviceRedis(resp.getHwDeviceId(), resp.getDeviceType(), SwitchDetailResp.class);

        if (switchDetailResp==null) {
            Optional<IotDevice> iotDeviceOptional=deviceDao.findByHwDeviceId(resp.getHwDeviceId());

            if (iotDeviceOptional.isEmpty()) {
                return;
            }
            switchDetailResp=new SwitchDetailResp();
            getRespFromApi(iotDeviceOptional.get(),switchDetailResp);
        }
        switchDetailResp.setHwDeviceId(resp.getHwDeviceId());
        switchDetailResp.setId(resp.getDeviceId());
        // switchDetailResp.setHwStatus(DeviceStatus.OFFLINE.name());
        switchDetailResp.setDeviceName(resp.getDeviceName());
        switchDetailResp.setAdditionalName(resp.getAdditionalName());
        commonDeviceRespList.add(new OfficeCommonDeviceResp(switchDetailResp, resp.getDeviceTypeName()));
    }

    @Override
    public void filterDevicePropertiesStatus(IotDevice iotDevice, CentralizedControlDeviceFilter controlDeviceFilter, String action) {
        log.info("进入四路开关筛选设备的策略方法！");
        SwitchDetailResp switchDetailResp=devicePropertiesService.getPropertiesFromDeviceRedis(iotDevice.getHwDeviceId(),SwitchDetailResp.class);

        if (switchDetailResp == null|| StringUtils.isEmpty(switchDetailResp.getHwStatus())) {
            switchDetailResp=new SwitchDetailResp();
            getRespFromApi(iotDevice,switchDetailResp);
        }
        Map<String,String> map=new ConcurrentHashMap<>();
        if(StringUtils.equals(action,IConst.SET_POWER_ON)){
            map.put(IConst.DICT_TARGET_POWER,IConst.SET_POWER_OFF);
        }else {
            map.put(IConst.DICT_TARGET_POWER,IConst.SET_POWER_ON);
        }
        map.put(IConst.DICT_POWER,action);
        //map.put(IConst.DICT_STATUS,switchDetailResp.getHwStatus());
        map.put(IConst.DICT_STATUS,IConst.STATUS_ONLINE);
        devicePropertiesService.filterDevice(iotDevice,map,controlDeviceFilter);
    }

    private void getRespFromApi(IotDevice iotDevice, SwitchDetailResp switchDetailResp) {
        ServiceResult serviceResult=hwProductsService.findProductProperties(iotDevice);
        DoorMagneticHWReq req= (DoorMagneticHWReq) serviceResult.getObject();
        SwitchResp switchResp = hwProductsService.findProperties(iotDevice.getHwDeviceId(),req.getService_id());
        BeanUtils.copyProperties(switchResp, switchDetailResp);
        devicePropertiesService.addPropertiesToDeviceRedis(iotDevice,switchDetailResp,SwitchDetailResp.class);

    }

    @Override
    public boolean isOpen(IotDevice iotDevice) {
        log.info("进入四路开关设备的策略方法！");
        if (StrUtil.isBlank(iotDevice.getHwDeviceId())) {
            return false;
        }
        SwitchDetailResp switchDetailResp = devicePropertiesService.getPropertiesFromDeviceRedis(iotDevice, SwitchDetailResp.class);
        if (switchDetailResp == null|| StringUtils.isEmpty(switchDetailResp.getHwStatus())) {
            switchDetailResp = new SwitchDetailResp();
            getRespFromApi(iotDevice, switchDetailResp);
        }
        if(StringUtils.isNotBlank(switchDetailResp.getHwStatus())){
            iotDevice.setHwStatus(DeviceStatus.valueOf(switchDetailResp.getHwStatus()));
        }
        if (!StrUtil.equalsIgnoreCase(IConst.STATUS_ONLINE, switchDetailResp.getHwStatus())) {
            return false;
        }
        if (StrUtil.equalsIgnoreCase(IConst.SET_POWER_OFF, switchDetailResp.getLight1())) {
            return false;
        }
        if (StrUtil.equalsIgnoreCase(IConst.SET_POWER_OFF, switchDetailResp.getLight2())) {
            return false;
        }
        if (StrUtil.equalsIgnoreCase(IConst.SET_POWER_OFF, switchDetailResp.getLight3())) {
            return false;
        }
        if (StrUtil.equalsIgnoreCase(IConst.SET_POWER_OFF, switchDetailResp.getLight4())) {
            return false;
        }
        return true;
    }

    @Override
    public DeviceStatus getNewestStatus(IotDevice iotDevice) {
        if (StrUtil.isBlank(iotDevice.getHwDeviceId())) {
            return null;
        }
        SwitchDetailResp switchDetailResp = devicePropertiesService.getPropertiesFromDeviceRedis(iotDevice, SwitchDetailResp.class);
        if (switchDetailResp == null|| StringUtils.isEmpty(switchDetailResp.getHwStatus())) {
            switchDetailResp = new SwitchDetailResp();
            getRespFromApi(iotDevice, switchDetailResp);
        }
        if(StringUtils.isNotBlank(switchDetailResp.getHwStatus())){
            return DeviceStatus.valueOf(switchDetailResp.getHwStatus());
        }
        return null;
    }
}
