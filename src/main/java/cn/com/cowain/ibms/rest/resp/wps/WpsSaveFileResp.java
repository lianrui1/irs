package cn.com.cowain.ibms.rest.resp.wps;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wei.cheng
 * @date 2022/02/28 15:13
 */
@Data
@ApiModel("WPS保存新版本文件返回数据")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WpsSaveFileResp {

    @ApiParam(value = "文件信息", required = true)
    private FileInfoResp file;
}
