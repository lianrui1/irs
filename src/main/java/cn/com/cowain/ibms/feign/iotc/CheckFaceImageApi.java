package cn.com.cowain.ibms.feign.iotc;

import cn.com.cowain.sfp.comm.JsonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author wei.cheng
 * @date 2022/06/15 16:57
 */
@Service
@RequestMapping({"/ms-iotCenter/api/v1"})
@FeignClient(value = "ms-iotCenter-backend-check-face-image", url = "sfp-api.cowain.cn")
public interface CheckFaceImageApi {

    /**
     * 验证人脸图片是否有效
     *
     * @param imageUrl 人脸照片URL
     * @author wei.cheng
     * @date 2022/3/10 10:02 上午
     **/
    @GetMapping("/image/check")
    JsonResult<Object> checkFaceImage(@RequestParam String imageUrl);
}
