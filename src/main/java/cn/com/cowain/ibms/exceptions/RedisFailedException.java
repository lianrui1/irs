package cn.com.cowain.ibms.exceptions;

/**
 * Redis处理异常类
 * @author Yang.Lee
 * @date 2020/12/15 15:39
 */
public class RedisFailedException extends RuntimeException{
    public RedisFailedException(String message) {
        super(message);
    }
}
