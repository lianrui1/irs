package cn.com.cowain.ibms.event.dto;

import cn.com.cowain.ibms.enumeration.integration_control.ControlDeviceRange;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import lombok.Data;

import java.util.List;

/**
 * 集控码缓存数据
 *
 * @author wei.cheng
 * @date 2022/09/19 16:29
 */
@Data
public class CentralizedCodeCacheData {
    /**
     * 集控码名称
     */
    private String name;
    /**
     * 关联项目id
     */
    private String projectId;
    /**
     * 关联空间id
     */
    private String spaceId;
    /**
     * 控制的设备类型列表
     */
    private List<DeviceType> controlDeviceTypeList;
    /**
     * 控制设备范围
     */
    private ControlDeviceRange controlDeviceRange;
}
