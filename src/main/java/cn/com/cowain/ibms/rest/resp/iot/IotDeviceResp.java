package cn.com.cowain.ibms.rest.resp.iot;

import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.space.IotProduct;
import cn.com.cowain.ibms.entity.space.Project;
import cn.com.cowain.ibms.enumeration.IotNodeType;
import cn.com.cowain.ibms.enumeration.iot.DataSource;
import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.feign.bean.iotc.resp.ApplicationResp;
import cn.com.cowain.ibms.rest.req.office.DeviceNameEditReq;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * @author yuchunlei
 * @version 1.0.0
 * @createTime 2020年12月17日 18:34:00
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class IotDeviceResp implements Serializable {


    private static final long serialVersionUID = -5779574165610341914L;

    @ApiModelProperty(value = "设备id", required = true, position = 1)
    private String id;

    @ApiModelProperty(value = "sn码", required = true, position = 2)
    private String sn;

    @ApiModelProperty(value = "设备名称", required = true, position = 3)
    private String deviceName;

    @ApiModelProperty(value = "所属产品", position = 4)
    private String iotProductName;

    @ApiModelProperty(value = "iot设备节点类型", required = true, position = 5)
    private IotNodeType iotNodeType;

    @ApiModelProperty(value = "iot设备节点类型名称", required = true, position = 6)
    private String iotNodeTypeName;

    @ApiModelProperty(value = "所属项目", required = true, position = 7)
    private String projectName;

    @ApiModelProperty(value = "空间", position = 8)
    private String space;

    @ApiModelProperty(value = "状态 0:未启用;1:已启用", required = true, position = 9)
    private int status;

    @ApiModelProperty(value = "所属产品id", position = 10)
    private String productId;

    @ApiModelProperty(value = "所属项目id", required = true, position = 11)
    private String projectId;

    @ApiModelProperty(value = "空间id", position = 12)
    private String spaceId;

    @ApiModelProperty(value = "排序序号", required = true, position = 13)
    private int seq;


    @ApiModelProperty(value = "设备网络状态", required = true, position = 14)
    private DeviceStatus deviceStatus;
    @ApiModelProperty(value = "设备网络状态名称", required = true, position = 14)
    private String deviceStatusName;

    @ApiModelProperty(value = "数据来源", position = 15)
    private String dataSource;

    @ApiModelProperty(value = "设备名称(EN)", position = 16)
    private String deviceNameEN;

    @ApiModelProperty(value = "设备类型", position = 17)
    private DeviceType deviceType;

    @ApiModelProperty(value = "设备位置", position = 18)
    private String address;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "最近一次设备状态变化时间", position = 19)
    private LocalDateTime lastStatusChangedTime;

    @ApiModelProperty(value = "是否可以空间控制.0： 不可以； 1 可以", position = 20)
    private int spaceControl;

    @ApiModelProperty(value = "所属应用", example = "IBMS", position = 21)
    private List<ApplicationResp> applicationList;

    @ApiModelProperty(value = "所属应用", example = "IBMS", position = 22)
    private String deviceTypeName;

    @ApiModelProperty(value = "所属应用", example = "IBMS", position = 23)
    private String hwDeviceId;

    @ApiModelProperty(value = "附加子名称")
    private DeviceNameEditReq.AdditionalName additionalName;

    /**
     * 对象类型转换
     *
     * @param source 原类型
     * @return 转换后类型
     * @author Yang.Lee
     * @date 2021/11/15 15:41
     **/
    public static IotDeviceResp convert(IotDevice source) {

        if (ObjectUtils.isEmpty(source)) {
            return IotDeviceResp.builder().build();
        }

        return IotDeviceResp.builder()
                .id(source.getId())
                .deviceName(source.getDeviceName())
                .sn(Optional.ofNullable(source.getSn()).orElse(""))
                .projectId(Optional.ofNullable(Optional.ofNullable(source.getSpace()).map(Space::getProject).orElse(null)).map(Project::getId).orElse(""))
                .projectName(Optional.ofNullable(Optional.ofNullable(source.getSpace()).map(Space::getProject).orElse(null)).map(Project::getProjectName).orElse(""))
                .address(Optional.ofNullable(source.getAddress()).orElse(""))
                .spaceId(Optional.ofNullable(source.getSpace()).map(Space::getId).orElse(""))
                .space(Optional.ofNullable(source.getSpace()).map(Space::getName).orElse(""))
                .spaceControl(Optional.ofNullable(source.getSpaceControl()).orElse(0))
                .dataSource(Optional.ofNullable(source.getDataSource()).map(DataSource::getName).orElse(""))
                .deviceStatus(Optional.ofNullable(source.getHwStatus()).orElse(DeviceStatus.UNKNOW))
                .deviceStatusName(Optional.ofNullable(source.getHwStatus()).map(DeviceStatus::getName).orElse(DeviceStatus.UNKNOW.getName()))
                .status(source.getStatus())
                .iotProductName(Optional.ofNullable(source.getIotProduct()).map(IotProduct::getName).orElse(""))
                .deviceType(source.getDeviceType())
                .deviceTypeName(source.getDeviceType().getName())
                .lastStatusChangedTime(source.getLastHwStatusChangedDatetime())
                .hwDeviceId(source.getHwDeviceId())
                .build();
    }
}
