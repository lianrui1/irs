package cn.com.cowain.ibms.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/1/19 16:42
 */
@Data
@Entity
@Table(name = "ibms_skill_person")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "ibms_skill_person", comment = "公众号推送表")
public class SkillPerson extends BaseEntity {
    private static final long serialVersionUID = 1L;


    // 人员工号
    @Column(name = "emp_no", nullable = false, columnDefinition = "varchar(45) COMMENT '人员工号'")
    private String empNo;

    /**
     * 人员名字
     */
    @Column(name = "name", nullable = false, columnDefinition = "varchar(45) COMMENT '人员名字'")
    private String name;


    /**
     * 微信openid
     */
    @Column(name = "open_id", nullable = false, columnDefinition = "varchar(45) COMMENT '微信openid'")
    private String openId;

}
