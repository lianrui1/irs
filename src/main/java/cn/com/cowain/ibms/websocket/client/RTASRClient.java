package cn.com.cowain.ibms.websocket.client;

import cn.com.cowain.ibms.rest.resp.audio.RTASTResultResp;
import cn.com.cowain.ibms.utils.websocket.DraftWithOrigin;
import cn.com.cowain.ibms.websocket.bean.xunfei.rtasr.CWResp;
import cn.com.cowain.ibms.websocket.bean.xunfei.rtasr.RTResp;
import cn.com.cowain.ibms.websocket.bean.xunfei.rtasr.STResp;
import cn.com.cowain.ibms.websocket.bean.xunfei.rtasr.WSResp;
import cn.com.cowain.ibms.websocket.service.impl.RTASRServiceImpl;
import cn.com.cowain.sfp.xunfei.util.EncryptUtil;
import cn.com.cowain.sfp.xunfei.websocket.Certifications;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.java_websocket.WebSocket;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;
import org.java_websocket.handshake.ServerHandshake;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.RandomAccessFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 实时语音转写
 * @author Yang.Lee
 * @date 2021/1/11 14:43
 */
@Data
@Component
@Slf4j
public class RTASRClient {

    // 请求地址
    private static final String HOST = "rtasr.xfyun.cn/v1/ws";

    private static final String BASE_URL = "wss://" + HOST;

    private static final String ORIGIN = "https://" + HOST;
    // 每次发送的数据大小 1280 字节
    private static final int CHUNKED_SIZE = 1280;

    // 数据发送间隔
    private static final int SEND_INTERVAL = 40;

    // 最大连接讯飞的客户端数量
    private static final int MAX_ONLINE = 10;

    // 当前正在与讯飞进行实时转写client数量
    private static AtomicInteger onlineCount = new AtomicInteger(0);


    // 讯飞应用id
    private static String appId;
    @Value("${xunfei.appId}")
    public void setAppId(String appId){
        setAppIdValue(appId);
    }
    public static void setAppIdValue(String appId){RTASRClient.appId = appId;}

    // 讯飞应用key
    private static String apiKey;
    @Value(("${xunfei.rt-asr.apiKey}"))
    public void setApiKey(String key){
        setApiKeyValue(key);
    }
    public static void  setApiKeyValue(String key){RTASRClient.apiKey = key;}

    private MyWebSocketClient client;

    private MyWebSocketClient2 client2;

    private CountDownLatch handshakeSuccess;

    private CountDownLatch connectClose;

    /**
     * 音频id
     */
    private String id;

    /**
     * 创建实例
     * @param audioId
     * @return
     */
    public static RTASRClient getInstance(String audioId) {
        RTASRClient rtasrClient = new RTASRClient();
        rtasrClient.setId(audioId);

        return rtasrClient;
    }

    /**
     * 与讯飞服务器握手
     * @throws URISyntaxException
     */
    public void handshake() throws URISyntaxException, InterruptedException {

        log.debug("当前连接数：{}，连接+1", onlineCount.get());
        // 握手成功后在线数加1
        onlineCount.incrementAndGet();

        URI url = new URI(BASE_URL + getHandShakeParams(appId, apiKey));
        DraftWithOrigin draft = new DraftWithOrigin(ORIGIN);

        handshakeSuccess = new CountDownLatch(1);
        connectClose = new CountDownLatch(1);

        client = new MyWebSocketClient(url, draft, handshakeSuccess, connectClose, id);

        client.connect();
        while (!client.getReadyState().equals(WebSocket.READYSTATE.OPEN)) {
            log.info("连接中，id = " + id + ", status:" + client.getReadyState());
            if(client.getReadyState().equals(WebSocket.READYSTATE.CLOSED)){
                log.debug("握手异常，断开连接");
                client.close();
                break;
            }
            Thread.sleep(1000);
        }


    }

    /**
     * 发送音频数据到实施转写
     * @param source
     * @throws InterruptedException
     */
    public void translate(byte[] source) throws InterruptedException, URISyntaxException {

        log.debug("tanslate , status:" + client.getReadyState());

        // 如果链接已关闭，则重建链接
        if(!client.getReadyState().equals(WebSocket.READYSTATE.OPEN)){
            handshake();
        }

        byte[] temp;

        int alreadyReadLen = 0;  // 已读长度
        int restLen;         // 剩余长度
        int readLen = 0;         // 每次读取长度

        while(true){
            // 计算每次读取的数据长度，不超过讯飞建议的数值
            restLen =  source.length - alreadyReadLen;
            readLen = restLen >= CHUNKED_SIZE ? CHUNKED_SIZE : readLen;
            temp =  Arrays.copyOfRange(source, alreadyReadLen, alreadyReadLen + readLen);
            alreadyReadLen += readLen;

            send(client, temp);

            if(alreadyReadLen >= source.length){
                break ;
            }

            // 每隔40毫秒发送一次数据
            Thread.sleep(SEND_INTERVAL);
        }
    }

    public void translate2(String id, String path) throws InterruptedException, URISyntaxException {

        // 握手成功后在线数加1
        log.debug("当前连接数：{}，连接+1", onlineCount.get());
        onlineCount.incrementAndGet();

        URI url = new URI(BASE_URL + getHandShakeParams(appId, apiKey));
        DraftWithOrigin draft = new DraftWithOrigin(ORIGIN);
        handshakeSuccess = new CountDownLatch(1);
        connectClose = new CountDownLatch(1);
        client2 = new MyWebSocketClient2(url, draft, handshakeSuccess, connectClose, id);

        client2.connect();

        while (!client2.getReadyState().equals(WebSocket.READYSTATE.OPEN)) {
            log.debug("\t连接中");
            Thread.sleep(1000);
        }

        // 等待握手成功
        handshakeSuccess.await();

        // 发送音频
        byte[] bytes = new byte[CHUNKED_SIZE];
        try (RandomAccessFile raf = new RandomAccessFile(path, "r")) {
            int len = -1;
            while ((len = raf.read(bytes)) != -1) {
                if (len < CHUNKED_SIZE) {
                    send(client2, bytes = Arrays.copyOfRange(bytes, 0, len));
                    break;
                }

                send(client2, bytes);
                // 每隔40毫秒发送一次数据
                Thread.sleep(40);
            }

            // 发送结束标识
            send(client2,"{\"end\": true}".getBytes(StandardCharsets.UTF_8));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            if(e instanceof InterruptedException){
                Thread.currentThread().interrupt();
            }
        }

        // 等待连接关闭
        connectClose.await();


        client2.close();
    }

    /**
     * 生成握手参数
     * @param appId
     * @param secretKey
     * @return
     */
    public static String getHandShakeParams(String appId, String secretKey) {
        String ts = System.currentTimeMillis()/1000 + "";
        String signa = "";
        try {
            signa = EncryptUtil.hmacSHA1Encrypt(EncryptUtil.md5(appId + ts), secretKey);
            return "?appid=" + appId + "&ts=" + ts + "&signa=" + URLEncoder.encode(signa, "UTF-8");
        } catch (Exception e) {
            log.error("与讯飞握手出现异常！！！", e);
        }

        return "";
    }

    /**
     * 发送消息到讯飞
     * @param client
     * @param bytes
     */
    public void send(WebSocketClient client, byte[] bytes) throws InterruptedException {
        if (client.isClosed()) {
            throw new RuntimeException("client connect closed!");
        }

        // 等待线程可用
        handshakeSuccess.await();

        client.send(bytes);
    }


    /**
     * 关闭连接
     */
    public void close()  {
        if (client.isClosed()) {
            return ;
        }

        // 发送结束信号
        try{
            send(client,"{\"end\": true}".getBytes(StandardCharsets.UTF_8));
        } catch(InterruptedException e){
            log.error(e.getMessage(), e);
            Thread.currentThread().interrupt();
        }

        client.close();
        log.debug("断开与的讯飞链");
    }

    /**
     * 获取实时转写最终结果（语句）
     * @param message
     * @return  返回最终结果,如果是中间结果,则返回null
     */
    public static RTASTResultResp getRealContent(String message){

        RTASTResultResp resultResp = null;
        try{
            JSONObject messageObj = JSON.parseObject(message);

            JSONObject cn = messageObj.getJSONObject("cn");
            JSONObject st = cn.getJSONObject("st");
            String type = st.getString("type");
            // 如果不是最终结果,则返回null
            if(!"0".equals(type)){
                return null;
            }

            StringBuilder stringBuffer = new StringBuilder();
            resultResp = new RTASTResultResp();

            int bg = Integer.parseInt(st.getString("bg"));    //句子在整段语音中的开始时间，单位毫秒(ms)
            int ed = Integer.parseInt(st.getString("ed"));    //句子在整段语音中的结束时间，单位毫秒(ms)

            resultResp.setStart(bg);
            resultResp.setEnd(ed);

            JSONArray rtArr = st.getJSONArray("rt");
            for (int i = 0; i < rtArr.size(); i++) {
                JSONObject rtArrObj = rtArr.getJSONObject(i);
                JSONArray wsArr = rtArrObj.getJSONArray("ws");
                for (int j = 0; j < wsArr.size(); j++) {
                    JSONObject wsArrObj = wsArr.getJSONObject(j);
                    JSONArray cwArr = wsArrObj.getJSONArray("cw");
                    for (int k = 0; k < cwArr.size(); k++) {
                        JSONObject cwArrObj = cwArr.getJSONObject(k);
                        String wStr = cwArrObj.getString("w");
                        stringBuffer.append(wStr);
                    }
                }
            }
            resultResp.setText(stringBuffer.toString());
        } catch (Exception e){
            log.error("讯飞实时语音转写解析失败！！！ message : " + message);
            log.error(e.getMessage(), e);
        }

        return resultResp;
    }

    /**
     * 获取实时转写最终结果（词）
     * @param message
     * @return  返回最终结果,如果是中间结果,则返回null
     */
    public static STResp getRealWords(String message){
        STResp stResp = null;

        try{
            JSONObject messageObj = JSON.parseObject(message);
            // 转写时语言默认选择的是cn，所以解析时默认使用cn的解析结果
            JSONObject cn = messageObj.getJSONObject("cn");
            JSONObject st = cn.getJSONObject("st");
            String type = st.getString("type");
            // 如果不是最终结果,则返回null
            if(!"0".equals(type)){
                return null;
            }
            // 数据赋值
            stResp = new STResp();

            stResp.setBg(Integer.parseInt(st.getString("bg")));
            stResp.setEd(Integer.parseInt(st.getString("ed")));
            stResp.setType(type);
            List<RTResp> rtRespList = new ArrayList<>();
            stResp.setRt(rtRespList);

            RTResp rtResp = null;
            CWResp cwResp = null;
            WSResp wsResp = null;
            JSONArray rtArr = st.getJSONArray("rt");
            for (int i = 0; i < rtArr.size(); i++) {
                rtResp = new RTResp();

                List<WSResp> wsRespList = new ArrayList<>();
                rtResp.setWs(wsRespList);

                JSONObject rtArrObj = rtArr.getJSONObject(i);
                JSONArray wsArr = rtArrObj.getJSONArray("ws");

                for (int j = 0; j < wsArr.size(); j++) {
                    JSONObject wsArrObj = wsArr.getJSONObject(j);

                    wsResp = new WSResp();
                    List<CWResp> cwRespList = new ArrayList<>();
                    wsResp.setCw(cwRespList);
                    JSONArray cwArr = wsArrObj.getJSONArray("cw");

                    for (int k = 0; k < cwArr.size(); k++) {
                        JSONObject cwArrObj = cwArr.getJSONObject(k);

                        cwResp = new CWResp();
                        cwResp.setW(cwArrObj.getString("w"));
                        cwResp.setWp(cwArrObj.getString("wp"));
                        cwResp.setWb(cwArrObj.getIntValue("wb"));
                        cwResp.setWe(cwArrObj.getIntValue("we"));

                        cwRespList.add(cwResp);
                    }
                    wsRespList.add(wsResp);

                }
                rtRespList.add(rtResp);
            }

        } catch(Exception e){
            log.error("讯飞实时语音转写解析失败！！！ message : " + message);
            log.error(e.getMessage(), e);
            return null;
        }

        return stResp;
    }

    /**
     * 判断与讯飞连接是否已达到讯飞并发上线
     * @return
     */
    public static boolean isConnectExceedLimit(){
        log.debug("当前连接数量：{}", onlineCount.get());
        return onlineCount.get() >= MAX_ONLINE;
    }


    /**
     * websocket client对象。
     * MyWebSocketClient  对象用于在实时录音过程中对碎片文件进行转写请求
     * MyWebSocketClient2 对象用于在录音完成后，对整体音频进行转写
     */
    public static class MyWebSocketClient extends WebSocketClient {

        private CountDownLatch handshakeSuccess;
        private CountDownLatch connectClose;
        private String id;

        public MyWebSocketClient(URI serverUri, Draft protocolDraft, CountDownLatch handshakeSuccess, CountDownLatch connectClose, String id) {
            super(serverUri, protocolDraft);
            this.handshakeSuccess = handshakeSuccess;
            this.connectClose = connectClose;
            this.id = id;
            if(serverUri.toString().contains("wss")){
                Certifications.trustAllHosts(this);
            }
            this.id = id;
        }

        @Override
        public void onOpen(ServerHandshake handshake) {
            log.debug("与讯飞websocket链接建立成功， id = " + this.id );
        }

        @Override
        public void onMessage(String msg) {
            JSONObject msgObj = JSON.parseObject(msg);

            String action = msgObj.getString("action");
            if (Objects.equals("started", action)) {
                // 握手成功
                log.debug("握手成功！sid:" + msgObj.getString("sid"));
                handshakeSuccess.countDown();
            } else if (Objects.equals("result", action)) {
                // 识别转写结果
                RTASTResultResp rtastResultResp = getRealContent(msgObj.getString("data"));

                // 忽略中间消息
                if(rtastResultResp != null){
                    log.debug("result, bg: "+rtastResultResp.getStart()+" , ed: "+rtastResultResp.getEnd()+" , text :" + rtastResultResp.getText());
                    // 转写结果写入cache
                    RTASRServiceImpl.writeToCache(id, rtastResultResp);
                }

            } else if (Objects.equals("error", action)) {
                // 连接发生错误
                log.error("讯飞消息返回error!!!, 错误内容： " + msg);
            }
        }

        @Override
        public void onError(Exception e) {
            log.error("与讯飞连接发生错误！！！！ id = " + id, e);
            connectClose.countDown();
        }

        @Override
        public void onClose(int arg0, String arg1, boolean arg2) {
            log.debug("与讯飞连接关闭, id = " + id);
            connectClose.countDown();
            // 断开后连接数-1
            log.debug("当前连接数{}，连接关闭，数量-1", onlineCount.get());
            onlineCount.decrementAndGet();
        }

        @Override
        public void onMessage(ByteBuffer bytes) {

            log.debug("讯飞服务端返回接口：" + new String(bytes.array(), StandardCharsets.UTF_8));
        }
    }

    /**
     * websocket client对象。
     * MyWebSocketClient  对象用于在实时录音过程中对碎片文件进行转写请求
     * MyWebSocketClient2 对象用于在录音完成后，对整体音频进行转写
     */
    public static class MyWebSocketClient2 extends WebSocketClient {

        private CountDownLatch handshakeSuccess;
        private CountDownLatch connectClose;
        private String id;

        public MyWebSocketClient2(URI serverUri, Draft protocolDraft, CountDownLatch handshakeSuccess, CountDownLatch connectClose, String audioId) {
            super(serverUri, protocolDraft);
            this.handshakeSuccess = handshakeSuccess;
            this.connectClose = connectClose;
            this.id = audioId;
            if(serverUri.toString().contains("wss")){
                Certifications.trustAllHosts(this);
            }
        }

        @Override
        public void onOpen(ServerHandshake handshake) {
            log.debug("与讯飞websocket链接建立成功， id = " + this.id );
        }

        @Override
        public void onMessage(String msg) {
            JSONObject msgObj = JSON.parseObject(msg);
            String action = msgObj.getString("action");
            if (Objects.equals("started", action)) {
                // 握手成功
                log.debug("握手成功！sid:" + msgObj.getString("sid"));
                handshakeSuccess.countDown();
            } else if (Objects.equals("result", action)) {
                // 识别转写结果
                RTASTResultResp rtastResultResp = getRealContent(msgObj.getString("data"));

                // 忽略中间消息
                if(rtastResultResp != null){
                    log.debug("result, bg: "+rtastResultResp.getStart()+" , ed: "+rtastResultResp.getEnd()+" , text :" + rtastResultResp.getText());
                    // 转写结果写入cache
                    RTASRServiceImpl.writeToCache2(id, rtastResultResp);
                }
//               STResp st =  getRealWords(msgObj.getString("data"));
//
//               if(st != null){
//                   log.debug("type = 1");
//                   RTASRServiceImpl.writeToCache2(id, st);
//               }

            } else if (Objects.equals("error", action)) {
                // 连接发生错误
                log.error("讯飞消息返回error!!!, 错误内容： " + msg);
            }
        }

        @Override
        public void onError(Exception e) {
            log.error("与讯飞连接发生错误！！！！ id = " + id, e);
            connectClose.countDown();
        }

        @Override
        public void onClose(int arg0, String arg1, boolean arg2) {
            log.debug("与讯飞连接关闭, id = " + id);
            connectClose.countDown();
            log.debug("当前连接数{}，连接关闭，数量-1", onlineCount.get());
            // 断开后连接数-1
            onlineCount.decrementAndGet();
        }

        @Override
        public void onMessage(ByteBuffer bytes) {

            log.debug("讯飞服务端返回接口：" + new String(bytes.array(), StandardCharsets.UTF_8));
        }


    }
}
