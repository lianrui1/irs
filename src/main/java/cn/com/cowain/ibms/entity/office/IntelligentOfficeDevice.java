package cn.com.cowain.ibms.entity.office;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.enumeration.iot.UsableStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author Yang.Lee
 * @date 2021/12/7 13:23
 */
@Data
@Entity
@Table(name = "ibms_intelligent_office_device",indexes = {@Index(columnList = "user_hr_id,office_id")})
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "ibms_intelligent_office_device", comment = "智能办公室设备表")
public class IntelligentOfficeDevice extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "office_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK'")
    private IntelligentOffice intelligentOffice;

    @ManyToOne
    @JoinColumn(name = "device_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK'")
    private IotDevice device;

    @Column(name = "usable_status", nullable = false, columnDefinition = "varchar(50) COMMENT '设备可用状态'")
    @Enumerated(EnumType.STRING)
    private UsableStatus usableStatus;

    @Column(name = "user_hr_id", nullable = false, columnDefinition = "varchar(20) COMMENT '用户工号'")
    private String userHrId;

    @Column(name = "device_alisa", columnDefinition = "varchar(20) COMMENT '设备别名'")
    private String deviceAlisa;

    @Column(name = "device_alisa_properties", columnDefinition = "varchar(2000) COMMENT '设备别名数据，json字符串'")
    private String deviceAlisaProperties;
}
