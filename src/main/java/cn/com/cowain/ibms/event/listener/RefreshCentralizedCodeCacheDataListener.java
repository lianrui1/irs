package cn.com.cowain.ibms.event.listener;

import cn.com.cowain.ibms.component.DynamicsDataComponent;
import cn.com.cowain.ibms.event.process.RefreshCentralizedCodeCacheDataEvent;
import cn.com.cowain.ibms.event.process.RefreshDynamicsDataEvent;
import cn.com.cowain.ibms.service.device.CentralizedControlService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 刷新集控码的缓存数据事件监听
 *
 * @author wei.cheng
 * @date 2022/09/16 19:17
 */
@Component
@Slf4j
public class RefreshCentralizedCodeCacheDataListener {
    @Autowired
    private CentralizedControlService centralizedControlService;

    @Autowired
    private DynamicsDataComponent dynamicsDataComponent;

    @Async
    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT, value = RefreshCentralizedCodeCacheDataEvent.class, fallbackExecution = true)
    public void refreshCentralizedCodeCacheDataListener(RefreshCentralizedCodeCacheDataEvent event) {
        log.info("监听到集体码刷新事件，id:{}", event.getCentralizedControlCode().getId());
        centralizedControlService.refreshCentralizedCodeCacheData(event.getCentralizedControlCode());
    }

    @Async
    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT, value = RefreshDynamicsDataEvent.class, fallbackExecution = true)
    public void refreshDynamicDataListener(RefreshDynamicsDataEvent event) {
        log.info("监听到删除容器集控码和人员关系事件，id:{}", event.getCentralizedUserBO().getHrId());
        dynamicsDataComponent.removeDynamicsDataMap(event.getCentralizedUserBO().getTraceId());
    }
}