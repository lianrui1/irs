package cn.com.cowain.ibms.enumeration.iot;

/**
 * 门磁设备源
 * @author Yang.Lee
 * @date 2021/6/7 19:12
 */
public enum DoorMagneticSource {

    /**
     * 华为云
     **/
    HW_CLOUD,

    /**
     * EHR
     **/
    EHR
}
