package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Yang.Lee
 * @date 2022/6/24 10:04
 */
@Data
@Entity
@Table(name = "iot_left_column_group")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_left_column_group", comment = "升降柱组表")
public class LiftColumnGroup extends BaseEntity {

    @Column(name = "group_name", nullable = false, columnDefinition = "varchar(50) COMMENT '组名称'")
    private String name;
}
