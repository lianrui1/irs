package cn.com.cowain.ibms.utils;

import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.codec.binary.Base64;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/17 16:50
 */
@Log4j2
public class PicUtils {


    /**
     * 读取图片
     *
     * @param strUrl 地址
     * @return byte[]
     */
    public static byte[] getImageFromNetByUrl(String strUrl) {
        try {
            //获取图片
            InputStream inStream = getInputStreamOfImageFromNetByUrl(strUrl);
            byte[] btImg = readInputStream(inStream);
            return btImg;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    //
//    /**
//     * 图片转换成字节流
//     * @param strUrl
//     * @return
//     */
    public static InputStream getInputStreamOfImageFromNetByUrl(String strUrl) {
        try {
            URL url = new URL(strUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(5 * 1000);
            return conn.getInputStream();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }
//

    /**
     * 转为二进制
     *
     * @param inStream 输入流
     * @return 二进制
     * @throws Exception
     */
    public static byte[] readInputStream(InputStream inStream) throws Exception {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = 0;
        while ((len = inStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, len);
        }
        inStream.close();
        return outStream.toByteArray();
    }
//
//    /**
//     * 写入磁盘
//     *
//     * @param img             图片数据流
//     * @param fileName        文件保存时的名称
//     * @param writePathPrefix 文件保存时的名称
//     */
//    public static void writeImageToDisk(byte[] img, String fileName, String writePathPrefix) {
//        try {
//            File file = new File(writePathPrefix + fileName);
//            FileOutputStream fops = new FileOutputStream(file);
//            fops.write(img);
//            fops.flush();
//            fops.close();
//            System.out.println("图片已经写入到" + writePathPrefix + "盘");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//

    /**
     * 根据指定大小压缩图片
     *
     * @param imageBytes  源图片字节数组
     * @param desFileSize 指定图片大小，单位kb
     * @param imageId     影像编号
     * @return 压缩质量后的图片字节数组
     */
    public static byte[] compressPicForScale(byte[] imageBytes, long desFileSize, String imageId) {
        //巨坑！！！这个“压缩图片方法”不支持多线程！
        if (imageBytes == null || imageBytes.length <= 0 || imageBytes.length < desFileSize * 1024) {
            return imageBytes;
        }
        long srcSize = imageBytes.length;
        double accuracy = getAccuracy(srcSize / 1024);
        try {
            while (imageBytes.length > desFileSize * 1024) {
                ByteArrayInputStream inputStream = new ByteArrayInputStream(imageBytes);
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream(imageBytes.length);
                Thumbnails.of(inputStream)
                        .scale(accuracy)
                        .outputQuality(accuracy)
                        .toOutputStream(outputStream);
                imageBytes = outputStream.toByteArray();
            }
            log.info("【图片压缩】imageId={} | 图片原大小={}kb | 压缩后大小={}kb",
                    imageId, srcSize / 1024, imageBytes.length / 1024);
        } catch (Exception e) {
            log.error("【图片压缩】msg=图片压缩失败!", e);
        }
        return imageBytes;
    }

    /**
     * 读取照片,压缩成适合海康的要求
     *
     * @return FileInputStream
     */
    @SneakyThrows
    public static InputStream getPhotoFitHIK(MultipartFile multipartFile) {
        //    byte[] btImg = PicUtils.getImageFromNetByUrl(fileUrl);
        byte[] btImg = multipartFile.getBytes();
        if (null != btImg && btImg.length > 0) {
            log.info("读取到:{} 字节",btImg.length);

//            String fileName = jobNum + "_" + DateStrUtils.getCurrentTimeStr2() + ".jpg";
            //压缩图片
            btImg = PicUtils.compressPicForScale(btImg, 200, "x");// 图片小于200kb
            InputStream input = new ByteArrayInputStream(btImg);
            return input;
        } else {
            System.out.println("没有从该连接获得内容");
        }
        return null;
    }
//
//

    /**
     * 自动调节精度(经验数值)
     *
     * @param size 源图片大小
     * @return 图片压缩质量比
     */
    private static double getAccuracy(long size) {
        double accuracy;
        if (size < 900) {
            accuracy = 0.85;
        } else if (size < 2047) {
            accuracy = 0.6;
        } else if (size < 3275) {
            accuracy = 0.44;
        } else {
            accuracy = 0.4;
        }
        return accuracy;
    }

    /**
     * 将图片流转换成Base64编码
     *
     * @param in 待处理图片流
     * @return
     */
    public static String getImgInputStream(InputStream in) {
        //将图片文件转化为字节数组字符串，并对其进行Base64编码处理
        byte[] data = null;
        //读取图片字节数组
        try {
            data = new byte[in.available()];
//            in.read(data);
            int count;
            while ((count = in.read(data)) > 0) {
                log.debug("read size :" + count);
            }

            in.close();
        } catch (IOException e) {
            log.info(e.getMessage());
        }
        return new String(Objects.requireNonNull(Base64.encodeBase64(data)), StandardCharsets.UTF_8);
    }

    /**
     * 压缩图片质量(图片尺寸不变)
     *
     * @param sourceFile 图片源文件
     * @param targetFile 压缩后图片
     * @param quality    压缩质量，0-1，越接近1质量越好
     * @return
     * @author Yang.Lee
     * @date 2022/7/11 16:55
     **/
    public static void compressQuality(File sourceFile, File targetFile, float quality) throws IOException {

        try (FileOutputStream out = new FileOutputStream(targetFile);) {
            BufferedImage srcBI = ImageIO.read(sourceFile);

            String fileExtName = FileUtil.getExt(sourceFile.getName());

            ImageWriter imageWriter = ImageIO.getImageWritersByFormatName(fileExtName).next();
            ImageWriteParam imgWriteParams = imageWriter.getDefaultWriteParam();

            imgWriteParams.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            imgWriteParams.setCompressionQuality(quality);

            ColorModel colorModel = srcBI.getColorModel();

            // 指定压缩时使用的色彩模式
            imgWriteParams.setDestinationType(new ImageTypeSpecifier(
                    colorModel, colorModel.createCompatibleSampleModel(16, 16)));


            BufferedImage targetBI = new BufferedImage(srcBI.getWidth(), srcBI.getHeight(), BufferedImage.TYPE_INT_RGB);
            targetBI.getGraphics()
                    .drawImage(srcBI.getScaledInstance(srcBI.getWidth(), srcBI.getHeight(), Image.SCALE_SMOOTH),
                            0,
                            0,
                            null);

            imageWriter.reset();
            imageWriter.setOutput(ImageIO.createImageOutputStream(out));
            imageWriter.write(null, new IIOImage(targetBI, null, null), imgWriteParams);

            srcBI.flush();
            out.flush();
            imageWriter.dispose();
        }
    }
}
