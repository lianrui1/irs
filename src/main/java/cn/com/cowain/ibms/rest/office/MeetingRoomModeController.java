package cn.com.cowain.ibms.rest.office;

import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.req.working_mode.ModeChangeReq;
import cn.com.cowain.ibms.rest.req.working_mode.WebWorkingModeSaveReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.office.TbDataDetailResp;
import cn.com.cowain.ibms.rest.resp.working_mode.DeviceTypeResp;
import cn.com.cowain.ibms.rest.resp.working_mode.ScreenWorkingModeDetailResp;
import cn.com.cowain.ibms.rest.resp.working_mode.WebWorkingModePageResp;
import cn.com.cowain.ibms.service.office.MeetingRoomModelService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.utils.IConst;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author feng
 * @title: MeetingRoomModeController
 * @projectName ibms
 * @Date 2021/12/27 18:34
 * 模式相关
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/roommode")
@Api(tags = IConst.INTELLIGENT_MEETING)
public class MeetingRoomModeController {
    @Autowired
    private MeetingRoomModelService meetingRoomModelService;
    /**
     * @title
     * @Description 模式创建
     * @author jf.sui
     * @Date
     */
    @PostMapping
    @ApiOperation(value = "添加工作模式场景-web", tags = {IConst.INTELLIGENT_MEETING, IConst.V22})
    public ResponseEntity<JsonResult<IdResp>> save(@RequestBody WebWorkingModeSaveReq req, BindingResult bindingResult) {
        ServiceResult result=meetingRoomModelService.save(req,bindingResult);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @PutMapping("/update/{id}")
    @ApiOperation(value = "修改工作模式场景-web", tags = {IConst.INTELLIGENT_MEETING, IConst.V22})
    public ResponseEntity<JsonResult<String>> update(@PathVariable @ApiParam(value = "工作模式ID", name = "id") String id, @RequestBody @Validated WebWorkingModeSaveReq req, BindingResult bindingResult) {

        ServiceResult result=  meetingRoomModelService.update(id,req);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @GetMapping("/info/{id}")
    @ApiOperation(value = "查询工作模式场景详情-web", tags = {IConst.INTELLIGENT_MEETING, IConst.V22})
    public ResponseEntity<JsonResult<ScreenWorkingModeDetailResp>> detail(@PathVariable @ApiParam("工作模式ID") String id) {
        ServiceResult result= meetingRoomModelService.findDetail(id);
        return ResponseEntity.ok(JsonResult.ok("OK" ,(ScreenWorkingModeDetailResp)result.getObject()));
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "删除工作模式场景-web", tags = {IConst.INTELLIGENT_MEETING, IConst.V22})
    public ResponseEntity<JsonResult<String>> delete(@PathVariable @ApiParam("工作模式ID") String id) {
        ServiceResult result=  meetingRoomModelService.delete(id);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }
    @GetMapping("/list")
    @ApiOperation(value = "查询工作模式场景列表-web", tags = {IConst.INTELLIGENT_OFFICE_APPLET, IConst.V22})
    public ResponseEntity<JsonResult<List<WebWorkingModePageResp>>> getPage() {
        return ResponseEntity.ok(JsonResult.ok("OK" ,meetingRoomModelService.getModelList()));
    }
    @PostMapping("/controller")
    @ApiOperation(value = "切换模式", tags = {IConst.INTELLIGENT_OFFICE_APPLET, IConst.V22})
    public ResponseEntity<JsonResult<String>> chageModel(@RequestBody ModeChangeReq req) {
        ServiceResult result= meetingRoomModelService.changeMode(req);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @GetMapping("/typelist")
    @ApiOperation(value = "获取类型列表", tags = {IConst.INTELLIGENT_OFFICE_APPLET, IConst.V22})
    public ResponseEntity<JsonResult<List<DeviceTypeResp>>> gettypelist() {
        List<DeviceTypeResp> typeList=new ArrayList<>();
        typeList.add(new DeviceTypeResp(DeviceType.AIR_CONDITIONER));
        typeList.add(new DeviceTypeResp(DeviceType.DOOR_MAGNETIC));
        typeList.add(new DeviceTypeResp(DeviceType.FOUR_WAY_SWITCH));
        typeList.add(new DeviceTypeResp(DeviceType.CURTAINS));
        return ResponseEntity.ok(JsonResult.ok("OK",typeList));
    }

    @GetMapping("/getsensordata/{appid}")
    @ApiOperation(value = "获取传感器数据", tags = {IConst.INTELLIGENT_OFFICE_APPLET, IConst.V22})
    public ResponseEntity<JsonResult<TbDataDetailResp>> getTdData(@PathVariable @ApiParam("appid") String appid) {
        ServiceResult result=  meetingRoomModelService.getSensorData(appid);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.ok("OK",(TbDataDetailResp)result.getObject()));
    }
}
