package cn.com.cowain.ibms.rest;

import cn.com.cowain.ibms.exceptions.RemoteServiceUnavailableException;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.ibms.service.EhrRemoteService;
import cn.com.cowain.ibms.utils.IConst;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * 手动调试控制器
 *
 * @author Hu Jingling
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/debug")
@Api(tags = IConst.MODULE_ELSE)
public class ManualDebugController {

    private EhrRemoteService ehrRemoteService;

    public ManualDebugController(EhrRemoteService ehrRemoteService) {
        this.ehrRemoteService = ehrRemoteService;
    }

    /**
     *
     */
    @GetMapping("/check/ehr")
    @ApiOperation(value = "手动检查 EHR 连接", tags = IConst.MODULE_ELSE)
    public ResponseEntity<JsonResult<String>> run() {
        log.info("Checking EHR Service..");
        boolean isOk = ehrRemoteService.serviceCheck();
        if (isOk) {
            log.info("EHR Service OK");
            JsonResult<String> jsonResult = JsonResult.okNoData("OK on " + new Date());
            return ResponseEntity.ok(jsonResult);
        } else {
            throw new RemoteServiceUnavailableException("EHR Service Unavailable");
        }
    }

}
