package cn.com.cowain.ibms.rest.distinguish;

import cn.com.cowain.ibms.annotation.Idempotent;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.distinguish.*;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.distinguish.*;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.disthinguish.AccessRulesService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author suwenhao
 * @date 2021/3/6 10:46
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/access")
@Api(tags = IConst.DISTINGUISH)
public class AccessRulesController {

    @Resource
    private AccessRulesService accessRulesService;

    /**
     * 查询通信权限
     *
     * @return
     * @author Yang.Lee
     * @date 2021/3/10 17:07
     **/
    @GetMapping("/page")
    @ApiOperation(value = "PC查询通行权限列表(分页)", tags = IConst.DISTINGUISH)
    public ResponseEntity<JsonResult<PageBean<AccessRulePageResp>>> find(
            @RequestParam(required = false) @ApiParam(value = "设备名称，模糊查询") String deviceName,
            @RequestParam(required = false) @ApiParam(value = "人脸识别设备ID") String deviceId,
            @RequestParam(required = false) @ApiParam(value = "项目ID") String projectId,
            @RequestParam(value = "page", defaultValue = "0") @ApiParam(value = "页码，从0开始，默认0") int page,
            @RequestParam(value = "size", defaultValue = "10") @ApiParam(value = "页长，默认10") int size) {


        PageBean<AccessRulePageResp> pageBean = accessRulesService.getPage(deviceName, deviceId, projectId, page, size);

        return ResponseEntity.ok(JsonResult.ok("OK", pageBean));
    }


    /**
     * 新建通行权限
     *
     * @param accessRulesReq 请求参数
     * @param bindingResult  参数校验对象
     * @return api返回结果
     * @author Yang.Lee
     * @date 2021/3/10 13:12
     **/
    @Idempotent(API_BASE + "/access")
    @PostMapping
    @ApiOperation(value = "PC新增人员组通行权限", tags = IConst.DISTINGUISH)
    public ResponseEntity<JsonResult<IdResp>> save(@RequestBody @Validated @ApiParam("通行权限信息") AccessRulesReq accessRulesReq,
                                                   BindingResult bindingResult) {

        ServiceResult result = accessRulesService.save(accessRulesReq);
        if (!result.isSuccess()) {
            String errorCode = Optional.ofNullable(result.getErrorCode()).orElse(ErrConst.E01);
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, errorCode));
        }

        return ResponseEntity.ok(JsonResult.ok("OK", (IdResp) result.getObject()));
    }


    /**
     * 更新通行权限数据
     *
     * @param id             权限ID
     * @param accessRulesReq 请求参数
     * @param bindingResult  参数校验对象
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/3/10 15:45
     **/
    @PutMapping("/{id}")
    @ApiOperation(value = "PC更新通行权限信息", tags = IConst.DISTINGUISH)
    public ResponseEntity<JsonResult<String>> update(@PathVariable @ApiParam("通行权限ID") String id,
                                                     @RequestBody @Validated @ApiParam("通行权限信息") AccessRulesReq accessRulesReq,
                                                     BindingResult bindingResult) {

        ServiceResult result = accessRulesService.update(id, accessRulesReq);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.ok("OK", null));
    }


    /**
     * 查询通行权限
     *
     * @param id 权限ID
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/3/12 17:17
     **/
    @GetMapping("/detail/{id}")
    @ApiOperation(value = "PC查询通行权限信息", tags = IConst.DISTINGUISH)
    public ResponseEntity<JsonResult<AccessRuleDetailResp>> search(@PathVariable @ApiParam("通行权限ID") String id) {

        return ResponseEntity.ok(JsonResult.ok("ok", accessRulesService.get(id)));
    }

    /**
     * 删除通行记录
     *
     * @param id 权限ID
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/3/11 10:23
     **/
    @DeleteMapping("/{id}")
    @ApiOperation(value = "PC删除通行权限信息", tags = IConst.DISTINGUISH)
    public ResponseEntity<JsonResult<String>> delete(@PathVariable @ApiParam("通行权限ID") String id) {

        ServiceResult result = accessRulesService.delete(id);

        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.ok("ok", null));
    }

    @PostMapping("/open")
    public ResponseEntity<JsonResult<ServiceResult>> open(@RequestBody String data) {
        log.info("旷视云回调数据" + data);
        JsonResult<ServiceResult> jsonResult = JsonResult.okNoData("OK");
        // 解析数据
        JSONObject jsonObject = JSON.parseObject(data);
        JSONArray jsonIndex = jsonObject.getJSONArray("data");
        JSONObject partDaily = jsonIndex.getJSONObject(0);
        ServiceResult result = accessRulesService.open(partDaily);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error("操作失败", result, null));
        }
        return ResponseEntity.ok(jsonResult);
    }

    // 添加单个人员
    @PostMapping("/add/user")
    public ResponseEntity<JsonResult<String>> addUser(@RequestBody HkUserAddReq req) {

        ServiceResult result = accessRulesService.addHkUser(req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @GetMapping("/hk/user/page")
    @ApiOperation(value = "PC查询HK人员列表(分页)", tags = IConst.DISTINGUISH)
    public ResponseEntity<JsonResult<PageBean<HkUserPageResp>>> hkUserList(
            @RequestParam(required = false) @ApiParam(value = "人员姓名/工号") String name,
            @RequestParam @ApiParam(value = "设备ID", required = true) String deviceId,
            @RequestParam(value = "page", defaultValue = "0") @ApiParam(value = "页码，从0开始，默认0") int page,
            @RequestParam(value = "size", defaultValue = "10") @ApiParam(value = "页长，默认10") int size) {


        PageBean<HkUserPageResp> pageBean = accessRulesService.hkUserList(name, deviceId, page, size);

        return ResponseEntity.ok(JsonResult.ok("OK", pageBean));
    }

    @GetMapping("/hk/face/record")
    @ApiOperation(value = "PC查询HK设备通行记录(分页)", tags = IConst.DISTINGUISH)
    public ResponseEntity<JsonResult<PageBean<HkFaceRecordPageResp>>> hkFaceRecord(HkFaceRecordPageReq req) {


        PageBean<HkFaceRecordPageResp> pageBean = accessRulesService.hkFaceRecord(req);
        if (null == pageBean.getList()) {
            return ResponseEntity.ok(JsonResult.error("设备不存在", null, null));
        }

        return ResponseEntity.ok(JsonResult.ok("OK", pageBean));
    }

    @GetMapping("/download/hk/face/record")
    @ApiOperation(value = "PC导出HK设备通行记录", tags = {IConst.DISTINGUISH, IConst.V20})
    public void downloadHkFaceRecord(HttpServletResponse response, HkFaceRecordPageReq req) {


        PageBean<HkFaceRecordPageResp> pageBean = accessRulesService.hkFaceRecord(req);

        byte[] bytes = accessRulesService.downloadHkFaceRecord(pageBean);

        response.addHeader(IConst.CONTENT_TYPE, IConst.APPLICATION);
        response.addHeader(IConst.CONTENT_DISPOSITION, "attachment;filename=hkRecord.xlsx");
        response.addHeader(IConst.CONTENT_LENGTH, String.valueOf(bytes.length));
        OutputStream stream = null;
        try {
            stream = response.getOutputStream();
            stream.write(bytes);
            stream.flush();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }

    }

    @GetMapping("/del/hk/user")
    @ApiOperation(value = "删除人员", tags = {IConst.DISTINGUISH, IConst.V20})
    public ResponseEntity<JsonResult<String>> deleteHkUser(String deviceId, String jobNum) {

        List<String> jobNos = new ArrayList<>();
        jobNos.add(jobNum);
        ServiceResult result = accessRulesService.deleteHkUser(deviceId, jobNos);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(result.getObject().toString(), null, null));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @PutMapping("/hk/user/time")
    @ApiOperation(value = "更新人员通行时间", tags = {IConst.DISTINGUISH, IConst.V20})
    public ResponseEntity<JsonResult<String>> updateUserHkTime(@RequestBody @Validated UserTimeReq req) {

        ServiceResult result = accessRulesService.updateUserHkTime(req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(result.getObject().toString(), null, null));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @PutMapping("/hk/face/update")
    @ApiOperation(value = "更新人脸", tags = IConst.DISTINGUISH)
    public ResponseEntity<JsonResult<String>> updateFace(@RequestBody @Validated HkUpdateFaceReq req) {

        ServiceResult result = accessRulesService.updateFace(req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(result.getObject().toString(), null, null));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @PostMapping("/upload/accessRules")
    @ApiOperation(value = "批量添加通行人员", tags = "${project.sprint}")
    public ResponseEntity<JsonResult<Object>> uploadAccessRules(MultipartFile file, String deviceId) {


        String[] split = deviceId.split(",");
        List<String> list = Arrays.asList(split);
        List<String> deviceIds = new ArrayList<>(list);
        ServiceResult result = accessRulesService.uploadAccessRules(file, deviceIds);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(result.getObject().toString(), null, result.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.ok(result.getObject()));
    }

    @PostMapping("/upload/del")
    @ApiOperation(value = "批量删除通行人员", tags = "${project.sprint}")
    public ResponseEntity<JsonResult<Object>> uploadDeleteAccessRules(MultipartFile file, String deviceId) {


        String[] split = deviceId.split(",");
        List<String> list = Arrays.asList(split);
        List<String> deviceIds = new ArrayList<>(list);
        ServiceResult result = accessRulesService.uploadDeleteAccessRules(file, deviceIds);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(result.getObject().toString(), null, result.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.ok(result.getObject()));
    }

    @GetMapping("/hk/result")
    @ApiOperation(value = "查询海康失败结果", tags = "${project.sprint}")
    public ResponseEntity<JsonResult<HkResultResp>> hkResult(ImportFailedReq req) {


        ServiceResult result = accessRulesService.hkResult(req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(result.getObject().toString(), null, result.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.ok((HkResultResp) result.getObject()));
    }

    @GetMapping("/download/importFailed/add")
    @ApiOperation(value = "下载批量添加导入失败的数据", tags = "${project.sprint}")
    public void getUploadAccessRulesErrorFile(HttpServletResponse response, @RequestParam String errorDataId) {


        byte[] bytes = accessRulesService.getImportErrorDataFileByte(errorDataId);

        response.addHeader(IConst.CONTENT_TYPE, IConst.APPLICATION);
        response.addHeader(IConst.CONTENT_DISPOSITION, "attachment;filename=" + errorDataId + ".xlsx");
        response.addHeader(IConst.CONTENT_LENGTH, String.valueOf(bytes.length));
        OutputStream stream = null;
        try {
            stream = response.getOutputStream();
            stream.write(bytes);
            stream.flush();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }


    }

    @GetMapping("/download/importFailed/del")
    @ApiOperation(value = "下载批量删除导入失败的数据", tags = "${project.sprint}")
    public void getUploadDelAccessRulesErrorFile(HttpServletResponse response, @RequestParam String errorDataId) {


        byte[] bytes = accessRulesService.getDelUserImportErrorDataFileByte(errorDataId);

        response.addHeader("content-type", "application/octet-stream;charset=UTF-8");
        response.addHeader("Content-Disposition", "attachment;filename=" + errorDataId + ".xlsx");
        response.addHeader("Content-Length", "" + bytes.length);
        OutputStream stream = null;
        try {
            stream = response.getOutputStream();
            stream.write(bytes);
            stream.flush();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }

    @PostMapping("/user/face")
    @ApiOperation(value = "更新人脸照片并下发", tags = {IConst.DISTINGUISH, IConst.V18})
    public ResponseEntity<JsonResult<String>> updateUserFace(@RequestBody @Validated UserFaceReq req) {

        ServiceResult result = accessRulesService.updateUserFace(req);
        if (!result.isSuccess() && "2".equals(result.getErrorCode())) {
            return ResponseEntity.ok(JsonResult.error(result.getObject().toString(), null, "2"));
        }
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(result.getObject().toString(), null, null));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @PostMapping("/hk/user/time")
    @ApiOperation(value = "批量下发人员", tags = IConst.DISTINGUISH)
    public ResponseEntity<JsonResult<String>> updateManyUserHkTime(@RequestBody @Validated ManyUserTimeReq req) {

        ServiceResult result = accessRulesService.updateManyUserHkTime(req);
        if (!result.isSuccess() && "2".equals(result.getErrorCode())) {
            return ResponseEntity.ok(JsonResult.error(result.getObject().toString(), null, "2"));
        }
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(result.getObject().toString(), null, null));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @GetMapping("/staff/page")
    @ApiOperation(value = "PC人员列表(分页)", tags = {IConst.DISTINGUISH, IConst.V18})
    public ResponseEntity<JsonResult<PageBean<StaffPageResp>>> findByStaff(@RequestParam(required = false) @ApiParam(value = "姓名/工号 模糊查询") String keyword,
                                                                           @RequestParam(required = false) @ApiParam(value = "部门") String dept,
                                                                           @RequestParam(required = false) @ApiParam(value = "是否员工") Integer isStaff,
                                                                           @RequestParam(value = "page", defaultValue = "0") @ApiParam(value = "页码，从0开始，默认0") int page,
                                                                           @RequestParam(value = "size", defaultValue = "20") @ApiParam(value = "页长，默认20") int size) {


        Sort sort = Sort.by(Sort.Direction.DESC, "createdTime");
        Pageable pageable = PageRequest.of(page, size, sort);
        PageBean<StaffPageResp> staffPage = accessRulesService.findByStaff(keyword, pageable, dept, isStaff);

        return ResponseEntity.ok(JsonResult.ok("OK", staffPage));
    }

    @GetMapping("/hk/device/page")
    @ApiOperation(value = "hk设备列表(分页)", tags = {IConst.DISTINGUISH, IConst.V18})
    public ResponseEntity<JsonResult<PageBean<HkDevicePageResp>>> findByHkDevice(@RequestParam(required = false) @ApiParam(value = "设备位置/名称 模糊查询") String keyword,
                                                                                 @RequestParam(required = false) @ApiParam(value = "项目或空间") String nodeType,
                                                                                 @RequestParam(required = false) @ApiParam(value = "项目或空间ID") String id,
                                                                                 @RequestParam(value = "page", defaultValue = "0") @ApiParam(value = "页码，从0开始，默认0") int page,
                                                                                 @RequestParam(value = "size", defaultValue = "20") @ApiParam(value = "页长，默认20") int size) {


        Sort sort = Sort.by(Sort.Direction.DESC, "createdTime");
        Pageable pageable = PageRequest.of(page, size, sort);
        PageBean<HkDevicePageResp> staffPage = accessRulesService.findByHkDevice(keyword, pageable, nodeType, id);

        return ResponseEntity.ok(JsonResult.ok("OK", staffPage));
    }
}
