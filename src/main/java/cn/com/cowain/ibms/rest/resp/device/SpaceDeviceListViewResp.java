package cn.com.cowain.ibms.rest.resp.device;

import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

/**
 * @Author tql
 * @Description 楼宇总览 出参类
 * @Date 21-11-4 下午3:13
 * @Version 1.0
 */
@Data
@Builder
@ApiModel("空间列表")
public class SpaceDeviceListViewResp {

    /**
     * 设备类型
     */
    @ApiModelProperty(value = "设备类型", example = "DOOR_PLATE(门牌)", position = 1)
    private DeviceType deviceType;

    /**
     * 设备类型
     */
    @ApiModelProperty(value = "设备类型描述", example = "DOOR_PLATE(门牌)", position = 1)
    private String deviceTypeDesc;

    /**
     * 设备id
     */
    @ApiModelProperty(value = "设备id", example = "1", position = 2)
    private String id;

    /**
     * 描述
     */
    @ApiModelProperty(value = "设备状态", example = "1", position = 3)
    private DeviceStatus hwStatus;

    /**
     * 设备id
     */
    @ApiModelProperty(value = "设备名", example = "1", position = 4)
    private String name;
}
