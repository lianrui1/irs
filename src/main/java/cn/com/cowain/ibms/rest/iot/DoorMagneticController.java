package cn.com.cowain.ibms.rest.iot;

import cn.com.cowain.ibms.annotation.Idempotent;
import cn.com.cowain.ibms.enumeration.ApprovalStatus;
import cn.com.cowain.ibms.enumeration.PermissionApplicationStatus;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.iot.DoorMagneticPageReq;
import cn.com.cowain.ibms.rest.req.iot.doorMagnetic.OpenDoorReq;
import cn.com.cowain.ibms.rest.resp.BaseListResp;
import cn.com.cowain.ibms.rest.resp.StaffResp;
import cn.com.cowain.ibms.rest.resp.iot.*;
import cn.com.cowain.ibms.rest.resp.iot.doorMagnetic.DeviceStatusAndUserPermissionResp;
import cn.com.cowain.ibms.rest.resp.iot.doorMagnetic.DoorMagneticStatusResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.iot.DoorMagneticService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author Yang.Lee
 * @date 2021/4/19 9:47
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/doorMagnetic")
@Api(tags = {IConst.MODULE_DOOR_MAGNETIC})
public class DoorMagneticController {

    @Resource
    private DoorMagneticService doorMagneticService;

    /**
     * 获取门磁列表
     *
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/4/19 11:08
     **/
    @GetMapping("/search/list")
    @ApiOperation(value = "获取门磁列表", tags = IConst.MODULE_DOOR_MAGNETIC)
    public ResponseEntity<JsonResult<List<BaseListResp>>> getList(@RequestParam(required = false) @ApiParam(value = "项目ID") String projectId,
                                                                  @RequestParam(required = false) @ApiParam(value = "空间ID") String spaceId) {

        return ResponseEntity.ok(JsonResult.ok("OK", doorMagneticService.getList(projectId, spaceId)));
    }

    /**
     * 获取门磁列表（分页）
     *
     * @param req 请求参数
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/10/11 9:59
     **/
    @GetMapping("/search/page")
    @ApiOperation(value = "获取门磁列表(分页)", tags = {IConst.MODULE_DOOR_MAGNETIC, IConst.V17})
    public ResponseEntity<JsonResult<PageBean<DoorMagneticPageResp>>> getPage(DoorMagneticPageReq req) {

        return ResponseEntity.ok(JsonResult.ok("OK", doorMagneticService.getPage(req)));
    }

    /**
     * 下载二维码
     *
     * @param deviceId 设备ID
     * @author Yang.Lee
     * @date 2021/10/11 10:05
     **/
    @GetMapping("/qrcode/download/{deviceId}")
    @ApiOperation(value = "下载二维码", tags = {IConst.MODULE_DOOR_MAGNETIC, IConst.V17})
    public void downloadQrcode(@PathVariable @ApiParam("设备ID") String deviceId) {
        throw new UnsupportedOperationException();

    }


    /**
     * 获取门磁详情
     *
     * @param doorMagneticId 设备ID
     * @author Yang.Lee
     * @date 2021/10/11 10:05
     **/
    @GetMapping("/{doorMagneticId}")
    @ApiOperation(value = "查询门磁详情", tags = {IConst.MODULE_DOOR_MAGNETIC, IConst.V17})
    public ResponseEntity<JsonResult<DoorMagneticDetailResp>> getDetail(@PathVariable @ApiParam("门磁ID") String doorMagneticId) {

        return ResponseEntity.ok(JsonResult.ok("OK", doorMagneticService.get(doorMagneticId)));
    }

    /**
     * 编辑门磁
     *
     * @param doorMagneticId 门磁ID
     * @param req            请求参数
     * @return 操作结果
     * @author Yang.Lee
     * @date 2021/10/19 18:14
     **/
    @PutMapping("/{doorMagneticId}")
    @ApiOperation(value = "修改门磁信息", tags = {IConst.MODULE_DOOR_MAGNETIC, IConst.V17})
    public ResponseEntity<JsonResult<String>> update(@PathVariable @ApiParam("门磁ID") String doorMagneticId, @RequestBody DoorMagneticEditReq req) {

        ServiceResult result = doorMagneticService.update(doorMagneticId, req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, result.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.ok(null));
    }


    /**
     * 根据静态二维码查询设备的状态
     *
     * @param dm 静态二维码
     * @return 查询结果
     * @author Yang.Lee
     * @date 2022/7/7 17:31
     **/
    @GetMapping("/{dm}/status")
    @ApiOperation(value = "根据门磁编号查询设备状态", tags = {IConst.MODULE_DOOR_MAGNETIC, IConst.MODULE_DOOR_MAGNETIC_V2})
    public ResponseEntity<JsonResult<DoorMagneticStatusResp>> checkStatusFromDM(@PathVariable
                                                                                @ApiParam(required = true, value = "静态二维码门磁编号")
                                                                                        String dm) {

        Optional<DoorMagneticStatusResp> statusRespOptional = doorMagneticService.getStatus(dm);

        if (statusRespOptional.isEmpty()) {
            return ResponseEntity.ok(JsonResult.error("未找到设备或设备状态查询失败", null, ErrConst.E06));
        }

        return ResponseEntity.ok(JsonResult.ok(statusRespOptional.get()));
    }

    /**
     * 根据动态二维码查询设备的状态
     *
     * @param doorPlateId 门牌ID
     * @return 查询结果
     * @author Yang.Lee
     * @date 2022/7/7 17:31
     **/
    @GetMapping("/dynamic/{doorPlateId}/status")
    @ApiOperation(value = "根据门磁编号查询设备状态", tags = {IConst.MODULE_DOOR_MAGNETIC, IConst.MODULE_DOOR_MAGNETIC_V2})
    public ResponseEntity<JsonResult<DoorMagneticStatusResp>> checkStatusFromDoorPlateId(@PathVariable
                                                                                         @ApiParam(required = true, value = "动态二维码的门牌编号")
                                                                                                 String doorPlateId) {

        throw new UnsupportedOperationException();
    }
    /**
     * 检查用户权限
     *
     * @param dm  门磁编号
     * @return 查询结果
     * @author Yang.Lee
     * @date 2022/7/13 11:17
     **/
    @GetMapping("/{dm}/status_device")
    @ApiOperation(value = "查询门磁的设备状态", tags = {IConst.MODULE_DOOR_MAGNETIC, IConst.MODULE_DOOR_MAGNETIC_V2})
    public ResponseEntity<JsonResult<DeviceStatusAndUserPermissionResp>> checkDeviceStatus(@PathVariable @ApiParam(required = true, value = "静态二维码门磁编号") String dm) {
        // 判断设备状态
        Optional<DoorMagneticStatusResp> statusRespOptional = doorMagneticService.getStatus(dm);

        if (statusRespOptional.isEmpty()) {
            return ResponseEntity.ok(JsonResult.error("未找到设备或设备状态查询失败", null, ErrConst.E06));
        }

        return ResponseEntity.ok(JsonResult.ok(DeviceStatusAndUserPermissionResp.builder()

                .deviceStatus(statusRespOptional.get())
                .build()));
    }
    /**
     * 检查用户权限
     *
     * @param dm  门磁编号
     * @param req 请求参数
     * @return 查询结果
     * @author Yang.Lee
     * @date 2022/7/13 11:17
     **/
    @GetMapping("/{dm}/status_permission")
    @ApiOperation(value = "检查用户对于门磁的权限", tags = {IConst.MODULE_DOOR_MAGNETIC, IConst.MODULE_DOOR_MAGNETIC_V2})
    public ResponseEntity<JsonResult<DeviceStatusAndUserPermissionResp>> checkUserPermissionStatus(@PathVariable
                                                                                                      @ApiParam(required = true, value = "静态二维码门磁编号")
                                                                                                              String dm,
                                                                                                      @Validated OpenDoorReq req,
                                                                                                      BindingResult bindingResult) {

        // 查询权限
        ServiceResult result;
        if (StringUtils.isEmpty(req.getDoorPlateId())) {
            // 静态二维码开门
            log.info("=======================================================");
            log.info("进入静态二维码扫码开门");
            result = doorMagneticService.checkStaticPermission(dm, req.getUid());
            log.info("静态二维码扫码开门结束，返回结果：{}",result);
            log.info("=======================================================");
        } else {
            // 动态二维码开门
            result = doorMagneticService.checkDynamicPermission(dm, req.getUid(), req.getDoorPlateId());
        }

        if (!result.isSuccess() && !ErrConst.E401.equals(result.getErrorCode())) {
            return ResponseEntity.ok(JsonResult.error((String) result.getObject(), null, result.getErrorCode()));
        }

        // 获取二维码的审批管理员列表
        List<StaffResp> adminList = doorMagneticService.getDoorMagneticApprovalAdminList(dm);

        List<StaffResp> deviceAdminList = doorMagneticService.getDoorMagneticDeviceAdminList(dm);

        if (ErrConst.E401.equals(result.getErrorCode()) || !(boolean) result.getObject()) {
            if(StringUtils.equals(PermissionApplicationStatus.CURRENT_TIME_NO_PERMISSION.name(), result.getErrorCode())){
                return ResponseEntity.ok(JsonResult.ok(DeviceStatusAndUserPermissionResp.builder()
                        .status(PermissionApplicationStatus.CURRENT_TIME_NO_PERMISSION)
                        .approvalAdminList(adminList)
                        .deviceAdminList(deviceAdminList)
                        .build()));
            }

            // 用户无权限，查询用户是否有正在审批的申请
            List<AccessControlSpotApplicationRecordResp> recordRespList = doorMagneticService.getUserApplyRecordList(req.getUid(), dm);

            // 按时间倒叙排，找到最新的一次记录
            Optional<AccessControlSpotApplicationRecordResp> lastRecordOptional = recordRespList.stream()
                    .sorted(Comparator.comparing(AccessControlSpotApplicationRecordResp::getCreatedTime).reversed())
                    .findFirst();

            PermissionApplicationStatus status = PermissionApplicationStatus.NO_PERMISSION;
            if(lastRecordOptional.isPresent()){

                // 判断是否是正在审批的记录
                AccessControlSpotApplicationRecordResp lastRecord = lastRecordOptional.get();

                if(ApprovalStatus.PENDING.equals(lastRecord.getStatusType())){
                    status = PermissionApplicationStatus.UNDER_APPROVAL;
                }
            }


            return ResponseEntity.ok(JsonResult.ok(DeviceStatusAndUserPermissionResp.builder()
                    .approvalAdminList(adminList)
                    .deviceAdminList(deviceAdminList)
                    .status(status)
                    .build()));
        }

        return ResponseEntity.ok(JsonResult.ok(
                DeviceStatusAndUserPermissionResp.builder()
                        .status(PermissionApplicationStatus.PASS)
                        .approvalAdminList(adminList)
                        .deviceAdminList(deviceAdminList)
                        .build()));
    }

    /**
     * 设备异常消息推送
     *
     * @param dm 门磁编号
     * @return 推送结果
     * @author Yang.Lee
     * @date 2022/7/13 11:22
     **/
    @Idempotent(value = "/doorMagnetic/exception/message/post", expireTime = 2)
    @PostMapping("/{dm}/exception/message")
    @ApiOperation(value = "设备异常消息推送", tags = {IConst.MODULE_DOOR_MAGNETIC, IConst.MODULE_DOOR_MAGNETIC_V2})
    public ResponseEntity<JsonResult<String>> sendMessageToAdmin(@PathVariable
                                                                 @ApiParam(required = true, value = "静态二维码门磁编号")
                                                                         String dm,
                                                                 @RequestBody OpenDoorReq req) {
        ServiceResult result = doorMagneticService.sendErrorMessageToAdmin(dm, req.getUid());

        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error((String) result.getObject(), null, result.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    /**
     * 门磁开门(V2.0)
     *
     * @param dm  门磁编号
     * @param req 开门参数
     * @return 开门结果
     * @author Yang.Lee
     * @date 2022/7/13 13:20
     **/
    @PutMapping("/{dm}/open")
    @ApiOperation(value = "开门", tags = {IConst.MODULE_DOOR_MAGNETIC, IConst.MODULE_DOOR_MAGNETIC_V2})
    public ResponseEntity<JsonResult<String>> openDoor(@PathVariable
                                                       @ApiParam(required = true, value = "门磁编号")
                                                               String dm,
                                                       @RequestBody @Validated @ApiParam("开门请求参数") OpenDoorReq req, BindingResult bindingResult) {

        String openType = StringUtils.isBlank(req.getDoorPlateId()) ? "static" : "dynamic";

        ServiceResult result = doorMagneticService.openV2(dm, req.getUid(), openType, req.getDoorPlateId());

        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error((String) result.getObject(), null, result.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    /**
     * 用户申请某一设备的权限
     *
     * @param dm  门磁编号
     * @param req 参数
     * @return 查询结果
     * @author Yang.Lee
     * @date 2022/7/13 11:17
     **/
    @Idempotent(value = "/doorMagnetic/permission/post", expireTime = 2L)
    @PostMapping("/{dm}/permission")
    @ApiOperation(value = "权限申请", tags = {IConst.MODULE_DOOR_MAGNETIC_V2})
    public ResponseEntity<JsonResult<String>> permissionApply(@PathVariable
                                                              @ApiParam(required = true, value = "静态二维码门磁编号")
                                                                      String dm,
                                                              @RequestBody OpenDoorReq req) {

        ServiceResult result = doorMagneticService.permissionApply(dm, req.getUid());

        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error((String) result.getObject(), null, result.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }
}
