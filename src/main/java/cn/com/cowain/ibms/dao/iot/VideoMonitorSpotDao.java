package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.VideoMonitorSpot;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author feng
 * @title: YSVideoCameraDao
 * @projectName bims
 * @Date 2021/11/24 11:12
 */
public interface VideoMonitorSpotDao extends JpaRepository<VideoMonitorSpot, String>, JpaSpecificationExecutor<VideoMonitorSpot> {

     Page<VideoMonitorSpot> getBySpaceId(String id, Pageable pageable);
    VideoMonitorSpot getByDeviceId(String id);
}
