package cn.com.cowain.ibms.websocket.enumeration;

/**
 * websocket Action枚举常量
 * @author Yang.Lee
 * @date 2021/1/12 10:36
 */
public enum WSActionType {
    /**
     * 实时语音翻译
     */
    RTASR,

    /**
     * 会议
     **/
    MEETING,

    /**
     * 会议预约
     **/
    MEETING_RESERVATION,

    /**
     * 录音
     **/
    RECORDING,

    /**
     * 结束录音
     **/
    STOP_RECORDING,


    /**
     * 结束会议
     **/
    END_MEETING;
}
