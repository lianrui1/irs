package cn.com.cowain.ibms.enumeration.meeting;

/**
 * @author Yang.Lee
 * @date 2021/8/17 10:45
 */
public enum DoorMagneticAccessResult {

    ACCESS("通行"),

    NO_ACCESS("未通行");

    private String name;

    DoorMagneticAccessResult(String name) {
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
}
