package cn.com.cowain.ibms.enumeration;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/5 16:45
 */
public enum  ReservationRecordType {

    /**
     * 内部
     */
    STAFF,


    /**
     * 访客
     */
    VISITOR
}
