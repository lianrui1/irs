package cn.com.cowain.ibms.rest.resp.device.centralized;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 集控码列表返回参数
 *
 * @author: yanzy
 * @date: 2022/9/16 15:22
 */
@Data
@ApiModel(value = "集控码列表返回参数")
public class CentralizedControlCodeResp {

    @ApiModelProperty(value = "集控码id", required = true, position = 1)
    private String id;

    @ApiModelProperty(value = "集控码名称", required = true, position = 2)
    private String name;

    @ApiModelProperty(value = "控制区域", required = true, position = 3)
    private String controlArea;

    @ApiModelProperty(value = "管理员工号")
    private String adminHrId;

    @ApiModelProperty(value = "管理员姓名")
    private String adminName;

    @ApiModelProperty(value = "控制空调个数", position = 4)
    private Long countAir;

    @ApiModelProperty(value = "控制灯个数", position = 5)
    private Long countLamp;
}
