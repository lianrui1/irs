package cn.com.cowain.ibms.rest.resp.office;

import cn.com.cowain.ibms.rest.resp.device.AirConditionerResp;
import lombok.Data;

/**
 * @author feng
 * @title: AirConditionerStatusDetailResp
 * @projectName ibms
 * @Date 2021/12/21 14:07
 */
@Data
public class AirConditionerStatusDetailResp extends AirConditionerResp {
    String id;
    String hwDeviceId;
    String hwStatus;
    private int temperature;
    private String windDirection;
    private String setFanAuto;
    private String  windSpeed;
    private String mode;
    private String deviceName;
    private String setSwing;
    private String setPower;

}
