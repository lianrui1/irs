package cn.com.cowain.ibms.entity.qrcode;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.distinguish.TimePlan;
import cn.com.cowain.ibms.entity.iot.DoorMagnetic;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * 静态二维码通行权限实体
 *
 * @author Yang.Lee
 * @date 2021/4/6 14:51
 **/
@Data
@Entity
@Table(name = "ibms_qrcode_access_rules")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_qrcode_access_rules", comment = "静态二维码通行权限表")
@Where(clause = "is_del=0")
public class QrcodeAccessRules extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "door_magnetic_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 门磁设备id'")
    private DoorMagnetic doorMagnetic;

    @ManyToOne
    @JoinColumn(name = "time_plan_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 时间计划ID'")
    private TimePlan timePlan;
}
