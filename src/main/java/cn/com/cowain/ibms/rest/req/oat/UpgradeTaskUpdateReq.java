package cn.com.cowain.ibms.rest.req.oat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * @author Yang.Lee
 * @date 2021/4/14 18:13
 */
@Data
@ApiModel("app升级任务更新请求参数")
public class UpgradeTaskUpdateReq {

    @Max(20)
    @NotEmpty
    @ApiModelProperty(value = "升级任务名称", required = true, example = "task name", position = 1)
    private String taskName;

    @NotNull
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "升级时间", required = true, example = "3000-12-31 09:30", position = 2)
    private LocalDateTime upgradeTime;

    @ApiModelProperty(value = "更新内容", example = "DOORPLATE_PAD", position = 3)
    private String content;

    @ApiModelProperty(value = "强制更新，默认否", example = "false", position = 4)
    private boolean forceUpdate = false;
}
