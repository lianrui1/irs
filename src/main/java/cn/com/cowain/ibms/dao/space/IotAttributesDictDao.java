package cn.com.cowain.ibms.dao.space;

import cn.com.cowain.ibms.entity.space.IotAttributesDict;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Description
 *
 * @author yuchunlei
 * @version 1.0.0
 * @createTime 2020年12月22日 10:28:00
 */
@Repository
public interface IotAttributesDictDao extends JpaRepository<IotAttributesDict, String> {
}
