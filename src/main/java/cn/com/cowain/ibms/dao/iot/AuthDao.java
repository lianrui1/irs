package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.Auth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author wei.cheng
 * @date 2022/06/29 18:15
 */
public interface AuthDao extends JpaRepository<Auth, String>, JpaSpecificationExecutor<Auth> {
}
