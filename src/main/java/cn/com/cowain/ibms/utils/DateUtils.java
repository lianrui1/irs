package cn.com.cowain.ibms.utils;

import cn.com.cowain.ibms.utils.enums.DateUnit;
import lombok.Value;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;
import java.util.Optional;

/**
 * 时间工具类
 *
 * @author 崔恩铭
 * @version 1.0
 * @date 2020/11/23 11:24
 */
@UtilityClass
@Value
@Slf4j
public class DateUtils {
    /**
     * 时候分秒
     */
    public static final String PATTERN_DATETIME = "yyyy-MM-dd HH:mm:ss";
    /**
     * 时候分
     */
    public static final String PATTERN_DATETIME1 = "yyyy-MM-dd HH:mm";
    /**
     * 日期
     */
    public static final String PATTERN_DATE = "yyyy-MM-dd";
    /**
     * 时分秒
     */
    public static final String PATTERN_TIME = "HH:mm:ss";
    /**
     * 时分
     */
    public static final String PATTERN_TIME1 = "HH:mm";
    /**
     * 格式
     */
    public static final String DATE_PATTERN = "yyyyMMdd";

    public static final String DATE_PATTERN2 = "yyyyMMddHHmmss";


    public static final String PATTERN_DATETIME_WITH_ZONE = "yyyy-MM-dd'T'HH:mm:ssXXX";

    public static final String PATTERN_DATETIME2_WITH_ZONE = "yyyyMMdd'T'HHmmss'Z'";


    public static final String PATTERN_YEAR_MONTH = "yyyy-MM";


    public static final String GMT8 = "GMT+8";

    /**
     * 格式化时间
     *
     * @param dateStr
     * @param pattern
     * @return 默认返回时间格式（yyyy-MM-dd) LocalDate
     */
    public LocalDate parseLocalDate(String dateStr, String pattern) {
        if (StringUtils.isBlank(dateStr)) {
            return null;
        }

        if (StringUtils.isBlank(pattern)) {
            pattern = PATTERN_DATE;
        }

        return LocalDate.parse(dateStr, DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * 格式化时间
     *
     * @param dateStr
     * @param pattern
     * @return 默认返回时间格式（yyyy-MM-dd HH:mm:ss) LocalDateTime
     */
    public LocalDateTime parseLocalDateTime(String dateStr, String pattern) {
        if (StringUtils.isBlank(dateStr)) {
            return null;
        }

        if (StringUtils.isBlank(pattern)) {
            pattern = PATTERN_DATETIME;
        }

        return LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern(pattern));
    }


    /**
     * localDate转换为Date时间
     *
     * @param localDate
     * @return
     */
    public Date localDateConvert(LocalDate localDate) {

        LocalDate currentDate = Optional.ofNullable(localDate).orElse(LocalDate.now());

        ZoneId zoneId = ZoneId.systemDefault();
        ZonedDateTime zdf = currentDate.atStartOfDay(zoneId);

        return Date.from(zdf.toInstant());
    }


    /**
     * localDateTime转换为Date
     *
     * @param localDateTime
     * @return
     */
    public Date localDateTimeConvert(LocalDateTime localDateTime) {
        //如果设置为空,择获取当前时间
        LocalDateTime currentDate = Optional.ofNullable(localDateTime)
                .orElse(LocalDateTime.now());

        ZoneId zoneId = ZoneId.systemDefault();
        ZonedDateTime zdf = currentDate.atZone(zoneId);

        return Date.from(zdf.toInstant());
    }


    /**
     * 将LocalDate 格式化
     *
     * @param localDate
     * @param pattern
     * @return
     */
    public String formatLocalDate(LocalDate localDate, String pattern) {
        //如果设置为空,择获取当前时间
        LocalDate currentDate = Optional.ofNullable(localDate)
                .orElse(LocalDate.now());

        String currentPattern = Optional.ofNullable(pattern).orElse(DateUtils.PATTERN_DATE);

        DateTimeFormatter ofPattern = DateTimeFormatter.ofPattern(currentPattern);

        return ofPattern.format(currentDate);
    }


    /**
     * 将localDateTime格式化
     *
     * @param localDateTime
     * @param pattern
     * @return
     */
    public String formatLocalDateTime(LocalDateTime localDateTime, String pattern) {
        //如果设置为空,择获取当前时间
        LocalDateTime currentDate = Optional.ofNullable(localDateTime)
                .orElse(LocalDateTime.now());

        String currentPattern = Optional.ofNullable(pattern).orElse(DateUtils.PATTERN_DATE);

        DateTimeFormatter ofPattern = DateTimeFormatter.ofPattern(currentPattern);

        return ofPattern.format(currentDate);
    }


    /**
     * 统计两个时间的相隔时间
     *
     * @param startTime
     * @param endTime
     * @return
     */
    public Long betweenTime(LocalDateTime startTime, LocalDateTime endTime, DateUnit dateUnit) {

        DateUnit currentUnit = Optional.ofNullable(dateUnit).orElse(DateUnit.MILLIS);
        Long time = null;
        Duration between = Duration.between(startTime, endTime);
        switch (currentUnit) {
            case MILLIS:
                time = between.toMillis();
                break;
            case SECOND:
                time = between.toSeconds();
                break;
            case HOUR:
                time = between.toHours();
                break;
            case DAY:
                time = between.toDays();
                break;
            case MINUTE:
                time = between.toMinutes();
                break;
            default:
                log.info("未找到对应的处理信息");
                break;
        }

        return time;
    }


    /**
     * 统计两个时间的相隔时间
     *
     * @param startTime
     * @param endTime
     * @return
     */
    public Long between(LocalDate startTime, LocalDate endTime, DateUnit dateUnit) {

        DateUnit currentUnit = Optional.ofNullable(dateUnit).orElse(DateUnit.MILLIS);
        Long time = null;
        Duration between = Duration.between(startTime, endTime);
        if (currentUnit.equals(DateUnit.DAY)) {
            time = between.toDays();
        } else {
            log.info("未找到对应的处理信息");
        }
        return time;
    }

    /**
     * 获取本月最后一天时间
     *
     * @param localDate
     * @return
     */
    public LocalDate lastDayLocalDate(LocalDate localDate) {
        LocalDate today = Optional.ofNullable(localDate).orElse(LocalDate.now());

        return today.with(TemporalAdjusters.lastDayOfMonth());
    }


    /**
     * 获取本月最后一天时间
     *
     * @param localDate
     * @return
     */
    public LocalDateTime lastDayLocalDateTime(LocalDateTime localDate) {
        LocalDateTime today = Optional.ofNullable(localDate).orElse(LocalDateTime.now());

        return today.with(TemporalAdjusters.lastDayOfMonth());
    }


    /**
     * 获取对比时间
     *
     * @param localDate
     * @param day
     * @return
     */
    public LocalDate plusDays(LocalDate localDate, Integer day) {
        LocalDate today = Optional.ofNullable(localDate).orElse(LocalDate.now());

        return today.plusDays(day);
    }

    /**
     * @param localDate
     * @param day
     * @return
     */
    public LocalDateTime plusDaysTime(LocalDateTime localDate, Integer day) {
        LocalDateTime today = Optional.ofNullable(localDate).orElse(LocalDateTime.now());

        return today.plusDays(day);
    }

    /**
     * LocalTime转String
     *
     * @param localTime
     * @param pattern
     * @return
     */
    public String formatLocalTime(LocalTime localTime, String pattern) {
        return localTime.format(DateTimeFormatter.ofPattern(pattern));
    }

    public String formatZoneDateTime(ZonedDateTime zonedDateTime, String pattern) {

        return zonedDateTime.format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * 判断时间段是否在目标时间段范围内
     *
     * @param sourceStartTime 待判定时间段的起始时间
     * @param sourceEndTime   待判定时间段的结束时间
     * @param rangeStartTime  时间范围起始时间
     * @param rangeEndTime    时间范围结束时间
     * @return 若带判定的时间段在目标时间段范围内，返回1；否则返回0
     * @author Yang.Lee
     * @date 2022/4/20 21:47
     **/
    public int isTimeInRange(LocalDateTime sourceStartTime, LocalDateTime sourceEndTime, LocalDateTime rangeStartTime, LocalDateTime rangeEndTime) {

        if ((sourceStartTime.isAfter(rangeStartTime) || sourceStartTime.equals(rangeStartTime))
                && (sourceEndTime.isBefore(rangeEndTime) || sourceEndTime.equals(rangeEndTime))) {
            return 1;
        } else {
            return 0;
        }
    }
}