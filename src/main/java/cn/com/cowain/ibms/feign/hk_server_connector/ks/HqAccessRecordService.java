package cn.com.cowain.ibms.feign.hk_server_connector.ks;

import cn.com.cowain.ibms.feign.hk_server_connector.ks.fallback.HqAccessRecordServiceFallBack;
import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.req.hk.FaceRecord;
import cn.com.cowain.ibms.rest.resp.EquipmentDataResp;
import cn.com.cowain.ibms.rest.resp.SearchStaffDataResp;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @Author tql
 * @Description 通行记录service
 * @Date 21-5-20 下午7:18
 * @Version 1.0
 */
@FeignClient(value = "ms-hk-server-connector", url = "${cowain.gateway.address}", fallback = HqAccessRecordServiceFallBack.class)
public interface HqAccessRecordService {


    /**
     * 获取考勤记录
     *
     * @param faceRecord 开始时间/结束时间 example:2021-05-15
     * @return
     */
    @PostMapping("/ks/hk-device/api/v1/faceData/faceRecord")
    JsonResult<List<SearchStaffDataResp>> searchStaff(@RequestBody FaceRecord faceRecord);


    /**
     * 心跳检测
     *
     * @return
     */
    @GetMapping("/ks/hk-device/api/v1/ping")
    JsonResult<String> healthCheck();

    /**
     * 获取设备列表
     *
     * @return
     */
    @GetMapping("/ks/hk-device/api/v1/sysData/sysRecordEquipmentList")
    JsonResult<List<EquipmentDataResp>> getEquipmentList();
}
