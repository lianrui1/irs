package cn.com.cowain.ibms.service.wps;

import cn.com.cowain.ibms.rest.req.wps.WpsOnnotifyReq;
import cn.com.cowain.ibms.service.bean.ServiceResult;

/**
 * @author wei.cheng
 * @date 2022/02/28 09:50
 */
public interface WpsCallbackService {
    /**
     * WPS平台通过回调请求来获取文件信息
     * 详情见 https://wwo.wps.cn/docs/server/callback-api-standard/get-file-metadata/
     *
     * @param appId  应用ID
     * @param fileId 文件的唯一标识
     * @param token  用户token
     * @param scene  场景，{@link cn.com.cowain.ibms.enumeration.wps.WpsScene}
     * @return
     */
    ServiceResult getFileInfo(String appId, String fileId, String token, String scene);

    /**
     * 打开或关闭文件时，WPS 会回调一些通知
     * 详情见 https://wwo.wps.cn/docs/server/callback-api-standard/open-and-close-file-notifications/
     *
     * @param appId  应用ID
     * @param fileId 文件的唯一标识
     * @param token  用户token
     * @param scene  场景，{@link cn.com.cowain.ibms.enumeration.wps.WpsScene}
     * @param req    req
     * @return
     */
    ServiceResult onNotify(String appId, String fileId, String token, String scene, WpsOnnotifyReq req);
}
