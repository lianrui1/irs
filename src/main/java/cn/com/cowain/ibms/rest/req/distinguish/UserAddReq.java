package cn.com.cowain.ibms.rest.req.distinguish;

import lombok.Data;

import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/15 10:56
 */
@Data
public class UserAddReq {
    private  List<String>  key;
    private  List<AuthConfigData> data;
    private  String  over;
}
