package cn.com.cowain.ibms.rest.resp.distinguish;

import cn.com.cowain.ibms.entity.iot.DeviceAccessRecord;
import cn.com.cowain.ibms.enumeration.iot.PersonType;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 访客resp
 *
 * @author: yanzy
 * @date: 2022/5/17 09:39
 */
@Data
@Builder
@ApiModel("访客查询")
public class VisitorAccessRecordResp {

    @ApiModelProperty(value = "工号")
    private String hrId;

    @ApiModelProperty(value = "类型 1代表入 2代表出")
    private Integer type ;

    @ApiModelProperty(value = "地点")
    private String checkinSite;

    @ApiModelProperty(value = "设备名称")
    private String ccDeviceId;

    @ApiModelProperty(value = "人员类型，1代表内部员工 2代表劳务工 3代表其他")
    private int personType;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "刷卡时间")
    private LocalDateTime checkinTime;

    private String ccDeviceCode;

    public static VisitorAccessRecordResp convert(DeviceAccessRecord record) {

        return VisitorAccessRecordResp.builder()
                .hrId(record.getEmpNo())
                // 不再使用设备sn作为唯一标识，使用门禁点ID
                .ccDeviceId(record.getIotDevice().getDeviceName())
                .checkinSite(record.getIotDevice().getProject().getProjectName())
                .type(0)
                .personType(PersonType.EMPLOYEE.equals(record.getStaffType()) ? 1 : 2)
                .checkinTime(record.getDataTime())
                .build();
    }
}
