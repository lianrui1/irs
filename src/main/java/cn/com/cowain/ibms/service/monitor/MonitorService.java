package cn.com.cowain.ibms.service.monitor;

import cn.com.cowain.ibms.entity.iot.VideoScreenshot;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.iot.MonitorReq;
import cn.com.cowain.ibms.rest.req.iot.ScreenShotReq;
import cn.com.cowain.ibms.rest.resp.LocationNodeResp;
import cn.com.cowain.ibms.rest.resp.MonitorLocationResp;
import cn.com.cowain.ibms.rest.resp.MonitorSitResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;

import java.util.List;

/**
 * @author feng
 * @title: MonitorService
 * @projectName bims
 * @Date 2021/11/24 10:55
 */
public interface MonitorService {
    PageBean<MonitorLocationResp> getMonitorList(MonitorReq req);
    Boolean createMonitorSit(MonitorSitResp sitResp);
    List<LocationNodeResp> getListMonitorSitList();
    PageBean<MonitorSitResp> getMonitorListBySportId(String spotId,MonitorReq req) ;
    MonitorLocationResp getMonitorDetail(String  monitorId) ;
    Boolean deleteSpot(String id);
    String upLoadPictureSave (ScreenShotReq req);
    PageBean<VideoScreenshot> creenshotList(MonitorReq req);
    ServiceResult deleteImage (String id);
    String checkoutName(String name);

}
