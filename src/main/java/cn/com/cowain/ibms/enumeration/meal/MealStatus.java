package cn.com.cowain.ibms.enumeration.meal;

/**
 * @author Yang.Lee
 * @date 2022/1/27 17:44
 */
public enum MealStatus {

    DOING("下发中"),

    SUCCESS("下发结束"),

    FAILED("下发失败");

    MealStatus(String name) {
        this.name = name;
    }

    private String name;

    public String getName() {
        return this.name;
    }
}
