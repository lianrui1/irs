package cn.com.cowain.ibms.service;

import cn.com.cowain.ibms.entity.ReservationRecord;

import java.util.Set;

/**
 * 查询即将要开始的会议
 *
 * @author 张宇鑫
 * @version 1.0
 * @since 2020 8/17/20
 */
public interface MeetingPollService {

    /**
     * 查询 timeMinute 后要开始的会议
     * @param timeMinute
     * @return
     */
    Set<ReservationRecord> getUpcomingMeeting(Integer timeMinute);

    /**
     * 查询 timeMinute 后要结束的会议
     * @param timeMinute
     * @return
     */
    Set<ReservationRecord> getUpcomingMeetingEnd(Integer timeMinute);
}
