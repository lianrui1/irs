package cn.com.cowain.ibms.rest.req.iot;

import cn.com.cowain.ibms.enumeration.iot.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author Yang.Lee
 * @date 2021/11/7 15:25
 */
@Data
@ApiModel("门禁点编辑请求")
public class AccessControlSpotEditReq {

    @Length(max = 10)
    @NotBlank
    @ApiModelProperty(value = "名称", required = true, example = "门禁点1", position = 1)
    private String name;

    @NotBlank
    @ApiModelProperty(value = "所属空间ID", required = true, example = "123", position = 2)
    private String spaceId;

    @Length(max = 100)
    @NotBlank
    @ApiModelProperty(value = "具体位置", required = true, example = "电梯出门左边", position = 3)
    private String address;

    @ApiModelProperty(value = "设备ID", example = "456", position = 4)
    private String deviceId;

    @NotNull
    @ApiModelProperty(value = "门禁等级。NORMAL:普通门禁;MANAGER:高管门禁", required = true, example = "NORMAL", position = 5)
    private AccessControlSpotLevel spotLevel;

    @ApiModelProperty(value = "门禁点类型。DOOR_MAGNETIC:门磁;HIK_FACE_MACHINE:海康人脸识别设备;OTHERS:其他", example = "NORMAL", position = 6)
    private AccessControlSpotType spotType;

    @ApiModelProperty(value = "门禁点权限类型。ALL_PASS:全部开放;STAFF_PASS:全员工开放;OTHERS:其他", example = "STAFF_PASS", position = 7)
    private AuthType authType;

    @ApiModelProperty(value = "门禁点地点。KS(\"昆山\"),\n" +
            "NT(\"南通\"),\n" +
            "NT_SY(\"南通善营\"),\n" +
            "DL(\"大连\"),\n" +
            "WF(\"潍坊\"),\n" +
            "NB(\"宁波\"),\n" +
            "DG(\"东莞\"),\n" +
            "KS_NEW(\"昆山新工厂\"),\n" +
            "OTHER(\"其他\");", example = "NORMAL", position = 8)
    private AccessControlSpotPlace accessControlSpotPlace;

    @ApiModelProperty(value = "门禁点进出类型。IN(\"进\"),\n" +
            "OUT(\"出\"),\n" +
            "OTHER(\"其他\");", example = "NORMAL", position = 9)
    private AccessControlInOut inOut;
}
