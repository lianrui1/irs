package cn.com.cowain.ibms.entity.meeting;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author Yang.Lee
 * @date 2021/12/27 10:51
 */
@Data
@Entity
@Table(name = "ibms_intelligent_meeting_app_mode_status")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "ibms_intelligent_meeting_app_mode_status", comment = "会议大屏模式状态表")
public class IntelligentMeetingAppModeStatus extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "mode_id", nullable = false, columnDefinition = "varchar(36) COMMENT '工作模式ID'")
    private IntelligentMeetingAppMode mode;

    @Enumerated(EnumType.STRING)
    @Column(name = "device_type", nullable = false, columnDefinition = "varchar(50) COMMENT '设备类型'")
    private DeviceType deviceType;

    @Column(name = "properties", nullable = false, columnDefinition = "varchar(500) COMMENT '设备属性'")
    private String properties;
}
