package cn.com.cowain.ibms.rest;

import cn.com.cowain.ibms.dao.iot.AccessControlSpotDao;
import cn.com.cowain.ibms.entity.iot.AccessControlSpot;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.annotation.Resource;
import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/11/20
 */
@WebMvcTest(TimeController.class)
class TimeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Resource()
    private AccessControlSpotDao accessControlSpotDao;

    @Test
    void getToday() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/ibms/api/v1/time"))
                .andReturn();
        System.out.println(mvcResult);
        mvcResult.getResponse().getContentAsString();
    }






}
