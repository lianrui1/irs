package cn.com.cowain.ibms.enumeration.ota;

import lombok.Getter;

/**
 * 应用枚举
 *
 * @author Yang.Lee
 * @date 2021/4/14 17:15
 */
@Getter
public enum Application {

    DOORPLATE_PAD("智能会议"),

    MEETING_AUDIO_APP("智能语音助手");

    private String name;

    Application(String name){
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
