package cn.com.cowain.ibms.service.impl;

import cn.com.cowain.ibms.entity.MessagePoolRecord;
import cn.com.cowain.ibms.service.MessageSendService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/3/24 19:59
 */
@Slf4j
@Service
public class MessageSendServiceImpl implements MessageSendService {

    @Value("${cowain.send.message.url}")
    private String sendMgsUrl;

    @Autowired
    private RestTemplate restTemplate;

    @Async
    @Override
    @Transactional
    public void send(MessagePoolRecord messagePoolRecord) {


        // json参数
        String requestParam = JSON.toJSONString(messagePoolRecord);
        log.debug("json参数 ： " + requestParam);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity<>(requestParam, headers);
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = restTemplate.postForEntity(sendMgsUrl, requestEntity, String.class);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            log.error("消息推送失败" + messagePoolRecord.getReceivePerson());
            return;
        }

        // 解析请求结果
        String responseBody = responseEntity.getBody();
        JSONObject jsonResult = JSON.parseObject(responseBody);
        String data = jsonResult.getString("data");
        log.info(data);
        log.info("消息推送成功");
    }
}
