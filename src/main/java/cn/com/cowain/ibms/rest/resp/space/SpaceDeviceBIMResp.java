package cn.com.cowain.ibms.rest.resp.space;

import cn.com.cowain.ibms.rest.resp.iot.IotDeviceResp;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Yang.Lee
 * @date 2021/11/18 22:15
 */
@Data
@ApiModel("BIM模型中空间下设备数据返回")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SpaceDeviceBIMResp {

    @ApiModelProperty(value = "空间信息")
    private SpaceDetailResp spaceDetailResp;

    @ApiModelProperty(value = "设备详情")
    private IotDeviceResp iotDeviceResp;
}
