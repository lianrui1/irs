package cn.com.cowain.ibms.rest.resp;

import cn.com.cowain.ibms.entity.Area;
import cn.com.cowain.ibms.enumeration.space.AreaType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 区域信息响应对象
 *
 * @author Yang.Lee
 * @date 2020/12/17 17:51
 */
@Data
@ApiModel("区域信息响应对象")
public class AreaResp {

    @ApiModelProperty(value = "区域名称", required = true)
    private String name;

    @ApiModelProperty(value = "区域编号", required = true, position = 1)
    private String code;

    @ApiModelProperty(value = "区域类型", required = true, example="PROVINCE: 省； CITY：市； DISTRICT：区", position = 2)
    private AreaType type;

    @ApiModelProperty(value = "区域层级", required = true, position = 3)
    private Integer level;

    @ApiModelProperty(value = "区域ID", required = true, position = 4)
    private Long id;

    //上一层级Id(for JPA)
    @ApiModelProperty(value = "区域父ID", required = true, position = 5)
    private Area parent;

}
