package cn.com.cowain.ibms.rest.req.device;

import lombok.Data;

import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/1 14:55
 */
@Data
public class ServiceCapability {

    /**
     * 华为设备的服务ID。
     */
    private String serviceId;

    /**
     * 设备服务支持的命令列表。
     */
    private List<ServiceCommand> commands;
}
