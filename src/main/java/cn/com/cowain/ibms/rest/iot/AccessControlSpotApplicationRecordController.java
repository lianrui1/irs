package cn.com.cowain.ibms.rest.iot;

import cn.com.cowain.ibms.annotation.Idempotent;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.Sorts;
import cn.com.cowain.ibms.rest.req.iot.AccessControlSpotApplicationRecordParamReq;
import cn.com.cowain.ibms.rest.req.iot.AccessControlSpotApplicationRecordReq;
import cn.com.cowain.ibms.rest.req.iot.HandlingAccessControlSpotApplicationRecordReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.iot.AccessControlSpotApplicationRecordResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.iot.AccessControlSpotApplicationRecordService;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author wei.cheng
 * @date 2022/03/08 14:57
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/accessControlSpotApplicationRecord")
@Api(tags = IConst.ACCESS_CONTROL_SPOT_APPLICATION)
public class AccessControlSpotApplicationRecordController {

    @Resource
    private AccessControlSpotApplicationRecordService accessControlSpotApplicationRecordService;

    /**
     * 门禁点权限申请
     *
     * @param token
     * @param req
     * @param bindingResult
     * @author wei.cheng
     * @date 2022/3/9 4:34 下午
     **/
    @PostMapping
    @ApiOperation(value = "门禁点权限申请", tags = {IConst.ACCESS_CONTROL_SPOT_APPLICATION, IConst.V26})
    public ResponseEntity<JsonResult<IdResp>> createAccessControlSpotApplicationRecord(@RequestHeader(name = "Authorization") @ApiParam(value = "当前用户token") String token,
                                                                                       @RequestBody @Validated AccessControlSpotApplicationRecordReq req,
                                                                                       BindingResult bindingResult) {
        ServiceResult result = accessControlSpotApplicationRecordService.createAccessControlSpotApplicationRecord(token, req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, result.getErrorCode()));
        }
        return ResponseEntity.ok(JsonResult.ok((IdResp) result.getObject()));

    }

    /**
     * 查询门禁点权限申请详情
     *
     * @param id
     * @author wei.cheng
     * @date 2022/3/9 4:35 下午
     **/
    @GetMapping("/{id}")
    @ApiOperation(value = "查询门禁点权限申请详情", tags = {IConst.ACCESS_CONTROL_SPOT_APPLICATION, IConst.V26})
    public ResponseEntity<JsonResult<AccessControlSpotApplicationRecordResp>> getAccessControlSpotApplicationRecord(@PathVariable("id") String id) {
        ServiceResult result = accessControlSpotApplicationRecordService.getAccessControlSpotApplicationRecord(id);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, result.getErrorCode()));
        }
        return ResponseEntity.ok(JsonResult.ok((AccessControlSpotApplicationRecordResp) result.getObject()));
    }

    /**
     * 门禁点权限申请审批
     *
     * @param token
     * @param id
     * @param req
     * @return
     */
    @Idempotent(value = "/accessControlSpotApplicationRecord/handling")
    @PostMapping("/{id}/handling")
    @ApiOperation(value = "门禁点权限申请审批", tags = {IConst.ACCESS_CONTROL_SPOT_APPLICATION, IConst.V26})
    public ResponseEntity<JsonResult<String>> handlingAccessControlSpotApplication(@RequestHeader(name = "Authorization") @ApiParam(value = "当前用户token") String token,
                                                                                   @PathVariable("id") String id,
                                                                                   @RequestBody @Validated HandlingAccessControlSpotApplicationRecordReq req,
                                                                                   BindingResult bindingResult) {
        ServiceResult result = accessControlSpotApplicationRecordService.handlingAccessControlSpotApplication(token, id, req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, result.getErrorCode()));
        }
        return ResponseEntity.ok(JsonResult.ok((String) result.getObject()));
    }

    /**
     * 查询申请|分配记录分页列表
     *
     * @return 查询结果
     * @author: yanzy
     * @param: req 查询参数
     * @data: 2022/3/8 13:03:19
     */
    @GetMapping("/page")
    @ApiOperation(value = "申请|分配记录分页查询列表", tags = {IConst.ACCESS_CONTROL_SPOT_APPLICATION, IConst.V26})
    public ResponseEntity<JsonResult<PageBean<AccessControlSpotApplicationRecordResp>>> getPage(AccessControlSpotApplicationRecordParamReq req) {
        req.addOrderDesc(Sorts.CREATED_TIME);
        PageBean<AccessControlSpotApplicationRecordResp> page = accessControlSpotApplicationRecordService.getPage(req);
        return ResponseEntity.ok(JsonResult.ok(page));
    }
}
