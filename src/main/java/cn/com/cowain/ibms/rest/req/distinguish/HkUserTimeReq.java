package cn.com.cowain.ibms.rest.req.distinguish;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/27 9:11
 */
@Data
public class HkUserTimeReq {

    @ApiModelProperty(value = "添加权限是否结束 1结束 0未结束", required = true)
    private String over;//start push hikservice flag

    @ApiModelProperty(value = "人员工号", required = true)
    List<AuthConfigData> data;

    @ApiModelProperty(value = "ip", required = true)
    private String ip;
}
