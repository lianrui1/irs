package cn.com.cowain.ibms.service.impl;

import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.feign.bean.pass_v2.PassV2UserResp;
import cn.com.cowain.ibms.rest.resp.distinguish.StaffPageResp;
import cn.com.cowain.ibms.service.EhrRemoteService;
import cn.com.cowain.ibms.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * 启用改类，改用SysUserServiceUCImpl。
 * Description
 *
 * @author 张宇鑫
 * @version 1.0
 * @date 2020/8/11 12:45
 */
@Slf4j
@Service("sysUserService")
@Deprecated
public class SysUserServiceImpl implements SysUserService {

    private final EhrRemoteService ehrRemoteService;

    public SysUserServiceImpl(EhrRemoteService ehrRemoteService) {
        this.ehrRemoteService = ehrRemoteService;

    }

//    @Override
//    @Transactional
//    public SysUser trySaveIfNeed(String sysId) {
//        Optional<SysUser> sysUserOptional = sysUserDao.findBySysId(sysId);
//        if (sysUserOptional.isPresent()) {
//            log.debug("已存在用户信息...跳过");
//            SysUser sysUser = sysUserOptional.get();
//
//            if(sysUser.getUcUserId() == null || sysUser.getUcUserId().length() == 0){
//                log.debug("数据库中数据无UCid , 同步Uc id");
//                JsonResult<PassportRespForFeign> ucUser = msUserApi.passport(sysUser.getEmpNo());
//                if(ucUser != null){
//                    sysUser.setUcUserId(ucUser.getData().getUserId());
//                    sysUserDao.save(sysUser);
//                }
//            }
//
//            return sysUserOptional.get();
//        } else {
//            log.debug("未找到用户信息...");
//            //从EHR查询员工信息
//            SysUser sysUser = ehrRemoteService.getInfoById(sysId);
//            // 获取ucID
//            JsonResult<PassportRespForFeign> ucUser = msUserApi.passport(sysUser.getEmpNo());
//
//            if(ucUser != null){
//                sysUser.setUcUserId(ucUser.getData().getUserId());
//            }
//
//            //保存至本地
//            sysUserDao.save(sysUser);
//            log.debug("方法trySaveIfNeed保存用戶信息成功，sysid：" + sysId);
//            return sysUser;
//        }
//    }

//    @Override
//    public SysUser trySaveIfNeedWithUserId(String userId) {
//        //从EHR查询员工信息
//        SysUser sysUser = ehrRemoteService.getInfoByUserId(userId);
//        Optional<SysUser> sysUserOptional = sysUserDao.findBySysId(sysUser.getSysId());
//        if (sysUserOptional.isPresent()) {
//            log.debug("已存在用户信息...跳过");
//            return sysUserOptional.get();
//        } else {
//            log.debug("未找到用户信息...");
//            //保存至本地
//            sysUserDao.save(sysUser);
//            log.debug("方法trySaveIfNeedWithUserId保存用戶信息成功，sysid：" + sysUser.getSysId());
//            return sysUser;
//        }
//    }

    /**
     * 通过工号查询用户信息，并存入数据库中
     *
     * @param empNo : 员工工号
     * @return 员工信息
     * @Author Yang.Lee
     * @date 2021/3/3 10:52
     **/
    @Override
    public SysUser trySaveIfNeed(String empNo) {
        return null;
    }

    /**
     * 根据员工编号获取员工信息
     *
     * @param empNo
     * @return
     */
    @Override
    public SysUser getUserByEmpNo(String empNo) {
        return ehrRemoteService.getUserByHrId(empNo);
    }

    /**
     * 根据token获取用户信息
     *
     * @param token
     * @return
     */
    @Override
    public SysUser getUserByToken(String token) {
        return null;
    }

    /**
     * 获取所有用户信息（包含所有员工及访客）
     *
     * @return 员工列表
     * @author Yang.Lee
     * @date 2021/12/14 10:57
     **/
    @Override
    public List<StaffPageResp> syncStaffList() {


        return new ArrayList<>();
    }

    @Override
    public List<StaffPageResp> syncGuestList() {
        return new ArrayList<>();
    }

    /**
     * 根据Uid查询用户信息
     *
     * @param uid ucId
     * @return 用户数据，查询不到时返回Optional.empty()
     * @author Yang.Lee
     * @date 2022/7/14 14:58
     **/
    @Override
    public Optional<PassV2UserResp> getUserByUid(String uid) {

        return Optional.empty();
    }

    //    @Override
//    @Transactional
//    public void tryToCreateRedis(SysUser sysUser) {
//        String sysId = sysUser.getSysId();
//        String redisKey = redisUtil.createRealKey(sysId);
//        if (Boolean.FALSE.equals(redisUtil.hasKey(redisKey))) {
//            //redis 没有该参数 ==> 去数据库查看
//            Optional<SysUser> optionalSysUser = sysUserDao.findBySysId(sysId);
//            // 判断 mysql 是否存有人员信息
//            if (optionalSysUser.isPresent()) {
//                SysUser user = optionalSysUser.get();
//                user.setCreatedTime(null);
//                user.setUpdatedTime(null);
//                // 数据库有信息 但 redis 没有信息...写入redis
//                redisUtil.set(redisKey, user);
//            } else {
//                //数据库和redis 都没有信息...写入redis mysql
//                SysUser sysUser2 = ehrRemoteService.getInfoById(sysUser.getSysId());
//                sysUserDao.save(sysUser2);
//                log.debug("方法tryToCreateRedis保存用戶信息成功，sysid：" + sysId);
//                sysUser.setUpdatedTime(null);
//                sysUser.setCreatedTime(null);
//                redisUtil.set(redisKey, sysUser2);
//            }
//        } else {
//            log.debug("redis 已存在该用户信息");
//        }
//    }

//    @Override
//    @Transactional(isolation = Isolation.SERIALIZABLE)
//    public SysUser getSysUserInfo(String sysId) {
//        SysUser sysUser = new SysUser();
//        //防止 sysId传入null
//        if (sysId != null) {
//            //直接从数据库中查询用户信息
//            Optional<SysUser> sysUserSql = sysUserDao.findBySysId(sysId);
//            if (sysUserSql.isPresent()) {
//                SysUser user = sysUserSql.get();
//                user.setCreatedTime(null);
//                user.setUpdatedTime(null);
//                sysUser = sysUserSql.get();
//            } else{
//                // 数据库中不存在从ehr查询
//                log.debug("getSysUserInfo:未找到用户信息...");
//                //从EHR查询员工信息
//                sysUser = ehrRemoteService.getInfoById(sysId);
//                //保存至本地
//                log.debug("向ehr服务同步用户数据,将数据保存数据库，用户id:" + sysId);
//                sysUserDao.save(sysUser);
//            }
//        }
//        return sysUser;
//    }


    @Override
    public Boolean isLabour(String hrId) {
        return false;
    }

    @Override
    public Map<String, String> batchGetNameByEmpNos(List<String> empNos) {
        return null;
    }
}