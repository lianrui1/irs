package cn.com.cowain.ibms.rest.resp.working_mode;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/10/21 17:30
 */
@Data
@ApiModel("设备使用权限列表")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ManagementPageResp {

    @ApiModelProperty(value = "头像")
    private String img;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "工号")
    private String empNo;

    @ApiModelProperty(value = "部门")
    private String dept;

    @ApiModelProperty(value = "是否是管理员 1是 0否")
    private Integer isAdmin;
}
