package cn.com.cowain.ibms.rest.req.iot.centralized;

import lombok.Data;

import java.util.List;

@Data

public class CommandTemplate {

    /**
     * 执行命令的设备id列表
     */
    private List<String> deviceIds;
    /**
     * 设备的服务ID
     */
    private String serviceId;
    /**
     * 设备命令名称
     */
    private String commandName;
    /**
     * 设备执行的命令
     */
    private String paras;

}
