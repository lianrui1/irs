package cn.com.cowain.ibms.exceptions;

/**
 * 幂等异常类
 * @author Yang.Lee
 * @date 2020/12/16 13:26
 */
public class CommandExecuteException extends RuntimeException {
    public CommandExecuteException(String message) {
        super(message);
    }
}
