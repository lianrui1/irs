package cn.com.cowain.ibms.service.audio;

import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.audio.RealtimeUpload;
import cn.com.cowain.ibms.rest.resp.ReservationRecordResp;
import cn.com.cowain.ibms.rest.resp.audio.AudioRecordResp;
import cn.com.cowain.ibms.rest.resp.audio.AudioRecordRespPage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import org.csource.common.MyException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/1/4 9:52
 */
public interface AudioService {

    /**
     * 上传文件到fastdfs服务器
     *
     * @param audioRecordId
     * @param endTime
     * @return
     */
    ServiceResult uploadToFastDFS(String audioRecordId, String path, String endTime) throws IOException, MyException, InterruptedException, URISyntaxException;


    /**
     * 根据会议室ID和日期查询当天预约会议清单
     *
     * @param roomId
     * @param localDate
     * @param sysId
     * @return
     */
    List<ReservationRecordResp> findByRoomIdAndDate(String roomId, LocalDate localDate, String sysId);

    /**
     * 新增音频记录信息
     *
     * @param audioRecordResp
     * @return
     */
    ServiceResult save(AudioRecordResp audioRecordResp);

    /**
     * 更新音频记录信息
     *
     * @param id
     * @return
     */
    ServiceResult update(String id);

    /**
     * 获取音频记录详细信息
     *
     * @param id
     * @return
     * @author Yang.Lee
     */
    ServiceResult get(String id);

    /**
     * 会议记录分页查询  根据会议室名称和会议时间模糊查
     *
     * @param name
     * @param localDate
     * @return
     */
    PageBean<AudioRecordRespPage> findByRoomName(String name, LocalDate localDate, String sysId, PageBean<AudioRecordRespPage> pageBean);

    /**
     * 会议记录分页查询  根据会议室名称模糊查
     *
     * @param name
     * @return
     */
    PageBean<AudioRecordRespPage> findByName(String name, String sysId, PageBean<AudioRecordRespPage> pageBean);

    /**
     * 获取实时语音转写信息
     * @param audioRecordId
     * @return
     */
    ServiceResult getRealTimeAudioRecord(String audioRecordId);

    /**
     * 检查上传PCM文件的合法性
     * @param file
     * @return
     */
    ServiceResult checkPcmFile(MultipartFile file);

    /**
     * 补传实时语音转写语音文件
     * @param id
     * @param realtimeUpload
     * @return
     */
    ServiceResult realtimeUpload(String id, RealtimeUpload realtimeUpload);

    /**
     * 实时转写中断
     * @param audioId
     * @return
     */
    ServiceResult rtasrBreak(String audioId);
}
