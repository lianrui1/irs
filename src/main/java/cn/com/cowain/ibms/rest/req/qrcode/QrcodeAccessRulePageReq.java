package cn.com.cowain.ibms.rest.req.qrcode;

import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 二维码通行权限分页查询参数对象
 *
 * @author Yang.Lee
 * @date 2021/4/8 15:58
 */
@Data
@ApiModel("二维码通行权限分页查询参数")
public class QrcodeAccessRulePageReq extends PageReq {

    @ApiModelProperty(value = "门磁设备名称，模糊查询", example = "test", position = 3)
    private String magneticDeviceName;

    @ApiModelProperty(value = "门磁设备ID", example = "123", position = 4)
    private String magneticDeviceId;

    @ApiModelProperty(value = "项目ID", example = "456", position = 5)
    private String projectId;
}
