package cn.com.cowain.ibms.enumeration.task;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wei.cheng
 * @date 2022/03/10 16:16
 */
public enum TaskOperateResult {
    /**
     * 审批中
     */
    PENDING(1),
    /**
     * 已批准
     */
    PASSED(2),
    /**
     * 已驳回
     */
    DECLINED(3),
    /**
     * 已撤回
     */
    CANCEL(4);

    private static final Map<Integer, TaskOperateResult> TASK_OPERATE_RESULT_MAP = new HashMap<>();

    static {
        for (TaskOperateResult task : TaskOperateResult.values()) {
            TASK_OPERATE_RESULT_MAP.put(task.value, task);
        }
    }

    private final Integer value;

    TaskOperateResult(Integer value) {
        this.value = value;
    }

    public static TaskOperateResult resolve(Integer value) {
        return TASK_OPERATE_RESULT_MAP.get(value);
    }

    public Integer getValue() {
        return this.value;
    }
}
