ALTER TABLE `ibms_uat`.`audio_record` ADD COLUMN `rt_text_file_path` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '实时翻译文本路径' AFTER `audio_file_path`;

ALTER TABLE `ibms_uat`.`audio_record` DROP COLUMN `flv_file_path`;

ALTER TABLE `ibms_uat`.`iot_door_magnetic` ADD COLUMN `huawei_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '华为云硬件ID' AFTER `device_id`;