package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.AlertCaptureor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * @author wei.cheng
 * @date 2022/03/29 11:18
 */
@Repository
public interface AlertCaptureorDao extends JpaRepository<AlertCaptureor, String>, JpaSpecificationExecutor<AlertCaptureor> {
    /**
     * 查询deviceId = {@code deviceId}的AlertCaptureor列表
     *
     * @param deviceId
     * @return
     */
    List<AlertCaptureor> findAllByDeviceId(String deviceId);

    /**
     * 获取存在记录的日期
     *
     * @return
     */
    @Query(
            value = "select date(alert_time) FROM iot_alert_captureor where is_del = 0 group by date(alert_time);",
            nativeQuery = true
    )
    Set<String> findAlertTimeDay();
}
