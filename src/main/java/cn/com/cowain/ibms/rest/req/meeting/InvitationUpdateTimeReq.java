package cn.com.cowain.ibms.rest.req.meeting;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/14 9:48
 */
@Data
public class InvitationUpdateTimeReq {

    // 邀请函单号
    private String id;

    // 开始时间
    private String startTime;

    // 结束时间
    private String endTime;
}
