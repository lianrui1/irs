package cn.com.cowain.ibms.exceptions;

import cn.com.cowain.ibms.aop.FieldErrorWrapper;

/**
 * 参数校验失败异常
 *
 * @author Hu Jingling
 * @since 2020-08-04
 */
public class ValidationFailedException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private final FieldErrorWrapper fieldErrorWrapper;

    public FieldErrorWrapper getFieldErrorWrapper() {
        return fieldErrorWrapper;
    }

    public ValidationFailedException(FieldErrorWrapper fieldErrorWrapper) {
        this.fieldErrorWrapper = fieldErrorWrapper;
    }

}
