package cn.com.cowain.ibms.rest.bim;

import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.resp.bim.BimResp;
import cn.com.cowain.ibms.utils.IConst;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/10/18 16:47
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/bim")
@Api(tags = IConst.BIM)
public class BimController {

    @GetMapping
    @ApiOperation(value = "楼宇总览", tags = IConst.V18)
    public ResponseEntity<JsonResult<BimResp>> get() {


        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }


    @GetMapping("/project/{id}")
    @ApiOperation(value = "根据项目获取设备信息，能源", tags = IConst.V18)
    public ResponseEntity<JsonResult<BimResp>> getProject(@PathVariable @ApiParam("项目ID") String id) {


        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @GetMapping("/space/{id}")
    @ApiOperation(value = "根据项目获取设备信息，能源", tags = IConst.V18)
    public ResponseEntity<JsonResult<BimResp>> getSpace(@PathVariable @ApiParam("空间ID") String id) {


        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }
}
