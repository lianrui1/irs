package cn.com.cowain.ibms.rest.req.oat;

import cn.com.cowain.ibms.enumeration.ota.Application;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author Yang.Lee
 * @date 2021/4/14 17:12
 */
@Data
@ApiModel("app升级任务创建请求参数")
public class UpgradeTaskCreateReq implements Serializable {

    @Pattern(regexp = "^.{1,20}$", message = "升级任务长度不超过20")
    @NotEmpty
    @ApiModelProperty(value = "升级任务名称", required = true, example = "3000-12-31 09:30", position = 1)
    private String taskName;

    @NotNull
    @ApiModelProperty(value = "安装任务地址", required = true, example = "http://a/x.apk", position = 2)
    private String appPackageUrl;

    @NotEmpty
    @ApiModelProperty(value = "文件名", required = true, example = "x.apk", position = 3)
    private String originalFileName;

    @NotNull
    @ApiModelProperty(value = "MD5验证码", required = true, example = "123456", position = 4)
    private String appPackageMD5;

    @NotNull
    @ApiModelProperty(value = "升级应用", required = true, example = "DOORPLATE_PAD", position = 5)
    private Application application;

    @NotNull
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "升级时间", required = true, example = "3000-12-31 09:30", position = 6)
    private LocalDateTime upgradeTime;

    @ApiModelProperty(value = "更新内容", example = "DOORPLATE_PAD", position = 7)
    private String content;

    @ApiModelProperty(value = "强制更新，默认否", example = "false", position = 8)
    private boolean forceUpdate = false;
}
