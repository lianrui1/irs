package cn.com.cowain.ibms.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author wei.cheng
 * @date 2022/10/10 15:09
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SimpleTimeSlot implements Serializable {

    /**
     * 关联Room
     */
    private String roomId;

    /**
     * 从时间
     */
    private String from;

    /**
     * 到时间
     */
    private String to;
}
