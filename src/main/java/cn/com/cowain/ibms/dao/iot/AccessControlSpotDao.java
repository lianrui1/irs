package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.AccessControlSpot;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.enumeration.iot.AccessControlSpotLevel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * 门禁点DAO
 *
 * @author Yang.Lee
 * @date 2021/11/19 14:10
 */
public interface AccessControlSpotDao extends JpaRepository<AccessControlSpot, String>, JpaSpecificationExecutor<AccessControlSpot> {

    // 根据ID获取所有门禁点
    List<AccessControlSpot> findByIdIn(List<String> accessControlIds);

    // 根据名称获取门禁点信息
    Optional<AccessControlSpot> findByName(String name);

    // 根据设备ID获取门禁信息
    Optional<AccessControlSpot> findByDeviceId(String id);

    // 根据设备对象获取门禁信息
    Optional<AccessControlSpot> findByDevice(IotDevice iotDevice);
    // 根据设备ids获取门禁点信息
    List<AccessControlSpot> findByDeviceIdIn(List<String> deviceIds);

    /**
     * @param spaceIds 空间列表
     * @return
     * @description 根据空间列表获取门禁点列表
     * @author tql
     * @date 21-12-9
     */
    List<AccessControlSpot> findBySpaceIdIn(List<String> spaceIds);

    // 根据门禁等级获取门禁列表
    List<AccessControlSpot> findByAccessControlSpotLevel(AccessControlSpotLevel normal);

    // 根据门禁Id查询为绑定的门禁点列表
    List<AccessControlSpot> findByIdNotIn(List<String> id);

    /**
     * 根据设备SN查询门禁点列表
     *
     * @param snCollection 设备SN集合
     * @return 门禁点集合
     * @author Yang.Lee
     * @date 2022/3/7 11:10
     **/
    List<AccessControlSpot> findByDeviceSnIn(Collection<String> snCollection);

    @Query("SELECT d.space FROM AccessControlSpot d where d.isDelete=0 GROUP BY d.space ")
    List<Space> findAllSpotGroupBySpace();


}
