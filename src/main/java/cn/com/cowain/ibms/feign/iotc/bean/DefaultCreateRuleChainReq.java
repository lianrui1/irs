package cn.com.cowain.ibms.feign.iotc.bean;

import cn.com.cowain.ibms.enumeration.RuleEngIneDetailTriggerCondition;
import cn.com.cowain.ibms.enumeration.iot.UsableStatus;
import cn.com.cowain.ibms.rest.req.office.RuleEngIneDetailActuatorReq;
import cn.com.cowain.ibms.rest.req.office.RuleEngIneDetailTriggerReq;
import cn.com.cowain.ibms.rest.req.office.RuleEngineDetailEffectiveTypeReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.util.List;

/**
 * 默认规则链创建接收对象
 *
 * @author: yanzy
 * @date: 2022/3/1 22:18
 */
@Data
@ApiModel("默认规则链创建接收对象")
public class DefaultCreateRuleChainReq {

    @ApiModelProperty(value = "空间id",required = true)
    private String spaceId;

    @ApiModelProperty(value = "办公室id",required = true)
    private String officeId;

    @ApiModelProperty(value = "联动规则小图片url",required = true)
    private String imgUrl;

    @Length(max = 20,message = "名称最长为20")
    @ApiModelProperty(value = "联动规则名称",required = true)
    private String name;

    @Length(max = 20,message = "简述最长为20")
    @ApiModelProperty(value = "联动规则简述")
    private String purpose;

    @ApiModelProperty(value = "条件关系",required = true)
    private RuleEngIneDetailTriggerCondition triggerCondition;

    @ApiModelProperty(value = "触发条件",required = true)
    private List<RuleEngIneDetailTriggerReq> triggerReqs;

    @ApiModelProperty(value = "执行任务",required = true)
    private List<RuleEngIneDetailActuatorReq> actuatorReqs;

    @ApiModelProperty(value = "是否启用",required = true)
    private UsableStatus usableStatus;

    @ApiModelProperty(value = "生效方式",required = true)
    private RuleEngineDetailEffectiveTypeReq effectiveTypeReq;
}
