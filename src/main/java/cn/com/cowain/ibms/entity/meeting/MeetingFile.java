package cn.com.cowain.ibms.entity.meeting;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.enumeration.meeting.FileType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 会议文档对象
 *
 * @author wei.cheng
 * @date 2022/02/23 10:50
 */
@Data
@Entity
@Table(name = "ibms_meeting_file")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_meeting_file", comment = "会议文档表")
@Where(clause = "is_del=0")
public class MeetingFile extends BaseEntity {

    /**
     * 会议预约记录ID，外键
     */
    @ManyToOne
    @JoinColumn(name = "reservation_record_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK'")
    private ReservationRecord reservationRecord;


    @Column(name = "creator_hr_id", nullable = false, columnDefinition = "varchar(36) COMMENT '上传用户工号'")
    private String creatorHrId;

    /**
     * 文件路径
     */
    @Column(name = "file_path", columnDefinition = "varchar(80) COMMENT '文件路径'")
    private String filePath;

    /**
     * 文件大小，单位为B
     */
    @Column(name = "size", columnDefinition = "int(10) COMMENT '文件大小，单位为B'")
    private Long size;

    /**
     * 文件名，不包括扩展名
     */
    @Column(name = "file_name", columnDefinition = "varchar(256)  COMMENT '文件名，不包括扩展名'")
    private String fileName;

    /**
     * 文件扩展名
     */
    @Column(name = "file_ext", columnDefinition = "varchar(16) COMMENT '文件扩展名'")
    private String fileExt;


    @Enumerated(EnumType.STRING)
    @Column(name = "file_type",nullable = false, columnDefinition = "varchar(50) COMMENT '文档类型'")
    private FileType fileType;

    @Column(name = "url_remark", columnDefinition = "varchar(500) COMMENT '链接说明'")
    private String urlRemark;

    @Column(name = "url", columnDefinition = "varchar(500) COMMENT '上传URL 用，分割'")
    private String url;
}
