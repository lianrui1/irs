package cn.com.cowain.ibms.rest.req.iot;

import cn.com.cowain.ibms.enumeration.iot.DeviceLinkStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 设备联动编辑req对象
 */
@Data
@ApiModel("设备联动编辑req对象")
public class DeviceLinkEditReq {

    @ApiModelProperty(value = "设备地址", position = 1)
    private String address;

    @NotEmpty
    @ApiModelProperty(value = "设备ID集合", required = true, position = 2)
    private List<String> deviceIdList;

    @NotNull
    @ApiModelProperty(value = "状态", required = true, position = 3)
    private DeviceLinkStatus status;

    @NotBlank
    @ApiModelProperty(value = "所属空间ID", required = true, position = 4)
    private String spaceId;

}
