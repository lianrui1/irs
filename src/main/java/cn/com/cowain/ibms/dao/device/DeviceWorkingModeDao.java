package cn.com.cowain.ibms.dao.device;

import cn.com.cowain.ibms.entity.office.DeviceWorkingMode;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/8/28 10:39
 */
@Repository
public interface DeviceWorkingModeDao extends JpaRepository<DeviceWorkingMode, String> , JpaSpecificationExecutor<DeviceWorkingMode> {

    // 根据空间获取场景模式
    Page<DeviceWorkingMode> findBySpaceId(String spaceId, Pageable pageable);

    // 根据空间和名称获取模式信息
    Optional<DeviceWorkingMode> findBySpaceIdAndName(String spaceId, String name);

    List<DeviceWorkingMode> findByIntelligentOfficeIdAndUserHrId(String intelligentOfficeId, String userHrId);


    // 根据空间ID 模式名称 创建人ID获取场景信息
    Optional<DeviceWorkingMode> findBySpaceIdAndNameAndUserHrId(String spaceId, String name, String hrId);



    // 根据模式ID和创建人工号获取模式信息
    Optional<DeviceWorkingMode> findByIdAndUserHrId(String id, String hrId);

    // 根据会议室ID 创建人工号 和默认模式ID获取模式信息
    Optional<DeviceWorkingMode> findByIntelligentOfficeIdAndUserHrIdAndDefaultModeId(String officeId, String hrId, String id);
    Optional<DeviceWorkingMode> findByIntelligentOfficeIdAndDefaultModeId(String officeId,  String id);
    // 根据空间获取场景模式
    Page<DeviceWorkingMode> findBySpaceIdAndShowroomCentralize(String intelligentOfficeId, int showroomCentralize,Pageable pageable);
    List<DeviceWorkingMode> findByIntelligentOfficeId(String officeId);
}
