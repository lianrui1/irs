package cn.com.cowain.ibms.mq.consumer;

import cn.com.cowain.ibms.mq.Topic;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * @author wei.cheng
 * @date 2022/04/13 15:34
 */
@Slf4j
@RocketMQMessageListener(
        topic = "${rocketmq.env}-" + Topic.IOTC_ALERT_CAPTUREOR,
        consumerGroup = "${rocketmq.group}-" + Topic.IOTC_ALERT_CAPTUREOR
)
@Component
public class AlertCaptureorConsumer implements RocketMQListener<String> {
//    @Resource
//    private AlertCaptureorService alertCaptureorService;

    @Override
    public void onMessage(String message) {
//        log.debug("AlertCaptureorConsumer onMessage, message:{}", message);
//        try {
//            // 解析数据
//            AlertCaptureorMessage alertCaptureorMessage = JSON.parseObject(message, AlertCaptureorMessage.class);
//            // 保存结果
//            ServiceResult serviceResult = alertCaptureorService.createAlertCaptureor(alertCaptureorMessage);
//
//            log.debug("result:{}", serviceResult);
//        } catch (Exception e) {
//            log.error(e.getMessage(), e);
//        }
    }
}
