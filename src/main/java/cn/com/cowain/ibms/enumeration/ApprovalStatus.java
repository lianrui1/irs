package cn.com.cowain.ibms.enumeration;

/**
 * @author Yang.Lee
 * @date 2022/3/8 11:03
 */
public enum ApprovalStatus {

    PENDING("审批中"),

    APPROVED("已批准"),

    DENIED("已驳回"),

    CANCEL("已取消");

    private String name;

    ApprovalStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
