package cn.com.cowain.ibms.entity.meeting;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.enumeration.meeting.InvitationCardStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author Yang.Lee
 * @date 2021/6/24 18:09
 */
@Data
@Entity
@Table(name = "ibms_meeting_invitation_card")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_meeting_invitation_card", comment = "会议邀请卡表")
@Where(clause = "is_del=0")
public class InvitationCard extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "record_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 会议ID'")
    private ReservationRecord reservationRecord;

    @Column(name = "invitation_code", nullable = false, columnDefinition = "varchar(36) COMMENT '邀请码'")
    private String invitationCode;

    @Column(name = "reason", nullable = false, columnDefinition = "varchar(36) COMMENT '事由'")
    private String reason;

    @Column(name = "reason_code", nullable = false, columnDefinition = "varchar(36) COMMENT '事由编码'")
    private String reasonCode;

    @Column(name = "start_time", nullable = false, columnDefinition = "datetime COMMENT '开始时间'")
    private LocalDateTime startTime;

    @Column(name = "end_time", nullable = false, columnDefinition = "datetime COMMENT '结束时间'")
    private LocalDateTime endTime;

    @Column(name = "is_vip",  columnDefinition = "tinyint(1) default 0 COMMENT '是否vip，1 是； 0 否'")
    private Integer isVip;

    @Column(name = "role_code",columnDefinition = "varchar(36) COMMENT '访客类型代码code'")
    private String roleCode;

    @Column(name = "remark", columnDefinition = "varchar(500) COMMENT '审批备注'")
    private String remark;

    @Column(name = "visitor_record_no", nullable = false, columnDefinition = "varchar(36) COMMENT '邀请函单号'")
    private String visitorRecordNo;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", columnDefinition = "varchar(50) COMMENT '邀请函状态'")
    private InvitationCardStatus status;
}
