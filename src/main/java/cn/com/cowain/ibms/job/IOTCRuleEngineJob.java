package cn.com.cowain.ibms.job;

import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.dao.iot.SensorBodySensingLastStatusDao;
import cn.com.cowain.ibms.dao.office.IntelligentOfficeRuleEngineDao;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.iot.SensorBodySensingLastStatus;
import cn.com.cowain.ibms.entity.office.IntelligentOfficeRuleEngine;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.enumeration.iot.UsableStatus;
import cn.com.cowain.ibms.feign.iotc.IOTCApi;
import cn.com.cowain.ibms.feign.iotc.bean.RuleEngineTbDataResp;
import cn.com.cowain.ibms.feign.iotc.bean.Telemetry;
import cn.com.cowain.ibms.feign.iotc.enumeration.MethodCode;
import cn.com.cowain.ibms.feign.iotc.enumeration.ModuleCode;
import cn.com.cowain.ibms.rest.req.iot.GroupBindingReq;
import cn.com.cowain.ibms.rest.resp.device.TbDataResp;
import cn.com.cowain.ibms.rest.resp.distinguish.SwitchResp;
import cn.com.cowain.ibms.rest.resp.office.RuleEngIneDetailTriggerResp;
import cn.com.cowain.ibms.service.bean.DoorMagneticHWReq;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.device.HwProductsService;
import cn.com.cowain.sfp.xunfei.util.EncryptUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.ZoneOffset;
import java.util.*;

/**
 * 联动规则 IOTC数据同步任务
 *
 * @author: yanzy
 * @date: 2022/2/21 14:52
 */
@Slf4j
@Component
public class IOTCRuleEngineJob {

    @Resource
    private HwProductsService productsService;

    @Resource
    private IntelligentOfficeRuleEngineDao intelligentOfficeRuleEngineDao;

    @Resource
    private DeviceDao deviceDao;

    @Resource
    private SensorBodySensingLastStatusDao sensorBodySensingLastStatusDao;

    @Resource
    private IOTCApi iotcApi;

    @Scheduled(cron = "0/30 * * * * ? ")
    public void run1(){

        // 保存人体传感器最后一次状态变更数据
        log.info("保存人体传感器最后一次状态变更数据");
        try {
            saveSensingStatus();
        }catch (Exception e){
            log.error("保存人体传感器最后一次状态变更数据异常");
            log.error(e.getMessage(), e);
        }
    }

    @Scheduled(cron = "0 */1 * * * ?")
    public void run(){
        log.info("开始获取遥测数据数据");
        try {
            ruleEngineDeviceSync();
        }catch (Exception e){
            log.error("遥测数据数据异常");
            log.error(e.getMessage(), e);
        }

    }

    private void saveSensingStatus() {

        // 获取所又人体传感器设备
        List<IotDevice> devliceList = deviceDao.findByDeviceType(DeviceType.SENSOR_BODY_SENSING);
        for(IotDevice device: devliceList){
            // 获取人体传感器当前状态
            // 获取设备所属产品的服务ID和命令名称
            ServiceResult serviceResult = productsService.findProductProperties(device);
            if(!serviceResult.isSuccess()){
                continue;
            }
            DoorMagneticHWReq hwReq = (DoorMagneticHWReq) serviceResult.getObject();
            SwitchResp data = productsService.findProperties(device.getHwDeviceId(), hwReq.getService_id());
            TbDataResp tbDataResp = new TbDataResp();
            if(StringUtils.isNotEmpty(data.getIsPeopleThere())){
                if(data.getIsPeopleThere().equals("0")){
                    tbDataResp.setValue("false");
                }else if(data.getIsPeopleThere().equals("1")){
                    tbDataResp.setValue("true");
                }
            }

             if(StringUtils.isNotEmpty(tbDataResp.getValue())){
                 // 更新传感器设备状态
                 Optional<SensorBodySensingLastStatus> bodySensingLastStatusOp = sensorBodySensingLastStatusDao.findByDeviceId(device.getId());
                 if(bodySensingLastStatusOp.isEmpty()){
                     SensorBodySensingLastStatus sensorBodySensingLastStatus = new SensorBodySensingLastStatus();
                     sensorBodySensingLastStatus.setDevice(device);
                     sensorBodySensingLastStatus.setIsPeopleThere(tbDataResp.getValue());
                     sensorBodySensingLastStatusDao.save(sensorBodySensingLastStatus);
                 }else {
                     SensorBodySensingLastStatus sensorBodySensingLastStatus = bodySensingLastStatusOp.get();
                     if(!sensorBodySensingLastStatus.getIsPeopleThere().equals(tbDataResp.getValue())){
                         sensorBodySensingLastStatus.setIsPeopleThere(tbDataResp.getValue());
                         sensorBodySensingLastStatusDao.save(sensorBodySensingLastStatus);
                     }

                 }
             }
        }
    }

    /**
     * 同步设备数据
     *
     * @author: yanzy
     * @data: 2022/2/21 14:02:27
     */
    public void ruleEngineDeviceSync(){

        List<IntelligentOfficeRuleEngine> ruleEngineList = intelligentOfficeRuleEngineDao.findAllByUsableStatus(UsableStatus.ENABLE);

        for (IntelligentOfficeRuleEngine intelligentOfficeRuleEngine : ruleEngineList){
            // 创建遥测数据集合
            List<RuleEngineTbDataResp> ruleEngineTbDataRespList = new ArrayList<>();

            //将执行条件string转换成对象
            List<RuleEngIneDetailTriggerResp> triggerRespList = JSON.parseArray(intelligentOfficeRuleEngine.getTrigger(), RuleEngIneDetailTriggerResp.class);

            for (RuleEngIneDetailTriggerResp ruleEngIneDetailTriggerResp : triggerRespList) {

                //创建联动规则所需遥测数据
                RuleEngineTbDataResp ruleEngineTbDataResp = new RuleEngineTbDataResp();
                ruleEngineTbDataResp.setRuleChainId(intelligentOfficeRuleEngine.getId());

                if (ruleEngIneDetailTriggerResp.getDeviceId() != null){
                    Optional<IotDevice> deviceDaoById = deviceDao.findById(ruleEngIneDetailTriggerResp.getDeviceId());
                    if (!deviceDaoById.isEmpty()) {

                        //set设备id
                        ruleEngineTbDataResp.setDeviceId(ruleEngIneDetailTriggerResp.getDeviceId());
                        //set设备属性
                        ruleEngineTbDataResp.setDeviceType(deviceDaoById.get().getDeviceType());

                        //根据华为云id查询tbid
                        String tbId = productsService.findTbId(deviceDaoById.get().getHwDeviceId());
                        // 如果是人体传感器
                        if (DeviceType.SENSOR_BODY_SENSING.equals(ruleEngineTbDataResp.getDeviceType())) {
                            Optional<SensorBodySensingLastStatus> byDeviceId = sensorBodySensingLastStatusDao.findByDeviceId(deviceDaoById.get().getId());
                            if(byDeviceId.isPresent()){
                                TbDataResp tbDataResp = new TbDataResp();
                                tbDataResp.setTs(byDeviceId.get().getUpdatedTime());
                                tbDataResp.setValue(byDeviceId.get().getIsPeopleThere());
                                ruleEngineTbDataResp.setTbDataResp(tbDataResp);
                            }

                        } else {
                            List<TbDataResp> data = productsService.findNowData(tbId);
                            if (data.get(0) != null){
                                ruleEngineTbDataResp.setTbDataResp(data.get(0));
                            }
                        }
                    }
                }
                ruleEngineTbDataRespList.add(ruleEngineTbDataResp);
            }

            Map<String, Object> data = new HashMap<>();

            Object val = "";
            for (RuleEngineTbDataResp ruleEngineTbDataResp : ruleEngineTbDataRespList){

                val = ruleEngineTbDataResp.getRuleChainId();
                data.put("ruleChainId",val);

                if (ruleEngineTbDataResp.getDeviceId() != null){

                    String deviceId = EncryptUtil.md5(ruleEngineTbDataResp.getDeviceId());


                    if (DeviceType.TEMPERATURE_AND_HUMIDITY_AND_ILLUMINATION_SENSOR.equals(ruleEngineTbDataResp.getDeviceType())){
                        val = ruleEngineTbDataResp.getTbDataResp().getLuminance();
                        data.put("luminance"+deviceId,val);

                        val = ruleEngineTbDataResp.getTbDataResp().getHumidity();
                        data.put("humidity"+deviceId,val);

                        val = ruleEngineTbDataResp.getTbDataResp().getTemperature();
                        data.put("temperature"+deviceId,val);
                    } else if (DeviceType.NOISE_SENSOR.equals(ruleEngineTbDataResp.getDeviceType())){
                        val = ruleEngineTbDataResp.getTbDataResp().getNoise();
                        data.put("Noise"+deviceId,val);

                    }else if (DeviceType.PM_SENSOR.equals(ruleEngineTbDataResp.getDeviceType())){

                        val = ruleEngineTbDataResp.getTbDataResp().getPm2_5();
                        data.put("PM2_5"+deviceId,val);

                        val = ruleEngineTbDataResp.getTbDataResp().getPm10();
                        data.put("PM10"+deviceId,val);
                    }else {
                        if (ruleEngineTbDataResp.getTbDataResp().getTs() != null){
                            //获取当前时间毫秒数
                            long now = new Date().getTime();
                            //获取指定时间毫秒数
                            long milli = ruleEngineTbDataResp.getTbDataResp().getTs().toInstant(ZoneOffset.of("+8")).toEpochMilli();
                            //当前时间毫秒数 减去 指定时间毫秒数
                            //毫秒差转换成分钟
                            long m = (now - milli)/1000/60;

                            val = ruleEngineTbDataResp.getTbDataResp().getValue();
                            //人体传感器参数 0：无人 1：有人
                            if ("0".equals(val) || "false".equals(val)){
                                //false:无人
                                data.put("people"+deviceId,"false");
                            }else if("1".equals(val) || "true".equals(val)){
                                //true:有人
                                data.put("people"+deviceId,"true");
                            }

                            val = m;
                            data.put("timeKey"+deviceId,val);
                        }
                    }

                }
            }

            Telemetry telemetry = new Telemetry();
            telemetry.setJson(JSON.toJSONString(data));
            String s = JSON.toJSONString(telemetry);
            //调用IOTCApi接口
            GroupBindingReq groupBindingReq = new GroupBindingReq();
            groupBindingReq.setChainSite("cloud");
            groupBindingReq.setChannelType("3");
            groupBindingReq.setJsonArg(s);
            groupBindingReq.setMethodCode(MethodCode.POST_TELEMETRY.getName());
            groupBindingReq.setModuleCode(ModuleCode.HW.getName());
            iotcApi.postToConn(groupBindingReq);
        }
    }
}
