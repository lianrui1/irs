package cn.com.cowain.ibms.dao.space;

import cn.com.cowain.ibms.entity.space.SpaceAdmin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SpaceAdminDao extends JpaRepository<SpaceAdmin, String>, JpaSpecificationExecutor<SpaceAdmin> {

    // 根据空间和管理员工号获取详情
    Optional<SpaceAdmin> findBySpaceIdAndAdminEmpNo(String spaceId, String hrId);

    // 根据空间ID获取管理员列表
    List<SpaceAdmin> findBySpaceId(String spaceId);

    // 根据工号获取管理员信息
    List<SpaceAdmin> findByAdminEmpNoIn(List<String> empNos);

    // 根据工号获取管理员信息
    List<SpaceAdmin> findByAdminEmpNo(String hrId);

    /**
     * 判断用户{@code adminEmpNo}是否是空间{@code spaceId}的管理员
     *
     * @param adminEmpNo
     * @param spaceId
     * @return
     */
    Boolean existsByAdminEmpNoAndSpaceId(String adminEmpNo, String spaceId);

    List<SpaceAdmin> findBySpaceIdInAndAdminEmpNo(List<String> spaceIds, String userHrId);
}
