package cn.com.cowain.ibms.service.impl;

import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.service.SysUserService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @author Yang.Lee
 * @date 2021/2/5 9:52
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SysUserServiceUCImplTest {

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserService;

    @Test
    public void getUserByTokenTest(){

        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxIiwicmVhbE5hbWUiOiLnrqHnkIblkZgiLCJuYmYiOjE2MTI0OTAxNzMsImNsaWVudFR5cGUiOiJQQyIsImhySWQiOiJDV0EwMDAxIiwiaXNzIjoid3d3LmNvd2Fpbi5jb20uY24iLCJleHAiOjE2MjAyNjYxNzMsInVzZXJOYW1lIjoiYWRtaW4iLCJpYXQiOjE2MTI0OTAxNzMsInVzZXJJZCI6IjEiLCJqdGkiOiIxMzU3NTA4Mjk2NjQ2MzMyNDE3IiwiZW1haWwiOiJvVERiS2hjUnhHeWduY0VDQVJsaSJ9.H3VVZclk4OK4ErrcFy7vu-rrDMdOEUxRFiGxYYmCJmc";
        SysUser sysUser = sysUserService.getUserByToken(token);
        System.out.println(sysUser.getEmpNo());
    }


    @Test
    public void testFullDepartment(){
        SysUser sysUser = sysUserService.getUserByEmpNo("CWA3275");

        System.out.println(sysUser.getFullDepartmentName());
    }
}
