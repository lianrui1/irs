package cn.com.cowain.ibms.enumeration.iot;

/**
 * 门派状态枚举类
 *
 * @author Yang.Lee
 * @date 2020/11/30 16:28
 */
public enum DoorPlateStatus {

    END("已结束"),

    /**
     * 空闲
     */
    FREE("空闲中"),

    /**
     * 正在进行中
     */
    IN_PROCESS( "正在会议中"),

    /**
     * 即将开始
     */
    ABOUT_TO_BEGIN("会议即将开始"),

    /**
     * 当前会议即将结束,下一场会议即将开始
     * 该状态为程序逻辑判断使用，不返回给前端。返沪前端时应该将其该为 ABOUT_TO_BEGIN
     */
    NEXT_ABOUT_TO_BEGIN("当前会议即将结束,下一场会议即将开始"),

    /**
     * 会议等待开始
     **/
    IN_FUTURE("等待开始");

    DoorPlateStatus(String describe){
        this.describe = describe;
    }

    private String describe;

    public String getDescribe() {
        return describe;
    }
}
