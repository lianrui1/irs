package cn.com.cowain.ibms.service.bean.amap.weather;

import lombok.Data;

import java.util.List;

/**
 * @author feng
 * @title: ForceCastsTempResult
 * @projectName ibms
 * @Date 2022/5/5 18:43
 */
@Data
public class ForceCastsTempResult {
    String city;
    String adcode;
    String province;
    String reporttime;
    List<ForcecastsResult> casts;

}
