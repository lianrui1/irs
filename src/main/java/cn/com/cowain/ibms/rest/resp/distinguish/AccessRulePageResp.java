package cn.com.cowain.ibms.rest.resp.distinguish;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

/**
 * 通行权限分页数据实体
 * @author Yang.Lee
 * @date 2021/3/10 17:08
 */
@Data
@Builder
@ApiModel("通行权限分页数据")
public class AccessRulePageResp {

    @ApiModelProperty(value = "记录ID", required = true, example = "2044090b-7b66-4f00-8c2d-2e16b265338d")
    private String id;

    @ApiModelProperty(value = "人脸识别设备名称", required = true, example = "设备A", position = 1)
    private String deviceName;

    @ApiModelProperty(value = "设备来源。KUANGSHI:旷世;HAIKANG:海康;MODIAN:魔点", example = "海康", position = 2)
    private String deviceSource;

    @ApiModelProperty(value = "项目名称", required = true, example = "昆山工厂", position = 3)
    private String projectName;

    @ApiModelProperty(value = "空间名称名称", example = "A楼", position = 4)
    private String spaceName;

    @ApiModelProperty(value = "人员组", required = true, example = "A楼", position = 5)
    private String staffGroups;

    @ApiModelProperty(value = "设备位置", required = true, example = "入口处", position = 6)
    private String deviceAddress;
}
