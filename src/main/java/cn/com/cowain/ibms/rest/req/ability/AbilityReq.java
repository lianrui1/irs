package cn.com.cowain.ibms.rest.req.ability;

import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author feng
 * @title: AbilityReq
 * @projectName ibms
 * @Date 2022/1/11 11:23
 */
@Data
@ApiModel("查询列表")
public class AbilityReq  extends PageReq {
    @ApiModelProperty(value = "基础ID", required = true)
    String basicId;
    //空间id
    @ApiModelProperty(value = "空间id", required = false)
    String spaceId;
    @ApiModelProperty(value = "设备名称", required = false)
    String accessControlName;
    @ApiModelProperty(value = "设备id", required = false)
    String accessControlId;
    @ApiModelProperty(value = "项目Id", required = false)
    String projectId;
}
