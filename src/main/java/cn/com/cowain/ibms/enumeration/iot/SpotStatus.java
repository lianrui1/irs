package cn.com.cowain.ibms.enumeration.iot;


public enum  SpotStatus {

    DOING("执行中"),

    SUCCESS("成功"),

    FAILED("失败");

    SpotStatus (String name){
        this.name = name;
    }
    String name;

    public String getName(){
        return this.name;
    }
}
