package cn.com.cowain.ibms.entity.ability;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.iot.AccessControlSpot;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Yang.Lee
 * @date 2022/1/10 16:31
 */
@Data
@Entity
@Table(name = "ibms_open_ability_access_control_spot")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_open_ability_access_control_spot", comment = "IBMS 开放能力关联门禁点表")
@Where(clause = "is_del=0")
public class OpenAbilityAccessControlSpot extends BaseEntity {

    public OpenAbilityAccessControlSpot(OpenAbility openAbility, AccessControlSpot accessControlSpot){
        this.openAbility = openAbility;
        this.spot = accessControlSpot;
    }

    public OpenAbilityAccessControlSpot(){}

    @ManyToOne
    @JoinColumn(name = "ability_id", nullable = false, columnDefinition = "varchar(36) COMMENT '开放能力ID'")
    private OpenAbility openAbility;


    @ManyToOne
    @JoinColumn(name = "spot_id", nullable = false, columnDefinition = "varchar(36) COMMENT '门禁点ID'")
    private AccessControlSpot spot;
}
