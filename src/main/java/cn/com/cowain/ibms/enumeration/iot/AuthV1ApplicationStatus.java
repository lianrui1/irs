package cn.com.cowain.ibms.enumeration.iot;

/**
 * @author wei.cheng
 * @date 2022/06/29 14:38
 */
public enum AuthV1ApplicationStatus {

    NOT_APPLIED("0", "未申请"),

    IN_APPLICATION("1", "审批中"),

    PASS("2", "审批通过"),

    DECLINE("3", "已拒绝");

    private final String value;

    private final String desc;

    AuthV1ApplicationStatus(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public String getValue() {
        return this.value;
    }

    public String getDesc() {
        return this.desc;
    }
}
