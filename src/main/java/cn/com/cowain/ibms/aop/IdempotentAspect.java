package cn.com.cowain.ibms.aop;

import cn.com.cowain.ibms.annotation.Idempotent;
import cn.com.cowain.ibms.exceptions.IdempotentException;
import cn.com.cowain.ibms.utils.RedisUtil;
import cn.com.cowain.sfp.xunfei.util.EncryptUtil;
import com.alibaba.fastjson.JSONArray;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.CodeSignature;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 幂等切面
 * <p>该切面依赖redis</p>
 * @author Yang.Lee
 * @date 2020/12/16 13:18
 */
@Slf4j
@Aspect
@Component
@ConditionalOnClass(RedisTemplate.class)
public class IdempotentAspect {

    @Pointcut("@annotation(cn.com.cowain.ibms.annotation.Idempotent)")
    public void executeIdempotent() {
        log.debug("进入Idempotent切点");
    }

    @Autowired
    private RedisUtil redisUtil;

    @Around("executeIdempotent()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        log.debug("进入幂等切面");
        // 获取方法
        Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();
        // 获取注解
        Idempotent idempotent = method.getAnnotation(Idempotent.class);

        Parameter[] ps =  method.getParameters();
        Set<String> realParameter = new HashSet<>();
        // 遍历方法里的所有参数，只有@RequestBody，@RequestParam，@PathVariable三个注解中的数据进入redis，后续可以根据需求添加
        for(Parameter p: ps){
            Annotation[] as =  p.getAnnotations();
            for(Annotation a : as){
                Class<? extends Class> aClass = a.annotationType().getClass();

                if(RequestBody.class.equals(aClass)
                        || RequestParam.class.equals(aClass)
                        || PathVariable.class.equals(aClass)){
                    realParameter.add(p.getName());
                }
            }
        }

        // 方法参数对象数组
        Object[] args = joinPoint.getArgs();
        // 方法参数名称数组
        String[] paramNames = ((CodeSignature)joinPoint.getSignature()).getParameterNames();
        List<Object> objectList = new ArrayList<>();
        for (int i = 0; i < paramNames.length; i++) {
            String param = paramNames[i];
            for(String realName : realParameter){
                // 根据参数名称判断该参数是否进入redis
                if(realName.equals(param)){
                    ObjectMapper objectMapper = new ObjectMapper();
                    // 将Object对象转为指定对象，并存入最终json化的参数集合中
                    objectList.add(objectMapper.convertValue(args[i], args[i].getClass()));
                }
            }
        }

        String jsonArrayStr = new JSONArray(objectList).toJSONString();
        String newRedisValue = EncryptUtil.md5(jsonArrayStr);
        String redisKey = redisUtil.createRealKey(idempotent.value());
        if(Boolean.TRUE.equals(redisUtil.hasKey(redisKey))){
            String oldRedisValue = redisUtil.get(redisKey).toString();
            if(oldRedisValue.equals(newRedisValue))
                 throw new IdempotentException("请求正在处理，请不要重复提交请求");
        }
        redisUtil.set(redisKey, newRedisValue, idempotent.expireTime());
        return joinPoint.proceed();
    }

}
