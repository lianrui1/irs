package cn.com.cowain.ibms.rest.resp.office;

import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.rest.resp.working_mode.WorkingModeDetailResp;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 联动规则执行任务返回对象
 *
 * @author: yanzy
 * @date: 2022/2/11 10:30
 */
@Data
@ApiModel("联动规则执行任务返回对象")
public class RuleEngIneDetailActuatorResp {

    @ApiModelProperty(value = "场景id")
    private String workingModeId;

    @ApiModelProperty(value = "场景")
    private WorkingModeDetailResp workingModeDetailResp;

    //可控设备
    @ApiModelProperty(value = "设备华为云ID", required = true)
    private String deviceId;

    @ApiModelProperty(value = "设备状态 true开 false关")
    private Boolean status;

    @ApiModelProperty(value = "门磁 窗帘开门 open close suspend(暂停)", position = 1)
    private String action;

    @ApiModelProperty(value = "空调的打开与关闭 on off", position = 2)
    private String setPower;

    @ApiModelProperty(value = "空调温度", position = 3)
    private Integer setTemp;

    @ApiModelProperty(value = "空调设置制冷 制热 除湿 送风 自动 模式 cool,heat,dry,fan,auto", position = 4)
    private String setMode;

    @ApiModelProperty(value = "空调风速 小 中 大 自动 min med max auto", position = 5)
    private String setFanSpeed;

    @ApiModelProperty(value = "空调风向 上 中 下 up med down", position = 6)
    private String setFanDirection;

    @ApiModelProperty(value = "空调风向 手动 自动 true false", position = 7)
    private String setFanAuto;

    @ApiModelProperty(value = "设置扫风模式 ，无扫风，上下扫风，左右扫风，上下左右扫风 noSwing,updownSwing,leftrightSwing,aroundSwing")
    private String setSwing;

    @ApiModelProperty(value = "四路开关 on off", position = 8)
    private String light1;

    @ApiModelProperty(value = "四路开关 on off", position = 9)
    private String light2;

    @ApiModelProperty(value = "四路开关 on off", position = 10)
    private String light3;

    @ApiModelProperty(value = "四路开关 on off", position = 11)
    private String light4;

    @ApiModelProperty(value = "设备类型", required = true)
    private DeviceType deviceType;

    @ApiModelProperty(value = "可控设备参数")
    private ControlledDeviceResp controlledDeviceResp;

}
