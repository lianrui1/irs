package cn.com.cowain.ibms.entity;

import cn.com.cowain.ibms.enumeration.ReservationRecordItemStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 房间预定记录明细
 *
 * @author 胡荆陵
 * @since 2020-7-30
 */
@Data
@Entity
@Table(name = "ibms_reservation_record_item")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_reservation_record_item", comment = "会议室预定记录明细")
@Where(clause = "is_del=0")
public class ReservationRecordItem extends BaseEntity {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 预定记录明细状态
     */
    @Column(name = "ri_status", nullable = false, columnDefinition = "varchar(50) COMMENT '预定记录明细状态'")
    @Enumerated(EnumType.STRING)
    private ReservationRecordItemStatus status;

    /**
     * 发起人ID
     */
    @Column(name = "ri_initiator", columnDefinition = "varchar(255) COMMENT '发起人ID'")
    private String initiator;

    /**
     * 发起人工号
     */
    @Column(name = "ri_initiator_emp_no", columnDefinition = "varchar(36) COMMENT '发起人工号'")
    private String initiatorEmpNo;

    /**
     * 明细的所有人ID
     */
    @Column(name = "ri_owner", columnDefinition = "varchar(255) COMMENT '明细的所有人ID'")
    private String owner;

    /**
     * 明细人工号
     */
    @Column(name = "ri_owner_emp_no", nullable = false, columnDefinition = "varchar(255) COMMENT '明细的所有人工号'")
    private String ownerEmpNo;

    @ManyToOne
    @JoinColumn(name = "record_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK'")
    private ReservationRecord reservationRecord;

    /**
     * 是否是会议组成员
     */
    @Column(name = "is_group", columnDefinition = "tinyint(1) COMMENT '1 是;0:否'")
    private Integer isGroup;

}
