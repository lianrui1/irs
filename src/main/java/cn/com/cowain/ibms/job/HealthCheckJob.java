package cn.com.cowain.ibms.job;

import cn.com.cowain.ibms.service.WeChatService;
import cn.com.cowain.ibms.service.bean.WeChatMsg;
import cn.com.cowain.ibms.service.hk.HealthCheckService;
import cn.com.cowain.ibms.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @Author tql
 * @Description 海康面板机心跳检测
 * @Date 21-5-21 下午3:43
 * @Version 1.0
 */
@Slf4j
@Component
public class HealthCheckJob {

    @Value("${wechat.msg.offline.user}")
    private String wechatMsgOfflineUser;

    private static int isStatus = 0;
    private static int isNtStatus = 0;

    private HealthCheckService healthCheckService;

    @Autowired
    private WeChatService weChatService;

    public HealthCheckJob(HealthCheckService healthCheckService) {
        this.healthCheckService = healthCheckService;
    }

    @Scheduled(cron = "0 0 23 * * ?")
    public void run() {
        log.info("Checking HK-DEIVCE Service..");
        // 总部检测
        boolean isOk = healthCheckService.healthCheckHq();
        if (isOk) {
            setStatus(0);
            log.info("HK-DEIVCE Service OK");
        } else {
            setStatus(HealthCheckJob.isStatus + 1);
            log.info("HK-DEIVCE Service ERROR");
        }
        if (isStatus == 5) {
            WeChatMsg weChatMsg = WeChatMsg.builder().first("总部海康服务下线")
                    .keyword1("offline").keyword2("下线").keyword3(DateUtils.formatLocalDateTime(null, "yyyy-MM-dd HH:mm:ss")).remark("海康服务下线").build();
            weChatService.sendMsg("", wechatMsgOfflineUser, weChatMsg);
            setStatus(0);
        }

        // 南通服务检测
        boolean isNtOk = healthCheckService.healthCheckNt();
        if (isNtOk) {
            setNtStatus(0);
            log.info("HK-DEIVCE Service OK");
        } else {
            setNtStatus(HealthCheckJob.isStatus + 1);
            log.info("HK-DEIVCE Service ERROR");
        }
        if (isNtStatus == 5) {
            WeChatMsg weChatMsg = WeChatMsg.builder().first("南通海康服务下线")
                    .keyword1("offline").keyword2("下线").keyword3(DateUtils.formatLocalDateTime(null, "yyyy-MM-dd HH:mm:ss")).remark("海康服务下线").build();
            weChatService.sendMsg("", wechatMsgOfflineUser, weChatMsg);
            setStatus(0);
        }
        log.info("Checking HK-DEIVCE Service End");
    }

    /**
     * 更新总部
     *
     * @param status
     */
    private static synchronized void setStatus(int status) {
        HealthCheckJob.isStatus = status;
    }

    /**
     * 更新南通
     *
     * @param status
     */
    private static synchronized void setNtStatus(int status) {
        HealthCheckJob.isNtStatus = status;
    }
}
