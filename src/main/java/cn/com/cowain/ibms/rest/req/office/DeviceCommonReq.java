package cn.com.cowain.ibms.rest.req.office;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author feng
 * @title: UserOfficeReq
 * @projectName bims
 * @Date 2021/12/7 16:23
 */
@Data
@ApiModel("设备隐藏")
public class DeviceCommonReq implements Serializable   {
    @ApiModelProperty(value = "办公室id")
    String officeId;
    @ApiModelProperty(value = "设备id")
    String deviceId;
    @ApiModelProperty(value = "打开关闭")
    Boolean open ;
}
