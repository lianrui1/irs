package cn.com.cowain.ibms.service.disthinguish.impl;

import cn.com.cowain.ibms.dao.distinguish.*;
import cn.com.cowain.ibms.dao.iot.AccessControlSpotUserDao;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.distinguish.*;
import cn.com.cowain.ibms.entity.iot.AccessControlSpot;
import cn.com.cowain.ibms.entity.iot.AccessControlSpotUser;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.PageReq;
import cn.com.cowain.ibms.rest.req.distinguish.*;
import cn.com.cowain.ibms.rest.resp.BaseListResp;
import cn.com.cowain.ibms.rest.resp.distinguish.AccessControlSpotResp;
import cn.com.cowain.ibms.rest.resp.distinguish.StaffGroupImportResp;
import cn.com.cowain.ibms.rest.resp.distinguish.StaffGroupResp;
import cn.com.cowain.ibms.rest.resp.distinguish.StaffSysUserResp;
import cn.com.cowain.ibms.service.ExcelService;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.DeviceType;
import cn.com.cowain.ibms.service.bean.ServiceMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.bean.UcResp;
import cn.com.cowain.ibms.service.disthinguish.StaffGroupClient;
import cn.com.cowain.ibms.service.disthinguish.StaffGroupService;
import cn.com.cowain.ibms.service.qrcode.QrcodeAccessRuleService;
import cn.com.cowain.ibms.utils.DateUtils;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.RedisUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/8 13:45
 */
@Slf4j
@Service
public class StaffGroupServiceImpl implements StaffGroupService {


    @Resource
    private StaffGroupClient staffGroupClient;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private StaffGroupDao staffGroupDao;
    @Autowired
    private StaffGroupMembersDao membersDao;
    @Autowired
    private AccessRulesDao accessRulesDao;
    @Autowired
    private AccessRulesToStaffGroupDao accessRulesToStaffGroupDao;

    @Value("${cowain.uc.url}")
    private String ucUrl;

    @Resource
    private QrcodeAccessRuleService qrcodeAccessRuleService;

    @Resource
    private ExcelService excelService;

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserServiceUC;

    @Resource
    private RedisUtil redisUtil;

    @Autowired
    private AccessControlSpotUserDao spotUserDao;

    @Autowired
    private AccessControlSpotStaffGroupRelationDao accessControlSpotStaffGroupRelationDao;

    @Override
    @Transactional
    public ServiceResult save(StaffGroupReq staffGroupReq) {
        // 获取人员组人清单
        List<String> participantList = staffGroupReq.getParticipantList();

        // 判断人员组名称是否已被使用
        Optional<StaffGroup> op = staffGroupDao.findByName(staffGroupReq.getName());
        if (op.isPresent()) {
            return ServiceResult.error("该名称已被使用,请重新输入");
        }
        // 保存人员组
        StaffGroup staffGroup = new StaffGroup();
        staffGroup.setName(staffGroupReq.getName());
        staffGroup.setCount(staffGroupReq.getParticipantList().size());
        // 获取人员组排序最大值
        Integer maxSeq = staffGroupDao.findMaxSeq();
        if (maxSeq != null) {
            maxSeq = maxSeq + 1;
        } else {
            maxSeq = 1;
        }
        staffGroup.setSeq(maxSeq);
        // 保存
        staffGroupDao.save(staffGroup);
        // 循环保存人员组人员明细
        if (!participantList.isEmpty()) {
            participantList.forEach(it -> {
                StaffGroupMembers sg = new StaffGroupMembers();
                sg.setGroupId(staffGroup);
                // 分割获取员工工号
                String[] split = it.split("\\|");
                String empNo = split[0];
                String name = split[1];
                sg.setEmpNo(empNo);
                sg.setRealName(name);
                // 保存人员组人员
                membersDao.save(sg);


            });
        }
        return ServiceResult.ok();
    }

    @Override
    @Transactional
    public PageBean<StaffGroupResp> search(StaffGroupPageReq req) {
        // 根据人员组创建时间正序排序
        Pageable pageable = PageRequest.of(req.getPage(), req.getSize(), Sort.by(req.getOrders()));
        Page<StaffGroup> staffGroupPage = staffGroupDao.findAll((root, criteriaQuery, criteriaBuilder) -> {

            // 构造查询条件
            List<Predicate> list = new ArrayList<>();

            // 员工名称
            String name = req.getName();
            if (StringUtils.isNotEmpty(name)) {
                list.add(criteriaBuilder.like(root.get("name").as(String.class), "%" + name + "%"));
            }

            // 开始时间
            if (req.getStartTime() != null) {
                list.add(criteriaBuilder.greaterThanOrEqualTo(
                        root.get("createdTime").as(String.class),
                        DateUtils.formatLocalDateTime(req.getStartTime(),
                                DateUtils.PATTERN_DATETIME)));
            }

            // 结束时间
            if (req.getEndTime() != null) {
                list.add(criteriaBuilder.lessThanOrEqualTo(
                        root.get("createdTime").as(String.class),
                        DateUtils.formatLocalDateTime(req.getEndTime(),
                                DateUtils.PATTERN_DATETIME)));
            }

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, pageable);

        PageBean<StaffGroupResp> pageBean = new PageBean<>();

        pageBean.setPage(req.getPage());
        pageBean.setSize(req.getSize());
        pageBean.setList(convertList(staffGroupPage.getContent()));
        pageBean.setTotalPages(staffGroupPage.getTotalPages());
        pageBean.setTotalElements(staffGroupPage.getTotalElements());

        return pageBean;
    }

    private List<StaffGroupResp> convertList(List<StaffGroup> staffGroupList){

        return staffGroupList.stream().map(this::convertBase).collect(Collectors.toList());
    }

    private StaffGroupResp convertBase(StaffGroup staffGroup){
        StaffGroupResp staffGroupResp = new StaffGroupResp();
        staffGroupResp.setCount(staffGroup.getCount());
        staffGroupResp.setCreatedTime(staffGroup.getCreatedTime());
        staffGroupResp.setId(staffGroup.getId());
        int accessControlSpotCount = accessControlSpotStaffGroupRelationDao.countByStaffGroupId(staffGroup.getId());
        staffGroupResp.setAccessControlSpotCount(accessControlSpotCount);
        staffGroupResp.setName(staffGroup.getName());
        staffGroupResp.setSeq(staffGroup.getSeq());
        return staffGroupResp;
    }

    @Override
    @Transactional
    public ServiceResult delete(String id) {
        // 判断该人员组是否存在
        Optional<StaffGroup> op = staffGroupDao.findById(id);
        if (op.isPresent()) {
            // 获取人员组信息
            StaffGroup staffGroup = op.get();


            // 判断人员组是否绑定门禁点不可删除
            List<AccessControlSpotStaffGroupRelation> relations = accessControlSpotStaffGroupRelationDao.findByStaffGroupId(staffGroup.getId());
            if(!relations.isEmpty()){
                return ServiceResult.error("该人员组已绑定门禁,不可删除");
            }

            // 旷世同步删除人员组
            staffGroupClient.deleteStaffGroup(DeviceType.KS.toString(), id);
            // 删除人员组
            staffGroup.setIsDelete(1);
            staffGroupDao.save(staffGroup);
            // 根据人员组获取成员
            List<StaffGroupMembers> memberList = membersDao.findByGroupId(staffGroup);
            List<StaffGroupMembers> objects = new ArrayList<>();
            // 删除成员
            for (StaffGroupMembers m : memberList) {
                m.setIsDelete(1);
                objects.add(m);
            }
            membersDao.saveAll(objects);

            // 判断该人员租是否拥有权限 如果有则删除
            List<AccessRulesToStaffGroup> list = accessRulesToStaffGroupDao.findByGroupId(id);
            // 判断权限和人员组是否关联 有则删除
            if (!list.isEmpty()) {
                for (AccessRulesToStaffGroup ruleToStaff : list) {
                    // 根据权限获取人员组
                    Set<AccessRulesToStaffGroup> byRuleId = accessRulesToStaffGroupDao.findByRuleId(ruleToStaff.getRule().getId());
                    byRuleId.forEach(rule2Group -> {
                        // 如果该权限仅有一个人员组则将此权限删除
                        if (byRuleId.size() == 1 && rule2Group.getGroup().getId().equals(id)) {
                            Optional<AccessRules> ruleOp = accessRulesDao.findById(rule2Group.getRule().getId());
                            if (ruleOp.isPresent()) {
                                AccessRules accessRules = ruleOp.get();
                                accessRules.setIsDelete(1);
                                accessRulesDao.save(accessRules);
                            }
                        }
                    });
                    // 删除
                    ruleToStaff.setIsDelete(1);
                    accessRulesToStaffGroupDao.save(ruleToStaff);
                }
            }

            // 进行二维码权限删除联动操作
            qrcodeAccessRuleService.staffGroupRemoveCheck(id);
        } else {
            return ServiceResult.error(ServiceMessage.NOT_FOUND_STAFF_GROUP);
        }


        return ServiceResult.ok();
    }

    @Override
    @Transactional
    public ServiceResult get(String id) {
        Optional<StaffGroup> staffOp = staffGroupDao.findById(id);
        if (!staffOp.isPresent()) {
            return ServiceResult.error("人员组ID错误，未查询到结果");

        }
        // 将获取到的人员组信息赋值
        StaffGroup staffGroup = staffOp.get();
        StaffGroupResp staffGroupResp = new StaffGroupResp();
        BeanUtils.copyProperties(staffGroup, staffGroupResp);
        // 根据人员组获取人员组成员
        List<StaffGroupMembers> staffList = membersDao.findByGroupId(staffGroup);
        List<StaffSysUserResp> sysUsers = new ArrayList<>();
        if (!staffList.isEmpty()) {
            for (StaffGroupMembers sg : staffList) {
                StaffSysUserResp sysUser = new StaffSysUserResp();
                sysUser.setNameZh(sg.getRealName());
                sysUser.setHrId(sg.getEmpNo());
                sysUsers.add(sysUser);
            }
        }
        // 赋值人员组成员
        staffGroupResp.setParticipantList(sysUsers);
        if (staffGroupResp.getParticipantList() == null) {
            staffGroupResp.setParticipantList(Collections.emptyList());
        }
        return ServiceResult.ok(staffGroupResp);
    }

    @Override
    @Transactional
    public ServiceResult update(String id, StaffGroupReq staffGroupReq) {

        List<StaffGroupMembers> staffGroupMembers = new ArrayList<>();
        log.info("变更人员组：" + staffGroupReq);
        ArrayList<UserReq> userReqArrayList = new ArrayList<>();
        // 获取人员组人清单
        List<String> participantList = staffGroupReq.getParticipantList();
        // 判断成员是否拥有照片
        /*ServiceResult result = photoConvert(participantList, token);
        if (!result.isSuccess()) {
            return ServiceResult.error(result.getObject());
        }*/
        // 判断该人员组是否存在
        Optional<StaffGroup> staffOp = staffGroupDao.findById(id);
        if (!staffOp.isPresent()) {
            return ServiceResult.error(ServiceMessage.NOT_FOUND_STAFF_GROUP);
        }
        StaffGroup staffGroup = staffOp.get();
        // 判断人员组名称是否已被使用
        Optional<StaffGroup> op = staffGroupDao.findByName(staffGroupReq.getName());
        if (op.isPresent() && !id.equals(op.get().getId())) {
            return ServiceResult.error("该名称已被使用,请重新输入");
        }
        // 保存人员组
        staffGroup.setName(staffGroupReq.getName());
        staffGroup.setCount(staffGroupReq.getParticipantList().size());
        // 确认本人员组在旷世是否存在
        GroupExistReq groupExistReq = new GroupExistReq();
        groupExistReq.setGroupName(staffGroupReq.getName());
        String exist = staffGroupClient.exist(groupExistReq);
        JSONObject existResult = JSON.parseObject(exist);
        String msg = existResult.getString("msg");
        if (msg.equals("用户组不存在")) {
            // 更改人员组
            staffGroupDao.save(staffGroup);
            // 获取原有人员组成员
            List<StaffGroupMembers> members = membersDao.findByGroupId(staffGroup);
            staffGroupMembers = new ArrayList<>();
            if (!participantList.isEmpty()) {
                // 获取变更人员
                staffGroupMembers = getStaffUser(participantList, staffGroup, members);
            } else {
                // 循环人员组原人员
                for (StaffGroupMembers member : members) {
                    // 原人员组成员删除
                    member.setIsDelete(1);
                    staffGroupMembers.add(member);
                }
            }
            membersDao.saveAll(staffGroupMembers);
        } else {
            try {
                // 保存
                staffGroupDao.save(staffGroup);
                ArrayList<UserGroupReq> userGroupReqs = new ArrayList<>();
                UserGroupReq userGroupReq = new UserGroupReq();
                userGroupReq.setId(staffGroup.getId());
                userGroupReq.setName(staffGroup.getName());
                userGroupReq.setType(1);
                userGroupReqs.add(userGroupReq);
                // 向ms-device修改人员组
                log.info("向ms-device修改人员组信息： " + userGroupReqs);
                String s = staffGroupClient.update(DeviceType.KS.toString(), userGroupReqs);
                JSONObject jsonResult = JSON.parseObject(s);
                String data = jsonResult.getString("data");
                if (!data.equals("更新成功")) {
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return ServiceResult.error("ms-device修改人员组失败");
                }
                // 获取原有人员组成员
                List<StaffGroupMembers> members = membersDao.findByGroupId(staffGroup);
                if (!participantList.isEmpty()) {
                    ArrayList<UserReq> userReqList = new ArrayList<>();
                    // 获取变更人员
                    staffGroupMembers = getStaffUser(participantList, staffGroup, members);
                    log.info("人员组变更成员: " + staffGroupMembers);
                    membersDao.saveAll(staffGroupMembers);
                    for (StaffGroupMembers member : staffGroupMembers) {
                        // 获取变更人员及其用户组
                        userReq(userReqList, member);
                    }
                    // 向ms-device更新人员所属人员组
                    log.info("ms-device更新人员组成员信息： " + userReqList);
                    String ss = staffGroupClient.updateUser(DeviceType.KS.toString(), userReqList);
                    JSONObject jsonResult1 = JSON.parseObject(ss);
                    String data1 = jsonResult1.getString("data");
                    if (!data1.equals("更新成功")) {
                        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                        return ServiceResult.error("旷世更新人员组成员信息失败: " + data1);
                    }
                } else {
                    // 循环保存人员组人员明细
                    for (StaffGroupMembers member : members) {
                        // 原人元组成员删除
                        member.setIsDelete(1);
                        membersDao.save(member);
                        staffGroupMembers.add(member);
                        // 获取变更人员及其用户组
                        userReq(userReqArrayList, member);
                    }
                    // 向ms-device更新人员所属人员组
                    log.info("ms-device更新人员组成员信息： " + userReqArrayList);
                    String ss = staffGroupClient.updateUser(DeviceType.KS.toString(), userReqArrayList);
                    JSONObject jsonResult1 = JSON.parseObject(ss);
                    String data1 = jsonResult1.getString("data");
                    if (!data1.equals("更新成功")) {
                        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                        return ServiceResult.error("旷世更新人员组成员信息失败: " + data1);
                    }
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error("旷世修改人员组失败");
            }
        }

        // 更新门禁点人员列表
        ArrayList<AccessControlSpotUser> spotUserList = new ArrayList<>();
        List<AccessControlSpotStaffGroupRelation> list = accessControlSpotStaffGroupRelationDao.findByStaffGroupId(staffGroup.getId());
        for(AccessControlSpotStaffGroupRelation it : list){
            for(StaffGroupMembers member : staffGroupMembers){

                List<AccessControlSpotUser> userList = spotUserDao.findByAccessControlSpotIdAndUserHrId(it.getAccessControlSpot().getId(), member.getEmpNo());
                userList.forEach(obj -> {
                    Optional<AccessControlSpotUser> spotUserOp = Optional.ofNullable(obj);
                    if(spotUserOp.isEmpty() && member.getIsDelete() == 0){
                        AccessControlSpotUser spotUser = new AccessControlSpotUser();
                        spotUser.setAccessControlSpot(it.getAccessControlSpot());
                        spotUser.setUserHrId(member.getEmpNo());
                        spotUser.setUserName(member.getRealName());
                        spotUser.setAccessStartTime(LocalDateTime.now().withNano(0));
                        spotUser.setAccessEndTime(LocalDateTime.now().withNano(0).plusYears(5));
                        spotUserList.add(spotUser);

                    }else if(spotUserOp.isPresent() && member.getIsDelete() != 0){
                        AccessControlSpotUser spotUser = spotUserOp.get();
                        spotUser.setIsDelete(1);
                        spotUserList.add(spotUser);
                    }
                });

            }
        }
        spotUserDao.saveAll(spotUserList);

        return ServiceResult.ok();
    }

    private void userReq(ArrayList<UserReq> userReqArrayList, StaffGroupMembers member) {
        String empNo = member.getEmpNo();
        Set<StaffGroupMembers> membersSet = membersDao.findByEmpNo(empNo);
        List<String> userReqList = new ArrayList<>();
        if (!membersSet.isEmpty()) {
            for (StaffGroupMembers staffGroupMembers : membersSet) {
                userReqList.add(staffGroupMembers.getGroupId().getId());
            }
        }
        UserReq userReq = new UserReq();
        userReq.setGroupUuidList(userReqList);
        userReq.setCode(empNo);
        userReq.setName(member.getRealName());
        userReqArrayList.add(userReq);
    }

    private List<StaffGroupMembers> getStaffUser(List<String> participantList, StaffGroup staffGroup, List<StaffGroupMembers> members) {
        List<StaffGroupMembers> list = new ArrayList<>();
        List<StaffGroupMembers> arrayList = new ArrayList<>();
        List<StaffGroupMembers> staffGroupMembers = new ArrayList<>();
        // 变更后人员信息
        participantList.forEach(it -> {
            StaffGroupMembers sg = new StaffGroupMembers();
            // 分割获取员工工号
            String str = it;
            String[] split = str.split("\\|");
            String empNo = split[0];
            String name = split[1];
            sg.setEmpNo(empNo);
            sg.setGroupId(staffGroup);
            sg.setRealName(name);
            list.add(sg);
        });
        // 循环人员组原人员
        for (StaffGroupMembers member : members) {
            StaffGroupMembers staff = new StaffGroupMembers();
            staff.setEmpNo(member.getEmpNo());
            staff.setGroupId(member.getGroupId());
            arrayList.add(staff);
            if (!list.toString().contains(member.toString())) {
                // 原人员组成员删除
                member.setIsDelete(1);
                staffGroupMembers.add(member);
            }
        }
        // 保存新增成员
        for (StaffGroupMembers staff : list) {
            if (!arrayList.toString().contains(staff.getEmpNo())) {
                staffGroupMembers.add(staff);
            }
        }
        return staffGroupMembers;
    }


    @Transactional
    public ServiceResult updateUser(String empNo, List<StaffGroupMembers> members, ArrayList<UserReq> userReqs, String name) {
        Set<StaffGroupMembers> membersSet = membersDao.findByEmpNo(empNo);
        List<String> userReqList = new ArrayList<>();
        if (!members.isEmpty()) {
            for (StaffGroupMembers member : membersSet) {
                userReqList.add(member.getGroupId().getId());
            }
        }
        UserReq userReq = new UserReq();
        userReq.setGroupUuidList(userReqList);
        userReq.setCode(empNo);
        userReq.setName(name);
        userReqs.add(userReq);
        try {
            // 向ms-device更新人员所属人员组
            String ss = staffGroupClient.updateUser(DeviceType.KS.toString(), userReqs);
            JSONObject jsonResult1 = JSON.parseObject(ss);
            String data = jsonResult1.getString("data");
            if (!data.equals("更新成功")) {
                return ServiceResult.error("更新失败");
            }
        } catch (Exception e) {
            log.error(e.toString());
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }

        return ServiceResult.ok();
    }

    @Transactional
    public ServiceResult photoConvert(List<String> participantList, String token) {
        // 获取成员工号
        String empNos = "";
        StringBuilder stringBuffer = new StringBuilder();
        if (!participantList.isEmpty()) {
            for (Object it : participantList) {
                // 分割获取员工工号
                String str = (String) it;
                String[] split = str.split("\\|");
                String empNo = split[0] + ",";

                stringBuffer.append(empNo);
            }
        }
        if (stringBuffer.length() > 0) {
            empNos = stringBuffer.substring(0, stringBuffer.length() - 1);
        }
        // url
        StringBuilder urlTemplate = new StringBuilder();
        urlTemplate.append(ucUrl).append(empNos);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", token);
        HttpEntity requestGet = new HttpEntity<>(headers);
        try {
            // 获取人员组成员信息
            ResponseEntity<String> response = restTemplate.exchange(urlTemplate.toString(), HttpMethod.GET, requestGet, String.class);
            // 解析数据
            String body = response.getBody();
            JSONObject jsonData = (JSONObject) JSON.parse(body);
            JSONArray jsonIndex = jsonData.getJSONArray("data");
            List<UcResp> list = JSON.parseArray(jsonIndex.toJSONString(), UcResp.class);
            // 判断人员是否拥有照片
            String names = "";
            stringBuffer.setLength(0);
            for (UcResp uc : list) {
                if (uc.getWorkPhoto().isEmpty()) {
                    String realName = uc.getRealName();
                    stringBuffer.append(realName).append(",");
                }
            }
            if (stringBuffer.length() > 0) {
                names = stringBuffer.substring(0, stringBuffer.length() - 1);
                return ServiceResult.error("提示:" + names + "没有上传照片，无法添加到人员组");
            }
        } catch (Exception e) {
            log.error(e.toString());
            return ServiceResult.error("获取人员信息失败");
        }
        return ServiceResult.ok();
    }

    /**
     * 查询所有人员组列表
     *
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/3/12 17:02
     **/
    @Override
    @Transactional
    public List<BaseListResp> getList() {

        List<StaffGroup> staffGroupList = staffGroupDao.findAll(Sort.by(Sort.Direction.DESC, "createdTime"));
        return convert(staffGroupList);
    }

    /**
     * 对象转换
     *
     * @param staffGroupList 待转对象
     * @return 目标对象
     * @author Yang.Lee
     * @date 2021/3/12 17:10
     **/
    @Override
    public List<BaseListResp> convert(List<StaffGroup> staffGroupList) {

        List<BaseListResp> result = new ArrayList<>();
        if (staffGroupList == null || staffGroupList.isEmpty()) {
            return result;
        }

        staffGroupList.forEach(obj -> result.add(convert(obj)));

        return result;
    }

    /**
     * 判断人员组是否存在
     *
     * @param groupIds 人员组ID,支持多个id
     * @return true:存在 ； false:不存在
     * @author Yang.Lee
     * @date 2021/4/7 9:09
     **/
    @Override
    @Transactional
    public ServiceResult isGroupExist(@NotNull String... groupIds) {

        Optional<StaffGroup> staffGroupOptional;
        for (String groupId : groupIds) {
            staffGroupOptional = staffGroupDao.findById(groupId);
            if (!staffGroupOptional.isPresent()) {
                return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_STAFF_GROUP, groupId));
            }
        }

        return ServiceResult.ok();
    }

    /**
     * 判断员工是否在人员组中
     *
     * @param empNo  员工工号
     * @param groups 人员组
     * @return true: 员工属于所查询人员组。 false: 员工不属于所查询人员组
     * @author Yang.Lee
     * @date 2021/4/9 13:28
     **/
    @Override
    public boolean isStaffInGroups(@NotNull String empNo, @NotNull StaffGroup... groups) {

        List<StaffGroupMembers> staffGroupMembersList = membersDao.findByGroupIdIn(groups);

        // 判断用户是否存在于人员组中
        List<StaffGroupMembers> userGroup = staffGroupMembersList
                .stream()
                .filter(obj -> obj.getEmpNo().equals(empNo))
                .collect(Collectors.toList());

        return !userGroup.isEmpty();
    }

    @Override
    public ServiceResult saveStaffGroup(StaffGroupReq staffGroupReq) {
        UserGroupReq userGroupReq = new UserGroupReq();
        userGroupReq.setId(staffGroupReq.getStaffGroup().getId());
        userGroupReq.setName(staffGroupReq.getStaffGroup().getName());
        userGroupReq.setType(1);
        List<StaffGroupMembers> participantList = staffGroupReq.getMembersList();
        try {
            // 向ms-device增加人员组
            log.info("旷世新增人员组： " + userGroupReq);
            String s = staffGroupClient.save(DeviceType.KS.toString(), userGroupReq);
            JSONObject jsonResult = JSON.parseObject(s);
            if (jsonResult.getInteger("status") != 1) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error(jsonResult.get("msg"));
            }

            String data = jsonResult.getString("data");
            ArrayList<UserReq> list = new ArrayList<>();
            // 循环获取人员组人员明细
            if (!participantList.isEmpty()) {
                participantList.forEach(it -> {
                    // 获取该员工所属所有人员组
                    Set<StaffGroupMembers> members = membersDao.findByEmpNo(it.getEmpNo());
                    List<String> arrayList = new ArrayList<>();
                    if (!members.isEmpty()) {
                        for (StaffGroupMembers member : members) {
                            arrayList.add(member.getGroupId().getId());
                        }
                    }
                    UserReq userReq = new UserReq();
                    userReq.setGroupUuidList(arrayList);
                    userReq.setCode(it.getEmpNo());
                    userReq.setName(it.getRealName());
                    list.add(userReq);
                });
                // 向ms-device更新人员所属人员组
                log.info("旷世新增人员组成员信息： " + list);
                String ss = staffGroupClient.updateUser(DeviceType.KS.toString(), list);
                JSONObject jsonResult1 = JSON.parseObject(ss);
                String data1 = jsonResult1.getString("data");
                if (!data1.equals("更新成功") || !data.equals("用户组添加成功")) {
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return ServiceResult.error("旷世新增人员组失败: " + data1);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return ServiceResult.error("旷世新增人员组失败");
        }


        return ServiceResult.ok();
    }

    @Override
    public PageBean<StaffGroupResp> searchPage(PageReq req, String accessControlSpotId) {
        int page = req.getPage();
        int size = req.getSize();
        // 创建分页查询对象
        Sort sort = Sort.by(Sort.Direction.ASC, "createdTime");
        Pageable pageable = PageRequest.of(page, size, sort);

        // 获取门禁点下默认人员组
        List<String> staffGroupIds = new ArrayList<>();
        accessControlSpotStaffGroupRelationDao.findByAccessControlSpotIdAndIsDefault(accessControlSpotId, 1).forEach(it-> staffGroupIds.add(it.getStaffGroup().getId()));
        Page<StaffGroup> staffGroupPage = staffGroupDao.findAll((root, criteriaQuery, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();

            if (!staffGroupIds.isEmpty()) {
                // 绑定的设备ID
                list.add(criteriaBuilder.not(criteriaBuilder.in(root.get("id")).value(staffGroupIds)));
            }

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, pageable);
        PageBean<StaffGroupResp> pageBean = new PageBean<>();
        //当前页
        pageBean.setPage(pageable.getPageNumber());
        //每页记录数
        pageBean.setSize(pageable.getPageSize());
        //总记录数
        pageBean.setTotalElements(staffGroupPage.getTotalElements());
        //总页数
        pageBean.setTotalPages(staffGroupPage.getTotalPages());

        List<StaffGroupResp> list = new ArrayList<>();
        //循环
        staffGroupPage.getContent().forEach(staffGroup -> {
            StaffGroupResp staffGroupResp = new StaffGroupResp();
            BeanUtils.copyProperties(staffGroup, staffGroupResp);
            list.add(staffGroupResp);
        });

        pageBean.setList(list);
        return pageBean;
    }

    @Override
    @Transactional
    public void delLeave(List<SysUser> sysUsers) {
        ArrayList<UserReq> userReqArrayList = new ArrayList<>();
        List<StaffGroupMembers> staffGroupMembers = new ArrayList<>();
        for (SysUser sysUser : sysUsers) {
            Set<StaffGroupMembers> byEmpNo = membersDao.findByEmpNo(sysUser.getEmpNo());
            for (StaffGroupMembers members : byEmpNo) {
                members.setIsDelete(1);
                staffGroupMembers.add(members);
            }
            // 向旷世删除人员信息
            List<String> userReqList = new ArrayList<>();
            UserReq userReq = new UserReq();
            userReq.setGroupUuidList(userReqList);
            userReq.setCode(sysUser.getEmpNo());
            userReq.setName(sysUser.getNameZh());
            userReqArrayList.add(userReq);
        }
        // 本地人员组删除此员工
        membersDao.saveAll(staffGroupMembers);
        // 向ms-device更新人员所属人员组
        log.info("ms-device更新人员组成员信息： " + userReqArrayList);
        String ss = staffGroupClient.updateUser(DeviceType.KS.toString(), userReqArrayList);
        JSONObject jsonResult1 = JSON.parseObject(ss);
        String data1 = jsonResult1.getString("data");
        if (!data1.equals("更新成功")) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
    }

    /**
     * 根据excel文件批量添加员工组
     *
     * @param file 文件
     * @return 操作结果
     * @author Yang.Lee
     * @date 2021/7/13 11:15
     **/
    @Override
    @Transactional
    public ServiceResult addStaffGroupBatch(MultipartFile file) {

        ServiceResult result;
        // 读取excel
        try {
            result = excelService.readStaffGroupExcel(file);
            if (!result.isSuccess() && ErrConst.E23.equals(result.getErrorCode())) {
                return result;
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ServiceResult.error("excel文件内容格式异常");
        }
        // 所有上传的数据行
        List<String[]> dataList = (List<String[]>) result.getObject();
        // 数据总数
        int totalCount = dataList.size();
        // 有错误的数据集合
        List<String[]> errorList = getErrorExcelData(dataList);
        // 数据写入成功的数据集合
        List<String[]> successList = new ArrayList<>();
        // 所有数据去除错误数据，剩下的就是正常数据，准备写入数据库
        dataList.removeAll(errorList);

        // 遍历数据，将List<String[]> 格式转为Map<String, List<String[]>>格式，key为员工组名称，value为原数据
        Map<String, List<String[]>> groupMap = new HashMap<>();

        String groupName;
        List<String[]> list;
        for (String[] data : dataList) {
            groupName = data[0];
            if (groupMap.containsKey(groupName)) {
                groupMap.get(groupName).add(data);

            } else {
                list = new ArrayList<>();
                list.add(data);
                groupMap.put(groupName, list);
            }
        }

        // 遍历所有的数据
        groupMap.forEach((k, v) -> {

            // 构建参数
            StaffGroupReq staffGroupReq = new StaffGroupReq();
            staffGroupReq.setName(k);

            List<String> participantList = new ArrayList<>();

            // 用户map， key=工号， val=名字
            Map<String, String> userMap = new HashMap<>();
            v.forEach(data -> userMap.put(data[2], data[1]));
            // 根据名称查询员工组是否已存在
            Optional<StaffGroup> staffGroupOptional = staffGroupDao.findByName(k);

            ServiceResult result1;
            // 如果原来无该员工组，则新增员工组
            if (staffGroupOptional.isEmpty()) {
                userMap.forEach((k1, v1) -> participantList.add(k1 + "|" + v1));
                staffGroupReq.setParticipantList(participantList);

                result1 = save(staffGroupReq);

            } else {
                // 如果数据库中存在员工组，则向旧员工组中给添加新的用户
                StaffGroup staffGroup = staffGroupOptional.get();
                // 查询出原来人员组内的的所有员工
                List<StaffGroupMembers> staffGroupMembersList = membersDao.findByGroupId(staffGroup);

                staffGroupMembersList.forEach(staffGroupMembers -> {
                    if (userMap.containsKey(staffGroupMembers.getEmpNo())) {
                        // 如果已存在与原数据中，则从需要添加的员工组数据中删除
                        userMap.remove(staffGroupMembers.getEmpNo());
                    }
                });
                List<StaffGroupMembers> newMemberList = new ArrayList<>();
                userMap.forEach((k1, v1) -> {
                    StaffGroupMembers staffGroupMembers = new StaffGroupMembers();
                    staffGroupMembers.setGroupId(staffGroup);
                    staffGroupMembers.setEmpNo(k1);
                    staffGroupMembers.setRealName(v1);
                    newMemberList.add(staffGroupMembers);
                });
                // 构建该人员组所有员工对象集合
                List<StaffGroupMembers> allMembers = new ArrayList<>();
                allMembers.addAll(staffGroupMembersList);
                allMembers.addAll(newMemberList);

                allMembers.forEach(members -> participantList.add(members.getEmpNo() + "|" + members.getRealName()));
                staffGroupReq.setParticipantList(participantList);
                // 调用更新员工组方法
                result1 = update(staffGroup.getId(), staffGroupReq);
            }

            if (result1.isSuccess()) {
                successList.addAll(v);
            } else {
                v.forEach(obj -> obj[3] = "导入失败");
                errorList.addAll(v);
            }

        });

        String uuid = "";
        if (!errorList.isEmpty()) {
            uuid = UUID.randomUUID().toString();
            // 将失败数据存入缓存，供用户下载
            String redisKey = redisUtil.createRealKey("staffGroupImportError-" + uuid);
            redisUtil.set(redisKey, errorList, RedisUtil.ONE_DAY_SECOND);
        }

        return ServiceResult.ok(StaffGroupImportResp.builder()
                .totalCount(totalCount)
                .successCount(successList.size())
                .errorCount(errorList.size())
                .errorDataId(uuid)
                .build());
    }

    /**
     * 获取导入失败数据的文件二进制数组
     *
     * @param errorDataId 失败数据的ID
     * @return excel文件的二进制数据
     * @author Yang.Lee
     * @date 2021/7/14 15:21
     **/
    @Override
    public byte[] getImportErrorDataFileByte(String errorDataId) {

        String redisKey = redisUtil.createRealKey("staffGroupImportError-" + errorDataId);
        if (!Boolean.TRUE.equals(redisUtil.hasKey(redisKey))) {
            return new byte[0];
        }

        JSONArray jsonArray = JSON.parseArray(redisUtil.get(redisKey).toString());
        List<String[]> errorList = new ArrayList<>();

        jsonArray.stream().forEach(obj -> {
            JSONArray dataArray = JSON.parseArray(obj.toString());
            String[] strings = new String[dataArray.size()];
            for (int i = 0; i < dataArray.size(); i++) {
                strings[i] = dataArray.getString(i);
            }
            errorList.add(strings);
        });

        return excelService.createStaffGroupErrorExcel(errorList);
    }

    /**
     * 获取上传excel文件中的错误数据集合
     *
     * @param dataList 原始数据集合
     * @return 错误数据集合
     * @author Yang.Lee
     * @date 2021/7/13 19:44
     **/
    public List<String[]> getErrorExcelData(List<String[]> dataList) {

        List<String[]> errorList = new ArrayList<>();
        String groupName = null;
        String empNo = null;
        for (String[] data : dataList) {

            groupName = data[0];
            if (groupName.length() > 32) {
                data[3] = "员工组名称过长，不可超过32个字符";
                errorList.add(data);
                continue;
            }

            // 判断工号是否存在
            empNo = data[2];

            if(StringUtils.isEmpty(empNo)){
                data[3] = "员工工号为空";
                errorList.add(data);
                continue;
            }
            SysUser sysUser = null;
            try {
                sysUser = sysUserServiceUC.getUserByEmpNo(empNo);
                if (sysUser == null) {
                    data[3] = "未查询到员工信息";
                    errorList.add(data);
                    continue;
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                data[3] = "未查询到员工信息";
                errorList.add(data);
                continue;
            }

            if(!sysUser.getNameZh().equals(data[1])){
                data[3] = "员工姓名与员工工号不对应";
                errorList.add(data);
            }

        }

        return errorList;
    }

    /**
     * 对象转换
     *
     * @param group 待转对象
     * @return 目标对象
     * @author Yang.Lee
     * @date 2021/3/12 17:10
     **/
    public BaseListResp convert(StaffGroup group) {

        BaseListResp result = new BaseListResp();
        result.setId(group.getId());
        result.setName(group.getName());

        return result;
    }

    /**
     * 根据人员组ID查询关联门禁点表
     *
     * @param staffGroupId 失败数据的ID
     * @author yanzy
     * @date 2022/01/12
     **/
    @Override
    public PageBean<AccessControlSpotResp> getAccessControlSpotRespList(String staffGroupId,PageReq req) {
        int page = req.getPage();
        int size = req.getSize();
        //创建分页查询对象
        Pageable pageable = PageRequest.of(page,size,Sort.by(req.getOrders()));

        Page<AccessControlSpotStaffGroupRelation> accessControlSpotPage = accessControlSpotStaffGroupRelationDao.findAllByStaffGroupId(staffGroupId, pageable);

        List<AccessControlSpotResp> list = new ArrayList<>();
        accessControlSpotPage.getContent().forEach(it ->{
            AccessControlSpotResp resp = new AccessControlSpotResp();
            AccessControlSpot accessControlSpot = it.getAccessControlSpot();
            resp.setId(accessControlSpot.getId());
            resp.setName(accessControlSpot.getName());
            resp.setAddress(accessControlSpot.getAddress());
            resp.setType(accessControlSpot.getAccessControlSpotType().getName());

            list.add(resp);
        });

        PageBean<AccessControlSpotResp> pageBean = new PageBean<>();
        pageBean.setList(list);
        pageBean.setPage(page);
        pageBean.setSize(size);
        pageBean.setTotalPages(accessControlSpotPage.getTotalPages());
        pageBean.setTotalElements(accessControlSpotPage.getTotalElements());
        return pageBean;
    }

}
