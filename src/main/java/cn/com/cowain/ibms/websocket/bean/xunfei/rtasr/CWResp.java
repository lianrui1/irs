package cn.com.cowain.ibms.websocket.bean.xunfei.rtasr;

import lombok.Data;

/**
 * 讯飞实时转写返回对象CW
 * @author Yang.Lee
 * @date 2021/2/3 17:42
 */
@Data
public class CWResp {

    /**
     * 词识别结果
     */
    private String w;

    /**
     * 词标识,n-普通词；s-顺滑词（语气词）；p-标点
     */
    private String wp;

    /**
     * 词在本句中的开始时间，单位是帧，1帧=10ms
     * 即词在整段语音中的开始时间为(bg+wb*10)ms
     * 中间结果的 wb 为 0
     */
    private int wb;

    /**
     * 词在本句中的结束时间，单位是帧，1帧=10ms
     * 即词在整段语音中的结束时间为(bg+we*10)ms
     * 中间结果的 we 为 0
     */
    private int we;
}
