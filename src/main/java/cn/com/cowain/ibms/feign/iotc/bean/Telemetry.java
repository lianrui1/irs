package cn.com.cowain.ibms.feign.iotc.bean;

import lombok.Data;

/**
 * key
 *
 * @author: yanzy
 * @date: 2022/2/24 13:57
 */
@Data
public class Telemetry {

    //遥测数据json
    private String json;

    //联动规则id
    private String ruleChainId;
}
