package cn.com.cowain.ibms.rest.req.distinguish;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/28 10:34
 */
@Data
@ApiModel("海康人脸修改")
public class HkUpdateFaceReq {

    @ApiModelProperty(value = "设备ID")
    private String deviceId;

    @ApiModelProperty(value = "人员Id")
    private String personId;

    @ApiModelProperty(value = "图片base64")
    private String faceData;
}
