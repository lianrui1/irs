package cn.com.cowain.ibms.rest.req.distinguish;

import lombok.Data;

import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/18 18:48
 */
@Data
public class UserReq {
    private String code;
    private String name;
    private List<String> groupUuidList;
}
