package cn.com.cowain.ibms.rest.req.oat;

import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2021/4/14 18:40
 */
@Data
@ApiModel("ota升级分页请求参数")
public class UpgradePageReq extends PageReq {

    @ApiModelProperty(value = "升级应用", example = "DOORPLATE_PAD", position = 3)
    private String application;

    @ApiModelProperty(value = "版本号", position = 3)
    private String versionOTA;

    @ApiModelProperty(value = "任务状态", example = "APPLY" , position = 3)
    private String status;
}
