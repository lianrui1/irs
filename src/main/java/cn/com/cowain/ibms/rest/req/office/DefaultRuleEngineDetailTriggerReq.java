package cn.com.cowain.ibms.rest.req.office;

import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalTime;
import java.util.List;

/**
 *  默认联动规则触发条件请求对象
 *
 * @author: yanzy
 * @date: 2022/2/28 20:59
 */
@Data
@ApiModel("默认联动规则触发条件请求对象")
public class DefaultRuleEngineDetailTriggerReq {

    @ApiModelProperty(value = "定时 ")
    @JsonFormat(pattern = "HH:mm")
    private LocalTime timing;

    //传感器设备
    @ApiModelProperty(value = "设备类型")
    private DeviceType deviceType;

    @ApiModelProperty(value = "传感器设备")
    private List<RuleEngIneDetailTriggerSensorReq> sensorReqs;

}
