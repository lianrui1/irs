package cn.com.cowain.ibms.bo;

import cn.com.cowain.ibms.entity.iot.AccessControlSpot;
import cn.com.cowain.ibms.rest.resp.iot.AuthSpotResp;
import lombok.Data;

import java.util.List;

/**
 * @author wei.cheng
 * @date 2022/06/30 13:35
 */
@Data
public class AuthV1BO {
    /**
     * 用户是否可以申请
     */
    private Boolean isApplicable;
    /**
     * 返回给用户可见的门禁点
     */
    private List<AuthSpotResp> spotList;
    /**
     * 权限的所有门禁点
     */
    private List<AccessControlSpot> accessControlSpotList;
}
