ALTER TABLE `ibms_project` ADD COLUMN `visit_company` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '单位' AFTER `project_area`;

ALTER TABLE `ibms_project` ADD COLUMN `is_visitor` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否可邀请访客' AFTER `visit_company`;

ALTER TABLE `ibms_reservation_record` ADD COLUMN `record_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否是内部会议  VISITOR 访客' AFTER `room_id`;

ALTER TABLE `ibms_reservation_record_item` ADD COLUMN `is_group` tinyint(1) NULL DEFAULT NULL COMMENT '1 是;0:否' AFTER `record_id`;

ALTER TABLE `ibms_sys_user` ADD COLUMN `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号码' AFTER `status`;

ALTER TABLE `ibms_meeting_invitation_card` ADD COLUMN `status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邀请函状态' AFTER `visitor_record_no`;


CREATE TABLE `ibms_meeting_invitation_card`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `created_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `version` int(11) NULL DEFAULT NULL COMMENT '版本号',
  `end_time` datetime(0) NOT NULL COMMENT '结束时间',
  `invitation_code` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '邀请码',
  `is_vip` tinyint(1) NOT NULL COMMENT '是否vip，1 是； 0 否',
  `reason` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '事由',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '审批备注',
  `start_time` datetime(0) NOT NULL COMMENT '开始时间',
  `record_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'FK 会议ID',
  `reason_code` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '事由编码',
  `visitor_record_no` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '邀请函单号',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKoyfrmleecfot85x1j0cgmt06w`(`record_id`) USING BTREE,
  CONSTRAINT `FKoyfrmleecfot85x1j0cgmt06w` FOREIGN KEY (`record_id`) REFERENCES `ibms_reservation_record` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '会议邀请卡表' ROW_FORMAT = Dynamic;


CREATE TABLE `ibms_meeting_guest_detail`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `created_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `version` int(11) NULL DEFAULT NULL COMMENT '版本号',
  `company` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '访客单位',
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '访客手机号码',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '访客姓名',
  `photo_url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '访客通行照片',
  `invitaion_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'FK 邀请id',
  `job_num` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '访客工号',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKf4lyl6aabl71mwb7pk74jmlvx`(`invitaion_id`) USING BTREE,
  CONSTRAINT `FKf4lyl6aabl71mwb7pk74jmlvx` FOREIGN KEY (`invitaion_id`) REFERENCES `ibms_meeting_invitation_card` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '会议访客详情表' ROW_FORMAT = Dynamic;

CREATE TABLE `ibms_meeting_guest_invitation_record`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `created_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `version` int(11) NULL DEFAULT NULL COMMENT '版本号',
  `code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '访客邀请记录码',
  `invitation_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '邀请状态。DEFAULT 待反馈； ACCEPTED 已接受； REJECTED 已拒绝',
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '访客手机号码',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '访客姓名',
  `invitation_card_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'FK 邀请卡ID',
  `inviter_code` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '访客邀请人code',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKlg4l1qj30v9a702j10v5bgr94`(`invitation_card_id`) USING BTREE,
  CONSTRAINT `FKlg4l1qj30v9a702j10v5bgr94` FOREIGN KEY (`invitation_card_id`) REFERENCES `ibms_meeting_invitation_card` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '访客邀请记录表' ROW_FORMAT = Dynamic;



CREATE TABLE `ibms_reservation_record_2_custom_user_group`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `created_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `version` int(11) NULL DEFAULT NULL COMMENT '版本号',
  `custom_user_group_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'FK 用户组id',
  `record_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'FK',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKio6os1g5i50d9g4d6tvokfs3p`(`custom_user_group_id`) USING BTREE,
  INDEX `FKftrn9h6w7has3h6m4reabu2xj`(`record_id`) USING BTREE,
  CONSTRAINT `FKftrn9h6w7has3h6m4reabu2xj` FOREIGN KEY (`record_id`) REFERENCES `ibms_reservation_record` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKio6os1g5i50d9g4d6tvokfs3p` FOREIGN KEY (`custom_user_group_id`) REFERENCES `ibms_custom_user_group` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '会议记录2用户组' ROW_FORMAT = Dynamic;