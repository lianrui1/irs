-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 10.0.1.232    Database: ibms_uat
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `audio_record`
--

DROP TABLE IF EXISTS `audio_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `audio_record` (
  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL COMMENT '主键',
  `created_by` varchar(36) CHARACTER SET utf8mb4 DEFAULT NULL,
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(36) CHARACTER SET utf8mb4 DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int(11) DEFAULT NULL COMMENT '版本号',
  `end_time` datetime DEFAULT NULL COMMENT '语音结束时间',
  `flv_file_path` varchar(80) CHARACTER SET utf8mb4 DEFAULT NULL,
  `pcm_file_path` varchar(80) CHARACTER SET utf8mb4 DEFAULT NULL,
  `start_time` datetime DEFAULT NULL COMMENT '语音开始时间',
  `text_file_path` varchar(80) CHARACTER SET utf8mb4 DEFAULT NULL,
  `reservation_record_id` varchar(36) COLLATE utf8mb4_bin NOT NULL COMMENT 'FK',
  `audio_file_path` varchar(80) CHARACTER SET utf8mb4 DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FKjebe96o1xd287henk30b9bhdu` (`reservation_record_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='语音记录数据表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audio_record`
--

LOCK TABLES `audio_record` WRITE;
/*!40000 ALTER TABLE `audio_record` DISABLE KEYS */;
INSERT INTO `audio_record` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `end_time`, `flv_file_path`, `pcm_file_path`, `start_time`, `text_file_path`, `reservation_record_id`, `audio_file_path`) VALUES ('1',NULL,'2021-01-21 21:17:03',0,NULL,'2021-01-21 21:17:07',0,'2021-01-21 21:17:10',NULL,NULL,'2021-01-21 21:17:13',NULL,'1b5e8e32-9dbd-4e83-bfc7-6d15cc6b5599','/group1/M00/00/12/rBUAAV_-2CuASR5eAAqALP_MAy4192.mp3');
/*!40000 ALTER TABLE `audio_record` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-25 15:53:41
