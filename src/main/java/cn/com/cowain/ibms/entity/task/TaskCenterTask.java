package cn.com.cowain.ibms.entity.task;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.enumeration.task.TaskOperateResult;
import cn.com.cowain.ibms.enumeration.task.TaskStatus;
import cn.com.cowain.ibms.enumeration.task.TaskType;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author wei.cheng
 * @date 2022/03/10 16:03
 */
@Data
@Entity
@Table(name = "ibms_task_center_task")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "ibms_task_center_task", comment = "待办中心task记录表")
public class TaskCenterTask extends BaseEntity {
    /**
     * 待办中心task.id
     */
    @Column(name = "task_center_id", nullable = false, columnDefinition = "bigint(64) COMMENT '待办中心task.id'")
    private Long taskCenterId;

    /**
     * 流程到达审批人时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", shape = JsonFormat.Shape.STRING)
    @Column(name = "arrvial_time", columnDefinition = "datetime COMMENT '流程到达审批人时间'")
    private LocalDateTime arrivalTime;
    /**
     * 任务申请人名称
     */
    @Column(name = "claimer_name", columnDefinition = "varchar(30) COMMENT '任务申请人名称'")
    private String claimerName;
    /**
     * 任务申请人工号
     */
    @Column(name = "claimer_hr_id", columnDefinition = "varchar(20) COMMENT '任务申请人工号'")
    private String claimerHrId;
    /**
     * 发起人视角流程详情地址
     */
    @Column(name = "claimer_url", columnDefinition = "varchar(500) COMMENT '发起人视角流程详情地址'")
    private String claimerUrl;
    /**
     * 根据模板的定义需要传入的数据，json格式
     */
    @Column(name = "data", columnDefinition = "json COMMENT '根据模板的定义需要传入的数据'")
    private String data;
    /**
     * 审批环节状态，`1`审批中，`2`已批准，`3`已驳回，`4`已撤回
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "operate_result", columnDefinition = "varchar(50) COMMENT '审批环节状态'")
    private TaskOperateResult operateResult;
    /**
     * 审批任务状态，`1`代表待办，`2`代表已办
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "status", columnDefinition = "varchar(50) COMMENT '审批任务状态'")
    private TaskStatus status;
    /**
     * 审批任务ID
     */
    @Column(name = "task_id", columnDefinition = "varchar(36) COMMENT '审批任务ID'")
    private String taskId;
    /**
     * 模版ID
     */
    @Column(name = "template_id", columnDefinition = "varchar(50) COMMENT '模版ID'")
    private String templateId;
    /**
     * 审批任务类型
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "type", columnDefinition = "varchar(50) COMMENT '审批任务类型'")
    private TaskType type;
    /**
     * 用户工号-给谁的任务
     */
    @Column(name = "uc_hr_id", columnDefinition = "varchar(20) COMMENT '用户工号-给谁的任务'")
    private String ucHrId;
    /**
     * 用户名-给谁的任务
     */
    @Column(name = "uc_name", columnDefinition = "varchar(30) COMMENT '用户名-给谁的任务'")
    private String ucName;
    /**
     * 任务详情地址
     */
    @Column(name = "url", columnDefinition = "varchar(500) COMMENT '任务详情地址'")
    private String url;
}
