package cn.com.cowain.ibms.service;

import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.rest.resp.EhrDeptResp;
import cn.com.cowain.ibms.rest.resp.EhrStaffResp;
import cn.com.cowain.ibms.rest.resp.EhrStaffRespPage;
import cn.com.cowain.ibms.rest.resp.EhrStaffRespRoot;
import com.alibaba.fastjson.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/8/4 17:11
 */
@SpringBootTest
class EhrRemoteServiceTest {

    @Autowired
    private EhrRemoteService ehrService;

    @Test
    void getDepartmentTree() {
        EhrDeptResp departmentTree = ehrService.getDepartmentTree();
        System.out.println(departmentTree);
    }

    @Test
    void findSysUserByDeptId() {
        EhrStaffRespPage staffRespPage = ehrService.getUserByDeptId("46");
        System.out.println(staffRespPage);
    }

    @Test
    void findSysUserByName() {
        EhrStaffRespRoot ehrStaffRespRoot = ehrService.findSysUserByNameContains("王冬");
        System.out.println(ehrStaffRespRoot);
    }

    @Test
    void findByEmpNo() {
        List<String> empNoList = Arrays.asList("CHT016", "CWA888");
        List<EhrStaffResp> byEmpNo = ehrService.findByEmpNo(empNoList);
        System.out.println(byEmpNo);
    }

    @Test
    void getInfoById() {
        String sysId = "2128";
        SysUser sysUser = ehrService.getInfoByUserId(sysId);
        System.out.println(sysUser);
    }

    @Test
    void serviceCheck() {
        boolean b = ehrService.serviceCheck();
        Assertions.assertTrue(b);
    }

    @Test
    void getAuth2UserInfo(){
        JSONObject body = ehrService.getAuth2UserInfo("asdasd");
        System.out.println(body);
    }

}
