package cn.com.cowain.ibms.entity.distinguish;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.iot.AccessControlSpot;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 该表只储存门禁点与旷世设备对应的人员组信息
 *
 * @author Yang.Lee
 * @date 2021/12/1 10:31
 */
@Data
@Entity
@Table(name = "ibms_access_control_spot_staff_group_relation")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_access_control_spot_staff_group_relation", comment = "门禁点与人员组关系表")
@Where(clause = "is_del=0")
public class AccessControlSpotStaffGroupRelation extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "staff_group_id", nullable = false, columnDefinition = "varchar(36) COMMENT '人员组ID'")
    private StaffGroup staffGroup;

    @ManyToOne
    @JoinColumn(name = "access_control_spot_id", nullable = false, columnDefinition = "varchar(36) COMMENT '门禁点Id'")
    private AccessControlSpot accessControlSpot;

    @Column(name = "is_default", columnDefinition = "tinyint(1) default 0 COMMENT '是否门禁点默认人员组，1-是，0-否'")
    private int isDefault;
}
