package cn.com.cowain.ibms.service.office;

import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.rest.resp.device.centralized.CentralizedControlDeviceFilter;

import java.util.Map;

public interface DevicePropertiesService {

     <T>T getPropertiesFromDeviceRedis(String hwDeviceId, Class<T> tClass);

     <T>T getPropertiesFromDeviceRedis(String hwDeviceId, DeviceType deviceType, Class<T> tClass);

     <T>T getPropertiesFromDeviceRedis(IotDevice iotDevice, Class<T> tClass);

     <T>T addPropertiesToDeviceRedis(IotDevice iotDevice, Class<T> respClass);

     <T>T addPropertiesToDeviceRedis(IotDevice iotDevice, T deviceDetailResp, Class<T> respClass);


     void filterDevice(IotDevice iotDevice, Map<String, String> paras, CentralizedControlDeviceFilter controlDeviceFilter);
}
