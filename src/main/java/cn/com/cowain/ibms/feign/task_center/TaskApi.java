package cn.com.cowain.ibms.feign.task_center;

import cn.com.cowain.ibms.feign.task_center.bean.CreateTaskReq;
import cn.com.cowain.ibms.feign.task_center.bean.CreateTaskResp;
import cn.com.cowain.ibms.feign.task_center.bean.UpdateTaskReq;
import cn.com.cowain.sfp.comm.JsonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author wei.cheng
 * @date 2022/03/10 15:02
 */
@Service
@RequestMapping({"/ms-task-center"})
@FeignClient(value = "ms-task-center-task", url = "${cowain.gateway.address}")
public interface TaskApi {

    /**
     * 新建task
     *
     * @param req
     * @author wei.cheng
     * @date 2022/3/10 3:06 下午
     **/
    @PostMapping("/task")
    JsonResult<CreateTaskResp> createTask(@RequestBody CreateTaskReq req);

    /**
     * 更新task
     *
     * @param req
     * @author wei.cheng
     * @date 2022/3/11 5:57 下午
     **/
    @PostMapping("/task/update")
    JsonResult<Object> updateTask(@RequestBody UpdateTaskReq req);
}
