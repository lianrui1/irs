package cn.com.cowain.ibms.feign.iotc.enumeration;

/**
 * @author Yang.Lee
 * @date 2022/2/14 19:18
 */
public enum ModuleCode {

    KSHT("KSHT"),

    HW("HW"),

    TSDB("TSDB"),

    HIK("HIK");

    private String name;

    ModuleCode(String name) {
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
}
