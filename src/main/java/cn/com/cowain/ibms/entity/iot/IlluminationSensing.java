package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author Yang.Lee
 * @date 2021/2/18 14:18
 */
@Data
@Entity
@Table(name = "iot_illumination_sensing")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_illumination_sensing", comment = "光照度传感器设备表")
public class IlluminationSensing extends BaseEntity {


    private static final long serialVersionUID = -7000491532407348229L;

    @Column(name = "value", columnDefinition = "varchar(256) COMMENT '光照度数值'")
    private String value;

    /**
     * iot 设备
     */
    @ManyToOne
    @JoinColumn(name = "device_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK。设备表主键id'")
    private IotDevice device;
}
