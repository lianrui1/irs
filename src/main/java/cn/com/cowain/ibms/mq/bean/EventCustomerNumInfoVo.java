package cn.com.cowain.ibms.mq.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author tql
 * @Description EventCustomerNumInfoVo
 * @Date 21-5-24 下午5:34
 * @Version 1.0
 */
@Data
public class EventCustomerNumInfoVo {

    /**
     * 通道号
     */
    @JsonProperty("AccessChannel")
    @ApiModelProperty(value = "通道号")
    private Integer accessChannel;

    /**
     * 进人数
     */
    @JsonProperty("EntryTimes")
    @ApiModelProperty(value = "进人数")
    private Integer entryTimes;

    /**
     * 出人数
     */
    @JsonProperty("ExitTimes")
    @ApiModelProperty(value = "出人数")
    private Integer exitTimes;

    /**
     * 总通行人数
     */
    @JsonProperty("TotalTimes")
    @ApiModelProperty(value = "总通行人数")
    private Integer totalTimes;
}
