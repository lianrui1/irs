package cn.com.cowain.ibms.entity.iotc;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Yang.Lee
 * @date 2021/10/20 16:12
 */
@Data
@Entity
@Table(name = "iotc_device_application_relation")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "iotc_device_application_relation", comment = "设备-应用关系表")
@Where(clause = "is_del=0")
public class DeviceApplicationRelation extends BaseEntity {

    /**
     * iot 设备
     */
    @ManyToOne
    @JoinColumn(name = "device_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK'")
    private IotDevice device;

    @ManyToOne
    @JoinColumn(name = "application_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK'")
    private Application application;
}
