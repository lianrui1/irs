package cn.com.cowain.ibms.dao.qrcode;

import cn.com.cowain.ibms.entity.qrcode.QrcodeAccessRules;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

/**
 * 二维码Dao
 *
 * @author Yang.Lee
 * @date 2021/4/7 10:16
 */
public interface QrcodeAccessRuleDao extends JpaRepository<QrcodeAccessRules, String>, JpaSpecificationExecutor<QrcodeAccessRules> {

    /**
     * 根据ID查询门磁设备是否已有通行权限
     *
     * @param id 门磁ID
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/4/8 17:06
     **/
    Optional<QrcodeAccessRules> findByDoorMagneticId(String id);
}
