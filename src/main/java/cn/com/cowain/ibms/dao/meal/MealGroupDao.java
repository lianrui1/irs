package cn.com.cowain.ibms.dao.meal;

import cn.com.cowain.ibms.entity.meal.KsHtTimePlan;
import cn.com.cowain.ibms.entity.meal.MealGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author Yang.Lee
 * @date 2022/1/14 10:54
 */
public interface MealGroupDao extends JpaRepository<MealGroup, String>, JpaSpecificationExecutor<MealGroup> {

    /**
     * @param ksHtTimePlan
     * @return
     * @description 根据时间计划找到报餐组
     * @author tql
     * @date 22-5-19
     */
    List<MealGroup> findByKsHtTimePlan(KsHtTimePlan ksHtTimePlan);

}
