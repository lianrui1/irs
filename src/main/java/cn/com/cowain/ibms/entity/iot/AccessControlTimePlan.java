package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.rest.bean.WorkDay;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import java.util.List;

/**
 * @author wei.cheng
 * @date 2022/09/28 13:27
 */
@Data
@Entity
@Table(name = "iot_access_control_time_plan", indexes = {@Index(columnList = "user_hr_id,spot_id")})
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_access_control_time_plan", comment = "通行时间计划")
@TypeDef(name = "json", typeClass = JsonStringType.class)
public class AccessControlTimePlan extends BaseEntity {

    @Column(name = "spot_id", nullable = false, columnDefinition = "varchar(36) COMMENT '门禁点id'")
    private String spotId;

    @Column(name = "user_hr_id", nullable = false, columnDefinition = "varchar(50) COMMENT '用户工号'")
    private String userHrId;

    @Type(type = "json")
    @Column(name = "work_days", columnDefinition = "json COMMENT '日程时段'")
    private List<WorkDay> workDays;
}
