package cn.com.cowain.ibms.mq.bean;

import cn.com.cowain.ibms.enumeration.ability.Ability;
import cn.com.cowain.ibms.enumeration.ability.MethodType;
import cn.com.cowain.ibms.enumeration.iot.AccessControlSpotPlace;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 开放能力消息
 *
 * @author Yang.Lee
 * @date 2022/1/26 16:11
 */
@Data
@ApiModel
public class OpenAbilityMessage {

    @ApiModelProperty(value = "能力", required = true, example = "VISITOR", position = 1)
    private Ability ability;

    @ApiModelProperty(value = "用户列表", required = true, position = 2)
    private List<User> userList;

    @ApiModelProperty(value = "用户列表", required = true, position = 2)
    private List<Meal> mealTimeList;

    @ApiModelProperty(value = "执行方式", required = true, example = "ADD", position = 3)
    private MethodType type;

    @ApiModelProperty(value = "数据发送结束标识位", required = true, example = "1", position = 4)
    private String over;


    @Data
    public static class Meal {
        @ApiModelProperty(value = "结束时间", required = true, example = "14:30", position = 1)
        private String endTime;
        @ApiModelProperty(value = "地点", required = true, example = "KS", position = 2)
        private AccessControlSpotPlace place;
        @ApiModelProperty(value = "开始时间", required = true, example = "14:30", position = 3)
        private String startTime;

    }

    @Data
    public static class User {
        @ApiModelProperty(value = "用户工号", required = true, example = "CWA8888", position = 1)
        private String hrId;

        @ApiModelProperty(value = "姓名", required = true, example = "张三", position = 2)
        private String name;

        @ApiModelProperty(value = "部门", example = "ABC", position = 3)
        private String dept;

        @ApiModelProperty(value = "部门编号", example = "123", position = 4)
        private String code;

        @ApiModelProperty(value = "手机号码", example = "13000000000", position = 5)
        private String phone;

        @ApiModelProperty(value = "用户图片，url", position = 6)
        private String photo;

        @ApiModelProperty(value = "权限开始时间", example = "2088-01-01 00:00:00", position = 7)
        private LocalDateTime startTime;

        @ApiModelProperty(value = "权限结束时间", example = "2099-01-01 00:00:00", position = 8)
        private LocalDateTime endTime;

        @ApiModelProperty(value = "邀请人的工号", example = "2099-01-01 00:00:00", position = 9)
        private String invitedUserHrId;

        @ApiModelProperty(value = "地点，报餐及访客使用", position = 10)
        private AccessControlSpotPlace place;

        @ApiModelProperty(value = "员工职级", position = 11)
        private int positionLevel;

        @ApiModelProperty(value = "访客类型，也是EHR访客类型接口的code字段", position = 12)
        private String visitorType;

        @ApiModelProperty(value = "离职类型。0:离职，1立即离职", position = 13)
        private Integer leaveType;
    }

}
