package cn.com.cowain.ibms.dao;


import cn.com.cowain.ibms.entity.ReservationRecordItem;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@SpringBootTest
public class ReservationRecordItemDaoTest2 {

    @Autowired
    private ReservationRecordItemDao rriDao;

    @Autowired
    private RoomServiceDao roomServiceDao;

    @Test
    void findByInitiatorEqualsAndOwnerIsNot() {
        String initiator = "testId22";
        Sort sort = Sort.by(Sort.Direction.DESC, "createdTime");
        int size = 10;
        Pageable pageable = PageRequest.of(0, size, sort);
        Page<ReservationRecordItem> page = rriDao.findByInitiatorEmpNoEqualsAndOwnerEmpNoIsNot(initiator, initiator, pageable);
        System.out.println(page);
        List<String> list = page.get().map(it -> it.getOwner()).distinct().collect(Collectors.toList());
        System.out.println(list);
    }

}
