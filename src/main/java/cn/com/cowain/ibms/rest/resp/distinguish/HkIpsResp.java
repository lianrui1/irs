package cn.com.cowain.ibms.rest.resp.distinguish;

import lombok.Data;

import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/5 9:51
 */
@Data
public class HkIpsResp {

    private List<String> ksIps;

    private List<String> ntIps;
}
