package cn.com.cowain.ibms.entity.voice;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 语音操作记录日志
 * @author Yang.Lee
 * @date 2020/12/29 13:01
 */
@Data
@Entity
@Table(name = "audio_operations_record")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "audio_operations_record", comment = "语音操作记录日志数据表")
public class AudioOperationsRecord extends BaseEntity {

    /**
     * 语音记录id
     */
    @ManyToOne
    @JoinColumn(name = "audio_record_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK'")
    private AudioRecord audioRecord;

    /**
     * 操作名称
     */
    @Column(name = "operation_name", nullable = false, columnDefinition = "varchar(100) COMMENT '操作名称'")
    private String operationName;

    /**
     * 操作时间
     */
    @Column(name = "operation_time", nullable = false, columnDefinition = "datetime COMMENT '操作时间'")
    private LocalDateTime operationTime;
}
