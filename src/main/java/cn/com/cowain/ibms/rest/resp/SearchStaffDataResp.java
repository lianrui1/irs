package cn.com.cowain.ibms.rest.resp;

import lombok.Data;

/**
 * @Author tql
 * @Description SearchStaffDataResp
 * @Date 21-5-20 下午9:05
 * @Version 1.0
 */
@Data
public class SearchStaffDataResp {

    private String ccDeviceId;

    private String ccDeviceCode;

    private String hrId;

    private String personType;


    /**
     * 设备类型
     */
    private String deviceType;

    private String type;

    private String checkinSite;

    private String checkinTime;
}
