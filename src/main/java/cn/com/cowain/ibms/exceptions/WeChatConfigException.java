package cn.com.cowain.ibms.exceptions;

/**
 * @author Yang.Lee
 * @date 2020/12/15 15:45
 */
public class WeChatConfigException extends RuntimeException{

    public WeChatConfigException(String message){
        super(message);
    }
}
