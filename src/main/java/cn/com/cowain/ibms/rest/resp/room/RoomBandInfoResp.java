package cn.com.cowain.ibms.rest.resp.room;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author feng
 * @title: RoomBandInfoResp
 * @projectName ibms
 * @Date 2021/12/27 19:56
 */
@Data
public class RoomBandInfoResp {
    @ApiModelProperty(value = "项目ID", required = true)
    private String projectId;

    @ApiModelProperty(value = "项目区域", required = true)
    private String projectArea;

    @ApiModelProperty(value = "会议室id", required = true)
    private String roomId;

    @ApiModelProperty(value = "会议室名字", required = true)
    private String roomName;
    @ApiModelProperty(value = "空间id", required = true)
    private String spaceId;

    @ApiModelProperty(value = "大屏设备Id", required = true)
    private String meetingAppId;
    @ApiModelProperty(value = "是否已绑定", required = true)
    private Boolean  hasBand;
}
