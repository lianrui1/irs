package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 设备-to-空间映射表
 *
 * @author cuiEnming
 * @date 2020/11/19  18:51
 * @since s2
 */
@Data
@Entity
@Table(name = "iot_device_to_space")
@org.hibernate.annotations.Table(appliesTo = "iot_device_to_space", comment = "设备-to-空间映射表")
@Where(clause = "is_del=0")
public class DeviceToSpace extends BaseEntity {

    /**
     * iot 设备
     */
    @ManyToOne
    @JoinColumn(name = "device_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'iot_device FK'")
    private IotDevice device;

    /**
     * iot 空间
     */
    @ManyToOne
    @JoinColumn(name = "space_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'iot_space FK'")
    private Space space;

}
