package cn.com.cowain.ibms.dao.iot.lift_column;

import cn.com.cowain.ibms.entity.iot.LiftColumn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * 升降柱设备DAO
 **/
@Repository
public interface LiftColumnDao extends JpaRepository<LiftColumn, String>, JpaSpecificationExecutor<LiftColumn> {


}
