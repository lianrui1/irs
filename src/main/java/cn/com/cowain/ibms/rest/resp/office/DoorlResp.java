package cn.com.cowain.ibms.rest.resp.office;

import lombok.Data;

/**
 * @author feng
 * @title: AirConditionerStatusDetailResp
 * @projectName ibms
 * @Date 2021/12/21 14:07
 */
@Data
public class DoorlResp  {
    String id;
    String hwDeviceId;
    String hwStatus;
    String deviceName;
}
