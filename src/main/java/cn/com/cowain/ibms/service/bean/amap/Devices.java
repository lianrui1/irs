package cn.com.cowain.ibms.service.bean.amap;

import cn.com.cowain.ibms.service.bean.Services;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/5/17 10:05
 */
@Data
public class Devices {

    @JsonProperty(value = "device_id")
    private String deviceId;
    private List<Services> services;
}
