package cn.com.cowain.ibms.rest.resp;

import cn.com.cowain.ibms.entity.iot.YSVideoCamera;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author feng
 * @title: MonitorSit
 * @projectName bims
 * @Date 2021/11/18 10:55
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MonitorLocationResp implements Serializable{
        @ApiModelProperty("id")
    String id;
    @ApiModelProperty("名称")
    String name;
    /** 设备sn码 */
    @ApiModelProperty("设备sn码")
    String sn;
    /**
    　　* @title: 品牌
    　　* @description
    　　* @author jfsui
    　　* @date 2021/11/18 13:19
    　　*/
    @ApiModelProperty("设备来源")
    String brand;
    /** 设备类型：摄像机 */
    @ApiModelProperty("设备类型：摄像机")
    String typeName;
    /**
    　　* @title: 具体位置
    　　* @description  摄像头所在的具体位置
    　　* @author jfsui
    　　* @date 2021/11/18 13:20
    　　*/
    @ApiModelProperty("具体位置")
    String location;
    @ApiModelProperty("编码")
    String code;

    @ApiModelProperty("华为云的设备id")
    String hwDeviceId;
    @ApiModelProperty("信号强弱")
    String signal;

     @ApiModelProperty( "网络类型，如有线连接wire")
    private String netType;
    @ApiModelProperty( "告警声模式")
    private int alarmSoundMode;

    @ApiModelProperty( "布防状态")
    private int defence;
    @ApiModelProperty( "监控点ID")
    private String sitId;
    @ApiModelProperty( "监控点名称")
    private String sitName;
    @ApiModelProperty( "是否已经被绑定")
    private Boolean hasBanding=false;
    @ApiModelProperty( "设备Id")
    private String deviceId;
    @ApiModelProperty( "在线状态")
    private String hwStatus;
    @ApiModelProperty( "编辑数据")
   private MonitorSitResp monitorSitResp;
    @ApiModelProperty( "token")
    private String token;

    /**
     * 对象类型转换
     *
     *
     **/
    public static MonitorLocationResp convert(YSVideoCamera source) {
        if(source==null){
            return new MonitorLocationResp();
        }
        return MonitorLocationResp.builder()
                .brand(source.getModel())
                .code(source.getSerial())
                .typeName(source.getYsDeviceType())
                .sn(source.getDevice().getSn())
                .location(source.getNetType())
                .name(source.getDevice().getDeviceName())
                .location(source.getDevice().getAddress())
                .hwDeviceId(source.getDevice().getHwDeviceId())
                .signal(source.getSignal())
                .netType(source.getNetType())
                .alarmSoundMode(source.getAlarmSoundMode())
                .defence(source.getDefence())
                .deviceId(source.getDevice().getId())
                .hwStatus(source.getDevice().getHwStatus().getName())
                .build();
    }
}
