package cn.com.cowain.ibms.rest.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.validation.constraints.Min;
import java.util.ArrayList;
import java.util.List;

/**
 * 分页请求对象
 *
 * @author Yang.Lee
 * @date 2021/3/12 10:16
 */
@Data
@ApiModel("分页请求参数")
public class PageReq {

    @Min(value = 0, message = "页码不能小于0")
    @ApiModelProperty(value = "页码，从0开始，默认0。", example = "0", position = 997)
    private int page = 0;

    @Min(value = 1, message = "页长不能小于1")
    @ApiModelProperty(value = "页长，从1开始，默认10。", example = "10", position = 998)
    private int size = 10;

    @ApiModelProperty(value = "排序方式", position = 999)
    private List<Sorts> sorts;

    /*
     * 获取分页实例的排序方式
     * @return 排序列表
     * @author Yang.Lee
     * @date 2021/4/8 16:12
     **/
    public List<Sort.Order> getOrders() {

        initSorts();
        List<Sort.Order> orders = new ArrayList<>();

        this.sorts.forEach(obj -> orders.add(new Sort.Order(obj.getDirection(), obj.getSortName())));

        return orders;
    }

    /**
     * 为该分页实列添加排序方式
     *
     * @param sort 排序方式
     * @author Yang.Lee
     * @date 2021/4/8 16:12
     **/
    public void addOrder(Sorts sort) {

        initSorts();
        this.sorts.add(sort);
    }

    /**
     * 为该分页实列添加排序方式
     *
     * @param name      排序字段
     * @param direction 排序方式。 DESC / ASC
     * @author Yang.Lee
     * @date 2021/4/8 16:12
     **/
    public void addOrder(String name, Sort.Direction direction) {

        initSorts();
        this.sorts.add(Sorts.getInstance(name, direction));
    }

    /**
     * 为该分页实列添加一个正序的排序方式
     *
     * @param name 排序字段
     * @author Yang.Lee
     * @date 2021/4/8 16:13
     **/
    public void addOrderAsc(String name) {

        addOrder(name, Sort.Direction.ASC);
    }

    /**
     * 为该分页实列添加一个倒叙的排序方式
     *
     * @param name 排序字段
     * @author Yang.Lee
     * @date 2021/4/8 16:13
     **/
    public void addOrderDesc(String name) {

        addOrder(name, Sort.Direction.DESC);
    }

    /**
     * 创建Pageable对象
     *
     * @return Pageable对象
     * @author Yang.Lee
     * @date 2021/4/8 16:39
     **/
    public Pageable createPageable() {
        return PageRequest.of(this.page, this.size, Sort.by(getOrders()));
    }

    /**
     * 初始化排序对象，如果排序对象为null，则初始化一个空集合
     *
     * @author Yang.Lee
     * @date 2021/4/8 16:15
     **/
    private void initSorts() {
        if (this.sorts == null) {
            this.sorts = new ArrayList<>();
        }
    }
}