package cn.com.cowain.ibms.rest.resp.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author wei.cheng
 * @date 2022/06/29 14:31
 */
@Data
@ApiModel(value = "权限中门禁点详情")
public class AuthSpotResp {

    @ApiModelProperty(value = "门禁id")
    private String id;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "位置")
    private String location;

    @ApiModelProperty(value = "申请状态, 可选值：`0`未申请,`1`审批中,`2`审批拒绝,`3`已拒绝")
    private String applicationStatus;

    @ApiModelProperty(value = "申请状态描述")
    private String applicationStatusDesc;
}
