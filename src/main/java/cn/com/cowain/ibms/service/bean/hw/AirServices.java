package cn.com.cowain.ibms.service.bean.hw;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * todo
 *
 * @author: yanzy
 * @date: 2022/8/16 15:34
 */
@Data
public class AirServices {
    @JsonProperty(value = "service_id")
    private String serviceId;
    private List<HwAirProperties> properties;
    private String eventTime;
}

