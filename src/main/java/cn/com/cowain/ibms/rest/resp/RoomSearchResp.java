package cn.com.cowain.ibms.rest.resp;

import cn.com.cowain.ibms.rest.req.OpeningHoursReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Description
 *
 * @author 张宇鑫
 * @version 1.0
 * @date 2020/8/8 12:38
 */
@Data
public class RoomSearchResp implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "会议室id", required = true, position = 0)
    private String id;

    @ApiModelProperty(value = "会议室名称", required = true, position = 0)
    private String name;

    /**
     * 人数容量
     */
    @ApiModelProperty(value = "会议室容纳人数", required = true, position = 0)
    private Integer capacity;

    /**
     * 楼栋
     */
    @ApiModelProperty(value = "会议室楼栋", required = true, position = 0)
    private String building;

    /**
     * 楼层
     */
    @ApiModelProperty(value = "会议室楼层", required = true, position = 0)
    private String floor;

    /**
     * 是否开放 0:否 1:是
     */
    @ApiModelProperty(value = "会议室是否开放", required = true, position = 0)
    private int isOpen;

    /**
     * 开放时间
     */
    @ApiModelProperty(value = "会议室开放时间", required = true, position = 0)
    private List<OpeningHoursReq> openingHours;

    /**
     * 包含设备
     */
    @ApiModelProperty(value = "会议室包含设备", required = true, position = 0)
    private List<String> devices;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "会议室创建人", required = true, position = 0)
    private String createUser;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "会议室更新人", required = false, position = 0)
    private String updateUser;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "会议室创建时间", required = true, position = 0)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "会议室更新时间", required = false, position = 0)
    private LocalDateTime updateTime;

    /**
     * 项目ID
     */
    @ApiModelProperty(value = "项目ID", required = true, position = 0)
    private String projectId;

    /**
     * 项目区域
     */
    @ApiModelProperty(value = "项目区域", required = true, position = 0)
    private String projectArea;

    /**
     * 会议室是否被删除状态 0存在预约  1无预约
     */
    @ApiModelProperty(value = "会议室是否有预约", required = true, position = 0)
    private int remove;

    @ApiModelProperty(value = "空间ID", required = true)
    private String spaceId;

    @ApiModelProperty(value = "空间名称", required = true)
    private String spaceName;

    @ApiModelProperty(value = "注意事项")
    private String attentions;

    @ApiModelProperty(value = "物件清单")
    private List<String> thingList;

    @ApiModelProperty(value = "会议室图片")
    private String img;

    @ApiModelProperty(value = "源文件文件名")
    private String originalImgName;

    @ApiModelProperty(value = "会议室详细地址")
    private String roomAddress;

    @ApiModelProperty(value = "是否存在会议大屏 1有/2没有")
    private Integer isMeetingApp;

    @ApiModelProperty(value = "是否存在门磁 1有/2没有")
    private Integer isDoorMagnetic;
}
