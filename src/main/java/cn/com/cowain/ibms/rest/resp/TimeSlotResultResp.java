package cn.com.cowain.ibms.rest.resp;

import cn.com.cowain.ibms.utils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;

/**
 * 时间槽
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/6/20
 */
@Data
@ApiModel("时间槽")
public class TimeSlotResultResp {

    /**
     * 空参构造
     */
    public TimeSlotResultResp() {
    }

    /**
     * 有参构造
     *
     * @param from
     * @param to
     * @param status
     */
    public TimeSlotResultResp(String from, String to, int status) {
        this.from = from;
        this.to = to;
        this.status = status;
    }

    public TimeSlotResultResp(String from, String to, LocalDate date,  int status) {
        this.from = from;
        this.to = to;
        this.status = status;
        this.date = date;
    }


    /**
     * 从时间
     */
    @ApiModelProperty(value = "从时间", required = true, position = 0)
    private String from;

    /**
     * 到时间
     */
    @ApiModelProperty(value = "到时间", required = true, position = 1)
    private String to;

    /**
     * 0:不能预定
     * 1:可以预定
     * 2:已预定
     */
    @ApiModelProperty(value = "0:不能预定, 1:可以预定, 2:已预定", required = true, position = 2)
    private int status;

    /**
     * roomId
     */
    @ApiModelProperty(value = "roomId", position = 3)
    private String roomId;

    /**
     * H5前端专用(后端不用)
     */
    @ApiModelProperty(value = "H5前端专用(后端不用)", position = 4)
    private boolean checked;

    /**
     * H5前端专用(后端不用)
     */
    @ApiModelProperty(value = "H5前端专用(后端不用)", position = 5)
    private boolean used;

    /**
     * EHR 员工数据
     */
    @ApiModelProperty(value = "员工数据", position = 9)
    private EhrStaffResp ehrStaff;

    @ApiModelProperty(value = "会议记录ID", required = true, position = 10)
    private String recordId;

    @ApiModelProperty(value = "会议主题", required = true, position = 11)
    private String topic;

    @JsonFormat(pattern = DateUtils.PATTERN_DATE)
    @ApiModelProperty(value = "时间",  position = 12)
    private LocalDate date;
}
