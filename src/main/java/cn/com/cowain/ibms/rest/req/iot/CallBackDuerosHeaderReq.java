package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Author tql
 * @Description dueros回调
 * @Date 22-9-26 下午3:21
 * @Version 1.0
 */
@Data
@ApiModel("返回头")
public class CallBackDuerosHeaderReq {

    @ApiModelProperty("payload的版本号")
    @NotBlank
    private String payloadVersion;

    @ApiModelProperty("消息的唯一标识符")
    @NotBlank
    private String messageId;

    @ApiModelProperty("指令的名称")
    @NotBlank
    private String name;

    @ApiModelProperty("指令的类别")
    @NotBlank
    private String namespace;

}
