package cn.com.cowain.ibms.service.audio;

import cn.com.cowain.ibms.entity.voice.AudioRecord;
import cn.com.cowain.ibms.rest.req.audio.RealtimeUpload;
import org.csource.common.MyException;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * 科大讯飞通信业务service
 * @author Yang.Lee
 * @date 2021/1/6 13:39
 */
public interface AudioTranscriptionService {

    /**
     * 语音转写
     * @param filePath 语音文件路径
     * @return
     */
    String translateAudio(String filePath) throws InterruptedException;

    /**
     * 进行语音转写
     * @param audioRecord
     * @throws Exception
     */
    void translate(AudioRecord audioRecord) throws IOException, MyException, InterruptedException, URISyntaxException;

    /**
     * 实时语音补传转写
     * @param audioId    语音音频
     * @param realtimeUpload  参数（文件，开始时间，结束时间）
     */
    void realtimeUploadTranslate(String audioId, RealtimeUpload realtimeUpload);
}
