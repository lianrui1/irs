package cn.com.cowain.ibms.service;

import cn.com.cowain.ibms.dao.RoomDao;
import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.service.bean.WeChatMsg;
import cn.com.cowain.ibms.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.Set;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/14/20
 */
@Slf4j
@SpringBootTest
class WeChatServiceTest {

    @Autowired
    private WeChatService weChatService;

    @Autowired
    private RoomDao roomDao;

//    @Value("#{'${cowain.emp.executor.list}'.split(',')}")
//    private List<String> executorWxOpenId;

    @Value("${cowain.emp.executor.list}")
    private Set<String> executorList;

    @Test
    public void test0() {
        Assertions.assertNotNull(weChatService);
    }

    /**
     * oLh_dv2rTGEy5adEaRgSAF2I--SA - hjl
     * oLh_dv1tKrijAShPxq3do-4vMQCE - zyx
     */
    @Test
    public void test1() {
        String url = "http://www.asdfasdf.com";
//        String toUserWxId = "oLh_dv2rTGEy5adEaRgSAF2I--SA";
        String toUserWxId = "oLh_dv1tKrijAShPxq3do-4vMQCE";
        WeChatMsg weChatMsg = WeChatMsg.builder().first("测试000")
                .keyword1("11").keyword2("22").keyword3("333").remark("asfasd123123").build();
        String msgId = weChatService.sendMsg(url, toUserWxId, weChatMsg);
        System.out.println(msgId);
    }

    @Test
    public void test2() {
        String ownerId = "aaaa";
        ReservationRecord reservationRecord = new ReservationRecord();
        reservationRecord.setTopic("发起会议室测试---标题");
        reservationRecord.setInitiator("createUser");
        reservationRecord.setFrom(LocalDateTime.now());
        reservationRecord.setRoom(roomDao.findById("be8f2e87-0dbf-4e61-9fdd-87122865c9ab").get());
        reservationRecord.setRemark("测试时的备注");
        weChatService.createMeetingReminders(ownerId, reservationRecord);

    }

    @Test
    public void test3() {
        Set<String> a = executorList;
        log.info(String.valueOf(executorList));
        log.info(String.valueOf(a));
    }


    @Test
    public void test4() {
        StringBuilder stringBuffer = new StringBuilder("");
        for (int i = 0; i < 4; i++) {
            if (!stringBuffer.toString().equals("")) {
                stringBuffer.append("、");
            }
            if (i == 0) {
                stringBuffer.append("000");
            } else if (i == 1) {
                stringBuffer.append("111");
            } else if (i == 2) {
                stringBuffer.append("222");
            } else if (i == 3) {
                stringBuffer.append("333");
            }
        }
        log.info(String.valueOf(stringBuffer));
    }

    @Test
    public void test5() {
        WeChatMsg weChatMsg = WeChatMsg.builder().first("海康服务下线")
                .keyword1("唐其亮").keyword2("下线").keyword3(DateUtils.formatLocalDateTime(null, "yyyy-MM-dd HH:mm:ss")).remark("海康服务下线").build();
        weChatService.sendMsg("", "oLh_dv--SP5CaFu1XakHft3n1Wpw", weChatMsg);
    }
}
