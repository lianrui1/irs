package cn.com.cowain.ibms.enumeration.ability;

/**
 * @author Yang.Lee
 * @date 2022/1/10 15:42
 */
public enum Ability {

    ON_BOARDING("入职"),

    LEAVE("离职"),

    VISITOR("访客"),

    MEAL("报餐"),

    ATTENDANCE("考勤"),

    UPDATE_FACE("重新下发人脸"),

    NT_SCHEDULE("南通排班"),

    ADMINISTRATOR_ADD("管理员添加"),

    REMOVE_AUT("解除权限"),

    OTHER("其它");

    private String name;

    Ability(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
