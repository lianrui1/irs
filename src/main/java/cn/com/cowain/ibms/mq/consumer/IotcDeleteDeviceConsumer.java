package cn.com.cowain.ibms.mq.consumer;

import cn.com.cowain.ibms.mq.Topic;
import cn.com.cowain.ibms.mq.bean.IotcDeleteDeviceMessage;
import cn.com.cowain.ibms.service.space.IotDeviceService;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author wei.cheng
 * @date 2022/03/28 15:49
 */
@Slf4j
@RocketMQMessageListener(
        topic = "${rocketmq.env}-" + Topic.IOTC_DELETE_DEVICE,
        consumerGroup = "${rocketmq.group}-" + Topic.IOTC_DELETE_DEVICE
)
@Component
public class IotcDeleteDeviceConsumer implements RocketMQListener<String> {

    @Autowired
    private IotDeviceService iotDeviceService;

    @Override
    public void onMessage(String message) {
        log.debug("IotcDeleteDeviceConsumer onMessage, message:{}", message);
        try {
            // 解析数据
            IotcDeleteDeviceMessage iotcDeleteDeviceMessage = JSON.parseObject(message, IotcDeleteDeviceMessage.class);
            // 保存结果
            iotDeviceService.deleteBySn(iotcDeleteDeviceMessage.getResourceIndexCode());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
