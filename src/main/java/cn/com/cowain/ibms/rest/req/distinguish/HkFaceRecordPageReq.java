package cn.com.cowain.ibms.rest.req.distinguish;

import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author tql
 * @email 2332347075@qq.com
 * @date 2021/7/22 14:05
 */
@Data
public class HkFaceRecordPageReq extends PageReq {

    @ApiModelProperty(value = "设备Id" , required = true)
    private String deviceId;

    @ApiModelProperty(value = "人员姓名/工号", example = "张三", position = 1)
    private String name;

    @ApiModelProperty(value = "开始日期", example = "2021-07-22 00:00:00", position = 2)
    private String startDate;

    @ApiModelProperty(value = "结束日期", example = "2021-07-22 00:00:00", position = 2)
    private String endDate;

}
