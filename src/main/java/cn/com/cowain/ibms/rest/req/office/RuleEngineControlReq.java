package cn.com.cowain.ibms.rest.req.office;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/2/16 15:48
 */
@Data
public class RuleEngineControlReq {

    @NotEmpty
    private String ruleChainId;

    private List<RuleEngIneDetailActuatorReq> saveReqs;

    private String workingModeId;
}
