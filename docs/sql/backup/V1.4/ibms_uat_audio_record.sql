-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 10.0.1.232    Database: ibms_uat
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `audio_record`
--

DROP TABLE IF EXISTS `audio_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `audio_record` (
  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL COMMENT '主键',
  `created_by` varchar(36) CHARACTER SET utf8mb4 DEFAULT NULL,
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(36) CHARACTER SET utf8mb4 DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int(11) DEFAULT NULL COMMENT '版本号',
  `end_time` datetime DEFAULT NULL COMMENT '语音结束时间',
  `pcm_file_path` varchar(80) CHARACTER SET utf8mb4 DEFAULT NULL,
  `start_time` datetime DEFAULT NULL COMMENT '语音开始时间',
  `text_file_path` varchar(80) CHARACTER SET utf8mb4 DEFAULT NULL,
  `reservation_record_id` varchar(36) COLLATE utf8mb4_bin NOT NULL COMMENT 'FK',
  `audio_file_path` varchar(80) CHARACTER SET utf8mb4 DEFAULT NULL,
  `rt_text_file_path` varchar(80) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '实时翻译文本路径',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FKjebe96o1xd287henk30b9bhdu` (`reservation_record_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='语音记录数据表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audio_record`
--

LOCK TABLES `audio_record` WRITE;
/*!40000 ALTER TABLE `audio_record` DISABLE KEYS */;
INSERT INTO `audio_record` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `end_time`, `pcm_file_path`, `start_time`, `text_file_path`, `reservation_record_id`, `audio_file_path`, `rt_text_file_path`) VALUES ('0c42d6dc-55a2-4740-99d1-218e3abd1154',NULL,'2021-02-05 17:52:19',0,NULL,'2021-02-05 17:52:27',2,'2021-02-05 17:52:20','/group1/M00/00/29/rBUAAWAdFVSATaGlAADAAAF5610421.pcm','2021-02-05 17:52:19','/group1/M00/00/29/rBUAAWAdFVuAITUVAAAAIZRGuD8524.txt','b7b39250-60bd-4671-81a4-9c4d82a7605b','/group1/M00/00/29/rBUAAWAdFVSAKl61AADALJspM1w005.mp3',NULL);
INSERT INTO `audio_record` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `end_time`, `pcm_file_path`, `start_time`, `text_file_path`, `reservation_record_id`, `audio_file_path`, `rt_text_file_path`) VALUES ('1',NULL,'2021-01-21 21:17:03',1,NULL,'2021-01-27 17:08:02',1,'2021-01-21 21:17:10',NULL,'2021-01-21 21:17:13',NULL,'1b5e8e32-9dbd-4e83-bfc7-6d15cc6b5599','/group1/M00/00/12/rBUAAV_-2CuASR5eAAqALP_MAy4192.mp3',NULL);
INSERT INTO `audio_record` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `end_time`, `pcm_file_path`, `start_time`, `text_file_path`, `reservation_record_id`, `audio_file_path`, `rt_text_file_path`) VALUES ('16a7e1e8-97ac-4b3e-9f49-7c97dbd1f805',NULL,'2021-02-05 17:07:39',0,NULL,'2021-02-05 17:08:37',2,'2021-02-05 17:08:21','/group1/M00/00/29/rBUAAWAdCwaAda2lABSQAJMPuxM563.pcm','2021-02-05 17:07:39','/group1/M00/00/29/rBUAAWAdCxSACsd_AAABoX8-z4k312.txt','a00f3e2a-ac7b-44f1-a534-3abb37de9537','/group1/M00/00/29/rBUAAWAdCwaAakHwABSQLLY0NUw887.mp3','/group1/M00/00/29/rBUAAWAdCxSAEfvGAAADnTPYXk0657.txt');
INSERT INTO `audio_record` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `end_time`, `pcm_file_path`, `start_time`, `text_file_path`, `reservation_record_id`, `audio_file_path`, `rt_text_file_path`) VALUES ('3332d0ef-2064-4ba7-aa60-ba0d822d702d',NULL,'2021-02-05 18:05:13',0,NULL,'2021-02-05 18:11:24',2,'2021-02-05 18:10:20','/group1/M00/00/29/rBUAAWAdGZOAbF_GAJaQAEa3FLg598.pcm','2021-02-05 18:05:13','/group1/M00/00/29/rBUAAWAdGcuAbXUqAAARKhmlc3E789.txt','a00f3e2a-ac7b-44f1-a534-3abb37de9537','/group1/M00/00/29/rBUAAWAdGZSAIQ1LAJaQLM6Z9Xc645.mp3','/group1/M00/00/29/rBUAAWAdGcyAPzwWAAAbabbk3PY468.txt');
INSERT INTO `audio_record` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `end_time`, `pcm_file_path`, `start_time`, `text_file_path`, `reservation_record_id`, `audio_file_path`, `rt_text_file_path`) VALUES ('42a08d6d-c14a-494b-b8c5-aec55671691a',NULL,'2021-01-25 20:20:52',1,NULL,'2021-01-27 17:07:56',3,'2021-01-25 20:21:33','/group1/M00/00/24/rBUAAWAOt8-ADljgABOAALM_hGg946.pcm','2021-01-25 20:20:52',NULL,'f5a749d7-09f1-4d2a-ac3c-c53af42150aa','/group1/M00/00/24/rBUAAWAOt8-AYpwcABOALDE_r_I434.mp3',NULL);
INSERT INTO `audio_record` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `end_time`, `pcm_file_path`, `start_time`, `text_file_path`, `reservation_record_id`, `audio_file_path`, `rt_text_file_path`) VALUES ('54ab8d1c-8014-4763-8a28-3744c071415c',NULL,'2021-01-25 20:29:57',1,NULL,'2021-01-27 17:07:44',3,'2021-01-25 20:34:45','/group1/M00/00/24/rBUAAWAOuvSALV6SAI0AAHfQy4g256.pcm','2021-01-25 20:29:57','/group1/M00/00/24/rBUAAWAOu0GAV7-RAAAKbc-Kqzo289.txt','f5a749d7-09f1-4d2a-ac3c-c53af42150aa','/group1/M00/00/24/rBUAAWAOuvSAMN5DAI0ALCEXVxA904.mp3',NULL);
INSERT INTO `audio_record` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `end_time`, `pcm_file_path`, `start_time`, `text_file_path`, `reservation_record_id`, `audio_file_path`, `rt_text_file_path`) VALUES ('5c3a7650-006b-4087-8ae1-dd4bd7382ef9',NULL,'2021-02-05 18:00:45',0,NULL,'2021-02-05 18:05:46',2,'2021-02-05 18:04:50','/group1/M00/00/29/rBUAAWAdGEiACE2XAHYwAMtVQ9U820.pcm','2021-02-05 18:00:45','/group1/M00/00/29/rBUAAWAdGHiAempDAAAOk7m0NqA042.txt','a00f3e2a-ac7b-44f1-a534-3abb37de9537','/group1/M00/00/29/rBUAAWAdGEmAKkxAAHYwLNOevLM378.mp3','/group1/M00/00/29/rBUAAWAdGHmAYwmZAAAZ5YOsbtc310.txt');
INSERT INTO `audio_record` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `end_time`, `pcm_file_path`, `start_time`, `text_file_path`, `reservation_record_id`, `audio_file_path`, `rt_text_file_path`) VALUES ('76e64082-5625-40d4-86a0-0d6c96410489',NULL,'2021-01-25 20:09:41',1,NULL,'2021-01-27 17:07:59',3,'2021-01-25 20:09:49','/group1/M00/00/24/rBUAAWAOtQ6AeMdAAASwAG0BTL8823.pcm','2021-01-25 20:09:41',NULL,'584100d9-e147-4e50-9d0b-b21370b89650','/group1/M00/00/24/rBUAAWAOtQ-AFaBVAASwLByftlo422.mp3',NULL);
INSERT INTO `audio_record` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `end_time`, `pcm_file_path`, `start_time`, `text_file_path`, `reservation_record_id`, `audio_file_path`, `rt_text_file_path`) VALUES ('7dc3f3bd-dab0-41e9-8066-f2f79f1525fb',NULL,'2021-01-25 20:20:44',1,NULL,'2021-01-27 17:07:58',3,'2021-01-25 20:22:27','/group1/M00/00/24/rBUAAWAOuAeAZacAADLQAH_R6p8368.pcm','2021-01-25 20:20:44',NULL,'8fd808c0-9042-495b-bd15-910adea1acdc','/group1/M00/00/24/rBUAAWAOuAeAONIuADLQLCcbtAk692.mp3',NULL);
INSERT INTO `audio_record` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `end_time`, `pcm_file_path`, `start_time`, `text_file_path`, `reservation_record_id`, `audio_file_path`, `rt_text_file_path`) VALUES ('9e614b10-8ba1-4a7e-a9fc-3ec21e88d067',NULL,'2021-01-25 20:28:23',1,NULL,'2021-01-27 17:07:54',3,'2021-01-25 20:28:34','/group1/M00/00/24/rBUAAWAOuXOADrtlAAXwAM2fp70872.pcm','2021-01-25 20:28:23','/group1/M00/00/24/rBUAAWAOuXuAGE5IAAAAmZSwI2A415.txt','f5a749d7-09f1-4d2a-ac3c-c53af42150aa','/group1/M00/00/24/rBUAAWAOuXOAU6XFAAXwLBS1QEY494.mp3',NULL);
INSERT INTO `audio_record` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `end_time`, `pcm_file_path`, `start_time`, `text_file_path`, `reservation_record_id`, `audio_file_path`, `rt_text_file_path`) VALUES ('a461e298-3a4a-4726-8f40-573a372b6c6e',NULL,'2021-01-25 20:29:05',1,NULL,'2021-01-27 17:07:51',3,'2021-01-25 20:29:38','/group1/M00/00/24/rBUAAWAOubOAG7raABCQANS8-VM943.pcm','2021-01-25 20:29:05','/group1/M00/00/24/rBUAAWAOubuAMbjxAAABFBtjiQE906.txt','f5a749d7-09f1-4d2a-ac3c-c53af42150aa','/group1/M00/00/24/rBUAAWAOubSACz6bABCQLHmow-A715.mp3',NULL);
INSERT INTO `audio_record` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `end_time`, `pcm_file_path`, `start_time`, `text_file_path`, `reservation_record_id`, `audio_file_path`, `rt_text_file_path`) VALUES ('aacc4d39-4592-488d-bdac-93d2ee9ec335',NULL,'2021-01-26 08:35:24',1,NULL,'2021-01-27 17:07:42',3,'2021-01-26 08:47:27','/group1/M00/00/24/rBUAAWAPZqyARsANAV8gAPAnJ-c245.pcm','2021-01-26 08:35:24','/group1/M00/00/24/rBUAAWAPZv-AcnCpAAAgOF2783w994.txt','8fd808c0-9042-495b-bd15-910adea1acdc','/group1/M00/00/24/rBUAAWAPZq2AWzULAV8gLKzKkgw648.mp3',NULL);
INSERT INTO `audio_record` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `end_time`, `pcm_file_path`, `start_time`, `text_file_path`, `reservation_record_id`, `audio_file_path`, `rt_text_file_path`) VALUES ('cae82125-7521-45c2-b612-7d231832432a',NULL,'2021-02-05 17:53:54',0,NULL,'2021-02-05 17:59:42',2,'2021-02-05 17:58:31','/group1/M00/00/29/rBUAAWAdFt-AFrxGAIcwAAEN9Y8174.pcm','2021-02-05 17:53:54','/group1/M00/00/29/rBUAAWAdFw2ADxvyAAANA1DwbSo214.txt','b7b39250-60bd-4671-81a4-9c4d82a7605b','/group1/M00/00/29/rBUAAWAdFt-AVNf5AIcwLO_FLgs940.mp3','/group1/M00/00/29/rBUAAWAdFw2AVKZGAAAaJUTKWIc034.txt');
INSERT INTO `audio_record` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `end_time`, `pcm_file_path`, `start_time`, `text_file_path`, `reservation_record_id`, `audio_file_path`, `rt_text_file_path`) VALUES ('cd87bccc-fe4d-4517-b547-865e15c982e4',NULL,'2021-02-05 17:23:32',0,NULL,'2021-02-05 17:24:19',2,'2021-02-05 17:24:07','/group1/M00/00/29/rBUAAWAdDriANl2CABGQACjJMcM230.pcm','2021-02-05 17:23:32','/group1/M00/00/29/rBUAAWAdDsKAEX2KAAAA1MLc70Y367.txt','a00f3e2a-ac7b-44f1-a534-3abb37de9537','/group1/M00/00/29/rBUAAWAdDrmAKhTTABGQLMJKxqQ670.mp3','/group1/M00/00/29/rBUAAWAdDsOAYTRdAAACL4vwx8k057.txt');
INSERT INTO `audio_record` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `end_time`, `pcm_file_path`, `start_time`, `text_file_path`, `reservation_record_id`, `audio_file_path`, `rt_text_file_path`) VALUES ('d1625abc-351a-4320-b412-e7186bf8948a',NULL,'2021-02-05 17:37:40',0,NULL,'2021-02-05 17:43:34',2,'2021-02-05 17:39:01','/group1/M00/00/29/rBUAAWAdEyaAf2JaACaQAP4cJ10489.pcm','2021-02-05 17:37:40','/group1/M00/00/29/rBUAAWAdE0WASmLYAAAD5A3N3q0150.txt','b7b39250-60bd-4671-81a4-9c4d82a7605b','/group1/M00/00/29/rBUAAWAdEyeAIiPmACaQLEI_Fe4494.mp3','/group1/M00/00/29/rBUAAWAdE0WAQ_M9AAAHJHB7vhc361.txt');
INSERT INTO `audio_record` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `end_time`, `pcm_file_path`, `start_time`, `text_file_path`, `reservation_record_id`, `audio_file_path`, `rt_text_file_path`) VALUES ('ef062365-91af-4bd3-828b-dad8dd0b3e82',NULL,'2021-02-05 17:52:02',0,NULL,'2021-02-05 17:52:24',2,'2021-02-05 17:52:10','/group1/M00/00/29/rBUAAWAdFUqADEJDAAQgAKfWmtc918.pcm','2021-02-05 17:52:02','/group1/M00/00/29/rBUAAWAdFVeAIjCCAAAAdYmaFJQ605.txt','b7b39250-60bd-4671-81a4-9c4d82a7605b','/group1/M00/00/29/rBUAAWAdFUuAWZtkAAQgLJntUdo058.mp3','/group1/M00/00/29/rBUAAWAdFVeAFy0AAAAArh_27zo374.txt');
INSERT INTO `audio_record` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `end_time`, `pcm_file_path`, `start_time`, `text_file_path`, `reservation_record_id`, `audio_file_path`, `rt_text_file_path`) VALUES ('ef6d5b9f-18fd-4859-ac45-97c8fde67439',NULL,'2021-02-05 17:43:11',0,NULL,'2021-02-05 17:52:46',2,'2021-02-05 17:49:26','/group1/M00/00/29/rBUAAWAdFT2Ad7UtAK4gALJz_x8486.pcm','2021-02-05 17:43:11','/group1/M00/00/29/rBUAAWAdFW6Adj_vAAAOzC6nKmc965.txt','b7b39250-60bd-4671-81a4-9c4d82a7605b','/group1/M00/00/29/rBUAAWAdFT2AX9AFAK4gLILAAns867.mp3','/group1/M00/00/29/rBUAAWAdFW6AKHLVAAAeLtWqu1I731.txt');
INSERT INTO `audio_record` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `end_time`, `pcm_file_path`, `start_time`, `text_file_path`, `reservation_record_id`, `audio_file_path`, `rt_text_file_path`) VALUES ('ef812230-f116-4a2a-9531-c7bb24efa0c9',NULL,'2021-02-05 17:28:13',0,NULL,'2021-02-05 17:37:44',2,'2021-02-05 17:36:05','/group1/M00/00/29/rBUAAWAdEZyAQ65qALoQAAbcBxo738.pcm','2021-02-05 17:28:13','/group1/M00/00/29/rBUAAWAdEeeANuz7AAAOGODs3GA870.txt','b7b39250-60bd-4671-81a4-9c4d82a7605b','/group1/M00/00/29/rBUAAWAdEZyAW03PALoQLN_LFEs142.mp3','/group1/M00/00/29/rBUAAWAdEeeAbNhhAAAfKYd5JIg227.txt');
INSERT INTO `audio_record` (`id`, `created_by`, `created_time`, `is_del`, `updated_by`, `updated_time`, `version`, `end_time`, `pcm_file_path`, `start_time`, `text_file_path`, `reservation_record_id`, `audio_file_path`, `rt_text_file_path`) VALUES ('fd3fedca-219f-48d8-93fc-6ea51170d558',NULL,'2021-02-05 17:27:42',0,NULL,'2021-02-05 17:28:07',2,'2021-02-05 17:27:56','/group1/M00/00/29/rBUAAWAdD52AVgylAATgAOvGg9Y330.pcm','2021-02-05 17:27:42','/group1/M00/00/29/rBUAAWAdD6WAUud3AAAAHRliIzQ961.txt','b7b39250-60bd-4671-81a4-9c4d82a7605b','/group1/M00/00/29/rBUAAWAdD56ABZ_cAATgLNWVJQY400.mp3','/group1/M00/00/29/rBUAAWAdD6aAS8Q7AAAAVi6U1eg626.txt');
/*!40000 ALTER TABLE `audio_record` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-26  9:34:26
