package cn.com.cowain.ibms.component;

import cn.com.cowain.ibms.bo.CentralizedUserBO;
import cn.hutool.core.collection.CollUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
@Log4j2
public class DynamicsDataComponent {
    public static final Map<String, CentralizedUserBO> map = new ConcurrentHashMap<>();

    public Map<String, CentralizedUserBO> getDynamicsDataMap() {
        return map;
    }
    public  void setDynamicsDataMap(String key, CentralizedUserBO valve) {
        map.put(key, valve);
    }
    public  void removeDynamicsDataMap(String key) {
        if(CollUtil.isNotEmpty(map)&& StringUtils.isNotEmpty(key)){
            map.remove(key);
        }
    }
}
