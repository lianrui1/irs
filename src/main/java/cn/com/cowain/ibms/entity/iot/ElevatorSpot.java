package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author Yang.Lee
 * @date 2022/3/17 11:01
 */
@Data
@Entity
@Table(name = "iot_elevator_spot")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_elevator_spot", comment = "梯控点")
public class ElevatorSpot extends BaseEntity {

    @Column(name = "name", columnDefinition = "varchar(30) COMMENT '名称'")
    private String name;

    @Column(name = "address", columnDefinition = "varchar(100) COMMENT '具体位置'")
    private String address;

    @ManyToOne
    @JoinColumn(name = "space_id", nullable = false, columnDefinition = "varchar(36) COMMENT '所属空间ID FK'")
    private Space space;


}
