package cn.com.cowain.ibms.enumeration.iot;

/**
 * 门禁点类型
 *
 * @author Yang.Lee
 * @date 2021/11/7 15:44
 */
public enum AccessControlSpotType {

    /**
     * 门磁
     */
    DOOR_MAGNETIC("门磁"),

    /**
     * 面板机
     **/
    FACE_MACHINE("面板机"),

    /**
     * 其他
     **/
    OTHERS("其他");

    AccessControlSpotType (String name){
        this.name = name;
    }
    String name;

    public String getName(){
        return this.name;
    }
}
