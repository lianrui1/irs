package cn.com.cowain.ibms.rest.resp.bim;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/10/18 16:58
 */
@Data
@ApiModel("bim项目数据及能耗")
public class BimAllResp {

    @ApiModelProperty(value = "项目id", required = true)
    private String projectId;

    @ApiModelProperty(value = "项目名称", required = true)
    private String projectName;

    @ApiModelProperty(value = "在线设备数", required = true)
    private Integer online;

    @ApiModelProperty(value = "离线设备数", required = true)
    private Integer offline;

    @ApiModelProperty(value = "电量", required = true)
    private Integer electricity;
}
