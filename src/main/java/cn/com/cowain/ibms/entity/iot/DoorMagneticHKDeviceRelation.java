package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Yang.Lee
 * @date 2021/10/19 13:59
 */
@Data
@Entity
@Table(name = "iot_door_magnetic_hk_device_relation")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_door_magnetic_hk_device_relation", comment = "门磁-海康面板机关系表")
public class DoorMagneticHKDeviceRelation extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "door_magnetic_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 门磁id'")
    private DoorMagnetic doorMagnetic;

    @ManyToOne
    @JoinColumn(name = "hk_device_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 海康面板机id'")
    private HkDevice hkDevice;
}
