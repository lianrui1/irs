package cn.com.cowain.ibms.rest.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/11/20
 */
@Data
@ApiModel("会议冲突检测结果")
public class ReservationConflictResp implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "系统用户ID", required = true, position = 0)
    private String sysId;

    /**
     * 从时间
     */
    @ApiModelProperty(value = "检测开始时间", required = true, position = 1)
    private LocalDateTime from;

    /**
     * 到时间
     */
    @ApiModelProperty(value = "检测结束时间", required = true, position = 2)
    private LocalDateTime to;

    /**
     * 0:无冲突
     * 1:有冲突
     */
    @ApiModelProperty(value = "冲突状态 0:无冲突;1:有冲突", required = true, position = 3)
    private int status;

    /**
     * 会议预约信息
     */
    @ApiModelProperty(value = "会议预约信息", position = 4)
    private ReservationRecordResp reservationRecordResp;

    /**
     * 系统用户工号
     **/
    @ApiModelProperty(value = "系统用户工号", required = true, position = 4)
    private String empNo;
}
