package cn.com.cowain.ibms.service.space.impl;

import cn.com.cowain.ibms.dao.device.HardwareDao;
import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.dao.space.IotAttributesDao;
import cn.com.cowain.ibms.dao.space.IotAttributesDictDao;
import cn.com.cowain.ibms.dao.space.IotProductDao;
import cn.com.cowain.ibms.entity.iot.Hardware;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.space.IotAttributes;
import cn.com.cowain.ibms.entity.space.IotAttributesDict;
import cn.com.cowain.ibms.entity.space.IotProduct;
import cn.com.cowain.ibms.enumeration.IotNodeType;
import cn.com.cowain.ibms.feign.iotc.IOTCApi;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.space.ProductReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.iot.IotProductResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.space.IotProductService;
import cn.com.cowain.ibms.utils.DateUtils;
import cn.com.cowain.sfp.comm.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * IOT产品 业务实现
 *
 * @author yuchunlei
 * @version 1.0.0
 */

@Slf4j
@Service
public class IotProductServiceImpl implements IotProductService {

    @Autowired
    private IotProductDao iotProductDao;
    @Autowired
    private IotAttributesDictDao iotAttributesDictDao;
    @Autowired
    private IotAttributesDao iotAttributesDao;

    @Resource
    private DeviceDao deviceDao;

    @Resource
    private HardwareDao hardwareDao;

    @Resource
    private IOTCApi iotcApi;

    @Override
    @Transactional
    public ServiceResult save(ProductReq productReq) {

        // 查询产品名称，不允许重复
        Optional<IotProduct> iotProductOptional = iotProductDao.findByName(productReq.getName());
        if (iotProductOptional.isPresent()) {
            return ServiceResult.error("产品名称已存在");
        }

        IotProduct product = new IotProduct();
        //产品名称
        product.setName(productReq.getName());
        //设备类型
        product.setIotNodeType(productReq.getIotNodeType());
        //创建时间
        product.setCreatedTime(LocalDateTime.now());
        // 产品备注名称
        product.setRemark(productReq.getRemark());

        //产品编号
        Integer maxProductNo = iotProductDao.findMaxProductNo();
        if (maxProductNo != null) {
            maxProductNo = maxProductNo + 1;
        } else {
            maxProductNo = 1;
        }

        product.setProductNo(maxProductNo);

        product.setHwName(productReq.getNameEn());

        IotProduct iotProduct = iotProductDao.save(product);
        //关联数据点
        List<String> iotAttributes = productReq.getIotAttributes();
        if (!CollectionUtils.isEmpty(iotAttributes)) {
            for (String id : iotAttributes) {
                Optional<IotAttributesDict> iotAttributesDict = iotAttributesDictDao.findById(id);
                if (iotAttributesDict.isPresent()) {
                    IotAttributes attributes = new IotAttributes();
                    attributes.setDictId(iotAttributesDict.get().getId());
                    attributes.setName(iotAttributesDict.get().getName());
                    attributes.setIotProduct(iotProduct);
                    iotAttributesDao.save(attributes);
                }
            }
        }

        IdResp idResp = new IdResp();
        idResp.setId(iotProduct.getId());

        return ServiceResult.ok(idResp);
    }


    @Override
    @Transactional
    public List<IotAttributesDict> getIotAttributesDictList() {
        // 根据seq字段排序
        Sort sort = Sort.by(Sort.Direction.ASC, "seq");

        return iotAttributesDictDao.findAll(sort);
    }

    @Override
    @Transactional
    public PageBean<IotProductResp> search(String searchName, IotNodeType nodeType, int page, int size) {
        Sort sort = Sort.by(Sort.Direction.DESC, "createdTime");
        Pageable pageable = PageRequest.of(page, size, sort);
        //查询条件
        Specification<IotProduct> specification = (root, criteriaQuery, criteriaBuilder) -> {
            //集合 用于封装查询条件
            List<Predicate> list = new ArrayList<>();

            list.add(criteriaBuilder.and(criteriaBuilder.greaterThan(root.get("productNo"), 0)));

            //设备类型
            if (Objects.nonNull(nodeType)) {
                list.add(criteriaBuilder.equal(root.get("iotNodeType"), nodeType));
            }
            //产品名称
            if (!StringUtils.isEmpty(searchName)) {

                String likeKey = "%" + searchName + "%";

                list.add(
                        criteriaBuilder.or(
                                criteriaBuilder.like(root.get("name").as(String.class), likeKey),
                                criteriaBuilder.like(root.get("hwName").as(String.class), likeKey)
                        )
                );
            }
            return criteriaBuilder.and(list.toArray(new Predicate[list.size()]));
        };
        Page<IotProduct> iotProductPage = iotProductDao.findAll(specification, pageable);

        PageBean<IotProductResp> pageBean = new PageBean<>();
        //当前页
        pageBean.setPage(pageable.getPageNumber());
        //每页记录数
        pageBean.setSize(pageable.getPageSize());
        //总记录数
        pageBean.setTotalElements(iotProductPage.getTotalElements());
        //总页数
        pageBean.setTotalPages(iotProductPage.getTotalPages());

        List<IotProductResp> list = new ArrayList<>();
        //循环 封装为productResp对象
        if (!CollectionUtils.isEmpty(iotProductPage.getContent())) {
            iotProductPage.getContent().forEach(iotProduct -> {
                IotProductResp productResp = new IotProductResp();
                productResp.setId(iotProduct.getId());
                productResp.setName(iotProduct.getName());
                productResp.setIotNodeType(iotProduct.getIotNodeType());
                if (iotProduct.getIotNodeType() != null) {
                    productResp.setIotNodeTypeName(iotProduct.getIotNodeType().getName());
                }
                productResp.setRemark(iotProduct.getRemark());
                //根据产品id查询该产品对应的数据点
                List<IotAttributes> iotAttributes = iotAttributesDao.findByIotProductId(iotProduct.getId());
                if (!CollectionUtils.isEmpty(iotAttributes)) {
                    List<String> nameList = new ArrayList<>();
                    iotAttributes.forEach(it -> nameList.add(it.getName()));
                    productResp.setIotAttributes(nameList);
                }
                productResp.setProductNo(String.format("%03d", iotProduct.getProductNo()));
                //创建时间
                productResp.setCreateTime(DateUtils.formatLocalDateTime(iotProduct.getCreatedTime(), DateUtils.PATTERN_DATETIME));

                productResp.setName(iotProduct.getName());
                productResp.setNameEn(iotProduct.getHwName());

                list.add(productResp);
            });
        }
        pageBean.setList(list);

        log.info(String.valueOf(pageBean));
        return pageBean;
    }

    @Override
    @Transactional
    public IotProductResp getIotProductInfo(String id) {
        Optional<IotProduct> iotProduct = iotProductDao.findById(id);
        IotProductResp iotProductResp = new IotProductResp();
        if (!iotProduct.isPresent()) {
            return iotProductResp;
        } else {
            IotProduct product = iotProduct.get();
            iotProductResp.setName(product.getName());
            iotProductResp.setNameEn(product.getHwName());
            iotProductResp.setIotNodeType(product.getIotNodeType());
            iotProductResp.setProductNo(String.valueOf(product.getProductNo()));
            List<IotAttributes> iotAttributes = iotAttributesDao.findByIotProductId(id);
            if (!CollectionUtils.isEmpty(iotAttributes)) {
                List<String> nameList = new ArrayList<>();
                iotAttributes.forEach(it -> nameList.add(it.getDictId()));
                iotProductResp.setIotAttributes(nameList);
            }
        }
        return iotProductResp;
    }

    @Override
    @Transactional
    public ServiceResult update(String id, ProductReq productReq) {

        //根据产品id查询iot产品
        Optional<IotProduct> iotProductOptional = iotProductDao.findById(id);
        //判断产品是否存在
        boolean present = iotProductOptional.isPresent();
        if (!present) {
            return ServiceResult.error("编辑失败，未找到产品数据");
        }
        IotProduct product = iotProductOptional.get();

        // 当产品的名称发生变化时，判断新的名称是否已存在
        if (!productReq.getName().equals(product.getName())) {
            Optional<IotProduct> iotProductNameOptional = iotProductDao.findByName(productReq.getName());
            if (iotProductNameOptional.isPresent()) {

                return ServiceResult.error("产品名称已存在");
            }
        }

        log.info(String.valueOf(iotProductOptional));

        product.setName(productReq.getName());
        product.setIotNodeType(productReq.getIotNodeType());
        // 产品备注名称
        product.setRemark(productReq.getRemark());

        iotProductDao.save(product);

        List<String> attributes = productReq.getIotAttributes();

        //从数据库中查询
        List<IotAttributes> attributesList = iotAttributesDao.findByIotProductId(id);
        List<String> iotList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(attributesList)) {
            for (IotAttributes iotAttributes : attributesList) {
                iotList.add(iotAttributes.getDictId());
            }

            iotList.removeAll(attributes);
            if (!CollectionUtils.isEmpty(iotList)) {
                for (String dictId : iotList) {
                    Optional<IotAttributes> iotAttribute = iotAttributesDao.findByDictIdAndIotProduct(dictId, id);
                    if (iotAttribute.isPresent()) {
                        IotAttributes iot = iotAttribute.get();
                        iot.setIsDelete(1);
                        iot.setUpdatedTime(LocalDateTime.now());
                        iotAttributesDao.save(iot);
                    }
                }
            }
        }

        //该产品已存在传入的数据点
        if (!CollectionUtils.isEmpty(attributes)) {
            for (String dictId : attributes) {
                Optional<IotAttributes> iotAttribute = iotAttributesDao.findByDictIdAndIotProduct(dictId, id);
                if (!iotAttribute.isPresent()) {
                    IotAttributes iot = new IotAttributes();
                    Optional<IotAttributesDict> attributesDict = iotAttributesDictDao.findById(dictId);
                    if (attributesDict.isPresent()) {
                        iot.setName(attributesDict.get().getName());
                    }

                    iot.setUpdatedTime(LocalDateTime.now());
                    iot.setDictId(dictId);
                    iot.setIotProduct(product);
                    iotAttributesDao.save(iot);
                } else {
                    IotAttributes iotAttributes = iotAttribute.get();
                    if (iotAttributes.getIsDelete() == 1) {
                        iotAttributes.setIsDelete(0);
                        iotAttributes.setUpdatedTime(LocalDateTime.now());
                        iotAttributesDao.save(iotAttributes);
                    }
                }

            }
        } else {
            //查询该产品下是否有数据点
            List<IotAttributes> iotAttributesList = iotAttributesDao.findByIotProductId(id);
            if (!CollectionUtils.isEmpty(iotAttributesList)) {
                for (IotAttributes iotAttributes : iotAttributesList) {
                    iotAttributes.setIsDelete(1);
                    iotAttributes.setUpdatedTime(LocalDateTime.now());
                    //更新该产品的数据点
                    iotAttributesDao.save(iotAttributes);
                }
            }
        }
        return ServiceResult.ok();
    }

    @Override
    @Transactional
    public ServiceResult deleteById(String id) {
        Optional<IotProduct> iotProductOptional = iotProductDao.findById(id);

        if (!iotProductOptional.isPresent()) {
            return ServiceResult.error("删除失败，未找到该产品数据");
        }

        // 判断该产品下是否有设备
        List<IotDevice> deviceList = deviceDao.findByIotProductId(id);
        if (!deviceList.isEmpty()) {
            return ServiceResult.error(String.format("删除失败，在设备管理中，该产品下含有%s个设备", deviceList.size()));
        }

        // 判断设备清单中的数据是否关联该产品
        List<Hardware> hardwareList = hardwareDao.findByIotProductId(id);
        if (!hardwareList.isEmpty()) {
            return ServiceResult.error(String.format("删除失败，在硬件清单中，该产品下含有%s个设备", hardwareList.size()));
        }

        IotProduct product = iotProductOptional.get();
        IotProduct iotProduct = new IotProduct();
        BeanUtils.copyProperties(product, iotProduct);
        iotProduct.setIsDelete(1);
        iotProduct.setUpdatedTime(LocalDateTime.now());
        iotProductDao.save(iotProduct);

        //查询该产品相关的数据点
        List<IotAttributes> iotAttributes = iotAttributesDao.findByIotProductId(id);
        if (!CollectionUtils.isEmpty(iotAttributes)) {
            iotAttributes.forEach(iotAttribute -> iotAttribute.setIsDelete(1));
            iotAttributesDao.saveAll(iotAttributes);
        }

        return ServiceResult.ok();
    }

    /**
     * 从iotc获取产品列表
     *
     * @return 产品列表
     * @author Yang.Lee
     * @date 2021/10/25 11:26
     **/
    @Override
    public List<IotProduct> getListFromIOTC() {

        JsonResult<PageBean<IotProduct>> productPage = iotcApi.getProductList(0, 9999);
        if (productPage.getStatus() == 0) {
            log.error("同步产品数据失败，IOTC返回内容： " + productPage);
            return new ArrayList<>();
        }

        List<IotProduct> list = productPage.getData().getList();
        list.forEach(obj -> {
            obj.setIotcId(obj.getId());
            obj.setId(null);
        });

        return list;
    }

    /**
     * 保存产品数据
     *
     * @param productList 产品列表
     * @author Yang.Lee
     * @date 2021/10/25 13:07
     **/
    @Override
    @Transactional
    public void saveProduct(List<IotProduct> productList) {
        if (CollectionUtils.isEmpty(productList)) {
            return ;
        }

        productList.forEach(obj -> {
            Optional<IotProduct> applicationOptional = iotProductDao.findByhwProductId(obj.getHwProductId());

            if(applicationOptional.isPresent()){
                BeanUtils.copyProperties(applicationOptional.get(), obj);
            }
        });

        iotProductDao.saveAll(productList);
    }
}
