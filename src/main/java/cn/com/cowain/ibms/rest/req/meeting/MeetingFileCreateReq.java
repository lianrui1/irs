package cn.com.cowain.ibms.rest.req.meeting;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * @author wei.cheng
 * @date 2022/02/22 19:01
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("创建会议室文档请求")
@AllArgsConstructor
@NoArgsConstructor
public class MeetingFileCreateReq extends MeetingFileReq {

    @ApiModelProperty(value = "会议室预约ID", required = true, position = 1)
    @NotBlank
    private String reservationRecordId;
}
