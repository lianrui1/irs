package cn.com.cowain.ibms.service.office;

import cn.com.cowain.ibms.enumeration.iot.UsableStatus;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.PageReq;
import cn.com.cowain.ibms.rest.req.office.CreatDefaultWorkingModeToMyWorkingModeReq;
import cn.com.cowain.ibms.rest.req.working_mode.ManagementSaveReq;
import cn.com.cowain.ibms.rest.req.working_mode.WorkingModePageReq;
import cn.com.cowain.ibms.rest.req.working_mode.WorkingModeSaveReq;
import cn.com.cowain.ibms.rest.resp.working_mode.DefaultWorkingModePageResp;
import cn.com.cowain.ibms.rest.resp.working_mode.ManagementPageResp;
import cn.com.cowain.ibms.rest.resp.working_mode.WorkingModePageResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;

import java.util.List;

public interface IntelligentOfficeAppletService {


    // 新建办公室工作场景
    ServiceResult save(WorkingModeSaveReq req);

    // 办公室工作模式列表(分页)
    PageBean<WorkingModePageResp> getPage(WorkingModePageReq req);

    // 查询工作模式详情
    ServiceResult detail(String id);

    // 删除工作模式场景
    ServiceResult delete(String id);

    // 更新智能会议室状态
    ServiceResult updateStatus(String id, UsableStatus status);

    // 修改工作模式场景
    ServiceResult update(String id, WorkingModeSaveReq req);

    // 查询默认工作模式场景列表(分页)-小程序
    PageBean<DefaultWorkingModePageResp> getDefaultWorkingModePage(String officeId, PageReq req);

    // 新增权限人员-小程序
    ServiceResult managementCreate(ManagementSaveReq req);

    // 查询办公室下拥有权限人员
    PageBean<ManagementPageResp> managementPage(String id, int page, int size);

    // 删除权限人员-小程序
    ServiceResult managementDelete(String id, List<String> empNos);

    // 控制工作模式-小程序
    ServiceResult controlWorkingMode(String id);

    // 添加默认工作模式场景到我的-小程序
    ServiceResult creatDefaultWorkingModeToMyWorkingMode(CreatDefaultWorkingModeToMyWorkingModeReq req);
}
