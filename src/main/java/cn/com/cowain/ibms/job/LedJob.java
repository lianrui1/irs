package cn.com.cowain.ibms.job;

import cn.com.cowain.ibms.rest.req.iot.AttendanceInOutResp;
import cn.com.cowain.ibms.rest.req.iot.AttendanceStatisticsResp;
import cn.com.cowain.ibms.service.led.LedService;
import cn.com.cowain.ibms.utils.RedisUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 南通大屏数据查询job
 *
 * @author Yang.Lee
 * @date 2022/5/13 9:52
 */
@Slf4j
@Component
public class LedJob {

    @Resource
    private LedService ledService;

    // 南通项目ID
    @Value("${nt-projectId}")
    private String ntProjectId;

    @Resource
    private RedisUtil redisUtil;

    @Scheduled(cron = "0 0/15 * * * ?")
    public void getAttendanceData() {

        try{
            log.debug("定时任务开始，准备获取考勤设备统计数据");

            AttendanceStatisticsResp resp = ledService.getAttendanceStatistic(ntProjectId);
            String respString = JSON.toJSONString(resp);
            log.debug("考勤设备统计数据统计完毕，项目：{}，结果：{}", ntProjectId, respString);

            String redisKey = "attendanceStatistics-" + ntProjectId;
            String realKey = redisUtil.createRealKey(redisKey);

            redisUtil.set(realKey, respString, RedisUtil.HALF_AN_HOUR_SECOND);
        } catch (Exception e){
            log.error("定时任务获取考勤设备统计数据异常");
            log.error(e.getMessage(), e);
        }

        try{
            log.debug("定时任务开始，准备获取12小时内进出统计数据");
            AttendanceInOutResp inOutResp = ledService.getAttendanceInOut(ntProjectId);
            String inOutRespString = JSON.toJSONString(inOutResp);
            log.debug("12小时内进出统计数据完毕，项目：{}，结果：{}", ntProjectId, inOutRespString);

            String inOutRedisKey = "attendanceInOut-" + ntProjectId;
            String inOutRealKey = redisUtil.createRealKey(inOutRedisKey);

            redisUtil.set(inOutRealKey, inOutRespString, RedisUtil.HALF_AN_HOUR_SECOND);

        } catch (Exception e){
            log.error("获取12小时内出入数据异常");
            log.error(e.getMessage(), e);
        }


    }
}
