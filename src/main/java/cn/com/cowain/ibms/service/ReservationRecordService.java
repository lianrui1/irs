package cn.com.cowain.ibms.service;

import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.enumeration.ReservationRecordStatus;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.bean.ReservationRecordBean;
import cn.com.cowain.ibms.rest.req.*;
import cn.com.cowain.ibms.rest.req.meeting.*;
import cn.com.cowain.ibms.rest.req.working_mode.WelcomeWordReq;
import cn.com.cowain.ibms.rest.resp.*;
import cn.com.cowain.ibms.rest.resp.audio.AudioDetailResp;
import cn.com.cowain.ibms.rest.resp.meeting.OngoingMeetingResp;
import cn.com.cowain.ibms.rest.resp.meeting.ReservationPageResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * 保存会议记录
 *
 * @author 张宇鑫
 * @version 1.0
 * @date 2020/8/4 17:50
 */
public interface ReservationRecordService {

    /**
     * 创建会议记录
     *
     * @param req
     * @param token
     * @return
     * @author Hu Jinling
     */
    ReservationRecord save(ReservationAddReq req, String token);

    /**
     * 更新会议记录
     *
     * @param reservationId
     * @param req
     * @return
     * @author 张宇鑫
     */
    ServiceResult update(String reservationId, ReservationUpdateReq req, String token);

    /**
     * 获取参会人员的会议记录列表
     *
     * @param reservationGetReq
     * @return
     * @author 张宇鑫
     */
    PageBean<ReservationRecordBean> getReservationRecordItemPage(ReservationGetReq reservationGetReq);

    /**
     * 根据用户工号，项目id查询会议室和对应预约记录
     *
     * @param hrId      用户工号（选填，如果hrId为null或“”，则不根据工号筛选）
     * @param projectId 项目ID（选填，如果projectId为null或“”，则不根据项目筛选）
     * @param date      查询日期
     * @param pageBean  分页对象
     * @return 会议室预约记录
     * @author 张宇鑫
     */
    PageBean<RoomWithReservationResp> findByHrId(String hrId, String projectId, LocalDate date, PageBean<RoomWithReservationResp> pageBean);

    /**
     * 指定日期,指定会议室,查询预约记录
     *
     * @param empEo  员工工号（可选）
     * @param roomId 会议室ID
     * @param date   时间
     * @return
     * @author 张宇鑫
     */
    RoomWithReservationResp findBySysUserAndRoomId(String empEo, String roomId, LocalDate date);

    /**
     * 指定查询人员id 会议id 查询预约记录详情
     *
     * @param ownerEmpNo 员工工号
     * @param recordId   会议记录id
     * @return ReservationRecordDetailsResp
     * @author 张宇鑫
     */
    ServiceResult getReservationRecordDetails(String ownerEmpNo, String recordId, String token);

    /**
     * 指定会议id 取消会议
     *
     * @param id
     * @return Boolean
     * @author 张宇鑫
     */
    ServiceResult cancel(String id, String token);

    /**
     * 指定会议id 参会人员id 拒绝、接受状态
     *
     * @param id
     * @param inviteeStatusReq
     * @return null
     * @author 张宇鑫
     */
    void updateInviteeStatus(String id, InviteeStatusReq inviteeStatusReq);

    /**
     * 查询会议冲突
     *
     * @param reservationConflictReq
     * @return
     */
    List<ReservationConflictResp> conflictDetect(ReservationConflictReq reservationConflictReq);

    /**
     * 根据用户 工号 查询最近联系人列表
     *
     * @param empNo
     * @return
     */
    List<String> findRecentListByUserEmpNo(String empNo);

    /**
     * 检查会议室是否已被预订
     *
     * @param req
     * @return
     */
    ServiceResult checkReservation(ReservationAddReq req);

    /**
     * 删除会议预约
     *
     * @param id    会议id
     * @param empNo 删除人工号
     * @return
     */
    ServiceResult deleteById(String id, @NotNull String empNo);

    /**
     * 根据会议室ID，查询下一个可用时间段
     *
     * @param roomId 会议室ID
     * @return
     */
    AppointmentsTimeCanBeMdeResp nextValidTime(String roomId, String date);


    /**
     * 判断会议室是否有门磁设备
     *
     * @param roomId 会议室ID
     * @return
     * @author Yang.Lee
     */
    ServiceResult ifRoomHasDoorMagnetic(String roomId);

    /**
     * 指定 会议id 查询预约记录详情
     *
     * @param recordId
     * @return ReservationRecordDetailsResp
     */
    ReservationRecordDetailsResp getAdministrativeReservationRecordDetails(String recordId);


    /**
     * 根据工号获取部门名称
     *
     * @param jobNames 工号
     * @return 部门map对象, key为工号，val为部门名称
     */
    Map<String, String> getUserDepartmentName(String... jobNames);


    /**
     * 获取用户会议室使用时长
     */
    void findRoomUsedTime();

    /**
     * 获取会议状态
     *
     * @param reservationRecord 会议对象
     * @return 会议状态
     * @author Yang.Lee
     * @date 2021/6/15 15:21
     **/
    ReservationRecordStatus getReservationStatus(ReservationRecord reservationRecord);


    /**
     * 查询用户正在进行中的会议列表
     *
     * @param empNo 员工工号
     * @return 会议列表
     * @author Yang.Lee
     * @date 2021/6/18 10:59
     **/
    List<OngoingMeetingResp> getOngoingMeetingList(String empNo);

    /**
     * 获取会议预约记录（分页）
     *
     * @param req 请求参数
     * @return 分页查询结果
     * @author Yang.Lee
     * @date 2021/7/15 9:46
     **/
    PageBean<ReservationPageResp> getReservationPage(ReservationPageReq req);

    /**
     * 获取会议统计数据
     *
     * @param projectId 项目ID
     * @return 统计结果
     * @author Yang.Lee
     * @date 2021/9/27 19:51
     **/
    MeetingStatisticsResp getTodayMeetingNumberStatistics(String projectId);

    /**
     * 获取当日会议时间分布统计数据
     *
     * @param projectId 项目ID
     * @return 统计结果
     * @author Yang.Lee
     * @date 2021/9/27 20:31
     **/
    List<ChartDataResp> getTodayMeetingTimePeriodStatistics(String projectId);

    /**
     * 欢迎语设置
     *
     * @author yanzy
     * @date 2021/12/31
     */

    ServiceResult welcome(WelcomeWordReq req);

    // 根据会议ID获取会议记录音频
    List<AudioDetailResp> getReservationAudioList(String id);

    // 结束会议
    ServiceResult finish(String id, String hrId);

    // 上传会议记录链接
    ServiceResult uploadUrl(ReservationUploadUrlReq reservationUploadUrlReq);

    /**
     * 创建会议（仅创建会议，不包含访客、消息等业务逻辑）
     *
     * @param req 请求参数
     * @return 创建结果，创建成功返回会议id(IdResp对象)
     * @author Yang.Lee
     * @date 2022/4/11 10:21
     **/
    ServiceResult createMeeting(ReservationAddReq req);

    /**
     * 更新会议信息（仅更新会议，不包含访客、消息等业务逻辑））
     *
     * @param meetingId 会议ID
     * @param req       参数
     * @return Gengxin会议
     * @author Yang.Lee
     * @date 2022/4/11 13:47
     **/
    ServiceResult updateMeeting(String meetingId, ReservationUpdateReq req);

    /**
     * 取消会议（仅取消会议，不包含访客、消息等业务逻辑））
     *
     * @param meetingId 会议ID
     * @return Gengxin会议
     * @author Yang.Lee
     * @date 2022/4/11 13:47
     **/
    ServiceResult cancelMeeting(String meetingId);


    /**
     * 获取空闲会议室列表
     *
     * @param req 查询参数
     * @return 空闲会议室列表
     * @author Yang.Lee
     * @date 2022/4/11 16:34
     **/
    List<RoomWithReservationResp> getEmptyRoom(MeetingRoomSearchReq req);

    /**
     * 查询会议室开发最晚时间
     *
     * @author: yanzy
     * @data: 2022/4/19 16:04:28
     */
    ServiceResult getMeetingDeadline();

    /**
     * 推荐会议查询
     *
     * @author: yanzy
     * @data: 2022/4/19 18:04:14
     */
    PageBean<RoomWithReservationResp> getRecommendMeeting(PageBean<RoomWithReservationResp> pageBean, RecommendMeetingReq req);

    /**
     * 查询预定时间最近空闲会议室时间
     *
     * @author: yanzy
     * @data: 2022/4/24 18:04:53
     */
    ServiceResult getRecentTime(RecommendMeetingReq req);

    /**
     * 查询访客系统对应的地址
     *
     * @param projectId
     * @return
     */
    ServiceResult getVisitorAddress(String projectId);

    /**
     * 查询访客类型
     *
     * @author: yanzy
     * @data: 2022/4/27 18:04:57
     */
    List<VisitorTypeResp> getVisitorType();

    /**
     * 邀请参会人
     *
     * @author: yanzy
     * @data: 2022/4/29 10:04:04
     */
    ServiceResult inviteParticipants(AttendeeReq req, String token);
}
