package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.YSVideoCamera;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author feng
 * @title: YSVideoCameraDao
 * @projectName bims
 * @Date 2021/11/24 11:12
 */
public interface YSVideoCameraDao extends JpaRepository<YSVideoCamera, String>, JpaSpecificationExecutor<YSVideoCamera> {
    YSVideoCamera findByDeviceId(String deviceId);
    Page<YSVideoCamera> findByDeviceIdNotIn(List<String> deviceId, Pageable pageable);
}
