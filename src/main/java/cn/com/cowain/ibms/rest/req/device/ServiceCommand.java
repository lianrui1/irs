package cn.com.cowain.ibms.rest.req.device;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/1 14:56
 */
@Data
public class ServiceCommand {

    // 设备命令名称。
    private String commandName;
}
