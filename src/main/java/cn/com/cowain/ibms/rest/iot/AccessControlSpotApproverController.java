package cn.com.cowain.ibms.rest.iot;

import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.iot.AccessControlSpotApproverPageReq;
import cn.com.cowain.ibms.rest.resp.iot.AccessControlSpotApproverPageResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.iot.AccessControlSpotApproverService;
import cn.com.cowain.ibms.utils.IConst;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/3/8 13:00
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/accessControlSpotApprover")
@Api(tags = IConst.ACCESS_CONTROL_SPOT_APPROVER)
public class AccessControlSpotApproverController {

    @Autowired
    private AccessControlSpotApproverService accessControlSpotApproverService;

    @GetMapping
    @ApiOperation(value = "门禁点通行权限分配人列表", tags = {IConst.ACCESS_CONTROL_SPOT_APPROVER, IConst.V26})
    public ResponseEntity<JsonResult<PageBean<AccessControlSpotApproverPageResp>>> accessControlSpotApproverPage(@ApiParam(name = "page") @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                                                                                                                 @ApiParam(name = "size") @RequestParam(value = "size", required = false, defaultValue = "10") int size) {

        ServiceResult result = accessControlSpotApproverService.accessControlSpotApproverPage(page, size);

        return ResponseEntity.ok(JsonResult.ok( "OK", (PageBean<AccessControlSpotApproverPageResp>)result.getObject()));
    }

    @PostMapping
    @ApiOperation(value = "添加门禁点通行权限分配人", tags = {IConst.ACCESS_CONTROL_SPOT_APPROVER, IConst.V26})
    public ResponseEntity<JsonResult<IdResp>> accessControlSpotApproverSave(@RequestBody @Validated AccessControlSpotApproverPageReq req, BindingResult bindingResult) {

        ServiceResult result = accessControlSpotApproverService.accessControlSpotApproverSave(req);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }

        return ResponseEntity.ok(JsonResult.ok("OK", (IdResp)result.getObject()));
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "获取门禁点通行权限分配人详情", tags = {IConst.ACCESS_CONTROL_SPOT_APPROVER, IConst.V26})
    public ResponseEntity<JsonResult<AccessControlSpotApproverPageResp>> accessControlSpotApproverDetails(@PathVariable @ApiParam(name = "id", value = "分配人ID") String id) {

        ServiceResult result = accessControlSpotApproverService.accessControlSpotApproverDetails(id);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }

        return ResponseEntity.ok(JsonResult.ok( "OK", (AccessControlSpotApproverPageResp)result.getObject()));
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "编辑门禁点通行权限分配人", tags = {IConst.ACCESS_CONTROL_SPOT_APPROVER, IConst.V26})
    public ResponseEntity<JsonResult<String>> accessControlSpotApproverUpdate(@PathVariable @ApiParam(name = "id", value = "分配人ID") String id, @RequestBody @Validated AccessControlSpotApproverPageReq req, BindingResult bindingResult) {

        ServiceResult result = accessControlSpotApproverService.accessControlSpotApproverUpdate(id, req);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }

        return ResponseEntity.ok(JsonResult.ok("OK", String.valueOf(result.getObject())));
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除门禁点通行权限分配人", tags = {IConst.ACCESS_CONTROL_SPOT_APPROVER, IConst.V26})
    public ResponseEntity<JsonResult<String>> accessControlSpotApproverDelete(@PathVariable @ApiParam(name = "id", value = "分配人ID") String id) {

        ServiceResult result = accessControlSpotApproverService.accessControlSpotApproverDelete(id);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }
}
