ALTER TABLE `iot_device` ADD COLUMN `last_hw_status_changed_datetime` datetime(0) NULL DEFAULT NULL COMMENT '华为云最近一次状态时间' AFTER `iot_node_type`;

ALTER TABLE `iot_device` ADD COLUMN `hw_gateway_id` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '华为云中的设备关联的网关ID' AFTER `space_id`;

ALTER TABLE `iot_door_magnetic` DROP COLUMN `entity_id`;

ALTER TABLE `ibms_sys_user` add COLUMN `full_department_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户部门全称' AFTER `wx_open_id`;

ALTER TABLE `ibms_distinguish_staff_group_members` ADD COLUMN `sys_user_name` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'FK 员工工号' AFTER `staff_group_id`;

CREATE TABLE `iot_circuit_breaker`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `created_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `version` int(11) NULL DEFAULT NULL COMMENT '版本号',
  `date_time` datetime(0) NULL DEFAULT NULL COMMENT '用电时间',
  `day_electricity` int(11) NULL DEFAULT NULL COMMENT '日用电量',
  `electricity` double(9, 2) NULL DEFAULT NULL COMMENT '总用电量',
  `device_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'FK',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKpduoexqhi7c3s83vi6kk0o8or`(`device_id`) USING BTREE,
  CONSTRAINT `FKpduoexqhi7c3s83vi6kk0o8or` FOREIGN KEY (`device_id`) REFERENCES `ibms_uat`.`iot_device` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'iot断路器设备' ROW_FORMAT = Dynamic;

CREATE TABLE `iot_device_network_change`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `created_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `version` int(11) NULL DEFAULT NULL COMMENT '版本号',
  `data_time` datetime(0) NOT NULL COMMENT '数据时间',
  `device_status` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '华为云中的设备的状态。ONLINE：设备在线。OFFLINE：设备离线。ABNORMAL：设备异常。INACTIVE：设备未激活。FROZEN：设备冻结',
  `device_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'FK 设备ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK8hn1lbcdcacdtt99t9ittoiel`(`device_id`) USING BTREE,
  CONSTRAINT `FK8hn1lbcdcacdtt99t9ittoiel` FOREIGN KEY (`device_id`) REFERENCES `ibms_uat`.`iot_device` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '设备上下线信息' ROW_FORMAT = Dynamic;


CREATE TABLE `iot_intelligent_circuit_breaker`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `created_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `version` int(11) NULL DEFAULT NULL COMMENT '版本号',
  `data_time` datetime(0) NOT NULL COMMENT '数据时间',
  `electricity` decimal(12, 2) NOT NULL COMMENT '用电量',
  `device_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'FK 设备ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKmsay28t0jds5ifx035rir0jdy`(`device_id`) USING BTREE,
  CONSTRAINT `FKmsay28t0jds5ifx035rir0jdy` FOREIGN KEY (`device_id`) REFERENCES `ibms_uat`.`iot_device` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '智能断路器数据表' ROW_FORMAT = Dynamic;