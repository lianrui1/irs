package cn.com.cowain.ibms.entity.distinguish;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/6 14:57
 */
@Data
@Entity
@Table(name = "ibms_access_record")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_access_record", comment = "通行记录表")
@Where(clause = "is_del=0")
public class AccessRecord extends BaseEntity {


    /**
     * 从时间
     */
    @Column(name = "dataTime", nullable = false, columnDefinition = "datetime COMMENT '识别时间'")
    private LocalDateTime dataTime;

    /**
     * 抓怕照片
     */
    @Column(name = "capture_pic", columnDefinition = "varchar(256) COMMENT '抓拍照片'")
    private String capturePicture;

    /**
     * 底库照片
     */
    @Column(name = "default_pic", columnDefinition = "varchar(256) COMMENT '底库照片'")
    private String defaultPicture;

    @Column(name = "device_name", nullable = false, columnDefinition = "varchar(8)  COMMENT '设备名称'")
    private String deviceName;

    @Column(name = "device_address", nullable = false, columnDefinition = "varchar(8)  COMMENT '设备地址'")
    private String deviceAddress;

    /**
     * 云端设备ID
     */
    @Column(name = "device_cloud_id", nullable = false, columnDefinition = "varchar(256) COMMENT 'FK 云端设备ID'")
    private String deviceCloudId;

    /**
     * 云端设备ID
     */
    @Column(name = "sys_user_emp_no", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 员工工号'")
    private String no;

    /**
     * 通行人员工号
     */
    @Column(name = "empNo", nullable = false, columnDefinition = "varchar(36) COMMENT '员工工号'")
    private String empNo;

    /**
     * 通行人员姓名
     */
    @Column(name = "nameZh", nullable = false, columnDefinition = "varchar(36) COMMENT '员工姓名'")
    private String nameZh;

    /**
     * 员工类型
     **/
    @Column(name = "staff_type", nullable = false, columnDefinition = "varchar(36) COMMENT '员工类型'")
    private String staffType;
    /**
     * 体温
     */
    @Column(name = "temperature", columnDefinition = "varchar(10) COMMENT '体温'")
    private String temperature;
    /**
     * 通行状态
     **/
    @Column(name = "access_status", columnDefinition = "varchar(10) COMMENT '通行状态'")
    private String accessStatus;
}
