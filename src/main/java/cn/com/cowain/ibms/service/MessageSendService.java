package cn.com.cowain.ibms.service;

import cn.com.cowain.ibms.entity.MessagePoolRecord;

public interface MessageSendService {

    // 消息推送
    void send(MessagePoolRecord messagePoolRecord);
}
