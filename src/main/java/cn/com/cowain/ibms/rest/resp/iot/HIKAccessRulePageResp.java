package cn.com.cowain.ibms.rest.resp.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Yang.Lee
 * @date 2021/7/20 13:54
 */
@Data
@ApiModel("海康面板及通行权限数据对象")
public class HIKAccessRulePageResp {

    @ApiModelProperty(value = "SN", position = 1)
    private String sn;

    @ApiModelProperty(value = "设备名称", position = 2)
    private String deviceName;

    @ApiModelProperty(value = "设备来源名称", position = 3)
    private String deviceSourceName;

    @ApiModelProperty(value = "项目名称", position = 4)
    private String projectName;

    @ApiModelProperty(value = "空间名称", position = 5)
    private String spaceName;

    @ApiModelProperty(value = "设备地址", position = 6)
    private String deviceAddress;

    @ApiModelProperty(value = "员工数量", position = 7)
    private int staffCount;

    @ApiModelProperty(value = "设备ID")
    private String deviceId;

    @ApiModelProperty(value = "设备类型名称", position = 8)
    private String deviceTypeName;
}
