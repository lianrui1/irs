package cn.com.cowain.ibms.dao.space;

import cn.com.cowain.ibms.entity.space.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * 项目信息数据持久化操作类
 *
 * @author Yang.Lee
 * @date 2020/12/21 10:22
 */
@Repository
public interface ProjectDao extends JpaRepository<Project, String>, JpaSpecificationExecutor<Project> {

    /**
     * 根据projectName模糊查找 分页
     *
     * @param projectName
     * @param pageable
     * @return
     */
    Page<Project> findByProjectNameContains(String projectName, Pageable pageable);

    /**
     * 根据projectName精确查找
     *
     * @param projectName
     * @return
     */
    Optional<Project> findByProjectName(String projectName);

    /**
     * 根据项目编号查询项目信息
     *
     * @param no
     * @return
     */
    Optional<Project> findByNo(String no);

    /**
     * 查询项目列表（不包含默认数据）
     *
     * @param projectNo: 项目编号
     * @return
     * @author Yang.Lee
     * @date 2021/3/8 18:51
     **/
    List<Project> findByNoNotOrderByCreatedTimeDesc(String projectNo);
}
