package cn.com.cowain.ibms.rest.resp.distinguish;

import cn.com.cowain.ibms.rest.req.office.DeviceNameEditReq;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/16 18:02
 */
@Data
public class SwitchResp {

    private String light1;
    private String light2;
    private String light3;
    private String light4;
    private Double electricity;
    @JsonProperty("switch")
    private String isSwitch;
    private String humidity;
    private String luminance;
    private String noise;
    private String pm10;
    private String pm2_5;
    private String temperature;
    private String read = "read";
    private String isPeopleThere;

    @ApiModelProperty(value = "设备附加名称", required = true)
    private DeviceNameEditReq.AdditionalName additionalName;
}
