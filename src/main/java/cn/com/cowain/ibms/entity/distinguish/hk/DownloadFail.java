package cn.com.cowain.ibms.entity.distinguish.hk;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/8/4 11:31
 */
@Data
public class DownloadFail {
    private String jobNo;
    private String msg;
}
