package cn.com.cowain.ibms.rest.req.meeting;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author feng
 * @title: BigScreenMeetingReq
 * @projectName ibms
 * @Date 2021/12/28 10:11
 */
@Data
public class BigScreenMeetingReq {
    @ApiModelProperty(value = "会议主题", position = 1)
    private String meetTheme;
    @ApiModelProperty(value = "会议开始时间： 2011-01-02 10:30:00 ", position = 2)
    private String startTime;
    @ApiModelProperty(value = "会议结束时间： 2011-01-02 10:30:00 ", position = 3)
    private String endTime;
    @ApiModelProperty(value = "大屏设备Id", position = 4)
    private String intelligentMeetingAppId;
}
