package cn.com.cowain.ibms.rest.device;

import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.req.person_permission.DepartmentPersonPermissionCreateReq;
import cn.com.cowain.ibms.rest.req.person_permission.PersonPermissionCreateReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.person_permission.PersonPermissionService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @Author tql
 * @Description 设备人员权限 控制器
 * @Date 21-10-18 下午5:27
 * @Version 1.0
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/person/permission")
@Api(tags = IConst.PERSON_PERMISSION)
public class PersonPermissionController {

    @Autowired
    private PersonPermissionService personPermissionService;

    /**
     * 产品管理-添加设备人员权限
     *
     * @param productReq
     * @return
     */
    @PostMapping
    @ApiOperation(value = "添加设备人员权限", tags = {IConst.PERSON_PERMISSION, IConst.V18})
    public ResponseEntity<JsonResult<IdResp>> save(
            @RequestHeader(name = "Authorization", required = false) String token,
            @RequestBody @Validated PersonPermissionCreateReq productReq, BindingResult bindingResult) {
        log.debug("save...");
        ServiceResult result = personPermissionService.save(productReq);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        JsonResult<IdResp> jsonResult = JsonResult.ok("数据保存成功", (IdResp) result.getObject());
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * @param token         权限
     * @param productReq    部门人员入参
     * @param bindingResult 异常结果
     * @return
     * @description 添加设备部门人员权限
     * @author tql
     * @date 21-11-19
     */
    @PostMapping("/department")
    @ApiOperation(value = "添加设备部门人员权限", tags = {IConst.PERSON_PERMISSION, IConst.V18})
    public ResponseEntity<JsonResult<IdResp>> departmentSave(
            @RequestHeader(name = "Authorization", required = false) String token,
            @RequestBody @Validated DepartmentPersonPermissionCreateReq productReq, BindingResult bindingResult) {
        log.debug("save...");

        ServiceResult result = personPermissionService.departmentSave(productReq);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        JsonResult<IdResp> jsonResult = JsonResult.ok("数据保存成功", (IdResp) result.getObject());
        return ResponseEntity.ok(jsonResult);
    }


    /**
     * 根据 ID 删除人员权限
     *
     * @param empNo   工号
     * @param spaceId 空间ID
     * @return
     */
    @DeleteMapping("/{empNo}/{spaceId}")
    @ApiOperation(value = "根据 ID 删除人员权限", tags = {IConst.PERSON_PERMISSION, IConst.V18})
    public ResponseEntity<JsonResult<ServiceResult>> delete(
            @PathVariable String empNo,
            @PathVariable String spaceId,
            @RequestHeader("Authorization") String token) {
        log.debug("delete, empNo:" + empNo);
        log.debug("delete, spaceId:" + spaceId);
        ServiceResult serviceResult = personPermissionService.deleteByEmpNoAndSpaceId(empNo, spaceId);
        JsonResult<ServiceResult> jsonResult = JsonResult.okNoData("OK");
        if (!serviceResult.isSuccess()) {
            jsonResult = JsonResult.error("操作失败", serviceResult, ErrConst.E01);

        }
        return ResponseEntity.ok(jsonResult);
    }
}
