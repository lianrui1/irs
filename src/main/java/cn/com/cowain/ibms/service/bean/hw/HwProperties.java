package cn.com.cowain.ibms.service.bean.hw;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/7/18 21:23
 */
@Data
public class HwProperties {

    private String isPeopleThere;
}
