package cn.com.cowain.ibms.rest.req.distinguish;

import cn.com.cowain.ibms.entity.distinguish.StaffGroup;
import cn.com.cowain.ibms.entity.distinguish.StaffGroupMembers;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/5 18:57
 */
@Data
@ApiModel("人员组响应对象")
public class StaffGroupReq {

    @Pattern(regexp = "^.{1,32}$", message = "人员组名称长度不超过32")
    @ApiModelProperty(value = "人员组名称", required = true, position = 0)
    private String name;

    @ApiModelProperty(value = "组内人数", position = 1)
    private int count;

    @ApiModelProperty(value = "排序", position = 2)
    private int seq;

    @ApiModelProperty(value = "参与人列表", position = 3)
    private List<String> participantList;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间", position = 4)
    private LocalDateTime createdTime;

    // 人员组信息
    private StaffGroup staffGroup;

    // 人员组成员列表
    private List<StaffGroupMembers> membersList;
}
