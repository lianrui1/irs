package cn.com.cowain.ibms.rest.req.office;

import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/8 15:42
 */
@Data
@ApiModel("智能办公室列表(分页)请求对象")
public class IntelligentOfficePageReq extends PageReq {


    @ApiModelProperty(value = "项目ID")
    private String projectId;

    @ApiModelProperty(value = "所属人名称/工号/办公室名称")
    private String keyword;
}
