package cn.com.cowain.ibms.job;

import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.service.MeetingPollService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Set;

/**
 * 每分钟判断是否有即将开始的会议
 *
 * @author Yuxin Zhang
 * @version 1.0
 * @since 2020-8-20 13:04:47
 */
@Slf4j
public class ReservationReminderJob {

    private MeetingPollService meetingPoll;

    @Value("${cowain.reservation.time}")
    private Integer timeMinute;

    public ReservationReminderJob(MeetingPollService meetingPoll) {
        this.meetingPoll = meetingPoll;
    }

    /**
     * 在每分钟的0s 搜索是否有 即将开始的会议
     */
    @Scheduled(cron = "0 * * * * ?")
    public void run() {
        log.info("start to get coming reservation...");
        Set<ReservationRecord> reservationRecordSet = meetingPoll.getUpcomingMeeting(timeMinute);
        log.debug(String.valueOf(reservationRecordSet));

        // 在每分钟的0s 搜索是否有 即将结束的会议
        log.info("end to get coming reservation...");
        Set<ReservationRecord> reservationRecordSetEnd = meetingPoll.getUpcomingMeetingEnd(timeMinute);
        log.debug(String.valueOf(reservationRecordSetEnd));
    }

}
