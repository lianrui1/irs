package cn.com.cowain.ibms.rest.req.distinguish;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 批量添加请求参数
 *
 * @author: yanzy
 * @date: 2022/4/8 14:37
 */
@Data
public class ImportAddFailedReq {

    @ApiModelProperty(value = "错误数据缓存中uuid")
    private String errorDataId;

    @ApiModelProperty(value = "导入总数据个数")
    private int totalCount;

    @ApiModelProperty(value = "任务id")
    private String taskId;
}
