package cn.com.cowain.ibms.rest.resp.iot;

import cn.com.cowain.ibms.rest.bean.WorkDay;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author wei.cheng
 * @date 2022/09/28 16:44
 */
@Data
public class AccessControlTimePlanResp {

    @ApiModelProperty(value = "日程时段")
    private List<WorkDay> workDays;
}
