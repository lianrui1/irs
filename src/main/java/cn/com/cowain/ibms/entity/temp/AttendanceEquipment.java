package cn.com.cowain.ibms.entity.temp;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Yang.Lee
 * @date 2022/1/12 16:08
 */
@Data
@Entity
@Table(name = "ibm_kqsb_temp")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibm_kqsb_temp", comment = "考勤设备临时表（善营设备临时数据表，考勤功能完善后可删除）")
@Where(clause = "is_del=0")
public class AttendanceEquipment extends BaseEntity {

    @Column(name = "ccDeviceCode", nullable = false, columnDefinition = "varchar(50) COMMENT ''")
    private String ccDeviceCode;

    @Column(name = "name", nullable = false, columnDefinition = "varchar(20) COMMENT ''")
    private String name;

    @Column(name = "optionType", nullable = false, columnDefinition = "varchar(20) COMMENT ''")
    private String optionType;

    @Column(name = "faceType", nullable = false, columnDefinition = "varchar(20) COMMENT ''")
    private String faceType;

    @Column(name = "ip", nullable = false, columnDefinition = "varchar(20) COMMENT ''")
    private String ip;

    @Column(name = "deviceType", nullable = false, columnDefinition = "varchar(20) COMMENT ''")
    private String deviceType;
}
