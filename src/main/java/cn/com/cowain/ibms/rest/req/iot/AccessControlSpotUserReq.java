package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author Yang.Lee
 * @date 2022/2/7 14:37
 */
@Data
public class AccessControlSpotUserReq {

    @ApiModelProperty(value = "人员工号")
    private String empNo;

    @ApiModelProperty(value = "人员姓名")
    private String name;

    @ApiModelProperty(value = "权限开始时间")
    private LocalDateTime startTime;

    @ApiModelProperty(value = "权限结束时间")
    private LocalDateTime endTime;

    @ApiModelProperty(value = "是否是Vip")
    private boolean isVip;
}
