package cn.com.cowain.ibms.rest.device;

import cn.com.cowain.ibms.annotation.Idempotent;
import cn.com.cowain.ibms.dao.iot.CurtainDao;
import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.dao.iot.DoorPlateDao;
import cn.com.cowain.ibms.dao.space.SpaceDeviceControlDao;
import cn.com.cowain.ibms.entity.iot.Curtain;
import cn.com.cowain.ibms.entity.iot.DoorPlate;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.space.SpaceDeviceControl;
import cn.com.cowain.ibms.enumeration.iot.DeviceStatus;
import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.device.DeviceControlLogReq;
import cn.com.cowain.ibms.rest.resp.device.*;
import cn.com.cowain.ibms.rest.resp.distinguish.SwitchResp;
import cn.com.cowain.ibms.rest.resp.iot.CircuitBreakerResp;
import cn.com.cowain.ibms.rest.resp.iot.doorplate.DoorPlateStatusResp;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.*;
import cn.com.cowain.ibms.service.device.HwProductsService;
import cn.com.cowain.ibms.service.iot.DoorPlateService;
import cn.com.cowain.ibms.service.space.IotDeviceService;
import cn.com.cowain.ibms.service.space.SpaceService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.ibms.utils.TokenUtils;
import cn.com.cowain.sfp.comm.utils.JwtUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/2/18 13:26
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/device")
@Api(tags = IConst.DEVICE)
public class DeviceController {

    @Autowired
    private HwProductsService productsService;

    @Autowired
    private DoorPlateService doorPlateService;

    @Autowired
    private DeviceDao deviceDao;

    @Autowired
    private DoorPlateDao doorPlateDao;

    @Autowired
    private CurtainDao curtainDao;

    @Autowired
    private SpaceDeviceControlDao spaceDeviceControlDao;

    @Autowired
    private IotDeviceService iotDeviceService;

    @Resource
    private SpaceService spaceService;

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserServiceUC;

    @GetMapping("/control/log/page")
    @ApiOperation(value = "查询设备控制日志列表（分页）", tags = IConst.DEVICE)
    public ResponseEntity<JsonResult<PageBean<DeviceControlLogResp>>> getDeviceControlLogPage(DeviceControlLogReq req) {

        log.debug(req.toString());

        PageBean<DeviceControlLogResp> pageBean = iotDeviceService.getDeviceControlLogPage(req);

        return ResponseEntity.ok(JsonResult.ok("ok" ,pageBean));
    }

    /**
     * 门磁开门
     *
     * @param id 设备ID
     * @return
     */
    @PutMapping("/{id}")
    @ApiOperation(value = "门磁开门", tags = IConst.DEVICE)
    public ResponseEntity<JsonResult<Object>> open(@PathVariable @ApiParam("设备ID") String id) {

        ServiceResult result = doorPlateService.openDoor(id);
        JsonResult<Object> jsonResult = JsonResult.okNoData("OK");
        if (!result.isSuccess()) {
            jsonResult = JsonResult.error("数据校验错误", result, ErrConst.E01);
        }
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 设置工作模式
     *
     * @param id 设备ID
     * @return
     */
    @PutMapping("/workingMode/{id}")
    @ApiOperation(value = "设置工作模式", tags = IConst.DEVICE)
    public ResponseEntity<JsonResult<Object>> workingMode(@PathVariable @ApiParam("模式id") String id) {

        ServiceResult result = productsService.workingMode(id);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    /**
     * 控制硬件设备
     *
     * @param id 华为云设备ID
     * @return
     */
    @PutMapping("/device/{id}")
    @ApiOperation(value = "控制硬件设备", tags = {IConst.DEVICE, IConst.V21})
    @Idempotent(value = API_BASE + "/device/device/post", expireTime = 3L)
    public ResponseEntity<JsonResult<AirConditionerReq>> update(@PathVariable @ApiParam("华为设备ID") String id, @RequestBody @Validated @ApiParam("设备命令") DoorMagneticHWParasReq paras, @RequestHeader("Authorization") String token) {

        if(null == paras.getIsAuth()){
            // 判断员工是否有设备控制权限
            ServiceResult resultRule = resultRule(id);
            if (!resultRule.isSuccess()) {
                return ResponseEntity.ok(JsonResult.error(String.valueOf(resultRule.getObject()), null, ErrConst.E01));
            }
        }
        // 判断该设备在华为云是否存在
            Optional<IotDevice> dev = deviceDao.findByHwDeviceId(id);
        if (dev.isPresent()) {
            IotDevice device = dev.get();
            if (!device.getHwStatus().equals(DeviceStatus.ONLINE)) {
                return ResponseEntity.ok(JsonResult.error(ServiceMessage.ERR_STATUS, null, ErrConst.E01));
            }
            // 获取设备所属产品的服务ID和命令名称
            ServiceResult serviceResult = productsService.findProductProperties(device);
            if(!serviceResult.isSuccess()){
                return ResponseEntity.ok(JsonResult.error(String.valueOf(serviceResult.getObject()), null, ErrConst.E01));
            }
            DoorMagneticHWReq hwReq = (DoorMagneticHWReq) serviceResult.getObject();
            String hrId = JwtUtil.getHrId(TokenUtils.getToken());
            String name = "";
            try {
                // 保存人员权限
                name = sysUserServiceUC.getUserByEmpNo(hrId).getNameZh();
            } catch (Exception e) {
                log.error(e.getMessage());
                log.error("获取人员信息失败");
            }
            // 下发设备控制命令
            hwReq.setDeviceId(device.getId());
            hwReq.setDevice_id(id);
            hwReq.setParas(paras);
            ServiceResult result = productsService.command(hwReq, hrId, name);
            if (!result.isSuccess()) {
                return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
            }
        } else {
            return ResponseEntity.ok(JsonResult.error(ServiceMessage.ERR_DEVICE_ID, null, ErrConst.E01));
        }
        IotDevice device = dev.get();
        // 将下发命令返回
        AirConditionerReq airConditionerReq = new AirConditionerReq();
        airConditionerReq.setDeviceName(device.getHwDeviceName());
        airConditionerReq.setMode(StringUtils.isNotEmpty(paras.getSetMode()) ? paras.getSetMode(): "");
        airConditionerReq.setSetFanAuto(StringUtils.isNotEmpty(paras.getSetFanAuto()) ? paras.getSetFanAuto() : "");
        airConditionerReq.setSetPower(StringUtils.isNotEmpty(paras.getSetPower()) ? paras.getSetPower() : "");
        airConditionerReq.setTemperature(paras.getSetTemp() != null ? paras.getSetTemp() : 0);
        airConditionerReq.setWindDirection(StringUtils.isNotEmpty(paras.getSetFanDirection()) ? paras.getSetFanDirection() : "");
        airConditionerReq.setWindSpeed(StringUtils.isNotEmpty(paras.getSetFanSpeed()) ? paras.getSetFanSpeed() : "");
        airConditionerReq.setSetSwing(StringUtils.isNotEmpty(paras.getSetSwing()) ? paras.getSetSwing() : "");
        JsonResult<AirConditionerReq> jsonResult = JsonResult.ok("OK", airConditionerReq);
        return ResponseEntity.ok(jsonResult);
    }

    private ServiceResult resultRule(String id) {

        String hrId = JwtUtil.getHrId(TokenUtils.getToken());
        Optional<IotDevice> deviceOp = deviceDao.findByHwDeviceId(id);

        if (deviceOp.isEmpty()) {
            return ServiceResult.error("未找到设备", ErrConst.E01);
        }
        IotDevice device = deviceOp.get();
        // 判断用户是否是设备所在空间的管理员
        Space deviceSpace = device.getSpace();

        // 设备不在任何空间下，无权限操作
        if (deviceSpace == null) {
            log.info("设备控制失败，设备不属于任何空间，设备华为ID {}", id);
            return ServiceResult.error("无权限", ErrConst.E01);
        }

        // hrIdList为空表示用户不是管理员，判断其他人的控制权限
        if (!spaceService.isUserSpaceAdmin(hrId, deviceSpace.getId())) {
            Optional<SpaceDeviceControl> deviceControlOp = spaceDeviceControlDao.findByUserEmpNoAndDeviceId(hrId, device.getId());
            if (deviceControlOp.isEmpty()) {
                return ServiceResult.error("无权限", ErrConst.E01);
            }
        }

        return ServiceResult.ok();
    }

    /**
     * 控制四路开关
     *
     * @param id 华为云设备ID
     * @return
     */
    @PutMapping("/switch/{id}")
    @ApiOperation(value = "控制四路开关/断路器", tags = {IConst.DEVICE, IConst.V21})
    @Idempotent(value = API_BASE + "/switch/post", expireTime = 3L)
    public ResponseEntity<JsonResult<AirConditionerReq>> updateSwitch(@PathVariable @ApiParam("华为设备ID") String id, @RequestBody @Validated @ApiParam("设备命令") DoorMagneticHWParasReq paras) {

        // 判断员工是否有设备控制权限
        ServiceResult resultRule = resultRule(id);
        if (!resultRule.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(resultRule.getObject()), null, ErrConst.E01));
        }
        // 判断该设备在华为云是否存在
        Optional<IotDevice> dev = deviceDao.findByHwDeviceId(id);
        if (dev.isPresent()) {
            IotDevice device = dev.get();
            if (!device.getHwStatus().equals(DeviceStatus.ONLINE)) {
                return ResponseEntity.ok(JsonResult.error(ServiceMessage.ERR_STATUS, null, ErrConst.E01));
            }
            // 获取设备所属产品的服务ID和命令名称
            ServiceResult serviceResult = productsService.findProductProperties(device);
            if(!serviceResult.isSuccess()){
                return ResponseEntity.ok(JsonResult.error(String.valueOf(serviceResult.getObject()), null, ErrConst.E01));
            }
            DoorMagneticHWReq hwReq = (DoorMagneticHWReq) serviceResult.getObject();
            String hrId = JwtUtil.getHrId(TokenUtils.getToken());
            String name = "";
            try {
                // 保存人员权限
                name = sysUserServiceUC.getUserByEmpNo(hrId).getNameZh();
            } catch (Exception e) {
                log.error(e.getMessage());
                log.error("获取人员信息失败");
            }
            // 下发控制设备
            hwReq.setDevice_id(id);
            hwReq.setParas(paras);
            hwReq.setDeviceId(device.getId());
            ServiceResult result = productsService.updateSwitch(hwReq, hrId, name);
            if (!result.isSuccess()) {
                return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
            }
        } else {
            return ResponseEntity.ok(JsonResult.error(ServiceMessage.ERR_DEVICE_ID, null, ErrConst.E01));
        }
        JsonResult<AirConditionerReq> jsonResult = JsonResult.ok("OK", null);
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 会议室通过设备属性控制设备 只有会议参会人可控制
     *
     * @param id 华为云设备ID
     * @return
     */
    @PutMapping("/properties/{id}")
    @ApiOperation(value = "会议室通过设备属性控制设备", tags = IConst.DEVICE)
    public ResponseEntity<JsonResult<String>> properties(@PathVariable @ApiParam("华为设备ID") String id, @RequestHeader("Authorization") String token, @RequestBody @Validated @ApiParam("设备命令") DoorMagneticHWParasReq paras) {
        // 从token中解析员工工号
        String hrId = JwtUtil.getHrId(token);
        ServiceResult result = productsService.properties(id, hrId, paras);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        JsonResult<String> jsonResult = JsonResult.ok("OK", null);
        return ResponseEntity.ok(jsonResult);
    }

    /**
     * 会议室通过命令控制设备 只有会议参会人可控制
     *
     * @param id 华为云设备ID
     * @return
     */
    @PutMapping("/command/{id}")
    @ApiOperation(value = "会议室通过命令控制设备", tags = IConst.DEVICE)
    public ResponseEntity<JsonResult<AirConditionerReq>> command(@PathVariable @ApiParam("华为设备ID") String id, @RequestHeader("Authorization") String token, @RequestBody @Validated @ApiParam("设备命令") DoorMagneticHWParasReq paras) {
        // 从token中解析员工工号
        String hrId = JwtUtil.getHrId(token);
        ServiceResult result = productsService.commandRecord(id, hrId, paras);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        JsonResult<AirConditionerReq> jsonResult = JsonResult.ok("OK", (AirConditionerReq) result.getObject());
        return ResponseEntity.ok(jsonResult);
    }


    /**
     * BIM模型查询门牌设备信息
     *
     * @param id 设备ID
     * @return
     */
    @GetMapping("/doorPlate/{id}")
    @ApiOperation(value = "BIM模型查询门牌设备信息", tags = IConst.DEVICE)
    public ResponseEntity<JsonResult<DoorPlateStatusResp>> search(@PathVariable @ApiParam("设备ID") String id) {

        Optional<DoorPlate> byDeviceId = doorPlateDao.findByDeviceId(id);
        if (byDeviceId.isPresent()) {
            DoorPlate doorPlate = byDeviceId.get();
            ServiceResult result = doorPlateService.status(doorPlate.getId());
            if (!result.isSuccess()) {
                return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
            }
            return ResponseEntity.ok(JsonResult.ok("ok", (DoorPlateStatusResp) result.getObject()));
        } else {
            return ResponseEntity.ok(JsonResult.error(ServiceMessage.ERR_DEVICE_ID, null, ErrConst.E01));
        }
    }

    /**
     * 查询空调设备
     *
     * @param id 华为云设备ID
     * @return
     */
    @GetMapping("/air/{id}")
    @ApiOperation(value = "查询空调设备参数", tags = {IConst.DEVICE, IConst.V21})
    public ResponseEntity<JsonResult<AirConditionerResp>> searchStatus(@PathVariable @ApiParam("华为云设备ID") String id) {

        // 判断该设备是否存在
        Optional<IotDevice> byHwDeviceId = deviceDao.findByHwDeviceId(id);
        if (byHwDeviceId.isPresent()) {
            IotDevice device = byHwDeviceId.get();

            //获取设备所属产品的服务ID和命令名称
            ServiceResult result = productsService.findProductProperties(device);
            if(!result.isSuccess()){
                return ResponseEntity.ok(JsonResult.error(ServiceMessage.ERR_STATUS, null, ErrConst.E01));
            }
            DoorMagneticHWReq hwReq = (DoorMagneticHWReq) result.getObject();
            ServiceResult serviceResult = productsService.findAirProperties(id, hwReq.getService_id());
            if (!serviceResult.isSuccess()){
                return ResponseEntity.ok(JsonResult.error(ServiceMessage.ERR_DEVICE_ID, null, ErrConst.E01));
            }
            return ResponseEntity.ok(JsonResult.ok("OK",(AirConditionerResp)serviceResult.getObject()));

        } else {
            return ResponseEntity.ok(JsonResult.error(ServiceMessage.ERR_DEVICE_ID, null, ErrConst.E01));
        }
    }


    /**
     * 查询指定设备的遥测数据
     *
     * @param id 华为云设备ID
     * @return
     */
    @GetMapping("/data/{id}")
    @ApiOperation(value = "查询指定设备的遥测数据", tags = {IConst.DEVICE, IConst.V21})
    public ResponseEntity<JsonResult<List<TbDataResp>>> searchData(@PathVariable @ApiParam("华为云设备ID") String id,
                                                                   @RequestParam(value = "days", required = false, defaultValue = "7") @ApiParam("查询天数") int days) {

        String tbId = productsService.findTbId(id);
        if (tbId.isEmpty()) {
            return ResponseEntity.ok(JsonResult.error("查询失败 设备id有误", null, null));
        }
        List<TbDataResp> data = productsService.findData(tbId, days);
        return ResponseEntity.ok(JsonResult.ok("ok", data));
    }

    /**
     * 查询传感器遥测数据
     *
     * @param id 华为云设备ID
     * @return
     */
    @GetMapping("/now/{id}")
    @ApiOperation(value = "查询传感器最近的遥测数据", tags = {IConst.DEVICE, IConst.V21})
    public ResponseEntity<JsonResult<List<TbDataResp>>> searchNowData(@PathVariable @ApiParam("华为云设备ID") String id) {

        String tbId = productsService.findTbId(id);
        if (tbId == null) {
            return ResponseEntity.ok(JsonResult.error("查询失败 设备id有误", null, null));
        }
        List<TbDataResp> data = productsService.findNowData(tbId);
        return ResponseEntity.ok(JsonResult.ok("ok", data));
    }

    /**
     * 查询窗帘状态
     *
     * @param id 华为云设备ID
     * @return
     */
    @GetMapping("/curtain/{id}")
    @ApiOperation(value = "查询窗帘状态", tags = {IConst.DEVICE, IConst.V21})
    public ResponseEntity<JsonResult<CurtainResp>> searchCurtain(@PathVariable @ApiParam("华为云设备ID") String id) {

        // 判断该设备是否存在
        Optional<IotDevice> byHwDeviceId = deviceDao.findByHwDeviceId(id);
        if (byHwDeviceId.isPresent()) {
            IotDevice device = byHwDeviceId.get();
            Optional<Curtain> byDevice = curtainDao.findByDevice(device);
            // 判断该空调是否是首次操作 是则返回空 不是则返回上次操作命令参数
            if (byDevice.isPresent()) {
                Curtain curtain = byDevice.get();
                CurtainResp curtainResp = new CurtainResp();
                curtainResp.setAction(curtain.getAction());
                return ResponseEntity.ok(JsonResult.ok("ok", curtainResp));
            } else {
                CurtainResp curtainResp = new CurtainResp();
                curtainResp.setAction("");
                return ResponseEntity.ok(JsonResult.ok("ok", curtainResp));
            }
        } else {
            return ResponseEntity.ok(JsonResult.error(ServiceMessage.ERR_DEVICE_ID, null, ErrConst.E01));
        }
    }

    /**
     * 查询四路开关
     *
     * @param id 华为云设备ID
     * @return
     */
    @GetMapping("/switch/{id}")
    @ApiOperation(value = "查询四路开关状态", tags = {IConst.DEVICE, IConst.V21})
    public ResponseEntity<JsonResult<SwitchResp>> searchSwitch(@PathVariable @ApiParam("华为云设备ID") String id) {

        // 判断该设备在华为云是否存在
        Optional<IotDevice> dev = deviceDao.findByHwDeviceId(id);
        if (dev.isPresent()) {
            IotDevice device = dev.get();
            // 获取设备所属产品的服务ID和命令名称
            ServiceResult serviceResult = productsService.findProductProperties(device);
            if(!serviceResult.isSuccess()){
                return ResponseEntity.ok(JsonResult.error(String.valueOf(serviceResult.getObject()), null, ErrConst.E01));
            }
            DoorMagneticHWReq hwReq = (DoorMagneticHWReq) serviceResult.getObject();
            SwitchResp data = productsService.findProperties(id, hwReq.getService_id());
            if (data.getRead().equals("error")) {
                return ResponseEntity.ok(JsonResult.error(ServiceMessage.ERR_STATUS, null, ErrConst.E01));
            }
            data.setRead(null);
            return ResponseEntity.ok(JsonResult.ok("OK", data));
        } else {
            return ResponseEntity.ok(JsonResult.error(ServiceMessage.ERR_DEVICE_ID, null, ErrConst.E01));
        }
    }

    /**
     * 查询智能断路器用电数据
     *
     * @param deviceId  设备ID
     * @param startTime 查询开始时间
     * @param endTime   查询结束时间
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/4/29 9:25
     **/
    @GetMapping("/intelligentCircuitBreaker/{deviceId}")
    @ApiOperation(value = "查询智能断路器数据", tags = IConst.DEVICE)
    public ResponseEntity<JsonResult<CircuitBreakerResp>>
    getIntelligentCircuitBreakerData(@PathVariable @ApiParam(value = "设备ID") String deviceId,
                                     @RequestParam @ApiParam(value = "查询开始时间", required = true) String startTime,
                                     @RequestParam @ApiParam(value = "查询结束时间", required = true) String endTime) {

        // 判断该设备在华为云是否存在
        Optional<IotDevice> dev = deviceDao.findByHwDeviceId(deviceId);
        if (dev.isPresent()) {
            IotDevice device = dev.get();
            CircuitBreakerResp data = productsService.getIntelligentCircuitBreakerData(device, startTime, endTime);
            return ResponseEntity.ok(JsonResult.ok("OK", data));
        } else {
            return ResponseEntity.ok(JsonResult.error(ServiceMessage.ERR_DEVICE_ID, null, ErrConst.E01));
        }
    }

    /**
     * 查询光照温湿度传感器数据
     *
     * @param deviceId 设备ID
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/4/29 9:25
     **/
    @GetMapping("/threeInOne/{deviceId}")
    @ApiOperation(value = "查询多传感器数据", tags = IConst.DEVICE)
    public ResponseEntity<JsonResult<SwitchResp>> getThreeInOneSensorData(@PathVariable @ApiParam(value = "设备ID") String deviceId) {
        // 判断该设备在华为云是否存在
        Optional<IotDevice> dev = deviceDao.findByHwDeviceId(deviceId);
        if (dev.isPresent()) {
            IotDevice device = dev.get();
            // 获取设备所属产品的服务ID和命令名称
            ServiceResult serviceResult = productsService.findProductProperties(device);
            if(!serviceResult.isSuccess()){
                return ResponseEntity.ok(JsonResult.error(String.valueOf(serviceResult.getObject()), null, ErrConst.E01));
            }
            DoorMagneticHWReq hwReq = (DoorMagneticHWReq) serviceResult.getObject();
            SwitchResp data = productsService.findPropertiesTwo(deviceId, hwReq.getService_id());
            return ResponseEntity.ok(JsonResult.ok("OK", data));
        } else {
            return ResponseEntity.ok(JsonResult.error(ServiceMessage.ERR_DEVICE_ID, null, ErrConst.E01));
        }
    }

    /**
     * 楼宇总览
     *
     * @return
     */
    @GetMapping("/full/view")
    @ApiOperation(value = "楼宇总览", tags = {IConst.DEVICE, IConst.V18})
    public ResponseEntity<JsonResult<FullViewResp>> fullView() {
        ServiceResult serviceResult = iotDeviceService.fullView();
        if (!serviceResult.isSuccess()) {
            return ResponseEntity.ok(JsonResult.okNoData("查无数据"));
        }
        return ResponseEntity.ok(JsonResult.ok("OK", (FullViewResp) serviceResult.getObject()));
    }

    /**
     * 楼栋视图
     *
     * @param projectId
     * @param spaceId
     * @return
     */
    @GetMapping("/building/view/{projectId}/{spaceId}")
    @ApiOperation(value = "楼栋视图", tags = {IConst.DEVICE, IConst.V18})
    public ResponseEntity<JsonResult<FullViewResp>> buildingView(@PathVariable String projectId, @PathVariable String spaceId) {
        ServiceResult serviceResult = iotDeviceService.buildingView(projectId, spaceId);
        if (!serviceResult.isSuccess()) {
            return ResponseEntity.ok(JsonResult.okNoData("查无数据"));
        }
        return ResponseEntity.ok(JsonResult.ok("OK", (FullViewResp) serviceResult.getObject()));
    }

    /**
     * @param spaceId
     * @return
     * @description 楼层视图
     * @author tql
     * @date 21-11-15
     */
    @GetMapping("/floor/view/{spaceId}")
    @ApiOperation(value = "楼层视图", tags = {IConst.DEVICE, IConst.V18})
    public ResponseEntity<JsonResult<FloorViewResp>> floorView(@PathVariable String spaceId) {

        ServiceResult serviceResult = iotDeviceService.floorView(spaceId);
        if (!serviceResult.isSuccess()) {
            return ResponseEntity.ok(JsonResult.okNoData("查无数据"));
        }
        return ResponseEntity.ok(JsonResult.ok("OK", (FloorViewResp) serviceResult.getObject()));
    }
}
