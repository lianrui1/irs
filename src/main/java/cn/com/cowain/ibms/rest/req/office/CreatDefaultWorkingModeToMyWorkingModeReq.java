package cn.com.cowain.ibms.rest.req.office;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/12/27 9:57
 */
@Data
@ApiModel("添加默认场景至我的接受对象")
public class CreatDefaultWorkingModeToMyWorkingModeReq {


    @NotEmpty
    @ApiModelProperty(value = "默认场景模式ID")
    private String defaultWorkingModeId;

    @NotEmpty
    @ApiModelProperty(value = "办公室ID")
    private String officeId;
}
