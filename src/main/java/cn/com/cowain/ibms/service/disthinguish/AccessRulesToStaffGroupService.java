package cn.com.cowain.ibms.service.disthinguish;

import cn.com.cowain.ibms.entity.distinguish.AccessRules;
import cn.com.cowain.ibms.entity.distinguish.StaffGroup;
import cn.com.cowain.ibms.service.bean.ServiceResult;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 通行权限 - 人员组关系Service
 *
 * @author Yang.Lee
 * @date 2021/3/10 15:23
 */
public interface AccessRulesToStaffGroupService {

    /**
     * 根据权限写入关系
     *
     * @param rule     通行权限
     * @param groupIds 人员组id列表
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/3/10 15:26
     **/
    ServiceResult saveGroupIdsByRule(@NotNull AccessRules rule, @NotNull List<StaffGroup> groupIds);
}
