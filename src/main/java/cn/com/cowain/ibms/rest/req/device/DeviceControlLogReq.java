package cn.com.cowain.ibms.rest.req.device;

import cn.com.cowain.ibms.enumeration.iot.DeviceControlStatus;
import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Data
@ApiModel("设备控制日志列表分页请求对象")
public class DeviceControlLogReq extends PageReq {

    @ApiModelProperty(value = "控制人姓名/工号/设备名称")
    private String keyword;

    @ApiModelProperty(value = "控制状态")
    private DeviceControlStatus deviceControlStatus;

    @ApiModelProperty(value = "开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime startTime;

    @ApiModelProperty(value = "结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime endTime;
}
