package cn.com.cowain.ibms.rest.resp.office;

import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.space.SpaceDeviceControl;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author feng
 * @title: PowerScopeResp
 * @projectName bims
 * @Date 2021/12/7 17:33
 */
@Data
public class PowerScopeResp implements Serializable {
    private Space space;
    private List<SpaceDeviceControl> spaceDeviceControlList;
}
