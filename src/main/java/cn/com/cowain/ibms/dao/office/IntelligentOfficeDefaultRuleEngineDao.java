package cn.com.cowain.ibms.dao.office;

import cn.com.cowain.ibms.entity.office.IntelligentOfficeDefaultRuleEngine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * IntelligentOfficeDefaultRuleEngineDao
 *
 * @author: yanzy
 * @date: 2022/2/26 13:23
 */
@Repository
public interface IntelligentOfficeDefaultRuleEngineDao extends JpaRepository<IntelligentOfficeDefaultRuleEngine,String> {
}
