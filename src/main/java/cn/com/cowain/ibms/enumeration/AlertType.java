package cn.com.cowain.ibms.enumeration;

/**
 * 警戒抓怕类型
 *
 * @author: yanzy
 * @date: 2022/4/11 18:33
 */
public enum AlertType {

    INTRUSION("人员入侵"),

    LINGER("人员徘徊"),

    SURMOUNT("翻越"),

    PLAY_PHONE("玩手机"),

    PHONE("打电话"),

    SMOKE("吸烟");

    private final String name;

    AlertType(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
