package cn.com.cowain.ibms.service.monitor.impl;

import cn.com.cowain.ibms.dao.iot.*;
import cn.com.cowain.ibms.entity.iot.*;
import cn.com.cowain.ibms.feign.iotc.IOTCApi;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.Sorts;
import cn.com.cowain.ibms.rest.req.iot.MonitorReq;
import cn.com.cowain.ibms.rest.req.iot.ScreenShotReq;
import cn.com.cowain.ibms.rest.resp.*;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.monitor.MonitorService;
import cn.com.cowain.ibms.service.space.SpaceService;
import cn.com.cowain.sfp.comm.JsonResult;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author feng
 * @title: MonitorServiceImpl
 * @projectName bims
 * @Date 2021/11/24 10:57
 */

@Slf4j
@Service
@Transactional
public class MonitorServiceImpl implements MonitorService {
    @Resource
    private YSVideoCameraDao ySVideoCameraDao;
    @Resource
    private VideoMonitorSpotDao videoMonitorSpotDao;
    @Resource
    private SpaceDao spaceDao;
    @Autowired
    private DeviceDao iotDeviceDao;
    @Resource
    private VideoScreenshotDao videoScreenshotDao;

    @Resource
    private IOTCApi iotcApi;
    @Override
    public PageBean<MonitorLocationResp> getMonitorList(MonitorReq req) {
        int page = req.getPage();
        int size = req.getSize();
        // 创建分页查询对象
        Sort sort = Sort.by(req.getOrders());
        Pageable pageable = PageRequest.of(page, size, sort);
        List<VideoMonitorSpot> cameras= videoMonitorSpotDao.findAll();
        Page<YSVideoCamera> cameraList=null;
        if(CollectionUtils.isNotEmpty(cameras)) {
            List<IotDevice> deviceList = cameras.stream().map(VideoMonitorSpot::getDevice).collect(Collectors.toList());
            List<String> deviceIds = deviceList.stream().map(IotDevice::getId).collect(Collectors.toList());
            cameraList = ySVideoCameraDao.findByDeviceIdNotIn(deviceIds,pageable);
        }else{
            cameraList=ySVideoCameraDao.findAll(pageable);
        }
        List<MonitorLocationResp> respList = new ArrayList<MonitorLocationResp>();
        if (CollectionUtils.isNotEmpty(cameraList.getContent())) {
            for (YSVideoCamera carmera : cameraList.getContent()) {
                MonitorLocationResp resp = MonitorLocationResp.convert(carmera);
                //通过deviceId 来查询视频流
                resp.setId(carmera.getDevice().getId());
                respList.add(resp);
            }
        }


        PageBean<MonitorLocationResp> pageBean = new PageBean<>();
        pageBean.setList(respList);
        pageBean.setTotalElements(cameraList.getTotalElements());
        pageBean.setTotalPages((int) Math.ceil((double) cameraList.getTotalElements() / (double) size));
        pageBean.setPage(page);
        pageBean.setSize(size);
        return pageBean;
    }

    @Override
    public Boolean createMonitorSit(MonitorSitResp sitResp) {
        VideoMonitorSpot spot = new VideoMonitorSpot(sitResp);

        Optional<Space> spaceOpt = spaceDao.findById(sitResp.getSpaceId());
        if (spaceOpt.isEmpty()) {
            log.error("未查到空间信息，请核对后再请求");
            return Boolean.FALSE;
        }
        spot.setSpace(spaceOpt.get());
        Optional<IotDevice> deviceOptional = iotDeviceDao.findById(sitResp.getMonitorId());
        if (deviceOptional.isEmpty()) {
            log.error("当前设备不存在！");
            return Boolean.FALSE;
        }
        spot.setDevice(deviceOptional.get());
        if (StringUtils.isNotEmpty(sitResp.getId())) {
            spot.setId(sitResp.getId());
            Optional<VideoMonitorSpot> spotOp = videoMonitorSpotDao.findById(sitResp.getId());
            if(spotOp.isPresent()) {
                spot.setVersion(spotOp.get().getVersion());
            }else{
                log.error("当前监控点不存在！"+"监控点ID："+sitResp.getId());
                return Boolean.FALSE;
            }
        }

        videoMonitorSpotDao.save(spot);
        return Boolean.TRUE;
    }

    @Override
    public List<LocationNodeResp> getListMonitorSitList() {

        List<VideoMonitorSpot> listSpot = videoMonitorSpotDao.findAll();
        // todo 需要对listSpot排序
        List<LocationNodeResp> result = new ArrayList<>();
        if (CollectionUtils.isEmpty(listSpot)) {
            //未查到数据
            return result;
        }
        Map<LocationNodeResp, List<VideoMonitorSpot>> concurrentHashMap = new LinkedHashMap<>();
        for (VideoMonitorSpot monitorSpot : listSpot) {
            Space space = monitorSpot.getSpace();
            LocationNodeResp resp = new LocationNodeResp();
            Space parent = space.getParent();
            String parentName = parent == null ? space.getName() : SpaceService.getFullSpaceName(parent, parent.getName());
            resp.setId(space.getId());
            resp.setName(parentName);
            if (StringUtils.isNotEmpty(parentName)) {
                if (concurrentHashMap.containsKey(resp)) {
                    List<VideoMonitorSpot> currentList = concurrentHashMap.get(resp);
                    currentList.add(monitorSpot);
                    concurrentHashMap.put(resp, currentList);
                } else {
                    List<VideoMonitorSpot> currentList = new ArrayList<>();
                    currentList.add(monitorSpot);
                    concurrentHashMap.put(resp, currentList);
                }
            }
        }
        List<LocationNodeResp> resultList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(concurrentHashMap)) {
            Set<Map.Entry<LocationNodeResp, List<VideoMonitorSpot>>> set = concurrentHashMap.entrySet();
            Iterator<Map.Entry<LocationNodeResp, List<VideoMonitorSpot>>> it = set.iterator();
            while (it.hasNext()) {
                LocationNodeResp resp = new LocationNodeResp();
                Map.Entry<LocationNodeResp, List<VideoMonitorSpot>> monitorSpot = it.next();
                resp.setName(monitorSpot.getKey().getName());
                resp.setId(monitorSpot.getKey().getId());
                if (CollectionUtils.isNotEmpty(monitorSpot.getValue())) {
                    List<MonitorNodeResp> monitorLocationRespList = new ArrayList<>();
                    for (VideoMonitorSpot video : monitorSpot.getValue()) {
                        MonitorNodeResp monitorNodeResp = new MonitorNodeResp(video);
                        monitorLocationRespList.add(monitorNodeResp);
                    }
                    resp.setMonitorLocationRespList(monitorLocationRespList);
                }

                resultList.add(resp);
            }
        }
        return resultList;
    }

    @Override
    public PageBean<MonitorSitResp> getMonitorListBySportId(String spotId, MonitorReq req) {
        Page<VideoMonitorSpot> page = null;
        if (StringUtils.isNotEmpty(spotId) && spotId.equals("-1")) {
            page = videoMonitorSpotDao.findAll(PageRequest.of(req.getPage(), req.getSize(), Sort.by(req.getOrders())));
        } else {
            page = videoMonitorSpotDao.getBySpaceId(spotId, PageRequest.of(req.getPage(), req.getSize(), Sort.by(req.getOrders())));
        }
        List<MonitorSitResp> sitResult = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(page.getContent())) {
            String token = "";
            try {
                JsonResult<TokenResp> tokenJson = iotcApi.getToken();
                if (tokenJson.getStatus() != 0) {
                    token = tokenJson.getData().getToken();
                }
            } catch (Exception e) {
                log.error(e.getMessage());
            }
            for (VideoMonitorSpot spot : page.getContent()) {
                MonitorSitResp sitResp = new MonitorSitResp(spot);
                sitResp.setToken(token);
                sitResult.add(sitResp);
            }
        }
        PageBean<MonitorSitResp> pageBean = new PageBean<>();
        pageBean.setPage(page.getPageable().getPageNumber());
        pageBean.setSize(page.getPageable().getPageSize());
        pageBean.setTotalPages(page.getTotalPages());
        pageBean.setTotalElements(page.getTotalElements());
        pageBean.setList(sitResult);
        return pageBean;
    }

    @Override
    public MonitorLocationResp getMonitorDetail(String monitorId) {


        Optional<VideoMonitorSpot> monitorSpot = videoMonitorSpotDao.findById(monitorId);
        MonitorLocationResp result = new MonitorLocationResp();
        if (!monitorSpot.isEmpty() && monitorSpot.get().getDevice() != null) {
            YSVideoCamera camera = ySVideoCameraDao.findByDeviceId(monitorSpot.get().getDevice().getId());
            result = MonitorLocationResp.convert(camera);
            result.setName(monitorSpot.get().getName());
            result.setLocation(monitorSpot.get().getAddress());
            result.setHasBanding(Boolean.TRUE);
            result.setId(monitorSpot.get().getId());
            MonitorSitResp monitorSitResp = new MonitorSitResp(monitorSpot.get());
            try {
                JsonResult<TokenResp> tokenJson = iotcApi.getToken();
                if (tokenJson.getStatus() != 0) {
                    monitorSitResp.setToken(tokenJson.getData().getToken());
                }
            }catch (Exception e){
                log.error(e.getMessage());
            }
            result.setMonitorSitResp(monitorSitResp);
        }

        return result;
    }

    @Override
    public Boolean deleteSpot(String id) {

        Optional<VideoMonitorSpot> spotSpot = videoMonitorSpotDao.findById(id);
        if(spotSpot.isEmpty()){
            return false;
        }
        VideoMonitorSpot spot =  spotSpot.get();
        spot.setIsDelete(1);
        videoMonitorSpotDao.save(spot);
            return true;


    }
    @Override
    public String upLoadPictureSave(ScreenShotReq req) {
        Optional<VideoMonitorSpot> spotSpot = videoMonitorSpotDao.findById(req.getShotSitId());
        if(spotSpot.isEmpty()){
            return "文件上传失败，未找到站点信息";
        }
        VideoScreenshot screenshot = new VideoScreenshot();
        screenshot.setVideoMonitorSpot(spotSpot.get());
        screenshot.setPic(req.getImageUrl());
        videoScreenshotDao.save(screenshot);
        return "文件上传成功";
    }

    @Override
    public PageBean<VideoScreenshot> creenshotList(MonitorReq req) {
        // 查询智能办公列表分页
        Sort sort = Sort.by(Sort.Direction.DESC, Sorts.CREATED_TIME);
        Pageable pageable = PageRequest.of(req.getPage(), req.getSize(), sort);

        Page<VideoScreenshot> videoScreenshotPage = videoScreenshotDao.findAll((root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> list = new ArrayList<>();
            // 根据项目ID获取
            if (req.getStartTime() != null) {

                list.add(criteriaBuilder.and(criteriaBuilder.greaterThanOrEqualTo(root.get("createdTime"), req.getStartTime())));
            }
            if (req.getEndTime() != null) {
                list.add(criteriaBuilder.and(criteriaBuilder.lessThanOrEqualTo(root.get("createdTime"), req.getEndTime())));
            }
            if (req.getSpaceId() != null) {
                list.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("videoMonitorSpot").get("space").get("id"), req.getSpaceId())));
            }
            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, pageable);

        PageBean<VideoScreenshot> videoScreenPage = new PageBean<>();
        videoScreenPage.setList(videoScreenshotPage.getContent());
        videoScreenPage.setTotalPages(videoScreenshotPage.getTotalPages());
        videoScreenPage.setTotalElements(videoScreenshotPage.getTotalElements());
        videoScreenPage.setPage(pageable.getPageNumber());
        videoScreenPage.setSize(pageable.getPageSize());
        return videoScreenPage;
    }

    @Override
    @Transactional
    public ServiceResult deleteImage(String id) {
        videoScreenshotDao.deleteById(id);
        return ServiceResult.ok("删除成功");

    }

    @Override
    public String checkoutName(String name) {
      List<VideoMonitorSpot> spotList=  videoMonitorSpotDao.findAll();
      if(CollectionUtils.isNotEmpty(spotList)){
          for(VideoMonitorSpot spot:spotList){
              if(spot.getName().equals(name)){
                  return spot.getId();
              }
          }
      }
        return null;
    }


}