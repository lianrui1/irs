package cn.com.cowain.ibms.utils;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @date 2020/7/31 12:23
 */
public interface IConst {

    /**
     * 数据库表
     */
    String TBL_ROOM_OPEN_HOURS = "ibms_room_open_hours";

    /**
     * API 路径
     */
    String API_BASE = "/ibms/api/v1";

    /**
     * API 模块名
     */
    String MODULE_EHR = "EHR对接模块";

    String MODULE_ROOM = "会议室管理模块";

    String MODULE_RESERVATION = "会议室预约模块";

    String MODULE_QUICK_APPOINTMENT_MEETING = "快速预约模块";

    String DOOR_PLATE = "门牌管理模块";

    String MODULE_QRCODE = "二维码模块";

    String MODULE_ELSE = "其他";

    String PRODUCT_MANAGE = "iot产品管理模块";

    String IOTDEVICE_MANAGE = "iot设备管理模块";

    String SPACE = "空间管理模块";

    String PROJECT = "项目管理模块";

    String AREA = "区域管理模块";

    String AUDIO = "会议录音模块";

    String DEPARTMENT = "组织部门模块";

    String DEVICE = "智能设备模块";

    String CENTRALIZED_CONTROL = "集控模块";

    String PERSON_PERMISSION = "智能设备模块";

    String DISTINGUISH = "人脸识别模块";

    String HARDWARE = "硬件设备模块";

    String MODULE_QRCODE_ACCESS_RULE = "二维码通行权限模块";

    String MODULE_OTA_UPGRADE = "OTA 应用升级模块";

    String MODULE_WEATHER = "天气模块";

    String MODULE_DOOR_MAGNETIC = "门磁模块";

    String UPLOAD_FILE = "文件上传";

    String MODULE_CUSTOM_USER_GROUP = "用户自定义会议用户组模块";

    String MODULE_INVITATION = "访客邀约模块";

    String MODULE_SMS = "短消息模块";

    String MODULE_ATTENDANCE = "会议签到模块";

    String V16 = "V1.16";

    String MODULE_LED = "LED模块";

    String MODULE_DEVICE_MODULE = "设备联动模块";

    String WORKING_MODE = "工作模式模块";

    String BIM = "bim模块";

    String V17 = "V1.17";

    String MODULE_IOTC_APPLICATION = "IOTC 应用模块";
    String MODULE_ABILITY = "能力配置";

    String MODULE_DEVICE_GROUP = "设备群组模块";

    String MODULE_TIMED_TASK = "定时任务模块";

    String MODULE_ELEVATOR_SPOT = "梯控模块";

    /**
     * 门磁模块
     **/
    String MODULE_DOOR_MAGNETIC_V2 = "门磁模块2.0";

    String V18 = "V1.18";

    String V19 = "V1.19";
    String V20 = "V1.20";
    String V21 = "V1.21";

    String V22 = "V1.22";
    String V23 = "V1.23";

    String V24 = "V1.24";
    String V25 = "V1.25";
    String V26 = "V1.26";
    String V27 = "V1.27";
    String V28 = "V1.28";

    String V29 = "V1.29";
    String V31 = "V1.31";

    String V34 = "V1.34";
    String V35 = "V1.35";

    String ACCESS_CONTROL = "通行权限模块";

    String MODULE_ACCESS_CONTROL_SPOT = "门禁点模块";

    String MODULE_ALERT_SPOT = "警戒点模块";

    String MODULE_ALERT_CAPTUREOR = "警戒抓拍模块";

    String ACCESS_CONTROL_SPOT_APPROVER = "门禁分配权限模块";

    String ACCESS_CONTROL_SPOT_APPLICATION = "门禁权限审批模块";

    String INTELLIGENT_OFFICE = "智能办公室模块";
    String INTELLIGENT_OFFICE_APPLET = "智能办公室模块-小程序";
    String MODULE_SECURITY = "智能安防";
    String MODULE_ENUM = "枚举模块";
    String INTELLIGENT_MEETING = "智能会议室模块";

    String MODULE_UC_V2 = "UC 2.0模块";

    String MODULE_WPS = "WPS对接模块";

    String MODULE_MEETING_FILE = "会议文档模块";
    String SHOW_ROOM_MODEL = "展厅-场景模式";

    String MODULE_OPEN_MEETING = "会议开放模块";

    String MODULE_MEAL = "报餐管理模块";

    String MODULE_LIFT_COLUMN = "升降柱模块";

    String MODULE_CALLBACK = "接口回调模块";

    String MODULE_AUTH = "权限模块";

    /**
     * 时间单位长度  08:00 ==> 08 2位
     */
    int TIME_STRING_LENGTH = 2;

    /**
     * 每小时60分钟
     */
    int TIME_MINUTE_MAX = 60;
    /**
     * 选择会议开放时间最小间隔 30
     */
    int TIME_MINUTE_ADD = 30;

    /**
     * 每分钟60s
     */
    int MINUTE_TO_SECOND = 60;

    /**
     * 其他
     */
    int RECENT_COUNT_REQUIRED = 10;

    int DEFAULT_PAGE_NO = 0;

    int RECENT_QUERY_PAGE_SIZE = 50;

    //纳入空间控制 1:是
    int SPACE_CONTROL = 1;

    String MEAL_OVER_END_VALUE = "1";

    String EXCEPTION = "Exception";

    //同步通道返回
    String SERVICE_ERROR = "500";

    /**
     * Redis的模块模块名
     */
    String DEVICE_MODULE = "device-iot";
    String HW_DEVICE_MODULE = "device-hw-all";
    String SPACE_ADMIN_MODULE = "space-admin";
    String SPACE_MODULE = "space";
    String SPACE_PARENT_MODULE = "space:parent";
    String OFFICE_MODULE = "office";
    String OFFICE_USER_MODULE = "office-user";
    String CENTRALIZED = "centralized";
    String CENTRALIZED_TRACE = "centralized-trace";
    String CENTRALIZED_TASK = "centralized-task";
    String CENTRALIZED_CONTROL_CODE = "centralized-control-code";
    String CENTRALIZED_CONTROL_VALID_UID = "centralized-control-valid-uid";
    String ACCESS_CONTROL_TIME_PLAN = "access-control-time-plan";
    String ROOM_MODULE = "room";
    String ROOM_USED_TIME_MODULE = "room_used_time";

    //同步通道返回
    String DOOR_MAGNETIC_MODULE = "door_magnetic";

    //同步通道返回
    String ACCESS_SPOT_MODULE = "access_spot";

    //同步通道返回
    String DOOR_PLATE_MODULE = "door_plate";

    String TRACE_ID = "traceId";
    String ACTION = "action";

    //集控属性字典
    String DICT_TARGET_POWER = "targetPower";
    String DICT_POWER = "power";
    String DICT_STATUS = "status";

    //集控属性
    String STATUS_ONLINE = "ONLINE";
    String STATUS_OFFLINE = "OFFLINE";
    String SET_POWER_ON = "on";
    String SET_POWER_OFF = "off";
    int SET_POWER_VALUE_ON = 1;
    int SET_POWER_VALUE_OFF = 0;
    //网络返回状态
    int HTTP_STATUS_SUCCESS = 1;
    int HTTP_STATUS_FAIL = 0;
    //http code
    String HTTP_CODE_SUCCESS = "0";
    String HTTP_CODE_FAIL = "-1";

    //控制模式
    String CONTROL_MODE_BATCH="batch";
    String CONTROL_MODE_SINGLE="single";

    String CONTENT_TYPE = "content-type";
    String APPLICATION = "application/octet-stream;charset=UTF-8";
    String CONTENT_DISPOSITION = "Content-Disposition";
    String CONTENT_LENGTH = "Content-Length";
}
