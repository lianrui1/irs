FROM usedtolove/openjdk11-with-arms-agent:v2

# timezone
ENV TZ Asia/Shanghai

# WORKDIR
WORKDIR /app

# copy jar
COPY ./target/*.jar app.jar

#ENV arms.licenseKey=
#ENV arms.appName=

# JAVA_OPTS
ENV JAVA_OPTS="\
 -Darms.licenseKey=ev7fuira9h@f8a36c5e2b0a587 -Darms.appName=ibms-backend \
-XX:+UseContainerSupport \
-XX:+HeapDumpOnOutOfMemoryError"

#entrypoint
ENTRYPOINT java ${JAVA_OPTS} -jar app.jar
