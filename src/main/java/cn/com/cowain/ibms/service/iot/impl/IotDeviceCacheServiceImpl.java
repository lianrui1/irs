package cn.com.cowain.ibms.service.iot.impl;

import cn.com.cowain.ibms.component.DeviceDataInfo;
import cn.com.cowain.ibms.dao.iot.DoorMagneticDao;
import cn.com.cowain.ibms.dao.iot.DoorPlateDao;
import cn.com.cowain.ibms.dao.iot.HwProductPropertiesDao;
import cn.com.cowain.ibms.dao.iot.SpaceDao;
import cn.com.cowain.ibms.entity.iot.DoorMagnetic;
import cn.com.cowain.ibms.entity.iot.DoorPlate;
import cn.com.cowain.ibms.entity.iot.HwProductProperties;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.service.iot.IotDeviceCacheService;
import cn.com.cowain.ibms.utils.DataUtils;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.ibms.utils.RedisUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Log4j2
@Service
public class IotDeviceCacheServiceImpl implements IotDeviceCacheService {
    @Autowired
    DoorMagneticDao doorMagneticDao;

    @Autowired
    private DoorPlateDao doorPlateDao;

    @Autowired
    SpaceDao spaceDao;

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    HwProductPropertiesDao hwProductPropertiesDao;

    @Override
    public void deviceRedis(IotDevice iotDevice) {

        //创建deviceKey module + deviceId + separator
        String deviceKey = redisUtil.createDeviceIdKey(IConst.HW_DEVICE_MODULE, iotDevice.getId(), RedisUtil.Separator.COLON);
//        redisUtil.del(deviceKey);
        //判断Redis中是否存在key
        if (!redisUtil.hasKey(deviceKey)) {

            //存入缓存
            Map<String, Object> map = DataUtils.targetObjectCastMap(iotDevice, IotDevice.class);
            if (iotDevice.getIotProduct() != null) {
                map.remove("iotProduct", iotDevice.getIotProduct());
            }
            if (iotDevice.getProject() != null) {
                map.remove("project", iotDevice.getProject());
            }
            if (iotDevice.getSpace() != null) {
                map.remove("space", iotDevice.getSpace());
                map.put("spaceId",iotDevice.getSpace().getId());
            }
            if (iotDevice.getLastHwStatusChangedDatetime() != null) {
                map.remove("lastHwStatusChangedDatetime", iotDevice.getLastHwStatusChangedDatetime());
            }
            log.info("device设备已存入redis，key:{}", deviceKey);
            redisUtil.hmset(deviceKey, map, true);
        }
    }

    @Override
    public Optional<DoorPlate> getDoorPlateToRedis(String doorPlateId) {
        DoorPlate doorPlate;
        //创建门牌key
        String doorPlateRedisKey = redisUtil.createDeviceRealKey(IConst.DEVICE_MODULE, String.valueOf(DeviceType.DOOR_PLATE), doorPlateId, RedisUtil.Separator.COLON);
        if(redisUtil.hasKey(doorPlateRedisKey)){
            Map<Object, Object> redisMap = redisUtil.hmget(doorPlateRedisKey);
            doorPlate = DataUtils.mapCastTargetObject(redisMap, DoorPlate.class);
            if (doorPlate == null) {
                return Optional.empty();
            }
        }else {
            Optional<DoorPlate> doorPlateOp = doorPlateDao.findById(doorPlateId);
            if (doorPlateOp.isEmpty()) {
                return Optional.empty();
            }
            doorPlate = doorPlateOp.get();
            Map<String, Object> map = DataUtils.targetObjectCastMap(doorPlate, DoorPlate.class);
            redisUtil.hmset(doorPlateRedisKey,map,true);
        }

        return Optional.of(doorPlate);
    }

    @Override
    public Optional<DoorMagnetic> getDoorMagneticToRedis(String dm) {
        DoorMagnetic doorMagnetic;
        //创建门磁key
        String doorMagneticRedisKey = redisUtil.createDeviceRealKey(IConst.DEVICE_MODULE, String.valueOf(DeviceType.DOOR_MAGNETIC), dm, RedisUtil.Separator.COLON);
        if (redisUtil.hasKey(doorMagneticRedisKey)) {
            Map<Object, Object> redisMap = redisUtil.hmget(doorMagneticRedisKey);
            doorMagnetic = DataUtils.mapCastTargetObject(redisMap, DoorMagnetic.class);
            if (doorMagnetic == null) {
                log.error("未能根据门磁编号查找到设备信息，门磁dm: {}", dm);
                return Optional.empty();
            }
        } else {
            Optional<DoorMagnetic> doorMagneticOptional = doorMagneticDao.findByNumber(dm);

            if (doorMagneticOptional.isEmpty()) {
                log.error("未能根据门磁编号查找到设备信息，门磁dm: {}", dm);
                return Optional.empty();
            }
            doorMagnetic = doorMagneticOptional.get();
            Map<String, Object> map = DataUtils.targetObjectCastMap(doorMagnetic, DoorMagnetic.class);
            redisUtil.hmset(doorMagneticRedisKey, map, false);
        }
        return Optional.of(doorMagnetic);
    }

    @Override
    public void doorPlateRedis(IotDevice iotDevice) {

        //判断是否是门牌设备
        if (DeviceType.DOOR_PLATE.equals(iotDevice.getDeviceType())) {

            //根据设备id查询门牌
            Optional<DoorPlate> doorPlate = doorPlateDao.findByDevice(iotDevice);
            if (doorPlate.isPresent()) {
                //创建门牌key
                String deviceNoKey = redisUtil.createDeviceRealKey(IConst.DEVICE_MODULE, String.valueOf(iotDevice.getDeviceType()), doorPlate.get().getId(), RedisUtil.Separator.COLON);

                //判断redis中是否存在key
                if (!redisUtil.hasKey(deviceNoKey)) {
                    setDoorPlateToRedis(deviceNoKey, doorPlate.get());

                } else {
                    redisUtil.del(deviceNoKey);
                    setDoorPlateToRedis(deviceNoKey, doorPlate.get());

                }
            }
        }
    }

    private void setDoorPlateToRedis(String deviceNoKey, DoorPlate doorPlate) {
        //存入缓存
        Map<String, Object> map = DataUtils.targetObjectCastMap(doorPlate, DoorPlate.class);
        log.info("门牌设备已存入redis，key：{}", deviceNoKey);
        redisUtil.hmset(deviceNoKey, map, true);
    }

    @Override
    public void doorMagneticRedis(IotDevice iotDevice) {

        //判断是否是门磁设备
        if (DeviceType.DOOR_MAGNETIC.equals(iotDevice.getDeviceType())) {

            //根据设备id查询门禁
            Optional<DoorMagnetic> doorMagnetic = doorMagneticDao.findByDevice(iotDevice);
            if (doorMagnetic.isPresent()) {
                //创建门磁key
                String deviceNoKey = redisUtil.createDeviceRealKey(IConst.DEVICE_MODULE, String.valueOf(iotDevice.getDeviceType()), doorMagnetic.get().getNumber(), RedisUtil.Separator.COLON);

                //判断Redis中是否存在key
                if (!redisUtil.hasKey(deviceNoKey)) {

                    //存入缓存
                    Map<String, Object> map = DataUtils.targetObjectCastMap(doorMagnetic.get(), DoorMagnetic.class);
//                    map.put("deviceId",iotDevice.getId());
                    log.info("门磁设备已存入redis，key：{}", deviceNoKey);
                    redisUtil.hmset(deviceNoKey, map, true);
                }
            }
        }
    }

    @Override
    public void redisDeviceTypeAdd(IotDevice iotDevice) {
        //创建key
        String deviceTypeListKey = redisUtil.createDeviceTypeListKey(IConst.DEVICE_MODULE, RedisUtil.Separator.COLON);
        //获取key中的value
        List<String> deviceTypes = (List<String>) redisUtil.lGet(deviceTypeListKey, 0, -1);
        if (redisUtil.hasKey(deviceTypeListKey) && deviceTypes.stream().noneMatch(deviceType -> StringUtils.equals(deviceType, iotDevice.getDeviceType().toString()))) {
            redisUtil.lSet(deviceTypeListKey, iotDevice.getDeviceType(), false);
        }
    }

    @Override
    public void redisDeviceDataInfo(IotDevice iotDevice) {
        //创建key
        String deviceIdListKey = redisUtil.createDeviceIdListKey(IConst.DEVICE_MODULE, String.valueOf(iotDevice.getDeviceType()), RedisUtil.Separator.COLON);
        DeviceDataInfo deviceDataInfo = new DeviceDataInfo();
        Optional<HwProductProperties> hwProductPropertiesOptional = hwProductPropertiesDao.findByHwProductIdAndDeviceType(iotDevice.getHwProductId(), iotDevice.getDeviceType());
        //添加hw设备id
        deviceDataInfo.setHwDeviceId(iotDevice.getHwDeviceId());
        //添加服务id
        hwProductPropertiesOptional.ifPresent(hwProductProperties -> deviceDataInfo.setServiceId(hwProductProperties.getServiceId()));
        //判断key是否存
        if (!redisUtil.hasKey(deviceIdListKey)){
            redisUtil.lSet(deviceIdListKey, deviceDataInfo, false);
        } else {
            //获取value
            List<DeviceDataInfo> deviceDataInfoList = redisUtil.lGet(deviceIdListKey, 0, -1, DeviceDataInfo.class);
            //判断value中是否存在
            if (deviceDataInfoList.stream().noneMatch(dataInfo -> StringUtils.equals(dataInfo.getHwDeviceId(), iotDevice.getHwDeviceId()))) {
                //存入缓存
                log.info("添加redis参数：deviceDataInfo={}",deviceDataInfo);
                redisUtil.lSet(deviceIdListKey, deviceDataInfo, false);
            }
        }
    }
}
