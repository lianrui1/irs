package cn.com.cowain.ibms.rest.resp;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/1/4 19:14
 */
@Data
public class RoomAllResp implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "会议室id", required = true, position = 0)
    private String id;

    @ApiModelProperty(value = "会议室名称", required = true, position = 0)
    private String name;
}
