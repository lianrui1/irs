package cn.com.cowain.ibms.service.hk;

/**
 * @Author tql
 * @Description 心跳检测
 * @Date 21-5-21 下午3:44
 * @Version 1.0
 */
public interface HealthCheckService {

    /**
     * 总部健康检查
     */
    boolean healthCheckHq();

    /**
     * 总部健康检查
     */
    boolean healthCheckNt();
}
