package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.DoorPlate;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DoorPlateDaoTest {

    @Autowired
    private DoorPlateDao doorPlateDao;
    @Autowired
    private DataFactory df;

    @Test
    public void test0(){
        Assertions.assertNotNull(doorPlateDao);
    }

    @Test
    public void test1Save(){
        DoorPlate doorPlate = new DoorPlate();
        doorPlate.setName(df.getName());
        doorPlateDao.save(doorPlate);
    }

}
