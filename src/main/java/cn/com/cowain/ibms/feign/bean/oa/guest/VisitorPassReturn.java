package cn.com.cowain.ibms.feign.bean.oa.guest;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 访客通行回调参数
 *
 * @author Yang.Lee
 * @date 2022/4/26 14:56
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VisitorPassReturn {

    @ApiModelProperty(value = "工号")
    private String hcCardNo;

    @ApiModelProperty(value = "出入标识；1 出；2 入")
    private int state;

    @ApiModelProperty(value = "结果；1 成功")
    private int result;

    @ApiModelProperty(value = "公司编号。详见AccessControlSpotPlace枚举ehrId字段")
    private int company;
}
