package cn.com.cowain.ibms.service.bean.hw;

import lombok.Data;

/**
 * 空调属性
 *
 * @author: yanzy
 * @date: 2022/8/16 13:20
 */
@Data
public class HwAirProperties {

    //下发开关
    private String setPower;

    //下发模式
    private String setMode;

    //下发风速
    private String setFanSpeed;

    //下发温度
    private String setTemp;

    //读取开关及模式状态
    private String readPowerAndMode;

    //读取风速
    private String readFanSpeed;

    //读取温度
    private String readTemp;

    //读取网关在线状态
    private String readState;

}
