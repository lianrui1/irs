package cn.com.cowain.ibms.component;

import cn.com.cowain.ibms.service.bean.WeChatMsg;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/14/20
 */
@Slf4j
@Component
public class WeChatSendMsgUtils {

    @Autowired
    private WxMpService wxMpService;

    /**
     * 只支持 3 行的微信模板
     *
     * @param templateId 模板id
     * @param url        跳转url
     * @param toUserWxId 目标wxId
     * @param weChatMsg  信息数组/集合
     */
    public String sendMsg3Lines(String templateId, String url, String toUserWxId, WeChatMsg weChatMsg) {
        log.debug("sendMsg3Lines()...");
        WxMpTemplateMessage templateMessage = WxMpTemplateMessage.builder()
                .toUser(toUserWxId)
                .url(url)
                .templateId(templateId)
                .build();
        String msgId = "";
        templateMessage.addData(new WxMpTemplateData("first", weChatMsg.getFirst()));
        templateMessage.addData(new WxMpTemplateData("keyword1", weChatMsg.getKeyword1()));
        templateMessage.addData(new WxMpTemplateData("keyword2", weChatMsg.getKeyword2()));
        templateMessage.addData(new WxMpTemplateData("keyword3", weChatMsg.getKeyword3()));
        templateMessage.addData(new WxMpTemplateData("keyword4", weChatMsg.getKetword4()));
        templateMessage.addData(new WxMpTemplateData("remark", weChatMsg.getRemark()));

        //推送微信消息
        try {
            msgId = wxMpService.getTemplateMsgService().sendTemplateMsg(templateMessage);
        } catch (WxErrorException e) {
            //可能会有员工未关注公众号的异常
            log.error("员工未关注 错误代码:"+e.getMessage());
        }
        return msgId;
    }

    public void sendDeviceStatusChangeMsgPush(String templateId, String openId, WeChatMsg weChatMsg) {
        WxMpTemplateMessage templateMessage = WxMpTemplateMessage.builder()
                .toUser(openId)
                .templateId(templateId)
                .build();
        String msgId = "";
        templateMessage.addData(new WxMpTemplateData("keyword1", weChatMsg.getKeyword1()));
        templateMessage.addData(new WxMpTemplateData("keyword2", weChatMsg.getKeyword2()));
        templateMessage.addData(new WxMpTemplateData("keyword3", weChatMsg.getKeyword3()));
        templateMessage.addData(new WxMpTemplateData("remark", weChatMsg.getRemark()));

        //推送微信消息
        try {
            msgId = wxMpService.getTemplateMsgService().sendTemplateMsg(templateMessage);
        } catch (WxErrorException e) {
            //可能会有员工未关注公众号的异常
            log.error("员工未关注 错误代码:"+e.getMessage());
        }
        log.error(msgId);
    }
}
