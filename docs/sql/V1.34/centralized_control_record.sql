/*
 Navicat Premium Data Transfer

 Source Server         : 172.18.16.32_13306
 Source Server Type    : MySQL
 Source Server Version : 50735
 Source Host           : 172.18.16.32:13306
 Source Schema         : ibms_dev_1.34

 Target Server Type    : MySQL
 Target Server Version : 50735
 File Encoding         : 65001

 Date: 23/09/2022 14:09:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for centralized_control_record
-- ----------------------------
DROP TABLE IF EXISTS `centralized_control_record`;
CREATE TABLE `centralized_control_record`  (
    `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
    `created_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
    `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
    `updated_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
    `updated_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `version` int(11) NULL DEFAULT NULL COMMENT '版本号',
    `control_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '操作名称',
    `control_time` datetime(0) NULL DEFAULT NULL COMMENT '控制时间',
    `hr_id` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '人员工号',
    `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '人员名称',
    `status` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '状态',
    `type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '人员类型',
    `trace_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'traceId',
    `centralized_control_code_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '集控码ID',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `centralized_control_record_trace_id_index`(`trace_id`) USING BTREE,
    INDEX `centralized_control_record_control_time_index`(`control_time`) USING BTREE,
    INDEX `centralized_control_record_hr_id_index`(`hr_id`) USING BTREE,
    INDEX `centralized_control_record_name_index`(`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '集控控制记录' ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;

-----------------------------------------------------------------------------------------------------------------------------------------------------------


SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for centralized_control_record
-- ----------------------------
DROP TABLE IF EXISTS `centralized_control_record`;
CREATE TABLE `centralized_control_record`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `created_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `version` int(11) NULL DEFAULT NULL COMMENT '版本号',
  `control_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '操作名称',
  `control_time` datetime(0) NULL DEFAULT NULL COMMENT '控制时间',
  `hr_id` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '人员工号',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '人员名称',
  `status` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '状态',
  `type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '人员类型',
  `trace_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'traceId',
  `centralized_control_code_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '集控码ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '集控控制记录' ROW_FORMAT = Dynamic;
create index centralized_control_record_trace_id_index
    on centralized_control_record (trace_id);
SET FOREIGN_KEY_CHECKS = 1;
