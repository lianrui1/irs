package cn.com.cowain.ibms.dao.space;

import cn.com.cowain.ibms.entity.space.ProjectArea;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * 项目区域
 *
 * @author Yang.Lee
 * @date 2020/12/21 10:22
 */
@Repository
public interface ProjectAreaDao extends JpaRepository<ProjectArea, String>, JpaSpecificationExecutor<ProjectArea> {

    /**
     * 根据姓名查询
     *
     * @param name 区域名称
     * @return 查询结果
     * @author Yang.Lee
     * @date 2021/11/9 9:35
     **/
    Optional<ProjectArea> findByName(String name);
}
