package cn.com.cowain.ibms.rest.req.iot.centralized;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * 华为云批量下发命令
 *
 * @author wei.cheng
 * @date 2022/09/21 14:11
 */
@Data
public class SingleCommandReq {
    /**
     * 跟踪连ID
     */
    @NotEmpty
    private String traceId;
    /**
     * 创建的批量下发任务列表
     */
    private CommandTemplate commandTemplate;

    private PropertiesTemplate propertiesTemplate;


}
