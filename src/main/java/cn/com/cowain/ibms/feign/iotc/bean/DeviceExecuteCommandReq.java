package cn.com.cowain.ibms.feign.iotc.bean;

import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Data;

/**
 * @author wei.cheng
 * @date 2022/03/31 09:42
 */
@Data
@Builder
@ApiModel(value = "设备控制-执行模版指令")
public class DeviceExecuteCommandReq {
    /**
     * 主设备设备号
     */
    private String deviceCode;
    /**
     * 执行指令，json格式
     */
    private String jsonArg;
}
