package cn.com.cowain.ibms.rest.resp;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author feng
 * @title: NodeSit 监控点列表
 * @projectName bims
 * @Date 2021/11/18 11:01
 */
@Data
public class NodeSitResp {
    /** 当前节点名称 */
    @ApiModelProperty("当前节点名称")
    String name;
    /** 当前节点深度 */
    @ApiModelProperty("当前节点深度")
    Integer currentNode;
    /** 子节点中的监视器 */
    @ApiModelProperty("子节点中的监视器")
    List<MonitorSitResp> sitList;
    /** 是否是最终子节点 */
    @ApiModelProperty("子节点列表")
    Boolean  isLastNode;
    /** 子节点 列表 */
    @ApiModelProperty("子节点列表")
    List<NodeSitResp> brotherSit;

}
