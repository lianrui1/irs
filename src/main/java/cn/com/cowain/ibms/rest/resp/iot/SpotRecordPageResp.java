package cn.com.cowain.ibms.rest.resp.iot;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/12/3 15:48
 */
@Data
@ApiModel("门禁点人员通行记录返回对象")
public class SpotRecordPageResp {

    @ApiModelProperty(value = "人员姓名")
    private String name;

    @ApiModelProperty(value = "人员工号")
    private String empNo;

    @ApiModelProperty(value = "人员部门")
    private String dept;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "通行时间")
    private LocalDateTime passTime;

    @ApiModelProperty(value = "状态")
    private String status;

    @ApiModelProperty(value = "抓拍图片", required = true, example = "http://xx/x/x/xx/picture.jpg", position = 3)
    private String capturePicture;

    @ApiModelProperty(value = "底库图片", required = true, example = "http://xx/x/x/xx/picture.jpg", position = 4)
    private String defaultPicture;

    @ApiModelProperty(value = "体温。",  example = "36.5/未检测", position = 8)
    private String temperature;
}
