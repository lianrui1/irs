package cn.com.cowain.ibms.service.office.impl;

import cn.com.cowain.ibms.dao.device.DeviceModeStatusDao;
import cn.com.cowain.ibms.dao.device.DeviceWorkingModeDao;
import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.dao.iot.SpaceDao;
import cn.com.cowain.ibms.dao.office.*;
import cn.com.cowain.ibms.dao.space.ProjectDao;
import cn.com.cowain.ibms.dao.space.SpaceAdminDao;
import cn.com.cowain.ibms.dao.space.SpaceDeviceControlDao;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.office.*;
import cn.com.cowain.ibms.entity.space.Project;
import cn.com.cowain.ibms.entity.space.SpaceAdmin;
import cn.com.cowain.ibms.entity.space.SpaceDeviceControl;
import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import cn.com.cowain.ibms.enumeration.iot.IntelligentOfficeStatus;
import cn.com.cowain.ibms.enumeration.iot.UsableStatus;
import cn.com.cowain.ibms.feign.iotc.bean.DefaultCreateRuleChainReq;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.Sorts;
import cn.com.cowain.ibms.rest.req.office.*;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.device.SensorDeviceResp;
import cn.com.cowain.ibms.rest.resp.iot.DeviceWechatResp;
import cn.com.cowain.ibms.rest.resp.office.IntelligentOfficeH5Resp;
import cn.com.cowain.ibms.rest.resp.office.IntelligentOfficePageResp;
import cn.com.cowain.ibms.rest.resp.office.SpaceAdminResp;
import cn.com.cowain.ibms.rest.resp.office.UserOfficeResp;
import cn.com.cowain.ibms.service.bean.ServiceMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.office.IOfficeRedisService;
import cn.com.cowain.ibms.service.office.IntelligentOfficeDeviceService;
import cn.com.cowain.ibms.service.office.IntelligentOfficeRuleEngineService;
import cn.com.cowain.ibms.service.office.IntelligentOfficeService;
import cn.com.cowain.ibms.service.space.SpaceService;
import cn.com.cowain.ibms.service.workingmode.WorkingModeService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.TokenUtils;
import cn.com.cowain.sfp.comm.utils.JwtUtil;
import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import java.util.*;
import java.util.stream.Collectors;


/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/10 15:27
 */
@Slf4j
@Service
public class IntelligentOfficeServiceImpl implements IntelligentOfficeService {

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private SpaceDao spaceDao;

    @Autowired
    private SpaceAdminDao spaceAdminDao;

    @Autowired
    private SpaceDeviceControlDao spaceDeviceControlDao;

    @Autowired
    private IntelligentOfficeDao officeDao;

    @Autowired
    private DeviceWorkingModeDao workingModeDao;

    @Autowired
    private IntelligentOfficeDeviceDao intelligentOfficeDeviceDao;

    @Autowired
    private SpaceService spaceService;

    @Autowired
    private WorkingModeService workingModeService;

    @Autowired
    private DeviceModeStatusDao modeStatusDao;

    @Autowired
    private IntelligentOfficeUserDao officeUserDao;

    @Autowired
    private IntelligentOfficeInitializationDao officeInitializationDao;

    @Resource
    private IntelligentOfficeDefaultRuleEngineDao intelligentOfficeDefaultRuleEngineDao;

    @Resource
    private IntelligentOfficeRuleEngineService intelligentOfficeRuleEngineService;

    @Autowired
    private IntelligentOfficeDeviceService intelligentOfficeDeviceService;

    @Resource
    private DeviceDao iotDeviceDao;

    @Resource
    private IntelligentOfficeRuleEngineDao intelligentOfficeRuleEngineDao;

    @Autowired
    private IntelligentOfficeDao intelligentOfficeDao;


    @Value("${cowain.office.url}")
    private String officeUrl;

    @Autowired
    private IOfficeRedisService iOfficeRedisService;

    @Override
    @Transactional
    public ServiceResult create(IntelligentOfficeCreateReq req) {

        // 根据空间ID获取空间
        Optional<Space> spaceOp = spaceDao.findById(req.getSpaceId());
        if (spaceOp.isEmpty()) {
            return ServiceResult.error("id有误,空间不存在");
        }

        // 根据空间ID获取管理员列表
        List<SpaceAdmin> spaceAdminList = spaceAdminDao.findBySpaceId(req.getSpaceId());
        if (spaceAdminList.isEmpty()) {
            return ServiceResult.error("该空间无管理人员，不可创建；请在该空间添加管理人员");
        }

        // 判断空间下是否已创建会议室
        Optional<IntelligentOffice> intelligentOfficeOp = officeDao.findBySpaceId(req.getSpaceId());
        if (intelligentOfficeOp.isPresent()) {
            return ServiceResult.error("该空间下已创建会议室,请勿重复创建");
        }

        // 查询办公室是否存在
        Optional<IntelligentOffice> officeOp = officeDao.findByName(req.getName());
        if (officeOp.isPresent()) {
            return ServiceResult.error("办公室已存在,请重新创建");
        }

        // 保存智能办公室
        IntelligentOffice office = new IntelligentOffice();
        office.setName(req.getName());
        office.setSpace(spaceOp.get());
        office.setStatus(req.getStatus());
        officeDao.save(office);
        iOfficeRedisService.setIntelligentOfficeRedis(office);

        //查询默认规则表
        List<IntelligentOfficeDefaultRuleEngine> defaultRuleEngineList = intelligentOfficeDefaultRuleEngineDao.findAll();

        //循环默认规则
        for (IntelligentOfficeDefaultRuleEngine intelligentOfficeDefaultRuleEngine : defaultRuleEngineList) {
            //根据默认规则创建新规则,并写入规则表
            //创建新建规则请求参数
            DefaultCreateRuleChainReq detailReq = new DefaultCreateRuleChainReq();

            //空间id
            detailReq.setSpaceId(office.getSpace().getId());

            //办公室id
            detailReq.setOfficeId(office.getId());
            //规则Loge
            detailReq.setImgUrl(intelligentOfficeDefaultRuleEngine.getLogo());
            //规则名称
            detailReq.setName(intelligentOfficeDefaultRuleEngine.getName());
            //规则条件关系
            detailReq.setTriggerCondition(intelligentOfficeDefaultRuleEngine.getTriggerCondition());

            //创建 触发条件 请求参数所需list
            List<RuleEngIneDetailTriggerReq> ruleEngIneDetailTriggerReqs = new ArrayList<>();

            //创建 执行任务 请求参数所需list
            List<RuleEngIneDetailActuatorReq> ruleEngIneDetailActuatorReqList = new ArrayList<>();

            List<IotDevice> allBySpaceId = iotDeviceDao.findAllBySpaceId(office.getSpace().getId());

            List<DefaultRuleEngineDetailTriggerReq> defaultRuleEngineDetailTriggerReqs = JSON.parseArray(intelligentOfficeDefaultRuleEngine.getTrigger(), DefaultRuleEngineDetailTriggerReq.class);

            RuleEngIneDetailTriggerReq ruleEngIneDetailTriggerReq = new RuleEngIneDetailTriggerReq();
            for (IotDevice iotDevice : allBySpaceId) {

                for (DefaultRuleEngineDetailTriggerReq defaultRuleEngineDetailTriggerReq : defaultRuleEngineDetailTriggerReqs) {
                    RuleEngIneDetailTriggerReq ruleEngIneDetailTriggerReq1 = new RuleEngIneDetailTriggerReq();
                    if (iotDevice.getDeviceType() != null && defaultRuleEngineDetailTriggerReq.getDeviceType() != null && defaultRuleEngineDetailTriggerReq.getDeviceType().equals(iotDevice.getDeviceType())) {

                        ruleEngIneDetailTriggerReq1.setDeviceId(iotDevice.getId());
                        ruleEngIneDetailTriggerReq1.setSensorReqs(defaultRuleEngineDetailTriggerReq.getSensorReqs());
                        ruleEngIneDetailTriggerReqs.add(ruleEngIneDetailTriggerReq1);

                    }
                    ruleEngIneDetailTriggerReq.setTiming(defaultRuleEngineDetailTriggerReq.getTiming());

                }

            }
            if (ruleEngIneDetailTriggerReq.getTiming() != null || ruleEngIneDetailTriggerReq.getDeviceId() != null || ruleEngIneDetailTriggerReq.getSensorReqs() != null) {
                ruleEngIneDetailTriggerReqs.add(ruleEngIneDetailTriggerReq);
            }
            detailReq.setTriggerReqs(ruleEngIneDetailTriggerReqs);

            List<RuleEngIneDetailActuatorReq> ruleEngIneDetailActuatorReqs = JSON.parseArray(intelligentOfficeDefaultRuleEngine.getActuator(), RuleEngIneDetailActuatorReq.class);
            for (IotDevice iotDevice : allBySpaceId) {
                RuleEngIneDetailActuatorReq ruleEngIneDetailActuatorReq1 = new RuleEngIneDetailActuatorReq();
                for (RuleEngIneDetailActuatorReq ruleEngIneDetailActuatorReq : ruleEngIneDetailActuatorReqs) {
                    if (iotDevice.getDeviceType() != null && ruleEngIneDetailActuatorReq.getDeviceType() != null && ruleEngIneDetailActuatorReq.getDeviceType().equals(iotDevice.getDeviceType())) {
                        ruleEngIneDetailActuatorReq1.setDeviceId(iotDevice.getId());

                        ruleEngIneDetailActuatorReq1.setStatus(ruleEngIneDetailActuatorReq.getStatus());
                        ruleEngIneDetailActuatorReq1.setAction(ruleEngIneDetailActuatorReq.getAction());
                        ruleEngIneDetailActuatorReq1.setSetPower(ruleEngIneDetailActuatorReq.getSetPower());
                        ruleEngIneDetailActuatorReq1.setSetTemp(ruleEngIneDetailActuatorReq.getSetTemp());
                        ruleEngIneDetailActuatorReq1.setSetMode(ruleEngIneDetailActuatorReq.getSetMode());
                        ruleEngIneDetailActuatorReq1.setSetFanSpeed(ruleEngIneDetailActuatorReq.getSetFanSpeed());
                        ruleEngIneDetailActuatorReq1.setSetFanDirection(ruleEngIneDetailActuatorReq.getSetFanDirection());
                        ruleEngIneDetailActuatorReq1.setSetFanAuto(ruleEngIneDetailActuatorReq.getSetFanAuto());
                        ruleEngIneDetailActuatorReq1.setSetSwing(ruleEngIneDetailActuatorReq.getSetSwing());
                        ruleEngIneDetailActuatorReq1.setLight1(ruleEngIneDetailActuatorReq.getLight1());
                        ruleEngIneDetailActuatorReq1.setLight2(ruleEngIneDetailActuatorReq.getLight2());
                        ruleEngIneDetailActuatorReq1.setLight3(ruleEngIneDetailActuatorReq.getLight3());
                        ruleEngIneDetailActuatorReq1.setLight4(ruleEngIneDetailActuatorReq.getLight4());
                        ruleEngIneDetailActuatorReqList.add(ruleEngIneDetailActuatorReq1);

                    }
                }
                detailReq.setActuatorReqs(ruleEngIneDetailActuatorReqList);
            }

            //规则是否启用
            detailReq.setUsableStatus(UsableStatus.DISABLED);
            //生效方式
            detailReq.setEffectiveTypeReq(JSON.parseObject(intelligentOfficeDefaultRuleEngine.getEffectiveType(), RuleEngineDetailEffectiveTypeReq.class));
            log.info("detailReqAll:{},", detailReq);
            if (CollUtil.isNotEmpty(detailReq.getTriggerReqs()) && CollUtil.isNotEmpty(detailReq.getActuatorReqs())) {
                log.info("detailReq:{},", detailReq);
                //调用 联动规则创建接口
                intelligentOfficeRuleEngineService.pcCreatRuleEngine(detailReq);
            }

        }
        return ServiceResult.ok(new IdResp(office.getId()));
    }

    @Override
    @Transactional
    public ServiceResult updateStatus(String id, IntelligentOfficeStatus status) {

        Optional<IntelligentOffice> officeOp = officeDao.findById(id);
        if (officeOp.isEmpty()) {
            return ServiceResult.error("id有误,办公室不存在");
        }
        Sort sort = Sort.by(Sort.Direction.DESC, "createdTime");
        Pageable pageable = PageRequest.of(0, 999, sort);

        // 更改智能办公室状态
        IntelligentOffice office = officeOp.get();
        office.setStatus(status);
        officeDao.save(office);
        iOfficeRedisService.setIntelligentOfficeRedis(office);

        // 获取拥有入口权限人员
        List<String> empNos = new ArrayList<>();
        List<SpaceDeviceControl> deviceControlList = spaceDeviceControlDao.findByDeviceSpaceIdOrderByCreatedTimeDesc(office.getSpace().getId());
        deviceControlList = deviceControlList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(SpaceDeviceControl::getUserEmpNo))), ArrayList::new));
        deviceControlList.forEach(it -> empNos.add(it.getUserEmpNo()));

        // 根据更新状态添加或删除办公室入口权限
        if (!IntelligentOfficeStatus.ENABLE.equals(status)) {
            try {

                // 判断人员是否还拥有办公室
                List<String> strings = new ArrayList<>();
                empNos.forEach(it -> {
                    List<String> spaceIds = new ArrayList<>();
                    List<SpaceDeviceControl> byUserEmpNoList = spaceDeviceControlDao.findByUserEmpNo(it);
                    byUserEmpNoList.forEach(i -> spaceIds.add(i.getDevice().getSpace().getId()));
                    Page<IntelligentOffice> officePage = officeDao.findBySpaceIdInAndStatus(spaceIds, IntelligentOfficeStatus.ENABLE, pageable);
                    if (officePage.getContent().isEmpty()) {
                        strings.add(it);
                    }
                });

            } catch (Exception e) {
                log.error(e.getMessage());
                // 业务处理失败，手动回滚事务,返回异常
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ServiceResult.error("向uc删除权限失败:");
            }
        }

        return ServiceResult.ok();
    }

    @Override
    public ServiceResult delete(String id) {

        Optional<IntelligentOffice> officeOp = officeDao.findById(id);
        if (officeOp.isEmpty()) {
            return ServiceResult.error("ID有误,删除失败");
        }
        IntelligentOffice office = officeOp.get();

        if (IntelligentOfficeStatus.ENABLE.equals(office.getStatus())) {
            return ServiceResult.error("该办公室启用中,不可删除");
        }
        Sort sort = Sort.by(Sort.Direction.DESC, "controlTime");
        Pageable pageable = PageRequest.of(0, 999, sort);
        Page<DeviceWorkingMode> workingModePage = workingModeDao.findBySpaceId(office.getSpace().getId(), pageable);
        workingModePage.getContent().forEach(workingMode -> {
            List<DeviceModeStatus> modeStatusList = modeStatusDao.findByModeId(workingMode.getId());
            modeStatusList.forEach(modeStatus -> {
                // 删除模式设备
                modeStatus.setIsDelete(1);
                modeStatusDao.save(modeStatus);
            });

            // 删除模式
            workingMode.setIsDelete(1);
            workingModeDao.save(workingMode);
        });

        // 删除办公室人员权限
        List<IntelligentOfficeUser> officeUserList = officeUserDao.findByIntelligentOfficeId(id);
        officeUserList.forEach(it -> it.setIsDelete(1));
        officeUserDao.saveAll(officeUserList);

        // 删除办公室初始化记录
        List<IntelligentOfficeInitialization> officeInitializationList = officeInitializationDao.findByIntelligentOfficeId(id);
        officeInitializationList.forEach(it -> it.setIsDelete(1));
        officeInitializationDao.saveAll(officeInitializationList);
        officeInitializationList.forEach(o -> iOfficeRedisService.delIntelligentOfficeInitRedis(o));
        // 删除办公室下设备
        List<IntelligentOfficeDevice> waitDeleteList = intelligentOfficeDeviceDao.findByIntelligentOfficeId(office.getId());
        if (CollectionUtils.isNotEmpty(waitDeleteList)) {
            for (IntelligentOfficeDevice device : waitDeleteList) {
                device.setIsDelete(1);
            }
            intelligentOfficeDeviceDao.saveAll(waitDeleteList);
        }

        // 删除办事下所有联动规则
        List<IntelligentOfficeRuleEngine> byIdAllRuleEngine = intelligentOfficeRuleEngineDao.findAllByIntelligentOfficeId(id);
        for (IntelligentOfficeRuleEngine intelligentOfficeRuleEngine : byIdAllRuleEngine) {
            intelligentOfficeRuleEngineService.pcDeleteRuleEngine(intelligentOfficeRuleEngine.getId());
        }


        office.setIsDelete(1);
        officeDao.save(office);
        iOfficeRedisService.delIntelligentOfficeRedis(office);
        return ServiceResult.ok();
    }

    @Override
    public PageBean<IntelligentOfficePageResp> page(IntelligentOfficePageReq req) {


        // 获取空间管理员
        List<SpaceAdmin> spaceAdminList = spaceAdminDao.findAll((root, criteriaQuery, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();

            // 根据权限所属人姓名工号模糊查
            if (StringUtils.isNotBlank(req.getKeyword())) {
                String like = "%" + req.getKeyword() + "%";
                list.add(criteriaBuilder.and(criteriaBuilder.or(criteriaBuilder.like(root.get("adminEmpNo"), like), criteriaBuilder.like(root.get("adminName"), like))));
            }

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        });

        // 空间ID
        List<String> spaceIds = new ArrayList<>();
        spaceAdminList.forEach(it -> spaceIds.add(it.getSpace().getId()));


        // 查询智能办公列表分页
        Sort sort = Sort.by(Sort.Direction.DESC, Sorts.CREATED_TIME);
        Pageable pageable = PageRequest.of(req.getPage(), req.getSize(), sort);

        Page<IntelligentOffice> officePage = officeDao.findAll((root, criteriaQuery, criteriaBuilder) -> {

            //封装and语句
            List<Predicate> listAnd = new ArrayList<>();
            // 根据项目ID获取
            if (StringUtils.isNotBlank(req.getProjectId())) {
                listAnd.add(criteriaBuilder.equal(root.get("space").get("project").get("id"), req.getProjectId()));
            }
            Predicate[] array_and = new Predicate[listAnd.size()];
            Predicate Pre_And = criteriaBuilder.and(listAnd.toArray(array_and));


            //封装or语句
            List<Predicate> listOr = new ArrayList<>();

            // 根据办公室名称模糊查
            if (StringUtils.isNotBlank(req.getKeyword())) {
                String like = "%" + req.getKeyword() + "%";
                listOr.add(criteriaBuilder.like(root.get("name"), like));
            }

            if (!spaceIds.isEmpty()) {
                CriteriaBuilder.In<String> inInitiator = criteriaBuilder.in(root.get("space").get("id"));
                spaceIds.forEach(spaceId -> inInitiator.value(spaceId));
                listOr.add(inInitiator);
            }
            Predicate[] arrayOr = new Predicate[listOr.size()];
            Predicate Pre_Or = criteriaBuilder.or(listOr.toArray(arrayOr));

            return criteriaQuery.where(Pre_And, Pre_Or).getRestriction();
        }, pageable);

        List<IntelligentOfficePageResp> officeList = new ArrayList<>();
        officePage.getContent().forEach(it -> {
            IntelligentOfficePageResp office = new IntelligentOfficePageResp();
            office.setId(it.getId());
            office.setName(it.getName());
            office.setSpaceId(it.getSpace().getId());
            office.setSpaceNumber(it.getSpace().getNumber());
            office.setCreateTime(it.getCreatedTime());
            office.setStatus(it.getStatus());
            office.setStatusName(it.getStatus().getName());
            // 所在空间位置
            String projectName = "";
            Optional<Project> projectOp = projectDao.findById(it.getSpace().getProject().getId());
            if (projectOp.isPresent()) {
                projectName = projectOp.get().getProjectName();
            }
            String fullSpaceName = SpaceService.getFullSpaceName(it.getSpace(), it.getSpace().getName());
            office.setSpaceAddress(projectName + "/" + fullSpaceName);

            // 办公室所属人
            List<SpaceAdminResp> adminList = new ArrayList<>();
            spaceAdminDao.findBySpaceId(it.getSpace().getId()).forEach(admin -> {
                SpaceAdminResp adminResp = new SpaceAdminResp();
                adminResp.setAdminEmpNo(admin.getAdminEmpNo());
                if (StringUtils.isBlank(adminResp.getAdminEmpNo())) {
                    adminResp.setAdminEmpNo("");
                }
                adminResp.setAdminName(admin.getAdminName());
                if (StringUtils.isBlank(adminResp.getAdminName())) {
                    adminResp.setAdminName("");
                }
                adminResp.setAdminDept(admin.getAdminDept());
                if (StringUtils.isBlank(adminResp.getAdminDept())) {
                    adminResp.setAdminDept("");
                }
                adminList.add(adminResp);
            });
            office.setSpaceAdminList(adminList);
            officeList.add(office);
        });

        PageBean<IntelligentOfficePageResp> officeRespPage = new PageBean<>();
        officeRespPage.setList(officeList);
        officeRespPage.setTotalPages(officePage.getTotalPages());
        officeRespPage.setTotalElements(officePage.getTotalElements());
        officeRespPage.setPage(pageable.getPageNumber());
        officeRespPage.setSize(pageable.getPageSize());

        return officeRespPage;
    }

    @Override
    public ServiceResult detail(String id) {

        // 判断办公室是否存在
        Optional<IntelligentOffice> officeOp = officeDao.findById(id);
        if (officeOp.isEmpty()) {
            return ServiceResult.error("查询失败,办公室不存在");
        }
        IntelligentOffice office = officeOp.get();

        // 返回对象
        IntelligentOfficePageResp officeResp = new IntelligentOfficePageResp();
        officeResp.setId(office.getId());
        officeResp.setName(office.getName());
        officeResp.setSpaceId(office.getSpace().getId());
        officeResp.setSpaceNumber(office.getSpace().getNumber());
        // 空间位置
        String fullSpaceName = SpaceService.getFullSpaceName(office.getSpace(), office.getSpace().getName());
        officeResp.setSpaceAddress(office.getSpace().getProject().getProjectName() + "/" + fullSpaceName);
        // 空间管理员
        List<SpaceAdmin> spaceAdmins = spaceAdminDao.findBySpaceId(office.getSpace().getId());
        List<SpaceAdminResp> spaceAdminList = new ArrayList<>();
        spaceAdmins.forEach(it -> {
            SpaceAdminResp spaceAdminResp = new SpaceAdminResp();
            spaceAdminResp.setAdminEmpNo(it.getAdminEmpNo());
            spaceAdminResp.setAdminName(it.getAdminName());
            spaceAdminResp.setAdminDept(it.getAdminDept());
            spaceAdminList.add(spaceAdminResp);
        });
        officeResp.setSpaceAdminList(spaceAdminList);
        officeResp.setCreateTime(office.getCreatedTime());
        officeResp.setStatus(office.getStatus());
        officeResp.setStatusName(office.getStatus().getName());
        officeResp.setQrCodeUrl(officeUrl + office.getSpace().getId());

        return ServiceResult.ok(officeResp);
    }

    @Override
    public ServiceResult detailH5(String id) {

        Optional<Space> spaceOp = spaceDao.findById(id);
        if (spaceOp.isEmpty()) {
            return ServiceResult.error("空间不存在");
        }
        Space space = spaceOp.get();

        // 办公室预览返回对象
        IntelligentOfficeH5Resp resp = new IntelligentOfficeH5Resp();
        resp.setSpaceName(space.getName());
        resp.setProjectName(space.getProject().getProjectName());


        // 工作模式列表
        Sort sort = Sort.by(Sort.Direction.DESC, "controlTime");
        Pageable pageable = PageRequest.of(0, 3, sort);
        resp.setWorkingModeList(workingModeService.getPage(id, pageable).getList());

        // 可控制设备列表
        ServiceResult deviceListControlResult = spaceService.getDeviceListControl(id);
        if (!deviceListControlResult.isSuccess()) {
            resp.setDeviceList(new ArrayList<>());
        } else {
            resp.setDeviceList((List<DeviceWechatResp>) deviceListControlResult.getObject());
        }

        // 温湿度光 空气 噪音
        /*String luminance = "";
        String temperature = "";
        String humidity = "";
        String pm10 = "";
        String pm2_5 = "";
        String noise = "";*/

        List<SensorDeviceResp> sensorList = new ArrayList<>();
        ServiceResult deviceListResult = spaceService.getDeviceList(id);
        if (deviceListResult.isSuccess()) {
            List<DeviceWechatResp> deviceList = (List<DeviceWechatResp>) deviceListResult.getObject();
            for (DeviceWechatResp device : deviceList) {
                if (DeviceType.NOISE_SENSOR.equals(device.getType()) || DeviceType.PM_SENSOR.equals(device.getType()) || DeviceType.TEMPERATURE_AND_HUMIDITY_AND_ILLUMINATION_SENSOR.equals(device.getType())) {
                    SensorDeviceResp sensorDeviceResp = new SensorDeviceResp();
                    sensorDeviceResp.setHwDeviceId(device.getDeviceId());
                    sensorDeviceResp.setDeviceType(device.getType());
                    sensorList.add(sensorDeviceResp);
                }

                /*// 噪音
                if(DeviceType.NOISE_SENSOR.equals(device.getType())){
                    String tbId = productsService.findTbId(device.getDeviceId());
                    if (tbId != null) {
                        List<TbDataResp> data = productsService.findNowData(tbId);
                        if(data.size() > 0){
                            noise = data.get(0).getNoise();
                        }
                    }
                }

                // 空气质量
                if(DeviceType.PM_SENSOR.equals(device.getType())){
                    String tbId = productsService.findTbId(device.getDeviceId());
                    if (tbId != null) {
                        List<TbDataResp> data = productsService.findNowData(tbId);
                        if(data.size() > 0){
                            pm2_5 = data.get(0).getPm2_5();
                            pm10 = data.get(0).getPm10();
                        }
                    }
                }

                // 温湿度光
                if(DeviceType.TEMPERATURE_AND_HUMIDITY_AND_ILLUMINATION_SENSOR.equals(device.getType())){
                    String tbId = productsService.findTbId(device.getDeviceId());
                    if (tbId != null) {
                        List<TbDataResp> data = productsService.findNowData(tbId);
                        if(data.size() > 0){
                            temperature = data.get(0).getTemperature();
                            humidity = data.get(0).getHumidity();
                            luminance = data.get(0).getLuminance();
                        }
                    }
                }*/
            }
        }

        /*TbDataResp tbDataResp = new TbDataResp();
        tbDataResp.setPm10(pm10);
        tbDataResp.setPm2_5(pm2_5);
        tbDataResp.setNoise(noise);
        tbDataResp.setTemperature(temperature);
        tbDataResp.setHumidity(humidity);
        tbDataResp.setLuminance(luminance);
        resp.setTbDataResp(tbDataResp);*/
        resp.setSensorList(sensorList);


        return ServiceResult.ok(resp);
    }

    @Override
    public PageBean<UserOfficeResp> userOfficePage(int page, int size) {

        // 获取工号
        String token = TokenUtils.getToken();
        String hrId = JwtUtil.getHrId(token);

        List<Sort.Order> orders = new ArrayList<>();
        orders.add(new Sort.Order(Sort.Direction.ASC, "spaceLevel"));
        orders.add(new Sort.Order(Sort.Direction.ASC, "spaceCreatedTime"));
        Sort sort = Sort.by(orders);
        Pageable pageable = PageRequest.of(page, size, sort);

        // 获取员工所在空间
        List<SpaceDeviceControl> spaceDeviceControlList = spaceDeviceControlDao.findByUserEmpNo(hrId);

        Set<String> deviceSpaceIdList = spaceDeviceControlList.stream().map(SpaceDeviceControl::getDevice).filter(obj -> obj.getSpace() != null).map(IotDevice::getSpace).map(Space::getId).collect(Collectors.toSet());


        List<SpaceAdmin> spaceAdminList = spaceAdminDao.findByAdminEmpNo(hrId);
        Set<String> spaceAdminIdList = spaceAdminList.stream().map(SpaceAdmin::getSpace).map(Space::getId).collect(Collectors.toSet());
        deviceSpaceIdList.addAll(spaceAdminIdList);

        Page<IntelligentOffice> intelligentOfficePage = officeDao.findAll((root, query, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();
            list.add(root.get("space").get("id").in(deviceSpaceIdList));
            list.add(criteriaBuilder.equal(root.get("status"), IntelligentOfficeStatus.ENABLE));

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, pageable);

        List<UserOfficeResp> officeList = new ArrayList<>();
        intelligentOfficePage.getContent().forEach(it -> {
            UserOfficeResp userOfficeResp = new UserOfficeResp();
            userOfficeResp.setOfficeId(it.getId());
            userOfficeResp.setOfficeName(it.getName());
            userOfficeResp.setSpaceId(it.getSpace().getId());
            userOfficeResp.setSpaceName(it.getSpace().getName());
            // 空间位置
            String fullSpaceName = SpaceService.getFullSpaceName(it.getSpace(), it.getSpace().getName());
            userOfficeResp.setAddress(it.getSpace().getProject().getProjectName() + "/" + fullSpaceName);
            officeList.add(userOfficeResp);
        });

        PageBean<UserOfficeResp> officePage = new PageBean<>();
        officePage.setList(officeList);
        officePage.setSize(size);
        officePage.setPage(page);
        officePage.setTotalPages(intelligentOfficePage.getTotalPages());
        officePage.setTotalElements(intelligentOfficePage.getTotalElements());
        return officePage;
    }

    /**
     * 更新设备别名
     *
     * @param userHrId 设备所属用户ID
     * @param deviceId 设备ID
     * @param req      修改参数
     * @return 更改结果
     * @author Yang.Lee
     * @date 2022/2/9 15:31
     **/
    @Override
    @Transactional
    public ServiceResult updateDeviceAlias(String userHrId, String deviceId, DeviceNameEditReq req) {

        Optional<IotDevice> deviceOp = iotDeviceDao.findById(deviceId);
        if (deviceOp.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_DEVICE, deviceId), ErrConst.E01);
        }
        Optional<IntelligentOffice> officeOp = officeDao.findById(req.getOfficeId());
        if (officeOp.isEmpty()) {
            return ServiceResult.error("智能办公室不存在");
        }

        Optional<IntelligentOfficeDevice> officeDeviceOptional = intelligentOfficeDeviceDao.findByIntelligentOfficeIdAndDeviceIdAndUserHrId(req.getOfficeId(), deviceId, userHrId);
        if (officeDeviceOptional.isEmpty()) {
            return ServiceResult.error(ServiceMessage.getMessageWithColon(ServiceMessage.NOT_FOUND_DEVICE, deviceId), ErrConst.E01);
        }

        IntelligentOfficeDevice officeDevice = officeDeviceOptional.get();

        // 权限验证
        // 找到设备所属空间
        Space space = officeDevice.getDevice().getSpace();
        // 判断用户是否是管理员
        List<String> spaceIds = new ArrayList<>();
        spaceIds.add(space.getId());
        getListBySpace(spaceIds, space);
        // Optional<SpaceAdmin> spaceAdminOptional = spaceAdminDao.findBySpaceIdAndAdminEmpNo(space.getId(), userHrId);

        List<SpaceAdmin> spaceAdminList = spaceAdminDao.findBySpaceIdInAndAdminEmpNo(spaceIds, userHrId);
        if (CollUtil.isEmpty(spaceAdminList)) {
            return ServiceResult.error(ServiceMessage.PERMISSION_DENIED, ErrConst.E401);
        }

        if (StringUtils.isNotBlank(req.getAlias()) && !req.getAlias().equals(officeDevice.getDeviceAlisa())) {
            // 找到设备所属会议室
            IntelligentOffice intelligentOffice = officeDevice.getIntelligentOffice();
            // 查询用户在会议室下所有设备列表
            List<IntelligentOfficeDevice> intelligentOfficeDeviceList = intelligentOfficeDeviceDao.findAllByUserHrIdAndIntelligentOfficeId(userHrId, intelligentOffice.getId());
            for (IntelligentOfficeDevice intelligentOfficeDevice : intelligentOfficeDeviceList) {
                String deviceName = StringUtils.isEmpty(intelligentOfficeDevice.getDeviceAlisa()) ? intelligentOfficeDevice.getDevice().getDeviceName() : intelligentOfficeDevice.getDeviceAlisa();
                // 验证用户是否已有重名的设备
                if (StringUtils.equals(deviceName, req.getAlias()) && !intelligentOfficeDevice.getId().equals(officeDevice.getId())) {
                    return ServiceResult.error("该名称已存在，不可重复", ErrConst.E01);
                }
            }
            officeDevice.setDeviceAlisa(req.getAlias());
        }

        if (req.getAdditionalName() != null) {
            DeviceNameEditReq.AdditionalName additionalNameToDB;
            if (StringUtils.isBlank(officeDevice.getDeviceAlisaProperties())) {
                additionalNameToDB = req.getAdditionalName();
            } else {
                additionalNameToDB = JSON.parseObject(officeDevice.getDeviceAlisaProperties(), DeviceNameEditReq.AdditionalName.class);

                if (StringUtils.isNotBlank(req.getAdditionalName().getSwitch1Name())) {
                    additionalNameToDB.setSwitch1Name(req.getAdditionalName().getSwitch1Name());
                }

                if (StringUtils.isNotBlank(req.getAdditionalName().getSwitch2Name())) {
                    additionalNameToDB.setSwitch2Name(req.getAdditionalName().getSwitch2Name());
                }

                if (StringUtils.isNotBlank(req.getAdditionalName().getSwitch3Name())) {
                    additionalNameToDB.setSwitch3Name(req.getAdditionalName().getSwitch3Name());
                }

                if (StringUtils.isNotBlank(req.getAdditionalName().getSwitch4Name())) {
                    additionalNameToDB.setSwitch4Name(req.getAdditionalName().getSwitch4Name());
                }
            }

            // 四路开关不允许存在同名的开关
            if (isExistedDuplicatedSwitchName(additionalNameToDB)) {
                return ServiceResult.error("该名称已存在，不可重复", ErrConst.E01);
            }

            officeDevice.setDeviceAlisaProperties(JSON.toJSONString(additionalNameToDB));
        }

        intelligentOfficeDeviceDao.save(officeDevice);
        return ServiceResult.ok();
    }

    private void getListBySpace(List<String> spaceIds, Space space) {

        if (null != space.getParent()) {
            Optional<Space> spaceOptional = spaceDao.findById(space.getParent().getId());
            if (spaceOptional.isPresent()) {

                // 获取所有夫空间
                getListBySpace(spaceIds, spaceOptional.get());

                spaceIds.add(spaceOptional.get().getId());
            }
        }
    }

    /**
     * 判断 {@code additionalName}中是否存在同名的开关
     *
     * @param additionalName
     * @return
     */
    private boolean isExistedDuplicatedSwitchName(DeviceNameEditReq.AdditionalName additionalName) {
        List<String> switchNameList = new ArrayList<>();

        if (StringUtils.isNotBlank(additionalName.getSwitch1Name())) {
            switchNameList.add(additionalName.getSwitch1Name());
        }

        if (StringUtils.isNotBlank(additionalName.getSwitch2Name())) {
            switchNameList.add(additionalName.getSwitch2Name());
        }

        if (StringUtils.isNotBlank(additionalName.getSwitch3Name())) {
            switchNameList.add(additionalName.getSwitch3Name());
        }

        if (StringUtils.isNotBlank(additionalName.getSwitch4Name())) {
            switchNameList.add(additionalName.getSwitch4Name());
        }

        return switchNameList.size() != switchNameList.stream().distinct().count();
    }

    /**
     * 查询办公室设备名称列表（只显示启用设备）
     *
     * @param officeId 办公室ID
     * @param userId   用户ID
     * @return 设备列表（若用户不含有设备权限，则返回空集合）
     * @author Yang.Lee
     * @date 2022/2/10 13:22
     **/
    @Override
    @Transactional
    public List<DeviceNameResp> getDeviceNameList(String officeId, String userId) {

        Optional<IntelligentOffice> intelligentOffice = intelligentOfficeDao.findById(officeId);
        if (intelligentOffice.isEmpty()) {
            log.error("未查到该办公室");
            return new ArrayList<>();
        }

        List<IntelligentOfficeDevice> officeDeviceList = new ArrayList<>();
        //获取办公室所有的设备
        intelligentOfficeDeviceService.getDeviceListOfHrIdAndSpaceId(userId, intelligentOffice.get()).forEach(it -> {
            if (UsableStatus.ENABLE.equals(it.getUsableStatus())) {
                officeDeviceList.add(it);
            }
        });

        /*List<IntelligentOfficeDevice> officeDeviceList = intelligentOfficeDeviceDao.findAll((root, query, criteriaBuilder) -> {

            List<Predicate> list = new ArrayList<>();

            list.add(criteriaBuilder.equal(root.get("intelligentOffice").get("id"), officeId));

            list.add(criteriaBuilder.equal(root.get("userHrId"), userId));

            list.add(criteriaBuilder.equal(root.get("usableStatus"), UsableStatus.ENABLE));

            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, Sort.by(Sort.Order.desc(Sorts.CREATED_TIME)));*/
        List<DeviceNameResp> collect = officeDeviceList.stream().map(DeviceNameResp::convert).filter(d->!DeviceType.DOOR_PLATE.equals(d.getDeviceType())).collect(Collectors.toList());
        collect.forEach(it -> it.setOfficeId(officeId));

        return collect;
    }
}
