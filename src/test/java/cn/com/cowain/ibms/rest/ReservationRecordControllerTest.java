package cn.com.cowain.ibms.rest;

import com.alibaba.fastjson.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/11/20
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class ReservationRecordControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private RestTemplate restTemplate;

    private String getPath() {
        return "http://localhost:" + port + API_BASE;
    }

    @Test
    void test1() throws Exception {
        String str = restTemplate.getForObject(getPath() + "/time", String.class);
        System.out.println(str);
    }

    @Test
    void test2() {
        ResponseEntity<JSONObject> forEntity = restTemplate.getForEntity(getPath() + "/reservation?date=2020-08-12", JSONObject.class);
        JSONObject jsonObject = forEntity.getBody();
        Object msg = jsonObject.get("msg");
        Assertions.assertEquals(msg, "OK");
    }

}
