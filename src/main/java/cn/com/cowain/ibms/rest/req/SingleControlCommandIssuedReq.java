package cn.com.cowain.ibms.rest.req;

import cn.com.cowain.ibms.enumeration.iot.DeviceType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class SingleControlCommandIssuedReq {
    @ApiModelProperty(value = "事务ID（唯一标识）", required = true)
    @NotBlank
    private String traceId;

    @ApiModelProperty(value = "集控码id", required = true)
    @NotBlank
    private String centralizedCode;

    @ApiModelProperty(value = "执行动作：on开 off关", required = true)
    @NotBlank
    private String action;

    @ApiModelProperty(value = "设备唯一标识ID", required = true)
    @NotBlank
    private String deviceId;

    @NotBlank
    @ApiModelProperty(value = "用户ucId", example = "123abcABC", required = true, position = 1)
    private String uid;

    @ApiModelProperty(value = "设备类型", required = true, example = "FOUR_WAY_SWITCH")
    private DeviceType deviceType;

}
