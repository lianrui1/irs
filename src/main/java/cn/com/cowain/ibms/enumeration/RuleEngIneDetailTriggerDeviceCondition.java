package cn.com.cowain.ibms.enumeration;

/**
 * 联动规则设备条件关系
 *
 * @author: yanzy
 * @date: 2022/2/10 13:10
 */
public enum RuleEngIneDetailTriggerDeviceCondition {

    GREATER("大于"),

    LESS("小于"),

    HAVE("有人移动"),

    NOTHING("无人移动");


    RuleEngIneDetailTriggerDeviceCondition(String name){
        this.name = name;
    }

    private String name;

    public String getName(){
        return this.name;
    }
}
