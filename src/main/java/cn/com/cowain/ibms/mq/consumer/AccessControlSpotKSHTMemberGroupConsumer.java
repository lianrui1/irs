package cn.com.cowain.ibms.mq.consumer;

import cn.com.cowain.ibms.mq.Topic;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * KSHT门禁点授权申请通过请求iotc权限下发回调处理
 *
 * @author: yanzy
 * @date: 2022/3/25 18:40
 */
@Slf4j
@RocketMQMessageListener(
        topic = "${rocketmq.env}-" + Topic.ACCESS_CONTROL_KSHT_VISITOR_RETURN,
        consumerGroup = "${rocketmq.env}-" + Topic.ACCESS_CONTROL_KSHT_VISITOR_RETURN
)
@Component
public class AccessControlSpotKSHTMemberGroupConsumer implements RocketMQListener<String> {

//    @Resource
//    private AbilityService abilityService;

    @Override
    public void onMessage(@RequestBody String data) {
//
//        try{
//            // 解析数据
//
//            BizReturnMessage message = JSON.parseObject(data, BizReturnMessage.class);
//            // 保存结果
//            abilityService.saveReturnMessage(message);
//
//        } catch (Exception e){
//            log.error(e.getMessage(), e);
//        }
    }
}
