package cn.com.cowain.ibms.utils;

import cn.com.cowain.ibms.rest.bean.TimeSlotBean;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

/**
 * Description
 *
 * @author 张宇鑫
 * @version 1.0
 * @date 2020/8/8 15:19
 */
public class DateUtil {

    private DateUtil() {
    }

    /**
     * 根據from，to 按半小時间隔进行拆分
     * 参数: 09:00,   11:00
     * <p>
     * 返回: [
     * {   from: 09:00
     * to: 09:30
     * },
     * {   from: 09:30
     * to: 10:00
     * },
     * <p>
     * ]
     *
     * @param timeFrom
     * @param timeTo
     * @return
     */
    public static List<TimeSlotBean> splitDate(String timeFrom, String timeTo) {
        List<TimeSlotBean> list = new ArrayList<>();
        //替换字符串 "08:00" ==> "0800"
        String timeFromWithout = timeFrom.replace(":", "");
        //替换字符串 "11:00" ==> "1100"
        String timeToWithout = timeTo.replace(":", "");
        //转换Integer '0800' ==> 800
        Integer timeFromNum = Integer.valueOf(timeFromWithout);
        //转换Integer '1100' ==> 1100
        Integer timeToNum = Integer.valueOf(timeToWithout);
        //获取差值 1100-800 = 300
        Integer timeDifference = timeToNum - timeFromNum;
        //判断时间是否大于30分钟
        if (timeDifference >= IConst.TIME_MINUTE_ADD) {
            // 分割字符串 "08:00" ==> ["08","00"]
            String[] timeFromSplit = timeFrom.split(":");
            //转换Integer '08' ==> 8
            Integer timeFromHigh = Integer.valueOf(timeFromSplit[0]);
            //转换Integer '00' ==> 0
            Integer timeFromLow = Integer.valueOf(timeFromSplit[1]);
            // 分割字符串 "11:00" ==> ["11","00"]
            String[] timeToSplit = timeTo.split(":");
            //转换Integer '11' ==> 11
            Integer timeToHigh = Integer.valueOf(timeToSplit[0]);
            //转换Integer '00' ==> 0
            Integer timeToLow = Integer.valueOf(timeToSplit[1]);
            Integer lowNum = 0;
            //获取末尾 差值 0-0 = 0 30-0=30 0-30=-30
            if ((timeToLow - timeFromLow) > 0) {
                lowNum = 1;
            } else if ((timeToLow - timeFromLow) < 0) {
                lowNum = -1;
            }
            // 获得n个 timeSlot   08:00-11:00 ==> 2*(11-8) + 0 = 6
            Integer number = 2 * (timeToHigh - timeFromHigh) + lowNum;
            String stringTimeFromHigh;
            String stringTimeFromLow;
            // 根据list个数 循环
            for (Integer i = 0; i < number; i++) {
                //创建一个timeSlot类
                TimeSlotBean timeSlot = new TimeSlotBean();
                //将高位转换成字符串 8 ==> "8"
                stringTimeFromHigh = String.valueOf(timeFromHigh);
                //补位 "8" == > "08"
                for (Integer numberLength = stringTimeFromHigh.length(); numberLength < IConst.TIME_STRING_LENGTH; numberLength++) {
                    stringTimeFromHigh = String.valueOf(new StringBuilder("0").append(stringTimeFromHigh));
                }
                //将地位转换成字符串 0 == "0"
                stringTimeFromLow = String.valueOf(timeFromLow);
                //补位 "0" == > "00"
                for (Integer numberLength = stringTimeFromLow.length(); numberLength < IConst.TIME_STRING_LENGTH; numberLength++) {
                    stringTimeFromLow = String.valueOf(new StringBuilder("0").append(stringTimeFromLow));
                }
                //拼接 "08"+":" + "00" == "08:00"
                timeSlot.setFrom(stringTimeFromHigh + ":" + stringTimeFromLow);
                //低位 + 30  0+30 = 30
                timeFromLow = timeFromLow + IConst.TIME_MINUTE_ADD;
                //若满60 高位 进 1  低位变成 0
                if (timeFromLow == IConst.TIME_MINUTE_MAX) {
                    timeFromLow = 0;
                    timeFromHigh = timeFromHigh + 1;
                }
                //将高位转换成字符串  8 ==> "8"
                stringTimeFromHigh = String.valueOf(timeFromHigh);
                //补位 "8" == > "08"
                for (Integer numberLength = stringTimeFromHigh.length(); numberLength < IConst.TIME_STRING_LENGTH; numberLength++) {
                    stringTimeFromHigh = String.valueOf(new StringBuilder("0").append(stringTimeFromHigh));
                }
                //将地位转换成字符串 0 == "0"
                stringTimeFromLow = String.valueOf(timeFromLow);
                //补位 "0" == > "00"
                for (Integer numberLength = stringTimeFromLow.length(); numberLength < IConst.TIME_STRING_LENGTH; numberLength++) {
                    stringTimeFromLow = String.valueOf(new StringBuilder("0").append(stringTimeFromLow));
                }

                //拼接 "08"+":" + "30" == "08:30"
                timeSlot.setTo(stringTimeFromHigh + ":" + stringTimeFromLow);

                //添加到list
                list.add(timeSlot);
            }
        }
        return list;
    }

    /**
     * 字符串转日期
     *
     * @param strDate
     * @return
     */
    public static LocalDate convert(String strDate) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(strDate, dateTimeFormatter);
    }

    public static String convert2(LocalDate date) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return date.format(dateTimeFormatter);
    }

    /**
     * 日期字符串 + 时间段字符串 转为  LocalDateTime
     *
     * @param date 2020-08-12
     * @param hour 08:00
     * @return LocalDateTime
     */
    public static LocalDateTime merge(String date, String hour) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        String str = date + " " + hour;
        return LocalDateTime.parse(str, dateTimeFormatter);
    }

    /**
     * 判断时间是否超过 指定长度
     *
     * @param dbTime   数据库时间(静态)
     * @param sysTime  系统时间(动态)
     * @param duration 时间长度
     * @return
     */
    public static boolean isExceed(LocalDateTime dbTime, LocalDateTime sysTime, long duration) {
        long lFrom = dbTime.toEpochSecond(ZoneOffset.of("+8"));
        long lNow = sysTime.toEpochSecond(ZoneOffset.of("+8"));
        if (lFrom < lNow && lNow - lFrom > duration) {
            return true;
        }
        return false;
    }

    /**
     * 获取可用时间段
     * @param localTimeFrom
     * @param localTimeNow
     * @return
     */
    public static boolean isTimeAvailable(LocalTime localTimeFrom, LocalTime localTimeNow){

        if(localTimeFrom.isBefore(localTimeNow)){
            // 如果from在now之前
            int fromHour = localTimeFrom.getHour();
            int nowHour = localTimeNow.getHour();

            int hourDiff = nowHour - fromHour;
            if(hourDiff > 1){
                return false;
            } else {

                int fromMin = localTimeFrom.getMinute();
                int nowMin = localTimeNow.getMinute();
                int minDiff = nowMin - fromMin;
                if(hourDiff == 1 && minDiff <= 30){
                    return false;
                }

                if( hourDiff == 0 && minDiff > 30){
                    return false;
                }
            }
        }

        return true;
    }

    public static boolean isTimeAvailable(LocalDateTime localTimeFrom, LocalDateTime localTimeNow){
        long nowTs = localTimeNow.toEpochSecond(ZoneOffset.of("+8"));
        long fromTs = localTimeFrom.toEpochSecond(ZoneOffset.of("+8"));

        if(fromTs - nowTs > -1800){
            return true;
        }
        return false;
    }

    /**
     * 获取指定日期时间加上指定数量日期时间单位之后的日期时间
     *
     * @author: yanzy
     * @param localDateTime 日期时间
     * @param num 数量
     * @param chronoUnit 日期时间单位
     * @return LocalDateTime 新的日期时间
     * @data: 2022/4/25 10:04:59
     */
    public static LocalDateTime plus(LocalDateTime localDateTime, int num, ChronoUnit chronoUnit) {

        return localDateTime.plus(num, chronoUnit);

    }

}
