package cn.com.cowain.ibms.utils;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.sfp.comm.utils.JwtUtil;
import lombok.experimental.UtilityClass;

/**
 * Token工具
 *
 * @author Yang.Lee
 * @date 2021/4/25 10:21
 */
@UtilityClass
public class DaoTools {

    /**
     * 添加创建人姓名
     *
     * @param entity 待添加对象，需要继承BaseEntity类
     * @author Yang.Lee
     * @date 2021/4/25 11:00
     **/
    public <T extends BaseEntity> void fillCreator(T entity) {

        String token = TokenUtils.getToken();
        entity.setCreatedBy(JwtUtil.getRealName(token));
    }

    /**
     * 添加修改人姓名
     *
     * @param entity 待添加对象，需要继承BaseEntity类
     * @author Yang.Lee
     * @date 2021/4/25 11:00
     **/
    public <T extends BaseEntity> void fillUpdater(T entity) {

        String token = TokenUtils.getToken();
        entity.setUpdatedBy(JwtUtil.getRealName(token));
    }
}
