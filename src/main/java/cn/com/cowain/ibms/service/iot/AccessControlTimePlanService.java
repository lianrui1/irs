package cn.com.cowain.ibms.service.iot;

import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.req.iot.AccessControlBindTimePlanReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.iot.AccessControlTimePlanResp;
import org.springframework.http.ResponseEntity;

/**
 * @author wei.cheng
 * @date 2022/09/28 16:10
 */
public interface AccessControlTimePlanService {
    /**
     * 用户门禁点权限绑定时间计划
     *
     * @param req
     * @return
     */
    ResponseEntity<JsonResult<IdResp>> bindTimePlan(AccessControlBindTimePlanReq req);

    /**
     * 查询用户在门禁点下的时间计划
     *
     * @param spotId
     * @param userHrId
     * @return
     */
    AccessControlTimePlanResp getAccessControlTimePlan(String spotId, String userHrId);

    /**
     * 删除用户门禁点权限绑定的时间计划
     *
     * @param spotId
     * @param userHrId
     * @return
     */
    ResponseEntity<JsonResult<IdResp>> deleteTimePlan(String spotId, String userHrId);
}
