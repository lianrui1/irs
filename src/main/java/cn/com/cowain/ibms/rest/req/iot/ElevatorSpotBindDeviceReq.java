package cn.com.cowain.ibms.rest.req.iot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/3/23 3:10
 */
@Data
@ApiModel("梯控绑定设备")
public class ElevatorSpotBindDeviceReq {

    @ApiModelProperty(value = "空间ID")
    private String spaceId;

    @ApiModelProperty(value = "面板机ID")
    private List<String> deviceIds;
}
