package cn.com.cowain.ibms.rest.req.device.centralized;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("新增集控管理员请求对象")
public class CentralizedControlAdminReq {

    private String id;

    @ApiModelProperty(value = "工号", required = true)
    private String hrId;

    @ApiModelProperty(value = "姓名")
    private String name;

}
