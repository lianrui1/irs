package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Yang.Lee
 * @date 2021/10/21 9:51
 */
@Data
@Entity
@Table(name = "iot_device_group_item")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_device_group_item", comment = "设备群组表")
public class DeviceGroupItem extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "device_group_id", nullable = false, columnDefinition = "varchar(36) COMMENT '设备群组id FK'")
    private DeviceGroup deviceGroup;

    @ManyToOne
    @JoinColumn(name = "device_id", nullable = false, columnDefinition = "varchar(36) COMMENT '设备ID FK'")
    private IotDevice iotDevice;
}
