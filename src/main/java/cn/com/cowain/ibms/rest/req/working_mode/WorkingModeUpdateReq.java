package cn.com.cowain.ibms.rest.req.working_mode;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/10/18 16:08
 */
@Data
@ApiModel("编辑工作模式对象")
public class WorkingModeUpdateReq {

    @NotBlank
    @ApiModelProperty(value = "小图标url", required = true)
    private String imgUrl;

    @Length(max = 10,message = "名称最长为10")
    @NotBlank
    @ApiModelProperty(value = "场景名称", required = true)
    private String name;

    @NotEmpty
    @ApiModelProperty(value = "设备状态设置对象", required = true)
    private List<DeviceModeStatusSaveReq> saveReqs;
}
