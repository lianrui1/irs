package cn.com.cowain.ibms.service.device.impl;

import cn.com.cowain.ibms.dao.device.HardwareDao;
import cn.com.cowain.ibms.dao.space.IotProductDao;
import cn.com.cowain.ibms.entity.iot.Hardware;
import cn.com.cowain.ibms.entity.space.IotProduct;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.device.HardwarePageReq;
import cn.com.cowain.ibms.rest.req.device.HardwareReq;
import cn.com.cowain.ibms.rest.resp.device.HardwareDetailResp;
import cn.com.cowain.ibms.rest.resp.device.HardwarePageResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.device.HardwareService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/3/23 15:50
 */
@Service
public class HardwareServiceImpl implements HardwareService {

    @Autowired
    private HardwareDao hardwareDao;

    @Autowired
    private IotProductDao iotProductDao;

    @Override
    @Transactional
    public ServiceResult save(HardwareReq req) {
        // 判断SN码是否唯一
        Optional<Hardware> op = hardwareDao.findByHardWareSn(req.getHardWareSn());
        if(op.isPresent()){
            return ServiceResult.error("该SN码已存在,不可重复");
        }
        // 验证所属产品
        Optional<IotProduct> productOp = iotProductDao.findById(req.getProductId());
        if(!productOp.isPresent()){
            return ServiceResult.error("该产品不存在,请重新选择");
        }
        Hardware hardware = new Hardware();
        // 赋值
        BeanUtils.copyProperties(req ,hardware);
        hardware.setIotProduct(productOp.get());
        hardwareDao.save(hardware);
        return ServiceResult.ok();
    }

    @Override
    @Transactional
    public PageBean<HardwarePageResp> getPage(HardwarePageReq req) {
        int page = req.getPage();
        int size = req.getSize();
        // 创建分页查询对象
        Sort sort = Sort.by(req.getOrders());
        Pageable pageable = PageRequest.of(page, size, sort);

        Page<Hardware> hardwarePage = hardwareDao.findAll((root, criteriaQuery, criteriaBuilder) -> {

            // 构造查询条件
            List<Predicate> list = new ArrayList<>();

            // SN码
            String hardwareSN = req.getHardwareSN();
            if (StringUtils.isNotEmpty(hardwareSN)) {
                list.add(criteriaBuilder.like(root.get("hardWareSn").as(String.class), "%" + hardwareSN + "%"));
            }
            // 设备名称
            String deviceName = req.getDeviceName();
            if (StringUtils.isNotEmpty(deviceName)) {
                list.add(criteriaBuilder.like(root.get("nameZh").as(String.class), "%" + deviceName + "%"));
            }
            // 产品
            String productId = req.getProductId();
            if (StringUtils.isNotEmpty(productId)) {
                list.add(criteriaBuilder.like(root.get("iotProduct").get("id"), productId));
            }


            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        }, pageable);

        // 数据对象转换
        List<HardwarePageResp> contentList = convertList(hardwarePage.getContent());
        PageBean<HardwarePageResp> pageBean = new PageBean<>();
        pageBean.setList(contentList);
        pageBean.setTotalElements(hardwarePage.getTotalElements());
        pageBean.setTotalPages((int)Math.ceil((double)hardwarePage.getTotalElements() / (double)size));
        pageBean.setPage(page);
        pageBean.setSize(size);
        return pageBean;
    }

    @Override
    @Transactional
    public ServiceResult delete(String id) {
        // 根据ID查询硬件信息
        Optional<Hardware> byId = hardwareDao.findById(id);
        if(!byId.isPresent()){
            return ServiceResult.error("硬件ID有误,请重新选择");
        }
        Hardware hardware = byId.get();
        hardware.setIsDelete(1);
        // 删除
        hardwareDao.save(hardware);
        return ServiceResult.ok();
    }

    @Override
    public ServiceResult get(String id) {
        // 查询硬件信息
        Optional<Hardware> byId = hardwareDao.findById(id);
        if(!byId.isPresent()){
            return ServiceResult.error("硬件ID有误");
        }
        Hardware hardware = byId.get();
        HardwareDetailResp hardwareDetailResp = new HardwareDetailResp();
        // 赋值
        BeanUtils.copyProperties(hardware ,hardwareDetailResp);
        // 硬件所属产品ID
        hardwareDetailResp.setProductId(hardware.getIotProduct().getId());
        // 硬件所属产品名称
        hardwareDetailResp.setProductName(hardware.getIotProduct().getName());
        // 所属设备类型
        hardwareDetailResp.setDeviceType(hardware.getDeviceType().toString());
        // 所属设备类型名称
        hardwareDetailResp.setDeviceTypeName(hardware.getDeviceType().getName());
        return ServiceResult.ok(hardwareDetailResp);
    }

    @Override
    @Transactional
    public ServiceResult update(String id, HardwareReq req) {
        // 判断硬件是否存在
        Optional<Hardware> byId = hardwareDao.findById(id);
        if(!byId.isPresent()){
            return ServiceResult.error("硬件ID有误");
        }
        // 判断SN码是否已存在
        Optional<Hardware> op = hardwareDao.findByHardWareSn(req.getHardWareSn());
        if(op.isPresent() && !op.get().getId().equals(id)){
                return ServiceResult.error("该SN码已存在,不可重复");
        }
        // 验证所属产品
        Optional<IotProduct> productOp = iotProductDao.findById(req.getProductId());
        if(!productOp.isPresent()){
            return ServiceResult.error("该产品不存在,请重新选择");
        }
        Hardware hardware = byId.get();
        // 赋值
        BeanUtils.copyProperties(req ,hardware);
        hardware.setIotProduct(productOp.get());
        // 保存
        hardwareDao.save(hardware);
        return ServiceResult.ok();
    }

    /**
     * 对象转换
     *
     * @return 目标对象
     **/
    public List<HardwarePageResp> convertList(@NotNull List<Hardware> hardwareList) {

        List<HardwarePageResp> result = new ArrayList<>();
        if (hardwareList.isEmpty()) {
            return result;
        }
        hardwareList.forEach(obj -> result.add(convert(obj)));

        return result;
    }

    /**
     * 对象转换
     *
     * @return 目标对象
     **/
    public HardwarePageResp convert(Hardware hardware) {

        return HardwarePageResp.builder()
                .id(hardware.getId())
                .hardwareSN(hardware.getHardWareSn())
                .nameZH(hardware.getNameZh())
                .nameEN(hardware.getNameEn())
                .productName(hardware.getIotProduct().getName())
                .deviceType(hardware.getDeviceType().getName())
                .brand(hardware.getBrand())
                .model(hardware.getModel())
                .createTime(hardware.getCreatedTime())
                .build();
    }

}
