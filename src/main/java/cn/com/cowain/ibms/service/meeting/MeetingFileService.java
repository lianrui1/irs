package cn.com.cowain.ibms.service.meeting;

import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.rest.req.ReservationUploadUrlReq;
import cn.com.cowain.ibms.rest.req.meeting.MeetingFileCreateReq;
import cn.com.cowain.ibms.rest.req.meeting.MeetingFileReq;
import cn.com.cowain.ibms.rest.req.meeting.UpdateMeetingFileReq;
import cn.com.cowain.ibms.rest.resp.meeting.MeetingFileResp;
import cn.com.cowain.ibms.rest.resp.meeting.MeetingFileUrlResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;

import java.util.List;

/**
 * @author wei.cheng
 * @date 2022/02/23 10:44
 */
public interface MeetingFileService {
    /**
     * 创建会议文档
     *
     * @param req
     * @param token
     * @return
     */
    ServiceResult createMeetingFile(MeetingFileCreateReq req, String token);

    /**
     * 删除会议文档
     *
     * @param id
     * @param token
     * @return
     */
    ServiceResult deleteMeetingFile(String id, String token);

    /**
     * 查询会议预约下的会议文档
     *
     * @param reservationRecordId
     * @return
     */
    ServiceResult searchMeetingFileOfReservationRecord(String reservationRecordId);

    /**
     * 查询会议预约下的会议文档
     *
     * @param reservationRecordId
     * @return
     */
    List<MeetingFileResp> getMeetingFileByRecordId(String reservationRecordId);

    /**
     * 查询会议预约下的会议文档
     *
     * @param reservationRecordId
     * @return
     */
    List<MeetingFileUrlResp> getMeetingFileUrlByRecordId(String reservationRecordId);

    /**
     * 创建会议文档
     *
     * @param reservationRecord 会议预约，不能为空
     * @param meetingFileList   要创建的会议信息
     * @param token             当前token，不能为空
     */
    void createMeetingFile(ReservationRecord reservationRecord, List<MeetingFileReq> meetingFileList, String token);

    /**
     * 更新会议文档
     *
     * @param reservationRecord 会议预约，不能为空
     * @param meetingFileList   要创建的会议信息
     * @param token             当前token，不能为空
     */
    void updateMeetingFile(ReservationRecord reservationRecord, List<UpdateMeetingFileReq> meetingFileList, String token);

    // 报存会议链接
    void createMeetingUrl(ReservationRecord reservationRecord, ReservationUploadUrlReq req, String token);
}
