package cn.com.cowain.ibms.rest.req.meeting;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author Yang.Lee
 * @date 2021/6/8 18:20
 */
@Data
@ApiModel("用户自定义用户组编辑数据对象")
public class CustomUserGroupEditReq {

    @NotBlank
    @Length(max=10, message = "名称长度不能超过10个字符")
    @ApiModelProperty(value = "用户组名称", required = true, example = "用户组A")
    private String name;

    @NotEmpty
    @ApiModelProperty(value = "用户组中用户工号列表", required = true, position = 1)
    private List<String> userList;
}
