package cn.com.cowain.ibms.mq.bean;

import lombok.Data;

import java.util.List;

/**
 * @author feng
 * @title: AccessPersion
 * @projectName ibms
 * @Date 2022/1/25 15:18
 */
@Data
public class AccessPersion {

    //   "[{'jobNo':'CWA3264','groupIds':['d40e04c8-3db7-4005-bc78-ce7b43b0936c']”),'photo':'http://file.cowainglobal.com/img/M00/07/25/rBIQr2HKe9uAHLdnAAJcCEuEUYo424.jpg','name':'唐其亮'}]}")

    private String jobNo ;
    private List<String> groupIds;
    String photo;
    String name;
}
