package cn.com.cowain.ibms.mq.bean;

import lombok.Data;

import java.io.Serializable;
import java.util.UUID;

/**
 * 门磁消息实体类
 * @author Yang.Lee
 * @date 2020/12/7 9:21
 */
@Data
public class DoorMagnetismMessage implements Serializable {

    public DoorMagnetismMessage (){
        this.uuid = createUUid();
    }

    /**
     * 消息id, 系统生成, 全局唯一
     */
    private String uuid;
    /**
     * 消息来源
     */
    private String source;

    /**
     * 门磁编号
     */
    private String doorMagneticNum;

    /**
     * 消息内容
     */
    private String message;

    /**
     * 门磁状态:
     * OPEN: 开门
     */
    private DoorMagnetismStatus status;

    /**
     * 数据时间
     */
    private String time;

    /**
     * iot编号
     */
    private String entityId;

    /**
     * 创建全局唯一ID
     * @return
     */
    private String createUUid(){

        return UUID.randomUUID().toString();
    }
}
