package cn.com.cowain.ibms.service.office;

import cn.com.cowain.ibms.rest.req.working_mode.ModeChangeReq;
import cn.com.cowain.ibms.rest.req.working_mode.WebWorkingModeSaveReq;
import cn.com.cowain.ibms.rest.resp.working_mode.WebWorkingModePageResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import org.springframework.validation.BindingResult;

import java.util.List;

/**
 * @author feng
 * @title: MeetingRoomService
 * @projectName ibms
 * @Date 2021/12/29 9:34
 */
public interface MeetingRoomModelService {
    // 新建办公室工作场景
    ServiceResult save(WebWorkingModeSaveReq req, BindingResult bindingResult);

    //查询详情
    ServiceResult findDetail(String modelId);

    ServiceResult update(String id, WebWorkingModeSaveReq req);

    List<WebWorkingModePageResp> getModelList();

    ServiceResult delete(String id);

    ServiceResult changeMode(ModeChangeReq req);

    ServiceResult getSensorData(String appId);
}
