package cn.com.cowain.ibms.service.bean.schedule.req;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Yang.Lee
 * @date 2021/6/21 13:46
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DataSyncReq {
    private String id;

    private String from;

    private String busiId;

    private String startTimeStr;

    private String endTimeStr;

    private String busiEventType;

    private String busiEventTitle;

    private String busiEventDesc;

    private String targetJobNo;
}
