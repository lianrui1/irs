package cn.com.cowain.ibms.entity;

import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.enumeration.RoomType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 房间
 *
 * @author 胡荆陵
 * @since 2020-7-30
 */
@Data
@Entity
@Table(name = "ibms_room")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_room", comment = "会议室")
@Where(clause = "is_del=0")
public class Room extends BaseEntity {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    @Column(name = "room_name", nullable = false, columnDefinition = "varchar(255) COMMENT '名称'")
    private String name;

    /**
     * 人数容量
     */
    @Column(name = "room_cap", nullable = false, columnDefinition = "integer COMMENT '人数容量'")
    private int capacity;

    /**
     * 楼栋
     */
    @Column(name = "building",  columnDefinition = "varchar(200) COMMENT '楼栋'")
    private String building;

    /**
     * 楼层
     */
    @Column(name = "floor", columnDefinition = "varchar(200) COMMENT '楼层'")
    private String floor;

    /**
     * 是否开放 0:否 1:是
     */
    @Column(name = "is_open", columnDefinition = "integer COMMENT '是否开放 0:否;1:是'")
    private int isOpen;

    /**
     * 包含设备
     */
    @Column(name = "devices", columnDefinition = "varchar(200) COMMENT '包含设备'")
    private String devices;

    /**
     * 排序
     */
    @Column(name = "seq", columnDefinition = "integer COMMENT '排序'")
    private int seq;

    /**
     * 房间类型
     */
    @Column(name = "room_type", columnDefinition = "varchar(50) COMMENT '房间类型'")
    @Enumerated(EnumType.STRING)
    private RoomType type;

    /**
     * iot 空间
     */
    @ManyToOne
    @JoinColumn(name = "space_id", columnDefinition = "varchar(36) COMMENT 'space_id FK'")
    private Space space;

    /**
     * 注意事项
     **/
    @Column(name = "attentions", columnDefinition = "varchar(300) COMMENT '注意事项'")
    private String attentions;

    @Column(name = "things", columnDefinition = "varchar(300) COMMENT '物品清单，多个数值使用英文字符\",\"分割'")
    private String things;

    @Column(name = "img", columnDefinition = "varchar(100) COMMENT '会议室图片'")
    private String img;

    @Column(name = "original_Img_name", columnDefinition = "varchar(150) COMMENT '会议室图片文件原名称'")
    private String originalImgName;
}
