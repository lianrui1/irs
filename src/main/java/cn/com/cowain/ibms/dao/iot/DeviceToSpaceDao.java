package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.DeviceToSpace;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 *
 *
 * @author Yang.Lee
 * @date 2020/11/27 15:56
 */
@Repository
public interface DeviceToSpaceDao extends JpaRepository<DeviceToSpace, String>  {


    /**
     * 根据deviceId查询对应关系
     * @param deviceId
     * @return
     * @author Yang.Lee
     */
    Optional<DeviceToSpace> findByDeviceId(String deviceId);

    /**
     * 根据spaceid，查询该space下所有的设备
     * @param spaceId
     * @return
     * @author Yang.Lee
     */
    List<DeviceToSpace> findAllBySpaceId(String spaceId);
}
