package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.DeviceAdmin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 设备管理员表Dao
 *
 * @author Yang.Lee
 * @date 2022/7/15 9:39
 */
@Repository
public interface DeviceAdminDao extends JpaRepository<DeviceAdmin, String>, JpaSpecificationExecutor<DeviceAdmin> {

    /**
     * 根据设备id查询管理员信息
     *
     * @param deviceId  设备ID
     * @return 设备管理员列表
     * @author Yang.Lee
     * @date 2022/7/15 9:48
     **/
    List<DeviceAdmin> findByDeviceId(String deviceId);
}
