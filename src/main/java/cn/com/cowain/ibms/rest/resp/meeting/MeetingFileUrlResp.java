package cn.com.cowain.ibms.rest.resp.meeting;

import cn.com.cowain.ibms.rest.resp.StaffResp;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/4/8 3:43
 */
@Data
@ApiModel("会议室文档链接详情")
public class MeetingFileUrlResp {

    @ApiModelProperty(value = "主键ID", required = true, position = 0)
    private String id;

    /**
     * 会议预约记录ID，外键
     */
    @ApiModelProperty(value = "会议id", required = true, position = 1)
    private String reservationRecordId;

    @ApiModelProperty(value = "文件URL", required = true, position = 2, example = "http://tests.cowain.cn/group1/M00/00/0C/rBIQA2IUxcSACgUHAAIqBPKjJ5A851.pdf")
    private List<String> url;

    @ApiModelProperty(value = "链接说明")
    private String urlRemark;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间", required = true, position = 8)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "会议文档创建者", required = true, position = 10)
    private StaffResp creator;

}
