package cn.com.cowain.ibms.utils;

import cn.com.cowain.ibms.dao.SysUserDao;
import cn.com.cowain.ibms.entity.SysUser;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

/**
 * Description
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/10/20
 */
@Slf4j
@SpringBootTest
class RedisUtilTest {

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private SysUserDao sysUserDao;

    @Test
    public void redisUtilTest() {
        SysUser sysUser = new SysUser();
//        sysUser.setSysId("123123123080");
//        sysUser.setNameZh("更新人2");
//        sysUser.setEmpNo("asd");
//        sysUser.setDeptName("测试部门");
        String sysId = "123123123080";
        if (!redisUtil.hasKey(sysId)) {
            //redis 没有该参数 ==> 去数据库查看
            Optional<SysUser> sysUserSql = sysUserDao.findBySysId(sysId);
            // 判断 mysql 是否存有人员信息
            if (sysUserSql.isPresent()) {
                // 数据库有信息 但 redis 没有信息 ==> 写入redis
                redisUtil.set(sysId, sysUserSql.get());
                sysUser = sysUserSql.get();
            } else {
                //数据库和redis 都没有信息 == > 返回 null
            }
        } else {
            JSONObject jsonObject = (JSONObject) redisUtil.get(sysId);
            SysUser sysUserRedis = JSONObject.toJavaObject(jsonObject, SysUser.class);
            sysUser = sysUserRedis;
        }
        log.info(String.valueOf(sysUser));
    }

    @Test
    public void tryToCreateRedisUtilTest() {
        SysUser sysUser = new SysUser();
        sysUser.setSysId("admin");
        sysUser.setNameZh("管理员");
        sysUser.setEmpNo("asd");
        sysUser.setDeptName("测试部门");
        String sysId = sysUser.getSysId();
        if (!redisUtil.hasKey(sysId)) {
            //redis 没有该参数 ==> 去数据库查看
            Optional<SysUser> sysUserSql = sysUserDao.findBySysId(sysId);
            // 判断 mysql 是否存有人员信息
            if (sysUserSql.isPresent()) {
                // 数据库有信息 但 redis 没有信息 ==> 写入redis
                redisUtil.set(sysId, sysUserSql.get());
            } else {
                //数据库和redis 都没有信息 == > 存入
                sysUserDao.save(sysUser);
                redisUtil.set(sysId, sysUser);
            }
        }
    }

    @Test
    public void test1() {
        SysUser sysUser = new SysUser();
        sysUser.setSysId("123");
        sysUser.setEmpNo("asd");
        sysUser.setDeptName("测试部门");
        redisUtil.set("someKey", sysUser);
    }

    @Test
    public void test2() {
        JSONObject jsonObject = (JSONObject) redisUtil.get("someKey");
        SysUser sysUser = JSONObject.toJavaObject(jsonObject, SysUser.class);
        System.out.println(sysUser);
    }

    @Test
    public void test3() {
        Boolean booleankey = redisUtil.hasKey("someKey");
        log.info("booleankey" + booleankey);
        Boolean booleankey1 = redisUtil.hasKey("someKey1");
        log.info("booleankey" + booleankey1);
        redisUtil.del("someKey");

        Boolean booleankey2 = redisUtil.hasKey("someKey");
        log.info("booleankey2" + booleankey2);

        JSONObject jsonObject = (JSONObject) redisUtil.get("someKey1");
        System.out.println(jsonObject.isEmpty());

        JSONObject jsonObject1 = (JSONObject) redisUtil.get("someKey");
        System.out.println(jsonObject1.isEmpty());
//        if(jsonObject.)
        SysUser sysUser = JSONObject.toJavaObject(jsonObject1, SysUser.class);
        System.out.println(sysUser);
    }

}
