package cn.com.cowain.ibms.rest.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Description
 *
 * @author 张宇鑫
 * @version 1.0
 * @date 2020/8/5 20:47
 */

@Data
public class IdAndSeqBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "被排序数据ID", required = true)
    @NotBlank(message = "id不能为空")
    private String id;

    @ApiModelProperty(value = "被排序数据序号", required = true, example = "1", position = 1)
    @NotNull(message = "seq不能为空")
    private Integer seq;

}