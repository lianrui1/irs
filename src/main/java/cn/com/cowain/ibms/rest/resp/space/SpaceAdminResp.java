package cn.com.cowain.ibms.rest.resp.space;

import cn.com.cowain.ibms.rest.resp.iot.IotDeviceResp;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/11/15 15:05
 */
@Data
public class SpaceAdminResp {

    @ApiModelProperty(value = "姓名", required = true)
    private String name;

    @ApiModelProperty(value = "工号", required = true)
    private String empNo;

    @ApiModelProperty(value = "部门", required = true)
    private String dept;

    @ApiModelProperty(value = "工作照", required = true)
    private String workPhoto;

    @ApiModelProperty(value = "可控设备列表")
    List<IotDeviceResp> deviceList;
}
