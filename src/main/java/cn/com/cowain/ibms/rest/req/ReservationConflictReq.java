package cn.com.cowain.ibms.rest.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Set;

/**
 * 会议预定冲突检测查询bean
 *
 * @author 胡荆陵
 * @version 1.0
 * @since 2020 8/11/20
 */
@Data
@ApiModel("冲突检查请求")
public class ReservationConflictReq implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户id集合", required = true)
    private Set<String> ids;

    @ApiModelProperty(value = "日期", required = true, position = 1, example = "2000-12-31")
    private String date;

    @ApiModelProperty(value = "开始时间", required = true, position = 2, example = "09:00")
    private String timeFrom;

    @ApiModelProperty(value = "结束时间", required = true, position = 2, example = "10:00")
    private String timeTo;
    @ApiModelProperty(value = "会议id")
    private String meetingId;

    @ApiModelProperty(value = "用户工号集合", required = true)
    private Set<String> empNos;
}
