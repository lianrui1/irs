package cn.com.cowain.ibms.rest.req.office;

import cn.com.cowain.ibms.enumeration.RuleEngineDetailEffectiveType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 生效方式
 *
 * @author: yanzy
 * @date: 2022/2/9 14:47
 */
@Data
@ApiModel("生效方式请求对象")
public class RuleEngineDetailEffectiveTypeReq {

    @ApiModelProperty(value = "生效方式")
    private RuleEngineDetailEffectiveType effectiveType;

    @ApiModelProperty(value = "定时时间")
    private RuleEngineDetailEffectiveTypeTimingTimeReq effectiveTypeTimingTimeReq;
}
