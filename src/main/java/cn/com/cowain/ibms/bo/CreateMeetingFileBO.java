package cn.com.cowain.ibms.bo;

import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import lombok.Data;

/**
 * @author wei.cheng
 * @date 2022/02/23 18:16
 */
@Data
public class CreateMeetingFileBO {
    /**
     * 当前token关联用户
     */
    private SysUser sysUser;
    /**
     * 会议预约
     */
    private ReservationRecord reservationRecord;
    /**
     * serviceResult
     */
    private ServiceResult serviceResult;
}
