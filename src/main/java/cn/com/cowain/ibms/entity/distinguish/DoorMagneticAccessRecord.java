package cn.com.cowain.ibms.entity.distinguish;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.entity.iot.DoorMagnetic;
import cn.com.cowain.ibms.enumeration.meeting.DoorMagneticAccessResult;
import cn.com.cowain.ibms.enumeration.meeting.UserType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 门磁通行记录
 *
 * @author Yang.Lee
 * @date 2021/8/17 9:23
 */
@Data
@Entity
@Table(name = "ibms_door_magnetic_access_record")
@EqualsAndHashCode(callSuper = true)
@org.hibernate.annotations.Table(appliesTo = "ibms_door_magnetic_access_record", comment = "二维码通行权限")
@Where(clause = "is_del=0")
public class DoorMagneticAccessRecord extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "door_magnetic_id", nullable = false, columnDefinition = "varchar(36) COMMENT '门磁ID'")
    private DoorMagnetic doorMagnetic;

    @Column(name = "emp_no", columnDefinition = "varchar(36) COMMENT '员工工号'")
    private String empNo;

    @Column(name = "name_zh", columnDefinition = "varchar(36) COMMENT '员工姓名'")
    private String nameZH;

    @Enumerated(EnumType.STRING)
    @Column(name = "access_state", nullable = false, columnDefinition = "varchar(36) COMMENT '通行状态'")
    private DoorMagneticAccessResult accessState;

    @Column(name = "data_time", nullable = false, columnDefinition = "varchar(36) COMMENT '数据时间'")
    private LocalDateTime dataTime;

    @Enumerated(EnumType.STRING)
    @Column(name = "user_type", columnDefinition = "varchar(36) COMMENT '员工类型'")
    private UserType userType;
}
