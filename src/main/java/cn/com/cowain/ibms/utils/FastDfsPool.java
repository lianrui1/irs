package cn.com.cowain.ibms.utils;

import lombok.extern.slf4j.Slf4j;
import org.csource.common.MyException;
import org.csource.common.NameValuePair;
import org.csource.fastdfs.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.*;
import java.net.InetSocketAddress;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author Yang.Lee
 * @date 2021/1/17 14:53
 */
@Component
@Slf4j
public class FastDfsPool {

    @Value("${fastdfs.connect_timeout}")
    private String connecTimeout;
    @Value("${fastdfs.network_timeout}")
    private String networkTimeout;
    @Value("${fastdfs.charset}")
    private String charset;
    @Value("${fastdfs.http.tracker_http_port}")
    private String trackerHttpPort;
    @Value("${fastdfs.http.anti_steal_token}")
    private String antiStealToken;
    @Value("${fastdfs.http.secret_key}")
    private String secretKey;
    @Value("${fastdfs.tracker_server}")
    private String trackerServerAddress;

    private static final int DEFAULT_CONNECT_TIMEOUT = 5;

    private static final int DEFAULT_NETWORK_TIMEOUT = 30;

    //默认连接池大小
    public static final int CONNECTION_SIZE = 20;

    private static TrackerClient trackerClient;
    private static LinkedBlockingQueue<StorageClientInfo> storageClient1s = new LinkedBlockingQueue<>(
            CONNECTION_SIZE);
    private static int currentIndex;

    /**
     * 初始化配置
     * @throws MyException
     */
    @PostConstruct
    public void init() throws MyException {

        // springboot 读取配置文件失败，因此手动实现服务配置

        int gConnectTimeout = connecTimeout == null ? DEFAULT_CONNECT_TIMEOUT : Integer.parseInt(connecTimeout);
        if(gConnectTimeout < 0){
            gConnectTimeout = DEFAULT_CONNECT_TIMEOUT;
        }
        gConnectTimeout *= 1000;
        ClientGlobal.setG_connect_timeout(gConnectTimeout);

        int gNetworkTimeout = networkTimeout == null ? DEFAULT_NETWORK_TIMEOUT : Integer.parseInt(networkTimeout);
        if(gNetworkTimeout < 0){
            gNetworkTimeout = DEFAULT_NETWORK_TIMEOUT;
        }
        gNetworkTimeout *= 1000;
        ClientGlobal.setG_network_timeout(gNetworkTimeout);

        String gCharset = charset == null || charset.length() == 0 ? "ISO8859-1" : charset;
        ClientGlobal.setG_charset(gCharset);

        if(trackerServerAddress == null){
            throw new MyException("item \"tracker_server\" not found");
        } else{
            String[] szTrackerServers = trackerServerAddress.split(";");
            InetSocketAddress[] trackerervers = new InetSocketAddress[szTrackerServers.length];

            for(int i = 0; i < szTrackerServers.length; ++i) {
                String[] parts = szTrackerServers[i].split("\\:", 2);
                if (parts.length != 2) {
                    throw new MyException("the value of item \"tracker_server\" is invalid, the correct format is host:port");
                }

                trackerervers[i] = new InetSocketAddress(parts[0].trim(), Integer.parseInt(parts[1].trim()));
            }

            ClientGlobal.setG_tracker_group(new TrackerGroup(trackerervers));
            int gTrackerHttpPort = trackerHttpPort == null ? 80 : Integer.parseInt(trackerHttpPort);
            ClientGlobal.setG_tracker_http_port(gTrackerHttpPort);

            boolean gAntiStealToken = false;
            if(antiStealToken != null){
                gAntiStealToken = antiStealToken.equalsIgnoreCase("yes") || antiStealToken.equalsIgnoreCase("on")
                        || antiStealToken.equalsIgnoreCase("true") || antiStealToken.equalsIgnoreCase("1");
            }
            ClientGlobal.setG_anti_steal_token(gAntiStealToken);
            if (gAntiStealToken) {
                ClientGlobal.setG_secret_key(secretKey);
            }
        }
        FastDfsPool.initTrackerClient();
    }

    private static void initTrackerClient(){
        trackerClient = new TrackerClient();
    }

    /**
     * 工具方法都为static,因此私有构造参数
     */
    private FastDfsPool() {

    }

    private static void createStorageClientInfo() {
        synchronized (trackerClient) {
            if (currentIndex < CONNECTION_SIZE) {
                try {
                    log.debug("创建连接");
                    TrackerServer server = trackerClient.getConnection();
                    StorageClient1 storageClient1 = new StorageClient1(trackerClient.getConnection(), null);
                    StorageClientInfo storageClientInfo = new StorageClientInfo(storageClient1, server);

                    if (storageClient1s.offer(storageClientInfo)) {
                        currentIndex++;
                    }
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }

    private static StorageClientInfo findStorageClient() {
        // 尝试获取一个有用的客户端连接信息
        StorageClientInfo clientInfo = storageClient1s.poll();
        if (clientInfo == null) {

            if (currentIndex < CONNECTION_SIZE) {
                createStorageClientInfo();
            }
            try {
                clientInfo=storageClient1s.poll(10,TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                log.error(e.getMessage(), e);
                Thread.currentThread().interrupt();
            }
        }
        return clientInfo;
    }


    public static String upload(MultipartFile file) throws IOException {
        if(file == null){
            throw new NullPointerException("上传的文件为空");
        }

        String localFileName = file.getOriginalFilename();

        if(localFileName == null || localFileName.length() == 0){
            throw new NullPointerException("上传的文件名称为空");
        }

        InputStream inputStream = file.getInputStream();

        String result = FastDfsPool.upload(inputStream, localFileName,file.getSize());
        inputStream.close();
        return result;
    }

    /**
     * 上传文件到fastdfs服务器
     * @param file
     * @return
     */
    public static String upload(File file) throws IOException {
        if(file == null ){
            throw new NullPointerException("上传的文件为空");
        }

        String fileName = file.getName();
        InputStream inputStream = new FileInputStream(file);

        String result = FastDfsPool.upload(inputStream, fileName,file.length());
        inputStream.close();
        return result;
    }
    public static String upload(InputStream inputStream, String localFileName, long size) throws IOException {

        NameValuePair[] metaList = new NameValuePair[1];
        metaList[0] = new NameValuePair("fileName", localFileName);

        String result = FastDfsPool.upload(localFileName,size , inputStream, metaList);
        inputStream.close();
        return result;
    }

    /**
     *
     * @param in
     *            文件输入流
     * @param valuePairs
     *            元信息
     * @param localFilename
     *            文件名
     * @param size
     *            文件大小
     * @return 上传后的文件路径
     * @throws MyException
     * @throws IOException
     */

    public static String upload(String localFilename, long size, InputStream in, NameValuePair[] valuePairs){

        StorageClientInfo client1 = findStorageClient();
        if(client1!=null){

            String ext = null;
            int nPos = localFilename.lastIndexOf('.');
            if (nPos > 0 && localFilename.length() - nPos <= ProtoCommon.FDFS_FILE_EXT_NAME_MAX_LEN + 1) {
                ext = localFilename.substring(nPos + 1);
            }

            String path=null;
            try {
                path=client1.storageClient1.upload_file1(null, size, new FileUpload(in), ext, valuePairs);
            } catch (Exception e1) {
                log.error(e1.getMessage(), e1);
            }finally {
                try {
                    if(client1.trackerServer.getSocket().isConnected() && storageClient1s.offer(client1)){
                        log.debug("storageClient1s on queue");
                    }
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
            return "/" + path;
        }
        return null;
    }

    private static class StorageClientInfo {

        private StorageClient1 storageClient1;
        private TrackerServer trackerServer;

        public StorageClientInfo(StorageClient1 storageClient1, TrackerServer trackerServer) {
            this.storageClient1 = storageClient1;
            this.trackerServer = trackerServer;

        }

    }

    private static class FileUpload implements UploadCallback {
        private InputStream in;

        public FileUpload(InputStream in) {
            this.in = in;
        }

        public int send(OutputStream out) throws IOException {

            try {
                BufferedOutputStream bufferOut = new BufferedOutputStream(out);
                if (in instanceof FileInputStream) {
                    in = new BufferedInputStream(in);
                }
                byte[] buff = new byte[4096];
                int off = -1;
                while ((off = in.read(buff)) != -1) {
                    bufferOut.write(buff, 0, off);
                }
                bufferOut.flush();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                throw e;
            } finally {
                in.close();
            }
            return 0;
        }
    }

}
