package cn.com.cowain.ibms.rest.resp.meeting;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 会议预约开始时间
 *
 * @author: yanzy
 * @date: 2022/4/25 15:10
 */
@Data
@ApiModel("会议预约开始时间")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MeetingDateResp {

    @ApiModelProperty(value = "会议预约日期",example = "2022-04-25",position = 1)
    private String startDate;

    @ApiModelProperty(value = "会议开始时间",example = "08:00",position = 2)
    private String startTime;
}
