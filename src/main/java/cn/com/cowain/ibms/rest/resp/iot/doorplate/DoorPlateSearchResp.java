package cn.com.cowain.ibms.rest.resp.iot.doorplate;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 门牌数据查询结果实体类
 *
 * @author Yang.Lee
 * @date 2020/12/1 9:21
 */
@Data
public class DoorPlateSearchResp {

    @ApiModelProperty(value = "门牌ID", required = true)
    private String id;

    @ApiModelProperty(value = "门牌名称", required = true, position = 1)
    private String name;

    @ApiModelProperty(value = "门磁编号", required = true, position = 2)
    private String magneticNum;

    @ApiModelProperty(value = "会议室ID", required = true, position = 3)
    private String roomId;

    @ApiModelProperty(value = "会议室名称", required = true, position = 4)
    private String roomName;

    @ApiModelProperty(value = "设备密码", required = true, position = 5)
    private String password;

    @ApiModelProperty(value = "通讯时间", required = true, position = 6)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime connectTime;
}
