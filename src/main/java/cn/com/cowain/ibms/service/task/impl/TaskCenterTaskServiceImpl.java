package cn.com.cowain.ibms.service.task.impl;

import cn.com.cowain.ibms.dao.task.TaskCenterTaskDao;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.iot.AccessControlSpotApplicationRecord;
import cn.com.cowain.ibms.entity.task.TaskCenterTask;
import cn.com.cowain.ibms.enumeration.task.TaskOperateResult;
import cn.com.cowain.ibms.enumeration.task.TaskStatus;
import cn.com.cowain.ibms.enumeration.task.TaskType;
import cn.com.cowain.ibms.feign.task_center.TaskApi;
import cn.com.cowain.ibms.feign.task_center.bean.AccessControlSpotApplicationTaskData;
import cn.com.cowain.ibms.feign.task_center.bean.CreateTaskReq;
import cn.com.cowain.ibms.feign.task_center.bean.CreateTaskResp;
import cn.com.cowain.ibms.feign.task_center.bean.UpdateTaskReq;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.task.TaskCenterTaskService;
import cn.com.cowain.ibms.utils.DateUtils;
import cn.com.cowain.sfp.comm.JsonResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Objects;

/**
 * @author wei.cheng
 * @date 2022/03/10 15:59
 */
@Slf4j
@Service
public class TaskCenterTaskServiceImpl implements TaskCenterTaskService {
    @Resource
    private TaskApi taskApi;
    @Value("${ms-task-center.access-control-spot-application.template-id}")
    private String accessControlSpotApplicationTemplateId;
    @Value("${ms-task-center.access-control-spot-application.applicant-h5-url}")
    private String accessControlSpotApplicationClaimerUrl;
    @Value("${ms-task-center.access-control-spot-application.approver-h5-url}")
    private String accessControlSpotApplicationUrl;
    @Resource
    private TaskCenterTaskDao taskCenterTaskRecordDao;

    @Transactional
    @Override
    public TaskCenterTask createAccessControlSpotApplicationTask(SysUser approver, AccessControlSpotApplicationRecord applicationRecord) {
        CreateTaskReq req = new CreateTaskReq();
        LocalDateTime localDateTime = LocalDateTime.now();
        long now = localDateTime.toEpochSecond(ZoneOffset.of("+8"));
        req.setStartTimestamp(now);
        req.setArrivalTimestamp(now);
        req.setClaimer(applicationRecord.getApplicantName() + "/" + (SysUserService.isVisitor(applicationRecord.getApplicantHrId()) ? "劳务" : "员工"));
        req.setClaimerUid(applicationRecord.getApplicantHrId());
        req.setClaimerUrl(accessControlSpotApplicationClaimerUrl + applicationRecord.getId());
        AccessControlSpotApplicationTaskData data = AccessControlSpotApplicationTaskData.builder()
                .spotAddress(applicationRecord.getSpotAddress())
                .spotName(applicationRecord.getSpotName())
                .applicationTime(DateUtils.formatLocalDateTime(localDateTime, DateUtils.PATTERN_DATETIME1)).build();
        req.setData(data);
        req.setFromSys("IBMS");
        req.setOperateResult(TaskOperateResult.PENDING.getValue());
        req.setStatus(TaskStatus.PENDING.getValue());
        req.setTaskId(applicationRecord.getId());
        req.setTemplateId(accessControlSpotApplicationTemplateId);
        req.setType(TaskType.ACCESS_CONTROL_SPOT_APPLICATION.getName());
        req.setUcId(approver.getEmpNo());
        req.setUcName(approver.getNameZh());
        req.setUrl(accessControlSpotApplicationUrl + applicationRecord.getId());
        JsonResult<CreateTaskResp> result = taskApi.createTask(req);
        if (result.getStatus() == 0) {
            log.error("向待办中心创建task数据错误，请求参数：{}， 返回结果：{}", JSON.toJSONString(req), result);
            return null;
        }
        TaskCenterTask taskCenterTask = buildTaskCenterTask(req, localDateTime, result.getData());
        taskCenterTaskRecordDao.save(taskCenterTask);
        return taskCenterTask;
    }

    @Transactional
    @Override
    public TaskCenterTask createAccessControlSpotApplicationTask(String userHrId, String userName, AccessControlSpotApplicationRecord applicationRecord) {
        CreateTaskReq req = new CreateTaskReq();
        LocalDateTime localDateTime = LocalDateTime.now();
        long now = localDateTime.toEpochSecond(ZoneOffset.of("+8"));
        req.setStartTimestamp(now);
        req.setArrivalTimestamp(now);
        req.setClaimer(applicationRecord.getApplicantName() + "/" + (SysUserService.isVisitor(applicationRecord.getApplicantHrId()) ? "劳务" : "员工"));
        req.setClaimerUid(applicationRecord.getApplicantHrId());
        req.setClaimerUrl(accessControlSpotApplicationClaimerUrl + applicationRecord.getId());
        AccessControlSpotApplicationTaskData data = AccessControlSpotApplicationTaskData.builder()
                .spotAddress(applicationRecord.getSpotAddress())
                .spotName(applicationRecord.getSpotName())
                .applicationTime(DateUtils.formatLocalDateTime(localDateTime, DateUtils.PATTERN_DATETIME1)).build();
        req.setData(data);
        req.setFromSys("IBMS");
        req.setOperateResult(TaskOperateResult.PENDING.getValue());
        req.setStatus(TaskStatus.PENDING.getValue());
        req.setTaskId(applicationRecord.getId());
        req.setTemplateId(accessControlSpotApplicationTemplateId);
        req.setType(TaskType.ACCESS_CONTROL_SPOT_APPLICATION.getName());
        req.setUcId(userHrId);
        req.setUcName(userName);
        req.setUrl(accessControlSpotApplicationUrl + applicationRecord.getId());
        JsonResult<CreateTaskResp> result = taskApi.createTask(req);
        if (result.getStatus() == 0) {
            log.error("向待办中心创建task数据错误，请求参数：{}， 返回结果：{}", JSON.toJSONString(req), result);
            return null;
        }
        TaskCenterTask taskCenterTask = buildTaskCenterTask(req, localDateTime, result.getData());
        taskCenterTaskRecordDao.save(taskCenterTask);
        return taskCenterTask;
    }

    /**
     * 构建TaskCenterTask
     *
     * @param req
     * @param localDateTime
     * @param createTaskResp
     * @return
     */
    private TaskCenterTask buildTaskCenterTask(CreateTaskReq req, LocalDateTime localDateTime, CreateTaskResp createTaskResp) {
        TaskCenterTask taskCenterTask = new TaskCenterTask();
        taskCenterTask.setTaskCenterId(createTaskResp.getId());
        taskCenterTask.setArrivalTime(localDateTime);
        taskCenterTask.setClaimerName(req.getClaimer());
        taskCenterTask.setClaimerHrId(req.getClaimerUid());
        taskCenterTask.setClaimerUrl(req.getClaimerUrl());
        taskCenterTask.setData(JSON.toJSONString(req.getData()));
        taskCenterTask.setOperateResult(TaskOperateResult.PENDING);
        taskCenterTask.setStatus(TaskStatus.PENDING);
        taskCenterTask.setTaskId(req.getTaskId());
        taskCenterTask.setTemplateId(req.getTemplateId());
        taskCenterTask.setType(TaskType.ACCESS_CONTROL_SPOT_APPLICATION);
        taskCenterTask.setUcHrId(req.getUcId());
        taskCenterTask.setUcName(req.getUcName());
        taskCenterTask.setUrl(req.getUrl());
        return taskCenterTask;
    }

    @Transactional
    @Override
    public ServiceResult updateTask(TaskCenterTask taskCenterTask, UpdateTaskReq req) {
        JsonResult<Object> result = taskApi.updateTask(req);
        if (result.getStatus() == 0) {
            log.error("向待办中心更新task数据错误，请求参数：{}， 返回结果：{}", JSON.toJSONString(req), result);
            return ServiceResult.error("向待办中心更新task数据错误", result.getCode());
        }
        if (Objects.nonNull(req.getStatus())) {
            taskCenterTask.setStatus(TaskStatus.resolve(req.getStatus()));
        }
        if (Objects.nonNull(req.getOperateResult())) {
            taskCenterTask.setOperateResult(TaskOperateResult.resolve(req.getOperateResult()));
        }
        if (Objects.nonNull(req.getData())) {
            taskCenterTask.setData(JSONObject.toJSONString(req.getData()));
        }
        if (Objects.nonNull(req.getClaimerUrl())) {
            taskCenterTask.setClaimerUrl(req.getClaimerUrl());
        }
        taskCenterTaskRecordDao.save(taskCenterTask);
        return ServiceResult.ok(result.getData());
    }
}
