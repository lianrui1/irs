package cn.com.cowain.ibms.service.bean;

import lombok.Data;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/28 17:21
 */
@Data
public class HkAddUser {
    private String jobNo;
    private String msg;

}
