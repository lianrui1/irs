package cn.com.cowain.ibms.service;

import cn.com.cowain.ibms.dao.ReservationRecordItemDao;
import cn.com.cowain.ibms.dao.RoomDao;
import cn.com.cowain.ibms.dao.RoomServiceDao;
import cn.com.cowain.ibms.entity.ReservationRecord;
import cn.com.cowain.ibms.entity.ReservationRecordItem;
import cn.com.cowain.ibms.entity.Room;
import cn.com.cowain.ibms.entity.RoomService;
import cn.com.cowain.ibms.enumeration.ReservationRecordItemStatus;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.bean.ReservationRecordBean;
import cn.com.cowain.ibms.rest.req.*;
import cn.com.cowain.ibms.rest.resp.AppointmentsTimeCanBeMdeResp;
import cn.com.cowain.ibms.rest.resp.ReservationRecordDetailsResp;
import cn.com.cowain.ibms.rest.resp.RoomWithReservationResp;
import cn.com.cowain.ibms.rest.resp.TimeSlotResultResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import lombok.extern.slf4j.Slf4j;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@SpringBootTest
@Slf4j
class ReservationRecordServiceTest {
    @Autowired
    private ReservationRecordService reservationRecordService;

    @Autowired
    private DataFactory dataFactory;

    @Autowired
    private RoomDao roomDao;
    @Autowired
    private RoomServiceDao roomServiceDao;
    @Autowired
    private ReservationRecordItemDao reservationRecordItemDao;

    @Test
    void save() {
        for (int ii = 0; ii < 5; ii++) {
            log.info("build reservation save....");
            Set<Room> roomSet = roomDao.findByNameLike("%room%");
            log.info(String.valueOf(roomSet));
            if (roomSet != null && roomSet.size() > 0) {
                ReservationAddReq reservationAddReq = new ReservationAddReq();
                reservationAddReq.setRoomId(roomSet.iterator().next().getId());
                reservationAddReq.setTopic("test reservation" + dataFactory.getName());
                reservationAddReq.setDate(LocalDate.now());
                reservationAddReq.setFrom(LocalDateTime.now().plusMinutes(30));
                reservationAddReq.setTo(LocalDateTime.now().plusMinutes(60));
                SysUserBean sysUserBean = new SysUserBean();
                //创建人 胡荆陵
                sysUserBean.setSysId("5561");
                reservationAddReq.setInitiator(sysUserBean);
                reservationAddReq.setRemark("remark");
                List<SysUserBean> sysUserBeanList = new ArrayList<>();
                SysUserBean sysUserBean1 = new SysUserBean();
                //参会人 张宇鑫
                sysUserBean1.setSysId("1");
                sysUserBeanList.add(sysUserBean1);
                SysUserBean sysUserBean2 = new SysUserBean();
                //参会人王冬
                sysUserBean2.setSysId("3856");
                sysUserBeanList.add(sysUserBean2);
                reservationAddReq.setParticipantList(sysUserBeanList);
                List<RoomService> roomServiceList = roomServiceDao.findAll();
                String roomServiceString = "";
                for (int i = 0; i < roomServiceList.size(); i++) {
                    roomServiceString = new StringBuilder().append(roomServiceString).append(roomServiceList.get(i).getId()).toString();
                    if (i != roomServiceList.size() - 1) {
                        roomServiceString += ",";
                    }
                }
                reservationAddReq.setService(roomServiceString);
                ReservationRecord reservationRecord = reservationRecordService.save(reservationAddReq, "token");
                System.out.println(reservationRecord);
            }
        }
    }

    @Test
    void update() {
        List<ReservationRecordItem> reservationRecordItemList = reservationRecordItemDao.findAll();
        if (reservationRecordItemList.size() > 0) {
            String reservationId = reservationRecordItemList.get(0).getReservationRecord().getId();
            ReservationUpdateReq reservationUpdateReq = new ReservationUpdateReq();
            Set<Room> roomSet = roomDao.findByNameLike("%room%");
            if (roomSet != null && roomSet.size() > 0) {
                String id = roomSet.iterator().next().getId();

                reservationUpdateReq.setRoomId(id);
                reservationUpdateReq.setTopic("test reservation" + dataFactory.getName());
                reservationUpdateReq.setDate(LocalDate.now());
                reservationUpdateReq.setFrom(LocalDateTime.now().plusMinutes(30));
                reservationUpdateReq.setTo(LocalDateTime.now().plusMinutes(60));
                SysUserBean sysUserBean = new SysUserBean();
                //创建人 胡荆陵
                sysUserBean.setSysId("5561");
                reservationUpdateReq.setInitiator(sysUserBean);
                reservationUpdateReq.setRemark("remark");
                List<UserStatusBean> sysUserBeanList = new ArrayList<>();
                UserStatusBean sysUserBean1 = new UserStatusBean();
                sysUserBean1.setSysId("1");
                sysUserBean1.setStatus(ReservationRecordItemStatus.CONFIRM);
                sysUserBeanList.add(sysUserBean1);
                UserStatusBean sysUserBean2 = new UserStatusBean();
                sysUserBean2.setSysId("3856");
                sysUserBean2.setStatus(ReservationRecordItemStatus.CONFIRM);
                sysUserBeanList.add(sysUserBean2);
                UserStatusBean sysUserBean3 = new UserStatusBean();
                sysUserBean3.setSysId("4425");
                sysUserBean3.setStatus(ReservationRecordItemStatus.UNDETERMINED);
                sysUserBeanList.add(sysUserBean3);


                List<RoomService> roomServiceList = roomServiceDao.findAll();
                String roomServiceString = "";
                for (int i = 0; i < roomServiceList.size(); i++) {
                    roomServiceString = new StringBuilder().append(roomServiceString).append(roomServiceList.get(i).getId()).toString();
                    if (i != roomServiceList.size() - 1) {
                        roomServiceString += ",";
                    }
                }
                reservationUpdateReq.setParticipantList(sysUserBeanList);
                reservationUpdateReq.setService(roomServiceString);
                reservationRecordService.update(reservationId, reservationUpdateReq, "token");
            }
        }
    }

    @Test
    void getReservationRecordItemPage() {
        ReservationGetReq reservationGetReq = new ReservationGetReq();
//        reservationGetReq.setId("1");
        reservationGetReq.setPage(0);
        reservationGetReq.setSize(10);
        reservationGetReq.setDays(7);
        PageBean<ReservationRecordBean> reservationRecordBeanPageBean = reservationRecordService.getReservationRecordItemPage(reservationGetReq);
        System.out.println(reservationRecordBeanPageBean);

    }

    @Test
    void findBySysUserId() {
        System.out.println("模拟数据");
    }

    @Test
    void findBySysUserIdAndRoomId() {
    }

    @Test
    void getReservationRecordDetails() {
        List<ReservationRecordItem> reservationRecordItemList = reservationRecordItemDao.findAll();
        if (reservationRecordItemList.size() > 0) {
            String reservationId = reservationRecordItemList.get(0).getReservationRecord().getId();

            String ownerId = "1";
            String recordId = reservationId;
            ServiceResult reservationRecordDetailsResp = reservationRecordService.getReservationRecordDetails(ownerId, recordId, "token");
            System.out.println(reservationRecordDetailsResp);
        }
    }

    @Test
    void cancel() {


        List<ReservationRecordItem> reservationRecordItemList = reservationRecordItemDao.findAll();
        if (reservationRecordItemList.size() > 0) {
            String reservationId = reservationRecordItemList.get(0).getReservationRecord().getId();
            String id = reservationId;
            reservationRecordService.cancel(id, "token");
        }

    }

    @Test
    void updateInviteeStatus() {
        List<ReservationRecordItem> reservationRecordItemList = reservationRecordItemDao.findAll();
        if (reservationRecordItemList.size() > 0) {
            String reservationId = reservationRecordItemList.get(0).getReservationRecord().getId();
            String id = reservationId;

            InviteeStatusReq inviteeStatusReq = new InviteeStatusReq();
            inviteeStatusReq.setSysId("1");
            inviteeStatusReq.setStatus(ReservationRecordItemStatus.CANCEL);
            reservationRecordService.updateInviteeStatus(id, inviteeStatusReq);
        }
    }

    @Test
    void conflictDetect() {
    }

    @Test
    void findRecentListByUserId() {
    }

    @Test
    void checkReservation() {
    }

    @Test
    void nextValidTime() {
        String roomId = "001b0f74-7a4c-4a7e-9bb1-5a05abd7001f";
        AppointmentsTimeCanBeMdeResp appointmentsTimeCanBeMde = new AppointmentsTimeCanBeMdeResp();
        LocalDate today = LocalDate.now();
        LocalTime localTimeNow = LocalTime.now();
        RoomWithReservationResp roomWithReservationResp = reservationRecordService.findBySysUserAndRoomId(null, roomId, today);
        List<AppointmentsTimeCanBeMdeResp> appointmentsTimeCanBeMdeRespList = new ArrayList<>();
        for (TimeSlotResultResp it : roomWithReservationResp.getTimeSlotResultRespList()) {
            AppointmentsTimeCanBeMdeResp appointmentsTimeCanBeMdeResp = new AppointmentsTimeCanBeMdeResp();
            if (it.getStatus() == 1) {
                LocalTime localTimeFrom = LocalTime.parse(it.getFrom());
                if (localTimeFrom.isAfter(localTimeNow)) {
                    appointmentsTimeCanBeMdeResp.setFrom(localTimeFrom);
                    appointmentsTimeCanBeMdeResp.setTo(LocalTime.parse(it.getTo()));
                    appointmentsTimeCanBeMdeRespList.add(appointmentsTimeCanBeMdeResp);
                }
            }
        }
        LocalTime localTimeLastOneFrom = localTimeNow;
        LocalTime localTimeLastOneTo = localTimeNow;
        Boolean setBoolean = false;
        if (appointmentsTimeCanBeMdeRespList != null && appointmentsTimeCanBeMdeRespList.size() > 0) {
            for (AppointmentsTimeCanBeMdeResp it : appointmentsTimeCanBeMdeRespList) {
                // 上一条的 到 时间 等于 这一条的从时间  说明有连续1小时的 可预约
                if (it.getFrom().equals(localTimeLastOneTo)) {
                    appointmentsTimeCanBeMde.setFrom(localTimeLastOneFrom);
                    appointmentsTimeCanBeMde.setTo(it.getTo());
                    setBoolean = true;
                    break;
                }
                localTimeLastOneFrom = it.getFrom();
                localTimeLastOneTo = it.getTo();
            }
            // 没找到连续的1小时
            if (!setBoolean) {
                appointmentsTimeCanBeMde.setFrom(appointmentsTimeCanBeMdeRespList.get(0).getFrom());
                appointmentsTimeCanBeMde.setTo(appointmentsTimeCanBeMdeRespList.get(0).getTo());
            }
        }
        log.info(String.valueOf(appointmentsTimeCanBeMde));
    }
}