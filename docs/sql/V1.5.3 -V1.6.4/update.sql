
CREATE TABLE `ibms_prod`.`iot_illumination_sensing`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `created_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `version` int(11) NULL DEFAULT NULL COMMENT '版本号',
  `value` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '光照度数值',
  `device_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'FK。设备表主键id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '光照度传感器设备表' ROW_FORMAT = Dynamic;

CREATE TABLE `ibms_prod`.`iot_air_conditioner`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `created_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) NOT NULL COMMENT '是否已删除,0:否;1:是',
  `updated_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `version` int(11) NULL DEFAULT NULL COMMENT '版本号',
  `mode` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '模式',
  `fan_auto` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '风向 手动自动',
  `set_power` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '开关',
  `temperature` int(11) NULL DEFAULT NULL COMMENT '温度',
  `wind_direction` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '风向',
  `wind_speed` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '风速',
  `device_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'FK。设备表主键id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '空调设备表' ROW_FORMAT = Dynamic;

CREATE TABLE `ibms_prod`.`iot_huawei_token`  (
  `id` int(11) NOT NULL,
  `expires_at` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `token` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

ALTER TABLE `ibms_prod`.`iot_device` ADD COLUMN `hw_description` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '华为云中的设备描述' AFTER `seq`;

ALTER TABLE `ibms_prod`.`iot_device` ADD COLUMN `hw_device_id` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '华为云中的设备ID' AFTER `hw_description`;

ALTER TABLE `ibms_prod`.`iot_device` ADD COLUMN `hw_device_name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '华为云中的设备名称' AFTER `hw_device_id`;

ALTER TABLE `ibms_prod`.`iot_device` ADD COLUMN `hw_fw_version` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '华为云中的云设备的固件版本' AFTER `hw_device_name`;

ALTER TABLE `ibms_prod`.`iot_device` ADD COLUMN `hw_gateway_id` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '华为云中的网关id' AFTER `hw_fw_version`;

ALTER TABLE `ibms_prod`.`iot_device` ADD COLUMN `hw_node_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '华为云中的设备标识码' AFTER `hw_gateway_id`;

ALTER TABLE `ibms_prod`.`iot_device` ADD COLUMN `hw_node_type` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '华为云中的设备节点类型.ENDPOINT：非直连设备,GATEWAY：直连设备或网关,UNKNOWN：未知' AFTER `hw_node_id`;

ALTER TABLE `ibms_prod`.`iot_device` ADD COLUMN `hw_product_id` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '华为云中的设备关联的产品ID' AFTER `hw_node_type`;

ALTER TABLE `ibms_prod`.`iot_device` ADD COLUMN `hw_product_name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '华为云中的设备关联的产品名称' AFTER `hw_product_id`;

ALTER TABLE `ibms_prod`.`iot_device` ADD COLUMN `hw_sw_version` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '华为云中的设备的软件版本' AFTER `hw_product_name`;

ALTER TABLE `ibms_prod`.`iot_device` ADD COLUMN `hw_status` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '华为云中的设备的状态。ONLINE：设备在线。OFFLINE：设备离线。ABNORMAL：设备异常。INACTIVE：设备未激活。FROZEN：设备冻结' AFTER `hw_sw_version`;

ALTER TABLE `ibms_prod`.`iot_product` ADD COLUMN `hw_attribute` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '华为云硬件属性(json字符串)' AFTER `prod_seq`;

ALTER TABLE `ibms_prod`.`iot_product` ADD COLUMN `hw_create_time` datetime(0) NULL DEFAULT NULL COMMENT '华为云中的在物联网平台创建产品的时间' AFTER `hw_attribute`;

ALTER TABLE `ibms_prod`.`iot_product` ADD COLUMN `hw_data_format` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '华为云中的设备上报数据的格式，取值范围：json，binary。' AFTER `hw_create_time`;

ALTER TABLE `ibms_prod`.`iot_product` ADD COLUMN `hw_description` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '华为云中的设备描述' AFTER `hw_data_format`;

ALTER TABLE `ibms_prod`.`iot_product` ADD COLUMN `hw_manufacturer_name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '华为云中的厂商名称' AFTER `hw_industry`;

ALTER TABLE `ibms_prod`.`iot_product` ADD COLUMN `hw_name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '华为云中的产品名称' AFTER `hw_manufacturer_name`;

ALTER TABLE `ibms_prod`.`iot_product` ADD COLUMN `hw_product_id` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '华为云中的设备关联的产品ID' AFTER `hw_name`;

ALTER TABLE `ibms_prod`.`iot_product` ADD COLUMN `hw_protocol_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '华为云中的设备使用的协议类型。取值范围：MQTT，CoAP，HTTP，HTTPS，Modbus，ONVIF。' AFTER `hw_product_id`;

ALTER TABLE `ibms_prod`.`iot_product` ADD COLUMN `hw_service` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '华为云硬件提供的服务能力(json字符串)' AFTER `hw_protocol_type`;

ALTER TABLE `ibms_prod`.`iot_product` ADD COLUMN `remark` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '产品名称备注' AFTER `hw_service`;ALTER TABLE `ibms_prod`.`iot_product` ADD COLUMN `hw_device_type` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '华为云中的设备类型' AFTER `hw_description`;

ALTER TABLE `ibms_prod`.`iot_product` ADD COLUMN `hw_industry` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '华为云中的设备所属行业' AFTER `hw_device_type`;

ALTER TABLE `ibms_prod`.`iot_space` ADD COLUMN `device_num` int(2) NOT NULL COMMENT 'deviceNum' AFTER `project_id`;
ALTER TABLE `ibms_prod`.`iot_product` ADD COLUMN `remark` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '产品名称备注' AFTER `hw_service`;
ALTER TABLE `ibms_prod`.`iot_product`  MODIFY COLUMN `prod_name` varchar(356) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '产品名称' AFTER `iot_node_type`;
ALTER TABLE `ibms_prod`.`iot_device` MODIFY COLUMN `project_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT 'FK' AFTER `product_id`;
