package cn.com.cowain.ibms.rest.resp.space;

import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.enumeration.iot.SpaceType;
import com.google.common.collect.Lists;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
import java.util.Objects;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2022/3/28 13:33
 */
@Data
@ApiModel("空间楼层返回对象")
public class SpaceFloorResp {

    @ApiModelProperty(value = "空间ID")
    private String spaceId;

    @ApiModelProperty(value = "空间类型")
    private SpaceType spaceType;

    @ApiModelProperty(value = "空间名称")
    private String spaceName;

    @ApiModelProperty(value = "空间楼层")
    private String floor;

    @ApiModelProperty(value = "子空间信息")
    private List<SpaceFloorResp> spaceList;

    public static SpaceFloorResp convert(Space space) {
        if (Objects.isNull(space)) {
            return null;
        }
        SpaceFloorResp spaceFloorResp = new SpaceFloorResp();
        spaceFloorResp.setSpaceId(space.getId());
        spaceFloorResp.setSpaceType(space.getSpaceType());
        spaceFloorResp.setSpaceName(space.getName());
        spaceFloorResp.setFloor(space.getFloor());
        spaceFloorResp.setSpaceList(Lists.newArrayList());
        return spaceFloorResp;
    }
}
