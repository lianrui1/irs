package cn.com.cowain.ibms.rest.meeting;

import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.meeting.AttendancePageReq;
import cn.com.cowain.ibms.rest.resp.meeting.AttendancePageResp;
import cn.com.cowain.ibms.service.meeting.AttendanceService;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * 会议签到Controller
 *
 * @author Yang.Lee
 * @date 2021/7/8 10:30
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/meeting/attendance")
@Api(tags = IConst.MODULE_ATTENDANCE)
public class AttendanceController {

    @Resource
    private AttendanceService attendanceService;

    @GetMapping("/page")
    @ApiOperation(value = "查询会议签到分页数据", tags = "${project.sprint}")
    public ResponseEntity<JsonResult<PageBean<AttendancePageResp>>> getAttendancePage(AttendancePageReq req){

        return ResponseEntity.ok(JsonResult.ok(attendanceService.getPage(req)));
    }
}
