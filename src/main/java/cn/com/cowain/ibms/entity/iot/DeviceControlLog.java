package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author Yang.Lee
 * @date 2021/12/7 13:42
 */
@Data
@Entity
@Table(name = "iot_device_control_log")
@EqualsAndHashCode(callSuper = true)
@Where(clause = "is_del=0")
@org.hibernate.annotations.Table(appliesTo = "iot_device_control_log", comment = "设备控制记录表")
public class DeviceControlLog extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "device_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK'")
    private IotDevice device;

    @Column(name = "cmd", nullable = false, columnDefinition = "varchar(1000) COMMENT '控制指令'")
    private String cmd;

    @Column(name = "user_hr_id", nullable = false, columnDefinition = "varchar(20) COMMENT '用户工号'")
    private String userHrId;

    @Column(name = "user_name", nullable = false, columnDefinition = "varchar(20) COMMENT '用户姓名'")
    private String userName;
}
