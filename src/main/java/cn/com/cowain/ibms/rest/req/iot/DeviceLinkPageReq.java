package cn.com.cowain.ibms.rest.req.iot;

import cn.com.cowain.ibms.enumeration.iot.DeviceLinkStatus;
import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 设备联动分页查询对象
 */
@Data
@ApiModel("设备联动分页查询对象")
public class DeviceLinkPageReq extends PageReq{

    @ApiModelProperty(value = "联动状态" , position = 1)
    private DeviceLinkStatus status;

    @ApiModelProperty(value = "模糊查询关键字" , position = 2)
    private String keyword;

    @ApiModelProperty(value = "空间ID", position = 3)
    private String spaceId;

    @ApiModelProperty(value = "项目IDID", position = 4)
    private String projectId;
}
