package cn.com.cowain.ibms.rest.iot;

import cn.com.cowain.ibms.rest.req.iot.AlertSpotCreateReq;
import cn.com.cowain.ibms.rest.resp.iot.AlertSpotResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.iot.AlertSpotService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * 警戒点controller
 *
 * @author: yanzy
 * @date: 2022/4/13 10:34
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/alertSpot")
@Api(tags = IConst.MODULE_ALERT_SPOT)
public class AlertSpotController {

    @Resource
    private AlertSpotService alertSpotService;

    @GetMapping
    @ApiOperation(value = "查询所有警戒点", tags = {IConst.MODULE_ALERT_SPOT, IConst.V28})
    public ResponseEntity<JsonResult<List<AlertSpotResp>>> getAlertSpot() {

        return ResponseEntity.ok(JsonResult.ok("OK", alertSpotService.getAlertSpot()));
    }

    @PostMapping
    @ApiOperation(value = "创建警戒点", tags = {IConst.MODULE_ALERT_SPOT})
    public ResponseEntity<JsonResult<AlertSpotResp>> createAlertSpot(@RequestBody @Validated AlertSpotCreateReq req, BindingResult bindingResult) {
        ServiceResult result = alertSpotService.createAlertSpot(req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.ok((AlertSpotResp) result.getObject()));
    }
}
