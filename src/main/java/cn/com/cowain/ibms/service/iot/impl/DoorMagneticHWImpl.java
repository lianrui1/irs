package cn.com.cowain.ibms.service.iot.impl;

import cn.com.cowain.ibms.dao.iot.DoorMagneticDao;
import cn.com.cowain.ibms.entity.iot.DoorMagnetic;
import cn.com.cowain.ibms.service.bean.DoorMagneticHWParasReq;
import cn.com.cowain.ibms.service.bean.DoorMagneticHWReq;
import cn.com.cowain.ibms.service.bean.ServiceMessage;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.iot.DoorMagneticControlService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

/**
 * 门磁开门（华为云方式）实现类
 * @author Yang.Lee
 * @date 2021/1/28 10:55
 */
@Slf4j
@Service("doorMagneticHWService")
public class DoorMagneticHWImpl implements DoorMagneticControlService {

    @Value("${cowain.door-magnetic.huawei.open.url}")
    private String openUrl;

    @Autowired
    private DoorMagneticDao doorMagneticDao;

    @Autowired
    private RestTemplate restTemplate;

    /**
     * 门磁开门
     *
     * @param doorMagneticNum 门磁编号
     * @return
     */
    @Override
    @Transactional
    public ServiceResult open(String doorMagneticNum) {

        log.debug("进入华为云开门方法,门磁编号：" + doorMagneticNum);

        Optional<DoorMagnetic> doorMagneticOptional = doorMagneticDao.findByNumber(doorMagneticNum);
        if(!doorMagneticOptional.isPresent()){
            return ServiceResult.error(ServiceMessage.NOT_FOUNT_DOOR_MAGNETISM);
        }

        // 获取门磁信息
        DoorMagnetic doorMagnetic = doorMagneticOptional.get();
        String hwId = doorMagnetic.getHuaweiId();

        // 生成请求参数
        DoorMagneticHWReq doorMagneticHWReq = new DoorMagneticHWReq();
        doorMagneticHWReq.setDevice_id(hwId);
        doorMagneticHWReq.setCommand_name("doorAction");
        doorMagneticHWReq.setService_id("DoorManage");

        DoorMagneticHWParasReq doorMagneticHWParasReq = new DoorMagneticHWParasReq();
        doorMagneticHWParasReq.setAction("open");

        doorMagneticHWReq.setParas(doorMagneticHWParasReq);

        // json参数
        String requestParam = JSON.toJSONString(doorMagneticHWReq);

        log.debug("json参数 ： " + requestParam);

        // 发送Http请求
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity<>(requestParam, headers);
        ResponseEntity<String> responseEntity = null;
        try{
            responseEntity = restTemplate.postForEntity(openUrl, requestEntity, String.class);
        } catch(Exception e){
            log.error("华为云方式开门失败！！！", e);
            return ServiceResult.error("开门失败");
        }

        // 解析请求结果
        String responseBody = responseEntity.getBody();
        log.debug("华为云开门结果 response : " + responseBody);
        JSONObject jsonResult = JSON.parseObject(responseBody);
        Integer status = jsonResult.getInteger("status");
        if (status != 1) {
            return ServiceResult.error("设备控制失败");
        }
        String data = jsonResult.getString("data");
        JSONObject jsonData = JSON.parseObject(data);
        // 根据返回的数据中是否包含error_code判断请求成功与否

        if(jsonData.containsKey("error_code")){
            log.error("华为云开门失败！！！" + responseBody);
            return ServiceResult.error(responseBody);
        }

        return ServiceResult.ok(responseBody);
    }
}
