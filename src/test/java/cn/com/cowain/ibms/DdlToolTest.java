package cn.com.cowain.ibms;

import cn.com.cowain.ibms.component.DdlTool;
import cn.com.cowain.ibms.dao.iot.SpaceDao;
import cn.com.cowain.ibms.entity.iot.Space;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

/**
 * DdlTool
 */
@SpringBootTest
public class DdlToolTest {

    @Autowired
    private DdlTool ddlTool;


    @Autowired
    private SpaceDao spaceDao;

    /**
     * 生成 mysql 表结构
     */
    @Test
//    @Disabled
    public void generateTables() {
        ddlTool.generateTables();
    }

    @Test
    @Disabled
    public void ddl2File() {
        String filename = ddlTool.ddl2File("E:/temp/");
        System.out.println("文件已保存至:" + filename);
    }

    @Test
    public void ceshi() {
        System.out.println("文件已保存至:");

//        List<Space> spaces = spaceDao.findAll();
//        List<String> childMenu = new ArrayList<String>();
//        return treeMenuList(spaces, "2e56f906-d406-4599-9f84-b878f227b452", childMenu);
    }

    /**
     * 获取某个父节点下面的所有子节点
     *
     * @param menuList
     * @param pid
     * @return
     */
    public List<String> treeMenuList(List<Space> menuList, String pid, List<String> childMenu) {
        for (Space mu : menuList) {
            //遍历出父id等于参数的id，add进子节点集合
            if (mu.getParent() != null) {

                if (mu.getParent().getId() == pid) {
                    //递归遍历下一级
                    treeMenuList(menuList, mu.getId(), childMenu);
                    childMenu.add(mu.getId());
                }
            }

        }
        return childMenu;
    }
}
