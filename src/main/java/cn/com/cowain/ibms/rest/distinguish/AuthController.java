package cn.com.cowain.ibms.rest.distinguish;

import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.req.distinguish.AuthTimeDataReq;
import cn.com.cowain.ibms.rest.resp.distinguish.AuthTimeDataFailRep;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.disthinguish.AuthService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author wei.cheng
 * @date 2022/03/22 17:37
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/auth")
@Api(tags = IConst.DISTINGUISH)
public class AuthController {
    @Resource
    private AuthService authService;

    @PostMapping("/update/auth")
    @ApiOperation(value = "更新权限", tags = {IConst.DISTINGUISH, IConst.V27})
    public ResponseEntity<JsonResult<List<AuthTimeDataFailRep>>> updateAuth(@RequestBody @Validated List<AuthTimeDataReq> authTimeDataList) {
        ServiceResult result = authService.updateAuth(authTimeDataList);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.ok("OK", (List<AuthTimeDataFailRep>) result.getObject()));
    }

    @PostMapping("/recoveryCache")
    @ApiOperation(value = "权限缓存数据恢复", tags = {IConst.DISTINGUISH, IConst.V27})
    public ResponseEntity<JsonResult<String>> recoveryAuth(@ApiParam(value = "要恢复的数据起始时间，格式为`yyyy-MM-dd HH:mm:ss`") @RequestParam String startTime) {
        ServiceResult result = authService.recoveryAuth(startTime);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        return ResponseEntity.ok(JsonResult.ok("OK", (String) result.getObject()));
    }
}
