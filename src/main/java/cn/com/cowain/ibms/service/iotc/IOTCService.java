package cn.com.cowain.ibms.service.iotc;

import cn.com.cowain.ibms.feign.iotc.bean.CreateHTMemberGroupReq;
import cn.com.cowain.ibms.mq.bean.AccessPermissions;
import cn.com.cowain.ibms.service.bean.ServiceResult;

import java.util.List;

/**
 * iotc 相关功能处理类
 * author Yang.Lee
 *
 * @date 2022/1/27 15:15
 */
public interface IOTCService {

    /**
     * 用户绑定设备
     *
     * @param message 消息内容
     * @return 处理结果
     * @author Yang.Lee
     * @date 2022/1/27 15:16
     **/
    ServiceResult bindUserWithDevices(AccessPermissions message);

    /**
     * 添加用户
     *
     * @param message 消息内容
     * @return 处理结果
     * @author Yang.Lee
     * @date 2022/1/27 15:17
     **/
    ServiceResult addUser(AccessPermissions message);

    /**
     * 添加用户
     *
     * @param message 消息内容
     * @return 处理结果
     * @author Yang.Lee
     * @date 2022/1/27 15:17
     **/
    ServiceResult updateFace(AccessPermissions message);

    /**
     * 用户解绑设备
     *
     * @param message 消息内容
     * @return 处理结果
     * @author Yang.Lee
     * @date 2022/1/27 15:17
     **/
    ServiceResult unBindUserWithDevices(AccessPermissions message);

    /**
     * 删除用户
     *
     * @param message 消息内容
     * @return 处理结果
     * @author Yang.Lee
     * @date 2022/1/27 15:18
     **/
    ServiceResult delUser(AccessPermissions message);


    /**
     * 创建鸿图员工组
     *
     * @param req 员工组创建参数
     * @return 创建结果
     * @author Yang.Lee
     * @date 2022/2/14 13:31
     **/
    ServiceResult createStaffHTMemberGroup(CreateHTMemberGroupReq req);

    /**
     * 删除鸿图员工组
     *
     * @param groupId 员工组ID
     * @return 创建结果
     * @author Yang.Lee
     * @date 2022/2/14 13:31
     **/
    ServiceResult removeStaffHTMemberGroup(String groupId);

    /**
     * 从旷视鸿图的人员组中移除用户
     *
     * @param groupId      组ID
     * @param userHrIdList 用户工号集合
     * @return 集合
     * @author Yang.Lee
     * @date 2022/2/17 14:31
     **/
    ServiceResult removeUserFromKSHTGroup(String groupId, List<String> userHrIdList);

    /**
     * 向鸿图设备添加人员
     *
     * @param groupId 员工组ID
     * @param userHrIdList 用户工号集合
     * @author: yanzy
     * @data: 2022/3/24 17:03:44
     */
    ServiceResult addUserFromKSHTGroup(String groupId, List<String> userHrIdList);

    /**
     * 从hk和ks平台上查询人员是否存在
     *
     * @author: yanzy
     * @data: 2022/4/2 11:04:40
     */
    ServiceResult getUserFromPlatform(String jobNO);
}
