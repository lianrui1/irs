package cn.com.cowain.ibms.dao;

import cn.com.cowain.ibms.entity.Area;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2020/12/22 15:47
 */
@Repository
public interface AreaDao extends JpaRepository<Area, String> {

    /**
     * 获取所有省级信息
     * @return
     */
    List<Area> findByLevel(int i);

    /**
     * 根据省级id获取市列表
     *
     * @param id
     * @return
     */
    List<Area> findByLevelAndParentId(int i, Long id);

    Optional<Area> findById(Long projectId);
}
