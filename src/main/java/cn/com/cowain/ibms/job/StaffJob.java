package cn.com.cowain.ibms.job;

import cn.com.cowain.ibms.dao.SysUserDao;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.service.disthinguish.AccessRulesService;
import cn.com.cowain.ibms.service.disthinguish.StaffGroupService;
import cn.com.cowain.ibms.service.meeting.CustomUserGroupService;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.usercenter.api.service.UserCenterStaffApi;
import cn.com.cowain.usercenter.common.resp.PassportRespForFeign;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/6/7 9:34
 */
@Slf4j
@Component
public class StaffJob {

    @Resource
    private UserCenterStaffApi userCenterStaffApi;

    @Autowired
    private SysUserDao sysUserDao;

    @Autowired
    private StaffGroupService staffGroupService;

    @Autowired
    private CustomUserGroupService customUserGroupService;

    @Autowired
    private AccessRulesService accessRulesService;

    @Scheduled(cron = "0  59  23  *  *  ?")
    // 每天十一点59删除离职人员信息
    public void check(){
        JsonResult<List<PassportRespForFeign>> leaveStaffList = userCenterStaffApi.getLeaveStaffList();
        List<PassportRespForFeign> data = leaveStaffList.getData();
        // 获取所有未离职员工
        List<SysUser> sysUserList = sysUserDao.findByStatusIsNull();
        List<SysUser> sysUsers = new ArrayList<>();
        for(SysUser user : sysUserList){
            if(data.toString().contains(user.getEmpNo())){
                user.setStatus("leave");
                sysUsers.add(user);
            }
        }
        sysUserDao.saveAll(sysUsers);
        // 从该员工所在人员组删除此人
        List<SysUser> sysUsers1 = new ArrayList<>();
        for (PassportRespForFeign user : data){
            SysUser sysUser = new SysUser();
            sysUser.setNameZh(user.getRealName());
            sysUser.setEmpNo(user.getJobNo());
            sysUsers1.add(sysUser);
        }
        staffGroupService.delLeave(sysUsers1);

        // 从会议人员组删除此员工
        customUserGroupService.delLeave(sysUsers1);
    }

    @Scheduled(cron = "0 0/30 * * * ? ")
    public void visitor(){
        accessRulesService.getStaff();
    }
}
