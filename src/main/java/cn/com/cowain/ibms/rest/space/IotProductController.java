package cn.com.cowain.ibms.rest.space;

import cn.com.cowain.ibms.dao.space.IotAttributesDictDao;
import cn.com.cowain.ibms.entity.space.IotAttributesDict;
import cn.com.cowain.ibms.enumeration.IotNodeType;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.space.IotAttributesDictReq;
import cn.com.cowain.ibms.rest.req.space.ProductReq;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.iot.IotProductResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.space.IotProductService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * IOT产品 控制器
 *
 * @author yuchunlei
 * @date 2020-12-17
 * @since s3
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/iotProduct")
@Api(tags = IConst.PRODUCT_MANAGE)
public class IotProductController {

    @Autowired
    private IotProductService iotProductService;

    @Autowired
    private IotAttributesDictDao dictDao;


    /**
     * 产品管理-分页查询iot产品列表
     *
     * @param searchName
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/search")
    @ApiOperation(value = "iot产品列表", tags = IConst.PRODUCT_MANAGE)
    public ResponseEntity<JsonResult<PageBean<IotProductResp>>> search(
            @RequestParam(value = "searchName", required = false, defaultValue = "") String searchName,
            @RequestParam(value = "nodeType", required = false, defaultValue = "") IotNodeType nodeType,
            @RequestParam(value = "page", required = false, defaultValue = "0") int page,
            @RequestParam(value = "size", required = false, defaultValue = "30") int size) {

        // 参数：搜索条件, 分页页码,
        // 数据：数据，页码，总页数，
        PageBean<IotProductResp> pageBean = iotProductService.search(searchName, nodeType, page, size);
        JsonResult<PageBean<IotProductResp>> pageBeanJsonResult = JsonResult.ok("OK", pageBean);
        return ResponseEntity.ok(pageBeanJsonResult);
    }


    /**
     * 产品管理-添加iot产品
     *
     * @param productReq
     * @return
     */
    @PostMapping
    @ApiOperation(value = "添加产品", tags = IConst.PRODUCT_MANAGE)
    public ResponseEntity<JsonResult<IdResp>> save(@RequestBody @Validated ProductReq productReq, BindingResult bindingResult) {

        log.debug("save...");
        ServiceResult result = iotProductService.save(productReq);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        JsonResult<IdResp> jsonResult = JsonResult.ok("数据保存成功", (IdResp)result.getObject());
        return ResponseEntity.ok(jsonResult);
    }

    @PostMapping(value = "/dict")
    @ApiOperation(value = "数据点添加", tags = IConst.PRODUCT_MANAGE)
    public ResponseEntity<JsonResult<IdResp>> save(@RequestBody IotAttributesDictReq iotAttributesDictReq) {
        log.debug("添加数据点...");

        IotAttributesDict dict = new IotAttributesDict();
        dict.setName(iotAttributesDictReq.getName());
        dictDao.save(dict);
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }


    /**
     * 产品管理-编辑iot产品
     *
     * @param id
     * @param productReq
     * @param bindingResult
     * @return
     */
    @PutMapping("/{id}")
    @ApiOperation(value = "编辑iot产品", tags = IConst.PRODUCT_MANAGE)
    public ResponseEntity<JsonResult<String>> update(@PathVariable String id, @RequestBody @Validated ProductReq productReq,
                                                     BindingResult bindingResult) {
        log.debug("update...");
        ServiceResult serviceResult = iotProductService.update(id, productReq);
        JsonResult<String> jsonResult = JsonResult.okNoData("OK");
        if (!serviceResult.isSuccess()) {
            jsonResult = JsonResult.error(serviceResult.getObject().toString(), null, ErrConst.E01);
        }
        return ResponseEntity.ok(jsonResult);
    }


    /**
     * 产品管理-删除iot产品
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除iot产品", tags = IConst.PRODUCT_MANAGE)
    public ResponseEntity<JsonResult<String>> delete(@PathVariable String id) {
        log.debug("delete, id:" + id);
        ServiceResult serviceResult = iotProductService.deleteById(id);

        if(!serviceResult.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(serviceResult.getObject()), null, ErrConst.E01));
        }

        JsonResult<String> jsonResult = JsonResult.ok("OK", null);
        return ResponseEntity.ok(jsonResult);
    }


    /**
     * 产品管理-查看产品详情
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "查看产品详情", tags = IConst.PRODUCT_MANAGE)
    public ResponseEntity<JsonResult<IotProductResp>> getIotProductInfo(@PathVariable String id) {
        IotProductResp iotProductResp = iotProductService.getIotProductInfo(id);
        JsonResult<IotProductResp> respJsonResult = JsonResult.ok("OK", iotProductResp);
        return ResponseEntity.ok(respJsonResult);
    }


    /**
     * 数据点查询
     *
     * @return
     */
    @GetMapping(value = "/iotAttributes")
    @ApiOperation(value = "查询数据点", tags = IConst.PRODUCT_MANAGE)
    public ResponseEntity<JsonResult<List<IotAttributesDict>>> getIotAttributesDict() {
        log.debug("====数据点查询====");
        List<IotAttributesDict> result = iotProductService.getIotAttributesDictList();
        return ResponseEntity.ok(JsonResult.ok("OK", result));
    }


}
