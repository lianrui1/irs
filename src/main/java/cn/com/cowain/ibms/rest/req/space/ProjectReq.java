package cn.com.cowain.ibms.rest.req.space;

import cn.com.cowain.ibms.enumeration.space.ProjectPropertyRightType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * 项目请求数据实体类
 * @author Yang.Lee
 * @date 2020/12/17 17:12
 */
@Data
@ApiModel("项目信息")
public class ProjectReq {

    @ApiModelProperty(value = "主键ID")
    private String id;

    @Pattern(regexp = "^[a-z0-9A-Z\u4e00-\u9fa5]{1,15}$", message = "项目名称校验失败，只允许输入中文英文数字，且长度不超过15")
    @ApiModelProperty(value = "项目名称", required = true, position = 1)
    private String projectName;

    @NotNull
    @Length(max = 6)
    @ApiModelProperty(value = "省级ID", required = true, position = 2)
    private String provinceId;

    @NotNull
    @ApiModelProperty(value = "省级名称", required = true, position = 3)
    private String provinceName;

    @NotNull
    @Length(max = 6)
    @ApiModelProperty(value = "市级ID", required = true, position = 4)
    private String cityId;

    @NotNull
    @ApiModelProperty(value = "市级名称", required = true, position = 5)
    private String cityName;

    @NotNull
    @Length(max = 6)
    @ApiModelProperty(value = "区级ID", required = true, position = 6)
    private String districtId;

    @NotNull
    @ApiModelProperty(value = "区级名称", required = true, position = 7)
    private String districtName;

    @NotNull
    @Length(max = 100)
    @Pattern(regexp = "^[a-z0-9A-Z\u4e00-\u9fa5]{1,100}$", message = "项目详细地址校验失败，只允许输入中文英文数字，100")
    @ApiModelProperty(value = "详细地址", position = 8)
    private String address;

    @NotNull
    @ApiModelProperty(value = "项目区域ID", required = true, position = 9)
    private String projectArea;


//    @Pattern(regexp = "^[a-z0-9A-Z\u4e00-\u9fa5]{1,50}$", message = "项目单位校验失败，只允许输入中文英文数字，且长度不超过50")
    @ApiModelProperty(value = "单位", position = 8)
    private String visitCompany;

    @NotNull
    @ApiModelProperty(value = "产权属性", required = true)
    private ProjectPropertyRightType propertyRightType;

    @ApiModelProperty(value = "面积")
    private String areaM2;

    @ApiModelProperty(value = "简介")
    private String introduction;

    @ApiModelProperty(value = "项目图片")
    private String img;

    @ApiModelProperty(value = "管理员工号列表")
    private List<String> adminHrIdList;

}
