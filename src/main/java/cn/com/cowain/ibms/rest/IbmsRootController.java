package cn.com.cowain.ibms.rest;

import cn.com.cowain.sfp.rest.RootController;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * IBMS RootController
 *
 * @author HJL
 */
@RestController
@ApiIgnore
@Api(
        hidden = true
)
public class IbmsRootController extends RootController {

    @GetMapping(API_BASE)
    @Override
    public String blank() {
        return super.blank();
    }

}
