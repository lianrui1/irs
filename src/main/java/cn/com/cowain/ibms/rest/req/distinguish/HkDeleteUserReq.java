package cn.com.cowain.ibms.rest.req.distinguish;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/7/22 18:23
 */
@Data
public class HkDeleteUserReq {

    @ApiModelProperty(value = "人员工号", required = true)
    private List<String> jobNo;
    @ApiModelProperty(value = "设备ip", required = true)
    private  String ip;
    @ApiModelProperty(value = "是否结束", required = true)
    private String over;
}
