package cn.com.cowain.ibms.rest.req.iot;

import cn.com.cowain.ibms.rest.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wei.cheng
 * @date 2022/03/30 13:28
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "梯控按楼层列表分页请求对象")
public class ElevatorSpotPageReq extends PageReq {

    @ApiModelProperty(value = "项目ID，根据所属项目筛选")
    private String projectId;

    @ApiModelProperty(value = "空间ID，根据所属楼宇或者楼层筛选")
    private String spaceId;
}
