package cn.com.cowain.ibms.feign.hk_server_connector.ks.fallback;

import cn.com.cowain.ibms.feign.hk_server_connector.ks.HqAccessRecordService;
import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.req.hk.FaceRecord;
import cn.com.cowain.ibms.rest.resp.EquipmentDataResp;
import cn.com.cowain.ibms.rest.resp.SearchStaffDataResp;
import cn.com.cowain.ibms.utils.ErrConst;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author tql
 * @Description HqAccessRecordServiceFallBack 服务降级
 * @Date 21-5-21 下午2:05
 * @Version 1.0
 */
@Component
public class HqAccessRecordServiceFallBack implements HqAccessRecordService {
    /**
     * 获取总部考勤记录
     *
     * @param faceRecord 开始时间/结束时间 example:2021-05-15
     * @return
     */
    @Override
    public JsonResult<List<SearchStaffDataResp>> searchStaff(FaceRecord faceRecord) {
        return JsonResult.error("请求失败", null, ErrConst.E01);
    }

    /**
     * 健康检测
     *
     * @return
     */
    @Override
    public JsonResult healthCheck() {
        return JsonResult.error("请求失败", null, ErrConst.E01);
    }

    /**
     * 获取设备列表
     *
     * @return
     */
    @Override
    public JsonResult<List<EquipmentDataResp>> getEquipmentList() {
        return JsonResult.error("请求失败", null, ErrConst.E01);
    }
}
