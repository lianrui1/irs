package cn.com.cowain.ibms.service.ota;

import cn.com.cowain.ibms.enumeration.ota.Application;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.oat.UpgradePageReq;
import cn.com.cowain.ibms.rest.req.oat.UpgradeTaskCreateReq;
import cn.com.cowain.ibms.rest.resp.ota.UpgradeTaskDetailResp;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import org.springframework.web.multipart.MultipartFile;


public interface AppUpgradeService {


    /*
     * 根据升级应用 版本号 任务状态 查询升级任务 按创建时间倒叙
     *
     */
    PageBean<UpgradeTaskDetailResp> search(UpgradePageReq req);

    /*
     * 创建升级任务
     *
     */
    ServiceResult save(UpgradeTaskCreateReq upgradeTaskCreateReq);

    /*
     * 查询指定升级任务
     *
     */
    ServiceResult get(String id);

    /*
     * 更新指定升级任务
     *
     */
    ServiceResult update(String id, UpgradeTaskCreateReq upgradeTaskCreateReq);

    /*
     * 删除指定升级任务
     *
     */
    ServiceResult delete(String id);

    /*
     * App查询是否有升级任务
     *
     */
    ServiceResult getTask(Application application, String version);

    /*
     * 文件上传接口
     *
     */
    ServiceResult upload(MultipartFile multipartFile);

    /*
     * App下载升级包
     *
     */
    ServiceResult download(String id);
}
