package cn.com.cowain.ibms.rest.resp.space;

import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.space.SpacePurpose;
import cn.com.cowain.ibms.enumeration.iot.SpaceType;
import cn.com.cowain.ibms.service.space.SpaceService;
import cn.com.cowain.ibms.utils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * @author Yang.Lee
 * @date 2021/11/11 14:03
 */
@Data
@ApiModel("空间详情返回对象")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SpaceDetailResp implements Serializable {

    @ApiModelProperty(value = "主键ID", required = true)
    private String id;

    @ApiModelProperty(value = "名称", required = true, position = 1)
    private String name;

    @ApiModelProperty(value = "空间全称", position = 2)
    private String fullName;

    @ApiModelProperty(value = "空间层级", required = true, position = 3)
    private int level;

    @JsonFormat(pattern = DateUtils.PATTERN_DATETIME1)
    @ApiModelProperty(value = "创建时间", required = true, position = 4)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "空间编号", required = true, position = 5)
    private String position;

    @ApiModelProperty(value = "空间类型", required = true, position = 6)
    private SpaceType spaceType;

    @ApiModelProperty(value = "空间类型名称", required = true, position = 7)
    private String spaceTypeName;

    @ApiModelProperty(value = "用途类型", required = true, position = 8)
    private String spacePurpose;

    /**
     * 对象转换
     *
     * @param source 原对象
     * @return 转换后对象
     * @author Yang.Lee
     * @date 2021/11/11 14:27
     **/
    public static SpaceDetailResp convert(Space source) {

        return SpaceDetailResp.builder()
                .id(source.getId())
                .name(source.getName())
                .fullName(SpaceService.getFullSpaceName(source, source.getName()))
                .level(source.getLevel())
                .createTime(source.getCreatedTime())
                .position(Optional.ofNullable(source.getPosition()).orElse(""))
                .spaceType(source.getSpaceType())
                .spaceTypeName(source.getSpaceType().getName())
                .spacePurpose(Optional.ofNullable(source.getSpacePurpose()).map(SpacePurpose::getName).orElse(""))
                .build();
    }
}
