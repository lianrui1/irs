package cn.com.cowain.ibms.rest;

import cn.com.cowain.ibms.rest.bean.JsonResult;
import cn.com.cowain.ibms.rest.req.office.LoginReq;
import cn.com.cowain.ibms.rest.resp.office.UserLoginResp;
import cn.com.cowain.ibms.service.office.LoginService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.utils.IConst;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author Yang.Lee
 * @date 2021/12/20 16:37
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/uc/v2")
@Api(tags = IConst.MODULE_UC_V2)
public class UCV2Controller {


    @Resource
    private LoginService loginService;

    /**
     * 智能办公室小程序登录
     *
     * @param loginReq 请求参数
     * @return 登录信息
     * @author Yang.Lee
     * @date 2021/12/20 17:31
     **/
    @PostMapping("/office/login")
    @ApiOperation(value = "智能办公室小程序登录", tags = {IConst.MODULE_UC_V2, IConst.V21})
    public ResponseEntity<JsonResult<UserLoginResp>> getUserInfo(@RequestBody LoginReq loginReq) {

        ServiceResult serviceResult = loginService.login(loginReq.getCode());

        if(!serviceResult.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(serviceResult.getObject()), null, serviceResult.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.ok("OK", (UserLoginResp)serviceResult.getObject()));
    }

    @PostMapping("/office/login/byToken")
    @ApiOperation(value = "智能办公室小程序登录(ByToken)", tags = {IConst.MODULE_UC_V2, IConst.V21})
    public ResponseEntity<JsonResult<UserLoginResp>> getUserInfoByToken(@RequestHeader("Authorization") String token) {

        ServiceResult serviceResult = loginService.loginByToken(token);

        if(!serviceResult.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(serviceResult.getObject()), null, serviceResult.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.ok("OK", (UserLoginResp)serviceResult.getObject()));
    }
}
