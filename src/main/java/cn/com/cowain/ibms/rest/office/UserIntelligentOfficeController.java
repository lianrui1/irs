package cn.com.cowain.ibms.rest.office;

import cn.com.cowain.ibms.rest.req.office.DeviceCommonReq;
import cn.com.cowain.ibms.rest.req.office.UserOfficeReq;
import cn.com.cowain.ibms.rest.resp.office.OccupyOfficeResp;
import cn.com.cowain.ibms.rest.resp.office.OfficeDetailDeviceResp;
import cn.com.cowain.ibms.rest.resp.office.ShowDeviceResp;
import cn.com.cowain.ibms.rest.resp.office.TbDataDetailResp;
import cn.com.cowain.ibms.service.office.UserIntelligentOfficeService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author feng
 * @title: UserIntelligentOfficeController
 * @projectName bims
 * @Date 2021/12/7 16:18
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/useroffice")
@Api(tags = IConst.INTELLIGENT_OFFICE)
public class UserIntelligentOfficeController {

    @Autowired
    private  UserIntelligentOfficeService userIntelligentOfficeService;
    @PostMapping("/initializeoffice")
    @ApiOperation(value = "用户初始化智能办公室", tags = {IConst.INTELLIGENT_OFFICE, IConst.V21})
    public ResponseEntity<JsonResult<String>> userInitialize(@RequestBody @Validated @ApiParam("办公室信息") UserOfficeReq req) {
        ServiceResult  result=  userIntelligentOfficeService.saveOffice(req);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }
    @PostMapping("/editofficename")
    @ApiOperation(value = "办公室名称编辑", tags = {IConst.INTELLIGENT_OFFICE, IConst.V21})
    public ResponseEntity<JsonResult<String>> editOfficeName(@RequestBody  @ApiParam("办公室信息") UserOfficeReq req) {
        if(StringUtils.isEmpty(req.getOfficeName())||StringUtils.isEmpty(req.getOfficeId())){
            return ResponseEntity.ok(JsonResult.error("办公室名称或者id不能为空", null, null));
        }
        if(!StringUtils.isEmpty(req.getOfficeName())&&req.getOfficeName().length()>20){
            return ResponseEntity.ok(JsonResult.error("字符长度不能超过20个，请重新编辑", null, null));
        }
        userIntelligentOfficeService.editOffice(req);
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    @PostMapping("/finishguide")
    @ApiOperation(value = "保存完成指引", tags = {IConst.INTELLIGENT_OFFICE, IConst.V21})
    public ResponseEntity<JsonResult<String>> finishguide() {
        userIntelligentOfficeService.saveGuide();
        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }
    @GetMapping("/finddevice/{officeId}")
    @ApiOperation(value = "发现设备", tags = {IConst.INTELLIGENT_OFFICE, IConst.V21})
    public ResponseEntity<JsonResult< List<ShowDeviceResp> >> findDevice(@PathVariable String officeId) {
        ServiceResult result= userIntelligentOfficeService.findNewDevice(officeId);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }

        return ResponseEntity.ok(JsonResult.ok("OK",(List<ShowDeviceResp>)result.getObject()));
    }
    @GetMapping("/finddevice/devicelist/{officeId}")
    @ApiOperation(value = "办公室详情-设备列表", tags = {IConst.INTELLIGENT_OFFICE, IConst.V21})
    public ResponseEntity<JsonResult<OfficeDetailDeviceResp>> deviceList(@PathVariable String officeId) {
        ServiceResult result= userIntelligentOfficeService.showDevice(officeId);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.ok("OK",(OfficeDetailDeviceResp)result.getObject()));
    }
    @GetMapping("/officesensor/{officeId}")
    @ApiOperation(value = "办公室详情传感器信息查询", tags = {IConst.INTELLIGENT_OFFICE, IConst.V21})
    public ResponseEntity<JsonResult<TbDataDetailResp>> showDevice(@PathVariable String officeId) {
        TbDataDetailResp resp=  userIntelligentOfficeService.sensor(officeId);
        return ResponseEntity.ok(JsonResult.ok("OK",resp));
    }
    @GetMapping("/findoffice")
    @ApiOperation(value = "办公室列表查询", tags = {IConst.INTELLIGENT_OFFICE, IConst.V21})
    public ResponseEntity<JsonResult< List<OccupyOfficeResp> >> findOfficeList() {

        ServiceResult result=  userIntelligentOfficeService.hasControlOffice();
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.ok("OK",( List<OccupyOfficeResp> )result.getObject()));
    }
   @GetMapping("/powerscope/{officeId}")
    @ApiOperation(value = "用户是否有权限", tags = {IConst.INTELLIGENT_OFFICE, IConst.V21})
    public ResponseEntity<JsonResult<Boolean>> powerScope(@PathVariable String officeId) {
       Boolean result=  userIntelligentOfficeService.powerScope(officeId);
        return ResponseEntity.ok(JsonResult.ok("OK",result));
    }
    @PutMapping("/hidedevice/{officeId}")
    @ApiOperation(value = "隐藏设备", tags = {IConst.INTELLIGENT_OFFICE, IConst.V21})
    public ResponseEntity<JsonResult<Object>> powerScope(@PathVariable String officeId,@RequestBody DeviceCommonReq req) {
        ServiceResult result=  userIntelligentOfficeService.hideDevice(req,officeId);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.ok("OK",result.getObject()));
    }
    @GetMapping("/finddevice/forweb/{officeId}")
    @ApiOperation(value = "当前办公室可控设备列表", tags = {IConst.INTELLIGENT_OFFICE, IConst.V21})
    public ResponseEntity<JsonResult< List<ShowDeviceResp> >> findForWebDevice(@PathVariable String officeId) {
        ServiceResult result= userIntelligentOfficeService.filterDevice(officeId);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.ok("OK",(List<ShowDeviceResp>)result.getObject()));
    }
}
