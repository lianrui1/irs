package cn.com.cowain.ibms.service.person_permission.impl;

import cn.com.cowain.ibms.dao.office.IntelligentOfficeDao;
import cn.com.cowain.ibms.dao.iot.DeviceDao;
import cn.com.cowain.ibms.dao.space.SpaceAdminDao;
import cn.com.cowain.ibms.dao.space.SpaceDeviceControlDao;
import cn.com.cowain.ibms.entity.SysUser;
import cn.com.cowain.ibms.entity.office.IntelligentOffice;
import cn.com.cowain.ibms.entity.iot.IotDevice;
import cn.com.cowain.ibms.entity.iot.Space;
import cn.com.cowain.ibms.entity.space.SpaceAdmin;
import cn.com.cowain.ibms.entity.space.SpaceDeviceControl;
import cn.com.cowain.ibms.rest.req.person_permission.DepartmentPersonPermissionCreateReq;
import cn.com.cowain.ibms.rest.req.person_permission.PersonPermissionCreateReq;
import cn.com.cowain.ibms.service.SysUserService;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.person_permission.PersonPermissionService;
import cn.com.cowain.ibms.service.space.SpaceService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.sfp.comm.JsonResult;
import cn.com.cowain.usercenter.api.service.UserCenterStaffApi;
import cn.com.cowain.usercenter.common.req.PassportRoleEditReq;
import cn.com.cowain.usercenter.common.resp.PassportRespForFeign;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @Author tql
 * @Description 设备人员权限 服务实现类
 * @Date 21-10-18 下午6:50
 * @Version 1.0
 */
@Slf4j
@Service
public class PersonPermissionServiceImpl implements PersonPermissionService {


    @Autowired
    private SpaceDeviceControlDao spaceDeviceControlDao;

    @Autowired
    private DeviceDao deviceDao;

    @Resource(name = "sysUserServiceUC")
    private SysUserService sysUserService;

    @Autowired
    private UserCenterStaffApi userCenterStaffApi;

    @Resource
    private SpaceService spaceService;

    @Resource
    private SpaceAdminDao spaceAdminDao;

    @Resource
    private IntelligentOfficeDao intelligentOfficeDao;

    /**
     * 添加人员权限
     *
     * @param personPermissionCreateReq
     * @return
     */
    @Override
    @Transactional
    public ServiceResult save(PersonPermissionCreateReq personPermissionCreateReq) {

        personPermissionCreateReq.getDeviceId().forEach(device -> {
            Optional<IotDevice> byId = deviceDao.findById(device);
            List<SpaceDeviceControl> byDeviceId = spaceDeviceControlDao.findByDeviceId(device);
            // 获取空间设备下的人员工号
            List<String> byDeviceIds = byDeviceId.stream().map(SpaceDeviceControl::getUserEmpNo).collect(Collectors.toList());
            List<SpaceDeviceControl> objects = new ArrayList<>();

            if (byDeviceId.isEmpty()) {
                personPermissionCreateReq.getWorkNo().forEach(s -> {

                    SysUser sysUser = sysUserService.getUserByEmpNo(s);

                    if (sysUser == null) {
                        log.error("添加人员权限失败，未找到用户{}", s);
                        return;
                    }

                    SpaceDeviceControl spaceDeviceControl = new SpaceDeviceControl();
                    spaceDeviceControl.setDevice(byId.get());
                    spaceDeviceControl.setUserEmpNo(s);
                    spaceDeviceControl.setUserName(sysUser.getNameZh());
                    objects.add(spaceDeviceControl);
                });

            } else {
                List<String> stringList = personPermissionCreateReq.getWorkNo().stream().filter(e -> !byDeviceIds.contains(e)).collect(Collectors.toList());

                stringList.forEach(s -> {
                    SysUser sysUser = sysUserService.getUserByEmpNo(s);
                    if (sysUser == null) {
                        log.error("添加人员权限失败，未找到用户{}", s);
                        return;
                    }

                    SpaceDeviceControl spaceDeviceControl = new SpaceDeviceControl();
                    spaceDeviceControl.setDevice(byId.get());
                    spaceDeviceControl.setUserEmpNo(s);
                    spaceDeviceControl.setUserName(sysUser.getNameZh());
                    objects.add(spaceDeviceControl);
                });
            }
            spaceDeviceControlDao.saveAll(objects);
        });

        // 给用户添加UC的权限
        // 判断添加权限的空间是否是智能会议室，如果是，则开通uc权限
        PassportRoleEditReq passportRoleEditReq = new PassportRoleEditReq();
        passportRoleEditReq.setAddUserHrIdList(personPermissionCreateReq.getWorkNo());
        passportRoleEditReq.setRmUserHrIdList(new ArrayList<>());

        ServiceResult serviceResult = spaceService.editUCIntelligentOfficeRole(personPermissionCreateReq.getSpaceId(), passportRoleEditReq);
        if(!serviceResult.isSuccess()){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return serviceResult;
        }

        return ServiceResult.ok();
    }

    @Override
    public ServiceResult departmentSave(DepartmentPersonPermissionCreateReq personPermissionCreateReq) {
        String[] stringDepartments = new String[personPermissionCreateReq.getDepartments().size()];
        personPermissionCreateReq.getDepartments().forEach(department -> stringDepartments[personPermissionCreateReq.getDepartments().indexOf(department)] = department);
        // 根据部门Id获取所有用户
        JsonResult<List<PassportRespForFeign>> listJsonResult = userCenterStaffApi.passportList(stringDepartments);
        log.info("Uc组织人员列表入参:{}", stringDepartments);
        // 判断返回
        if (listJsonResult.getStatus() != 1) {
            return ServiceResult.error(listJsonResult.getMsg());
        }
        // 获取组织下的人员
        List<PassportRespForFeign> personList = listJsonResult.getData();

        personPermissionCreateReq.getDeviceId().forEach(device -> {
            Optional<IotDevice> byId = deviceDao.findById(device);
            List<SpaceDeviceControl> byDeviceId = spaceDeviceControlDao.findByDeviceId(device);
            // 获取空间设备下的人员工号
            List<String> devicePersons = byDeviceId.stream().map(SpaceDeviceControl::getUserEmpNo).collect(Collectors.toList());
            List<SpaceDeviceControl> persons = new ArrayList<>();

            if (devicePersons.isEmpty()) {
                // 如果设备人员为空直接添加
                personList.forEach(s -> {

                    SysUser sysUser = sysUserService.getUserByEmpNo(s.getJobNo());

                    if (sysUser == null) {
                        log.error("添加人员权限失败，未找到用户{}", s);
                        return;
                    }

                    SpaceDeviceControl spaceDeviceControl = new SpaceDeviceControl();
                    spaceDeviceControl.setDevice(byId.get());
                    spaceDeviceControl.setUserEmpNo(s.getJobNo());
                    spaceDeviceControl.setUserName(sysUser.getNameZh());
                    persons.add(spaceDeviceControl);
                });

            } else {
                // 过滤出还没存在的人员
                List<String> stringList = personList.stream().filter(e -> !devicePersons.contains(e.getJobNo())).map(PassportRespForFeign::getJobNo).collect(Collectors.toList());

                stringList.forEach(s -> {
                    SysUser sysUser = sysUserService.getUserByEmpNo(s);
                    if (sysUser == null) {
                        log.error("添加人员权限失败，未找到用户{}", s);
                        return;
                    }

                    SpaceDeviceControl spaceDeviceControl = new SpaceDeviceControl();
                    spaceDeviceControl.setDevice(byId.get());
                    spaceDeviceControl.setUserEmpNo(s);
                    spaceDeviceControl.setUserName(sysUser.getNameZh());
                    persons.add(spaceDeviceControl);
                });
            }
            spaceDeviceControlDao.saveAll(persons);

        });

        return ServiceResult.ok();
    }

    /**
     * 删除设备人员权限
     *
     * @param empNo
     * @param spaceId
     * @return
     */
    @Override
    @Transactional
    public ServiceResult deleteByEmpNoAndSpaceId(String empNo, String spaceId) {

        // 判断移除的用户是否是空间管理员，如果是则不能移除
        Optional<SpaceAdmin> spaceAdminOptional = spaceAdminDao.findBySpaceIdAndAdminEmpNo(spaceId, empNo);
        if(spaceAdminOptional.isPresent()){
            return ServiceResult.error("该用户是此空间的管理员，无法删除权限，请更换管理员后再删除", ErrConst.E01);
        }

        List<IotDevice> bySpaceId = deviceDao.findBySpaceId(spaceId);
        List<String> stringList = bySpaceId.stream().map(IotDevice::getId).collect(Collectors.toList());

        // 根据空间获取设备列表
        List<SpaceDeviceControl> bySpaceIdAndUserEmpNo = spaceDeviceControlDao.findByUserEmpNoAndDeviceIdIn(empNo, stringList);
        List<SpaceDeviceControl> objects = new ArrayList<>();
        bySpaceIdAndUserEmpNo.forEach(e -> {
            e.setIsDelete(1);
            objects.add(e);
        });
        spaceDeviceControlDao.saveAll(objects);

        // 判断空间是否为智能办公室
        Optional<IntelligentOffice> intelligentOfficeOptional = intelligentOfficeDao.findBySpaceId(spaceId);
        if (intelligentOfficeOptional.isPresent()) {
            // 判断该用户是否对办公室下其他设备有操作权限

            List<SpaceDeviceControl> spaceDeviceControlList = spaceDeviceControlDao.findByDeviceSpaceIdAndUserEmpNo(spaceId, empNo);
            if (spaceDeviceControlList.isEmpty()) {

                // 判断该用户是否有其他智能会议室的权限
                List<SpaceAdmin> spaceAdminList = spaceAdminDao.findByAdminEmpNo(empNo);
                Page<IntelligentOffice> intelligentOfficePage = intelligentOfficeDao.findBySpaceIdIn(spaceAdminList.stream()
                                .map(SpaceAdmin::getSpace)
                                .map(Space::getId)
                                .collect(Collectors.toList()),
                        PageRequest.of(0, 9999));


                if(intelligentOfficePage.getTotalElements() == 0){
                    PassportRoleEditReq passportRoleEditReq = new PassportRoleEditReq();
                    passportRoleEditReq.setAddUserHrIdList(new ArrayList<>());
                    passportRoleEditReq.setRmUserHrIdList(Arrays.asList(empNo));

                    ServiceResult serviceResult = spaceService.editUCIntelligentOfficeRole(spaceId, passportRoleEditReq);
                    if (!serviceResult.isSuccess()) {
                        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                        return serviceResult;
                    }
                }
            }
        }

        return ServiceResult.ok();
    }

    @Override
    public ServiceResult spaceDetail(String id) {
        return null;
    }
}
