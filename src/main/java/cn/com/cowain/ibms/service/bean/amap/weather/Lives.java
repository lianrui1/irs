package cn.com.cowain.ibms.service.bean.amap.weather;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 实时天气信息
 *
 * @author Yang.Lee
 * @date 2021/4/19 14:36
 */
@Data
public class Lives{

    /**
     * 省份名
     **/
    private String province;

    /**
     * 城市名
     **/
    private String city;

    /**
     * 区域编码
     **/
    @JSONField(name = "adcode")
    private String adCode;

    /**
     * 天气现象
     **/
    private String weather;

    /**
     * 实时气温，单位：摄氏度
     **/
    private String temperature;

    /**
     * 风向描述
     **/
    @JSONField(name = "winddirection")
    private String windDirection;

    /**
     * 风力级别，单位：级
     **/
    @JSONField(name = "windpower")
    private String windPower;

    /**
     * 空气湿度
     **/
    private String humidity;

    /**
     * 数据发布的时间
     **/
    @JSONField(name = "reporttime")
    private String reportTime;
}
