package cn.com.cowain.ibms.rest.req.qrcode;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * 二维码通行权限数据请求对象
 *
 * @author Yang.Lee
 * @date 2021/4/6 15:07
 */
@Data
@ApiModel("二维码通行权限请求参数")
public class QrcodeAccessRuleReq {

    /**
     * 门磁表数据主键
     **/
    @NotBlank(message = "设备不能为空")
    @ApiModelProperty(value = "门磁设备ID", required = true, example = "11223344", position = 1)
    private String magneticDeviceId;

    @NotEmpty(message = "人员组不能为空")
    @ApiModelProperty(value = "包含人员组ID", required = true, example = "[1,2]", position = 2)
    private List<String> staffGroups;

    @NotBlank(message = "时间计划不能为空")
    @ApiModelProperty(value = "时间计划", required = true, example = "123123", position = 3)
    private String timePlanId;
}
