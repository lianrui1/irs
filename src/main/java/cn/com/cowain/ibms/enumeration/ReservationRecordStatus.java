package cn.com.cowain.ibms.enumeration;

/**
 * 会议预定记录 状态
 * some Ibms Config
 *
 * @author Yuxin Zhang
 * @version 1.0
 * @since 2020-08-08
 */
public enum ReservationRecordStatus {


    /**
     * 数据库层面: 未确认
     * 前端：未开始
     */
    DEFAULT("未开始"),

    /**
     * 数据库不使用
     * 前端：进行中
     *
     */
    PENDING("进行中"),

    /**
     * 数据库不使用
     * 前端：已结束
     */
    FINISH("已结束"),

    /**
     * 数据库层面: 已取消
     */
    CANCEL("已取消");

    ReservationRecordStatus(String name){
        this.name = name;
    }

    private String name;

    public String getName(){

        return this.name;
    }

}
