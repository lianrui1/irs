package cn.com.cowain.ibms.rest.resp.audio;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Yang.Lee
 * @date 2020/12/29 16:46
 */
@Data
@ApiModel("音频文件详情对象")
public class AudioDetailResp implements Serializable {

    @ApiModelProperty(value = "音频记录ID", required = true)
    private String audioRecordId;

    @ApiModelProperty(value = "会议室名称", required = true, example = "天权")
    private String roomName;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "会议开始日期", required = true, position = 1, example = "2020-12-29")
    private LocalDate datetime;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "会议开始时间", required = true, position = 2, example = "09:00")
    private LocalDateTime fromTime;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "会议结束时间", position = 3, example = "10:00")
    private LocalDateTime endTime;

    @ApiModelProperty(value = "会议室主题", required = true, position = 4, example = "关于如何成为500强")
    private String topic;

    @ApiModelProperty(value = "音频文件url", position = 5, example = "http://file.cowain.com.cn/audio.mp3")
    private String audioFileUrl;

    @ApiModelProperty(value = "语音文本内容", position = 6, example = "本次会议开始...")
    private String text;

    @ApiModelProperty(value = "实时语音转写文本内容", position = 7)
    private List<RTASTResultResp> realtimeText;

    @ApiModelProperty(value = "音频是否未过期, true：未过期，false：已过期", position = 8, example = "true")
    private boolean notExpired;

    @ApiModelProperty(value = "会议是否已结束, true：已结束，false：未结束", position = 9, example = "true")
    private boolean meetingEnd;

    @ApiModelProperty(value = "实时语音每段步长", position = 10, required = true, example = "15")
    private int rtStep;

}
