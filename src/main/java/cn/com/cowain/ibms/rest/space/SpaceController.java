package cn.com.cowain.ibms.rest.space;

import cn.com.cowain.ibms.annotation.Idempotent;
import cn.com.cowain.ibms.feign.bean.common.CommonReq;
import cn.com.cowain.ibms.rest.bean.PageBean;
import cn.com.cowain.ibms.rest.req.iot.DeviceIdReq;
import cn.com.cowain.ibms.rest.req.space.SpacePageReq;
import cn.com.cowain.ibms.rest.req.space.SpacePurposeEditReq;
import cn.com.cowain.ibms.rest.req.space.SpaceTreeReq;
import cn.com.cowain.ibms.rest.resp.BaseListResp;
import cn.com.cowain.ibms.rest.resp.IdResp;
import cn.com.cowain.ibms.rest.resp.iot.DeviceWechatResp;
import cn.com.cowain.ibms.rest.resp.iot.IotDeviceResp;
import cn.com.cowain.ibms.rest.resp.meeting.OngoingMeetingResp;
import cn.com.cowain.ibms.rest.resp.space.*;
import cn.com.cowain.ibms.service.bean.ServiceResult;
import cn.com.cowain.ibms.service.office.SpaceCacheService;
import cn.com.cowain.ibms.service.space.SpaceService;
import cn.com.cowain.ibms.utils.ErrConst;
import cn.com.cowain.ibms.utils.IConst;
import cn.com.cowain.ibms.utils.ThreadPoolUtils;
import cn.com.cowain.sfp.comm.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.List;

import static cn.com.cowain.ibms.utils.IConst.API_BASE;

/**
 * @author Yang.Lee
 * @date 2020/12/17 14:03
 */
@Slf4j
@RestController
@RequestMapping(API_BASE + "/space")
@Api(tags = IConst.SPACE)
public class SpaceController {

    @Autowired
    private SpaceService spaceService;
    @Autowired
    SpaceCacheService spaceCacheService;
    /**
     * 新增空间信息
     *
     * @param spaceTreeReq  空间信息
     * @param bindingResult
     * @return
     */
    @Idempotent(value = API_BASE + "/space")
    @PostMapping
    @ApiOperation(value = "PC新增空间", tags = {IConst.SPACE, IConst.V19})
    public ResponseEntity<JsonResult<IdResp>> save(@RequestBody @Validated @ApiParam("空间信息") SpaceTreeReq spaceTreeReq,
                                                   BindingResult bindingResult) {

        ServiceResult result = spaceService.save(spaceTreeReq);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        //将空间信息放入缓存
        Runnable runnable=()->{
            spaceCacheService.initSpace();
        };
        ThreadPoolUtils.EXECUTOR_SERVICE_REDIS.submit(runnable);
        return ResponseEntity.ok(JsonResult.ok("OK", (IdResp) result.getObject()));
    }

    /**
     * 更新空间信息
     *
     * @param id            空间ID
     * @param spaceTreeReq  空间信息
     * @param bindingResult
     * @return
     */
    @PutMapping("/{id}")
    @ApiOperation(value = "PC更新空间信息", tags = {IConst.SPACE, IConst.V19})
    public ResponseEntity<JsonResult<String>> update(@PathVariable @ApiParam("空间ID") String id,
                                                     @RequestBody @Validated @ApiParam("空间信息") SpaceTreeReq spaceTreeReq,
                                                     BindingResult bindingResult) {

        ServiceResult result = spaceService.update(id, spaceTreeReq);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }
        //将空间信息放入缓存
        Runnable runnable=()->{
            spaceCacheService.initSpace();
        };
        ThreadPoolUtils.EXECUTOR_SERVICE_REDIS.submit(runnable);


        return ResponseEntity.ok(JsonResult.okNoData("ok"));
    }

    /**
     * 删除空间
     *
     * @param id 空间ID
     * @return
     * @author Yang.Lee
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "PC删除空间", tags = {IConst.SPACE, IConst.V19})
    public ResponseEntity<JsonResult<String>> delete(@PathVariable @ApiParam("空间ID") String id) {

        ServiceResult result = spaceService.delete(id);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.okNoData("OK"));
    }

    /**
     * 根据ID查询空间信息
     *
     * @param id 空间ID
     * @return
     * @author Yang.Lee
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "PC查询空间信息", tags = {IConst.SPACE, IConst.V19})
    public ResponseEntity<JsonResult<SpaceTreeResp>> search(@PathVariable @ApiParam("空间ID") String id) {

        ServiceResult result = spaceService.get(id);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.ok("ok", (SpaceTreeResp) result.getObject()));
    }

//    /**
//     * 查询空间列表分页数据
//     *
//     * @return
//     * @author Yang.Lee
//     */
//    @GetMapping("/page")
//    @ApiOperation(value = "PC查询空间分页数据", tags = {IConst.SPACE, IConst.V19})
//    public ResponseEntity<JsonResult<PageBean<SpaceTreeResp>>> searchPage(SpacePageReq req) {
//
//        return ResponseEntity.ok(JsonResult.okNoData("ok"));
//
//    }


    /**
     * 根据项目ID查询空间列表数据
     *
     * @param projectId 项目ID
     * @return
     */
    @GetMapping("/list")
    @ApiOperation(value = "PC查询空间列表数据", tags = {IConst.SPACE, IConst.V18, IConst.V20})
    public ResponseEntity<JsonResult<List<SpaceTreeResp>>> searchList(@RequestParam(required = false) @ApiParam(value = "项目ID", allowableValues = "项目Id或不传该参数，不传该参数表示查询所有列表")
                                                                              String projectId) {

        ServiceResult result;

        // 根据参数判断是查询所有数据还是某项目下的数据
        if (StringUtils.isEmpty(projectId)) {
            result = spaceService.getList();
        } else {
            result = spaceService.getListByProjectId(projectId);
        }
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        List<SpaceTreeResp> treeList = (List<SpaceTreeResp>) result.getObject();
        // 对结果数据排序
        treeList = spaceService.sortByCreateTime(treeList);

        return ResponseEntity.ok(JsonResult.ok("OK", treeList));
    }

    /**
     * 获取上一级空间列表
     *
     * @param projectId
     * @param level
     * @return
     */
    @GetMapping("/level/list")
    @ApiOperation(value = "获取上一级空间列表")
    public ResponseEntity<JsonResult<List<SpaceParentResp>>> getParentList(@RequestParam @ApiParam(value = "项目ID", required = true) String projectId,
                                                                           @RequestParam @Max(4) @Min(1) @ApiParam(value = "空间层级", required = true) int level) {

        ServiceResult result = spaceService.getPreLevelList(projectId, level);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.ok("ok", (List<SpaceParentResp>) result.getObject()));
    }

    /**
     * 获取项目最大层级
     *
     * @param projectId
     * @return
     */
    @GetMapping("/level/max")
    @ApiOperation(value = "获取项目最大层级")
    public ResponseEntity<JsonResult<SpaceLevelResp>> getMaxLevel(@RequestParam @ApiParam(value = "项目ID", required = true) String projectId) {

        ServiceResult result = spaceService.getMaxLevel(projectId);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.ok("OK", (SpaceLevelResp) result.getObject()));
    }

    /**
     * 获取空间内设备列表
     *
     * @param id 空间ID
     * @return 设备列表
     */
    @GetMapping("/{id}/devices")
    @ApiOperation(value = "获取空间下设备列表")
    public ResponseEntity<JsonResult<List<DeviceWechatResp>>> getDeviceListForWeChat(@PathVariable @ApiParam(value = "空间ID", required = true) String id) {

        ServiceResult result = spaceService.getDeviceList(id);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E99));
        }
        return ResponseEntity.ok(JsonResult.ok("", (List<DeviceWechatResp>) result.getObject()));
    }


    @GetMapping("/{id}/devices/page")
    @ApiOperation(value = "获取空间下设备列表(分页)", tags = {IConst.SPACE, IConst.V19})
    public ResponseEntity<JsonResult<PageBean<IotDeviceResp>>> getDevicePageForWeChat(@PathVariable @ApiParam(value = "空间ID", required = true) String id, @RequestParam(value = "page", defaultValue = "0") int page,
                                                                                      @RequestParam(value = "size", defaultValue = "20") int size) {

        PageBean<IotDeviceResp> deviceRespPage = spaceService.getDevicePageBySpace(id, page, size);
        return ResponseEntity.ok(JsonResult.ok(deviceRespPage));
    }

    /**
     * 根据项目ID和父空间ID查询空间列表（单节点，无子空间数据）
     *
     * @param projectId 项目ID
     * @param parentId  父空间ID
     * @return 空间列表
     * @author Yang.Lee
     * @date 2021/6/28 10:47
     **/
    @GetMapping("/list/singleNode")
    @ApiOperation(value = "APP 根据项目ID和父空间ID查询空间列表（单节点，无子空间数据）", tags = IConst.SPACE)
    public ResponseEntity<JsonResult<List<SpaceTreeResp>>> searchSingleNodeList(@RequestParam(required = false) @ApiParam(value = "项目ID")
                                                                                        String projectId,
                                                                                @RequestParam(required = false) @ApiParam(value = "父空间ID")
                                                                                        String parentId
    ) {


        return ResponseEntity.ok(JsonResult.ok(spaceService.getListWithoutChildren(projectId, parentId)));
    }

    /**
     * 获取空间内设备列表
     *
     * @param id 空间ID
     * @return 设备列表
     */
    @GetMapping("/{id}/devices/con")
    @ApiOperation(value = "获取空间下可控制设备列表")
    public ResponseEntity<JsonResult<List<DeviceWechatResp>>> getDeviceList(@PathVariable @ApiParam(value = "空间ID", required = true) String id) {

        ServiceResult result = spaceService.getDeviceListControl(id);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E99));
        }
        return ResponseEntity.ok(JsonResult.ok("", (List<DeviceWechatResp>) result.getObject()));
    }

    @GetMapping("/projectSpace/tree")
    @ApiOperation(value = "PC查询项目区域列表", tags = {IConst.SPACE, IConst.V17})
    public ResponseEntity<JsonResult<List<ProjectSpaceTreeResp>>> spaceTree() {
        // 查询所有项目列表
        List<ProjectSpaceTreeResp> result = spaceService.getProjectSpaceTree();
        return ResponseEntity.ok(JsonResult.ok("OK", result));
    }

    @GetMapping("/tree")
    @ApiOperation(value = "PC查询左侧项目楼栋树", tags = {IConst.SPACE, IConst.V27})
    public ResponseEntity<JsonResult<List<ProjectSpaceTreeResp>>> spaceTree2() {
        // 查询所有项目列表
        List<ProjectSpaceTreeResp> result = spaceService.getSpaceTree();
        return ResponseEntity.ok(JsonResult.ok("OK", result));
    }

    /**
     * 根据ID查询空间平面信息
     *
     * @param id 空间ID
     * @return
     * @author Yang.Lee
     */
    @GetMapping("/plane/{id}")
    @ApiOperation(value = "PC查询空间平面信息", tags = IConst.SPACE)
    public ResponseEntity<JsonResult<SpacePlaneResp>> plane(@PathVariable @ApiParam("空间ID") String id) {

        ServiceResult result = spaceService.spacePlaneDetail(id);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.ok("ok", (SpacePlaneResp) result.getObject()));
    }

    /**
     * 新增添加用途接口
     *
     * @param req           请求参数
     * @param bindingResult 校验对象
     * @return 创建结果
     * @author Yang.Lee
     * @date 2021/11/17 20:15
     **/
    @PostMapping("/purpose")
    @ApiOperation(value = "PC新增空间用途", tags = {IConst.SPACE, IConst.V19})
    public ResponseEntity<JsonResult<IdResp>> saveUse(@RequestBody @Validated SpacePurposeEditReq req, BindingResult bindingResult) {

        ServiceResult result = spaceService.addSpacePurpose(req);
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, ErrConst.E01));
        }

        return ResponseEntity.ok(JsonResult.ok((IdResp) result.getObject()));
    }

    @GetMapping("/purpose")
    @ApiOperation(value = "PC查询用途列表", tags = {IConst.SPACE, IConst.V19})
    public ResponseEntity<JsonResult<List<BaseListResp>>> findUse() {

        return ResponseEntity.ok(JsonResult.ok(spaceService.getSpacePurposeList()));
    }

    /**
     * 获取空间详情（包含设备与人员）
     *
     * @param id 空间ID
     * @return 空间详情
     * @author Yang.Lee
     * @date 2021/11/15 15:52
     **/
    @GetMapping("/{id}/allDetail")
    @ApiOperation(value = "查询空间下所有详情", tags = {IConst.SPACE, IConst.V19})
    public ResponseEntity<JsonResult<SpaceAllDetailResp>> getAllDetail(@PathVariable String id) {

        return ResponseEntity.ok(JsonResult.ok("OK", spaceService.getAllDetail(id)));
    }


    /**
     * 给TV 用
     **/
    @PostMapping("/TVDetail")
    @ApiOperation(value = "查询空间下所有详情", tags = {IConst.SPACE, IConst.V19})
    public JsonResult<List<SpaceAllDetailShowRoomResp>> getTVDetail(@RequestBody  CommonReq req) {
        return JsonResult.ok("OK", spaceService.getAllDetailForShowRoom(req.getId()));
    }


    /**
     *  showroom展厅项目获取设备 信息
     **/
    @PostMapping("/deviceInfo")
    @ApiOperation(value = "查询空间下所有详情", tags = {IConst.SPACE, IConst.V19})
    public JsonResult<DeviceInfoShowRoomResp> getDeviceInfo(@RequestBody  CommonReq req) {
        return JsonResult.ok("OK", spaceService.getDeviceInfo(req.getId()));
    }
    /**
     *  TV创建设备
     **/
    @PostMapping("/createDevice")
    @ApiOperation(value = "查询空间下所有详情", tags = {IConst.SPACE, IConst.V19})
    public JsonResult<DeviceInfoShowRoomResp> getCreateTVDevice(@RequestBody  CommonReq req) {
        return JsonResult.ok("OK", spaceService.getCreateTVDevice(req));
    }

    /**
     * 从空间中移除设备
     *
     * @param id          空间ID
     * @param deviceIdReq 需要被移除的设备ID集合
     * @return 处理结果
     * @author Yang.Lee
     * @date 2021/11/15 15:52
     **/
    @PutMapping("/{id}/Device/remove")
    @ApiOperation(value = "从空间中移除设备", tags = {IConst.SPACE, IConst.V19})
    public ResponseEntity<JsonResult<String>> removeDevices(@PathVariable String id, @RequestBody DeviceIdReq deviceIdReq) {

        ServiceResult result = spaceService.removeDevices(id, deviceIdReq.getIdList());
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, result.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.ok("ok"));
    }

    /**
     * 向空间中添加设备
     *
     * @param id          空间ID
     * @param deviceIdReq 设备ID集合
     * @return
     * @author Yang.Lee
     * @date 2021/11/15 18:42
     **/
    @PutMapping("/{id}/Device/add")
    @ApiOperation(value = "从空间中添加设备", tags = {IConst.SPACE, IConst.V19})
    public ResponseEntity<JsonResult<String>> addDevices(@PathVariable String id, @RequestBody DeviceIdReq deviceIdReq) {

        ServiceResult result = spaceService.addDevices(id, deviceIdReq.getIdList());
        if (!result.isSuccess()) {
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, result.getErrorCode()));
        }

        return ResponseEntity.ok(JsonResult.ok("ok"));
    }

    @GetMapping("/admin/list")
    @ApiOperation(value = "PC查询空间管理员列表", tags = {IConst.SPACE, IConst.V19})
    public ResponseEntity<JsonResult<List<SpaceAdminResp>>> findSpaceAdminBySpaceId(@ApiParam(value = "空间ID", required = true) String spaceId) {


        List<SpaceAdminResp> spaceAdminResps = spaceService.findSpaceAdminList(spaceId);
        return ResponseEntity.ok(JsonResult.ok("OK", spaceAdminResps));
    }

    /**
     * 获取空间列表（包含所有子空间）
     *
     * @param req 参数
     * @return 空间列表
     * @author Yang.Lee
     * @date 2021/11/18 9:55
     **/
    @GetMapping("/list/childrenAllByProject")
    @ApiOperation(value = "获取项目下空间列表（包含所有子空间）", tags = {IConst.SPACE, IConst.V19})
    public ResponseEntity<JsonResult<PageBean<SpaceDetailResp>>> getListWithChildrenAllByProject(SpacePageReq req) {

        return ResponseEntity.ok(JsonResult.ok(spaceService.getListWithChildrenAllByProject(req)));
    }

    /**
     * 查询空间下空间列表（包含所有子空间及其子空间）
     *
     * @param req 参数
     * @return 空间列表
     * @author Yang.Lee
     * @date 2021/11/18 21:02
     **/
    @GetMapping("/list/childrenAllBySpace")
    @ApiOperation(value = "获取空间列表（包含所有子空间）", tags = {IConst.SPACE, IConst.V19})
    public ResponseEntity<JsonResult<PageBean<SpaceDetailResp>>> getListWithChildrenAllBySpace(SpacePageReq req) {

        return ResponseEntity.ok(JsonResult.ok(spaceService.getListWithChildrenAllBySpace(req)));
    }

    /**
     * 获取BIM中的设备-空间数据
     *
     * @param parentSpaceId 父空间ID
     * @param keyword       查询关键字
     * @return 结果
     * @author Yang.Lee
     * @date 2021/11/18 22:18
     **/
    @GetMapping("/device/list/bim")
    @ApiOperation(value = "获取空间下所有子空间中的设备", tags = {IConst.SPACE, IConst.V19})
    public ResponseEntity<JsonResult<List<SpaceDeviceBIMResp>>> getDeviceList(@RequestParam String parentSpaceId, @RequestParam String keyword) {

        return ResponseEntity.ok(JsonResult.ok(spaceService.getBIMDeviceList(parentSpaceId, keyword)));
    }


    /**
     * 获取BIM中的设备-空间数据
     *
     * @param parentSpaceId 父空间ID
     * @param keyword       查询关键字
     * @return 结果
     * @author Yang.Lee
     * @date 2021/11/18 22:18
     **/
    @GetMapping("/user/list/bim")
    @ApiOperation(value = "获取空间下所有有权限的用户列表", tags = {IConst.SPACE, IConst.V19})
    public ResponseEntity<JsonResult<List<SpaceDeviceBIMResp>>> getUserList(@RequestParam String parentSpaceId, @RequestParam String keyword) {

        return ResponseEntity.ok(JsonResult.ok(spaceService.getBIMUserList(parentSpaceId, keyword)));
    }

    /**
     * 获取用户在空间中的设备权限详情
     *
     * @param spaceId 空间ID
     * @param empNo   用户工号
     * @return 详情
     * @author Yang.Lee
     * @date 2021/11/19 15:44
     **/
    @GetMapping("/staff/detail")
    @ApiOperation(value = "获取用户在空间中的设备权限详情", tags = {IConst.SPACE, IConst.V19})
    public ResponseEntity<JsonResult<SpaceAdminResp>> getStaffDetail(@RequestParam @ApiParam(value = "空间ID", required = true) String spaceId,
                                                                     @RequestParam @ApiParam(value = "员工工号", required = true) String empNo) {

        return ResponseEntity.ok(JsonResult.ok(spaceService.getUserSpaceAdmin(spaceId, empNo)));
    }

    @GetMapping("/search/device/properties")
    @ApiOperation(value = "查询空间下设备信息", tags = {IConst.MODULE_RESERVATION, IConst.V25})
    public ResponseEntity<JsonResult<OngoingMeetingResp>> searchDeviceProperties(@RequestParam @ApiParam(value = "空间ID", required = true) String spaceId) {

        ServiceResult result = spaceService.searchDeviceProperties(spaceId);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.ok((OngoingMeetingResp)result.getObject()));
    }

    @GetMapping("/floor/tree")
    @ApiOperation(value = "查询空间下楼层树", tags = {IConst.SPACE, IConst.V27})
    public ResponseEntity<JsonResult<SpaceResp>> searchFloorTree(@RequestParam(required = false) @ApiParam(value = "项目ID") String projectId, @RequestParam(required = false) @ApiParam(value = "空间ID") String spaceId) {

        ServiceResult result = spaceService.searchFloorTree(projectId, spaceId);
        if(!result.isSuccess()){
            return ResponseEntity.ok(JsonResult.error(String.valueOf(result.getObject()), null, null));
        }
        return ResponseEntity.ok(JsonResult.ok((SpaceResp)result.getObject()));
    }

}
