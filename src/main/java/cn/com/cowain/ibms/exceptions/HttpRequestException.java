package cn.com.cowain.ibms.exceptions;

/**
 * 发送http请求异常
 */
public class HttpRequestException extends RuntimeException{
    public HttpRequestException(String message){
        super(message);
    }

    public HttpRequestException(){
        super();
    }
}
