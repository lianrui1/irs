package cn.com.cowain.ibms.dao.temp;

import cn.com.cowain.ibms.entity.temp.AttendanceEquipment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author Yang.Lee
 * @date 2022/1/12 16:35
 */
public interface AttendanceEquipmentDao extends JpaRepository<AttendanceEquipment, String>, JpaSpecificationExecutor<AttendanceEquipment> {
}
