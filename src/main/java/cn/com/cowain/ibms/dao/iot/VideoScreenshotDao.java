package cn.com.cowain.ibms.dao.iot;

import cn.com.cowain.ibms.entity.iot.VideoScreenshot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author feng
 * @title: VideoScreenshot
 * @projectName bims
 * @Date 2021/11/24 11:12
 */
public interface VideoScreenshotDao extends JpaRepository<VideoScreenshot, String>, JpaSpecificationExecutor<VideoScreenshot> {


}
