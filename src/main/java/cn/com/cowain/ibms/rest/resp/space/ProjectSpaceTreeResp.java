package cn.com.cowain.ibms.rest.resp.space;

import cn.com.cowain.ibms.entity.iot.AccessControlSpot;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Yang.Lee
 * @date 2021/10/18 11:12
 */
@Data
@ApiModel("项目及空间树列表")
public class ProjectSpaceTreeResp {

    @ApiModelProperty(value = "节点ID,当nodeType=PROJECT时为项目ID;当nodeType=SPACE时为空间ID", required = true, position = 1)
    private String id;

    @ApiModelProperty(value = "节点类型.PROJECT:项目;SPACE:空间;ACCESS_CONTROL_SPOT:门禁点", required = true, position = 2)
    private String nodeType;

    @ApiModelProperty(value = "节点名称", required = true, position = 4)
    private String name;

    @ApiModelProperty(value = "子节点列表", required = true, position = 5)
    private List<ProjectSpaceTreeResp> childrenList;

    public static ProjectSpaceTreeResp convert(SpaceTreeResp source){
        ProjectSpaceTreeResp target = new ProjectSpaceTreeResp();

        target.setId(source.getId());
        target.setName(source.getName());
        target.setNodeType("SPACE");

        if (!CollectionUtils.isEmpty(source.getChildrenList())) {
            target.setChildrenList(source.getChildrenList().stream().map(obj -> convert(obj)).collect(Collectors.toList()));
        }

        return target;
    }

    public static ProjectSpaceTreeResp convert(AccessControlSpot source){

        ProjectSpaceTreeResp spotNode = new ProjectSpaceTreeResp();
        spotNode.setId(source.getId());
        spotNode.setName(source.getName());
        spotNode.setNodeType("ACCESS_CONTROL_SPOT");
        
        return spotNode;
    }
}
