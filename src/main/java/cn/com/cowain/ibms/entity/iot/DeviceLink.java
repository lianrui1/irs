package cn.com.cowain.ibms.entity.iot;

import cn.com.cowain.ibms.entity.BaseEntity;
import cn.com.cowain.ibms.enumeration.iot.DeviceLinkStatus;
import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Data
@Entity
@Table(name = "iot_device_link")
@org.hibernate.annotations.Table(appliesTo = "iot_device_link", comment = "设备联动基础信息表")
@Where(clause = "is_del=0")
public class DeviceLink extends BaseEntity {

    @Column(name = "number", nullable = false, columnDefinition = "varchar(30) COMMENT '编号'")
    private String number;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false, columnDefinition = "varchar(20) COMMENT '设备状态'")
    private DeviceLinkStatus status;

    @Column(name = "address", columnDefinition = "varchar(300) COMMENT '设备位置'")
    private String address;

    @ManyToOne
    @JoinColumn(name = "space_id", nullable = false, columnDefinition = "varchar(36) COMMENT 'FK 空间Id'")
    private Space space;
}
