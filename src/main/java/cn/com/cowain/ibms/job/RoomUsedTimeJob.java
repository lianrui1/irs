package cn.com.cowain.ibms.job;

import cn.com.cowain.ibms.service.ReservationRecordService;
import cn.com.cowain.ibms.utils.ThreadPoolUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author suwenhao
 * @email 2332347075@qq.com
 * @date 2021/4/8 17:33
 */
@Slf4j
@Component
public class RoomUsedTimeJob {

    @Autowired
    private ReservationRecordService reservationRecordService;

    @Scheduled(cron = "0 0 1/23 * * ? ")
    @PostConstruct
    public void roomUsedTime() {
        // 获取用户使用时长
        ThreadPoolUtils.EXECUTOR_SERVICE_REDIS.submit(() -> reservationRecordService.findRoomUsedTime());
    }
}
